<?php
	$config = array();
	
	$config["pageSize"] = 20;
	
	$config["shortDateFormat"] = '%m/%d/%Y';
	$config["longDateFormat"] = '%m/%d/%Y';
	$config["resources"]="../../classes/resource.php";	
	$config["groups"]="../../classes/groups.php";
	$config["register"]="../../classes/register.php";
	$config["superfunctions"]="../../classes/superfunctions.php";
		
?>