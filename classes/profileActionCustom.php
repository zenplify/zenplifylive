<?php
	error_reporting(0);

	include('../../classes/init.php');
	include('../../classes/profiles.php');
	include('../../classes/contact.php');
	include('../../classes/plan.php');

	$database = new database();
	$profiles = new Profiles();
	$contact = new contact();
	$plan = new plan();
	$userId = $_POST['userId'];
	$pageId = $_POST['pageId'];
 	$filledQuestions;
	$questions = $profiles->getPageQuestions($database, $_POST['pageId'], -1, 1);
	$emptyFlag = true;
	foreach($questions as $q)
	{
		$name = 'question'.$q->questionId;
		if($q->typeId != '5')
		{
			if(!empty($_POST[$name]) || $_POST[$name] != '')
			{
				$filledQuestions .= $q->questionId.',';
				preg_match('/[a-zA-Z]+:\/\/[0-9a-zA-Z;.\/?:@=_#&%~,+$]+/', $_POST[$name], $matches);
				if(!isset($matches[0]))
				{
					$emptyFlag = true;
				}
				else
				{
					$emptyFlag = false;
					break;
				}

			}
			else
			{
				if($name == 'question36' || $name == 'question44' || $name == 'question28' || $name == 'question15' || $name == 'question13' || $name == 'question16')
				{
					$emptyFlag = true;
				}
				else
				{
					$emptyFlag = false;
					break;
				}
			}
		}
		else
		{
			$answers = $profiles->getQuestionChoices($database, $q->questionId);
			foreach($answers as $radio)
			{
				$name = 'answer'.$radio->choiceId;
				if(!empty($_POST[$name]) || $_POST[$name] != '')
				{
					$emptyFlag = true;
				}
				else
				{
					$emptyFlag = false;
					break;
				}

			}

		}

	}
	$filledQuestionsArray = explode(',', $filledQuestions);

	if($emptyFlag != false)
	{
 		$flag = 1;
		if(isset($_POST['email']))
		{
			if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
			{
				$flag = 0;
			}
			else
			{
				if($_POST['pageName'] == 'guestProfile')
				{
					if(empty($_POST['firstName']) || empty($_POST['lastName']) || empty($_POST['phone']) || empty($_POST['birthday_month']) || empty($_POST['birthday_day']) || empty($_POST['birthday_year']) || empty($_POST['referredBy']) || sizeof($_POST['bestTimeToCall']) == 0 || empty($_POST['street']) || empty($_POST['city']) || empty($_POST['state']) || empty($_POST['zipCode']) || empty($_POST['country']))
					{
						$flag = 0;
					}
				}
				elseif(empty($_POST['firstName']) || empty($_POST['lastName']) || empty($_POST['phone']))
				{
					$flag = 0;
				}
			}

		}
		else
		{
			$flag = 0;
		}

 		if($flag == 1)
		{
			$results = $profiles->generateContact($database, $_POST['pageId'], $_POST['userId'], $_POST['firstName'], $_POST['lastName'], $_POST['email'], $_POST['phone'], $_POST['viewId'], $_POST['pageUrl'], $_POST['code'], $_POST['pageName']);
			$contactId = $results['contactId'];
			$replyId = $results['replyId'];
			$emailId = $results['emailId'];
			$contact->createPermissionRequest($_POST['userId'], $contactId, $emailId);
			$contactInfo = '<b>First Name:</b> '.$_POST['firstName'].'<br /><b>Last Name:</b> '.$_POST['lastName'].'<br /><b>Birthday:</b> '.$_POST['birthday_month'].'/'.$_POST['birthday_day'].'/'.$_POST['birthday_year'].'<br /><b>Phone:</b> '.$_POST['phone'].'<br />'.'<b>Email:</b> '.$_POST['email'].'<br />';
			$customRules = $profiles->getPageCustomRules($database, $pageId, 1);
			$customQuestionRules = $profiles->getPageCustomRules($database, $pageId, 2);
			foreach($customRules as $cr)
			{
				$groupId = $profiles->addToGroup($database, $contactId, $cr->groupId);
				$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId, '1');
			}
			foreach($customQuestionRules as $cqr)
			{
				if(in_array($cqr->questionId, $filledQuestionsArray))
				{
					if(!(is_null($cqr->groupId)))
					{
						if(is_array($_POST['question'.$cqr->questionId]))
						{
							foreach($_POST['question'.$cqr->questionId] as $questionChoice)
							{
								if(strstr($cqr->questionChoices,$questionChoice))
								{
									$groupId = $profiles->addToGroup($database, $contactId, $cqr->groupId);
									$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId, '1');
									//echo $_POST['question'.$cqr->questionId].' is choice';
									//echo '<br>';
								}
								//match questionchoice with choices in qustomruletable
							}
						}
						else
						{
							if(strstr($cqr->questionChoices,$_POST['question'.$cqr->questionId]))
							{
								$groupId = $profiles->addToGroup($database, $contactId, $cqr->groupId);
								$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId, '1');
								//echo $_POST['question'.$cqr->questionId].' is choice';
								//echo '<br>';
							}
						}
						//echo 'assign '.$cqr->groupName;
						//echo '<br>';
					}
					if(!(is_null($cqr->planId)))
					{
						if(is_array($_POST['question'.$cqr->questionId]))
						{
							foreach($_POST['question'.$cqr->questionId] as $questionChoice)
							{
								if(strstr($cqr->questionChoices,$questionChoice))
								{
									$groupId = $profiles->addToGroup($database, $contactId, $cqr->groupId);
									$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId, '1');
									//echo $_POST['question'.$cqr->questionId].' is choice';
									//echo '<br>';
								}
								//match questionchoice with choices in qustomruletable
							}
						}
						else
						{
							if(strstr($cqr->questionChoices,$_POST['question'.$cqr->questionId]))
							{
								$groupId = $profiles->addToGroup($database, $contactId, $cqr->groupId);
								$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId, '1');
								//echo $_POST['question'.$cqr->questionId].' is choice';
								//echo '<br>';
							}
						}
					}
				}
			}
			//die();

			if($_POST['pageName'] == 'guestProfile')
			{
				//				$birthday = $_POST['birthday_month'].'/'.$_POST['birthday_day'].'/'.$_POST['birthday_year'];
				//				$address = $profiles->addMailingAddress($database, $replyId, $contactId, $_POST['userId'], $birthday, $_POST['referredBy'], $_POST['bestTimeToCall'], $_POST['street'], $_POST['city'], $_POST['state'], $_POST['zipCode'], $_POST['country']);
				//				//fetch group linked to that campaign and add this contact to a group using groupId instead Group Name
				//				$groupId = $profiles->addToGroup($database, $contactId, 'Guests');
				//				$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $_POST['userId'], '1');
				//				$bestTime = implode(", ", $_POST['bestTimeToCall']);
				//				$contactInfo = $contactInfo.'<b>Street:</b> '.$_POST['street'].'<br /><b>City:</b> '.$_POST['city'].'<br /><b>State/Prov:</b> '.$_POST['state'].'<br /><b>Zip/Postal:</b> '.$_POST['zipCode'].'<br /><b>Country:</b> '.$_POST['country'].'<br /><b>Best Times to Call:</b> '.$bestTime.'<br /><b>Referred by:</b> '.$_POST['referredBy'].'<br />';
				//				$profiles->sendPermissionEmail($database, $_POST['userId'], $contactId);
			}
			if($_POST['pageName'] == 'consultantProfile')
			{
				//$planId = $profiles->addToPlan($database, $contactId, 'Consultant Profile Response (automatic)', $_POST['userId']);
			}
			$mailQuesions = '';
			foreach($questions as $q)
			{
				$mailQuesions = $mailQuesions.'<b>'.$q->label.':</b> ';
				$name = 'question'.$q->questionId;
				if($q->typeId == '1' || $q->typeId == '2')
				{
					$ans = str_replace("'", "\'", $_POST[$name]);
					$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$ans."', '".$contactId."')");
					$mailQuesions = $mailQuesions.$ans.'<br />';

				}
				else if($q->typeId == '3')
				{
					$choices = array();
					if($q->questionId == '10' || $q->questionId == '14' || $q->questionId == '42')
					{
						$interests = '';
						foreach($_POST[$name] as $checkox)
						{
							$inter = $database->executeObject("SELECT choiceText from pageformquestionchoices where choiceId ='".$checkox."'");
							$start = strpos($inter->choiceText, '<b>');
							$stop = strpos($inter->choiceText, ':</b>');
							$stop = $stop-3;
							$choiceText = substr($inter->choiceText, $start+3, $stop);
							if($choiceText == 'NO THANK YOU')
								$choiceText = 'CLIENT';
							if($interests != '')
								$interests = $interests.', '.$choiceText;
							else
								$interests = $choiceText;
							//$mailQuesions=$mailQuesions.$interests.'<br />';
						}
						$database->executeNonQuery("UPDATE contact SET interest='".$interests."' WHERE contactId='".$contactId."'");

					}
					$j = 0;
					foreach($_POST[$name] as $checkox)
					{
						$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$checkox."', '".$contactId."')");
						$choice = $database->executeScalar("SELECT choiceText from pageformquestionchoices where choiceId ='".$checkox."'");
						$choices[$j] = $choice;
						$j++;
					}
					$answerss = implode(', ', $choices);
					$mailQuesions = $mailQuesions.$answerss.'<br />';
				}
				else if($q->typeId == '4')
				{
					$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$_POST[$name]."', '".$contactId."')");
					$choice = $database->executeScalar("SELECT choiceText from pageformquestionchoices where choiceId ='".$_POST[$name]."'");
					$mailQuesions = $mailQuesions.$choice.'<br />';
				}
				else if($q->typeId == '5')
				{
					$i = 0;
					$answers = $profiles->getQuestionChoices($database, $q->questionId);
					foreach($answers as $radio)
					{
						$name = 'answer'.$radio->choiceId;
						$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, value, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$radio->choiceId."', '".$_POST[$name]."' ,'".$contactId."')");
						if($q->questionId == '21')
						{
							$options = array('In your first year', 'In 1-2 years', 'In 2-3 years', 'In 3-4 years', 'In 5+ Years');
							$answersss = $options[$i].':'.$_POST[$name];
							$answerss = $answersss;
							$i++;
							$mailQuesions = $mailQuesions.$answerss.'<br />';
						}
						if($q->questionId == '27')
						{
							$options1 = array("Facebook", "Technology in general", "Talking to people you know", "Talking to people you don't know", "Talking to groups of people", "Talking to groups of people", "Talking on the phone", "Inviting someone to try a sample", "Recommending products", "Taking an order", "Customer service", "Recruiting/Hiring", "Teaching/Training");
							$answersss = $options1[$i].':'.$_POST[$name];
							$answerss = $answersss;
							$i++;
							$mailQuesions = $mailQuesions.$answerss.'<br />';
						}
						if($q->questionId == '3')
						{
							$options2 = array("Sleep", "Energy", "Appetite", "Digestion", "Menses/ Men's health", "Mood", "Muscles/ joints/ body pain", "Skin", "Stress Management"); //,"Sex"
							$answersss = $options2[$i].':'.$_POST[$name];
							$answerss = $answersss;
							$i++;
							$mailQuesions = $mailQuesions.$answerss.'<br />';
						}
					}

				}

			}
			//echo $contactInfo.$mailQuesions;
			$pageTitle = $profiles->getPageTitle($database, $_POST['pageId']);
			$userInfo = $database->executeObject("SELECT * FROM user WHERE userId='".$_POST['userId']."'");
			/*$headers = "From: info@zenplify.biz\r\n";
			$headers .= "Content-Type: text/html";*/
			$headers = "From:".$userInfo->firstName." ".$userInfo->lastName." <".$userInfo->email.">\r\n"."Reply-To: ".$_POST['email']."\r\n";
			$headers .= "Content-Type: text/html";
			$body = nl2br($contactInfo.$mailQuesions);
			$subject = $pageTitle.' - '.$_POST['firstName'].' '.$_POST['lastName'];
			$mailBody = '<html><body>'.$body.'</html></body>';
			$additionalParameter = "-f ".$_POST['email'];
			$profiles->contactfillmaillog($database, $_POST['userId'], $contactId, $userInfo->email, $subject, $body, $headers, $additionalParameter);
			mail($userInfo->email, $subject, $mailBody, $headers, $additionalParameter);
			//mail($userInfo->email,$pageTitle.' - '.$_POST['firstName'].' '.$_POST['lastName'],'<html><body>'.$body.'</html></body>',$headers,"-f ".$_POST['email']."");
			//die();
		}
	}
 	echo '<head><META http-equiv="refresh" content="0;URL='.$CONF['siteURL'].$userInfo->userName.'/'.$_POST['code'].'/Thanks.html"></head> ';
?>