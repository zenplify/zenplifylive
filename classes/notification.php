<?php
    require_once('init.php');
    require_once('group.php');
    require_once('profiles.php');

    class notification
    {

        function notificationToUser($database, $leaderId, $changeId, $type, $action, $text)
        {
            $tableName = "";
            if (isset($type) && $type == 1)
            {
                $tableName = "pages";
                if ($action == 1)
                {
                    $sql = "select * from `user` where leaderId = '" . $leaderId . "'";
                }
                else
                {
                    $sql = "Select u.* from `user` u  LEFT JOIN pages p on u.userId=p.userId where p.`oldId` = '" . $changeId . "' AND p.`status`='1'";
                }
//				$sql = "Select u.* from `user` u  LEFT JOIN pages p on u.userId=p.userId where p.`pageId` = '" . $changeId . "'";
//				$sql = "Select * from `user` where `leaderId` = '" . $leaderId . "'";
            } //profiles
            else if (isset($type) && $type == 2)
            {
                $tableName = "groups";
                if ($action == 1)
                {
                    $sql = "SELECT userId FROM `user` WHERE leaderId = '" . $leaderId . "' AND userId NOT IN(SELECT userId FROM groupusers WHERE groupId = '" . $changeId . "')";
                }
                else if ($action == 2)
                {
                    $sql = "SELECT `userId` from `groupusers` where `groupId` = '" . $changeId . "' and `isactive`= 1 and `userId` NOT IN('" . $leaderId . "')";
                }
                else if ($action == 3)
                {
                    $sql = "SELECT `userId` from `groupusers` where `groupId` = '" . $changeId . "' and `isactive`= 1 and `userId` NOT IN('" . $leaderId . "')";
                }
//				$sql = "Select * from `user` where `leaderId` = '" . $leaderId . "'";
            } //groups
            else if (isset($type) && $type == 3)
            {
                $tableName = "plan";
                if ($action == 2)
                {
                    $sql = "SELECT `userId` FROM `plan` WHERE `oldId` = '" . $changeId . "' AND `isactive` = 1 and userId not in('" . $leaderId . "')";
                }
                else if ($action == 3)
                {
                    $sql = "SELECT `userId` FROM `plan` WHERE `oldId` = '" . $changeId . "' AND `isactive` = 1 and `leaderId` = '" . $leaderId . "'";
                }
                else
                {
                    $sql = "Select * from `user` where `leaderId` = '" . $leaderId . "'";
                }
            } //plans
            else if (isset($type) && $type == 5)
            {
                $tableName = "plansteps";
                $planDetails = $database->executeObject("select planId, title from plan where planId in (select planId from plansteps where stepId = '" . $changeId . "')");
                $planId = $planDetails->planId;
                $planName = $planDetails->title;
                if ($action == 1)
                {
                    $sql = "SELECT u.userId FROM `user` u LEFT JOIN plan p ON p.`userId` = u.`userId` WHERE u.leaderId = '" . $leaderId . "' AND p.`oldId` = '" . $planId . "' and p.isactive = 1";
                }
                else if ($action == 2)
                {
                    $sql = "SELECT u.userId FROM `user` u JOIN plan p ON p.`userId` = u.`userId` JOIN plansteps ps ON ps.planId = p.`planId` WHERE ps.oldId ='" . $changeId . "'";
                }
                elseif ($action == 3)
                {
                    $sql = "SELECT u.userId  FROM `user` u  LEFT JOIN plan p ON p.`userId` = u.`userId` LEFT JOIN plansteps ps ON ps.`planId` = p.`planId` WHERE u.leaderId = '" . $leaderId . "' AND p.`oldId` = '" . $planId . "' AND p.isactive = 1 AND ps.oldId = '" . $changeId . "' AND ps.`isactive` = 1 ";
                }
            } //steps
            $database->executeNonQuery("insert into notification (type,textToShow,leaderId,changeId,action,tablename,datetime) values('" . $type . "','" . $text . "','" . $leaderId . "','" . $changeId . "','" . $action . "','" . $tableName . "',NOW())");
            $notificationId = $database->insertid();
            $users = $database->executeObjectList($sql);

            foreach ($users as $u)
            {
                $notificationlog = $database->executeNonQuery("insert into notificationuserstatus (notificationId,userId,status,datetime) values('" . $notificationId . "','" . $u->userId . "','1',NOW())");
            }
            return 'done';
        }

        function changeUserNotification($database, $plan, $userNotificationId, $status)
        {
            if ($status == 1) // 1 FOR not now
            {
                $database->executeNonQuery("UPDATE notificationuserstatus SET dismissTime=NOW(),status=2 WHERE userstatusId='" . $userNotificationId . "'");
            }
            else if ($status == 2) // 2 FOR confirm
            {
                $database->executeNonQuery("UPDATE notificationuserstatus SET acceptTime=NOW(),status=3 WHERE userstatusId='" . $userNotificationId . "'");
                $notificationdetail = $database->executeAssoc("select notifuser.userId,notif.leaderId,notif.changeId,notif.type,notif.tablename, notif.action from notificationuserstatus notifuser join notification notif  on notifuser.notificationId=notif.notificationId where notifuser.userstatusId='" . $userNotificationId . "'");

                $action = $notificationdetail["action"];
                $groupId = $notificationdetail["changeId"];
                $planId = $notificationdetail["changeId"];
                $stepId = $notificationdetail["changeId"];
                $changeId = $notificationdetail["changeId"];

                $userId = $notificationdetail["userId"];
                $leaderId = $notificationdetail["leaderId"];

                $type = $notificationdetail['type'];
                $group = new group();
                $profile = new profiles();
//				$plans = new plan();
                if ($notificationdetail["type"] == 1)
                {
                    if ($action == 1)
                    {
                        $profile->publishProfileUser($database, $changeId, $userId, $leaderId);
                    }
                    else if ($action == 2)
                    {

                        $profile->EditProfileForUser($database, $changeId, $userId, $leaderId);
                    }
                    else if ($action == 3)
                    {
                        $profile->archiveProfileUser($database, $changeId, $userId, $leaderId);
                    }
                } //profiles
                else if ($notificationdetail["type"] == 2)
                {
                    if ($action == 1) //  add
                        $group->publishGroup($database, $groupId, $userId, $leaderId);
                    else if ($action == 3) //  archive
                        $group->archiveGroupForUser($database, $groupId, $userId);
                    else if ($action == 2) // edit
                        //$group->archiveGroups( $groupId,$userId);
                        $group->editGroupFroUser($groupId, $userId, $leaderId);
                } //groups
                else if ($notificationdetail["type"] == 3)
                {
                    if ($action == 1) // 1 add
                        $plan->publishPlan($database, $planId, $userId, $leaderId);
//							$plan->publishPlan($database, $planId, $userId, $leaderId);
                    else if ($action == 2) // 3 Edit Plan
                        $plan->editPlanFromUser($database, $planId, $userId, $leaderId);
                    else if ($action == 3) // 3 archive
                        $plan->archiveUserPlan($database, $planId, $userId, $leaderId);
                } //plan
                else if ($notificationdetail["type"] == 5)
                {
                    if ($action == 1)
                    {
                        $plan->publishStep($database, $changeId, $userId, $leaderId);
                    }
                    else if ($action == 2)
                    {
                        $plan->EditPlanTaskForUser($database, $changeId, $userId, $leaderId);
                        $changeStepId = $database->executeScalar("SELECT stepId FROM plansteps ps WHERE ps.oldId = '" . $changeId . "' AND ps.planId IN (SELECT planId FROM plan WHERE leaderId = '" . $leaderId . "' AND userId = '" . $userId . "' AND oldId IN (SELECT planId FROM plansteps WHERE stepId = '" . $changeId . "'))");

                        $stepToArchive = $database->executeScalar("SELECT stepId FROM plansteps WHERE parentId=(SELECT parentId FROM plansteps WHERE stepId='" . $changeStepId . "') AND isactive='1' AND isArchive='0' AND planId=
(SELECT  planId FROM plan WHERE leaderId = '" . $leaderId . "' AND userId = '" . $userId . "' AND oldId IN (SELECT planId FROM plansteps WHERE stepId = '" . $changeId . "'))
");
                        $plan->DisableStep($database, $stepToArchive);
//						self::markArchive($database, $changeStepId, $type, 0);
                    }
                    else if ($action == 3)
                    {
                        $changeStepId = $database->executeScalar("SELECT stepId FROM plansteps ps WHERE ps.oldId = '" . $changeId . "' AND ps.planId IN (SELECT planId FROM plan WHERE leaderId = '" . $leaderId . "' AND userId = '" . $userId . "' AND oldId IN (SELECT planId FROM plansteps WHERE stepId = '" . $changeId . "'))");
                        $plan->archiveStepForUser($database, $changeStepId);
                        //self::markArchive($database, $changeStepId, $type, 0);

                    }
                } //plansteps
            }
        }

        function markArchive($database, $id, $type, $action)
        {
            if (isset($type) && $type == 1)
            {
                $tableName = "pages";
                $updateId = "pageId";
                $field = "status";
                $archive = "isArchive";
                $isArchive = ($action == 0 ? '1' : 0);
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "', `" . $archive . "` = '" . $isArchive . "' where `" . $updateId . "` = '" . $id . "'");
            }
            else if (isset($type) && $type == 2)
            {
                $tableName = "groupusers";
                $updateId = "groupId";
                $field = "isactive";
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "' where `" . $updateId . "` = '" . $id . "'");
            }
            else if (isset($type) && $type == 3)
            {
                $tableName = "plan";
                $updateId = "planId";
                $field = "isactive";
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "' where `" . $updateId . "` = '" . $id . "'");
            }
            else if (isset($type) && $type == 5)
            {
                $tableName = "plansteps";
                $updateId = "stepId";
                $field = "isactive";
                $archive = "isArchive";
                $isArchive = ($action == 0 ? '1' : 0);

                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "', `" . $archive . "` = '" . $isArchive . "' where `" . $updateId . "` = '" . $id . "'");
            }
        }

        function markunArchive($database, $id, $type, $action, $userId)
        {
            if (isset($type) && $type == 1)
            {
                $tableName = "pages";
                $updateId = "pageId";
                $field = "status";
                $isArchive = "isArchive";
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "',`" . $isArchive . "`='0' where `" . $updateId . "` = '" . $id . "' AND userId='" . $userId . "'");
            }
            else if (isset($type) && $type == 2)
            {
                $tableName = "groupusers";
                $updateId = "groupId";
                $field = "isactive";
                $isArchive = "isArchive";
                echo("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "',`" . $isArchive . "`='0' where `" . $updateId . "` = '" . $id . "' AND userId='" . $userId . "'");
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "',`" . $isArchive . "`='0' where `" . $updateId . "` = '" . $id . "' AND userId='" . $userId . "'");
            }
            else if (isset($type) && $type == 3)
            {
                $tableName = "plan";
                $updateId = "planId";
                $field = "isactive";
                $isArchive = "isArchive";
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "',`" . $isArchive . "`='0' where `" . $updateId . "` = '" . $id . "' AND userId='" . $userId . "'");
            }
            else if (isset($type) && $type == 5)
            {
                $tableName = "plansteps";
                $updateId = "stepId";
                $field = "isactive";
                $isArchive = "isArchive";
                $database->executeNonQuery("Update `" . $tableName . "` set `" . $field . "` = '" . $action . "',`" . $isArchive . "`='0' where `" . $updateId . "` = '" . $id . "'");
            }

        }

        function getNotification($database, $userId)
        {
            return $database->executeObjectList("select notifuser.userstatusId,notifuser.userId,notif.leaderId,notif.changeId,notif.type,notif.tablename,notif.textToShow,notif.`action`,notif.`tablename` from notificationuserstatus notifuser join notification notif  on notifuser.notificationId=notif.notificationId where notifuser.userId='" . $userId . "' AND notifuser.status=1");
        }

        function markPublish($database, $id, $type, $action)
        {
            if (isset($type) && $type == 1)
            {
                $tableName = "pages";
                $updateId = "pageId";
            }
            else if (isset($type) && $type == 2)
            {
                $tableName = "groups";
                $updateId = "groupId";
            }
            else if (isset($type) && $type == 3)
            {
                $tableName = "plan";
                $updateId = "planId";
            }
            else if (isset($type) && $type == 5)
            {
                $tableName = "plansteps";
                $updateId = "stepId";
            }
            $database->executeNonQuery("Update `" . $tableName . "` set privacy = '" . $action . "' where `" . $updateId . "` = '" . $id . "'");
        }

        function getNotNowNotifications($userId)
        {
            $database = new database();

            $getNotNowNotifications = $database->executeObjectList("SELECT nus.*,notif.leaderId,notif.changeId,notif.type,notif.tablename,notif.textToShow,notif.`action`,notif.`tablename` FROM `notificationuserstatus` nus LEFT JOIN notification notif ON notif.`notificationId` =nus.`notificationId` WHERE nus.userId = '" . $userId . "' AND nus.`status` = 2");

            return $getNotNowNotifications;
        }

        function getArchivedData($database, $userId, $type, $txtSearch)
        {
            $search;
            if (isset($type) && $type == 1)
            {
                if (!empty($txtSearch))
                {
                    $search = "and `p`.`newCode` like '%" . $txtSearch . "%'";
                }
                $sql = "select `p`.`pageId` ,`p`.`newCode` from `pages` `p`  where `p`.`userId` = '" . $userId . "' AND `p`.`status` = 0 AND `p`.`isArchive` = 1 " . $search;
            }
            else if (isset($type) && $type == 2)
            {
                if (!empty($txtSearch))
                {
                    $search = "and `g`.`name` like '%" . $txtSearch . "%'";
                }
                $sql = "SELECT `gu`.`id`,`gu`.`groupId` ,`g`.`name` FROM `groupusers` `gu` JOIN `groups` `g` ON `g`.`groupId` = `gu`.`groupId` WHERE `gu`.`userId` = '" . $userId . "' AND `gu`.`isactive` = 0 AND `gu`.`isArchive` = 1 " . $search;
            }
            else if (isset($type) && $type == 3)
            {
                if (!empty($txtSearch))
                {
                    $search = "and `p`.`title` like '%" . $txtSearch . "%'";
                }
                $sql = "SELECT `p`.`planId` ,`p`.`title` FROM `plan` `p` WHERE `p`.`userId` = '" . $userId . "' AND `p`.`isactive` = 0 AND `p`.`isArchive` = 1 " . $search;
            }
            else if (isset($type) && $type == 5)
            {
                if (!empty($txtSearch))
                {
                    $search = "and `ps`.`summary` like '%" . $txtSearch . "%'";
                }
                $sql = "SELECT `ps`.`stepId` ,`ps`.`summary` FROM `plansteps` `ps` JOIN `plan` `p` ON `p`.`planId` = `ps`.`planId` WHERE `p`.`userId` = '" . $userId . "' AND `ps`.`isactive` = 0 AND `ps`.`isArchive` = 1 " . $search;
            }
            return $database->executeObjectList($sql);
        }

    }

?>