<?php
/**
 * Created by PhpStorm.
 * User: Mansoor
 * Date: 10/14/2015
 * Time: 11:03 AM
 */

require_once("classes/init.php");

$database = new database();

// get users
$users = getAllUsers($database);

// insert in signature table
foreach ($users as $user) {

    addPermissionEmail($database,$user->userId);

}

// get all users
function getAllUsers($database) {

    $sql="SELECT userId from `user`";
	return $database->executeObjectList($sql);
}


// insert signature into new table
function addPermissionEmail($database,$userId){

$email='<p>Thank you for submitting a Profile.  Your information is safe and will not be shared!</p>

<p>Pretty please click the approve link below if you want to receive my newsletter so the internet police know I have your permission!</p>

<p>To Accept Emails from [{First Name}] click the link below.</p>

<p>[{Accept Emails Link}]<br />
To Decline Emails from [{First Name}]&nbsp;click the link below:</p>

<p>[{DeclineEmails Link}]</p>';
$subject="Newsletter Permission from [{First Name}]";

   return $addSignature= $database->executeNonQuery("INSERT INTO `userpermissionemail` (userId,permissionEmailSubject,permissionEmailBody) VALUE ('".$userId."','".$subject."','".mysql_real_escape_string($email)."')");

}
echo 'complete';