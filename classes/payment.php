<?php
	require_once('init.php');

	class payment
	{

		function getUser($payer_id){
			$database = new database();
			return $database->executeObject("SELECT * FROM user WHERE payerId='".$payer_id."'");

		}

		function verifyPaypallUser($userId){
			$database = new database();
			return $database->executeNonQuery("UPDATE user SET isVerifiedPaypal=1 WHERE userId='".$userId."'");

		}

		function orderConfirmation($planname){
			$database = new database();
			return $database->executeObject("SELECT * FROM useraccountpaymentplans WHERE title='".$planname."'");
		}

		function trialPeriod($userId){
			$database = new database();
			return $database->executeObject("SELECT up.* FROM userpayments up INNER JOIN (SELECT MAX(transactionDateTime) AS MaxDateTime FROM userpayments WHERE userId='".$userId."' )upay ON up.transactionDateTime = upay.MaxDateTime");

		}

		function userPayments($userId, $paymentuserInformation){
			$database = new database();
			$getplanInfo = $database->executeObject("select * from useraccountpaymentplans where product_name='".$paymentuserInformation['product_name']."'");
			$startdate = date('Y-m-d');
			$expirydate = Date('Y-m-d', strtotime($paymentuserInformation['next_payment_date']));
			$transactionDateTime = date('Y-m-d H:i:s');
			return $database->executeNonQuery("INSERT INTO userpayments(userId, planId, fee, startDate, expiryDate, choosePlanTitle, duration, billingId, transactionId, transactionStatus, transactionDateTime) VALUES ('".$userId."','".$getplanInfo->planId."','".$getplanInfo->fee."','".$startdate."','".$expirydate."','".$getplanInfo->title."','".$getplanInfo->duration."','".$paymentuserInformation['payer_id']."','".$paymentuserInformation['txn_id']."','".$paymentuserInformation['payment_status']."','".$transactionDateTime."') ");
		}

        function userPaymentsforOntraPort($userId, $chargeAmount,$nexPaymentDate,$planTitle){
            $database = new database();
            $productuName='ontraport';

            $startdate = date('Y-m-d');
            $expirydate = Date('Y-m-d', strtotime($nexPaymentDate));
            $transactionDateTime = date('Y-m-d H:i:s');
            $mysqlquery="INSERT INTO userpayments(userId, planId, fee, startDate, expiryDate, choosePlanTitle, duration, billingId, transactionId, transactionStatus, transactionDateTime) VALUES ('".$userId."','11','".$chargeAmount."','".$startdate."','".$expirydate."','".$planTitle."','30','ontraport','ontraport','1','".$transactionDateTime."')";
            $database->executeNonQuery($mysqlquery);

        }

		function userPaymentsAsTrial($userId, $paymentuserInformation){
			$database = new database();
			$gettrialInfo = $database->executeObject("select * from useraccountpaymentplans where planId=1");
			$startdate = date('Y-m-d');
			$transactionDateTime = date('Y-m-d H:i:s');
			$expirydate = Date('Y-m-d', strtotime($paymentuserInformation['next_payment_date']));
			return $database->executeNonQuery("INSERT INTO userpayments(userId, planId, fee, startDate, expiryDate, choosePlanTitle, duration, billingId, transactionId, transactionStatus, transactionDateTime) VALUES ('".$userId."','".$gettrialInfo->planId."','".$gettrialInfo->fee."','".$startdate."','".$expirydate."','".$gettrialInfo->title."','".$gettrialInfo->duration."','".$paymentuserInformation['payer_id']."','".$paymentuserInformation['txn_id']."','".$paymentuserInformation['payment_status']."','".$transactionDateTime."') ");

		}

		public function PlanInformation($planId){
			$database = new database();
			return $database->executeObject("SELECT * FROM useraccountpaymentplans WHERE planId='".$planId."'");
		}

		function isPaypalVerified($userId){
			$database = new database();
			return $database->executeScalar("SELECT isVerifiedPaypal FROM user WHERE userId='".$userId."'");
		}

		function AssignPaymentIdToUser($payerId, $userId){
			$database = new database();
			$database->executeNonQuery("UPDATE user SET payerId='".$payerId."' WHERE userId='".$userId."'");

		}

		function IsUserExistAgainstPayerId($payerId){
			$database = new database();
			$result = $database->executeObjectList("Select * from user WHERE payerId='".$payerId."'");
			if(empty($result)){
			return false;
			}else{
			return true;
			}
		}

		function IsPayerIdExists($payerId, $userId){
			$database = new database();
			$totalExists = $database->executeScalar("Select count(payerId) as totalExists from user WHERE payerId='".$payerId."' and userId<>'".$userId."'");
			return $totalExists;

		}

		function IsUserExistAgainstPayerIdWithUserId($payerId, $userId){
			$database = new database();
			$result = $database->executeObjectList("Select * from user WHERE payerId='".$payerId."' and userId != ".$userId);
			if(empty($result)){return false;}else{return true;}
					
			}

		function addPaypalProfile($userId, $profileId){
			$database = new database();
			$database->executeNonQuery("UPDATE user SET PaypalProfileId='".$profileId."', isPaypalProfileActive=1, varificationDate=NOW() WHERE userId='".$userId."'");

		}

		function deletePaypalProfile($userId){
			$database = new database();
			$database->executeNonQuery("UPDATE user SET payerId='', PaypalProfileId='', isPaypalProfileActive=0 WHERE userId='".$userId."'");

		}

		function logPaypalTransactionDetails($post, $response){
			$details = '';
			foreach($post as $key => $value)
				$details .= $key.' : '.$value." \n";
			$database = new database();
			$database->executeNonQuery("INSERT INTO userpaymentstransactions (userId, payerId, response, type, ipnTrackId, transactionDate, details) select userId, '".$post['payer_id']."', '".$response."', '".$post['txn_type']."', '".$post['ipn_track_id']."', now(), '".$details."' from user where payerId = '".$post['payer_id']."'");

		}

		function getUserByProfileId($profileId){
			$database = new database();
			return $database->executeObject("SELECT * FROM user WHERE paypalProfileId='".$profileId."' OR paypalProfileId='".str_replace("-", "%2d", $profileId)."' ");

		}

		function getpaymentPlans($accounttype){
			$database = new database();
			return $database->executeObjectList("SELECT * FROM useraccountpaymentplans where accountTypeId='".$accounttype."'");
		}

		function getPaypalTransactionDetail($userId){
			$database = new database();
			return $database->executeObject("SELECT * FROM userpaymentstransactions where userId='".$userId."' order by transactionDate desc limit 1");
		}

		function getUserById($userId){
			$database = new database();
			return $database->executeObject("SELECT * FROM user WHERE userId='".$userId."'");

		}

		function getPaidUser(){
			$database = new database();
			$resultPaidUser =$database->executeScalar("SELECT count(distinct userId) FROM userpayments WHERE planId<>1 AND userId<>0 AND CURDATE()<=expiryDate AND DATEDIFF(`expiryDate`,`startDate`)<='92' ");
			//$totalRows=mysql_num_rows($resultPaid);
			return $resultPaidUser;

		}

		function getLastSevenDaysPaidUser(){
			$database = new database();
			$resultPaidSevenDays = $database->executeScalar("SELECT  count(distinct userId) FROM userpayments u WHERE u.startDate >=(now()-INTERVAL 7 DAY) AND u.planId<>1 AND u.userId<>0 AND u.userId NOT IN (SELECT p.userId from userpayments p GROUP BY p.userId having count(p.paymentId) > 2)  ");
			//$totalRows=mysql_num_rows($resultPaid);
			return $resultPaidSevenDays;

		}

		function getLastFifteenDaysPaidUser(){
			$database = new database();
			$resultPaidFifteenDays = $database->executeScalar("SELECT  count(distinct userId) FROM userpayments u WHERE u.startDate >=(now()-INTERVAL 15 DAY) AND u.planId<>1 AND u.userId<>0 AND u.userId NOT IN (SELECT p.userId from userpayments p GROUP BY p.userId having count(p.paymentId) > 2)  ");
			//$totalRows=mysql_num_rows($resultPaid);
			return $resultPaidFifteenDays;

		}

		function getLastThirtyOneDaysPaidUser(){
			$database=new database();
		$resultPaidThirtyOneDays= $database->executeScalar("SELECT  count(distinct userId) FROM userpayments u WHERE u.startDate >=(now()-INTERVAL 31 DAY) AND u.planId<>1 AND u.userId<>0 AND u.userId NOT IN (SELECT p.userId from userpayments p GROUP BY p.userId having count(p.paymentId) > 2)  ");
		//$totalRows=mysql_num_rows($resultPaid);
		return $resultPaidThirtyOneDays;
			
		}

	}


?>
