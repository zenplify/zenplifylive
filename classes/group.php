<?php
	require_once('init.php');
	$groupIdForArchive = $_GET['groupId'];
	class group
	{

		function GetAllGroups($userId)
		{
			$database = new database();
			$groups = $database->executeObjectList("SELECT * FROM groups WHERE groupId not in(56,82,61,76,77,78,79,80,81) and (userId='" . $userId . "' OR userId='0')");
			return $groups;
		}

		function GetUserGroups($userId)
		{
			$database = new database();

			$registrationDate = $database->executeScalar("select registrationDate from `user` where userId = '" . $userId . "'");
			if ($registrationDate > registrationDate)
			{
				return $database->executeObjectList("SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON  groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1' AND groupusers.`userId`='" . $userId . "' ORDER BY groups.`orderBy` ");
			}
			else
			{
				return $database->executeObjectList("SELECT '" . $userId . "',userId AS guser,`name`,privacy,leaderId,isactive,color,description,groupId FROM groups WHERE (userId='" . $userId . "' OR userId=0) AND groupId NOT IN(56,82,61,76,77,78,79,80,81) AND isactive = 1
UNION
SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1'AND groupusers.`isArchive`='0' AND groupusers.`userId`='" . $userId . "'");
			}

//			$groups = $database->executeObjectList("SELECT * FROM groups where (userId='" . $userId . "' OR userId='0') and groupId not in(56,82,61,76,77,78,79,80,81)");
//			return $groups;
		}

		function GetGroupName($group_id)
		{
			$database = new database();
			$groupNmae = $database->executeObjectList("SELECT name FROM groups where groupId='" . $group_id . "' and groupId not in(56,82,61,76,77,78,79,80,81)");
			if (!empty($groupNmae))
			{
				return $groupNmae[0]->name;
			}
		}

		function GetGroupCategory()
		{
			$database = new database();
			$result = $database->executeObjectList("SELECT groupTypeId,type,canChangeColor FROM grouptype");

			return $result;
		}

		function addGroups1($group, $generatedVia)
		{
			$database = new database();
			$database->executeNonQuery("INSERT INTO groups
								  (userId, groupTypeId, name, description, color, orderBy,generatedVia)
								   VALUES
								   ('" . $group['userId'] . "',
								    '" . $group['groupCategory'] . "',
									'" . $group['groupTitle'] . "',
									'" . $group['groupDescription'] . "',
									'" . $group['color'] . "',
									'" . $group['groupOrder'] . "',
									'" . $generatedVia . "')");

			return ($database->getAffectedRows() != '' ? true : false);
		}

		function showGroupsCategory()
		{
			$database = new database();

			return $database->executeObjectList("SELECT groupTypeId,type,canChangeColor FROM grouptype ORDER BY groupOrder ASC ");
		}

		function showGroupsByTypeId($groupTypeId)
		{
			$database = new database();

			return $database->executeObjectList("SELECT * FROM groups where groupTypeId='" . $groupTypeId . "' ORDER BY orderBy ASC");
		}

		function showGroupById1($database, $groupId)
		{
			return $database->executeObjectList("SELECT * FROM groups WHERE groupId='" . $groupId . "'");
		}

		function groupCanChangeColor1($database, $groupTypeId)
		{
			return $database->executeScalar("SELECT canChangeColor FROM grouptype WHERE groupTypeId='" . $groupTypeId . "'");
		}

		function updateGroups1($database, $group, $groupId)
		{
			$database->executeNonQuery("UPDATE  groups SET
														groupTypeId='" . $group['groupCategory'] . "',
												   		name='" . $group['groupTitle'] . "',
												   		description='" . $group['groupDescription'] . "',
												   		color='" . $group['color'] . "',
												   		orderBy='" . $group['groupOrder'] . "'
												  WHERE groupId='" . $groupId . "'");

			return ($database->getAffectedRows() != '' ? true : false);
		}

		function GroupInfo($groupId)
		{
			$database = new database();

			return $database->executeObject("SELECT * FROM groups WHERE groupId='" . $groupId . "' and groupId not in(56,82,61,76,77,78,79,80,81)");
		}

		function CanChangeColor($groupTypeId)
		{
			$database = new database();

			return $database->executeScalar("SELECT canChangeColor FROM grouptype WHERE groupTypeId='" . $groupTypeId . "'");
		}

		public function addGroups($group, $generatedVia, $userId, $leaderId)
		{
			$database = new database();

			$database->executeNonQuery("INSERT INTO groups
								  (userId,name, description, color,privacy,parentId,leaderId,generatedVia,oldId)
								   VALUES 
								   ('" . $group['userId'] . "',
								    '" . $group['groupTitle'] . "',
									'" . $group['groupDescription'] . "',
									'" . $group['color'] . "',
									'0',
									'0',
									'" . $leaderId . "',
									'" . $generatedVia . "',
									0)");
			$groupId = $database->insertid();
			$database->executeNonQuery("INSERT INTO groupusers (groupId, userId ,isactive) Values('" . $groupId . "', '" . $userId . "','1') ");

			return ($database->getAffectedRows() != '' ? true : false);

		}

		public function editGroup($POST, $generatedVia, $userId, $privacy, $groupId, $leaderId)
		{
			$database = new database();


			$database->executeNonQuery("update `groupusers` set `isactive` = '0', `isArchive`='0' where `groupId` = '" . $groupId . "' AND userId='" . $userId . "'");

			$database->executeNonQuery("update `groups` set `isactive` = '0'  where `groupId` = '" . $groupId . "'");

			$oldGroup = $database->executeObject("select `parentId`,orderby from `groups` where `groupId` = '" . $groupId . "'");

			//  $parentId = $database->executeScalar("select `parentId` from `groups` where `groupId` = '".$groupId."'");
			$parentId = "";
			if (($oldGroup->parentId == 0) || (is_null($oldGroup->parentId)))
				$parentId = $groupId;
			else
				$parentId = $oldGroup->parentId;

			$database->executeNonQuery("INSERT INTO groups (userId,name, description, color,privacy,oldId,leaderId,generatedVia,parentId,orderBy)
								   VALUES
								   ('" . $userId . "',
								    '" . $POST['groupTitle'] . "',
									'" . $POST['groupDescription'] . "',
									'" . $POST['colors'] . "',
									'" . $privacy . "',
									'" . $groupId . "',
									'" . $leaderId . "',
									'" . $generatedVia . "',
									'" . $parentId . "',
									'" . $oldGroup->orderby . "')");
			$groupId = $database->insertid();
			$database->executeNonQuery("INSERT INTO groupusers (groupId, userId ,isactive) Values('" . $groupId . "', '" . $userId . "','1') ");

			return ($database->getAffectedRows() != '' ? true : false);
		}

		public function editGroupFroUser($groupId, $userId, $leaderId)
		{
			$database = new database();
			$statusCheck = $database->executeScalar("SELECT groupId FROM `groupusers` WHERE `groupId` = '" . $groupId . "' AND userId = '" . $leaderId . "' AND isactive = 1");
			if (empty($statusCheck))
			{
				$groupIds = $database->executeScalar("SELECT GROUP_CONCAT(groupId) FROM groups WHERE oldId = '" . $groupId . "' OR parentId = '" . $groupId . "' GROUP BY userId");
				$newGroupId = $database->executeScalar("SELECT groupId FROM groupusers WHERE userId = '" . $leaderId . "' AND groupId IN($groupIds) AND isactive = 1");

				if (empty($newGroupId)) {

					$groupParentId = $database->executeScalar("Select parentId from groups where groupId = '" . $groupIds . "'");

					$newGroupId = $database->executeScalar("Select groupId from groups where parentId = '" . $groupParentId . "' and isactive = 1");
				}
				
				$database->executeNonQuery("INSERT INTO groupusers (groupId, userId ,isactive) values ('" . $newGroupId . "', '" . $userId . "','1')");
				$database->executeNonQuery("update `groupusers` set `isactive` = 0 where `groupId` = '" . $groupId . "' AND userId='" . $userId . "'");
			}
			else
			{
				$database->executeNonQuery("INSERT INTO groupusers (groupId, userId ,isactive) SELECT groupId, '" . $userId . "','1' FROM `groups` WHERE oldId='" . $groupId . "' ");
				$database->executeNonQuery("update `groupusers` set `isactive` = 0 where `groupId` = '" . $groupId . "' AND userId='" . $userId . "'");
				$database->executeNonQuery("update `groups` set `isactive` = '0'  where `groupId` = '" . $groupId . "'");

			}


			return ($database->getAffectedRows() != '' ? true : false);
		}

		/*public function showGroupsCategory($database){
			return $database->executeObjectList("SELECT groupTypeId,type,canChangeColor FROM grouptype ORDER BY groupOrder ASC ");

			}*/
		public function showGroups($userId)
		{
			$database = new database();
			$registrationDate = $database->executeScalar("select registrationDate from `user` where userId = '" . $userId . "'");
			if ($registrationDate > registrationDate)
			{
				return $database->executeObjectList("SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON  groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1' AND groupusers.`userId`='" . $userId . "' ORDER BY groups.`orderBy` ");
			}
			else
			{
				return $database->executeObjectList("SELECT '" . $userId . "',userId AS guser,`name`,privacy,leaderId,isactive,color,description,groupId FROM groups WHERE (userId='" . $userId . "' OR userId=0) AND groupId NOT IN(56,82,61,76,77,78,79,80,81) AND isactive = 1
UNION
SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1'AND groupusers.`isArchive`='0' AND groupusers.`userId`='" . $userId . "'");
//				return $database->executeObjectList("SELECT * FROM groups where (userId='" . $userId . "' OR userId=0) and groupId not in(56,82,61,76,77,78,79,80,81) ORDER BY orderBy ASC");
			}


			//return $database->executeObjectList("select * from groups where userId = '".$userId."' and isactive = 1 order by orderBy ");
			//return $database->executeObjectList("SELECT * FROM groups where (userId='".$userId."' OR userId=0) and groupId not in(56,82,61,76,77,78,79,80,81) ORDER BY orderBy ASC");
			//return $database->executeObjectList("SELECT * FROM groups where userId='".$userId."' OR userId=0 ORDER BY orderBy ASC");
		}

		public function showGroupById($groupId)
		{
			$database = new database();

			return $database->executeObject("SELECT * FROM groups WHERE groupId='" . $groupId . "' and groupId not in(56,82,61,76,77,78,79,80,81)");
		}

		public function groupCanChangeColor($database, $groupTypeId)
		{
			return $database->executeScalar("SELECT canChangeColor FROM grouptype WHERE groupTypeId='" . $groupTypeId . "'");
		}

		public function updateGroups($group, $groupId)
		{
			$database = new database();
			$database->executeNonQuery("UPDATE  groups SET
														groupTypeId='" . $group['groupCategory'] . "',
												   		name='" . $group['groupTitle'] . "',
												   		description='" . $group['groupDescription'] . "',
												   		color='" . $group['colors'] . "',
												   		orderBy='" . $group['groupOrder'] . "'
												  WHERE groupId='" . $groupId . "'");

			return ($database->getAffectedRows() != '' ? true : false);
// mansoo

// mansoor
		}

		function showContactGroups($contactId)
		{
			$database = new database();

			return $database->executeObjectList("SELECT * FROM groups  WHERE groupId IN(SELECT groupId FROM groupcontacts WHERE contactId='" . $contactId . "') and groupId not in(56,82,61,76,77,78,79,80,81)");
		}

		function isGroupLinked($groupId, $userId)
		{
			$database = new database();
			$result = $database->executeObjectList("SELECT planId,title FROM plan WHERE groupId='" . $groupId . "' AND userId='" . $userId . "' AND isactive = 1");

			return $result;
		}

		function countGroupContacts($groupId, $userId)
		{
			$database = new database();

			return $database->executeScalar("SELECT count(*) as number FROM contact c LEFT JOIN groupcontacts gc on c.contactId=gc.contactId WHERE groupId='" . $groupId . "' AND userId='" . $userId . "' AND c.isActive=1");
		}

		function countPlanContacts($planId)
		{
			$database = new database();

			return $database->executeScalar("SELECT COUNT(*) FROM `plansteps` ps JOIN  `planstepusers` psu on ps.stepId=psu.stepId JOIN contact c on psu.contactId=c.contactId  WHERE ps.planId='" . $planId . "' and c.isActive=1");
		}

		function archiveGroups($groupId, $userId)
		{
			$database = new database();

			$archiveGroup = $database->executeNonQuery("update `groupusers` set `isactive` = 0,`isArchive`='1' where `groupId` = '" . $groupId . "' AND userId='" . $userId . "'");
			$database->executeNonQuery("update `groups` set `isactive` = '0'  where `groupId` = '" . $groupId . "'");

			return $archiveGroup;
		}

		function archiveGroupForUser($database, $groupId, $userId)
		{
			$orderby = $database->executeScalar("Select orderby from groups where groupId = '" . $groupId . "'");
			$database->executeNonQuery("update `groupusers` set `isactive` = '0', `isArchive`='1' where `groupId` = '" . $groupId . "' AND userId='" . $userId . "'");
			$database->executeNonQuery("update `groups` set `isactive` = '0'  where `groupId` = '" . $groupId . "'");

			return $orderby;
		}

		function publishGroup($database, $groupId, $userId, $leaderId, $generatedVia)
		{

			$groupStatus = $database->executeObject("select * from `groupusers` where groupId = '" . $groupId . "' and `userId` = '" . $leaderId . "'");
			if (($groupStatus->isactive == 0))
			{
				$groupId = $database->executeScalar("SELECT g.groupId FROM groups g JOIN groupusers gu ON g.groupId = gu.groupId WHERE g.parentId = '" . $groupId . "' AND g.userId = '" . $leaderId . "' AND g.generatedVia = 2 AND gu.userId= '" . $leaderId . "' AND gu.isactive = 1 AND gu.isArchive = 0");
			}
			$userIds = $database->executeObjectList("Select * from user where userId = '" . $leaderId . "'");

			$database->executeNonQuery("INSERT INTO groupusers (groupId, userId ,isactive) Values('" . $groupId . "', '" . $userId . "','1') ");

			return 'done';

		}

// mansoor

		function publishGroupForown($database, $groupId, $generatedVia)
		{


			$publishGroupForown = $database->executeNonQuery("Update `groups` set `privacy` = 1,`generatedVia`='" . $generatedVia . "' where  `groupId` = '" . $groupId . "'");

			return $publishGroupForown;
		}

		function getArchiveGroup($database, $leaderId)
		{
			$database = new database();
			$getArchiveGroup = $database->executeObjectList("SELECT * FROM `groups` WHERE isactive='0' AND isArchiveChange='0'  AND userId='" . $leaderId . "'");

			return $getArchiveGroup;
		}

		function unArchive($groupId)
		{
			$database = new database();

			$unArchive = $database->executeNonQuery("Update `groups` set `isArchiveChange` = 1,`isactive`=1 where `groupId` = '" . $groupId . "' ");

			return $unArchive;
		}

		function editRecord($name, $color, $description, $description, $groupId, $userId, $groupId, $userId)
		{

			$database = new database();
			$database->executeNonQuery("INSERT INTO groups (userId, groupTypeId ,name, description, color, orderBy, isactive, privacy, parentId, leaderId,oldId)
SELECT userId, groupTypeId, '$name', '$description', '$color', orderBy,0, privacy, parentId, '" . $userId . "','" . $groupId . "' FROM groups WHERE privacy=1 AND leaderId = '" . $userId . "'");

			return ($database->getAffectedRows() != '' ? true : false);
		}

		function getAllUserGroups($userId)
		{
			$database = new database();
			$registrationDate = $database->executeScalar("select registrationDate from `user` where userId = '" . $userId . "'");
			if ($registrationDate > registrationDate)
			{

				return $database->executeObjectList("SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON  groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1' AND groupusers.`userId`='" . $userId . "' ORDER BY groups.`orderBy` ");
			}
			else
			{

				return $database->executeObjectList("SELECT '" . $userId . "',userId AS guser,`name`,privacy,leaderId,isactive,color,description,groupId FROM groups WHERE (userId='" . $userId . "' OR userId=0) AND groupId NOT IN(56,82,61,76,77,78,79,80,81) AND isactive = 1
UNION
SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1'AND groupusers.`isArchive`='0' AND groupusers.`userId`='" . $userId . "'");
			}
//			return $database->executeObjectList("select g.groupId, g.name,g.color from groups g left join groupusers gu on g.groupId = gu.groupId where gu.userId = '" . $userId . "' and gu.isactive = 1");
		}

	}

?>