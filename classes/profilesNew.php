<?php

    include_once("class.smtpmail.php");

    class Profiles
    {
        public function PageView($database, $pageId, $url, $code, $userId, $browserInfo)
        {
            $ip = self::get_client_ip();
            $database->executeNonQuery("INSERT INTO pageviews(pageId, IpAddress, viewDate, userId, pageURL, uniqueCode, browserInfo) VALUES ('" . $pageId . "','" . $ip . "', NOW(), '" . $userId . "', '" . $url . "', '" . $code . "', '" . $browserInfo . "')");
            $viewId = $database->insertid();
            return $viewId;

        }

        function curPageURL()
        {
            $pageURL = 'http';
            if ($_SERVER["HTTPS"] == "on")
            {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80")
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            }
            else
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }

        public function get_client_ip()
        {
            $ipaddress = '';
            if ($_SERVER['HTTP_CLIENT_IP'])
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if ($_SERVER['HTTP_X_FORWARDED'])
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if ($_SERVER['HTTP_FORWARDED_FOR'])
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if ($_SERVER['HTTP_FORWARDED'])
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if ($_SERVER['REMOTE_ADDR'])
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';

            return $ipaddress;
        }

        public function getUserDetails($database, $uerId)
        {

            $details = $database->executeObject("Select email, firstName, consultantId, webAddress, facebookAddress from user where userId='" . $_SESSION['userId'] . "'");
            return $details;
        }

        public function getUserDetailsfromCode($database, $code)
        {

            $details = $database->executeObject("Select email, firstName, consultantId, webAddress, facebookAddress, pageId from user u join
		pages p on  u.userId=p.userId where uniqueCode='" . $code . "' ");
            return $details;
        }

        public function getAllProfiles($database, $userId)
        {

            $profiles = $database->executeObjectList("select p.*,pt.thumbnail from pages p 
		left join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId where userId='" . $userId . "' ORDER BY pt.order ASC");
            return $profiles;
        }

        public function getUserPageDetails($database, $code)
        {
            //OLD QUERY - JUST REMOVE p.parentPageId,
            /*"select u.userId, u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.email, u.facebookPersonalAddress, u.facebookPartyAddress, p.pageId, p.parentPageId, p.uniqueCode,p.thankYouMessage,p.pageName, pt.templateHTML from user u 
		join pages p on p.userId=u.userId 
		join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId where p.uniqueCode='".$code."'";
		*/
            $userDetails = $database->executeObject("select u.userId, u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.email, u.facebookPersonalAddress, u.facebookPartyAddress, p.pageId,  p.uniqueCode,p.thankYouMessage,p.pageName, pt.templateHTML from user u 
		join pages p on p.userId=u.userId 
		join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId where p.uniqueCode='" . $code . "'");
            return $userDetails;

        }

        public function getUserPageName($database, $code)
        {
            $pageName = $database->executeScalar("select pageName FROM pages where uniqueCode='" . $code . "'");
            return $pageName;
        }

        public function getPageQuestions($database, $pageId)
        {
            $questions = $database->executeObjectList("select * from questions q join pagequestions pq on q.questionId= pq.questionId where pageId='" . $pageId . "' ORDER BY q.order ASC");
            return $questions;
        }

        public function getPageQuestionAnswers($database, $pageId, $replyId)
        {
            $questions = $database->executeObjectList("SELECT distinct Q.questionId, Q.typeId,Q.question, RA.answer, RA.value, RA.answerText,QC.choiceText from pageformreplyanswers RA join questions Q on RA.questionId = Q.questionId
 left join pageformquestionchoices QC on RA.answer = QC.choiceId

where RA.replyId ='" . $replyId . "' order by Q.order ASC, QC.order ASC");
            return $questions;
        }

        public function getPageQuestionAnswersEdit($database, $pageId, $replyId)
        {
            $questions = $database->executeObjectList("SELECT distinct Q.questionId, Q.typeId,Q.question,QC.choiceId, QC.choiceText,RA.answer, RA.value, RA.answerText from  questions Q join pagequestions pq on Q.questionId= pq.questionId left join pageformquestionchoices QC  on Q.questionId=QC.questionId  left join pageformreplyanswers RA on RA.questionId = Q.questionId   where pq.pageId='" . $pageId . "' and replyId='" . $replyId . "'
 
 order by Q.order ASC, QC.order ASC");
            return $questions;
        }

        public function getQuestionChoices($database, $questionId)
        {
            $choices = $database->executeObjectList(" SELECT * from pageformquestionchoices  pfq where questionId='" . $questionId . "' ORDER BY pfq.order");
            return $choices;
        }

        public function generateContact($database, $pageId, $userId, $firstName, $lastName, $email, $phone, $viewId, $pageurl, $code)
        {

            //echo "Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='".$email."' and c.userId='".$userId."'";
            $contactId = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE (ec.email='" . $email . "' OR c.email='" . $email . "') and c.userId='" . $userId . "'");
            //echo die();
            if (empty($contactId))
            {

                $database->executeNonQuery("INSERT INTO contact
											SET
											userId='" . $userId . "',
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											addedDate=now(),
											isActive=1,
											isGenerated=1,
											email='" . $email . "',
											phoneNumber='" . $phone . "'");

                $contactId = $database->insertid();
                $database->executeNonQuery("INSERT INTO emailcontacts 
											SET 
											contactId='" . $contactId . "',
											email='" . $email . "',
											addedDate=CURDATE()");

                $emailId = $database->insertid();

                $database->executeNonQuery("INSERT INTO contactphonenumbers 
									SET contactId='" . $contactId . "',
										typeId=0,
										phoneNumber='" . $phone . "',
										isPrimary=0,
										addedDate=CURDATE()");


            }

            else
            {
                $database->executeNonQuery("UPDATE contact
											SET
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',isActive=1,email='" . $email . "',
											phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "'");


                $database->executeNonQuery("UPDATE contactphonenumbers SET  phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "'");


                $emailId = $database->executeScalar("SELECT emailId FROM emailcontacts WHERE contactId='" . $contactId . "'");


            }
            $database->executeNonQuery("INSERT INTO pageformreply
											SET
											pageId='" . $pageId . "',
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											email='" . $email . "',
											phoneNumber='" . $phone . "',
											isContactGenerated=1,
											contactId='" . $contactId . "',
											addDate=NOW(), userId='" . $userId . "', viewId='" . $viewId . "', pageURL='" . $pageurl . "', uniqueCode='" . $code . "'");

            $replyId = $database->insertid();
            $results = array("contactId" => $contactId, "replyId" => $replyId, "emailId" => $emailId);
            return $results;

        }

        public function addMailingAddress($database, $replyId, $contactId, $userId, $birthdate, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country)
        {
            $bestTime = implode(", ", $bestTimeToCall);
            $jsondata = file_get_contents('http://www.worldweatheronline.com/feed/tz.ashx?key=1f58bdbcdb144121131103&q=' . $zipCode . '&format=json');
            $obj = json_decode($jsondata, true);
            $offset = ($obj['data']['time_zone'][0]['utcOffset']);
            $birthdate = date('Y-m-d', strtotime($birthdate));
            $database->executeNonQuery("UPDATE pageformreply
											SET
											
											dateOfBirth='" . $birthdate . "',
											refferedBy='" . mysql_real_escape_string($referredBy) . "',
											bestTimeToCall='" . $bestTime . "',
											address='" . $street . "',
											city='" . $city . "',
											zipCode='" . $zipCode . "',
											state='" . $state . "',
											country='" . $country . "'
											WHERE replyId='" . $replyId . "'");

            $database->executeNonQuery("UPDATE contactaddresses
											SET
											 street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "'
											 WHERE contactId='" . $contactId . "'");
            $addressId = $database->getAffectedRows();
            if (!$addressId || empty($addressId))
            {
                $database->executeNonQuery("INSERT INTO contactaddresses SET 
											contactId='" . $contactId . "',
											street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "', addedDate=CURDATE()");
            }
            $database->executeNonQuery("UPDATE contact
											SET 
											dateOfBirth='" . $birthdate . "',
											referredBy='" . mysql_real_escape_string($referredBy) . "',
											street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "',
											utc='" . $offset . "' 
											WHERE contactId='" . $contactId . "'");

            $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
            if (in_array('Weekdays', $bestTimeToCall))
                $weekdays = 'Weekdays';
            if (in_array('Weekday Evenings', $bestTimeToCall))
                $weekdaysEvening = 'Weekday Evenings';
            if (in_array('Weekend Days', $bestTimeToCall))
                $weekends = 'Weekend Days';
            if (in_array('Weekend Evenings', $bestTimeToCall))
                $weekendsEvening = 'Weekend Evenings';

            $database->executeNonQuery("UPDATE contactcalltime SET 
			weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
            $rowId = $database->getAffectedRows();
            $database->executeNonQuery("UPDATE contact SET 
			weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
            $rowId = $database->getAffectedRows();

            if (!$rowId || empty($rowId))
            {
                $database->executeNonQuery("INSERT INTO contactcalltime SET 
					contactId='" . $contactId . "', weekdays='" . $weekdays . "', 
					weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', 
					weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE()");
            }
            return 1;

        }

        public function addToGroup($database, $contactId, $group)
        {
            $groupId = $database->executeScalar("select groupId from groups WHERE (LOWER(name)=LOWER('" . $group . "') OR groupId='" . $group . "') ");
            $contact = $database->executeObjectList("select contactId from groupcontacts where groupId='" . $groupId . "' and contactId='" . $contactId . "' ");
            if (empty($contact))
            {
                $database->executeNonQuery("INSERT INTO groupcontacts(groupId, contactId)  (select groupId, '" . $contactId . "' as contactId from groups WHERE (LOWER(name)=LOWER('" . $group . "') OR groupId='" . $group . "'))");
                //$groupId=$database->insertid();
            }
            return $groupId;
        }

        public function addToPlan($database, $contactId, $plan, $userId)
        {
            $date = date('Y-m-d');
            $date = strtotime($date);
            $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
            $plan = $database->executeObject("SELECT * from plan WHERE title='" . $plan . "' and userId='" . $userId . "'");
            $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
            if (!empty($planId2))
            {
                $database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $contactId . "'");
            }
            //print_r($plan);
            $palnStepsExists = $database->executeObjectList("SELECT *
FROM plan p
JOIN plansteps ps ON p.planId = ps.planId
JOIN planstepusers psu ON ps.stepId = psu.stepId
WHERE p.planId ='" . $plan->planId . "'
AND psu.contactId ='" . $contactId . "'");

            if ((empty($palnStepsExists)) && (empty($planId2)))
            {

                $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' order by order_no asc");

                $i = 0;
                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                foreach ($palnSteps as $ps)
                {


                    //echo 'not exists'.$ps->stepId;
                    //echo '<br />';
                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                    $newStepId = $database->insertid();


                    if ($ps->isTask == 0)
                    {

                        if ($ps->daysAfter == 0)
                        {

                            //echo "\r\nemail step";
                            //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                            $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                            if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                            {
                                //echo 'emial allowed';
                                $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);

                                foreach ($match[1] as $key)
                                {
                                    if ($key == "First Name")
                                    {
                                        $firstname = 'firstName';
                                    }
                                    if ($key == "Last Name")
                                    {
                                        $lastname = 'lastName';
                                    }
                                    if ($key == "Phone Number")
                                    {
                                        $phonenumber = 'phoneNumber';
                                    }
                                    if ($key == "Company Name")
                                    {
                                        $companyname = 'companyTitle';
                                    }
                                    if ($key == "@Signature")
                                    {
                                        $signature = self::getSignatures($database, $userId);
                                    }


                                }
                                if (!empty($firstname))
                                {
                                    $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($lastname))
                                {
                                    $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($companyname))
                                {
                                    $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($phonenumber))
                                {
                                    $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                }

                                $subject = $mailBody->subject;
                                $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                $message = str_replace("\'", "'", $rplbysig);

                                $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                    "Reply-To: " . $userInfo->email . "\r\n";
                                $headers .= "Content-Type: text/html";
                                //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;

                                $mail = new smtpEmail();
                                $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject);


                                //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");


                                $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
														('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");

                                $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                if ($totalSteps == $stepCounter)
                                {
                                    $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                                }
                            }
                        }
                        else
                        {
                            //echo 'breaking days after not zero';
                            //echo '<br>';
                            break;
                        }
                    }
                    else
                    {

                        //echo "<br /> task step<br />";
                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                        $taskId = $database->insertid();
                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                        //echo '<br />';
                        //echo 'breaking task occured';
                        break;
                    }


                    $stepCounter++;

                }

            }


            return $plan->planId;


        }

        public function getPageViews($database, $pageId)
        {

            $views = $database->executeScalar("Select count(*) as totalViews FROM  pageviews WHERE pageId='" . $pageId . "' and isCounterSet=0");
            return $views;
        }

        public function getPageGeneratedContacts($database, $pageId)
        {

            $totalContacts = $database->executeScalar("Select count(*) as totalContacts FROM pageformreply WHERE pageId='" . $pageId . "' and isContactGenerated=1  and isCounterSet=0");
            return $totalContacts;
        }

        public function editProfileLink($database, $pageId, $linkcode, $userId)

        {

            $uniqueCode = $database->executeScalar("select uniqueCode from pages where pageId='" . $pageId . "' ");


            $page = $database->executeScalar("select pageId from pages where lower(uniqueCode)='" . strtolower(trim($linkcode)) . "' and pageId<>'" . $pageId . "' ");
            if (empty($page))
            {
                $database->executeNonQuery("UPDATE pages set uniqueCode='" . trim($linkcode) . "' WHERE  pageId='" . $pageId . "' ");
                $database->executeNonQuery("INSERT INTO profilelinklog( pageId, userId, oldCode, newCode, editDateTime) VALUES ('" . $pageId . "','" . $userId . "','" . $uniqueCode . "','" . $linkcode . "',NOW())");
                return 'done';
            }
            else
            {
                return 'not done';
            }
        }

        public function deleteProfileLink($database, $pageId)
        {
            $page = $database->executeNonQuery("DELETE FROM pages  WHERE  pageId='" . $pageId . "'");
            return 'done';

        }

        public function resetCounter($database, $pageId)
        {
            $page = $database->executeNonQuery("UPDATE pageformreply set isCounterSet=1  WHERE  pageId='" . $pageId . "'");
            $page = $database->executeNonQuery("UPDATE pageviews set isCounterSet=1  WHERE  pageId='" . $pageId . "'");
            return 'done';

        }

        public function checkContactReplyAgainstProfile($database, $contactId, $profileName)
        {
            $results = $database->executeObject("SELECT pfr.pageId, pfr.replyId, parentPageId  from pages p join pageformreply pfr on p.pageId=pfr.pageId where pfr.contactId='" . $contactId . "' and p.pageName = '" . $profileName . "' ORDER BY replyId DESC LIMIT 1");
            $res = array("pageId" => $results->pageId, "replyId" => $results->replyId, "parentPageId" => $results->parentPageId);
            return $res;
        }

        public function getContactAnswerAgainstQuestion($database, $replyId, $questionId, $questionType)
        {
            if ($questionType == '1' || $questionType == '2')
            {
                $answers = $database->executeObject("SELECT answer , value, answerText from  pageformreplyanswers  where replyId='" . $replyId . "' AND questionId='" . $questionId . "'");
                return $answers;
            }
            else
            {

                $answers = $database->executeObjectList("SELECT  pc.choiceText , pfr.answer, value, pfr.answerText from pageformreplyanswers pfr join pageformquestionchoices pc on pfr.answer=pc.choiceId  where replyId='" . $replyId . "' AND pfr.questionId='" . $questionId . "'");
                if (empty($answers))
                    $answers = $database->executeObjectList("SELECT  answer, value, answerText from pageformreplyanswers  where replyId='" . $replyId . "' AND questionId='" . $questionId . "'");
                return $answers;
            }


        }

        public function getQuestionLabel($database, $questionId)
        {

            $lables = $database->executeObjectList("SELECT  label from questionradiolabels ql where ql.questionId='" . $questionId . "' ORDER BY ql.order");
            return $lables;

        }

        public function editContact($database, $userId, $contactId, $firstName, $lastName, $email, $phone)
        {

            //echo "Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='".$email."' and c.userId='".$userId."' and contactId<>'".$contactId."'";
            $contactId2 = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='" . $email . "' and c.userId='" . $userId . "' and c.contactId<>'" . $contactId . "'");
            if (empty($contactId2))
            {
                $database->executeNonQuery("UPDATE emailcontacts
											SET
											email='" . $email . "' WHERE contactId='" . $contactId . "'");
                $database->executeNonQuery("UPDATE contact SET email='" . $email . "', phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "'");
            }


            $database->executeNonQuery("UPDATE contact
											SET
											firstName='" . $firstName . "',
											lastName='" . $lastName . "' WHERE contactId='" . $contactId . "'");

            $phoneId = $database->executeScalar("SELECT phoneNumberId from contactphonenumbers where contactId='" . $contactId . "'");


            if (empty($phoneId))
            {
                $database->executeNonQuery("INSERT INTO contactphonenumbers SET contactId='" . $contactId . "', phoneNumber='" . $phone . "',  addedDate=CURDATE()");
            }
            else
            {
                $database->executeNonQuery("UPDATE contactphonenumbers SET  phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "' and phoneNumberId='" . $phoneId . "' ");
            }


            return $contactId;

        }

        public function editMailingAddress($database, $contactId, $userId, $title, $birthdate, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country, $company, $jobTitle)
        {
            $bestTime = implode(", ", $bestTimeToCall);
            if ($birthdate != '')
                $birthdate = date('Y-m-d', strtotime($birthdate));
            else
                $birthdate = '0000-00-00';

            $jsondata = file_get_contents('http://www.worldweatheronline.com/feed/tz.ashx?key=1f58bdbcdb144121131103&q=' . $zipCode . '&format=json');
            $obj = json_decode($jsondata, true);
            $offset = ($obj['data']['time_zone'][0]['utcOffset']);

            $database->executeNonQuery("UPDATE contactaddresses
											SET
											
											 street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "',utc='" . $offset . "'
											 WHERE contactId='" . $contactId . "'");
            $addressId = $database->getAffectedRows();
            if (!$addressId || empty($addressId))
            {
                $database->executeNonQuery("INSERT INTO contactaddresses SET 
											contactId='" . $contactId . "',
											street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "', utc='" . $offset . "' ,addedDate=CURDATE()");
            }

            $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
            if (in_array('Weekdays', $bestTimeToCall))
                $weekdays = 'Weekdays';
            if (in_array('Weekday Evenings', $bestTimeToCall))
                $weekdaysEvening = 'Weekday Evenings';
            if (in_array('Weekend Days', $bestTimeToCall))
                $weekends = 'Weekend Days';
            if (in_array('Weekend Evenings', $bestTimeToCall))
                $weekendsEvening = 'Weekend Evenings';


            $database->executeNonQuery("UPDATE contact
											SET 
											 title='" . $title . "',
											companyTitle='" . $company . "',
											jobTitle='" . $jobTitle . "',
											 dateOfBirth='" . $birthdate . "',
											referredBy='" . $referredBy . "', street='" . $street . "',
											zipcode='" . $zipCode . "',
											state='" . $state . "',
											city='" . $city . "',
											country='" . $country . "',utc='" . $offset . "', weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "' WHERE contactId='" . $contactId . "'");


            $database->executeNonQuery("UPDATE contactcalltime SET 
			weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
            $rowId = $database->getAffectedRows();
            if (!$rowId || empty($rowId))
            {
                $database->executeNonQuery("INSERT INTO contactcalltime SET 
					contactId='" . $contactId . "', weekdays='" . $weekdays . "', 
					weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', 
					weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE()");
            }
            return 1;

        }

        function contactNotes($contactId)
        {
            $database = new database();
            return $database->executeObject("SELECT noteId, contactId, notes, addedDate, lastModifiedDate, isActive FROM contactnotes WHERE contactId='" . $contactId . "' AND isActive=1 ORDER BY noteId");
        }


        /*function assignPlanStep($userId,$contactId,$planSteps)
	{
		$database = new database();
		//today date			
		$date =date('Y-m-d');
		$date = strtotime($date);

		$database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId='".$contactId."'");
		$database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId='".$contactId."'");
		
		$count=sizeof($planSteps['plan']); 
		if($count!=0){
			$step=0;
			
			while($count>0){
				
	
				$database->executeNonQuery("INSERT INTO planstepusers SET stepId='".$planSteps['planStepDropDown'][$step]."',contactId='".$contactId."'");
				
				$stepDesc=$database->executeObject("SELECT * FROM plansteps WHERE stepId='".$planSteps['planStepDropDown'][$step]."'");
				$dueDate = strtotime("+".$stepDesc->daysAfter." day", $date);//add days in date
				$database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId	."','".$stepDesc->summary."','0','','','".date('Y-m-d',$dueDate)."','0','".$date."','0','0','".$stepDesc->planId."','','0','1')");
				$taskId=$database->insertid();	
				$database->executeNonQuery("INSERT INTO taskcontacts SET contactId='".$contactId."',taskId='".$taskId."'");
				$count--;
				$step++;
				}

					}
		
		}*/
        function assignPlanStep($userId, $contactId, $planSteps)
        {
            $database = new database();
            //today date			
            $date = date('Y-m-d');
            $date = strtotime($date);

            $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $contactId . "'");
            $x = 0;
            $contactSteps = array();
            $newContactSteps = array();
            foreach ($existingSteps as $exists)
            {
                $contactSteps[$x] = $exists->stepId;
                $x++;
            }
            //print_r($contactSteps);
            $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
            //echo 'TotalPlans';
            $count = sizeof($planSteps['plan']);
            //echo '<br />';

            if ($count != 0)
            {
                //echo 'Count: '.$count;
                $step = 0;
                $j = 0;
                $y = 0;

                foreach ($planSteps['plan'] as $planss)
                {


                    $i = 0;
                    /*echo 'Count: '.$count;
					echo '<br />';*/
                    $plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $planss . "'");
                    /*if($planSteps['planStepDropDown'.$planss]!='' &&  $planSteps['planStepDropDown'.$planss]!='Completed')
					{*/
                    if ($planSteps['planStepDropDown' . $planss] != 'Completed')
                    {
                        /*echo $planss;
							echo "<br />";*/


                        $firstStepOfPlan = $database->executeScalar("SELECT ps.stepId
						FROM plansteps ps
						WHERE  ps.planId='" . $planss . "' and ps.order_no=(select min(order_no) from plansteps where planId='" . $planss . "')");
                        if ($planSteps['planStepDropDown' . $planss] == '')
                        {
                            $firstStepOfPlan;
                        }
                        else
                        {
                            $firstStepOfPlan = $planSteps['planStepDropDown' . $planss];
                        }

                        $palnStepsToSkip = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no<(select order_no from plansteps where stepId='" . $firstStepOfPlan . "') AND planId='" . $planss . "' order by order_no asc");
                        foreach ($palnStepsToSkip as $pss)
                        {
                            /*echo "SELECT ceh.stepId cehId , cts.stepId ctsId, t.stepId tId  from plansteps ps left  join contacttaskskipped cts on ps.planId=cts.planId and ps.stepId=cts.stepId and cts.contactId='".$contactId."'
left join contactemailhistory ceh on ps.stepId=ceh.stepId and ps.planId=ceh.planId and ceh.contactId='".$contactId."'
left join (SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
inner join tasks t on tc.taskId = t.taskId
inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
where tc.contactId = '".$contactId."' and t.isCompleted=1
) as t on ps.stepId = t.stepId and ps.planId = t.planId
where ps. planId='".$planss."' and ps.stepId='".$pss->stepId."'
";*/
                            $stepSkipId = $database->executeObject("SELECT ceh.stepId cehId , cts.stepId ctsId, t.stepId tId  from plansteps ps left  join contacttaskskipped cts on ps.planId=cts.planId and ps.stepId=cts.stepId and cts.contactId='" . $contactId . "'
left join contactemailhistory ceh on ps.stepId=ceh.stepId and ps.planId=ceh.planId and ceh.contactId='" . $contactId . "'
left join (SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
inner join tasks t on tc.taskId = t.taskId
inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
where tc.contactId = '" . $contactId . "' and t.isCompleted=1
) as t on ps.stepId = t.stepId and ps.planId = t.planId
where ps. planId='" . $planss . "' and ps.stepId='" . $pss->stepId . "'
");
                            if ($stepSkipId->cehId == NULL && $stepSkipId->ctsId == NULL && $stepSkipId->tId == NULL)
                            {
                                //echo"INSERT INTO `contacttaskskipped`( `stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES ('".$pss->stepId."','".$pss->planId."','".$contactId."',NOW(),'".mysql_real_escape_string($pss->summary)."')";
                                $database->executeNonQuery("INSERT INTO `contacttaskskipped`( `stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES ('" . $pss->stepId . "','" . $pss->planId . "','" . $contactId . "',NOW(),'" . mysql_real_escape_string($pss->summary) . "')");


                            }


                        }


                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='" . $firstStepOfPlan . "') AND planId='" . $planss . "' order by order_no asc");
                        //print_r($palnSteps);
                        /*echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$planSteps['planStepDropDown'.$planss]."') AND planId='".$planss."' order by stepId asc";
							echo "<br />";*/

                        $x = 0;
                        $SkippedSteps = array();
                        foreach ($palnSteps as $ps)
                        {
                            $SkippedSteps[$x] = $ps->stepId;
                            $x++;

                        }
                        $skipSteps = implode(",", $SkippedSteps);
                        //echo "delete from contacttaskskipped where planId='".$planss."' and stepId IN(".$skipSteps.") and contactId='".$contactId."'";
                        $database->executeNonQuery("delete from contacttaskskipped where planId='" . $planss . "' and stepId IN(" . $skipSteps . ") and contactId='" . $contactId . "'");


                        $totalSteps = sizeof($palnSteps);
                        $stepCounter = 1;
                        foreach ($palnSteps as $ps)
                        {
                            //echo "next plan step";
                            $newContactSteps[$y] = $ps->stepId;
                            $y++;
                            if (!in_array($ps->stepId, $contactSteps))
                            {

                                $planId2 = self::CheckLastStepOfPlan($planss, $contactId);
                                if (!empty($planId2))
                                {
                                    $database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $contactId . "'");
                                }

                                /*echo 'not exists'.$ps->stepId;
										echo '<br />';*/
                                $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                                $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                                $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                                $newStepId = $database->insertid();
                                //echo 'inserted';

                                if ($ps->isTask == 0)
                                {

                                    if ($ps->daysAfter == 0)
                                    {

                                        /*echo "\r\nemail step";
												echo '<br>';*/
                                        //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                        $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                                        if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                        {
                                            /*echo 'emial allowed';
														echo '<br>';*/
                                            $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                            $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                            preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);

                                            foreach ($match[1] as $key)
                                            {
                                                if ($key == "First Name")
                                                {
                                                    $firstname = 'firstName';
                                                }
                                                if ($key == "Last Name")
                                                {
                                                    $lastname = 'lastName';
                                                }
                                                if ($key == "Phone Number")
                                                {
                                                    $phonenumber = 'phoneNumber';
                                                }
                                                if ($key == "Company Name")
                                                {
                                                    $companyname = 'companyTitle';
                                                }
                                                if ($key == "@Signature")
                                                {
                                                    $signature = self::getSignatures($database, $userId);
                                                }


                                            }
                                            if (!empty($firstname))
                                            {
                                                $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                            }
                                            if (!empty($lastname))
                                            {
                                                $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                            }
                                            if (!empty($companyname))
                                            {
                                                $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                            }
                                            if (!empty($phonenumber))
                                            {
                                                $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                            }

                                            $subject = $mailBody->subject;
                                            $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                            $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                            $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                            $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                            $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                            $message = str_replace("\'", "'", $rplbysig);

                                            $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                                "Reply-To: " . $userInfo->email . "\r\n";
                                            $headers .= "Content-Type: text/html";
                                            //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;


                                            $mail = new smtpEmail();
                                            $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject);


                                            //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                            echo "INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												//('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())";


                                            $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                            $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                            if ($totalSteps == $stepCounter)
                                            {
                                                $alreadyCompleted = $database->executeScalar("SELECT count(*) as exists from contactPlansCompleted where stepId='" . $stepDesc->stepId . "' and planId='" . $planss . "' and contactId='" . $contactId . "'");
                                                if ($alreadyCompleted == 0)
                                                {

                                                    $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $planss . "','" . $contactId . "',CURDATE())");
                                                    $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "' and planId='" . $planss . "'");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        /*echo 'breaking days after not zero';
												echo '<br>';*/
                                        break;
                                    }
                                }
                                else
                                {

                                    //echo "<br /> task step<br />";
                                    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                    $taskId = $database->insertid();
                                    $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                                    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                                    /*echo '<br />';
											echo 'breaking task occured';*/
                                    break;
                                }


                            }
                            else
                            {

                                /*echo '<br />';
									echo 'no change';*/
                                break;
                            }

                            $stepCounter++;

                        }
                        $count--;

                        $step++;
                    }
                    else
                    {
                        /*echo "PlanId".$planss;
						echo "<br />";
						echo "SELECT ps.stepId, ps.planId,order_no
						FROM plansteps ps
						WHERE  ps.planId='".$planss."' and ps.order_no=(select max(order_no) from plansteps where planId='".$planss."')";
							echo "<br />";*/
                        $lastStepOfPlan = $database->executeObject("SELECT ps.stepId, ps.planId,order_no
						FROM plansteps ps
						WHERE  ps.planId='" . $planss . "' and ps.order_no=(select max(order_no) from plansteps where planId='" . $planss . "')");
                        /*print_r($lastStepOfPlan);
							echo "<br />";
							echo "INSERT INTO contactPlansCompleted(stepId ,planId, contactId, dateCompleted) VALUES ('".$lastStepOfPlan->stepId."','".$lastStepOfPlan->planId."','".$contactId."',CURDATE())";
						
*/ //echo "SELECT count(*) as `exists` from contactPlansCompleted where stepId='".$lastStepOfPlan->stepId."' and planId='".$lastStepOfPlan->planId."' and contactId='".$contactId."'";
                        $alreadyCompleted = $database->executeScalar("SELECT count(*) as `exists` from contactPlansCompleted where stepId='" . $lastStepOfPlan->stepId . "' and planId='" . $lastStepOfPlan->planId . "' and contactId='" . $contactId . "'");
                        if ($alreadyCompleted == 0)
                        {
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId ,planId, contactId, dateCompleted) VALUES ('" . $lastStepOfPlan->stepId . "','" . $lastStepOfPlan->planId . "','" . $contactId . "',CURDATE())");

                        }
                        $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "' and planId='" . $planss . "'");
                        /*echo "<br>";
						echo "Added". $lastStepOfPlan->planId;
						echo "<br />";*/
                    }
                }

            }
            else
            {

                //echo "DELETE from taskcontacts where taskId  IN (SELECT tc.taskId FROM tasks t join taskcontacts tc on t.taskId=tc.taskId where t.userId='".$userId."' and isCompleted<>1 and tc.contactId='".$contactId."') and contactId='".$contactId."'";
                $taskId2 = $database->executeObjectList("SELECT tc.taskId FROM tasks t join taskcontacts tc on t.taskId=tc.taskId where t.userId='" . $userId . "' and isCompleted<>1 and tc.contactId='" . $contactId . "' and planId<>0");
                foreach ($taskId2 as $t2)
                {
                    //echo "DELETE from taskcontacts where taskId='".$t2->taskId."' and contactId='".$contactId."'";
                    $database->executeNonQuery("DELETE from taskcontacts where taskId='" . $t2->taskId . "' and contactId='" . $contactId . "'");
                    //echo "DELETE from tasks where taskId='".$t2->taskId."'";
                    $database->executeNonQuery("DELETE from tasks where taskId='" . $t2->taskId . "'");
                }
                //echo "DELETE FROM contactPlansCompleted where  contactId='".$contactId."'";
                $database->executeNonQuery("DELETE FROM contactPlansCompleted where  contactId='" . $contactId . "'");
                $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "'");
            }
            /*print_r($contactSteps);
		echo "New Steps";
		print_r($newContactSteps);*/

            $delSteps = array_diff($contactSteps, $newContactSteps);
            /*echo "<br>";
		echo "Diiference";
		print_r($steps);
		echo "<br>";*/
            //print_r($delSteps);
            //echo "SELECT  planId FROM  contactPlansCompleted WHERE contactId='".$contactId."' and planId NOT IN (".$plansContact.")";
            /*$plansContact=implode(',',$planSteps['plan']);
		if($plansContact!='')
		{
			$planInfo=$database->executeScalar("SELECT  planId FROM  contactPlansCompleted WHERE contactId='".$contactId."' and planId NOT IN (".$plansContact.")");
			$database->executeNonQuery("DELETE FROM contactPlansCompleted where planId='".$planInfo."' and contactId='".$contactId."'");
		}
		else
		{
			$database->executeNonQuery("DELETE FROM contactPlansCompleted where contactId='".$contactId."'");
		}*/
            $steps = array();
            $j = 0;
            foreach ($delSteps as $key => $val)
            {
                $steps[$j] = $val;
                $j++;
            }

            //print_r($steps);
            $newSteps = implode(',', $newContactSteps);

            if ($newSteps != '')
            {
                //echo "DELETE FROM  planstepusers WHERE contactId='".$contactId."' and stepId NOT IN (".$newSteps.")";
                $database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId='" . $contactId . "' and stepId NOT IN (" . $newSteps . ")");


                /*echo "DELETE FROM  planstepusers WHERE contactId='".$contactId."' and stepId='".$steps[$i]."'";
					echo "<br>";*/

                /*echo "SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId NOT IN (".$newSteps.") and contactId='".$contactId."'";
					echo "<br>";*/

                $taskInfo = $database->executeObjectList("SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId NOT IN (" . $newSteps . ") and contactId='" . $contactId . "'");

                foreach ($taskInfo as $ts)
                {
                    /*echo "DELETE FROM tasks WHERE taskId='".$ts->taskId."' and isCompleted<>1";
						echo "<br>";*/
                    $database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $ts->taskId . "' and isCompleted<>1");
                    $isDeleted = $database->getAffectedRows();
                    /*echo "task deleted : ".$database->getAffectedRows();
						echo '<br>';*/
                    if ($database->getAffectedRows() > 0)
                    {
                        //echo "DELETE FROM taskcontacts WHERE contactId='".$contactId."' and taskId='".$ts->taskId."'";
                        $database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $ts->taskId . "'");
                    }
                    //echo "DELETE FROM contactPlansCompleted where planId='".$taskInfo->planId."' and contactId='".$contactId."'";
                }


            }
            else
            {
                //echo "DELETE FROM  planstepusers WHERE contactId='".$contactId."'";
                $database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId='" . $contactId . "'");

            }
            if (!empty($contactSteps))
            {
                for ($i = 0; $i < sizeof($steps); $i++)
                {
                    if (!in_array($steps[$i], $newContactSteps))
                    {
                        /*echo "DELETE FROM  planstepusers WHERE contactId='".$contactId."' and stepId='".$steps[$i]."'";
					echo "<br>";*/

                        /*echo "SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId='".$steps[$i]."' and contactId='".$contactId."'";
					echo "<br>";*/

                        $taskInfo = $database->executeObject("SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId='" . $steps[$i] . "' and contactId='" . $contactId . "'");

                        /*echo "DELETE FROM tasks WHERE taskId='".$taskInfo->taskId."' and isCompleted<>1";
					echo "<br>";*/
                        $database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $taskInfo->taskId . "' and isCompleted<>1");
                        $isDeleted = $database->getAffectedRows();
                        /*echo "task deleted : ".$database->getAffectedRows();
					echo '<br>';*/
                        if ($database->getAffectedRows() > 0)
                        {
                            //echo "DELETE FROM taskcontacts WHERE contactId='".$contactId."' and taskId='".$taskInfo->taskId."'";
                            $database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $taskInfo->taskId . "'");
                        }
                        //echo "DELETE FROM contactPlansCompleted where planId='".$taskInfo->planId."' and contactId='".$contactId."'";
                        $database->executeNonQuery("DELETE FROM contactPlansCompleted where planId='" . $taskInfo->planId . "' and contactId='" . $contactId . "'");
                        $database->executeNonQuery("delete from contacttaskskipped where planId='" . $taskInfo->planId . "' and contactId='" . $contactId . "'");

                    }
                }

            }
            if (!empty($planSteps['plan']))
            {

                $NewPlans = implode(",", $planSteps['plan']);
                //echo "DELETE FROM contactPlansCompleted where planId NOT IN (".$NewPlans.") and contactId='".$contactId."'";
                $database->executeNonQuery("DELETE FROM contactPlansCompleted where planId NOT IN (" . $NewPlans . ") and contactId='" . $contactId . "'");
                $database->executeNonQuery("delete from contacttaskskipped where planId NOT IN (" . $NewPlans . ") and contactId='" . $contactId . "'");

            }


        }

        public function getUserSignature($database, $userId)
        {
            $userDetails = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress , u.consultantId, u.title, u.leaderId,u.userId from user u where u.userId='" . $userId . "'");

            return $userDetails;

        }


        public function EditContactPlanByGroup($database, $contactId, $groupId, $userId)
        {

            //echo "Edit Contact Plan By Group";
            $date = date('Y-m-d');
            $date = strtotime($date);
            $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
            /*echo "SELECT * from plan WHERE groupId='".$groupId."' and userId='".$userId."'";
		echo "<br>";*/
            $plan = $database->executeObject("SELECT * from plan WHERE groupId='" . $groupId . "' and (userId='" . $userId . "' OR userId='" . $userInfo->leaderId . "')");
            /*echo $plan->planId;
		echo "<br>";*/
            $planType = $database->executeScalar("select isDoubleOptIn from plan where planId='" . $plan->planId . "'");

            $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $contactId . "'");
            $x = 0;
            $contactSteps = array();

            foreach ($existingSteps as $exists)
            {
                $contactSteps[$x] = $exists->stepId;
                $x++;
            }
            //echo 'All Steps';
            //print_r($contactSteps);

            if ($plan->planId != '' || !empty($plan->planId))
            {
                //echo "plaN FOUND";
                $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
                /*echo "planCompleted:".$planId2;
			echo "<br>";*/
                /*if(!empty($planId2))
			{
				$database->executeNonQuery("delete from contactPlansCompleted where planId='".$planId2."' and contactId='".$contactId."'");
			}*/

                $palnStepsExists = $database->executeObjectList("SELECT *
FROM plan p
JOIN plansteps ps ON p.planId = ps.planId
JOIN planstepusers psu ON ps.stepId = psu.stepId
WHERE p.planId ='" . $plan->planId . "'
AND psu.contactId ='" . $contactId . "'");

                if ((empty($palnStepsExists)) && (empty($planId2)))
                {
                    $i = 0;
                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' order by order_no asc");
                    $stepno = 0;
                    $allsteps = array();
                    foreach ($palnSteps as $ps)
                    {

                        $allsteps[$stepno] = $ps->stepId;
                        $stepno++;
                    }
                    //echo 'All Steps';
                    //print_r($allsteps);

                    //echo '<br />';
                    //$alreadyExists=array_intersect($allsteps,$usersteps);
                    //print_r($alreadyExists);


                    $totalSteps = sizeof($palnSteps);
                    $stepCounter = 1;
                    foreach ($palnSteps as $ps)
                    {

                        if (!in_array($ps->stepId, $contactSteps))
                        {

                            //echo 'not exists'.$ps->stepId;
                            //echo '<br />';
                            $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                            $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                            $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                            $newStepId = $database->insertid();


                            if ($ps->isTask == 0)
                            {

                                if ($ps->daysAfter == 0)
                                {

                                    //echo "\r\nemail step";
                                    //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                    $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                                    if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                    {
                                        //echo 'emial allowed';
                                        $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                        $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                        preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);

                                        foreach ($match[1] as $key)
                                        {
                                            if ($key == "First Name")
                                            {
                                                $firstname = 'firstName';
                                            }
                                            if ($key == "Last Name")
                                            {
                                                $lastname = 'lastName';
                                            }
                                            if ($key == "Phone Number")
                                            {
                                                $phonenumber = 'phoneNumber';
                                            }
                                            if ($key == "Company Name")
                                            {
                                                $companyname = 'companyTitle';
                                            }
                                            if ($key == "@Signature")
                                            {
                                                $signature = self::getSignatures($database, $userId);
                                            }


                                        }
                                        if (!empty($firstname))
                                        {
                                            $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                        }
                                        if (!empty($lastname))
                                        {
                                            $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                        }
                                        if (!empty($companyname))
                                        {
                                            $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                        }
                                        if (!empty($phonenumber))
                                        {
                                            $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                        }

                                        $subject = $mailBody->subject;
                                        $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                        $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                        $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                        $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                        $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                        $message = str_replace("\'", "'", $rplbysig);

                                        $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                            "Reply-To: " . $userInfo->email . "\r\n";
                                        $headers .= "Content-Type: text/html";
                                        //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;

                                        $mail = new smtpEmail();
                                        $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject);


                                        //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");


                                        $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                        $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                        if ($totalSteps == $stepCounter)
                                        {
                                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                                        }
                                    }
                                }
                                else
                                {
                                    //echo 'breaking days after not zero';
                                    //echo '<br>';
                                    break;
                                }
                            }
                            else
                            {

                                //echo "<br /> task step<br />";
                                $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                $taskId = $database->insertid();
                                $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                                $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                                //echo '<br />';
                                //echo 'breaking task occured';
                                break;
                            }


                        }
                        else
                        {
                            //echo 'Plan exists, breaking';
                            break;
                        }


                        $stepCounter++;
                    }


                    return $plan->planId;

                }

            }

        }

        function getPageTitle($database, $pageId)
        {

            return $database->executeScalar("SELECT pageBrowserTitle FROM pages where pageId='" . $pageId . "'");
        }

        function sendPermissionEmail($database, $userId, $contactId)
        {
            $uid = base62_encode($userId);
            //$uid=$userId;
            $user = $database->executeObject("SELECT firstName,lastName,leaderId,email FROM user WHERE userId='" . $userId
                . "'");
//
            if ($user->leaderId == '2506')
            {
                $permissiionText = 'Click the link below to receive emails and a monthly newsletter, if you requested it on your Guest profile.';
            }
            else
            {
                $permissiionText = 'I\'ll be sending a little info each day for the first week and a monthly newsletter if you requested it on your Guest profile.';
            }
//		$contactemail=$database->executeObject("SELECT c.email, emailId, firstName FROM  contact c join emailcontacts ec on c.contactId=ec.contactId  WHERE c.contactId='".$contactId."'");
            $emailId = $contactemail->emailId . '';

            $to = $contactemail->email;
            $from = $user->email;
            $subject = "Newsletter Permission from " . $user->firstName . " " . $user->lastName;
            //$emailbody= strip_tags($_POST["emailbody"]);
            $body = "<html><body><p>Hi " . $contactemail->firstName . "<br /><br />Thank you for submitting a Guest Profile. Your information is safe and will not be shared!
<br /><br />" . $permissiionText . " <br /><a href='http://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "'>http://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "</a>
		<br /><br /> Best Regards, <br />" . $user->firstName . " " . $user->lastName . "
		<br /><br />To Decline Emails from " . $user->firstName . " " . $user->lastName . " click the link below: <br
		 />" .
                "<a href='http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "'>http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "</a></p></html></body>";


            $headers = "From:" . $user->firstName . " " . $user->lastName . " <" . $user->email . ">\r\n" .
                "Reply-To: " . $user->email . "\r\n";
            //$headers .= "Content-Type: text/html";

            $mail = new smtpEmail();
            $mail->sendMail($database, $user->email, $to, $user->firstName, $user->lastName, $body, $subject, '1');


            //$status=mail($to,$subject,$body,$headers,"-f ".$user->email."");

            if ($status == 1)
            {
                $flag = 0;
                $requestDate = date("Y-m-d");
                $permissionContacts = $database->executeObject("SELECT userId,toContactId FROM contactpermissionrequests WHERE userId='" . $userId . "' AND toContactid='" . $contactId . "'");

                if (empty($permissionContacts))
                {
                    $database->executeNonquery("INSERT INTO contactpermissionrequests SET userId='" . $userId . "',toContactId='" . $contactId . "',emailId='" . $emailId . "',requestDate='" . $requestDate . "',statusId=2");
                    //return $email;
                }
            }

        }

        function getUserFitProfile($database, $userId)
        {
            $uniqueCode = $database->executeScalar("SELECT uniqueCode FROM pages WHERE userId='" . $userId . "' and pageBrowserTitle='Fit Profile'");
            return $uniqueCode;
        }

        function getUserSkinProfile($database, $userId)
        {
            $uniqueCode = $database->executeScalar("SELECT uniqueCode FROM pages WHERE userId='" . $userId . "' and pageBrowserTitle='Skin Care Profile'");
            return $uniqueCode;
        }

        function CheckLastStepOfPlan($planId, $contactId)
        {
            $database = new database();
            return $database->executeScalar("select planId from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");

        }


        public function CompleteContactPlanByGroup($database, $contactId, $groupId, $userId)
        {


            $date = date('Y-m-d');
            $date = strtotime($date);
            $plan = $database->executeObject("SELECT * from plan WHERE groupId='" . $groupId . "' and userId='" . $userId . "'");


            if ($plan->planId != '' || !empty($plan->planId))
            {


                $i = 0;
                $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' order by order_no asc");
                $stepno = 0;

                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                foreach ($palnSteps as $ps)
                {

                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date


                    if ($ps->isTask == 0)
                    {

                        if ($totalSteps == $stepCounter)
                        {
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                        }

                    }
                    else
                    {


                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,completedDate, customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','1','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','',NOW(),'0','1')");
                        $taskId = $database->insertid();
                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                        if ($totalSteps == $stepCounter)
                        {
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                        }
                    }

                    $stepCounter++;
                }


                return $plan->planId;


            }

        }

        public function showContactGroups($database, $contactId)
        {

            return $database->executeObjectList("SELECT groupId FROM groups  WHERE groupId IN(SELECT groupId FROM groupcontacts WHERE contactId='" . $contactId . "')");

        }

        public function deleteContactPlanLinkedtoGroup($database, $contactId, $userId, $newGroups)
        {


            $contactGroupsOld = self::showContactGroups($database, $contactId);
            $i = 0;
            $contactGroups = array();
            foreach ($contactGroupsOld as $cg)
            {
                $contactGroups[$i] = $cg->groupId;
                $i++;
            }
            /*echo "new groups";
		print_r($newGroups);*/
            $groups = array_diff($contactGroups, $newGroups);
            /*echo "diff groups";
		print_r($groups);*/
            for ($j = 0; $j < sizeof($groups); $j++)
            {
                if (!in_array($groups[$j], $newGroups['groups']))
                {
                    $groupPlans = $database->executeObjectList("SELECT gc.groupId,planId from groupcontacts gc JOIN plan p on gc.groupId=p.groupId where gc.contactId='" . $contactId . "' and p.userId='" . $userId . "' and gc.groupId='" . $groups[$j] . "'");
                    foreach ($groupPlans as $plans)
                    {

                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plans->planId . "' order by order_no asc");
                        foreach ($palnSteps as $ps)
                        {
                            $database->executeNonQuery("DELETE from planstepusers where stepId='" . $ps->stepId . "' and contactId='" . $contactId . "'");
                            //echo "SELECT tc.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId  where userId='".$userId."' and tc.contactId='".$contactId."' and planId='".$plans->planId."'";
                            $tasks = $database->executeObjectList("SELECT tc.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId  where userId='" . $userId . "' and tc.contactId='" . $contactId . "' and planId='" . $plans->planId . "'");
                            foreach ($tasks as $t)
                            {
                                //echo "DELETE from tasks where taskId='".$t->taskId."' and isCompleted<>1";
                                $database->executeNonQuery("DELETE from tasks where taskId='" . $t->taskId . "' and isCompleted<>1");
                                $isDeleted = $database->getAffectedRows();
                                /*echo "task deleted : ".$isDeleted;
							echo '<br>';*/
                                if ($isDeleted > 0)
                                {
                                    /*echo "DELETE  FROM taskplan WHERE taskId='".$t->taskId."' and planId='".$ps->planId."' and stepId='".$ps->stepId."'";
								echo "DELETE  FROM taskcontacts WHERE contactId='".$contactId."' and taskId='".$t->taskId."'";*/
                                    $database->executeNonQuery("DELETE  FROM taskplan WHERE taskId='" . $t->taskId . "' and planId='" . $ps->planId . "' and stepId='" . $ps->stepId . "'");
                                    $database->executeNonQuery("DELETE  FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $t->taskId . "'");
                                }
                            }


                        }
                        $database->executeNonQuery("DELETE  FROM contactPlansCompleted WHERE planId='" . $plans->planId . "' and  contactId='" . $contactId . "'");
                        $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "' and planId='" . $plans->planId . "'");

                    }

                    $database->executeNonQuery("DELETE FROM groupcontacts WHERE contactId='" . $contactId . "'");
                }

            }


        }

        function getSignatures($database, $userId)
        {
            $details = self::getUserSignature($database, $userId);
            $consultantTitleUserId = '';
            if ($details->leaderId == 0)
            {
                $consultantTitleUserId = $details->userId;
            }
            else
            {
                $consultantTitleUserId = $details->leaderId;
            }
            $consultantTitle = $database->executeScalar("SELECT `fieldCustomName` FROM `leaderdefaultfields` WHERE userId='" . $consultantTitleUserId . "' AND `fieldMappingName`='consultantId' AND `status`='1'");


            if (empty($consultantTitle))
            {
                $arbonLogo = '<img src="http://zenplify.biz/images/GreenLogo1x1.png">';
                $firstName = $details->firstName;
            }
            $signature = '<span style="font-size:14px;">Best Regards,</span><br /><br /><span style="font-size:14px;">' . $firstName . '</span><br />
			<div><div style="float:left;">' . $arbonLogo . '</div><div style=" margin-left:5px;float:left;font-size:14px;">' . $details->firstName . ' ' . $details->lastName;

            if ($details->title != '')
                $signature = $signature . ', ' . $details->title;
            if (strpos($details->webAddress, "http://") >= 0 || strpos($details->webAddress, "https://") >= 0)
                $web = $details->webAddress;
            else
                $web = "http://" . $details->webAddress;

            if (strpos($details->facebookAddress, "http://") >= 0 || strpos($details->facebookAddress, "https://") >= 0)
                $facebook = $details->facebookAddress;
            else
                $facebook = "http://" . $details->facebookAddress;

            $webNew = preg_replace('#^https?://#', '', $web);
            $fbNew = preg_replace('#^https?://#', '', $facebook);
            $signature = $signature . '<br /><span style="color: #2E6A30;font-size:14px;">Arbonne Consultant ' . $details->consultantId . '</span><br /><span style="color: #2E6A30;font-size:14px;"><a style="color: #2E6A30;font-size:14px;" href="' . $web . '" target="_blank">' . $webNew . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $facebook . '" target="_blank">' . $fbNew . '</a></div></div>';

            return $signature;
        }
    }

?>