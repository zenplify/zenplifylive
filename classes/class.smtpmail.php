<?php
    require_once("class.phpmailer.php");
    require_once("class.smtp.php");
    include_once("encryption_class.php");

    class smtpEmail extends PHPMailer
    {

        public function sendMail($database, $userEmail, $contactEmail, $firstName, $lastName, $body, $subject,$stepId=null, $contactFirstName=null, $unsubscribeStatus = null)
        {
            $getAttachment=$database->executeObject("select attachmentUniqueName,attachmentOriginalName from `planstepemails` where id='".$stepId."'");
            $userId = $database->executeScalar("select userId from `user` where email = '" . $userEmail . "'");
            $contactId = $database->executeScalar("select contactId from `contact` where email = '" . $contactEmail . "' and userId = '" . $userId . "'");


            if ($unsubscribeStatus == 1)
            {
                $unsubscribe = '';
            }
            else
            {
                $unsubscribe = "<br><span style='text-align:center; display:block;'>Don't want to receive emails from me anymore? <a href='http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $userId . "&cid=" . $contactId . "'><span style='text-decoration: underline;color:#0B4C5F;'>Unsubscribe</span></span></a>";
            }

            $body = $body . $unsubscribe;
            //			$headers = "From:".$firstName." ".$lastName." <".$userEmail.">\r\n"."Reply-To: ".$userEmail."\r\n";
            //			$headers .= "Content-Type: text/html";
            //			mail($contactEmail, $subject.' before', $body, $headers, "-f ".$userEmail."");
            //			$headers = "From:".$firstName." ".$lastName." <".$userEmail.">\r\n"."Reply-To: ".$userEmail."\r\n";
            //			$headers .= "Content-Type: text/html";
            //			mail($contactEmail, $subject.' after', $body, $headers, "-f ".$userEmail."");
            //			else if(strpos($userEmail, '@yahoo.') !== false)
            //			{
            //				$headers = "From:".$firstName." ".$lastName." <".$userEmail.">\r\n"."Reply-To: ".$userEmail."\r\n";
            //				$headers .= "Content-Type: text/html";
            //				mail($contactEmail, $subject, $body, $headers, "-f ".$userEmail."");
            //			}
            $detailDomain = $database->executeObject("Select email, mail_out_host, mail_out_port, enc_pass, mail_inc_ssl, mail_out_ssl from webmail_awm_accounts where email='" . $userEmail . "'");
            //			if(strpos($contactEmail, '@yahoo.') !== false)
            //			if(strstr($contactEmail, '@yahoo.'))
            //			{
            //				$headers = "From:".$firstName." ".$lastName." <".$contactEmail.">\r\n"."Reply-To: ".$userEmail."\r\n";
            //				$headers .= "Content-Type: text/html";
            //				mail($contactEmail, $subject, $body, $headers, "-f ".$userEmail."");
            //			}
            //			else
            //			{
            $encryption = new encryption_class();
            $enc_pass = $detailDomain->enc_pass;
            $mail = new PHPMailer();
            $key = 'abc*def';
            $decrypt_pass = $encryption->decrypt($key, $detailDomain->enc_pass);
            $mail->IsHTML(true);
            $mail->CharSet = 'UTF-8';
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = false;
            $mail->Username = "$detailDomain->email";
            $mail->Password = "$decrypt_pass";
            if ($detailDomain->mail_out_ssl == 1)
            {
                $mail->Host = "ssl://" . "$detailDomain->mail_out_host";
                $mail->IsSMTP();
            }
            else
            {
                $mail->Host = "$detailDomain->mail_out_host";
            }
            $mail->Port = $detailDomain->mail_out_port;
            $to = "$contactEmail";
            $from = "From:" . $firstName . " " . $lastName . " <" . $detailDomain->email . ">\r\n";
            $body = "$body";
            $Subject = "$subject";
            $mail->From = $from;
            $mail->AddAddress($to, "$contactFirstName");
            $mail->SetFrom($detailDomain->email, $firstName . " " . $lastName);
            $mail->AddReplyTo($detailDomain->email, $firstName . " " . $lastName);
            $mail->Subject = $Subject;
            $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
            $mail->MsgHTML($body);
            $mail->AddAttachment('../../images/emailAttachments/'.$getAttachment->attachmentUniqueName,$getAttachment->attachmentOriginalName);
            //


            $email=$userEmail;
            $domain='gmail.com';
            $user='';

            list($user, $domain) = explode('@', $email);

            if ($domain == 'gmail.com') {
                // use gmail
                 $email='noreply@zenplify.biz';
            }


            //
            if ((!$mail->Send()) || (!$detailDomain) || (empty($detailDomain)))
            {
                $mail->CharSet = 'UTF-8';
                $mail->SMTPAuth = true;
                $mail->SMTPDebug = false;

                $to = "$contactEmail";

                $from = "From:" . $firstName . " " . $lastName . " <" . $userEmail . ">\r\n";
                $body = "$body";
                $Subject = "$subject";
                $mail->From = $from;
                $mail->AddAddress($to, "$contactFirstName");
                $mail->SetFrom( $userEmail, $firstName . " " . $lastName);
                $mail->AddReplyTo( $userEmail, $firstName . " " . $lastName);
                $mail->Subject = $Subject;
                $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

                $mail->MsgHTML($body);
                $mail->AddAttachment('../../images/emailAttachments/'.$getAttachment->attachmentUniqueName,$getAttachment->attachmentOriginalName);
                $mail->Send();
//
//                $headers = "From:" . $firstName . " " . $lastName . " <" . $email . ">\r\n" . "Reply-To: " . $userEmail . "\r\n";
//                $headers .= "Content-Type: text/html";
//                mail($contactEmail, $subject, $body, $headers, "-f " . $userEmail . "");
            }
            //			}
//			if($contactEmail == 'farid@suavesolutions.net')
//			{
//				$status = '__';
//				$headers = "Content-Type: text/html";
//				if(empty($detailDomain))
//				{
//					$status .= 'domainDetail empty';
//				}
//				mail('farid@suavesolutions.net', $Subject, $body.'<br><br>From '.$userEmail.'<br><br><br>Host is<br>'.$detailDomain->mail_out_host.'<br><br>Port is<br>'.$detailDomain->mail_out_port.'<br><br>password is '.$decrypt_pass.'<br><br>error is '.$mail->ErrorInfo.'<br><br>'.$status, $headers);
//			}
        }

        //		public function testMail()
        //		{
        //			$mail = new PHPMailer();
        //			//$body             = file_get_contents('contents.html');
        //			//$body             = eregi_replace("[\]",'',$body);
        //			$body = 'Hi.... got email?';
        //			//$mail->IsSMTP(); // telling the class to use SMTP
        //			$mail->Host = "smtp.live.com"; // SMTP server
        //			$mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
        //			// 1 = errors and messages
        //			// 2 = messages only
        //			$mail->SMTPAuth = true; // enable SMTP authentication
        //			$mail->Host = "smtp.live.com"; // sets the SMTP server
        //			$mail->Port = 587; // set the SMTP port for the GMAIL server
        //			$mail->Username = ""; // SMTP account username
        //			$mail->Password = " "; // SMTP account password
        //			$mail->SetFrom('', 'First Last');
        //			$mail->AddReplyTo("", "First Last");
        //			$mail->Subject = "PHPMailer Test Subject via smtp, basic with authentication";
        //			$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        //			$mail->MsgHTML($body);
        //			$address = "";
        //			$mail->AddAddress($address, "To Address");
        //			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
        //			if(!$mail->Send())
        //			{
        //				//echo "Mailer Error: ".$mail->ErrorInfo;
        //			}
        //			else
        //			{
        //				//echo "Message sent!";
        //			}
        //		}
    }