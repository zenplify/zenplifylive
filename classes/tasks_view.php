<?php 
error_reporting(0);
session_start();
include_once("../headerFooter/header.php");
require_once ("../../classes/task.php");
require_once ("../../classes/plan.php");

$userId=$_SESSION['userId'];

$task = new task();
$plan = new plan();
$tasks = $task->GetAllUserTask($userId);
$todayTasks=$task->getTodayTasks($userId);
$upComingTasks=$task->getUpcomingtasks($userId);
$overdueTasks=$task->getOverdueTasks($userId);
$generalTask=$task->getGeneralTasks($userId);
$reminderTask=$task->GetPlanReminderTasks($userId);
//$planStepTasks=$plan->planStepTasksToday($userId);
//foreach($planStepTasks as $pst){
	//echo $pst->summary;
	
	//}

?>
 <script type="text/javascript" src="../../js/jquery-1.8.3.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
 <script type="text/javascript" src="../../js/script.js"></script>

 <script type="text/javascript" charset="utf-8">

			 			  var contactArray=new Array();
   
			$(document).ready( function () {
				
			
			         $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra']})
					.tablesorterPager({container: $("#pager"),size:50}); 
	// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function(){
			// When value of the input is not blank
			if( $(this).val() != "")
			{
				// Show only matching TR, hide rest of them
				$("#contact_table tbody>tr").hide();
				$("#contact_table td:contains-ci('" + $(this).val() + "')").parent("tr").show();
			}
			else
			{
				// When there is no input or clean again, show everything back
				$("#contact_table tbody>tr").show();
			}
		});

		
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"], 
	{
	    "contains-ci": function(elem, i, match, array) 
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
	function check(field)
	{
		$("#contact_table tbody>tr").hide();

		var list = new Array();
		var j=0;
	 	for (i=0;i<field.length;i++)
	 		{
				if (field[i].checked==true)
				{	
					list[j]=field[i].value;	
					j++;
					
				}
			
	 		}
		var arrayLength=list.length;
		if(arrayLength!=0)
			{
				for (k=0;k<arrayLength;k++)
	 				{
						$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
	 				}
			}
		else
			{
				$("#contact_table tbody>tr").show();
			}
 		//alert (list.length);		


 	}
	
		    	 
				
	       
			    					
			function deleteTask(id){

				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{deleteId:id,action:'deleteIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
	
            }
				function skipTask(id){

				//alert(id);
				
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{skipId:id,action:'skipIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
				
            }
			function editTask(editType){
				var taskId=$("#taskId").val();
				window.location.href='edit_task.php?edit_type='+editType+'&task_id='+taskId;
				}
            </script>
 
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <a href="new_task.php"><input type="button" id="new_task" value=""  /></a>
    
     </div>
       <div class="sub_container">
          <div class="col_table">
             <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
  				<thead>    
                <tr class="header">
                    <th class="first" >&nbsp;</th>
                    <th class="center big">Summary </th>
                    <th class="center">Priority </th>
                    <th class="center">Due Date </th>
                    <th class="center">Who </th>	
                    <th class="center smallcol">Actions</th>
                    <th class="last"></th>
                </tr>
                </thead>
                
                <?php
				if(empty($todayTasks)&& empty($upComingTasks)&& empty($generalTask)&&empty($overdueTasks)&&empty($reminderTask)){
					echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
					}else{
                if(!empty($todayTasks)){
					 echo "<tr  class='taskDiff'>
					 <td colspan='7'>&nbsp; Today</td>
					 </tr>";
					foreach($todayTasks as $tt){
						
						$priority=$task->GetTaskPriority($tt->priorityId);
						$contactId=$task->getcontactId($tt->taskId);
						
							  echo "<tr class=\"odd_gradeX\" id='task".$tt->taskId."'>";
							  echo "<td>&nbsp;</td>";
							  echo "<td><a href=edit_task.php?task_id=".$tt->taskId.">".$tt->title;?>
                               
							 <img  src="<?php echo ($tt->planId==0?'':'../../images/ndent_task.png')?>" alt="<?php echo ($tt->planId==0?'':'Plan Step')?>" title="<?php echo ($tt->planId==0?'':'PlanStep')?>" />
                              <?php echo "</td>";
                             
							  echo "<td>".$priority->priority."</td>";
							  
							  echo "<td>".date("m/d/Y", strtotime($tt->dueDateTime));
					?>
                              
					<img  src="<?php echo ($tt->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($tt->repeatTypeId!=1?'recurs ':'')?>"/>	
							  <?php echo "</td>";
							  echo "<td>";
							  if($tt->planId==0){
                                foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
							  }else{
								 $task_contact=$task->taskContact($task->getcontactId($tt->taskId));
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
								  
								  }
                              echo "</td>";
													if($tt->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $tt->taskId;?>')" />
							<img onclick=<?php echo ($tt->planId==0?"deleteTask('$tt->taskId')":"skipTask('$tt->taskId')")?> src="<?php echo ($tt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($tt->planId==0?'Archive':'Skip')?>" title="<?php echo ($tt->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="edit_task.php?task_id=<?php echo $tt->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($tt->planId==0?"deleteTask('$tt->taskId')":"skipTask('$tt->taskId')")?> src="<?php echo ($tt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($tt->planId==0?'Archive':'Skip')?>" title="<?php echo ($tt->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }
													
												 echo "<td>&nbsp;</td>";
												echo "<td></td>";
												echo "</tr>";
						}
					}
				?>
                <?php 
				if(!empty($upComingTasks)){
					 echo "<tr  class='taskDiff'>
					 <td colspan='7'>&nbsp; Upcoming - Tasks for future</td>
					 </tr>";
					 foreach($upComingTasks as $ut){
						 
						$priority=$task->GetTaskPriority($ut->priorityId);
						$contactId=$task->getcontactId($ut->taskId);
						
							  echo "<tr class=\"odd_gradeX\" id='task".$ut->taskId."'>";
							   echo "<td>&nbsp;</td>";
							  echo "<td><a href=edit_task.php?task_id=".$ut->taskId.">".$ut->title ;?>
							 <img  src="<?php echo ($ut->planId==0?'':'../../images/ndent_task.png')?>" alt="<?php echo ($ut->planId==0?'':'Plan Step')?>" title="<?php echo ($ut->planId==0?'':'PlanStep')?>" />
                              <?php "</td>";
							   echo "<td>".$priority->priority."</td>";
							  echo "<td>".date("m/d/Y", strtotime($ut->dueDateTime));
					?>
                              
					<img  src="<?php echo ($ut->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($ut->repeatTypeId!=1?'recurs ':'')?>"/>	
							  <?php echo "</td>";
							  echo "<td>";
							  if($ut->planId==0){
                                foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
							  }else{
								echo $task_contact=$task->taskContact($task->getcontactIds($ut->taskId));
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
								  
								  }
                              echo "</td>";
													if($ut->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $ut->taskId;?>')" />
							<img onclick=<?php echo ($ut->planId==0?"deleteTask('$ut->taskId')":"skipTask('$ut->taskId')")?> src="<?php echo ($ut->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($ut->planId==0?'Archive':'Skip')?>" title="<?php echo ($ut->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="edit_task.php?task_id=<?php echo $ut->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($ut->planId==0?"deleteTask('$ut->taskId')":"skipTask('$ut->taskId')")?> src="<?php echo ($ut->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($ut->planId==0?'Archive':'Skip')?>" title="<?php echo ($ut->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }
													
												 echo "<td>&nbsp;</td>";
												echo "<td></td>";
												echo "</tr>";
						 }
					}
				?>
                <?php 
				if(!empty($overdueTasks)){
					echo "<tr class='taskDiff' >
					 <td colspan='7'>&nbsp; Overdue - Tasks with deadline in past</td>
					 </tr>";
					 foreach($overdueTasks as $ot){
					 $priority=$task->GetTaskPriority($ot->priorityId);
						$contactId=$task->getcontactId($ot->taskId);
						
							  echo "<tr class=\"odd_gradeX\" id='task".$ot->taskId."'>";
							  echo "<td>&nbsp;</td>";
							  echo "<td><a href=edit_task.php?task_id=".$ot->taskId.">".$ot->title."</td>";
							  echo "<td>".$priority->priority."</td>";
							  echo "<td>".date("m/d/Y", strtotime($ot->dueDateTime));
					?>
                              
					<img  src="<?php echo ($ot->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($ot->repeatTypeId!=1?'recurs ':'')?>"/>	
							  <?php echo "</td>";
							   echo "<td>";
							  if($tt->planId==0){
                                foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
							  }else{
								 $task_contact=$task->taskContact($task->getcontactId($tt->taskId));
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
								  
								  }
                              echo "</td>";
													if($ot->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $ot->taskId;?>')" />
							<img onclick=<?php echo ($ot->planId==0?"deleteTask('$ot->taskId')":"skipTask('$ot->taskId')")?> src="<?php echo ($ot->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($ot->planId==0?'Archive':'Skip')?>" title="<?php echo ($ot->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="edit_task.php?task_id=<?php echo $ot->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($ot->planId==0?"deleteTask('$ot->taskId')":"skipTask('$ot->taskId')")?> src="<?php echo ($ot->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($ot->planId==0?'Archive':'Skip')?>" title="<?php echo ($ot->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }
													
												 echo "<td>&nbsp;</td>";
												echo "<td></td>";
												echo "</tr>";
					 }
					}
				?>
                <?php if(!empty($generalTask)){
					echo "<tr class='taskDiff'>
					 <td  colspan='7'> &nbsp;General Items - Tasks with no dates</td>
					 </tr>";
					 foreach($generalTask as $gt){
						 $priority=$task->GetTaskPriority($gt->priorityId);
						$contactId=$task->getcontactId($gt->taskId);
						
							  echo "<tr class=\"odd_gradeX\" id='task".$gt->taskId."'>";
							  echo "<td>&nbsp;</td>";
							  echo "<td><a href=edit_task.php?task_id=".$gt->taskId.">".$gt->title."</td>";
							  echo "<td>".$priority->priority."</td>";
							  echo "<td>"."<span style='margin-left:56px;'>&nbsp;</span>";
					?>
                              
					<img  src="<?php echo ($gt->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($gt->repeatTypeId!=1?'recurs ':'')?>"/>	
							  <?php echo "</td>";
							  echo "<td>";
							  if($tt->planId==0){
                                foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
							  }else{
								 $task_contact=$task->taskContact($task->getcontactId($tt->taskId));
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
								  
								  }
                              echo "</td>";
													if($gt->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $gt->taskId;?>')" />
							<img onclick=<?php echo ($gt->planId==0?"deleteTask('$gt->taskId')":"skipTask('$gt->taskId')")?> src="<?php echo ($gt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($gt->planId==0?'Archive':'Skip')?>" title="<?php echo ($gt->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="edit_task.php?task_id=<?php echo $gt->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($gt->planId==0?"deleteTask('$gt->taskId')":"skipTask('$gt->taskId')")?> src="<?php echo ($gt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($gt->planId==0?'Archive':'Skip')?>" title="<?php echo ($gt->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }
													
												 echo "<td>&nbsp;</td>";
												echo "<td></td>";
												echo "</tr>";
						 }
					}
				?>
                <?php if(!empty($reminderTask)){
					echo "<tr class='taskDiff'>
					 <td  colspan='7'> &nbsp;Reminder Tasks</td>
					 </tr>";
					 foreach($reminderTask as $rt){
						 $priority=$task->GetTaskPriority($rt->priorityId);
						$contactId=$task->getcontactId($rt->taskId);
						
							  echo "<tr class=\"odd_gradeX\" id='task".$rt->taskId."'>";
							   echo "<td>&nbsp;</td>";
							  echo "<td><a href=edit_task.php?task_id=".$rt->taskId.">".$rt->title."</td>";
							  echo "<td>".$priority->priority."</td>";
							  echo "<td>".date("m/d/Y", strtotime($rt->dueDateTime));
					?>
                              
					<img  src="<?php echo ($rt->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($rt->repeatTypeId!=1?'recurs ':'')?>"/>	
							  <?php echo "</td>";
							   echo "<td>";
							  if($tt->planId==0){
                                foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
							  }else{
								 $task_contact=$task->taskContact($task->getcontactId($tt->taskId));
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
								  
								  }
                              echo "</td>";
													if($rt->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $rt->taskId;?>')" />
							<img onclick=<?php echo ($rt->planId==0?"deleteTask('$rt->taskId')":"skipTask('$rt->taskId')")?> src="<?php echo ($rt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($rt->planId==0?'Archive':'Skip')?>" title="<?php echo ($rt->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="edit_task.php?task_id=<?php echo $rt->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($rt->planId==0?"deleteTask('$rt->taskId')":"skipTask('$rt->taskId')")?> src="<?php echo ($rt->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($rt->planId==0?'Archive':'Skip')?>" title="<?php echo ($rt->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }
													
												 echo "<td>&nbsp;</td>";
												echo "<td></td>";
												echo "</tr>";
						}
					}
					}
				?>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="float:right; margin-top:10px;">
                               
                                   
                                    <div class="pager" id="pager">
	<form>
		<img class="first" src="../../images/paging_far_left.gif" />
		<img class="prev" src="../../images/paging_left.gif" />
		<input type="text" class="pagedisplay" value="1/1" />
		<img class="next" src="../../images/paging_right.gif" />
		<img class="last" src="../../images/paging_far_right.gif" />
		
        <select id="pagesize" name="pagesize" class="pagesize"><option value="50" selected="selected">50</option><option value="100">100</option><option value="250">250</option></select>       
	</form>
</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                
       		</div>
           <div class="col_search">
                <p class="search_header">Find Tasks</p>
               
           		<input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
                 <br />
                
                
                 <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <!--
                <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <img src="images/check.png" /> Include Custome Fields-->
               <!-- <p class="search_header small">By the Group <img src="images/arrow.png" class="arrow" /></p>-->
           </div>
           
       </div>
      
     <?php 
		include_once("../headerFooter/footer.php");
	?>
	
    <div id="Popup"></div>

<div id="popupReoccur" >
  <h1 class="gray">Changing 	a recurring task</h1>
  <input type="hidden" name="taskId" id="taskId" />onlyThisTask
  <span class="popupEditTask">You're changing a recurring task. Do you want to change only this task, or all future tasks?</span>
  <br /><br />
  <span ><input type="button" value="" class="onlyThisTask" onclick="editTask('1')" /> <input type="button" value="" onclick="editTask('0')" class="allFutureTask"/><input type="button" name="cancel" class="cancel" value="" onClick="disablereoccurPopup()"/></span>
</div> 
