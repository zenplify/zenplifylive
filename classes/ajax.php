<?php

    include 'init.php';

    require_once("register.php");
    require_once("task.php");
    require_once("appiontment.php");
    require_once("contact.php");
    require_once("settings.php");
    require_once("profiles.php");
    require_once("plan.php");
    require_once("group.php");
    require_once("notification.php"); // added by junaid
    include_once("class.smtpmail.php");

    $userId = $_SESSION['userId'];
    $leaderId = $_SESSION['leaderId'];
    $generatedVia = $_SESSION['generatedVia'];

    if (isset($_POST['action']) && !empty($_POST['action']))
    {
        $action = $_POST['action'];
        switch ($action)
        {
            case 1 :
                CheckForUserAleadyExist();
                break;
            case 2 :
                CheckForEmailAleadyExist();
                break;
            case 'deleteTask':
                deleteTask();
                break;
            case 'deleteAppiontment':
                deleteAppiontment();
                break;
            case 'deleteNotes':
                deleteNotes();
                break;
            case 'addNotes':
                addStickyNotes();
                break;
            case 'moveToHistory':
                moveToHistory();
                break;
            case 'checkStickyNotes':
                checkStickyNotes();
                break;
            case 'addQuickContact':
                addQuickContact();
                break;
            case 'deleteIt':
                archiveTask();
                break;
            case 'skipIt':
                skipTask();
                break;
            case 'pagination':
                recordsPerPage();
                break;
            case 'paginationOfContact':
                contactRecordsPerPage();
                break;
            case 'archiveContact':
                archiveContact();
                break;
            case 'assignNextStep':
                assignNextStep();
                break;
            case 'Unsubscribed':
                Unsubscribed();
                break;
            case 'sendPermissionToBlock':
                sendPermissionToBlock();
                break;
            case 'sendPermissionMail':
                sendPermissionMail($leaderId, $userId);
                break;
            case 'markascompleted':
                markascompleted();
                break;
            case 'searchInfo':
                searchInfo();
                break;
            case 'searchPermissionManagerContact':
                searchPermissionManagerContact();
                break;
            case 'updateTaskAgainstContact':
                updateTaskAgainstContact();
                break;
            case 'restoreContact':
                restoreContact();
                break;
            case 'deleteContact':
                deleteContact();
                break;
            case 'restoreAllContacts':
                restoreAllContacts();
                break;
            case 'deleteAllContacts':
                deleteAllContacts();
                break;
            case 'archiveMultipleContacts':
                archiveMultipleContacts();
                break;
            case 'checkLeaderSignupName':
                checkLeaderSignupName($userId);
                break;
            case 'saveLeaderSignupName':
                saveLeaderSignupName();
                break;
            case 'saveLeaderDefaultFields':
                saveLeaderDefaultFields();
                break;
            case 'toggleLeaderDefaultFields':
                toggleLeaderDefaultFields();
                break;
            case 'toggleLeaderQuickLinks':
                toggleLeaderQuickLinks();
                break;
            case 'getLeaderQuestionDetails':
                getLeaderQuestionDetails();
                break;
            case 'updateDefaultFieldOrder':
                updateDefaultFieldOrder();
                break;
            case 'updateCustomQuestionsOrder':
                updateCustomQuestionsOrder();
                break;
            case 'updateProfileOrder':
                updateProfileOrder();
                break;
            case 'updatePlanStepOrder':
                updatePlanStepOrder();
                break;
            case 'checkProfileNameAvailability':
                checkProfileNameAvailability();
                break;
            case 'getDefaultTemplateValues':
                getDefaultTemplateValues();
                break;
            case 'updatePageQuestiontFieldOrder':
                updatePageQuestiontFieldOrder();
                break;
            case 'togglePageQuestions':
                togglePageQuestions();
                break;
            case 'toggleCustomRuleQuestion':
                toggleCustomRuleQuestion();
                break;
            case 'archiveProfile':
                archiveProfile($leaderId, $userId);
                break;
            case 'discardProfile':
                discardProfile($leaderId, $userId);
                break;
            case 'archivePlan':
                archivePlan($userId, $leaderId);
                break;
            case 'publishPlan':
                publishPlan($userId, $leaderId);
                break;
            case 'editPlan':
                editPlan($userId, $leaderId);
                break;
            case 'archiveStep':
                archiveStep($userId, $leaderId);
                break;
            case 'archiveGroup':
                archiveGroup($userId, $leaderId);
                break;
            case 'publishGroup':
                publishGroup($userId, $leaderId, $generatedVia);
                break;
            case 'unArchiveGroup':
                unArchive();
                break;
            case 'NotNowRequest':
                NotNowRequest();
                break;
            case 'confirmForstep':
                confirmForstep($userId);
                break;
            case 'ConfirmRequest';
                ConfirmRequest();
                break;
            case 'publishProfile';
                publishProfile($user, $leaderId);
                break;
            case 'editProfileNotification';
                editProfileNotification($userId, $leaderId);
                break;
            case 'markArchive';
                markArchive();
                break;
            case 'markunArchive';
                markunArchive($userId);
                break;
            case 'toggleDefaultQuestions':
                toggleDefaultQuestions();
                break;
            case 'RequestAllowsignup';
                RequestAllowsignup($_REQUEST['status'],$userId);
                break;
            case 'editVideoForVENVPsTemplate';
                editVideoForVENVPsTemplate($_REQUEST['editedVideo'],$_REQUEST['pageId'],$userId);
                break;
            case 'isOntraportLeader';
                isOntraportLeader($_REQUEST['leaderName']);
                break;
            case 'saveSignature':
                saveSignature($_POST);
                break;
            case 'savePermissionEmail':
                savePermissionEmail($_POST);
                break;
            case 'deleteFile':
                deleteFile($_REQUEST['step_id']);
                break;
            case 'getDaysAfter':
                getDaysAfter($_REQUEST['selectedstepId']);
                break;

        }
    }

    function saveSignature($post) {
        $settings  = new settings();
        $result = $settings->saveSignature($userId, $post['signature'], $post['signatureId']);
        echo json_encode($result);
        exit();
    }
function savePermissionEmail($post) {
    $settings  = new settings();
    $result = $settings->savePermissionEmail($userId, $post['permissionEmail'], $post['permissionEmailId'],$_POST['permissionEmailSubject']);
    echo json_encode($result);
    exit();
}

    function CheckForUserAleadyExist()
    {
        $username = $_POST['us'];
        $reg = new register();
        $result = $reg->getUserForSignupCheck($username);
        echo $result;
    }

    function CheckForEmailAleadyExist()
    {
        $email = $_POST['em'];
        $reg = new register();
        $userId = $_SESSION['userId'];
        $result = $reg->getEmailForSignupCheck($email,$userId);
        echo $result;
    }

    function deleteTask()
    {
        $taskId = $_POST['taskId'];
        $contactId = $_POST['contactId'];
        //alert('function call for delete Task'.$taskId.$contactId);
        $task = new task();
        $result = $task->deleteTaskOnContactDetail($taskId, $contactId);
        echo $result;
    }

    function deleteAppiontment()
    {
        $appiontmentId = $_POST['appiontmentId'];
        $contactId = $_POST['contactId'];
        //alert('function call for delete Task'.$taskId.$contactId);
        $appiontment = new appiontment();
        $result = $appiontment->deleteAppiontmentOnContactDetail($appiontmentId, $contactId);
        echo $result;
    }

    function deleteNotes()
    {
        $notesId = $_POST['notesId'];
        $contactId = $_POST['contactId'];
        //alert('function call for delete Task'.$taskId.$contactId);
        $notes = new task();
        $result = $notes->deleteNotes($notesId);
        echo $result;
    }

    function checkStickyNotes()
    {
        $contactId = $_POST['contactId'];
        $checkNotes = new task();
        $result = $checkNotes->checkStickyNotesAlreadyExist($contactId);
        if ($result == 1)
        {
            addStickyNotes();
        }
        else
        {
            updateStickyNotes($result);
        }
    }

    function addStickyNotes()
    {
        $stickyNotes = $_POST['stickyNotes'];
        $contactId = $_POST['contactId'];
        $notes = new task();
        $nt = $notes->stickyNotes($stickyNotes, $contactId);
        if (!empty($nt))
        {
            echo $stickyNotes;
        }
    }

    function updateStickyNotes($result)
    {
        $stickyNotes = $_POST['stickyNotes'];
        $contactId = $_POST['contactId'];
        $notes = new task();
        $nt = $notes->updateStickyNotes($stickyNotes, $contactId, $result);
        if (!empty($nt))
        {
            echo $stickyNotes;
        }
    }

    function moveToHistory()
    {
        $saveNotes = $_POST['stickyNotes'];
        $contactId = $_POST['contactId'];
        $noteId = $_POST['noteId'];
        $notes = new task();
        if (empty($noteId) || $noteId == 0)
        {
            $result = $notes->checkStickyNotesAlreadyExist2($contactId, $saveNotes);
            if ($result)
            {
                $noteId = $notes->stickyNotes($saveNotes, $contactId);
            }
        }
        $noteId2 = $notes->moveToHistory($contactId, $saveNotes, $noteId);
        if (!empty($noteId2))
        {
            $n = $notes->showHistory($noteId2);
            echo '<tr id="recordNotes' . $n->noteId . '"><td><img src="../../images/sticky_notes.jpg" style="cursor:pointer; width:15px; height:15px;" id="img' . $n->noteId . '"></td>
                    <td>' . $n->notes . '</td>
                    <td>' . date('m/d/Y', strtotime($n->addedDate)) . '</td>
                     <td>&nbsp;</td>

                      <td>';
            echo "<a href=../contact/contact_detail.php?contactId=" . $n->contactId . ">" . $n->name . "</a>";
            echo '</td>
                      <td><img src="../../images/delete.png" alt="delete" title="delete" style="margin-left:25px;" class="linkButton" onclick="delnotes(\'' . $n->noteId . '\');" /></td>
                 </tr>';
        }
    }

    function addQuickContact()
    {
        $contact = new contact();
        $result = $contact->addQuickContactDetail($_POST);
        echo $result;
    }

    function archiveTask()
    {
        $taskId = $_POST['deleteId'];
        $task = new task();
        $result = $task->archiveTaskOnTaskView($taskId);
        echo $result;
    }

    function skipTask()
    {
        $taskId = $_POST['skipId'];
        $task = new task();
        $result = $task->skipTaskOnTaskView($taskId);
        echo $result;
    }

    function recordsPerPage()
    {
        $recordperpage = $_POST['records'];
        $userId = $_POST['userId'];
        $task = new task();
        $nodate = '1990-12-12';
        $tasks = $task->showTaskHistoryByLimit($userId, $recordperpage);
        if (empty($tasks))
        {
            echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
        }
        foreach ($tasks as $row)
        {
            $id = $row->taskId;
            $preority = $task->GetTaskPriority($row->priorityId);
            $contactId = $task->getcontactId($id);
            $task_contact = $task->taskContact($contactId->contactId);
            $dateTime = $row->endingOn;
            echo "<tr class=\"odd_gradeX\" id=\"" . $id . "\">";
            //echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
            echo "<td> </td>";
            echo "<td  class='cross'><a href='edit_task.php?task_id=" . $id . "'>" . $row->title . "</a></td>";
            echo "<td  class='cross'>" . $preority->priority . "</td>";
            echo "<td class='cross'>" . (strtotime($dateTime) < strtotime($nodate) ? '' : date("d/m/Y", strtotime($dateTime))) . "</td><td>";
            foreach ($contactId as $ci)
            {
                $task_contact = $task->taskContact($ci->contactId);
                echo "<a class='cross' href=../contact/contact_detail.php?contactId=" . $task_contact->contactId . ">" . $task_contact->firstName . " " . $task_contact->lastName . "</a><br>";
            }
            echo "</td>";
            echo "<td><a href='edit_task.php?task_id=" . $id . "'><img src=\"../../images/edit.png\" title=\"Edit\" alt=\"Edit\" /></a><a href=\"javascript:\"><img src=" . ($row->planId == 0 ? '../../images/Archive-icon.png' : '../../images/skip.png') . " alt=" . ($row->planId == 0 ? 'Archive' : 'Skip') . " title=" . ($row->planId == 0 ? 'Archive' : 'Skip') . " /></a></td><td>&nbsp";
            echo "<td></td>";
            echo "</tr>";
        }
    }

    function contactRecordsPerPage()
    {
        $recordperpage = $_POST['records'];
        $userId = $_POST['userId'];
        $contact = new contact();
        $data = $contact->GetAllContacts($userId, $recordperpage);
        foreach ($data as $row)
        {
            $id = $row->contactId;
            echo "<tr class=\"odd_gradeX\" id=\"" . $id . "\">";
            echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=" . $row->contactId . "></td>";
            echo "<td><a href=contact_detail.php?contactId=" . $id . ">" . $row->firstName . "</a></td>";
            echo "<td>" . $row->lastName . "</td>";
            echo "<td>" . $row->companyTitle . "</td>";
            echo "<td>" . $row->jobTitle . "</td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "</tr>";
        }
    }

    function archiveContact()
    {
        $contactId = $_POST['contact_id'];
        $contact = new contact();
        $result = $contact->archiveContact($contactId);
        echo $result;
    }

    function assignNextStep()
    {
        $planId = $_POST["planStep"];
        $taskId = $_POST["taskId"];
        $userId = $_POST['userId'];
        $stepName = $_POST['name'];
        $task = new task();
        $contactId = $task->getTaskContactId($taskId);
        $result = $task->assignNextStepToContact($userId, $planId, $stepName, $taskId, $contactId);
        echo $result;
    }

    function Unsubscribed()
    {
        $contactId = $_POST['contactId'];
        $contact = new contact();
        echo $result = $contact->UnsubscribeContact($contactId);
    }

    function sendPermissionToBlock()
    {
        $contactId = $_POST['contactId'];
        $contact = new contact();
        $result = $contact->editContactEmail($contactId);
        echo $result->email;
    }

    function sendPermissionMail($leaderId, $userId1)
    {


        $uid = base62_encode($_POST["userId"]);
        $contact = new contact();
        $database = new database();
        $profile = new profiles();
        $mail = new smtpEmail();
        $contactIdForudate=$_POST["contactId"];
        $userIdForupdate=$_POST["userId"];
      
        $contact->updateProcess($contactIdForudate,$userIdForupdate);

        //
        $getPermissionEmail = $database->executeObject("SELECT permissionEmailBody,permissionEmailSubject from userpermissionemail where userId='".$userId1."'");



        //

        if ($_POST["emailflag"] == 1)
        {
            $idCounter = 0;
            $userId = $_POST["userId"];
            $user = $contact->GetUserName($userId);
            $emailTo = explode(',', $_POST['to']);
            $emailId = explode(',', $_POST['emailId']);
            $contact_ids = explode(',', $_POST['contactId']);
            $html = $_POST['area2'];
            $stripped = strip_tags($html);
            $count = sizeof($emailTo);
            $contactId = $_POST["contactId"];
            //


            //
            while ($count != 0)
            {
                if ($count != 0)
                {
                    $subject = $getPermissionEmail->permissionEmailSubject;
                    $subject = str_replace("[{First Name}]","$user->firstName","$subject");
                    $AcceptLink="<a href='http://techgeese.com/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "'>http://techgeese.com/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "</a>";
                    $declineLink="<a href='http://techgeese.com/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "'>http://techgeese.com/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "</a>";

                    $body = $getPermissionEmail->permissionEmailBody;
                    $body='<p>'.$stripped.'</p>'.$body;
                    $body= str_replace("[{Accept Emails Link}]","$AcceptLink","$body");
                    $body= str_replace("[{DeclineEmails Link}]","$declineLink","$body");
                    $body= str_replace("[{First Name}]","$user->firstName","$body");
                    $headers = "From:" . $user->firstName . " " . $user->lastName . " <" . $user->email . ">\r\n" . "Reply-To: " . $user->email . "\r\n";


                    $mail->sendMail($database, $user->email, $emailTo[$idCounter], $user->firstName, $user->lastName, $body, $subject,$emptyperameter='','1');

                   // ($database, $user->email, $to, $user->firstName, $user->lastName, $body, $subject,
               // $contactFirstName,'1');

                    $contact->permissionRequest($userId, $contact_ids[$idCounter], $emailId[$idCounter]);
                    $idCounter++;
                }
                $count--;
            }
            echo 1;
        }
        else
        {
            $contactId = $_POST["contactId"];
            $userId = $_POST["userId"];
            $user = $contact->GetUserName($userId);
            $contactemail = $contact->showContactEmailAddress($contactId);
            $emailId = $contactemail->emailId . '';
            $to = $_POST["to"];
            $from = $_POST["from"];
            $html = $_POST['area2'];
            $stripped = strip_tags($html);

            $subject = $getPermissionEmail->permissionEmailSubject;
            $subject = str_replace("[{First Name}]","$user->firstName","$subject");
            $AcceptLink="<a href='http://techgeese.com/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "'>http://techgeese.com/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "</a>";
            $declineLink="<a href='http://techgeese.com/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "'>http://techgeese.com/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "</a>";

            $body = $getPermissionEmail->permissionEmailBody;
            $body='<p>'.$stripped.'</p>'.$body;
            $body= str_replace("[{Accept Emails Link}]","$AcceptLink","$body");
            $body= str_replace("[{DeclineEmails Link}]","$declineLink","$body");
            $body= str_replace("[{First Name}]","$user->firstName","$body");
            $headers = "From:" . $user->firstName . " " . $user->lastName . " <" . $user->email . ">\r\n" . "Reply-To: " . $user->email . "\r\n";

            $contactFirstName='';
            $mail->sendMail($database, $user->email, $to, $user->firstName, $user->lastName, $body, $subject,
                $contactFirstName,'1');
            $contact->permissionRequest($userId, $contactId, $emailId);
            echo 1;
        }
    }

    function markascompleted()
    {
        $task = new task();
        $taskId = $_POST['taskId'];
        $userId = $_POST['userId'];
        $contactId = '';
        $completeDate = '0000-00-00';
        $completeTime = '00:00:00';
        //echo $result=$task->updateTask($taskId,$userId);
        echo $result = $task->markTaskCompleted($userId, $taskId, $contactId, $completeDate, $completeTime);
    }

    function updateTaskAgainstContact()
    {
        $task = new task();
        $taskId = $_POST['taskId'];
        $userId = $_POST['userId'];
        $contactId = $_POST['contactId'];
        //echo $result=$task->updateTaskAgainstContact($taskId,$contactId,$userId);
        $completeDate = '0000-00-00';
        $completeTime = '00:00:00';
        echo $result = $task->markTaskCompleted($userId, $taskId, $_POST['contactId'], $completeDate, $completeTime);
    }

    function searchInfo()
    {
        $contact = new contact();
        $search = $_POST['searchdata'];
        $userId = $_POST['userId'];
        $data = $contact->getSearchContact($userId, $search);
        if (!empty($data))
        {
            foreach ($data as $row)
            {
                $id = $row->contactId;
                $gmtDiff = $row->gmt;
                //$contactAddress=$contact->showContactAddress($id);
                //$offset=$contactAddress->utc;
                //$contactPermissionManager=$contact->showContactpermission($id);
                $profiles = $contactemailStep = $contact->countEmailStepOfContact($id);
                //$contactProfiles=$contact->getContactProfiles($id);
                $contactProfiles = $row->profiles;
                $date = date('g:i a');
                $offset = $gmtDiff * 60 * 60; //converting 5 hours to seconds.
                $timeFormat = "g:i a";
                $currentTime = gmdate($timeFormat, time() + $offset);
                //$currentTime=gmdate("g:i a", strtotime($date) + (3600 * $offset) );
                $client = strtolower("CLIENT");
                $interest = str_replace($client, "PRODUCTS", strtolower($row->interest));
                $matched = false;
                if (strstr($interest, 'team leader'))
                {
                    $matched = true;
                    $interest = str_replace(strtolower('TEAM LEADER,'), "IC,", $interest);
                    $interest = str_replace(strtolower(',TEAM LEADER'), ",IC", $interest);
                    $interest = str_replace(strtolower('TEAM LEADER'), "IC", $interest);
                }
                if ((strstr($interest, 'sales consultant')) || (strstr($interest, 'SALES CONSULTANT')))
                {
                    if ($matched)
                    {
                        $interest = str_replace(strtolower('SALES CONSULTANT'), "", $interest);
                    }
                    else
                    {
                        $matched = true;
                        $interest = str_replace(strtolower(',SALES CONSULTANT'), ",IC", $interest);
                        $interest = str_replace(strtolower('SALES CONSULTANT,'), "IC,", $interest);
                        $interest = str_replace(strtolower('SALES CONSULTANT'), "IC", $interest);
                    }
                }
                if (!$matched)
                {
                    $interest = str_replace(strtolower('INDEPENDENT CONSULTANT'), "IC", $interest);
                }
                else
                {
                    $interest = str_replace(strtolower(',INDEPENDENT CONSULTANT'), "", $interest);
                    $interest = str_replace(strtolower('INDEPENDENT CONSULTANT,'), "", $interest);
                    $interest = str_replace(strtolower('INDEPENDENT CONSULTANT'), "", $interest);
                }
                $interest = str_replace(strtolower(',,'), ",", $interest);
                $interest = str_replace(strtolower(', ,'), ",", $interest);
                if (trim($interest) == 'IC,')
                {
                    $interest = str_replace(",", "", $interest);
                }
                //$interest=str_replace(strtolower('TEAM LEADER'),"TL",$interest);
                //$interest=str_replace(strtolower('SALES CONSULTANT'),"SC",$interest);
                $interest = str_replace(strtolower('VIRTUAL HOST'), "HOST", $interest);
                $interest = str_replace("PRODUCTS", "CLIENT", $interest);
                $guestProfileDate = $row->addDate;
                if ($guestProfileDate == '' || $guestProfileDate == null)
                {
                    $guestProfileDate = "";
                }
                else
                {
                    $guestProfileDate = date('m/d/Y', strtotime($guestProfileDate));
                }
                echo "<tr class=\"odd_gradeX\" id=\"" . $id . "\">";
                //echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
                echo "<td><input type='checkbox' name='contact[]' class='ads_Checkbox' id='contact'  value=" . $row->contactId . " ></td>";
                echo "<td><a href=contact_detail.php?contactId=" . $id . ">" . $row->firstName . " " . $row->lastName . "</a></td>";
                echo "<td>" . $currentTime . "</td>";
                echo "<td>" . $contactProfiles . "</td>";
                echo "<td>" . strtoupper($interest) . "</td>";
                echo "<td>" . $row->phoneNumber . "</td>";
                echo "<td>" . $guestProfileDate . "</td>";
                echo "<td>";
                echo "<img class='traficLight' src='" . $row->imagePath . "' onclick=" . ($row->statusId != 3 ? "sendPermission('$id')" : "sendPer('$id')") . " title='" . $row->title . "' />";
                //echo"<img class='traficLight' src='".($contactPermissionManager->statusId==1 || $contactemailStep!=0 ?"../../images/orange.png":-($contactPermissionManager->statusId==2|| $contactemailStep!=0 ?"../../images/yellow.png":-($contactPermissionManager->statusId==3 ?"../../images/green.png":-($contactPermissionManager->statusId==4 || $contactemailStep!=0 ?"../../images/red.png":''))))."' onclick=".($contactPermissionManager->statusId!=3 || $contactemailStep!=0 ?"sendPermission('$id')":"sendPer('$id')")." title='".($contactPermissionManager->statusId==1 ?"Unprocessed":-($contactPermissionManager->statusId==2?"Pending":-($contactPermissionManager->statusId==3 ?"Subscribed":-($contactPermissionManager->statusId==4 ?"Unsubscribed":''))))."' />";
                echo "</td>";
                echo "<td></td>";
                echo "</tr>";
            }
        }
        else
        {
            echo "<tr><td colspan='8' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
        }
    }

    function searchPermissionManagerContact()
    {
        $contact = new contact();
        $search = $_POST['searchdata'];
        $userId = $_POST['userId'];
        $data = $contact->getSearchContactForPermissionManager($userId, $search);
        if (!empty($data))
        {
            foreach ($data as $row)
            {
                $id = $row->contactId;
                $status = $contact->GetContactPermissionStatus($id);
                if (empty($status))
                {
                    $cStatus = "Unprocessed";
                }
                else
                {
                    $cStatus = $status;
                }
                echo "<tr class=\"odd_gradeX\" id=\"permission" . $id . "\">";
                echo "<td><input type='checkbox' name='contact[]' class='ads_Checkbox' id='contact[]' value=" . $row->contactId . " ></td>";
                echo "<td><a href=contactdetail.php?id=" . $id . ">" . $row->firstName . " " . $row->lastName . "</a></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td class='status" . $row->contactId . "'>" . $cStatus . "</td>";
                if ($cStatus == 'Subscribed')
                {
                    echo "<td><img src='../../images/unsubscribe.png' class='Unsubscribed' id='" . $row->contactId . "' width='20px' height='20px' alt='Turn it to Unsubscribe' title='Turn it to Unsubscribe' /></td>";
                }
                echo "<td></td>";
                echo "</tr>";
            }
        }
        else
        {
            echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
        }
    }

    function restoreContact()
    {
        $contact = new contact();
        $contactId = $_POST['contactId'];
        echo $result = $contact->restoreContact($contactId);
    }

    function deleteContact()
    {
        $contact = new contact();
        $contactId = $_POST['contactId'];
        echo $result = $contact->deleteOneContact($contactId);
    }

    function restoreAllContacts()
    {
        $contactId = $_GET['contactId'];
        $contact = new contact();
        echo $rresult = $contact->restoreAllContacts($contactId);
    }

    function deleteAllContacts()
    {
        $contactId = $_GET['contactId'];
        $contact = new contact();
        echo $result = $contact->deleteAllContacts($contactId);
    }

    function archiveMultipleContacts()
    {
        $contactId = $_GET['contactId'];
        $contact = new contact();
        echo $rresult = $contact->archiveMultipleContacts($contactId);
    }

    function checkLeaderSignupName($userId)
    {
        $settings = new settings();
        echo $settings->getUserForSignupCheck($_POST['us'], $userId);
    }

    function saveLeaderSignupName()
    {
        $settings = new settings();
        echo $settings->saveLeaderSignupName($_POST['us']);
    }

    function saveLeaderDefaultFields()
    {
        $settings = new settings();
        echo $settings->saveLeaderDefaultFields($_POST['us'], $_POST['id']);
    }

    function toggleLeaderDefaultFields()
    {
        $settings = new settings();
        echo $settings->toggleLeaderDefaultFields($_POST['id'], $_POST['legend']);
    }

    function toggleLeaderQuickLinks()
    {
        $settings = new settings();
        echo $settings->toggleLeaderQuickLinks($_POST['id']);
    }

    function getLeaderQuestionDetails()
    {
        $settings = new settings();
        echo $settings->getLeaderQuestionDetails($_POST['id']);
    }

    function updateDefaultFieldOrder()
    {
        $settings = new settings();
        echo $settings->updateDefaultFieldOrder($_POST['id']);
    }

    function updateCustomQuestionsOrder()
    {
        $settings = new settings();
        echo $settings->updateCustomQuestionsOrder($_POST['id']);
    }

    function updateProfileOrder()
    {
        echo ' in updateprofileorder';
        $profiles = new profiles();
        echo $profiles->updateProfileOrder($_POST['id']);
    }
    function updatePlanStepOrder(){

        $plan = new plan();
        echo $plan->updatePlanStepOrder($_POST['id']);
    }

    function checkProfileNameAvailability()
    {
        $profiles = new profiles();

        return $profiles->checkProfileNameAvailability($_POST);
    }

    function getDefaultTemplateValues()
    {
        $profiles = new profiles();
        $database = new database();
        $result = $profiles->getProfileDetails($database, $_POST['id']);
        if ($result) {
            $id = ($_SESSION['leaderId']) ? $_SESSION['leaderId'] : $_SESSION['userId'];
            $defaults = $database->executeObject("SELECT * FROM `leadertemplatedefaults` WHERE `leaderId` = '".$id."'");
            if ($defaults) {
                $result->profileDefaultText = $defaults->defaultText;
                $result->profileVideoLink = $defaults->defaultProfileVideo;
                $result->profileHeader = $defaults->defaultProfileHeader;
                $result->thankyouVideoLink = $defaults->defaultThankYouVideo;
                $result->thankyouHeader = $defaults->defaultThankYouHeader;
            }
        }
        //print_r($result);
        return implode(', ', $result);
    }

    function updatePageQuestiontFieldOrder()
    {
        $profiles = new profiles();
        $database = new database();
        $profiles->updateQuestionsOrder($database, $_POST['order'], $_POST['orderStart']);
    }

    function togglePageQuestions()
    {
        $profiles = new profiles();
        $database = new database;
        $profiles->toggleQuestions($database, $_POST['id'], 'togglePageQuestions');
    }

    function toggleCustomRuleQuestion()
    {
        $profiles = new profiles();
        $database = new database;
        $profiles->toggleQuestions($database, $_POST['id'], 'toggleCustomRuleQuestion');
    }

    function archivePlan()
    {
        $plan = new plan();
        $database = new database();
        $plan->archivePlan($_POST['planId']);
        $notification = new notification();
        if ($userid == $leaderId)
        {
            echo $notification->notificationToUser($database, $_REQUEST['userId'], $_REQUEST['planId'], 3, 3, "Do you want to Archive this Plan \'" . $_REQUEST['planName'] . "\'");
        }
    }

    function publishPlan($userId, $leaderId)
    {
        $plan = new plan();
        $database = new database();
        $notification = new notification();
        $database = new database();
        $plan->publishPlanleader($database, $_POST['planId']);
        $notification->notificationToUser($database, $leaderId, $_REQUEST['planId'], 3, 1, "Do you want to publish Plan \'" . $_REQUEST['planName'] . "\'");
        echo 'done';
    }

    function editPlan($userId, $leaderId)
    {
        $plan = new plan();
        $database = new database();
        $notification = new notification();

        $plan->publishPlanleader($database, $_POST['planId']);
        $notification->notificationToUser($database, $leaderId, $_REQUEST['planId'], 3, 2, "Do you want to publish Plan \'" . $_REQUEST['planName'] . "\'");
        echo 'done';
    }

    function archiveStep($userId, $leaderId)
    {
//		$plan = new plan();
        $database = new database();
        $notification = new notification();
//		$plan->archiveStep($database, $_REQUEST['stepId']);
        $notification->markarchive($database, $_REQUEST['stepId'], 5, 0);
        if ($userId == $leaderId)
        {
            $notification->notificationToUser($database, $userId, $_REQUEST['stepId'], 5, 3, "Do you want to archive step \'" . $_REQUEST['stepName'] . "\'");
        }
    }

    function archiveGroup($userId, $leaderId)
    {
        $group = new group();
        $database = new database();
        $notification = new notification();
        $group->archiveGroups($_REQUEST['groupId'], $_REQUEST['userId']);
        if ($userId == $leaderId)
        {
            $notification->notificationToUser($database, $userId, $_REQUEST['groupId'], 2, 3, "Do you want to archive the Group \'" . $_REQUEST['groupName'] . "\'");
        }
    }

    function NotNowRequest()
    { //by me
        $plan = new plan();
        $database = new database();
        $notification = new notification();
        $notification->changeUserNotification($database, $plan, $_REQUEST["id"], 1, "Published");
    }

    function ConfirmRequest()
    { //by me
        $plan = new plan();
        $database = new database();
        $notification = new notification();
        $notification->changeUserNotification($database, $plan, $_REQUEST["id"], 2, 1);
        //database object , planId/groupId/profileId ,status 2 for confirm , action 1 for add/2 for edit/3 for archive
    }

    function confirmForstep($userId)
    {

        $plan = new plan();
        $database = new database();

        $checkStepStatus = $plan->checkStepStatus($database, $_REQUEST["id"], $userId);
        //database object , planId/groupId/profileId ,status 2 for confirm , action 1 for add/2 for edit/3 for archive
    }

    function publishGroup($userId, $leaderId, $generatedVia)
    {
        $group = new group();
        $database = new database();
        $notification = new notification();
        $group->publishGroupForown($database, $_POST['groupId'], $generatedVia);
        $notification->notificationToUser($database, $leaderId, $_REQUEST['groupId'], 2, 1, "Do you want to publish the Group \'" . $_REQUEST['groupName'] . "\'");
    }

    function unArchive() //by mansoor
    {
        $groupId = $_REQUEST["groupId"];
        $group = new group();
        $group->unArchive($groupId);
    }

    function publishProfile($user, $leaderId)
    {
        $notification = new notification();
        $database = new database();
        $Profiles = new Profiles();
        $profileId = $_REQUEST['profileId'];
        $newCode = $_REQUEST['newCode'];
        $markProfilePublish = $Profiles->publishProfile($database, $profileId);
        $notification->notificationToUser($database, $leaderId, $profileId, 1, 1, "Do you want to publish profile " . $newCode);
    }

    function archiveProfile($leaderId, $userId)
    {
        $database = new database();
        $notification = new notification();
        $notification->markarchive($database, $_REQUEST['profileId'], 1, 0);
        $profileName = $_REQUEST['pageName'];
        $profileId = $_REQUEST['profileId'];
        if ($userId == $leaderId)
        {
            $notification->notificationToUser($database, $leaderId, $profileId, 1, 3, "Do you want to archive profile " . $profileName);
        }
        //		$notification = new notification();
        //		$database = new database;
        //		echo $notification->notificationToUser($database, $leaderId, $_REQUEST['profileId'], 1, 3, "Do you want to archive profile " . $_REQUEST['pageName']);
    }

    function discardProfile($leaderId, $userId)
    {
        $database = new database();
        $profiles = new profiles();
        $profiles->markDiscard($database, $_REQUEST['profileId'], $_REQUEST['pageName'], $userId);


    }

    function editProfileNotification($userId, $leaderId)
    {
        $changeId = $_POST['changeId'];
        $profileName = $_POST['profileName'];
        $database = new database();
//		profiles = new profiles();
//		echo $profileName=$profiles->getProfileName($database,$changeId);

        $notification = new notification();
        if ($userId == $leaderId)
        {
            echo $notification->notificationToUser($database, $leaderId, $changeId, 1, 2, "Do you want to edit profile " . $profileName);
        }
    }

    function markArchive()
    {
        $database = new database();
        $notification = new notification();
        $notification->markArchive($database, $_REQUEST['id'], $_REQUEST['type'], 1);
    }

    function markunArchive($userId)
    {
        $database = new database();
        $notification = new notification();
        $notification->markunArchive($database, $_REQUEST['id'], $_REQUEST['type'], 1, $userId);
    }

    function toggleDefaultQuestions()
    {
        $profiles = new profiles();
        $database = new database;
        $profiles->toggleQuestions($database, $_POST, 'toggleDefaultQuestions');
    }
    function RequestAllowsignup ($status,$userId){
        $database = new database();
        $settings = new settings();

        $settings->RequestAllowsignup($database,$status,$userId);


    }
function editVideoForVENVPsTemplate($editedVideo,$pageId,$userId){
    $database = new database();
    $profiles = new profiles();

    $profiles-> editVideoForVENVPsTemplate($database,$editedVideo,$pageId,$userId);

}
function isOntraportLeader($leaderId){

    $reg = new register();
    echo $reg->isOntraportLeader($leaderId);
}
function deleteFile($stepId){

  $plan= new plan();

    $plan->deleteFile($stepId);
}
function getDaysAfter($stepId){
    $database = new database();
     $daysAfter = $database->executeObject("SELECT `daysAfter` from plansteps where stepId='".$stepId."'and `isTask`='0'");
    $dueDate= date('Y-m-d', strtotime("+".$daysAfter->daysAfter." days"));
   $stepExecutionDate= date("d-M-Y", strtotime($dueDate));

        echo $stepExecutionDate;

}
?>
