<?php
	require_once('init.php');
	//require_once("profiles.php");
	include_once("class.smtpmail.php");

	class contact {

		/********************** imran Code******************/
		function GetUserDetails($userId) {
			$database = new database();

			return $database->executeObject("select * from `user` where `userId` ='" . $userId . "'");
		}

		function GetUserContacts($user_id) {
			$database = new database();
			$contacts = $database->executeObjectList("SELECT * FROM contact where userId='" . $user_id . "' AND isActive=1 order by firstName ");

			return $contacts;
		}

		function showContact() {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contact");
		}

		//phone number for contact
		public function addContactPhoneNumberByContactId1($contactId, $phone) {
			$database = new database();
			($phone[2] == 'on' ? $phoneNumberIsPrimary = 1 : $phoneNumberIsPrimary = 0);
			$phoneaddedDate = date("Y-n-j");
			$database->executeNonQuery("INSERT INTO contactphonenumbers
									SET contactId='" . $contactId . "',
										typeId='" . $phone[0] . "',
										phoneNumber='" . $phone[1] . "',
										isPrimary='" . $phoneNumberIsPrimary . "',
										addedDate='" . $phoneaddedDate . "'");
			$database->executeNonQuery("UPDATE contact SET phoneNumber='" . $phone[1] . "' WHERE contactId='" . $contactId . "'");
		}

		public function addContactEmailByContactId1($contactId, $email) {
			$database = new database();
			($email[2] == 'on' ? $emailIsPrimary = 1 : $emailIsPrimary = 0);
			$emailaddedDate = date("Y-n-j");
			$database->executeNonQuery("INSERT INTO emailcontacts
									SET contactId='" . $contactId . "',
									typeId='" . $email[0] . "',
										email='" . $email[1] . "',
										isPrimary='" . $emailIsPrimary . "',
										addedDate='" . $emailaddedDate . "'");
			$database->executeNonQuery("UPDATE contact SET email='" . $email[1] . "' WHERE contactId='" . $contactId . "'");
		}

		public function addContactWebsiteByContactId1($contactId, $website) {
			$database = new database();
			($website[2] == 'on' ? $websiteIsPrimary = 1 : $websiteIsPrimary = 0);
			$websiteaddedDate = date("Y-n-j");
			$database->executeNonQuery("INSERT INTO contactwebsites
									SET contactId='" . $contactId . "',
										typeId='" . $website[0] . "',
										website='" . $website[1] . "',
										isPrimary='" . $websiteIsPrimary . "',
										addedDate='" . $websiteaddedDate . "'");
		}

		//Address detail for contact
		public function addContactAddressByContactId1($contactId, $address) {
			$database = new database();
			$jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address[1] . ',+' . $address[4] . ',+' . $address[3] . ',+' . $address[2] . ',+' . $address[5]) . '&sensor=true');
			$obj = json_decode($jsondata, true);
			$lat = $obj['results'][0]['geometry']['location']['lat'];
			$long = $obj['results'][0]['geometry']['location']['lng'];
			$jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');
			$obj2 = json_decode($jsondata2, true);
			$offset = $obj2['rawOffset'];
			$gmtDifference = ($offset / 60) / 60;
			$addressaddedDate = date("Y-n-j");
			$database->executeNonQuery("INSERT INTO contactaddresses
									SET contactId='" . $contactId . "',
										typeId='" . $address[0] . "',
										street='" . mysql_real_escape_string($address[1]) . "',
										zipcode='" . $address[2] . "',
										state='" . mysql_real_escape_string($address[3]) . "',
										city='" . mysql_real_escape_string($address[4]) . "',
										country='" . mysql_real_escape_string($address[5]) . "',
										gmt='" . $gmtDifference . "',
										addedDate='" . $addressaddedDate . "'");
			$database->executeNonQuery("UPDATE contact SET street='" . mysql_real_escape_string($address[1]) . "',
										zipcode='" . $address[2] . "',
										state='" . mysql_real_escape_string($address[3]) . "',
										city='" . mysql_real_escape_string($address[4]) . "',
										country='" . mysql_real_escape_string($address[5]) . "',
										gmt='" . $gmtDifference . "' WHERE contactId='" . $contactId . "'");
		}

		//Assign Group to Contact
		function assignGroupTo1($contactId, $groups) {
			$database = new database();
			$count = sizeof($groups['groupcheckbox']);
			$group = 0;
			while ($count > 0) {
				$database->executeNonQuery("INSERT INTO groupcontacts
										SET groupId='" . $groups['groupcheckbox'][$group] . "',
											contactId='" . $contactId . "'");
				$count--;
				$group++;
			}
		}

		//Assign Plan Steps to Contact
		function assignPlanStep1($contactId, $planSteps) {
			$database = new database();

			return $count = sizeof($planSteps['planStepDropDown']);
			$step = 0;
			while ($count > 0) {
				$database->executeNonQuery("INSERT INTO planstepusers
										SET stepId='" . $planSteps['planStepDropDown'][$step] . "',
											contactId='" . $contactId . "'");
				$count--;
				$step++;
			}
		}

		public function showCustomFields1($userId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM usercontactcustomdata WHERE userId='" . $userId . "'");
		}

		public function showCustomFieldsById1($userId, $customFieldId) {
			//return $database->executeObject
			$database = new database();

			return ("SELECT * FROM usercontactcustomdata WHERE userId='" . $userId . "' AND customDataId='" . $customFieldId . "'");
		}

		/*contact details	*/
		function showContactDetail1($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");
		}

		function showContactPhoneNumber1($contactId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contactphonenumbers WHERE contactId='" . $contactId . "'");
		}

		function showContactEmailAddress1($contactId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM emailcontacts WHERE contactId='" . $contactId . "'");
		}

		function showContactWebsite1($contactId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='" . $contactId . "'");
		}

		function showContactAddress1($contactId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contactaddresses WHERE contactId='" . $contactId . "'");
		}

		function GetAllContacts($userId) {
			$database = new database();
			/*	$contacts=$database->executeObjectList("SELECT
			c.contactId, firstName,lastName,c.phoneNumber,interest
		FROM
			contact c

			LEFT JOIN ( SELECT cp.phoneNumber, cp.contactId
				FROM contactphonenumbers as cp
				LEFT JOIN contactphonenumbers AS cp2
					 ON cp.contactId = cp2.contactId AND cp.addedDate < cp2.addedDate
				) as contactphonenumbers_tmp

			ON (c.contactId = contactphonenumbers_tmp.contactId) where userId='".$userId."'  AND c.isActive=1 order by firstName");*/
			/*$sqlOptimized = "SELECT
				DISTINCT(c.contactId),cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
			FROM
				contact c

				LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
					) AS gpr ON c.contactId=gpr.contactId
				LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

				LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
				INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
				AS cpr ON c.contactId = cpr.tocontactId

				LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
				 LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
				WHERE c.userId='".$userId."'  AND c.isActive=1 ORDER BY c.firstName ";


				 $sqlNew = "SELECT
				DISTINCT(c.contactId),cas.gmt,cpr.statusId,c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
			FROM
				contact c

				LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
					) as gpr ON c.contactId=gpr.contactId
				LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
				LEFT JOIN contactpermissionrequests as cpr on c.contactId = cpr.tocontactId
				LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply
				 where c.userId='".$userId."'  AND c.isActive=1 order by c.firstName limit 5 ";

				 $sqlOriginal = "SELECT
				c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
			FROM
				contact c

				LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
					) as gpr ON c.contactId=gpr.contactId
				LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply
				 where c.userId='".$userId."'  AND c.isActive=1 order by c.firstName limit 5";*/
			$sqlUnchecked = "SELECT
    DISTINCT(c.contactId),c.profiles,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId 
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply
	
	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1 
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId 
	
	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
    WHERE c.userId='" . $userId . "'  AND c.isActive=1 ORDER BY c.firstName ";

			$sql = "SELECT DISTINCT(c.contactId),c.profiles, cas.gmt, cas.addressId, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest,  pfr1.addDate,c.`addedDate` AS contactAddDate FROM
    contact c
    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId  ) AS gpr ON c.contactId=gpr.contactId
    LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply
    LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId
	FROM contactpermissionrequests
	GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`) AS cpr ON c.contactId = cpr.tocontactId
	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN (SELECT MAX(ca.`addressId`) AS addressId, contactId FROM contactaddresses ca GROUP BY ca.`contactId`) AS casc ON c.contactId = casc.contactId
	LEFT JOIN contactaddresses cas ON casc.addressId = cas.`addressId`
    WHERE c.userId = '" . $userId . "'  AND c.isActive=1 ORDER BY c.firstName";

			$contacts = $database->executeObjectList($sql);

			return $contacts;
			//echo "SELECT * FROM contact where contactId='".$userId."'";
		}

		function DeleteContact($id) {
			$database = new database();
			$database->executeNonQuery("delete from contact where contactId='" . $id . "'");
		}

		function AddNewContact($postArray) {
			//Add contact personal info
			$title = $postArray['title'];
			$firstname = $postArray['firstname'];
			$lastname = $postArray['lastname'];
			$companyname = $postArray['companyname'];
			$jobtitle = $postArray['jobtitle'];
			$birthday = $postArray['birthday'];
			$anniversary = $postArray['anniversary'];
			$Notes = $postArray['Notes'];
			$isActive = "1";
			$user_id = "1";
			if (!empty($postArray)) {
				$database = new database();
				$database->executeNonQuery("INSERT INTO contact
								 	SET
									userId='" . $user_id . "',
									firstName='" . mysql_real_escape_string($firstname) . "',
									lastName='" . mysql_real_escape_string($lastname) . "',
									title='" . $title . "',
									companyTitle='" . mysql_real_escape_string($companyname) . "',
									jobTitle='" . mysql_real_escape_string($jobtitle) . "',
									dateOfBirth='" . $birthday . "',
									anniversary='" . $anniversary . "',
									addedDate=now(),
									notes='" . $Notes . "',
									isActive='" . $isActive . "'
					");
			}
			$contact_id = $database->insertid();
			$phoneNumbers = $postArray['numberOfPhoneNumbers'];
			if ($phoneNumbers == 0) {
			}
			else {
				$numberOfPhoneNumbers = explode(',', $phoneNumbers);
				for ($p = 0; $p <= sizeof($numberOfPhoneNumbers); $p++) {
					$phoneDetail = array($postArray['phoneType_' . $numberOfPhoneNumbers[$p]],
						$postArray['number_' . $numberOfPhoneNumbers[$p]],
						$postArray['phoneNumberPrimary' . $numberOfPhoneNumbers[$p]]);
					contacts::addContactPhoneNumberByContactId($contact_id, $phoneDetail);
				}
			}
			$emailAddress = $postArray['numberOfEmailAddress'];
			$numberOfEmailAddress = explode(',', $emailAddress);
			for ($e = 0; $e < sizeof($numberOfEmailAddress); $e++) {
				$emailDetail = array($postArray['emailType_' . $numberOfEmailAddress[$e]],
					$postArray['emailaddress_' . $numberOfEmailAddress[$e]],
					$postArray['primaryEmail_' . $numberOfEmailAddress[$e]]);
				//print_r($emailDetail);
				contacts::addContactEmailByContactId($contact_id, $emailDetail);
			}
			$websiteAddress = $postArray['numberOfWebsiteAddress'];
			$numberOfWebsiteAddress = explode(',', $websiteAddress);
			for ($w = 0; $w < sizeof($numberOfWebsiteAddress); $w++) {
				$websiteDetail = array($postArray['websiteType_' . $numberOfWebsiteAddress[$w]],
					$postArray['websiteAddress_' . $numberOfWebsiteAddress[$w]],
					$postArray['primaryWebsiteAddress_' . $numberOfWebsiteAddress[$w]]);
				//print_r($emailDetail);
				$contact->addContactWebsiteByContactId($contact_id, $websiteDetail);
			}
			$Address = $postArray['numberOfAddress'];
			$numberOfAddress = explode(',', $Address);
			for ($a = 0; $a < sizeof($numberOfAddress); $a++) {
				$addressDetail = array($postArray['addressType_' . $numberOfAddress[$a]],
					$postArray['street_' . $numberOfAddress[$a]],
					$postArray['zipCode_' . $numberOfAddress[$a]],
					$postArray['state_' . $numberOfAddress[$a]],
					$postArray['city_' . $numberOfAddress[$a]],
					$postArray['country_' . $numberOfAddress[$a]]
				);
				//print_r($addressDetail);
				$contact->addContactAddressByContactId($contact_id, $addressDetail);
			}
		}

		/*
		function addContactPhoneNumberByContactId($contactId,$phone)
		{
			$database = new database();
			($phone[2]=='on'?$phoneNumberIsPrimary=1:$phoneNumberIsPrimary=0);
			$phoneaddedDate		=date("Y-n-j");
			$database = new database();
			$database->executeNonQuery("INSERT INTO contactphonenumbers
										SET contactId='".$contactId."',
											typeId='".$phone[0]."',
											phoneNumber='".$phone[1]."',
											isPrimary='".$phoneNumberIsPrimary."',
											addedDate='".$phoneaddedDate."'");
		}
		function addContactEmailByContactId($contactId,$email)
		{
			$database = new database();
			($email[2]=='on'?$emailIsPrimary=1:$emailIsPrimary=0);
			$emailaddedDate		=date("Y-n-j");

			$database->executeNonQuery("INSERT INTO emailcontacts
										SET contactId='".$contactId."',
										typeId='".$email[0]."',
											email='".$email[1]."',
											isPrimary='".$emailIsPrimary."',
											addedDate='".$emailaddedDate."'");
		}

		function addContactWebsiteByContactId($contactId,$website)
		{
			$database = new database();
			($website[2]=='on'?$websiteIsPrimary=1:$websiteIsPrimary=0);
			$websiteaddedDate	=date("Y-n-j");
			$database->executeNonQuery("INSERT INTO contactwebsites
										SET contactId='".$contactId."',
											typeId='".$website[0]."',
											website='".$website[1]."',
											isPrimary='".$websiteIsPrimary."',
											addedDate='".$websiteaddedDate."'");
		}
		//Address detail for contact

		function addContactAddressByContactId($contactId,$address)
		{
			$database = new database();
			$addressaddedDate	=date("Y-n-j");
			$database->executeNonQuery("INSERT INTO contactaddresses
										SET contactId='".$contactId."',
											typeId='".$address[0]. "',
											street='".$address[1]. "',
											zipcode='".$address[2]. "',
											state='".$address[3]. "',
											city='".$address[4]. "',
											country='".$address[5]. "',
											addedDate='".$addressaddedDate."'");
		}
		*/
		function GetContactDetail($contact_id) {
			$database = new database();
			$detail = $database->executeObject("SELECT * FROM contact WHERE contactId=" . $contact_id . "");

			return $detail;
		}

		function GetContactPhoneNumbers($contact_id) {
			$database = new database();
			$detail = $database->executeObjectList("SELECT * FROM contactphonenumbers WHERE contactId='" . $contact_id . "'");

			return $detail;
		}

		function GetContactEmailAddress($contact_id) {
			$database = new database();
			$detail = $database->executeObjectList("SELECT * FROM emailcontacts WHERE contactId='" . $contact_id . "'");

			return $detail;
		}

		function GetContactWebsites($contact_id) {
			$database = new database();
			$detail = $database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='" . $contact_id . "'");

			return $detail;
		}

		function GetContactUserAddress($contact_id) {
			$database = new database();
			$detail = $database->executeObjectList("SELECT * FROM contactaddresses WHERE contactId='" . $contact_id . "'");

			return $detail;
		}

		function GetAllPermissions() {
			$database = new database();
			$detail = $database->executeObjectList("SELECT * FROM contactpermissionrequeststatus");

			return $detail;
		}

		function GetContactPermissionStatus($contact_id) {
			$database = new database();
			$detail = $database->executeObject("select cpr.statusId,cprs.title as title
												from contactpermissionrequests cpr
												left join contactpermissionrequeststatus cprs on cpr.statusId=cprs.statusId 
												where cpr.toContactId='" . $contact_id . "' order by permissionRequestId desc LIMIT 1"
			);

			//return $detail;
			return $detail->title;
		}

        function AddTransferContact($user_id, $contacts, $Suser)
        {
            //$Suser is send request user
            $database = new database();
            $requestDate = date("Y-m-d");
            $userToSend = preg_replace('/[^\-\d]*(\-?\d*).*/', '$1', $user_id);
            $duplicateContacts = 0;
            foreach ( $contacts as $id => $val ) {

                $contact = $database->executeObject("SELECT * FROM `contacttransferrequest` WHERE `userId` = '".$Suser."' AND `contactId`='" . $val . "' AND `requestToUserId`='" . $userToSend . "' AND `statusId`='1'");
                if (count($contact) == 0) {
                    $database->executeNonQuery("INSERT INTO contacttransferrequest SET `userId`='" . $Suser . "',`contactId`='" . $val . "',`requestToUserId`='" . $userToSend . "',`requestedDate`='" . $requestDate . "',`statusId`='1',`approvedDate`='',`rejectedDate`='',`newContactId`=''");
                } else {
                    $duplicateContacts++;                 
                }
            }
            return $duplicateContacts;
        }

		function GetAllTransferContacts($user_id) {
			$database = new database();
			$contacts = $database->executeObjectList("SELECT * FROM contacttransferrequest where requestToUserId='" . $user_id . "' and statusId='1'");

			return $contacts;
		}

        function  getTransferByName($userId)
        {
            $database = new database();

            $transferedByName = $database->executeObject("SELECT firstName,lastName FROM `user` where userId='" . $userId . "'");

            return $transferedByName;
        }

        function GetContactName($contact_id)
        {
            $database = new database();
            //$detail=$database->executeObjectList("SELECT * FROM contact WHERE contactId='".$contact_id."'");
            $detail = $database->executeObject("SELECT firstName ,lastName,email,phoneNumber  FROM contact WHERE contactId='" . $contact_id . "'");

			return $detail;
		}

		function GetUserName($user_id) {
			$database = new database();
			//$detail=$database->executeObjectList("SELECT * FROM contact WHERE contactId='".$contact_id."'");
			$detail = $database->executeObject("SELECT firstName,lastName,email FROM user WHERE userId='" . $user_id . "'");

			return $detail;
		}

        function AcceptTransferContact($contactId, $user_id)
        {


            $database = new database();
            $approveDate = date("Y-m-d");
            
            $transferBy = $database->executeObject("SELECT * FROM `contacttransferrequest` WHERE `requestToUserId` = '" . $_SESSION['userId'] . "' AND `contactId` = '" . $contactId . "' AND `statusId` = '1'");
            // record from contact transfer request
            $userInfo = $database->executeObject("SELECT * FROM `user` WHERE `userId` = '" . $_SESSION['userId'] . "'");

            $transferByUser = $database->executeObject("SELECT * FROM `user` WHERE `userId` = '" . $transferBy->userId . "'");
            // transfer by detail

            $getExitingEmail = $database->executeScalar("SELECT email FROM contact WHERE contactId='" . $contactId . "'");

            $isNewEmail = $database->executeScalar("SELECT COUNT(*) FROM contact WHERE userId='" . $user_id . "' AND email='" . $getExitingEmail . "' AND isActive = '1'");
            
            //SELECT * FROM source_table WHERE id='7';
            //UPDATE temp_table SET id='100' WHERE id='7';
            //INSERT INTO source_table SELECT * FROM temp_table;
            //DROP TEMPORARY TABLE temp_table;
            if ( $isNewEmail == 0 ) {

                $database->executeNonQuery("INSERT INTO contact (userId, firstName,lastName,title,companyTitle,
          			jobTitle,dateOfBirth,notes,isActive,email,phoneNumber,weekdays, weekdayEvening, weekenddays, weekenddayEvening, street, zipcode, state, city, country, gmt,profiles )
          			select '" . $user_id . "' as id, firstName,lastName,title,companyTitle,jobTitle,dateOfBirth,notes,isActive,email,phoneNumber,weekdays, weekdayEvening, weekenddays, weekenddayEvening, street, zipcode, state, city, country, gmt,profiles
          			from contact where contactId='$contactId'");
                $newContactId = $database->insertid();

                $database->executeNonQuery("INSERT INTO contactphonenumbers(contactId, typeId, phoneNumber, isPrimary, addedDate)
			 		select '" . $newContactId . "' as id, typeId, phoneNumber, isPrimary, CURDATE() as adddate from contactphonenumbers where contactId='$contactId'");

                $database->executeNonQuery("INSERT INTO `contactpermissionrequests` (userId, toContactId, emailId, requestDate, statusId)
                    select '" . $user_id . "','" . $newContactId . "', emailId, CURDATE(), statusId from `contactpermissionrequests` where toContactId = '" . $contactId . "'");

                $database->executeNonQuery("INSERT INTO contactnotes( contactId, notes, addedDate, lastModifiedDate, isActive)
			 		select '" . $newContactId . "' as id,  notes, CURDATE() as addeddate, CURDATE() as lastmodifieddate, isActive from contactnotes where contactId='$contactId'");
                $database->executeNonQuery("INSERT INTO contactwebsites(contactId, typeId, website, isprimary, addedDate)
			 		select '" . $newContactId . "' as id,  typeId, website, isprimary, CURDATE() as addeddate from contactwebsites where contactId='$contactId'");

                $database->executeNonQuery("UPDATE  contacttransferrequest SET statusId='2', approvedDate='" . $approveDate . "', newContactId ='".$newContactId."' WHERE contactId='" . $contactId . "'");

                $pageFromReply = $database->executeObjectList("SELECT * FROM `pageformreply` WHERE `contactId` ='" . $contactId . "' GROUP BY `pageId` ");

                $totalPagesGroups = array();
                $totalPagesCompaigns = array();

                foreach ( $pageFromReply as $pfr ) // Checking sendToUser nad TransferByUser relation

                {
                    // Get all page groups
                    $pageGroups = $database->executeObjectList("SELECT DISTINCT groupId FROM `pagecustomrulesdetails` WHERE `pageCustomRulesid` = (SELECT `pageCustomRulesId` FROM `pagecustomrules` WHERE `pageId` = '".$pfr->pageId."') AND `groupId` <> ''");
                    foreach ($pageGroups as $group) {
                        $totalPagesGroups[] = $group->groupId;
                    }

                    // Get all page compaigns
                    $pageCompaigns = $database->executeObjectList("SELECT DISTINCT planId, stepId FROM `pagecustomrulesdetails` WHERE `pageCustomRulesid` = (SELECT `pageCustomRulesId` FROM `pagecustomrules` WHERE `pageId` = '".$pfr->pageId."') AND planId <> ''");
                    foreach ($pageCompaigns as $compaign) {
                        $totalPagesCompaigns[] = $compaign->planId;
                    }

                    if ( $transferByUser->leaderId == $userInfo->userId ) { // userToSend is a leader

                        $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "' AND privacy='1') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                        if ( empty($page->pageId) ) {
                            $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `pageId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                        }

                    } elseif ( $transferByUser->leaderId == 0 ) { // transferByUser is a leader

                        $page = $database->executeObject("SELECT * FROM `pages` WHERE `parentId` = '" . $pfr->pageId . "' AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");

                        if ( empty($page->pageId) ) {

                            $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                        }

                    } else { // Both are users

                        $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                        if ( empty($page->pageId) ) {
                            $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `pageId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                        }

                    }

                    if ( !empty($page) ) {

                        // Copy groups attached to the current profile
                        foreach ($pageGroups as $pageGroup) {
                            $database->executeNonQuery("INSERT INTO groupcontacts SET groupId = '".$pageGroup->groupId."', contactId = '" . $newContactId . "'");
                        }
                        
                        $database->executeNonQuery("INSERT INTO pageformreply  (pageId,firstName,lastName,email,phoneNumber,dateOfBirth,refferedBy,bestTimeToCall,address,city,zipCode,state,country,isContactGenerated,contactId,isCounterSet,`addDate`,userId,viewId,pageURL,uniqueCode)SELECT '" . $page->pageId . "',firstName,lastName,email,phoneNumber,dateOfBirth,refferedBy,bestTimeToCall,address,city,zipCode,state,country,isContactGenerated,'" . $newContactId . "',isCounterSet,`addDate`,'" . $_SESSION['userId'] . "',viewId,pageURL,uniqueCode FROM pageformreply WHERE replyId='" . $pfr->replyId . "'");

                        $newReply = $database->executeObject("SELECT * FROM `pageformreply` WHERE `contactId` = '" . $newContactId . "' AND pageId='".$page->pageId ."'");
                        $questions = $database->executeObjectList("SELECT `questionId` FROM `pagequestions` WHERE `pageId`='" . $newReply->pageId . "'");
                        $oldQuestions = $database->executeRowList("SELECT `questionId` FROM `pagequestions` WHERE `pageId`='" . $pfr->pageId . "'");
                        $i = 0;

                        foreach ( $questions as $question ) {	

							$typeId = $database->executeScalar("SELECT `typeId` FROM `questions` WHERE `questionId` = '".$question->questionId."'");

							if ($typeId > 2) {

                                $oldAnswers = $database->executeObjectList("SELECT `answer` FROM `pageformreplyanswers` WHERE `questionId` = '".$oldQuestions[$i][0]."' AND `replyId` = '".$pfr->replyId."'");
                                
                                foreach($oldAnswers as $oldAnswer) {

                                    if ( $transferByUser->leaderId == $userInfo->userId  ) {
                                        $answer = $database->executeScalar("SELECT `oldId` FROM `pageformquestionchoices` WHERE `choiceId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND questionId = '".$oldQuestions[$i][0]."'");
                                    } elseif ( $transferByUser->leaderId == 0 ) {
                                        $answer = $database->executeScalar("SELECT `choiceId` FROM `pageformquestionchoices` WHERE `oldId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND questionId = '".$question->questionId."'");
                                    } else {
                                        $answer = $database->executeScalar("SELECT `choiceId` FROM `pageformquestionchoices` WHERE `oldId` = (SELECT `oldId` FROM  `pageformquestionchoices` WHERE `choiceId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND `questionId` = '".$oldQuestions[$i][0]."') AND `isActive` = '1' AND `questionId` = '".$question->questionId."'");
                                    }

                                    $database->executeNonQuery("INSERT INTO `pageformreplyanswers` (replyId,questionId,answer,value,contactId,answerText) SELECT '" . $newReply->replyId . "','" . $question->questionId . "', '".$answer."',value,'" . $newContactId . "',answerText FROM `pageformreplyanswers` WHERE `contactId` = '" . $contactId . "' AND replyId ='" . $pfr->replyId . "' AND questionId = '" . $oldQuestions[$i][0] . "'");
                                }

							} else {
								$database->executeNonQuery("INSERT INTO `pageformreplyanswers` (replyId,questionId,answer,value,contactId,answerText) SELECT '" . $newReply->replyId . "','" . $question->questionId . "', answer,value,'" . $newContactId . "',answerText FROM `pageformreplyanswers` WHERE contactId = '" . $contactId . "' AND replyId='" . $pfr->replyId . "' AND questionId = '" . $oldQuestions[$i][0] . "'");
							}
                            $i++;
                        }

                        if ($pageCompaigns) {
                            $planIds = array();
                            $stepIds = array();
                            $planstepusers = $database->executeObjectList("SELECT * FROM `planstepusers` WHERE `contactId` = '".$contactId."'");
                            
                            foreach ($planstepusers as $planstepuser) {

                                foreach ($pageCompaigns as $pageCompaign) {
                                
                                    if ($transferByUser->leaderId == $userInfo->userId) { // userToSend is a leader

                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        if ( empty($planId) ) {
                                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        }
                                        if ($planId) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                            if (empty($stepId)) {
                                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                            }

                                            if (!in_array($planstepuser->stepId, $stepIds)) {
                                                $stepIds[] = $planstepuser->stepId;
                                            }

                                        }
                                        
                                    } elseif ( $transferByUser->leaderId == 0 ) { // transferByUser is a leader

                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = '" . $pageCompaign->planId . "' AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        if ( empty($planId) ) {
                                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        }
                                        if ($planId) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = '".$planstepuser->stepId."' AND `planId` = '".$planId."' AND `isactive`='1'");
                                            if (empty($stepId)) {
                                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                            }

                                            if (!in_array($planstepuser->stepId, $stepIds)) {
                                                $stepIds[] = $planstepuser->stepId;
                                            }
                                        }
                                        
                                    } else {

                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        if ( empty($planId) ) {
                                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                        }
                                        if ($planId) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` =  '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                            if (empty($stepId)) {
                                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                            } 
                                            if (!in_array($planstepuser->stepId, $stepIds)) {
                                                $stepIds[] = $planstepuser->stepId;
                                            }
                                            
                                        }
                                    }

                                    if (!in_array($planId, $planIds)) {
                                        $planIds[] = $planId;
                                    }
                                    
                                    if ($stepId) {

                                        $database->executeNonQuery("INSERT INTO `planstepusers` (contactId, stepId, stepStartDate, stepEndDate) SELECT '".$newContactId."', '".$stepId."', stepStartDate, stepEndDate FROM `planstepusers` WHERE `stepId` = '".$planstepuser->stepId."' AND `contactId` = '".$contactId."'");

                                    }
                                } // END of Inner foreach loop
                            } // END of Outer foreach loop
                        }
                    }
                }
                // Copy groups which are directly attached to the contact
                $totalPagesGroups = implode(',', $totalPagesGroups);
                $database->executeNonQuery("INSERT INTO `groupcontacts` (groupId, contactId) SELECT groupId, '".$newContactId."' FROM `groupcontacts` WHERE `groupId` NOT IN ('".$totalPagesGroups."') AND contactId = '".$contactId."'");

                // Copy compaings attached directly to the contact
                $stepIds = implode(',', $stepIds);
                $planstepusers = $database->executeObjectList("SELECT * FROM `planstepusers` WHERE `contactId` = '".$contactId."' AND stepId NOT IN ('".$stepIds."')");
                        
                foreach ($planstepusers as $planstepuser) {

                    $oldPlanId = $database->executeScalar("SELECT `planId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."' AND `isactive` = '1'");
                    
                    if ($transferByUser->leaderId == $userInfo->userId) { // userToSend is a leader

                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $oldPlanId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        if ( empty($planId) ) {
                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $oldPlanId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        }
                        if ($planId) {
                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                            if (empty($stepId)) {
                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                            }
                        }
                        
                    } elseif ( $transferByUser->leaderId == 0 ) { // transferByUser is a leader

                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = '" . $oldPlanId . "' AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        if ( empty($planId) ) {
                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $oldPlanId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        }
                        if ($planId) {
                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = '".$planstepuser->stepId."' AND `planId` = '".$planId."' AND `isactive`='1'");
                            if (empty($stepId)) {
                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                            }
                        }
                        
                    } else {

                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $oldPlanId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        if ( empty($planId) ) {
                            $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $oldPlanId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                        }
                        if ($planId) {
                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` =  '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                            if (empty($stepId)) {
                                $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                            } 
                        }
                    }

                    if (!in_array($planId, $planIds)) {
                        $planIds[] = $planId;
                    }
                        
                    if ($stepId) {

                        $database->executeNonQuery("INSERT INTO `planstepusers` (contactId, stepId, stepStartDate, stepEndDate) SELECT '".$newContactId."', '".$stepId."', stepStartDate, stepEndDate FROM `planstepusers` WHERE `stepId` = '".$planstepuser->stepId."' AND `contactId` = '".$contactId."'");

                    }
                }

                // Copy contact task
                $contactTasks = $database->executeObjectList("SELECT * FROM `taskcontacts` WHERE `contactId` = '".$contactId."'");
                $index = 0;
                foreach ($contactTasks as $task) {
                    
                    $database->executeNonQuery("INSERT INTO `tasks` (userId,title,isCompleted,priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId, isTransferred) SELECT '".$_SESSION['userId']."',title,isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, '".$planIds[$index]."', endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId, '1' FROM `tasks` WHERE `taskId` = '".$task->taskId."'");
                    $newTaskId = $database->insertid();
                    $database->executeNonQuery("INSERT INTO `taskcontacts` SET `taskId` = '".$newTaskId."', `contactId` = '".$newContactId."'");
                    $index++;
                }
                $database->executeNonQuery("UPDATE `contactemailhistory` SET `isArchive` = '1', `isTransferred` = '1' WHERE `contactId` = '".$newContactId."' AND `userId` = '".$_SESSION['userId']."'");
                
                // Copy contact mail history
                $database->executeNonQuery("INSERT INTO `contactemailhistory` (planId, stepId, contactId, userId, toEmail, fromEmail, `subject`, `body`, `dateTime`)  SELECT planId, stepId, '".$newContactId."', '".$_SESSION['userId']."',toEmail, fromEmail, `subject`, `body`, `dateTime` FROM `contactemailhistory` WHERE `contactId` = '".$contactId."' AND `userId` = '".$transferByUser->userId."'");

                return 1;
            } else {
                return 0;
            }

        }


        // function 'is contact already exist'
        function acceptTransferContactIfconfirm($contactId, $user_id)
        {

            $database = new database();
            $approveDate = date("Y-m-d");

            $transferBy = $database->executeObject("SELECT * FROM `contacttransferrequest` WHERE `requestToUserId` = '" . $_SESSION['userId'] . "' AND `contactId` = '" . $contactId . "' AND `statusId` = '1'");
            // record from contact transfer request
            $transferByUser = $database->executeObject("SELECT * FROM `user` WHERE `userId` = '" . $transferBy->userId . "'");
            // transfer by detail

            // Logged In user information
            $userInfo = $database->executeObject("SELECT * FROM `user` WHERE `userId` = '" . $_SESSION['userId'] . "'");

            $getExitingEmail = $database->executeScalar("SELECT email FROM contact WHERE contactId='" . $contactId . "'");

            //$isNewEmail = $database->executeScalar("SELECT `email` FROM contact WHERE userId='" . $user_id . "' AND email='" . $getExitingEmail . "'");

            // Get current contact Id
            $currentUserContact = $database->executeObject("SELECT * FROM contact WHERE userId='" . $user_id . "' AND email='" . $getExitingEmail . "'");


            $contactIfno = $database->executeObject("select firstName,lastName,title,companyTitle,jobTitle,dateOfBirth,notes,isActive,email,phoneNumber,weekdays, weekdayEvening, weekenddays, weekenddayEvening, street, zipcode, state, city, country, gmt,profiles
          			from contact where contactId='" . $contactId . "'");


            $database->executeNonQuery("UPDATE contact SET
            firstName='" . $contactIfno->firstName . "',
            lastName='" . $contactIfno->lastName . "',
            title='" . $contactIfno->title . "',
            companyTitle='" . $contactIfno->title . "',
            jobTitle='" . $contactIfno->jobTitle . "',
            dateOfBirth='" . $contactIfno->dateOfBirth . "',
            notes='" . $contactIfno->notes . "',
            isActive='" . $contactIfno->isActive . "',
            phoneNumber='" . $contactIfno->phoneNumber . "',
            weekdays='" . $contactIfno->weekdays . "',
            weekdayEvening='" . $contactIfno->weekdayEvening . "',
            weekenddays='" . $contactIfno->weekenddays . "',
            weekenddayEvening='" . $contactIfno->weekenddayEvening . "',
            street='" . $contactIfno->street . "',
            zipcode='" . $contactIfno->zipcode . "',
            state='" . $contactIfno->state . "',
            city='" . $contactIfno->city . "',
            country='" . $contactIfno->country . "',
            gmt='" . $contactIfno->gmt . "',
            profiles='" . $contactIfno->profiles . "' WHERE contactId = '" . $currentUserContact->contactId . "'");


            $newContactId = $currentUserContact->contactId;

            $database->executeNonQuery("INSERT INTO contactnotes( contactId, notes, addedDate, lastModifiedDate, isActive)
	 		select '" . $newContactId . "' as id,  notes, CURDATE() as addeddate, CURDATE() as lastmodifieddate, isActive from contactnotes where contactId='$contactId'");
            $database->executeNonQuery("INSERT INTO contactwebsites(contactId, typeId, website, isprimary, addedDate)
	 		select '" . $newContactId . "' as id,  typeId, website, isprimary, CURDATE() as addeddate from contactwebsites where contactId='$contactId'");

           // $database->executeNonQuery("INSERT INTO groupcontacts (groupId, contactId) select groupId, " . $newContactId . " from groupcontacts where contactId='" . $contactId . "'");
            $database->executeNonQuery("UPDATE  contacttransferrequest SET statusId='2', approvedDate='" . $approveDate . "', newContactId ='".$newContactId."'  WHERE contactId='" . $contactId . "'");

            $pageFromReply = $database->executeObjectList("SELECT * FROM `pageformreply` WHERE `contactId` ='" .
                $contactId . "' GROUP BY `pageId` ");

            $totalPagesGroups = array();
            $totalPagesCompaigns = array();

            foreach ( $pageFromReply as $pfr ) {

                // Get all page groups
                $pageGroups = $database->executeObjectList("SELECT DISTINCT groupId FROM `pagecustomrulesdetails` WHERE `pageCustomRulesid` = (SELECT `pageCustomRulesId` FROM `pagecustomrules` WHERE `pageId` = '".$pfr->pageId."') AND `groupId` <> ''");
                foreach ($pageGroups as $group) {
                    $totalPagesGroups[] = $group->groupId;
                }

                // Get all page compaigns
                $pageCompaigns = $database->executeObjectList("SELECT DISTINCT planId, stepId FROM `pagecustomrulesdetails` WHERE `pageCustomRulesid` = (SELECT `pageCustomRulesId` FROM `pagecustomrules` WHERE `pageId` = '".$pfr->pageId."') AND planId <> ''");
                foreach ($pageCompaigns as $compaign) {
                    $totalPagesCompaigns[] = $compaign->planId;
                }

                if ( $transferByUser->leaderId == $userInfo->userId ) { // userToSend is a leader
                    $page = $database->executeObject("SELECT * FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "' AND privacy='1') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                    if ( empty($page->pageId) ) {
                        $page = $database->executeObject("SELECT * FROM `pages` WHERE `pageId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                    }

                } elseif ( $transferByUser->leaderId == 0 ) { // transferByUser is a leader

                    $page = $database->executeObject("SELECT * FROM `pages` WHERE `parentId` = '" . $pfr->pageId . "' AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");


                    if ( empty($page->pageId) ) {
                        $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");

                    }

                } else { // Both are users

                    $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `parentId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                    if ( empty($page->pageId) ) {
                        $page = $database->executeObject("SELECT `pageId` FROM `pages` WHERE `pageId` = (SELECT `parentId` FROM `pages` WHERE `pageId` ='" . $pfr->pageId . "') AND userId='" . $_SESSION['userId'] . "' AND `status`='1'");
                    }
                }


                if ( !empty($page) ) {

                    // Copy groups attached to the current profile
                    foreach ($pageGroups as $pageGroup) {
                        $database->executeNonQuery("INSERT INTO groupcontacts SET groupId = '".$pageGroup->groupId."', contactId = '" . $newContactId . "'");
                    }

                    $database->executeNonQuery("INSERT INTO `pageformreply`  (pageId,firstName,lastName,email,phoneNumber,dateOfBirth,refferedBy,bestTimeToCall,address,city,zipCode,state,country,isContactGenerated,contactId,isCounterSet,`addDate`,userId,viewId,pageURL,uniqueCode)SELECT '" . $page->pageId . "',firstName,lastName,email,phoneNumber,dateOfBirth,refferedBy,bestTimeToCall,address,city,zipCode,state,country,isContactGenerated,'" . $newContactId . "',isCounterSet,`addDate`,'" . $_SESSION['userId'] . "',viewId,pageURL,uniqueCode FROM pageformreply WHERE replyId='" . $pfr->replyId . "'");
                    $pageformreplyId = $database->insertid();

                    $newReply = $database->executeObject("SELECT * FROM `pageformreply` WHERE `replyId` = '" . $pageformreplyId . "'");

                    $questions = $database->executeObjectList("SELECT * FROM `pagequestions` WHERE `pageId`='" . $newReply->pageId . "'");

                    $oldQuestions = $database->executeRowList("SELECT `questionId` FROM `pagequestions` WHERE `pageId`='" . $pfr->pageId . "'");
                    $i = 0;

                    foreach ( $questions as $question ) {

                        $typeId = $database->executeScalar("SELECT `typeId` FROM `questions` WHERE `questionId` = '".$question->questionId."'");

                        if ($typeId > 2) {

                            $oldAnswers = $database->executeObjectList("SELECT `answer` FROM `pageformreplyanswers` WHERE `questionId` = '".$oldQuestions[$i][0]."' AND `replyId` = '".$pfr->replyId."'");

                            foreach($oldAnswers as $oldAnswer) {

                                if ( $transferByUser->leaderId == $userInfo->userId  ) {
                                    $answer = $database->executeScalar("SELECT `oldId` FROM `pageformquestionchoices` WHERE `choiceId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND questionId = '".$oldQuestions[$i][0]."'");
                                } elseif ( $transferByUser->leaderId == 0 ) {
                                    $answer = $database->executeScalar("SELECT `choiceId` FROM `pageformquestionchoices` WHERE `oldId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND questionId = '".$question->questionId."'");
                                } else {
                                    $answer = $database->executeScalar("SELECT `choiceId` FROM `pageformquestionchoices` WHERE `oldId` = (SELECT `oldId` FROM  `pageformquestionchoices` WHERE `choiceId` = '".$oldAnswer->answer."' AND `isActive` = '1' AND `questionId` = '".$oldQuestions[$i][0]."') AND `isActive` = '1' AND `questionId` = '".$question->questionId."'");
                                }

                                $database->executeNonQuery("INSERT INTO `pageformreplyanswers` (replyId,questionId,answer,value,contactId,answerText) SELECT '" . $newReply->replyId . "','" . $question->questionId . "', '".$answer."',value,'" . $newContactId . "',answerText FROM `pageformreplyanswers` WHERE `contactId` = '" . $contactId . "' AND replyId ='" . $pfr->replyId . "' AND questionId = '" . $oldQuestions[$i][0] . "'");
                            }

                        } else {
                            $database->executeNonQuery("INSERT INTO `pageformreplyanswers` (replyId,questionId,answer,value,contactId,answerText) SELECT '" . $newReply->replyId . "','" . $question->questionId . "', answer,value,'" . $newContactId . "',answerText FROM `pageformreplyanswers` WHERE contactId = '" . $contactId . "' AND replyId='" . $pfr->replyId . "' AND questionId = '" . $oldQuestions[$i][0] . "'");
                        }
                        $i++;
                    }

                    if ($pageCompaigns) {
                        $planIds = array();
                        $stepIds = array();
                        $planstepusers = $database->executeObjectList("SELECT * FROM `planstepusers` WHERE `contactId` = '".$contactId."'");
                        
                        foreach ($planstepusers as $planstepuser) {

                            foreach ($pageCompaigns as $pageCompaign) {
                            
                                if ($transferByUser->leaderId == $userInfo->userId) { // userToSend is a leader

                                    $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    if ( empty($planId) ) {
                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    }
                                    if ($planId) {
                                        $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                        if (empty($stepId)) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` = '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                        }

                                        if (!in_array($planstepuser->stepId, $stepIds)) {
                                            $stepIds[] = $planstepuser->stepId;
                                        }

                                    }
                                    
                                } elseif ( $transferByUser->leaderId == 0 ) { // transferByUser is a leader

                                    $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = '" . $pageCompaign->planId . "' AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    if ( empty($planId) ) {
                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    }
                                    if ($planId) {
                                        $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = '".$planstepuser->stepId."' AND `planId` = '".$planId."' AND `isactive`='1'");
                                        if (empty($stepId)) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                        }

                                        if (!in_array($planstepuser->stepId, $stepIds)) {
                                            $stepIds[] = $planstepuser->stepId;
                                        }
                                    }
                                    
                                } else {

                                    $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `parentId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    if ( empty($planId) ) {
                                        $planId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `planId` = (SELECT `parentId` FROM `plan` WHERE `planId` ='" . $pageCompaign->planId . "') AND userId='" . $_SESSION['userId'] . "' AND `isactive`='1'");
                                    }
                                    if ($planId) {
                                        $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `parentId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` =  '".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                        if (empty($stepId)) {
                                            $stepId = $database->executeScalar("SELECT `stepId` FROM `plansteps` WHERE `stepId` = (SELECT `parentId` FROM `plansteps` WHERE `stepId` ='".$planstepuser->stepId."') AND `planId` = '".$planId."' AND `isactive`='1'");
                                        } 
                                        if (!in_array($planstepuser->stepId, $stepIds)) {
                                            $stepIds[] = $planstepuser->stepId;
                                        }
                                        
                                    }
                                }

                                if (!in_array($planId, $planIds)) {
                                    $planIds[] = $planId;
                                }
                                
                                if ($stepId) {

                                    $database->executeNonQuery("INSERT INTO `planstepusers` (contactId, stepId, stepStartDate, stepEndDate) SELECT '".$newContactId."', '".$stepId."', stepStartDate, stepEndDate FROM `planstepusers` WHERE `stepId` = '".$planstepuser->stepId."' AND `contactId` = '".$contactId."'");

                                }

                                
                            } // END of Inner foreach loop
                        } // END of Outer foreach loop
                    }
                }
            }

            // Copy groups which are directly attached to the contact
            $totalPagesGroups = implode(',', $totalPagesGroups);
            $database->executeNonQuery("INSERT INTO `groupcontacts` (groupId, contactId) SELECT groupId, '".$newContactId."' FROM `groupcontacts` WHERE `groupId` NOT IN ('".$totalPagesGroups."') AND contactId = '".$contactId."'");

            
            $index = 0;
            $tasks = $database->executeObjectList("SELECT tasks.taskId FROM `tasks` JOIN `taskcontacts` ON tasks.`taskId` = taskcontacts.`taskId` WHERE userId = '".$_SESSION['userId']."' AND contactId = '".$newContactId."' AND isArchive = '0' AND title IN (SELECT title FROM tasks JOIN taskcontacts ON tasks.`taskId` = taskcontacts.`taskId` WHERE contactId = '".$contactId."' AND userId = '".$transferByUser->userId."')");
            foreach ($tasks as $task) {
                    $database->executeNonQuery("UPDATE `tasks` SET `isArchive` = '1' WHERE taskId = '".$task->taskId."'");
            }
            // Copy contact task
            $contactTasks = $database->executeObjectList("SELECT * FROM `taskcontacts` WHERE `contactId` = '".$contactId."'");
            foreach ($contactTasks as $task) {
                $database->executeNonQuery("INSERT INTO `tasks` (userId,title,isCompleted,priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId, isTransferred) SELECT '".$_SESSION['userId']."',title,isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, '".$planIds[$index]."', endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId, '1' FROM `tasks` WHERE `taskId` = '".$task->taskId."'");
                $newTaskId = $database->insertid();
                $database->executeNonQuery("INSERT INTO `taskcontacts` SET `taskId` = '".$newTaskId."', `contactId` = '".$newContactId."'");
                $index++;
            }
            
            $database->executeNonQuery("UPDATE `contactemailhistory` SET `isArchive` = '1', `isTransferred` = '1' WHERE `contactId` = '".$newContactId."' AND `userId` = '".$_SESSION['userId']."'");
            // Copy contact mail history
            $database->executeNonQuery("INSERT INTO `contactemailhistory` (planId, stepId, contactId, userId, toEmail, fromEmail, `subject`, `body`, `dateTime`)  SELECT planId, stepId, '".$newContactId."', '".$_SESSION['userId']."',toEmail, fromEmail, `subject`, `body`, `dateTime` FROM `contactemailhistory` WHERE `contactId` = '".$contactId."' AND `userId` = '".$transferByUser->userId."'");


            return ($database->getAffectedRows() != '' ? true : false);
        }

        // end function if contact Already exist


        function RejectTransferContact($contactId)
        {
            $database = new database();
            $rejecttDate = date("Y-m-d");
            $database->executeNonQuery("UPDATE  contacttransferrequest SET statusId='3', rejectedDate='" . $rejecttDate . "' WHERE contactId='" . $contactId . "'");

			return ($database->getAffectedRows() != '' ? true : false);
		}

		//Junaid code
		public function addContact($user) {
			$database = new database();
			if (empty($user['dateOfBirth'])) {
				$dateOfBirth = '0000-00-00';
			}
			else {
				$dateOfBirth = date('Y-m-d', strtotime($user['dateOfBirth'])); // outputs 2006-01-24
			}
			$addedDate = date("Y-m-d");
			//custom data for contact
			$userId = $_SESSION["userId"];
			$contactId = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='" . $user['email'] . "' and c.userId='" . $userId . "'");
			//echo die();
			if (empty($contactId)) {
				$database->executeNonQuery("INSERT INTO contact
									  SET userId='" . $userId . "',
										title='" . $user['title'] . "',
										firstName='" . mysql_real_escape_string($user['firstname']) . "',
										lastName='" . mysql_real_escape_string($user['lastname']) . "',
										companyTitle='" . mysql_real_escape_string($user['companyname']) . "',
										jobTitle='" . mysql_real_escape_string($user['jobtitle']) . "',
										dateOfBirth='" . $dateOfBirth . "',
										referredBy='" . mysql_real_escape_string($user['referred_by']) . "',
										addedDate='" . $addedDate . "',
										isActive=1");
				$contactId = $database->insertid();
			}
			else {
				$database->executeNonQuery("UPDATE contact
									  SET
										title='" . $user['title'] . "',
										firstName='" . mysql_real_escape_string($user['firstname']) . "',
										lastName='" . mysql_real_escape_string($user['lastname']) . "',
										companyTitle='" . mysql_real_escape_string($user['companyname']) . "',
										jobTitle='" . mysql_real_escape_string($user['jobtitle']) . "',
										dateOfBirth='" . $dateOfBirth . "',
										referredBy='" . mysql_real_escape_string($user['referred_by']) . "',
										addedDate='" . $addedDate . "',
										isActive=1 WHERE  contactId='" . $contactId . "'");
			}

			return $contactId;
		}

		//phone number for contact
		public function addContactPhoneNumberByContactId($contactId, $phone) {
			$database = new database();
			$phoneaddedDate = date("Y-n-j");
			$contactId2 = $database->executeScalar("SELECT contactId FROM contactphonenumbers where contactId='" . $contactId . "'");
			if (empty($contactId2)) {
				$database->executeNonQuery("INSERT INTO contactphonenumbers
										SET contactId='" . $contactId . "',
											phoneNumber='" . $phone['phone'] . "',
											addedDate='" . $phoneaddedDate . "'");
				$database->executeNonQuery("UPDATE contact SET phoneNumber='" . $phone['phone'] . "' WHERE contactId='" . $contactId . "'");
			}
			else {
				$database->executeNonQuery("UPDATE contactphonenumbers
										SET 
											phoneNumber='" . $phone['phone'] . "'
											WHERE contactId='" . $contactId . "'
											 ");
				$database->executeNonQuery("UPDATE contact SET phoneNumber='" . $phone['phone'] . "' WHERE contactId='" . $contactId . "'");
			}
		}

		public function addContactEmailByContactId($contactId, $email) {
			$database = new database();
			$emailaddedDate = date("Y-n-j");
			$emailId = $database->executeScalar("SELECT emailId FROM emailcontacts where contactId='" . $contactId . "'");
			if (empty($emailId)) {
				$database->executeNonQuery("INSERT INTO emailcontacts
									SET contactId='" . $contactId . "',
										email='" . $email['email'] . "',
										addedDate='" . $emailaddedDate . "'");
				$database->executeNonQuery("UPDATE contact SET email='" . $email['email'] . "' WHERE contactId='" . $contactId . "'");

				return $database->insertid();
			}
			else {
				return $emailId;
			}
		}

		public function addContactcallTimeByContactId($contactId, $calltime) {
			$database = new database();
			$calltimeaddedDate = date("Y-n-j");
			$contactId2 = $database->executeScalar("SELECT contactId FROM contactcalltime where contactId='" . $contactId . "'");
			if (empty($contactId2)) {
				$database->executeNonQuery("INSERT INTO contactcalltime
									SET contactId='" . $contactId . "',
										weekdays='" . $calltime['BestTimestoCallWeekdays'] . "',
										weekdayEvening='" . $calltime['BestTimestoCallWeekdayEvenings'] . "',
										weekenddays='" . $calltime['BestTimestoCallWeekendDays'] . "',
										weekenddayEvening='" . $calltime['BestTimestoCallWeekendEvening'] . "',
										addedDate='" . $calltimeaddedDate . "'");
			}
			else {
				$database->executeNonQuery("UPDATE contactcalltime
									SET 
										weekdays='" . $calltime['BestTimestoCallWeekdays'] . "',
										weekdayEvening='" . $calltime['BestTimestoCallWeekdayEvenings'] . "',
										weekenddays='" . $calltime['BestTimestoCallWeekendDays'] . "',
										weekenddayEvening='" . $calltime['BestTimestoCallWeekendEvening'] . "'
										WHERE contactId='" . $contactId . "'");
			}
			$database->executeNonQuery("UPDATE contact SET weekdays='" . $calltime['BestTimestoCallWeekdays'] . "',
										weekdayEvening='" . $calltime['BestTimestoCallWeekdayEvenings'] . "',
										weekenddays='" . $calltime['BestTimestoCallWeekendDays'] . "',
										weekenddayEvening='" . $calltime['BestTimestoCallWeekendEvening'] . "' WHERE contactId='" . $contactId . "'");
		}

		//Address detail for contact
		public function addContactAddressByContactId($contactId, $address) {
			$database = new database();
			$jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address['street'] . ',+' . $address['city'] . ',+' . $address['state'] . '+,' . $address['zip'] . ',+' . $address['country']) . '&sensor=true');
			$obj = json_decode($jsondata, true);
			$lat = $obj['results'][0]['geometry']['location']['lat'];
			//echo "\n";
			$long = $obj['results'][0]['geometry']['location']['lng'];
			$jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');
			$obj2 = json_decode($jsondata2, true);
			$offset = $obj2['rawOffset'];
			$gmtDifference = ($offset / 60) / 60;
			$addressaddedDate = date("Y-n-j");
			$contactId2 = $database->executeScalar("SELECT contactId FROM contactaddresses where contactId='" . $contactId . "'");
			if (empty($contactId2)) {
				$database->executeNonQuery("INSERT INTO contactaddresses
										SET contactId='" . $contactId . "',
											street='" . mysql_real_escape_string($address['street']) . "',
											zipcode='" . $address['zip'] . "',
											state='" . mysql_real_escape_string($address['state']) . "',
											city='" . mysql_real_escape_string($address['city']) . "',
											country='" . mysql_real_escape_string($address['country']) . "',
											gmt='" . $gmtDifference . "',
											addedDate='" . $addressaddedDate . "'");
			}
			else {
				$database->executeNonQuery("UPDATE contactaddresses
										SET 
											street='" . mysql_real_escape_string($address['street']) . "',
											zipcode='" . $address['zip'] . "',
											state='" . mysql_real_escape_string($address['state']) . "',
											city='" . mysql_real_escape_string($address['city']) . "',
											country='" . mysql_real_escape_string($address['country']) . "',
											gmt='" . $gmtDifference . "' WHERE contactId='" . $contactId . "'");
			}
			$database->executeNonQuery("UPDATE contact SET street='" . mysql_real_escape_string($address['street']) . "',
											zipcode='" . $address['zip'] . "',
											state='" . mysql_real_escape_string($address['state']) . "',
											city='" . mysql_real_escape_string($address['city']) . "',
											country='" . mysql_real_escape_string($address['country']) . "',
											gmt='" . $offset . "' WHERE contactId='" . $contactId . "'");
		}

		//Assign Group to Contact
		function assignGroupTo($contactId, $groups, $userId) {
			$database = new database();
			$profiles = new Profiles();
			$count = sizeof($groups['groupcheckbox']);
			$group = 0;
			while ($count > 0) {
				$contactId2 = $database->executeScalar("SELECT contactId FROM groupcontacts where contactId='" . $contactId . "' and groupId='" . $groups['groupcheckbox'][$group] . "'");
				if (empty($contactId2)) {
					$database->executeNonQuery("INSERT INTO groupcontacts
											SET groupId='" . $groups['groupcheckbox'][$group] . "',
												contactId='" . $contactId . "'");

				}
				$profiles->EditContactPlanByGroup($database, $contactId, $groups['groupcheckbox'][$group], $userId);
				$count--;
				$group++;
			}
		}

		//Assign Plan Steps to Contact
		function assignPlanStep($userId, $contactId, $planSteps) {
			$database = new database();
			//today date
			$date = date('Y-m-d');
			$date = strtotime($date);
			$existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $contactId . "'");
			$x = 0;
			$contactSteps = array();
			$newContactSteps = array();
			foreach ($existingSteps as $exists) {
				$contactSteps[$x] = $exists->stepId;
				$x++;
			}
			//print_r($existingSteps);
			$userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
			//echo 'TotalPlans';
			$count = sizeof($planSteps['plan']);
			//echo '<br />';
			if ($count != 0) {
				//echo 'Count: '.$count;
				$step = 0;
				$j = 0;
				$y = 0;
				foreach ($planSteps['plan'] as $planss) {
					$i = 0;
					/*echo 'Count: '.$count;
					echo '<br />';*/
					$plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $planss . "'");
					if ($planSteps['planStepDropDown' . $planss] != '' && $planSteps['planStepDropDown' . $planss] != 'Completed') {
						/*echo $planss;
						echo "<br />";*/
						$palnStepsToSkip = $database->executeObjectList("SELECT * FROM plansteps WHERE isactive='1' AND isArchive='0' AND order_no<(select order_no from plansteps where stepId='" . $planSteps['planStepDropDown' . $planss] . "') AND planId='" . $planss . "' order by order_no asc");
						foreach ($palnStepsToSkip as $pss) {
							$stepSkipId = $database->executeScalar("SELECT stepId from contacttaskskipped  where  stepId='" . $pss->stepId . "' and contactId='" . $contactId . "'");
							if (empty($stepSkipId) || $stepSkipId = '') {
								$database->executeNonQuery("INSERT INTO `contacttaskskipped`( `stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES ('" . $pss->stepId . "','" . $pss->planId . "','" . $contactId . "',NOW(),'" . mysql_real_escape_string($pss->summary) . "')");
							}
						}
						$palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE isactive='1' AND
						isArchive='0' AND order_no>=(select order_no from plansteps where stepId='" . $planSteps['planStepDropDown' . $planss] . "') AND planId='" . $planss . "' order by order_no asc");
						//print_r($palnSteps);
						/*echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$planSteps['planStepDropDown'.$planss]."') AND planId='".$planss."' order by stepId asc";
						echo "<br />";*/
						$totalSteps = sizeof($palnSteps);
						$stepCounter = 1;
						foreach ($palnSteps as $ps) {
							//echo "next plan step";
							$newContactSteps[$y] = $ps->stepId;
							$y++;
							if (!in_array($ps->stepId, $contactSteps)) {
								$planId2 = self::CheckLastStepOfPlan($planss, $contactId);
								if (!empty($planId2)) {
									$database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $contactId . "'");
								}
								/*echo 'not exists'.$ps->stepId;
								echo '<br />';*/
								$stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
								$dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
								$database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
								$newStepId = $database->insertid();
								//echo 'inserted';
                                $permissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='".$userId."'");

                                //echo 'inserted';
                                if ( $ps->isTask == 0 && $permissionStatus !='4' ) {
									if ($ps->daysAfter == 0) {
										/*echo "\r\nemail step";
										echo '<br>';*/
										//echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
										$contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
										if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {
											/*echo 'emial allowed';
											echo '<br>';*/
											$contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
											$mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
											preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
											foreach ($match[1] as $key) {
												if ($key == "First Name") {
													$firstname = 'firstName';
												}
												if ($key == "Last Name") {
													$lastname = 'lastName';
												}
												if ($key == "Phone Number") {
													$phonenumber = 'phoneNumber';
												}
												if ($key == "Company Name") {
													$companyname = 'companyTitle';
												}
												if ($key == "@Signature") {
													$signature = self::getSignatures($database, $userId);
												}
											}
											if (!empty($firstname)) {
												$f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
											}
											if (!empty($lastname)) {
												$l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
											}
											if (!empty($companyname)) {
												$c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
											}
											if (!empty($phonenumber)) {
												$p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
											}
											$subject = $mailBody->subject;
											$rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
											$rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
											$rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
											$rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
											$rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
											$message = str_replace("\'", "'", $rplbysig);
											$headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
												"Reply-To: " . $userInfo->email . "\r\n";
											$headers .= "Content-Type: text/html";
											//echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
											$mail = new smtpEmail();
											$mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject,$mailBody->id);
											//mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
											//echo "INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
											//('".$plan->planId."', '".$stepDesc->stepId."','".$contactId."', '".$userId."', '".$contactemail."', '".$userInfo->email."', '".mysql_real_escape_string($subject)."', '".mysql_real_escape_string($message)."', NOW())";
											$database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
											$database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
											if ($totalSteps == $stepCounter) {
												$database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $planss . "','" . $contactId . "',CURDATE())");
											}
										}
									}
									else {
										/*echo 'breaking days after not zero';
										echo '<br>';*/
										break;
									}
								}
								else {
                                    if ( $ps->isTask == 1 ){


									//echo "<br /> task step<br />";
									$database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
									$taskId = $database->insertid();
									$database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
									$database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                                        }
                                        }
									/*echo '<br />';
									echo 'breaking task occured';*/
									break;
								}

							else {
								/*echo '<br />';
								echo 'no change';*/
								break;
							}
							$stepCounter++;
						}
						$count--;
						$step++;
					}
					else {
						/*echo "PlanId".$planss;
						echo "<br />";
						echo "SELECT ps.stepId, ps.planId,order_no
						FROM plansteps ps
						WHERE  ps.planId='".$planss."' and ps.order_no=(select max(order_no) from plansteps where planId='".$planss."')";
							echo "<br />";*/
						$lastStepOfPlan = $database->executeObject("SELECT ps.stepId, ps.planId,order_no
						FROM plansteps ps
						WHERE  ps.planId='" . $planss . "' and ps.order_no=(select max(order_no) from plansteps where planId='" . $planss . "')");
						/*print_r($lastStepOfPlan);
						echo "<br />";
						echo "INSERT INTO contactPlansCompleted(stepId ,planId, contactId, dateCompleted) VALUES ('".$lastStepOfPlan->stepId."','".$lastStepOfPlan->planId."','".$contactId."',CURDATE())";
	*/
						$database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId ,planId, contactId, dateCompleted) VALUES ('" . $lastStepOfPlan->stepId . "','" . $lastStepOfPlan->planId . "','" . $contactId . "',CURDATE())");
						/*echo "<br>";
						echo "Added". $lastStepOfPlan->planId;
						echo "<br />";*/
					}
				}
			}
			else {
				//echo "DELETE from taskcontacts where taskId  IN (SELECT tc.taskId FROM tasks t join taskcontacts tc on t.taskId=tc.taskId where t.userId='".$userId."' and isCompleted<>1 and tc.contactId='".$contactId."') and contactId='".$contactId."'";
				$taskId2 = $database->executeObjectList("SELECT tc.taskId FROM tasks t join taskcontacts tc on t.taskId=tc.taskId where t.userId='" . $userId . "' and isCompleted<>1 and tc.contactId='" . $contactId . "' and planId<>0");
				foreach ($taskId2 as $t2) {
					//echo "DELETE from taskcontacts where taskId='".$t2->taskId."' and contactId='".$contactId."'";
					$database->executeNonQuery("DELETE from taskcontacts where taskId='" . $t2->taskId . "' and contactId='" . $contactId . "'");
					//echo "DELETE from tasks where taskId='".$t2->taskId."'";
					$database->executeNonQuery("DELETE from tasks where taskId='" . $t2->taskId . "'");
				}
				$database->executeNonQuery("DELETE FROM contactPlansCompleted where  contactId='" . $contactId . "'");
			}
			/*print_r($contactSteps);
			echo "New Steps";
			print_r($newContactSteps);*/
			$delSteps = array_diff($contactSteps, $newContactSteps);
			$steps = array();
			$j = 0;
			foreach ($delSteps as $key => $val) {
				$steps[$j] = $val;
				$j++;
			}
			/*echo "<br>";
			echo "Diiference";
			print_r($steps);
			echo "<br>";*/
			//echo "SELECT  planId FROM  contactPlansCompleted WHERE contactId='".$contactId."' and planId NOT IN (".$plansContact.")";
			/*$plansContact=implode(',',$planSteps['plan']);
			if($plansContact!='')
			{
				$planInfo=$database->executeScalar("SELECT  planId FROM  contactPlansCompleted WHERE contactId='".$contactId."' and planId NOT IN (".$plansContact.")");
				$database->executeNonQuery("DELETE FROM contactPlansCompleted where planId='".$planInfo."' and contactId='".$contactId."'");
			}
			else
			{
				$database->executeNonQuery("DELETE FROM contactPlansCompleted where contactId='".$contactId."'");
			}*/
			//print_r($steps);
			if (!empty($contactSteps)) {
				for ($i = 1; $i <= sizeof($steps); $i++) {
					if (!in_array($steps[$i], $newContactSteps)) {
						/*echo "DELETE FROM  planstepusers WHERE contactId='".$contactId."' and stepId='".$steps[$i]."'";
						echo "<br>";*/
						$database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId='" . $contactId . "' and stepId='" . $steps[$i] . "'");
						/*echo "SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId='".$steps[$i]."' and contactId='".$contactId."'";
						echo "<br>";*/
						$taskInfo = $database->executeObject("SELECT tp.taskId, planId FROM  taskplan tp join taskcontacts tc on tp.taskId=tc.taskId  WHERE stepId='" . $steps[$i] . "' and contactId='" . $contactId . "'");
						/*echo "DELETE FROM tasks WHERE taskId='".$taskInfo->taskId."' and isCompleted<>1";
						echo "<br>";*/
						$database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $taskInfo->taskId . "' and isCompleted<>1");
						$isDeleted = $database->getAffectedRows();
						/*echo "task deleted : ".$database->getAffectedRows();
						echo '<br>';*/
						if ($database->getAffectedRows() > 0) {
							//echo "DELETE FROM taskcontacts WHERE contactId='".$contactId."' and taskId='".$taskInfo->taskId."'";
							$database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $taskInfo->taskId . "'");
						}
						$database->executeNonQuery("DELETE FROM contactPlansCompleted where planId='" . $taskInfo->planId . "' and contactId='" . $contactId . "'");
					}
				}
			}
			if (!empty($planSteps['plan'])) {
				$NewPlans = implode(",", $planSteps['plan']);
				$database->executeNonQuery("DELETE FROM contactPlansCompleted where planId NOT IN (" . $NewPlans . ") and contactId='" . $contactId . "'");
			}
		}

		public function CheckLastStepOfPlan($planId, $contactId) {
			$database = new database();

			return $database->executeScalar("select planId from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");
		}

		public function getSignatures($database, $userId) {
            $sql = "SELECT `signature` FROM `usersignature` WHERE `userId` = '".$userId."'";
            return $database->executeScalar($sql);
        }

		public function getUserSignature($database, $userId) {
			$userDetails = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.leaderId,u.userId from user u
				where u.userId='" . $userId . "'");

			return $userDetails;
		}

		function getconsultantTitle($database, $userId, $fieldMappingName) {

			return $consultantTitle = $database->executeObject("SELECT `fieldCustomName`,`status` FROM `leaderdefaultfields` WHERE userId='" . $userId . "' AND `fieldMappingName`='" . $fieldMappingName . "' ");

		}

		/*contact details	*/

		public function showCustomFields($userId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM usercontactcustomdata WHERE userId='" . $userId . "'");
		}

		public function showCustomFieldsById($userId, $customFieldId) {
			//return $database->executeObject
			$database = new database();

			return ("SELECT * FROM usercontactcustomdata WHERE userId='" . $userId . "' AND customDataId='" . $customFieldId . "'");
		}

		function showContactDetail($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");
		}

		function showContactPhoneNumber($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='" . $contactId . "'");
		}

		function showContactEmailAddress($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM emailcontacts WHERE contactId='" . $contactId . "'");
		}

		function showContactWebsite($contactId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='" . $contactId . "'");
		}

		//contact list for auto search

		function showContactAddress($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactaddresses WHERE contactId='" . $contactId . "'");
		}

		function showContactTimeForCall($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactcalltime WHERE contactId='" . $contactId . "'");
		}

		function getContactList($name) {
			$database = new database();

			return $database->executeObjectList("SELECT firstName ,lastName ,contactId FROM contact WHERE firstName LIKE '%$name%' OR lastName LIKE '%$name%'");
		}

		function editContactDetail($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");
		}

		function editContactPhone($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='" . $contactId . "'");
		}

		function editContactEmail($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM emailcontacts WHERE contactId='" . $contactId . "'");
		}

		function editContactAddress($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactaddresses WHERE contactId='" . $contactId . "'");
		}

		//contcta for manage contacts

		function editContactCallTime($contactId) {
			$database = new database();

			return $database->executeObject("SELECT * FROM contactcalltime WHERE contactId='" . $contactId . "'");
		}

		function editContactGroup($groupId, $contactId) {
			$database = new database();
			$result = $database->executeObject("SELECT * FROM groupcontacts WHERE contactId='" . $contactId . "' AND groupId='" . $groupId . "'");

			return (!empty($result) ? 1 : 0);
		}

		function manageContacts($userId, $groupId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contact WHERE userId='" . $userId . "' AND isActive=1 AND contactId NOT IN (SELECT contactId FROM groupcontacts WHERE groupId=" . $groupId . ")");
		}

		function manageGroupsContacts($userId, $groupId) {
			$database = new database();

			return $database->executeObjectList("SELECT * FROM contact WHERE userId='" . $userId . "' AND isActive=1 AND contactId IN (SELECT contactId FROM groupcontacts WHERE groupId=" . $groupId . ")");
		}

		function manageAddContactsToGroup($contactId, $groupId) {
			$database = new database();
			//echo "SELECT * from groupcontacts where groupId=" . $groupId . " and contactId=" . $contactId . "";
			$exists = $database->executeObject("SELECT * from groupcontacts where groupId=" . $groupId . " and contactId=" . $contactId . "");
			if (empty($exists)) {
				//echo "INSERT INTO groupcontacts SET groupId=" . $groupId . ",contactId=" . $contactId . "";
				$database->executeNonQuery("INSERT INTO groupcontacts SET groupId=" . $groupId . ",contactId=" . $contactId . "");

				return ($database->getAffectedRows() != '' ? 1 : 0);
			}
		}

		//add quick contact

		function removeContactFromGroups($groupId) {
			$database = new database();

			return $database->executeNonQuery("DELETE FROM groupcontacts WHERE groupId='" . $groupId . "'");
		}

		function manageDeleteContactsFromGroup($groupId, $contactId) {
			$database = new database();

			return $database->executeNonQuery("DELETE FROM groupcontacts WHERE groupId=" . $groupId . " AND contactId=" . $contactId . "");
		}

		function addQuickContactDetail($user) {
			$database = new database();
			$addedDate = date("Y-m-d");
			$dateOfBirth = '0000-00-00 00:00:00';
			//custom data for contact
			$contactId = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='" . $user['email'] . "' and c.userId='" . $user['userid'] . "'");
			//echo die();
			if (empty($contactId)) {
				$database->executeNonQuery("INSERT INTO contact
									  SET userId='" . $user['userid'] . "',
										firstName='" . mysql_real_escape_string($user['quickfirstname']) . "',
										lastName='" . mysql_real_escape_string($user['quicklastname']) . "',
										companyTitle='" . mysql_real_escape_string($user['quickcompanyname']) . "',
										dateOfBirth='" . $dateOfBirth . "',
										jobTitle='" . mysql_real_escape_string($user['quickjobtitle']) . "',
										addedDate='" . $addedDate . "',
										isActive=1,email='" . $user['quickemail'] . "', phoneNumber='" . $user['quickphone'] . "'");
				$contactId = $database->insertid();
				$database->executeNonQuery("INSERT INTO contactphonenumbers
										SET contactId='" . $contactId . "',
											phoneNumber='" . $user['quickphone'] . "',
											addedDate='" . $addedDate . "'");
				$emailaddedDate = date("Y-n-j");
				$database->executeNonQuery("INSERT INTO emailcontacts
										SET contactId='" . $contactId . "',
											email='" . $user['quickemail'] . "',
											addedDate='" . $addedDate . "'");
				$emailId = $database->insertid();
				self::createPermissionRequest($user['userid'], $contactId, $emailId);
			}
			else {
				$database->executeNonQuery("UPDATE contact
									 	SET
										firstName='" . mysql_real_escape_string($user['quickfirstname']) . "',
										lastName='" . mysql_real_escape_string($user['quicklastname']) . "',
										companyTitle='" . mysql_real_escape_string($user['quickcompanyname']) . "',
										dateOfBirth='" . $dateOfBirth . "',
										jobTitle='" . mysql_real_escape_string($user['quickjobtitle']) . "',
										addedDate='" . $addedDate . "',
										isActive=1,email='" . $user['quickemail'] . "', phoneNumber='" . $user['quickphone'] . "' WHERE contactId='" . $contactId . "'");
				$database->executeNonQuery("UPDATE contactphonenumbers
										SET
											phoneNumber='" . $user['quickphone'] . "' WHERE contactId='" . $contactId . "'");
			}

			return 1;
		}

        function createPermissionRequest($userId, $contactId, $emailId,  $permissionStatusId = 2)
        {
            $database = new database();
            $database->executeNonQuery("INSERT INTO contactpermissionrequests SET userId='" . $userId . "', toContactId='" . $contactId . "', emailId='" . $emailId . "', requestDate=CURDATE(), statusId='".$permissionStatusId."'");
        }

        function createPermissionRequestNewContact($userId, $contactId, $emailId) {
            $database = new database();
            $database->executeNonQuery("INSERT INTO contactpermissionrequests SET userId='" . $userId . "', toContactId='" . $contactId . "', emailId='" . $emailId . "', requestDate=CURDATE(), statusId=4");
        }

		function archiveContact($contactId) {
			$database = new database();
			$database->executeNonQuery("UPDATE contact SET isActive=0, archiveDate=NOW() WHERE contactId='" . $contactId . "'");

			return ($database->getAffectedRows() != '' ? 1 : 0);
		}

		function addNewTask() {
			//query section for new task
		}

		function GetGroupContacts($userId, $groupId) {
			$database = new database();
			if (!empty($groupId)) {
				/*$contacts=$database->executeObjectList("SELECT
			c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
		FROM
			contact c

			LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
				) as gpr ON c.contactId=gpr.contactId
			LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

				JOIN groupcontacts gc on c.contactId=gc.contactId
				 where c.userId='".$userId."' AND gc.groupId IN (".$groupId.") AND c.isActive=1  order by c.firstName ");*/
				$contacts = $database->executeObjectList("SELECT
    DISTINCT(c.contactId),c.profiles,c.email,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId

	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
 	JOIN groupcontacts gc ON c.contactId=gc.contactId
    WHERE c.userId='" . $userId . "'  AND gc.groupId IN (" . $groupId . ")  AND c.isActive=1 ORDER BY c.firstName ");
			}
			else {
				/*$contacts=$database->executeObjectList("SELECT
	c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
	FROM
	contact c

	LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
		) as gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply


		 where c.userId='".$userId."'  AND c.isActive=1 order by c.firstName");*/
				$contacts = $database->executeObjectList("SELECT
    DISTINCT(c.contactId),c.profiles,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId

	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
    WHERE c.userId='" . $userId . "'  AND c.isActive=1 ORDER BY c.firstName ");
			}

			return $contacts;
		}

		function checkContactGroups($groupId, $contactId) {
			$database = new database();
			$contact = $database->executeScalar("Select contactId FROM groupcontacts WHERE groupId='" . $groupId . "' and contactId='" . $contactId . "'");

			return $contact;
		}

		function getContactForDefault($contactId) {
			$database = new database();
			$result = $database->executeObject("SELECT contactId as id, CONCAT(firstName,' ',lastName) as name from contact WHERE contactId='" . $contactId . "'");
			$rar = '{"id":"' . $result->id . '","name":"' . $result->name . '"},';

			return $rar;
		}

		function GetEmail($user_id) {
			$database = new database();

			return $database->executeScalar("SELECT email FROM `user` WHERE userId='" . $user_id . "'");
		}

		function permissionRequest($user_id, $contactId, $emailId) {
			$database = new database();
			$flag = 0;
			$requestDate = date("Y-m-d");

			$permissionContacts = $database->executeObject("SELECT userId,toContactId FROM contactpermissionrequests WHERE userId='" . $user_id . "' AND toContactid='" . $contactId . "'");
			if (empty($permissionContacts)) {
				
				$database->executeNonQuery("INSERT INTO contactpermissionrequests SET userId='" . $user_id . "',toContactId='" . $contactId . "',emailId='" . $emailId . "',requestDate='" . $requestDate . "',statusId=2");

				return $email;
			}

		}

		function subscribeEmail($userId, $contactId, $statusId) {
			$database = new database();
			$database->executeNonQuery("UPDATE `contactpermissionrequests` SET `statusId` = '" . $statusId . "' WHERE `toContactId` = '" . $contactId . "' ORDER BY `permissionRequestId` DESC LIMIT 1");
			// $done = $database->getAffectedRows();
			//echo "UPDATE contactpermissionrequests SET statusId='".$statusId."' WHERE toContactId='".$contactId."' AND userId='".$userId."'";
			$database->executeNonQuery("UPDATE contactpermissionrequests SET statusId='" . $statusId . "' WHERE toContactId='" . $contactId . "' ");
			$done = $database->getAffectedRows();
			/*if(empty($done) || $done==0)
			{
				$database->executeNonQuery("INSERT INTO contactpermissionrequests SET statusId='".$statusId."' , toContactId='".$contactId."' , userId='".$userId."'");
			}*/
		}
function updateProcess($contactId,$userId){
    $database = new database();

   $udtadteResult = $database->executeNonQuery("UPDATE contactpermissionrequests SET statusId='2' WHERE toContactId='"
       . $contactId . "' AND userId='".$userId."' ");
return $udtadteResult;
}
		function UnsubscribeContact($contactId) {
			$database = new database();
			$database->executeNonQuery("UPDATE contactpermissionrequests SET statusId='4' WHERE toContactId='" . $contactId . "'");

			return ($database->getAffectedRows() != '' ? 1 : 0);
		}

		function showContactpermission($contactId) {
			$database = new database();
			$permission = $database->executeObject("SELECT * FROM contactpermissionrequests WHERE toContactId='" . $contactId . "' order by permissionRequestId desc LIMIT 1");

			return $permission;
		}

		function countEmailStepOfContact($contactId) {
			$database = new database();
			$emailstepCount = $database->executeScalar("select count(stepId) from plansteps,plan where stepId IN( select stepId from planstepusers where contactId=" . $contactId . ") and isTask=0 and plan.planId=plansteps.planId and plan.isDoubleOptIn=1");

			return $emailstepCount;
		}

		/*public function getSearchContact($userId,$search){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM contact WHERE userId='".$userId."' AND isActive=1 AND ( firstName LIKE '%".$search."%' OR lastName LIKE '%".$search."%' OR interest LIKE '%".$search."%') order by firstName");

		}*/

		public function deleteContactPlanLinkedtoOneGroup($contactId, $userId, $groupId) {
			$database = new database();
			$plans = $database->executeObjectList("SELECT gc.groupId,planId from groupcontacts gc JOIN plan p on gc.groupId=p.groupId where gc.contactId='" . $contactId . "' and p.userId='" . $userId . "' and gc.groupId='" . $groupId . "'");
			foreach ($groupPlans as $plans) {
				$palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plans->planId . "' order by order_no asc");
				foreach ($palnSteps as $ps) {
					$database->executeNonQuery("DELETE from planstepusers where stepId='" . $ps->stepId . "' and contactId='" . $contactId . "'");
					$taskId = $database->executeScalar("SELECT taskId from tasks where userId='" . $userId . "' and planId='" . $ps->planId . "'");
					$database->executeNonQuery("DELETE from tasks where taskId='" . $taskId . "'");
					$database->executeNonQuery("DELETE  FROM taskplan WHERE taskId='" . $taskId . "' and planId='" . $ps->planId . "' and stepId='" . $ps->stepId . "'");
					$database->executeNonQuery("DELETE  FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $taskId . "'");
				}
				$database->executeNonQuery("DELETE  FROM contactPlansCompleted WHERE planId='" . $plans->planId . "' and  contactId='" . $contactId . "'");
			}
			$database->executeNonQuery("DELETE FROM groupcontacts WHERE contactId='" . $contactId . "'");
		}

		public function AddContactsToPlanByGroup($contactId, $groupId, $userId) {
			$database = new database();
			$profiles = new Profiles();
			$planId = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $userId);
		}

		public function getSearchContact($userId, $search) {
			$database = new database();
			$sqlNew = "SELECT
    DISTINCT(c.contactId),c.profiles,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId

	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
    WHERE c.userId='" . $userId . "'  AND c.isActive=1 AND ( c.firstName LIKE '%" . $search . "%' OR c.lastName LIKE '%" . $search . "%' OR c.interest LIKE '%" . $search . "%' OR c.referredBy LIKE '%" . $search . "%')  order by c.firstName";
			$sqlOld = "SELECT
    c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
		) as gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply


		  WHERE c.userId='" . $userId . "' AND c.isActive=1 AND ( c.firstName LIKE '%" . $search . "%' OR c.lastName LIKE '%" . $search . "%' OR c.interest LIKE '%" . $search . "%' OR c.referredBy LIKE '%" . $search . "%')  order by c.firstName";

			return $database->executeObjectList($sqlNew);
		}

		public function getSearchContactForPermissionManager($userId, $search) {
			$database = new database();

			return $database->executeObjectList("SELECT c.contactId ,c.firstName,c.lastName,p.title FROM contact c,contactpermissionrequeststatus p, contactpermissionrequests cpr WHERE c.userId='" . $userId . "' AND ( c.firstName LIKE '%" . $search . "%' OR c.lastName LIKE '%" . $search . "%' OR p.title LIKE '%" . $search . "%') AND p.statusId=cpr.statusId AND c.contactId=cpr.toContactId AND c.isActive=1 order by c.firstName ");
		}

		public function GetAllArchivedContacts($userId) {
			$database = new database();
			$contacts = $database->executeObjectList("SELECT
    contactId, firstName,lastName, archiveDate
FROM
    contact
 where userId='" . $userId . "'  AND isActive=0 order by firstName");

			return $contacts;
		}

		public function restoreContact($contactId) {
			$database = new database();
			$database->executeNonQuery("UPDATE contact SET isActive=1 WHERE contactId='" . $contactId . "'");

			return ($database->getAffectedRows() != '' ? 1 : 0);
		}

		public function restoreAllContacts($contactIds) {
			$database = new database();
			$database->executeNonQuery("UPDATE contact SET isActive=1 WHERE contactId IN (" . $contactIds . ") ");

			return ($database->getAffectedRows() != '' ? 1 : 0);
		}

		public function deleteAllContacts($contactIds) {
			$database = new database();
			$database->executeNonQuery("DELETE FROM `contactaddresses` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactcalltime` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactnotes` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM  contactpermissionrequests WHERE toContactId IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactphonenumbers` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactwebsites` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `emailcontacts` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `groupcontacts` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `pageformreply` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `pageformreplyanswers` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM  planstepusers where contactId  IN (" . $contactIds . ")");
			$tasks = $database->executeObjectList("Select t.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId where tc.contactId IN (" . $contactIds . ")");
			foreach ($tasks as $task) {
				$taskId = $database->executeScalar("select taskId from taskcontacts where contactId not IN (" . $contactIds . ") and taskId=" . $task->taskId . "");
				if (empty($taskId)) {
					$database->executeNonQuery("DELETE FROM  tasks  WHERE taskId= '" . $task->taskId . "'");
				}
			}
			$database->executeNonQuery("DELETE FROM  taskcontacts  WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM contactPlansCompleted WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM contact WHERE `contactId` IN (" . $contactIds . ")");

			return 1;
		}

		public function deleteOneContact($contactIds) {
			$database = new database();
			$database->executeNonQuery("DELETE FROM `contactaddresses` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactcalltime` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactnotes` WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM  contactpermissionrequests WHERE toContactId IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactphonenumbers` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `contactwebsites` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `emailcontacts` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `groupcontacts` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `pageformreply` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM `pageformreplyanswers` WHERE `contactId`  IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM  planstepusers where contactId  IN (" . $contactIds . ")");
			$tasks = $database->executeObjectList("Select t.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId where tc.contactId IN (" . $contactIds . ")");
			foreach ($tasks as $task) {
				$taskId = $database->executeScalar("select taskId from taskcontacts where contactId not IN (" . $contactIds . ") and taskId=" . $task->taskId . "");
				if (empty($taskId)) {
					$database->executeNonQuery("DELETE FROM  tasks  WHERE taskId= '" . $task->taskId . "'");
				}
			}
			$database->executeNonQuery("DELETE FROM  taskcontacts  WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM contactPlansCompleted WHERE `contactId` IN (" . $contactIds . ")");
			$database->executeNonQuery("DELETE FROM contact WHERE `contactId` IN (" . $contactIds . ")");

			return 1;
		}

		public function archiveMultipleContacts($contactIds) {
			$database = new database();
			$database->executeNonQuery("UPDATE contact SET isActive=0, archiveDate=NOW() WHERE contactId IN (" . $contactIds . ") ");

			return ($database->getAffectedRows() != '' ? 1 : 0);
		}

		public function getContactProfiles($contactId) {
			$database = new database();
			$contactProfiles = $database->executeObjectList("select DISTINCT  pfr.pageId,  LEFT(pageBrowserTitle, 1) as ProfileName from pages p left join  pageformreply pfr on p.pageId=pfr.pageId where contactId='" . $contactId . "'  group by pfr.pageId ");
			$profiles_array = array();
			$i = 0;
			foreach ($contactProfiles as $cp) {
				//if($cp->counter>1)
				$profiles_array[$i] = $cp->ProfileName;
				$i++;
			}

			return $profiles = implode(", ", $profiles_array);
		}

		public function getContactProfilesConcat($contactId) {
			$database = new database();

			return $contactProfiles = $database->executeScalar("SELECT GROUP_CONCAT(DISTINCT(LEFT(pageBrowserTitle,1)) ORDER BY pageBrowserTitle)  AS ProfileName FROM pages p LEFT JOIN  pageformreply pfr ON p.pageId=pfr.pageId WHERE contactId='" . $contactId . "' ");
		}

		function GetlevelContacts($userId, $levelId) {
			$database = new database();
			$levels = explode(',', $levelId);
			$filter = "";
			if (!empty($levelId)) {
				$filter = "AND(";
				$count = 0;
				foreach ($levels as $l) {
					if ($count > 0) {
						$filter .= "OR c.interest LIKE '%" . $l . "%'";
					}
					else {
						$filter .= "c.interest LIKE '%" . $l . "%'";
					}
					$count++;
				}
				$filter .= ")";
				$contacts = $database->executeObjectList("SELECT
    DISTINCT(c.contactId),c.profiles,c.email,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId

	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
    WHERE c.userId='" . $userId . "'  " . $filter . " AND c.isActive=1 order by firstName");
				/*$contacts=$database->executeObjectList("SELECT
		c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
	FROM
		contact c

		LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
			) as gpr ON c.contactId=gpr.contactId
		LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

			  where c.userId='".$userId."' ".$filter." AND c.isActive=1 order by c.firstName");*/
			}
			else {
				$contacts = $database->executeObjectList("SELECT
    DISTINCT(c.contactId),c.profiles,cas.gmt, cpr.statusId, cprs.imagePath, cprs.title, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
FROM
    contact c

    LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) AS latestReply FROM pageformreply pfr WHERE pfr.address<>'' GROUP BY pfr.contactId
		) AS gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply

	LEFT JOIN (SELECT t2.statusId,t1.toContactId FROM (SELECT toContactId, MAX(permissionRequestId) AS maxPermissionRequestId FROM contactpermissionrequests GROUP BY (toContactId)) AS t1
	INNER JOIN contactpermissionrequests AS t2 ON t1.maxPermissionRequestId = t2.`permissionRequestId`)
	AS cpr ON c.contactId = cpr.tocontactId

	LEFT JOIN contactpermissionrequeststatus cprs ON cprs.statusId = cpr.statusId
 	LEFT JOIN contactaddresses cas ON c.contactId = cas.contactId
    WHERE c.userId='" . $userId . "' AND c.isActive=1 order by firstName");
				/*$contacts=$database->executeObjectList("SELECT
	c.contactId, c.firstName,c.lastName,c.phoneNumber,c.interest, pfr1.addDate
	FROM
	contact c

	LEFT JOIN ( SELECT pfr.contactId, MAX(replyId) as latestReply from pageformreply pfr where pfr.address<>'' group by pfr.contactId
		) as gpr ON c.contactId=gpr.contactId
	LEFT JOIN pageformreply pfr1 ON pfr1.replyId=gpr.latestReply where c.userId='".$userId."' AND c.isActive=1 order by firstName");*/
			}

			return $contacts;
		}

		function GetpermissionContacts($userId, $levelId) {
			$database = new database();
			$levels = explode(',', $levelId);
			$filter = "";
			if (!empty($levelId)) {
				$filter = "AND(";
				$count = 0;
				foreach ($levels as $l) {
					if ($count > 0) {
						$filter .= "OR cprs.title LIKE '%" . $l . "%'";
					}
					else {
						$filter .= "cprs.title LIKE '%" . $l . "%'";
					}
					$count++;
				}
				$filter .= ")";
				$contacts = $database->executeObjectList("SELECT c.contactId, firstName, lastName, interest,cprs.title FROM contact c  left join contactpermissionrequests cpr on cpr.toContactId=c.contactId left join contactpermissionrequeststatus cprs on cpr.statusId=cprs.statusId
		 where c.userId='" . $userId . "' " . $filter . "  AND c.isActive=1 GROUP BY c.contactId order by firstName");
			}
			else {
				$contacts = $database->executeObjectList("SELECT c.contactId, firstName, lastName, interest,cprs.title FROM contact c  left join contactpermissionrequests cpr on cpr.toContactId=c.contactId left join contactpermissionrequeststatus cprs on cpr.statusId=cprs.statusId
		 where c.userId='" . $userId . "' AND c.isActive=1 GROUP BY c.contactId order by firstName");
			}

			return $contacts;
		}

		function queryValueUpcomingTasks($leaderId) {
			$database = new database();

			return $database->executeScalar("Select `uct`.`queryValue` from `user` u left join `tasksupcoming` `uct` ON `u`.`upComingTasks` = `uct`.`id` where `u`.`userId` = '" . $leaderId . "'");
		}

		function getCurrentAssociatedGroups($database, $contactId, $userId) {
			return $database->executeObjectList("SELECT gc.*,p.* FROM `groupcontacts` gc LEFT JOIN plan p ON p.`groupId` = gc.`groupId` WHERE gc.contactId = '" . $contactId . "' AND p.`userId` = '" . $userId . "'");
		}

		function deleteContactPlanLinkedtoGroup($database, $contactId, $userId, $groupId, $planId) {
			$palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $planId . "' order by order_no asc");
			foreach ($palnSteps as $ps) {
				$database->executeNonQuery("DELETE from planstepusers where stepId='" . $ps->stepId . "' and contactId='" . $contactId . "'");
				$tasks = $database->executeObjectList("SELECT tc.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId  where userId='" . $userId . "' and tc.contactId='" . $contactId . "' and planId='" . $planId . "'");
				foreach ($tasks as $t) {
					$database->executeNonQuery("DELETE from tasks where taskId='" . $t->taskId . "' and isCompleted<>1");
					$isDeleted = $database->getAffectedRows();
					if ($isDeleted > 0) {
						$database->executeNonQuery("DELETE  FROM taskplan WHERE taskId='" . $t->taskId . "' and planId='" . $ps->planId . "' and stepId='" . $ps->stepId . "'");
						$database->executeNonQuery("DELETE  FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $t->taskId . "'");
					}
				}
			}
			$database->executeNonQuery("DELETE  FROM contactPlansCompleted WHERE planId='" . $planId . "' and  contactId='" . $contactId . "'");
			$database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "' and planId='" . $planId . "'");
			$database->executeNonQuery("DELETE FROM groupcontacts WHERE contactId='" . $contactId . "' and `groupId` = '" . $groupId . "'");
		}

		function getAllimportcontactDetails($contactId, $database) {

			return $database->executeObjectList("select * from `importcontactdetail` where `contactId` ='" . $contactId . "'");


		}
        function GetPermissionEmail($userId){
            $database = new database();
            $getPermissionEmail = $database->executeObject("SELECT permissionEmailBody,permissionEmailSubject from userpermissionemail where userId='".$userId."'");

            return  $getPermissionEmail;
        }
	}

?>