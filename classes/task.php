<?php
    require_once('init.php');
    include_once("class.smtpmail.php");

    class task
    {

        function moveToHistory($contactId, $saveNotes, $noteId)
        {
            $database = new database();
            $addedDate = date("Y-n-j");
            $database->executeNonQuery("UPDATE contactnotes SET
														 addedDate=CURDATE(),
														 isActive=0 WHERE noteId='" . $noteId . "' AND contactId='" . $contactId . "'");

            return $noteId;

        }

        function showHistory($noteId)
        {
            $database = new database();

            return $database->executeObject("SELECT noteId, cn.contactId, cn.notes, cn.addedDate, cn.lastModifiedDate, cn.isActive, CONCAT(firstName,' ',lastName) as name FROM contactnotes cn join contact c on cn.contactId=c.contactId WHERE  cn.isActive=0 AND  noteId='" . $noteId . "'");

        }

        function stickyNotes($stickyNotes, $contactId)
        {
            $database = new database();
            $addedDate = date("Y-n-j");
            if ($stickyNotes != '' || $stickyNotes != NULL)
            {
                $database->executeNonQuery("INSERT INTO contactnotes SET contactId='" . $contactId . "',
														 notes='" . mysql_real_escape_string($stickyNotes) . "',
														 addedDate=CURDATE(),
														 isActive='1'");

                return $database->insertid();
            }
            else
                return 0;

        }

        function updateStickyNotes($notes, $contactId, $noteId)
        {
            $database = new database();
            $modifiedDate = date("Y-n-j");
            if ($notes != '' || $notes != NULL)
            {
                $database->executeNonQuery("UPDATE contactnotes SET notes='" . mysql_real_escape_string($notes) . "' , lastModifiedDate='" . $modifiedDate . "' WHERE noteId='" . $noteId . "' AND contactId='" . $contactId . "'");
                if ($database->getAffectedRows() != '')
                {
                    return 1;
                }
            }
            else
            {
                return 0;
            }
        }

        function showStickyNotes($contactId)
        {
            $database = new database();

            return $database->executeObject("SELECT notes, noteId FROM contactnotes WHERE contactId='" . $contactId . "' AND isActive=1 ORDER BY noteId DESC");
        }

        function checkStickyNotesAlreadyExist($contactId)
        {
            $database = new database();
            $notes = $database->executeObject("SELECT * FROM contactnotes WHERE contactId='" . $contactId . "' AND isActive=1");
            if (empty($notes))
            {
                return 1;

            }
            else
            {
                return $notes->noteId;
            }
        }

        function checkStickyNotesAlreadyExist2($contactId, $notes)
        {
            $database = new database();
            $notes = $database->executeObject("SELECT * FROM contactnotes WHERE contactId='" . $contactId . "' AND isActive=1 AND notes='" . $notes . "'");
            if (empty($notes))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        function upcomingActivities($userId, $contactId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks  WHERE userId='" . $userId . "' AND taskId IN (select taskId FROM taskcontacts WHERE contactId='" . $contactId . "')  AND isCompleted!=1");
        }

        function ActivitiesHistory($userId, $contactId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks  WHERE userId='" . $userId . "' AND taskId IN (select taskId FROM taskcontacts WHERE contactId='" . $contactId . "')  AND isCompleted=1 ");

        }

        function contactNotes($contactId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT noteId, cn.contactId, cn.notes, cn.addedDate, cn.lastModifiedDate, cn.isActive, CONCAT(firstName,' ',lastName) as name FROM contactnotes cn join contact c on cn.contactId=c.contactId WHERE cn.contactId='" . $contactId . "' AND cn.isActive=0");
        }

        function deleteTask($taskId)
        {
            $database = new database();
            $database->executeNonQuery("DELETE FROM taskcontacts WHERE taskId='" . $taskId . "'");
            if ($database->getAffectedRows() != '')
            {
                return 1;
            }
        }

        function deleteNotes($noteid)
        {
            $database = new database();
            $database->executeNonQuery("DELETE FROM contactnotes WHERE noteId='" . $noteid . "'");
            if ($database->getAffectedRows() != '')
            {
                return 1;
            }
        }

        //new task screen starts
        function taskPriority()
        {
            $database = new database();

            return $database->executeObjectList("SELECT priorityId,priority FROM taskpriority ");
        }

        function reminder()
        {
            $database = new database();

            return $database->executeObjectList("SELECT reminderTypeId,text,numberOfMinutes FROM remindertype ");
        }

        function occur()
        {
            $database = new database();

            return $database->executeObjectList("SELECT repeatTypeId,type FROM repeattypes ");

        }

        function TaskEdit($taskId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM tasks WHERE taskId='" . $taskId . "'");

        }

        function getTaskReminder($taskId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM taskreminder WHERE taskId='" . $taskId . "'");
        }

        function getTaskCustomRule($customRuleId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM customrules WHERE customRuleId='" . $customRuleId . "'");
        }

        function getTaskCustomRuledaily($customRuleId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM customrulesdaily WHERE customRuleId='" . $customRuleId . "'");

        }

        function getTaskCustomRuleWeekly($customRuleId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM customruleweekly WHERE customRuleId='" . $customRuleId . "'");
        }

        function getTaskCustomRuleMonthly($customRuleId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM customrulemonthly WHERE customRuleId='" . $customRuleId . "'");
        }

        function getTaskCustomRuleYearly($customRuleId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM customrulesyearly WHERE customRuleId='" . $customRuleId . "'");
        }

        function deleteTaskOnContactDetail($taskId, $contactId)
        {
            $database = new database();
            $database->executeNonQuery("DELETE FROM taskcontacts WHERE taskId='" . $taskId . "' AND contactId='" . $contactId . "'");
            $otherContacts = $database->executeScalar("SELECT count(*) as otherContacts FROM taskcontacts WHERE taskId='" . $taskId . "'");
            if ($otherContacts == 0)
            {
                $database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $taskId . "'");
            }

            if ($database->getAffectedRows() != '')
            {
                return 1;
            }

        }

        function GetAllUserTask($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted!=1  AND isPlanReminder=0
			AND isActive=0");
        }

        function GetTaskPriority($pId)
        {
            $database = new database();
            $detail = $database->executeObject("SELECT priority FROM taskpriority WHERE priorityId='" . $pId . "'");

            return $detail;

        }

        function GetAllUserTaskHistory($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted=1 order by completedDate DESC");
        }

        function showTaskHistoryByLimit($userId, $limit)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted=1  AND dueDateTime < SYSDATE()  order by completedDate LIMIT 0," . $limit . "");
        }

        function taskContact($contactId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "' AND isActive=1");
        }

        function getcontactId($taskId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT c.contactId, c.firstName, c.lastName FROM contact c, taskcontacts tc WHERE taskId='" . $taskId . "' and c.contactId=tc.contactId AND c.isActive=1");

        }

        function getcontactId2($taskId, $userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT c.contactId, c.firstName, c.lastName FROM contact c, taskcontacts tc WHERE taskId='" . $taskId . "' and c.contactId=tc.contactId and c.userId='" . $userId . "' AND c.isActive=1 ");

        }

        function getcontactIds($taskId)
        {
            $database = new database();

            return $database->executeScalar("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "'");

        }

        function gettaskContacts($taskId)
        {
            $database = new database();
            $result = $database->executeObjectList("SELECT contactId as id, concat(firstName,' ',lastName) as name from contact WHERE contactId IN (SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "') AND isActive=1");
            $rar = '';
            foreach ($result as $r)
            {
                $rar .= '{"id":"' . $r->id . '","name":"' . $r->name . '"},';
            }

            return $rar;
        }

        function archiveTaskOnTaskView($taskId)
        {
            $database = new database();
            $database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $taskId . "'");
            if ($database->getAffectedRows() != '')
            {
                return 1;
            }
        }

        function skipTaskOnTaskView($taskId, $contactId)
        {
            $database = new database();
            $cid = $contactId;
            $plan_info = $database->executeObject("SELECT planId,userId FROM tasks WHERE  taskId='" . $taskId . "'");
            //echo 'PlanId'.$plan_id;
            if (!empty($plan_info->planId))
            {
                $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $plan_info->userId . "'");
                $userId = $userInfo->userId;
                if (empty($contactId) || $contactId = '')
                {
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "'");
                }
                else
                {
                    //echo $cid;
                    //echo "SELECT contactId FROM taskcontacts WHERE  taskId='".$taskId."' AND contactId IN (".$cid.")";
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "' AND contactId IN (" . $cid . ")");
                }
                if (!empty($contactIds))
                {
                    foreach ($contactIds as $cc)
                    {
                        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $cc->contactId . "'");
                        $x = 0;
                        $contactSteps = array();
                        foreach ($existingSteps as $exists)
                        {
                            $contactSteps[$x] = $exists->stepId;
                            $x++;
                        }
                        //echo "SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '".$cc->contactId."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by order_no limit 1";
                        //echo '--------';
                        //$thisstep=$database->executeScalar("SELECT stepId from  taskplan where taskId='".$taskId."' and planId='".$plan_id."'");
                        /*echo "SELECT * FROM plansteps WHERE order_no > (select order_no from plansteps where stepId='".$thisstep."') AND planId='".$plan_id."' order by order_no asc LIMIT 1";
                        echo '--------';*/
                        //echo "SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '".$cc->contactId."' and stepId IN (select stepId from plansteps  where planId='".$plan_info->planId."')) and planId='".$plan_info->planId."' order by order_no limit 1";
                        $stepInfo = $database->executeObject("SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '" . $cc->contactId . "' and stepId IN (select stepId from plansteps  where planId='" . $plan_info->planId . "')) and planId='" . $plan_info->planId . "' order by order_no limit 1");

                        $stepInfo = $database->executeObject("SELECT psu.stepId FROM planstepusers psu LEFT JOIN plansteps ps ON psu.`stepId` = ps.`stepId` WHERE psu.contactId='" . $cc->contactId . "' AND ps.planId = '" . $plan_info->planId . "'");

                        $thisstep = $database->executeScalar("SELECT stepId from  taskplan where taskId='" . $taskId . "' and planId='" . $plan_info->planId . "'");
                        $skipStepInfo = $database->executeObject("SELECT * FROM plansteps  WHERE  stepId='" . $thisstep . "'");
                        //echo "SELECT * FROM plansteps  WHERE stepId >(select stepId from planstepusers where contactId = '".$contact_id."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by stepId limit 1";
                        //echo '--------';
                        //echo "DELETE FROM planstepusers WHERE contactId='".$cc->contactId."' and stepId='".$thisstep."'";
                        $database->executeNonQuery("DELETE FROM planstepusers WHERE contactId='" . $cc->contactId . "' and stepId='" . $thisstep . "'");
                        //echo "INSERT INTO contacttaskskipped(taskId, stepId, planId, contactId, addDate, title) VALUES ('".$taskId."','".$stepInfo->stepId."','".$plan_info->planId."','".$cc->contactId."',NOW(),'".$stepInfo->summary."')";
                        $database->executeNonQuery("INSERT INTO `contacttaskskipped`(`taskId`, `stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES ('" . $taskId . "','" . $skipStepInfo->stepId . "','" . $skipStepInfo->planId . "','" . $cc->contactId . "',NOW(),'" . mysql_real_escape_string($skipStepInfo->summary) . "')");
                        //echo "DELETE FROM tasks WHERE taskId='".$taskId."'";
                        $database->executeNonQuery("DELETE FROM tasks WHERE taskId='" . $taskId . "'");
                        /*$taskId2=$database->executeScalar("select taskId from taskcontacts where contactId<>'".$cc->contactId."' and taskId=".$taskId."");
                        if(empty($taskId2))
                        {

                        $database->executeNonQuery("DELETE FROM  tasks  WHERE taskId= '".$taskId."'");
                        }*/
                        //$database->executeNonQuery("DELETE FROM taskcontacts  WHERE taskId='".$taskId."' and contactId='".$cc->contactId."'");
                        //Assign next step - copy from profile.php by Atif
                        //$i=0;
                        $plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $plan_info->planId . "'");
                        //echo '--------';
                        //echo 'PlanId'.$plan->planId;
                        //echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$stepInfo->stepId."') AND planId='".$plan_id."' order by stepId asc";
                        //echo '--------';

                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no > (select order_no from plansteps where stepId='" . $stepInfo->stepId . "') AND planId='" . $plan->planId . "' and isactive = 1 and isArchive = 0 order by order_no asc");
                        //delete all next skiped task
                        /*$y=0;
                        $SkippedSteps=array();
                        foreach($palnSteps as $ps)
                        {
                            $SkippedSteps[$y]=$ps->stepId;
                            $y++;

                        }
                        $skipSteps=implode(",",$SkippedSteps);
                        //echo "delete from contacttaskskipped where planId='".$plan->planId."' and stepId IN(".$skipSteps.") and contactId='".$cc->contactId."'";
                        $database->executeNonQuery("delete from contacttaskskipped where planId='".$plan->planId."' and stepId IN(".$skipSteps.") and contactId='".$cc->contactId."'");
                        */
                        //echo "totalDel".$database->getAffectedRows();
                        $date = date('Y-m-d');
                        $date = strtotime($date);
                        $totalSteps = sizeof($palnSteps);
//						echo "Total Steps:". $totalSteps.'<br>';
                        $stepCounter = 1;
                        if ($totalSteps == 0)
                        {
                            //echo "INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('".$thisstep."','".$plan_id."','".$cc->contactId."',CURDATE())";
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $thisstep . "','" . $plan->planId . "','" . $cc->contactId . "',CURDATE())");
                        }
                        else
                        {
                            foreach ($palnSteps as $ps)
                            {
                                //echo "next plan step";
                                $newContactSteps[$y] = $ps->stepId;
                                $y++;
                                if (!in_array($ps->stepId, $contactSteps))
                                {
                                    $planId2 = self::CheckLastStepOfPlan($plan->planId, $cc->contactId);
                                    if (!empty($planId2))
                                    {
                                        echo '-11';
                                        $database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $cc->contactId . "'");
                                    }
                                    echo 'not exists' . $ps->stepId;
                                    echo '<br />';
                                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                                    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $cc->contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                                    $newStepId = $database->insertid();
                                    //echo 'inserted';
                                    $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "' AND userId='" . $userId . "'");
                                    if ($ps->isTask == 0 && $PermissionStatus != '4')
                                    {
                                        if ($ps->daysAfter == 0)
                                        {
                                            /*echo "\r\nemail step";
                                            echo '<br>';*/
                                            //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                            $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "'");
                                            if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                            {
                                                /*echo 'emial allowed';
                                                echo '<br>';*/
                                                $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $cc->contactId . "'");
                                                $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                                preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                                foreach ($match[1] as $key)
                                                {
                                                    if ($key == "First Name")
                                                    {
                                                        $firstname = 'firstName';
                                                    }
                                                    if ($key == "Last Name")
                                                    {
                                                        $lastname = 'lastName';
                                                    }
                                                    if ($key == "Phone Number")
                                                    {
                                                        $phonenumber = 'phoneNumber';
                                                    }
                                                    if ($key == "Company Name")
                                                    {
                                                        $companyname = 'companyTitle';
                                                    }
                                                    if ($key == "@Signature")
                                                    {
                                                        $signature = self::getSignatures($database, $userId);
                                                    }

                                                }
                                                if (!empty($firstname))
                                                {
                                                    $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($lastname))
                                                {
                                                    $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($companyname))
                                                {
                                                    $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($phonenumber))
                                                {
                                                    $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $cc->contactId . "'");
                                                }
                                                $subject = $mailBody->subject;
                                                $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                                $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                                $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                                $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                                $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                                $message = str_replace("\'", "'", $rplbysig);
                                                $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                                    "Reply-To: " . $userInfo->email . "\r\n";
                                                $headers .= "Content-Type: text/html";
                                                //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                                $mail = new smtpEmail();
                                                $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject,$mailBody->id);
                                                //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                                //echo "INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
                                                //('".$plan->planId."', '".$stepDesc->stepId."','".$cc->contactId."', '".$userId."', '".$contactemail."', '".$userInfo->email."', '".mysql_real_escape_string($subject)."', '".mysql_real_escape_string($message)."', NOW())";
                                                $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
															('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $cc->contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                                $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $cc->contactId . "'");
                                                if ($totalSteps == $stepCounter)
                                                {
                                                    $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $cc->contactId . "',CURDATE())");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            /*echo 'breaking days after not zero';
                                            echo '<br>';*/
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        //echo "<br /> task step<br />";
                                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                        $taskId = $database->insertid();
                                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $cc->contactId . "',taskId='" . $taskId . "'");
                                        /*echo '<br />';
                                        echo 'breaking task occured';*/
                                        break;
                                    }

                                }
                                else
                                {
                                    /*echo '<br />';
                                    echo 'no change';*/
                                    break;
                                }
                                $stepCounter++;

                            }
                        }

                    }
                }

            }

            return 1;

        }

        //get tasks date wise

        function CheckLastStepOfPlan($planId, $contactId)
        {
            $database = new database();

            return $database->executeScalar("select planId from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");

        }

        function getSignatures($database, $userId){
            $sql = "SELECT `signature` FROM `usersignature` WHERE `userId` = '".$userId."'";
            return $database->executeScalar($sql);
        }


        function getTodayTasks($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks t, taskcontacts tc WHERE t.userId='" . $userId . "' AND t.isCompleted!=1  AND DATE(t.dueDateTime)=CURDATE()  AND t.isPlanReminder=0 AND t.isActive=0 AND t.taskId=tc.taskId group by tc.taskId");
        }

        function getTodaysTask($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT t.taskId, t.title, dueDateTime, c.contactId FROM tasks t JOIN taskcontacts tc on t.taskId=tc.taskId JOIN contact c on tc.contactId=c.contactId WHERE t.userId='" . $userId . "' AND t.isCompleted!=1  AND DATE(t.dueDateTime)=CURDATE()  AND t.isPlanReminder=0 AND t.isActive=0  AND c.isActive=1  group by tc.taskId order by t.dueDateTime ASC LIMIT 15");
        }

        function getUpcomingtask5($userId, $interval)
        {
            $database = new database();
            if (empty($interval))
            {
                return $database->executeObjectList("SELECT t.taskId, t.title, dueDateTime, c.contactId FROM tasks t JOIN taskcontacts tc on t.taskId=tc.taskId JOIN contact c on tc.contactId=c.contactId WHERE t.userId='" . $userId . "' AND isCompleted!=1  AND DATE(dueDateTime)>CURDATE() AND isPlanReminder=0 AND t.isActive=0 AND c.isActive=1  group by tc.taskId order by t.dueDateTime ASC LIMIT 15");
            }
            else
            {
                return $database->executeObjectList("SELECT t.taskId, t.title, dueDateTime, c.contactId FROM tasks t JOIN taskcontacts tc on t.taskId=tc.taskId JOIN contact c on tc.contactId=c.contactId WHERE t.userId='" . $userId . "' AND isCompleted!=1  AND isPlanReminder=0 AND t.isActive=0 AND c.isActive=1 AND (DATE(dueDatetime) BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL " . $interval . ")) GROUP BY tc.taskId order by t.dueDateTime ASC ");
            }

        }

        function getOverdueTask5($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT t.taskId, t.title, dueDateTime, c.contactId FROM tasks t JOIN taskcontacts tc on t.taskId=tc.taskId JOIN contact c on tc.contactId=c.contactId WHERE t.userId='" . $userId . "' AND isCompleted!=1  AND dueDateTime<CURDATE() AND dueDateTime!='0000-00-00 00:00:00' AND isPlanReminder=0 AND t.isActive=0 AND c.isActive=1 group by tc.taskId order by t.dueDateTime ASC LIMIT 15");
        }

        function getUpcomingtasks($userId)
        {
            $database = new database();

            //return $database->executeObjectList("SELECT * FROM tasks WHERE userId='".$userId."' AND isCompleted!=1  AND DATE(dueDateTime)>CURDATE() AND isPlanReminder=0 AND isActive=0");
            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted!=1  AND dueDateTime BETWEEN DATE_ADD(CURDATE(),INTERVAL 1 DAY)  AND DATE_ADD(CURDATE(),INTERVAL 30 DAY) AND isPlanReminder=0 AND isActive=0 order by dueDateTime ASC");
        }

        function getOverdueTasks($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted!=1  AND dueDateTime<CURDATE() AND dueDateTime!='0000-00-00 00:00:00' AND isPlanReminder=0 AND isActive=0 order by dueDateTime ASC");
        }

        function getGeneralTasks($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted!=1  AND dueDateTime='0000-00-00 00:00:00' AND isPlanReminder=0 AND isActive=0 order by dueDateTime ASC");
        }

        //assign next plan step to contacts if exist

        function GetPlanReminderTasks($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM tasks WHERE userId='" . $userId . "' AND isCompleted!=1  AND isPlanReminder=1 AND isActive=0 order by dueDateTime ASC");

        }

        function getTaskContact($taskId)
        {
            $database = new database();
            $contact = $database->executeScalar("SELECT firstName FROM contact c JOIN taskcontacts tc on c.contactId=tc.contactId WHERE taskId='" . $taskId . "'");

            return $contact;
        }

        function getTaskContactId($taskId)
        {
            $database = new database();
            $contact = $database->executeScalar("SELECT c.contactId FROM contact c JOIN taskcontacts tc on c.contactId=tc.contactId WHERE taskId='" . $taskId . "'");

            return $contact;
        }

        function assignNextStepToContact($userId, $planId, $step, $stepId, $contactId)
        {

            $database = new database();
            $date = date('Y-m-d');
            $date = strtotime($date);

            $nextStep = $database->executeObject("SELECT * FROM plansteps WHERE planId='" . $planId . "' AND order_no>(select order_no from plansteps where summary='" . $step . "') order by order_no  asc limit 1");

            if (!empty($nextStep))
            {
                //$contactId=getTaskContactId($stepId);

                $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $nextStep->stepId . "',contactId='" . $contactId . "'");

                $dueDate = strtotime("+" . $nextStep->daysAfter . " day", $date); //add days in date

                $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($nextStep->summary) . "','0','','','" . date('Y-m-d', $dueDate) . "','0','" . $date . "','0','0','" . $nextStep->planId . "','','0','1')");

                $taskId = $database->insertid();

                $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");

                return 1;
            }


        }

        function updateTask($taskId, $userId)
        {

            $database = new database();
            $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate=NOW() WHERE taskId='" . $taskId . "'");
            $plan_id = $database->executeScalar("SELECT planId FROM tasks WHERE  taskId='" . $taskId . "'");
            //echo 'PlanId'.$plan_id;
            if (!empty($plan_id))
            {
                $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
                $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "'");
                if (!empty($contactIds))
                {
                    foreach ($contactIds as $cc)
                    {
                        //echo "SELECT contactId FROM taskcontacts WHERE  taskId='".$taskId."'";
                        //echo '--------';
                        $stepInfo = $database->executeObject("SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '" . $cc->contactId . "' and stepId IN (select stepId from plansteps  where planId='" . $plan_id . "')) and planId='" . $plan_id . "' order by order_no limit 1");
                        $thisstep = $database->executeScalar("SELECT stepId from  taskplan where taskId='" . $taskId . "' and planId='" . $plan_id . "'");
                        //echo "SELECT * FROM plansteps  WHERE stepId >(select stepId from planstepusers where contactId = '".$contact_id."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by stepId limit 1";
                        //echo '--------';
                        //echo "DELETE FROM planstepusers WHERE contactId='".$contact_id."' and stepId IN (select stepId from plansteps where planId='".$plan_id."')";
                        $database->executeNonQuery("DELETE FROM planstepusers WHERE contactId='" . $cc->contactId . "' and stepId IN (select stepId from plansteps where planId='" . $plan_id . "')");
                        /*$taskId2=$database->executeScalar("select taskId from taskcontacts where contactId<>'".$cc->contactId."' and taskId=".$taskId."");
                        if(empty($taskId2))
                        {

                        $database->executeNonQuery("DELETE FROM  tasks  WHERE taskId= '".$taskId."'");
                        }*/
                        //$database->executeNonQuery("DELETE FROM taskcontacts  WHERE taskId='".$taskId."' and contactId='".$cc->contactId."'");
                        //Assign next step - copy from profile.php by Atif
                        //$i=0;
                        $plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $plan_id . "'");
                        //echo '--------';
                        //echo 'PlanId'.$plan->planId;
                        //echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$stepInfo->stepId."') AND planId='".$plan_id."' order by stepId asc";
                        //echo '--------';
                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='" . $stepInfo->stepId . "') AND planId='" . $plan_id . "' order by order_no asc");
                        $date = date('Y-m-d');
                        $date = strtotime($date);
                        $totalSteps = sizeof($palnSteps);
                        $stepCounter = 1;
                        if ($totalSteps == 0)
                        {
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepInfo->stepId . "','" . $plan_id . "','" . $cc->contactId . "',CURDATE())");
                        }
                        else
                        {
                            foreach ($palnSteps as $ps)
                            {
                                /*
                                $newContactSteps[$y]=$ps->stepId;
                                $y++;
                                */
                                $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                                $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                                $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $cc->contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                                $newStepId = $database->insertid();
                                $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "' AND userId='" . $userId . "'");
                                if ($ps->isTask == 0 && $PermissionStatus != '4')
                                {
                                    if ($ps->daysAfter == 0)
                                    {
                                        $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "'");
                                        if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                        {
                                            /*echo 'emial allowed';
                                            echo '<br>';*/
                                            $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $cc->contactId . "'");
                                            $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                            preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                            foreach ($match[1] as $key)
                                            {
                                                if ($key == "First Name")
                                                {
                                                    $firstname = 'firstName';
                                                }
                                                if ($key == "Last Name")
                                                {
                                                    $lastname = 'lastName';
                                                }
                                                if ($key == "Phone Number")
                                                {
                                                    $phonenumber = 'phoneNumber';
                                                }
                                                if ($key == "Company Name")
                                                {
                                                    $companyname = 'companyTitle';
                                                }
                                                if ($key == "@Signature")
                                                {
                                                    $signature = self::getSignatures($database, $userId);
                                                }

                                            }
                                            if (!empty($firstname))
                                            {
                                                $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $cc->contactId . "'");
                                            }
                                            if (!empty($lastname))
                                            {
                                                $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $cc->contactId . "'");
                                            }
                                            if (!empty($companyname))
                                            {
                                                $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $cc->contactId . "'");
                                            }
                                            if (!empty($phonenumber))
                                            {
                                                $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $cc->contactId . "'");
                                            }
                                            $subject = $mailBody->subject;
                                            $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                            $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                            $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                            $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                            $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                            $message = str_replace("\'", "'", $rplbysig);
                                            $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                                "Reply-To: " . $userInfo->email . "\r\n";
                                            $headers .= "Content-Type: text/html";
                                            //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                            $mail = new smtpEmail();
                                            $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject,$mailBody->id);
                                            //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                            $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $cc->contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                            $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $cc->contactId . "'");
                                            if ($totalSteps == $stepCounter)
                                            {
                                                $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan_id . "','" . $cc->contactId . "',CURDATE())");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    //echo "-------";
                                    //echo "INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId."','".$stepDesc->summary."','0','','','".date('Y-m-d',$dueDate)."','0','".date('Y-m-d')."','0','0','".$stepDesc->planId."','','0','1')";
                                    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                    $newTaskId = $database->insertid();
                                    //echo "-------";
                                    //echo $newTaskId;
                                    //echo "-------";
                                    //echo "INSERT INTO taskplan set taskId='".$newTaskId."',planId='".$stepDesc->planId."',stepId='".$stepDesc->stepId."'";
                                    $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $newTaskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                                    //echo "-------";
                                    //echo "INSERT INTO taskcontacts SET contactId='".$contact_id."',taskId='".$newTaskId."'";
                                    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $cc->contactId . "',taskId='" . $newTaskId . "'");
                                    break;
                                }
                                $stepCounter++;
                            }
                        }

                    }
                }
            }
            /* if($database->getAffectedRows()!='')
                 {
                 return 1;
                 }   */
            echo 1;
        }

        function updateTaskAgainstContact($taskId, $contact_id, $userId)
        {
            $database = new database();
            $database->executeNonQuery("UPDATE tasks SET isCompleted=1, completedDate=NOW() WHERE taskId='" . $taskId . "'");
            $plan_id = $database->executeScalar("SELECT planId FROM tasks WHERE  taskId='" . $taskId . "'");
            //echo 'PlanId'.$plan_id;
            if (!empty($plan_id))
            {
                $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
                //echo "SELECT contactId FROM taskcontacts WHERE  taskId='".$taskId."'";
                //echo '--------';
                $stepInfo = $database->executeObject("SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '" . $contact_id . "' and stepId IN (select stepId from plansteps  where planId='" . $plan_id . "')) and planId='" . $plan_id . "' order by order_no limit 1");
                $thisstep = $database->executeScalar("SELECT stepId from  taskplan where taskId='" . $taskId . "' and planId='" . $plan_id . "'");
                //echo "SELECT * FROM plansteps  WHERE stepId >(select stepId from planstepusers where contactId = '".$contact_id."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by stepId limit 1";
                //echo '--------';
                //echo "DELETE FROM planstepusers WHERE contactId='".$contact_id."' and stepId IN (select stepId from plansteps where planId='".$plan_id."')";
                $database->executeNonQuery("DELETE FROM planstepusers WHERE contactId='" . $contact_id . "' and stepId IN (select stepId from plansteps where planId='" . $plan_id . "')");
                //Assign next step - copy from profile.php by Atif
                //$i=0;
                $plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $plan_id . "'");
                //echo '--------';
                //echo 'PlanId'.$plan->planId;
                //echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$stepInfo->stepId."') AND planId='".$plan_id."' order by stepId asc";
                //echo '--------';
                $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='" . $stepInfo->stepId . "') AND planId='" . $plan_id . "' order by order_no asc");
                $date = date('Y-m-d');
                $date = strtotime($date);
                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                if ($totalSteps == 0)
                {
                    $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $thisstep . "','" . $plan_id . "','" . $contact_id . "',CURDATE())");
                }
                else
                {
                    foreach ($palnSteps as $ps)
                    {
                        /*
                        $newContactSteps[$y]=$ps->stepId;
                        $y++;
                        */
                        $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                        $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                        $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contact_id . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                        $newStepId = $database->insertid();
                        $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contact_id . "'");
                        if ($ps->isTask == 0 && $PermissionStatus != '4')
                        {
                            if ($ps->daysAfter == 0)
                            {
                                $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contact_id . "'");
                                if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                {
                                    /*echo 'emial allowed';
                                    echo '<br>';*/
                                    $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contact_id . "'");
                                    $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                    preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                    foreach ($match[1] as $key)
                                    {
                                        if ($key == "First Name")
                                        {
                                            $firstname = 'firstName';
                                        }
                                        if ($key == "Last Name")
                                        {
                                            $lastname = 'lastName';
                                        }
                                        if ($key == "Phone Number")
                                        {
                                            $phonenumber = 'phoneNumber';
                                        }
                                        if ($key == "Company Name")
                                        {
                                            $companyname = 'companyTitle';
                                        }
                                        if ($key == "@Signature")
                                        {
                                            $signature = self::getSignatures($database, $userId);
                                        }

                                    }
                                    if (!empty($firstname))
                                    {
                                        $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contact_id . "'");
                                    }
                                    if (!empty($lastname))
                                    {
                                        $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contact_id . "'");
                                    }
                                    if (!empty($companyname))
                                    {
                                        $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contact_id . "'");
                                    }
                                    if (!empty($phonenumber))
                                    {
                                        $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contact_id . "'");
                                    }
                                    $subject = $mailBody->subject;
                                    $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                    $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                    $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                    $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                    $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                    $message = str_replace("\'", "'", $rplbysig);
                                    $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                        "Reply-To: " . $userInfo->email . "\r\n";
                                    $headers .= "Content-Type: text/html";
                                    //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                    $mail = new smtpEmail();
                                    $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject,$mailBody->id);
                                    //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                    $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contact_id . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                    $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contact_id . "'");
                                    if ($totalSteps == $stepCounter)
                                    {
                                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan_id . "','" . $contact_id . "',CURDATE())");
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            //echo "-------";
                            //echo "INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId."','".$stepDesc->summary."','0','','".mysql_escape_string($stepDesc->description)."','".date('Y-m-d',$dueDate)."','0','".date('Y-m-d')."','0','0','".$stepDesc->planId."','','0','1')";
                            $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                            $newTaskId = $database->insertid();
                            //echo "-------";
                            //echo $newTaskId;
                            //echo "-------";
                            //echo "INSERT INTO taskplan set taskId='".$newTaskId."',planId='".$stepDesc->planId."',stepId='".$stepDesc->stepId."'";
                            $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $newTaskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                            //echo "-------";
                            //echo "INSERT INTO taskcontacts SET contactId='".$contact_id."',taskId='".$newTaskId."'";
                            $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contact_id . "',taskId='" . $newTaskId . "'");
                            break;
                        }
                        $stepCounter++;
                    }

                }
            }
            if ($database->getAffectedRows() != '')
            {
                return 1;
            }

        }

        public function markTaskCompleted($userId, $taskId, $contactId, $completeDate, $completeTime)
        {
            // echo "ContactId:".$contactId." ";
            $cid = $contactId;
            $database = new database();
            $taskToEdit = $database->executeObject("SELECT * FROM  tasks WHERE taskId='" . $taskId . "'");
            $dueDateTime = $taskToEdit->dueDateTime;
            if ($taskToEdit->repeatTypeId == 1 || $taskToEdit->repeatTypeId == 0)
            {
                if ($completeDate == '0000-00-00' && $completeTime == '00:00:00')
                    $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate=NOW() WHERE taskId='" . $taskId . "'");
                else
                    $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate='" . $completeDate . " " . $completeTime . "' WHERE taskId='" . $taskId . "'");
            }
            else if (($taskToEdit->planId == 0 || $taskToEdit->planId == '' || $taskToEdit->planId == NULL) && ($taskToEdit->repeatTypeId != 1 || $taskToEdit->repeatTypeId != 0))
            {
                //echo "INSERT INTO tasks (userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId)  (Select userId, title, '1' as isCompleted, priorityId, notes, dueDateTime,'0' as recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, NOW() as completedDate, '0' as customRuleId, '1' as repeatTypeId, '".$taskId."' as  parentTaskId from tasks where taskId='".$taskId."')";
                if (empty($contactId) || $contactId = '')
                {
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "'");
                }
                else
                {
                    //echo $cid;
                    //echo "SELECT contactId FROM taskcontacts WHERE  taskId='".$taskId."' AND contactId IN (".$cid.")";
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "' AND contactId IN (" . $cid . ")");
                }
                if ($taskToEdit->repeatTypeId == 2)
                {
                    //echo "weekly task";
                    //echo "UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL 1 WEEK) WHERE taskId='".$taskId."'";
                    $database->executeNonQuery("UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL 1 WEEK) WHERE taskId='" . $taskId . "'");

                }
                if ($taskToEdit->repeatTypeId == 3)
                {
                    $database->executeNonQuery("UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL 1 MONTH) WHERE taskId='" . $taskId . "'");

                }
                if ($taskToEdit->repeatTypeId == 4)
                {
                    //echo "daily task";
                    //echo "UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL 1 DAY) WHERE taskId='".$taskId."'";
                    $database->executeNonQuery("UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL 1 DAY) WHERE taskId='" . $taskId . "'");

                }
                if ($taskToEdit->repeatTypeId == 5)
                {
                    $database->executeNonQuery("UPDATE tasks SET dueDateTime=IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL 1 DAY))=7,DATE_ADD(dueDateTime, INTERVAL 3 DAY),IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL 1 DAY))=1,DATE_ADD(dueDateTime, INTERVAL 2 DAY),DATE_ADD(dueDateTime, INTERVAL 1 DAY))
) WHERE  taskId='" . $taskId . "'");

                }
                if ($taskToEdit->repeatTypeId == 6)
                {
                    $customRuleId = $taskToEdit->customRuleId;
                    //echo "SELECT  cr.customRuleId, frequency, every, isNotweekEnd,isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, crm.*, cry.* from customrules cr left join customrulesdaily crd on cr.customRuleId=crd.customRuleId left join customruleweekly crw on cr.customRuleId=crw.customRuleId left join customrulemonthly crm on cr.customRuleId=crm.customRuleId left join customrulesyearly cry on cr.customRuleId=cry.customRuleId where cr.customRuleId='".$customRuleId."'";
                    $customDetails = $database->executeObject("SELECT  cr.customRuleId, frequency, every, isNotweekEnd,isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, crm.*, cry.* from customrules cr left join customrulesdaily crd on cr.customRuleId=crd.customRuleId left join customruleweekly crw on cr.customRuleId=crw.customRuleId left join customrulemonthly crm on cr.customRuleId=crm.customRuleId left join customrulesyearly cry on cr.customRuleId=cry.customRuleId where cr.customRuleId='" . $customRuleId . "'");
                    //print_r($customDetails);
                    switch ($customDetails->frequency)
                    {
                        case 1:
                            if ($customDetails->every == '' || $customDetails->every == 0 || $customDetails->every == NULL || $customDetails->every == 1)
                            {
                                $every = 0;
                                $daysToAdd = 1;

                            }
                            else
                            {
                                $every = $customDetails->every;
                                $daysToAdd = 0;
                            }
                            if ($customDetails->isNotweekEnd == 1)
                            {
                                //echo "UPDATE tasks SET dueDateTime=IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." DAY))=7,DATE_ADD(dueDateTime, INTERVAL 2+".$every."+".$daysToAdd." DAY),IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." DAY))=1,DATE_ADD(dueDateTime, INTERVAL 1+".$every."+".$daysToAdd." DAY),DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." DAY))
                                //) WHERE  taskId='".$taskId."'";
                                $database->executeNonQuery("UPDATE tasks SET dueDateTime=IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " DAY))=7,DATE_ADD(dueDateTime, INTERVAL 2+" . $every . "+" . $daysToAdd . " DAY),IF(DAYOFWEEK(DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " DAY))=1,DATE_ADD(dueDateTime, INTERVAL 1+" . $every . "+" . $daysToAdd . " DAY),DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " DAY))
	) WHERE  taskId='" . $taskId . "'");
                            }
                            else
                            {
                                //echo "UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." DAY) WHERE taskId='".$taskId."'";
                                $database->executeNonQuery("UPDATE tasks SET dueDateTime=DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " DAY) WHERE taskId='" . $taskId . "'");
                            }
                            break;
                        case 2:
                            $customRuleId = $taskToEdit->customRuleId;
                            //$customDetails=$database->executeObject("SELECT  cr.customRuleId, frequency, every, isNotweekEnd,isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, crm.*, cry.* from customrules cr left join customrulesdaily crd on cr.customRuleId=crd.customRuleId left join customruleweekly crw on cr.customRuleId=crw.customRuleId left join customrulemonthly crm on cr.customRuleId=crm.customRuleId left join customrulesyearly cry on cr.customRuleId=cry.customRuleId where cr.customRuleId='".$customRuleId."'");
                            //print_r($customDetails);
                            if ($customDetails->every == '' || $customDetails->every == 0 || $customDetails->every == NULL || $customDetails->every == 1)
                                $every = 0;
                            else
                                $every = $customDetails->every;
                            $dueDate = $taskToEdit->dueDateTime;
                            $dayOfweek = date('l', strtotime($dueDate));
                            $flag = 1;
                            $daysToAdd = 0;
                            if ($dayOfweek == 'Sunday')
                            {
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Monday')
                            {
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Tuesday')
                            {
                                /*echo "Found";
                                echo "Day Found";
                                echo $customDetails->isWednesday;
                                echo "Flag Found";
                                echo $flag;*/
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Wednesday')
                            {
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Thursday')
                            {
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Friday')
                            {
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            if ($dayOfweek == 'Saturday')
                            {
                                if ($customDetails->isSunday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 1;
                                    $flag = 0;
                                }
                                if ($customDetails->isMonday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 2;
                                    $flag = 0;
                                }
                                if ($customDetails->isTuesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 3;
                                    $flag = 0;
                                }
                                if ($customDetails->isWednesday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 4;
                                    $flag = 0;
                                }
                                if ($customDetails->isThursday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 5;
                                    $flag = 0;
                                }
                                if ($customDetails->isFriday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 6;
                                    $flag = 0;
                                }
                                if ($customDetails->isSaturday == 1 && $flag == 1)
                                {
                                    $daysToAdd = 7;
                                    $flag = 0;
                                }
                            }
                            //echo $customDetails->isSunday." ".$customDetails->isMonday." ".$customDetails->isTuesday." ".$customDetails->isWednesday." ".$customDetails->isThursday." ".$customDetails->isFriday." ".$customDetails->isSaturday;
                            if ($flag == 1)
                            {
                                $daysToAdd = 7;
                            }
                            $every = $every * 7;
                            //echo "UPDATE tasks SET dueDateTime=IF(WEEK(DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." DAY))=WEEK(dueDateTime),DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." DAY),DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." DAY)) WHERE taskId='".$taskId."'";
                            $database->executeNonQuery("UPDATE tasks SET dueDateTime=IF(WEEK(DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " DAY))=WEEK(dueDateTime),DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " DAY),DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " DAY)) WHERE taskId='" . $taskId . "'");
                            break;
                        case 3:
                            //print_r($customDetails);
                            if ($customDetails->every == '' || $customDetails->every == 0 || $customDetails->every == NULL || $customDetails->every == 1)
                                $every = 0;
                            else
                                $every = $customDetails->every;
                            $dueDate = explode("-", $taskToEdit->dueDateTime);
                            $year = $dueDate[0];
                            $month = $dueDate[1];
                            $month = ltrim($month, '0');
                            $dateDue = explode(" ", $dueDate[2]);
                            $dateDue = ltrim($dateDue[0], '0');
                            $monthDays = array();
                            $customRuleId = $taskToEdit->customRuleId;
                            $monthDays = $database->executeRow("SELECT is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30,is31 from customrules cr left join customrulemonthly crm on cr.customRuleId=crm.customRuleId where cr.customRuleId='" . $customRuleId . "'");
                            //$monthDays[]=$customMonthly;
                            //$monthDays = (array) $customMonthly;
                            //print_r($customMonthly);
                            //array_merge($monthDays,$customMonthly);
                            //print_r($monthDays);
                            //echo sizeof($monthDays);
                            $daysToAdd = 0;
                            $flag = 1;
                            for ($day = $dateDue; $day < sizeof($monthDays); $day++)
                            {
                                if ($monthDays[$day] == 1 && $flag == 1)
                                {
                                    //echo "found";
                                    $daysToAdd = $day - $dateDue + 1;
                                    $flag = 0;

                                }

                            }
                            //echo "1 check passed";
                            if ($flag != 0)
                            {
                                for ($day = 0; $day < $dateDue; $day++)
                                {
                                    if ($monthDays[$day] == 1 && $flag == 1)
                                    {
                                        if ($month != 9 || $month != 4 || $month != 6 || $month != 11)
                                            $daysToAdd = 31 - $dateDue + $day + 1;
                                        if ($month == 2)
                                            $daysToAdd = 28 - $dateDue + $day + 1;
                                        if ($month == 9 || $month == 4 || $month == 6 || $month == 11)
                                            $daysToAdd = 30 - $dateDue + $day + 1;
                                        $flag = 0;
                                    }

                                }
                            }
                            if (!in_array(1, $monthDays) && $flag == 1)
                            {
                                if ($month != 9 || $month != 4 || $month != 6 || $month != 11)
                                    $daysToAdd = 31;
                                if ($month == 2)
                                    $daysToAdd = 28;
                                if ($month == 9 || $month == 4 || $month == 6 || $month == 11)
                                    $daysToAdd = 30;

                            }
                            for ($i = 1; $i <= $every; $i++)
                            {
                                $nextMonth = $month + 1;
                                if ($i == 1)
                                {
                                    $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                                    $everyDays = 0;
                                }
                                else
                                {
                                    $days = cal_days_in_month(CAL_GREGORIAN, $nextMonth, $year);
                                    $everyDays = $everyDays + $days;
                                }
                            }
                            //echo "UPDATE tasks SET dueDateTime=IF(MONTH(DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." DAY))=MONTH(dueDateTime),DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." DAY),DATE_ADD(dueDateTime, INTERVAL ".$everyDays."+".$daysToAdd." DAY)) WHERE taskId='".$taskId."'";
                            $database->executeNonQuery("UPDATE tasks SET dueDateTime=IF(MONTH(DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " DAY))=MONTH(dueDateTime),DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " DAY),DATE_ADD(dueDateTime, INTERVAL " . $everyDays . "+" . $daysToAdd . " DAY)) WHERE taskId='" . $taskId . "'");
                            break;
                        case 4:
                            $customRuleId = $taskToEdit->customRuleId;
                            $yearMonths = $database->executeRow("SELECT isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep, isOct, isNov, isDec from customrules cr  left join customrulesyearly cry on cr.customRuleId=cry.customRuleId where cr.customRuleId='" . $customRuleId . "'");
                            //print_r($yearMonths);
                            if ($customDetails->every == '' || $customDetails->every == 0 || $customDetails->every == NULL || $customDetails->every == 1)
                                $every = 0;
                            else
                                $every = $customDetails->every;
                            //$yearMonths=array($customDetails->isJan,$customDetails->isFeb,$customDetails->isMar,$customDetails->isApril,$customDetails->isMay,$customDetails->isJune,$customDetails->isJuly,$customDetails->isAug,$customDetails->isSep,$customDetails->isOct,$customDetails->isNov,$customDetails->isDec);
                            $dueDate = explode("-", $taskToEdit->dueDateTime);
                            $month = $dueDate[1];
                            $month = ltrim($month, '0');
                            $daysToAdd = 0;
                            $flag = 1;
                            //echo sizeof($yearMonths);
                            for ($m = $month; $m < sizeof($yearMonths); $m++)
                            {
                                if ($yearMonths[$m] == 1 && $flag == 1)
                                {
                                    $daysToAdd = $m - $month + 1;
                                    $flag = 0;

                                }

                            }
                            if ($flag != 0)
                            {
                                for ($m = 0; $m < $month; $m++)
                                {
                                    if ($yearMonths[$m] == 1 && $flag == 1)
                                    {
                                        $daysToAdd = 12 - $month + $m + 1;
                                        $flag = 0;
                                    }

                                }
                            }
                            if (!in_array(1, $yearMonths))
                            {
                                $daysToAdd = 12;

                            }
                            $everyYear = $every;
                            $i = 0;
                            while ($i < $everyYear)
                            {
                                if ($i == 0)
                                    $every = 0;
                                else
                                    $every = $every + 12;
                                $i = $i + 1;

                            }
                            //echo "UPDATE tasks SET dueDateTime=IF(YEAR(DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." MONTH))=YEAR(dueDateTime),DATE_ADD(dueDateTime, INTERVAL ".$daysToAdd." MONTH),DATE_ADD(dueDateTime, INTERVAL ".$every."+".$daysToAdd." MONTH)) WHERE taskId='".$taskId."'";
                            $database->executeNonQuery("UPDATE tasks SET dueDateTime=IF(YEAR(DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " MONTH))=YEAR(dueDateTime),DATE_ADD(dueDateTime, INTERVAL " . $daysToAdd . " MONTH),DATE_ADD(dueDateTime, INTERVAL " . $every . "+" . $daysToAdd . " MONTH)) WHERE taskId='" . $taskId . "'");
                            break;

                    }

                }
                //echo "Select IF(endingOn='0000-00-00',0,IF(dueDateTime>endingOn,1,0)) as isEnded from tasks WHERE taskId='".$taskId."'";
                $isEnded = $database->executeScalar("Select IF(endingOn=0000-00-00,0,IF(dueDateTime>endingOn,1,0)) as isEnded from tasks WHERE taskId='" . $taskId . "'");
                if ($isEnded == 0)
                {
                    //echo "INSERT INTO tasks (userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId)  (Select userId, title, '1' as isCompleted, priorityId, notes, dueDateTime,'0' as recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, NOW() as completedDate, '0' as customRuleId, '1' as repeatTypeId, '".$taskId."' as  parentTaskId from tasks where taskId='".$taskId."')";
                    if ($completeDate == '0000-00-00' && $completeTime == '00:00:00')
                        $database->executeNonQuery("INSERT INTO tasks (userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId)  (Select userId, title, '0' as isCompleted, priorityId, notes, dueDateTime,'0' as recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, NOW() as completedDate, '0' as customRuleId, repeatTypeId, '" . $taskId . "' as  parentTaskId from tasks where taskId='" . $taskId . "')");
                    else
                        $database->executeNonQuery("INSERT INTO tasks (userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, completedDate, customRuleId, repeatTypeId, parentTaskId)  (Select userId, title, '1' as isCompleted, priorityId, notes, dueDateTime,'0' as recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, '" . $completeDate . " " . $completeTime . "' as completedDate, '0' as customRuleId, '1' as repeatTypeId, '" . $taskId . "' as  parentTaskId from tasks where taskId='" . $taskId . "')");
                    $newTaskId = $database->insertid();
                    if (!empty($contactIds))
                    {
                        foreach ($contactIds as $cc)
                        {
                            //echo "INSERT INTO taskcontacts (taskId, contactId) VALUES('".$newTaskId."','".$cc->contactId."')";
                            $database->executeNonQuery("INSERT INTO taskcontacts (taskId, contactId) VALUES('" . $newTaskId . "','" . $cc->contactId . "')");
                        }
                    }
                }
                else
                {
                    //echo "Ended";
                    if ($completeDate == '0000-00-00' && $completeTime == '00:00:00')
                        $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate=NOW() WHERE taskId='" . $taskId . "'");
                    else
                        $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate='" . $completeDate . " " . $completeTime . "' WHERE taskId='" . $taskId . "'");

                }

            }
            $plan_id = $database->executeScalar("SELECT planId FROM tasks WHERE  taskId='" . $taskId . "'");
            //echo 'PlanId'.$plan_id;
            if (!empty($plan_id) || $plan_id != 0 || $plan_id != NULL)
            {
                if ($completeDate == '0000-00-00' && $completeTime == '00:00:00')
                {
                    //echo "UPDATE tasks SET isCompleted=1 , completedDate=NOW() WHERE taskId='".$taskId."'";
                    $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate=NOW() WHERE taskId='" . $taskId . "'");
                }
                else
                {
                    //echo "UPDATE tasks SET isCompleted=1 , completedDate='".$completeDate." ".$completeTime."' WHERE taskId='".$taskId."'";
                    $database->executeNonQuery("UPDATE tasks SET isCompleted=1 , completedDate='" . $completeDate . " " . $completeTime . "' WHERE taskId='" . $taskId . "'");
                }
                $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
                if (empty($contactId) || $contactId = '')
                {
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "'");
                }
                else
                {
                    //echo $cid;
                    //echo "SELECT contactId FROM taskcontacts WHERE  taskId='".$taskId."' AND contactId IN (".$cid.")";
                    $contactIds = $database->executeObjectList("SELECT contactId FROM taskcontacts WHERE  taskId='" . $taskId . "' AND contactId IN (" . $cid . ")");
                }
                if (!empty($contactIds))
                {
                    foreach ($contactIds as $cc)
                    {
                        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $cc->contactId . "'");
                        $x = 0;
                        $contactSteps = array();
                        foreach ($existingSteps as $exists)
                        {
                            $contactSteps[$x] = $exists->stepId;
                            $x++;
                        }
                        //echo "SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '".$cc->contactId."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by order_no limit 1";
                        //echo '--------';
                        //$thisstep=$database->executeScalar("SELECT stepId from  taskplan where taskId='".$taskId."' and planId='".$plan_id."'");
                        /*echo "SELECT * FROM plansteps WHERE order_no > (select order_no from plansteps where stepId='".$thisstep."') AND planId='".$plan_id."' order by order_no asc LIMIT 1";
                                echo '--------';*/

                        //$stepInfo=$database->executeObject("SELECT * FROM plansteps WHERE order_no > (select order_no from plansteps where stepId='".$thisstep."') AND planId='".$plan_id."' order by order_no asc LIMIT 1");

//						$stepInfo = $database->executeObject("SELECT * FROM plansteps  WHERE  stepId >(select stepId from planstepusers where contactId = '" . $cc->contactId . "' and stepId IN (select stepId from plansteps  where planId='" . $plan_id . "' and isactive = 1 and isArchive = 0 )) and planId='" . $plan_id . "'and isactive = 1 and isArchive = 0  order by order_no limit 1");
//

                        $stepInfo = $database->executeObject("SELECT psu.stepId FROM planstepusers psu LEFT JOIN plansteps ps ON psu.`stepId` = ps.`stepId` WHERE psu.contactId='" . $cc->contactId . "' AND ps.planId = '" . $plan_id . "'");

                        $thisstep = $database->executeScalar("SELECT stepId from  taskplan where taskId='" . $taskId . "' and planId='" . $plan_id . "'");


                        //echo "SELECT * FROM plansteps  WHERE stepId >(select stepId from planstepusers where contactId = '".$contact_id."' and stepId IN (select stepId from plansteps  where planId='".$plan_id."')) and planId='".$plan_id."' order by stepId limit 1";
                        //echo '--------';
                        //echo "DELETE FROM planstepusers WHERE contactId='".$contact_id."' and stepId IN (select stepId from plansteps where planId='".$plan_id."')";
                        $database->executeNonQuery("DELETE FROM planstepusers WHERE contactId='" . $cc->contactId . "' and stepId IN (select stepId from plansteps where planId='" . $plan_id . "')");
                        /*$taskId2=$database->executeScalar("select taskId from taskcontacts where contactId<>'".$cc->contactId."' and taskId=".$taskId."");
                        if(empty($taskId2))
                        {

                        $database->executeNonQuery("DELETE FROM  tasks  WHERE taskId= '".$taskId."'");
                        }*/
                        //$database->executeNonQuery("DELETE FROM taskcontacts  WHERE taskId='".$taskId."' and contactId='".$cc->contactId."'");
                        //Assign next step - copy from profile.php by Atif
                        //$i=0;
                        $plan = $database->executeObject("SELECT * FROM plan WHERE planId='" . $plan_id . "'");
                        //echo '--------';
                        //echo 'PlanId'.$plan->planId;
                        //echo "SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='".$stepInfo->stepId."') AND planId='".$plan_id."' order by stepId asc";
                        //echo '--------';

                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no > (select order_no from plansteps where stepId = '" . $stepInfo->stepId . "') AND planId = '" . $plan_id . "' and isactive = 1 and isArchive = 0 order by order_no asc");

                        $date = date('Y-m-d');
                        $date = strtotime($date);
                        $totalSteps = sizeof($palnSteps);
//						echo "Total Steps:". $totalSteps."<br>";
                        $stepCounter = 1;
                        if ($totalSteps == 0)
                        {
                            //echo "INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('".$thisstep."','".$plan_id."','".$cc->contactId."',CURDATE())";
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $thisstep . "','" . $plan_id . "','" . $cc->contactId . "',CURDATE())");
                        }
                        else
                        {
                            foreach ($palnSteps as $ps)
                            {
                                /*
                                $newContactSteps[$y]=$ps->stepId;
                                $y++;
                                */
                                if (!in_array($ps->stepId, $contactSteps))
                                {
                                    $planId2 = self::CheckLastStepOfPlan($planss, $contactId);
                                    if (!empty($planId2))
                                    {
                                        echo '-12';
                                        $database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $cc->contactId . "'");
                                    }
                                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                                    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $cc->contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                                    $newStepId = $database->insertid();
                                    $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "'");
                                    if ($ps->isTask == 0 && $PermissionStatus !='4')
                                    {
                                        if ($ps->daysAfter == 0)
                                        {
                                            $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cc->contactId . "'");
                                            if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0)
                                            {
                                                /*echo 'emial allowed';
                                                echo '<br>';*/
                                                $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $cc->contactId . "'");
                                                $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                                preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                                foreach ($match[1] as $key)
                                                {
                                                    if ($key == "First Name")
                                                    {
                                                        $firstname = 'firstName';
                                                    }
                                                    if ($key == "Last Name")
                                                    {
                                                        $lastname = 'lastName';
                                                    }
                                                    if ($key == "Phone Number")
                                                    {
                                                        $phonenumber = 'phoneNumber';
                                                    }
                                                    if ($key == "Company Name")
                                                    {
                                                        $companyname = 'companyTitle';
                                                    }
                                                    if ($key == "@Signature")
                                                    {
                                                        $signature = self::getSignatures($database, $userId);
                                                    }

                                                }
                                                if (!empty($firstname))
                                                {
                                                    $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($lastname))
                                                {
                                                    $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($companyname))
                                                {
                                                    $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $cc->contactId . "'");
                                                }
                                                if (!empty($phonenumber))
                                                {
                                                    $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $cc->contactId . "'");
                                                }
                                                $subject = $mailBody->subject;
                                                $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                                $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                                $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                                $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                                $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                                $message = str_replace("\'", "'", $rplbysig);
                                                $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                                    "Reply-To: " . $userInfo->email . "\r\n";
                                                $headers .= "Content-Type: text/html";
                                                //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                                $mail = new smtpEmail();
                                                $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject,$mailBody->id);
                                                //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                                $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
														('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $cc->contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                                $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $cc->contactId . "'");
                                                if ($totalSteps == $stepCounter)
                                                {
                                                    $alreadyCompleted = $database->executeScalar("SELECT count(*) as exists from contactPlansCompleted where stepId='" . $ps->stepId . "' and planId='" . $plan_id . "' and contactId='" . $cc->contactId . "'");
                                                    if ($alreadyCompleted == 0)
                                                    {
                                                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $ps->stepId . "','" . $plan_id . "','" . $cc->contactId . "',CURDATE())");
                                                        $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $cc->contactId . "' and planId='" . $plan_id . "'");
                                                    }

                                                    //echo "INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('".$ps->stepId."','".$plan_id."','".$cc->contactId."',CURDATE())";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ( $ps->isTask == 1 ){
                                        //echo "-------";
                                        //echo "INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId."','".$stepDesc->summary."','0','','".mysql_escape_string($stepDesc->description)."','".date('Y-m-d',$dueDate)."','0','".date('Y-m-d')."','0','0','".$stepDesc->planId."','','0','1')";
                                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                        $newTaskId = $database->insertid();
                                        //echo "-------";
                                        //echo $newTaskId;
                                        //echo "-------";
                                        //echo "INSERT INTO taskplan set taskId='".$newTaskId."',planId='".$stepDesc->planId."',stepId='".$stepDesc->stepId."'";
                                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $newTaskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                                        //echo "-------";
                                        //echo "INSERT INTO taskcontacts SET contactId='".$contact_id."',taskId='".$newTaskId."'";
                                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $cc->contactId . "',taskId='" . $newTaskId . "'");
                                        }
                                        break;
                                    }
                                    $stepCounter++;
                                }
                            }
                        }

                    }
                }
            }
            echo 1;

        }

        function getconsultantTitle($database, $userId, $fieldMappingName)
        {

            return $consultantTitle = $database->executeObject("SELECT `fieldCustomName`,`status` FROM `leaderdefaultfields` WHERE userId='" . $userId . "' AND `fieldMappingName`='" . $fieldMappingName . "' AND `status`='1'");

        }

    }

?>