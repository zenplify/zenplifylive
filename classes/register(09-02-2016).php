<?php
require_once('init.php');
require_once('plan.php');
require_once('profiles.php');
include_once('encryption_class.php');

class register extends plan
{
    function UpdateUser($postArray)
    {
        $userId = $postArray['userId'];
        $userName = strtolower($postArray['username']);
        $where = '';
        if (!(empty($postArray['current_password']))) {
            $password = $postArray['newpassword'];
            $where = "password='" . $password . "',";
        }
        //			if(empty($postArray['current_password']))
        //			{
        //				$password = $postArray['currentPassword'];
        //			}
        //			else
        //			{
        //				$password = $postArray['newpassword'];
        //			}
        $firstName = $postArray['firstname'];
        $lastName = $postArray['lastname'];
        $email = $postArray['email'];
        $timeZoneId = $postArray['timezone'];
        $phoneNumber = $postArray['phonenumber'];
        $termAndConditions = $postArray['termandconditions'];
        $consultantId = $postArray['consultant_id'];
        $title = $postArray['title'];
        $webAddress = $postArray['web'];
        $facebookAddress = $postArray['facebook_page'];
        $facebookPersonalAddress = $postArray['facebook_personalpage'];
        $facebookPartyAddress = $postArray['facebook_partypage'];
        //$registrationDate = date("Y-n-j");
        //$registrationDate = date("Y-m-d");
        $emailVerificationCode = generateUniqueID();
        if (!empty($postArray)) {
            $database = new database();
//	            registrationDate='" . $registrationDate . "',


            $sql = "UPDATE  user
                                     SET
                                    userName='" . $userName . "',
                                    email='" . $email . "',
                                    " . $where . "
                                    timeZoneId='" . $timeZoneId . "',
                                    phoneNumber='" . $phoneNumber . "',
                                    firstName='" . $firstName . "',
                                    lastName='" . $lastName . "',
                                    consultantId='" . $consultantId . "',
                                    title='" . $title . "',
                                    webAddress='" . $webAddress . "',
                                    facebookAddress='" . $facebookAddress . "',
                                    facebookPersonalAddress='" . $facebookPersonalAddress . "',
                                    facebookPartyAddress='" . $facebookPartyAddress . "',
                                    emailVerificationCode='" . $emailVerificationCode . "' WHERE userId='" . $userId . "'";
            $database->executeNonQuery($sql);

            return $emailVerificationCode . '/' . $userId;

        }

    }

//		function UpdateUser($postArray)
//		{
//			$userId = $postArray['userId'];
//			$userName = strtolower($postArray['username']);
//			if (empty($postArray['current_password']))
//			{
//				$password = $postArray['currentPassword'];
//			}
//			else
//			{
//				$password = $postArray['newpassword'];
//
//			}
//			$firstName = $postArray['firstname'];
//			$lastName = $postArray['lastname'];
//			$email = $postArray['email'];
//			$timeZoneId = $postArray['timezone'];
//			$phoneNumber = $postArray['phonenumber'];
//			$termAndConditions = $postArray['termandconditions'];
//			$consultantId = $postArray['consultant_id'];
//			$title = $postArray['title'];
//			$webAddress = $postArray['web'];
//			$facebookAddress = $postArray['facebook_page'];
//			$facebookPersonalAddress = $postArray['facebook_personalpage'];
//			$facebookPartyAddress = $postArray['facebook_partypage'];
//			//$registrationDate = date("Y-n-j");
//			$registrationDate = date("Y-m-d");
//			$emailVerificationCode = generateUniqueID();
//			if (!empty($postArray))
//			{
//				$database = new database();
//				$database->executeNonQuery("UPDATE  user
//                                     SET
//                                    userName='" . $userName . "',
//                                    email='" . $email . "',
//                                    password='" . $password . "',
//                                    timeZoneId='" . $timeZoneId . "',
//                                    phoneNumber='" . $phoneNumber . "',
//                                    firstName='" . $firstName . "',
//                                    lastName='" . $lastName . "',
//                                    consultantId='" . $consultantId . "',
//                                    title='" . $title . "',
//                                    webAddress='" . $webAddress . "',
//                                    facebookAddress='" . $facebookAddress . "',
//                                    facebookPersonalAddress='" . $facebookPersonalAddress . "',
//                                    facebookPartyAddress='" . $facebookPartyAddress . "',
//                                    emailVerificationCode='" . $emailVerificationCode . "' WHERE userId='" . $userId . "'"
//
//				);
//
//				return $emailVerificationCode . '/' . $userId;
//
//			}
//
//		}

    function RegisterUser($postArray)
    {
        $inheritedLeaderId = $postArray['inheritedLeaderId'];
        $summary = $this->replaceComma($postArray['summary']);
        $userName = $this->replaceComma($postArray['username']);
        $password = $postArray['password'];
        $firstName = $this->replaceComma($postArray['firstname']);
        $lastName = $this->replaceComma($postArray['lastname']);
        $email = $this->replaceComma($postArray['email']);
        $timeZoneId = $postArray['timezone'];
        $phoneNumber = $this->replaceComma($postArray['phonenumber']);
        $termAndConditions = $postArray['termandconditions'];
        $consultantId = $this->replaceComma($postArray['consultant_id']);
        $title = $this->replaceComma($postArray['title']);
        $webAddress = $this->replaceComma($postArray['web']);
        $facebookAddress = $this->replaceComma($postArray['facebook_page']);
        $facebookPersonalAddress = $this->replaceComma($postArray['facebook_personalpage']);
        $facebookPartyAddress = $this->replaceComma($postArray['facebook_partypage']);

        $leaderTemplateId = 0;
        if (isset($postArray['leaderTemplateId'])) {
            $leaderTemplateId = $postArray['leaderTemplateId'];
        }

        if ($postArray['videoOption'] == "AUS")
            $leaderId = 1;
        else
            $leaderId = 236;
        if ($postArray['isleader'] == '1') {
            $leaderId = 0;
            $addedFields = ",`signupName` = '" . $userName . "'";
        } else {
            $leaderId = $postArray['leaderTemplate'];
        }
        //$registrationDate = date("Y-n-j");
        $registrationDate = date("Y-m-d");
        $emailVerificationCode = generateUniqueID();
        if (!empty($postArray)) {


            $database = new database();
            $sql = "Insert into user SET
									userName='" . $userName . "',
									email='" . $email . "',
									password='" . $password . "',
									timeZoneId='" . $timeZoneId . "',
									phoneNumber='" . $phoneNumber . "',
									firstName='" . $firstName . "',
									lastName='" . $lastName . "',
									consultantId='" . $consultantId . "',
									title='" . $title . "',
									webAddress='" . $webAddress . "',
									facebookAddress='" . $facebookAddress . "',
									leaderId=" . $leaderId . ",
									facebookPersonalAddress='" . $facebookPersonalAddress . "',
									facebookPartyAddress='" . $facebookPartyAddress . "',
									registrationDate='" . $registrationDate . "',
									isVerifiedPaypal='0',
									inheritedLeaderId = '" . $inheritedLeaderId . "',
									emailVerificationCode='" . $emailVerificationCode . "' " . $addedFields;
//                $sql = "Insert into user SET
//									userName='" . $userName . "',
//									email='" . $email . "',
//									password='" . $password . "',
//									timeZoneId='" . $timeZoneId . "',
//									phoneNumber='" . $phoneNumber . "',
//									firstName='" . $firstName . "',
//									lastName='" . $lastName . "',
//									consultantId='" . $consultantId . "',
//									title='" . $title . "',
//									webAddress='" . $webAddress . "',
//									facebookAddress='" . $facebookAddress . "',
//									leaderId=" . $leaderId . ",
//									facebookPersonalAddress='" . $facebookPersonalAddress . "',
//									facebookPartyAddress='" . $facebookPartyAddress . "',
//									registrationDate='" . $registrationDate . "',
//									isVerifiedPaypal='0',
//									signupName='" . $userName . "',
//									emailVerificationCode='" . $emailVerificationCode . "' " . $addedFields;
            //upComingTasks='".$postArray['upComingTask']."' ".$addedFields;
            $database->executeNonQuery($sql);
            $result = $database->insertid();
            if (!empty($result)) {
// signup copy groups for leader to user  manu//

// end code for copying groups manu//
                foreach ($postArray as $key => $value) {
                    if (strpos($key, 'choice') === 0) {
                        $choiceSplited = split('choice', $key);
                        $questionId = $choiceSplited[1];
                        foreach ($postArray[$key] as $choice) {
                            $optionSplitted = split('_', $choice);
                            $choiceId = $optionSplitted[0];
                            $choiceText = $optionSplitted[1];
                            $database->executeNonQuery("Insert into `signupquestionreply` set `questionId` = '" . $choiceSplited[1] . "', `choiceId` = '" . $choiceId . "', `choiceText` = '" . $choiceText . "', `userId` = '" . $result . "', `datetime` = NOW()");
                        }
                    }
                }

//					$database->executeNonQuery("INSERT INTO groupusers (groupId ,userId, isactive)
//SELECT gu.`groupId` ,'".$result."',gu.`isactive` FROM groupusers gu JOIN groups g ON g.`groupId` = gu.`groupId` WHERE gu.userId = '".$leaderId."' AND g.privacy='1' AND gu.isactive='1' ");
//					$plan = new plan();
//					$plan->setPublishedPlanForUser($database, $result, $leaderId);
//
//					$profile = new Profiles();
//					$profile->setPublishedPagesForUser($database, $result, $leaderId);


                return $emailVerificationCode . '/' . $result;
            }
        }
    }

    function replaceComma($string)
    {
        return str_replace("'", "''", $string);
    }

    function loginUserAfterRegister($userId)
    {
        $database = new database();
        $result = $database->executeObject("SELECT * FROM user where	userId ='" . $userId . "'");

        return $result;
    }

    function getTimeZone()
    {
        $database = new database();

        return $database->executeObjectList("select timeZoneId,zoneTitle,GMTDiff from timezone ");

    }

    function VerifyUserCode($code)
    {
        $database = new database();
        $database->executeNonQuery("UPDATE user SET isVerifiedEmail=1 WHERE emailVerificationCode LIKE '" . $code . "'");

    }

    function createUserProfiles($code)
    {
        $database = new database();
        $userInfo = $database->executeObject("SELECT userId,leaderId, inheritedLeaderId FROM user WHERE emailVerificationCode LIKE '" . $code . "'");
        if (!empty($userInfo)) {
            $userId = $userInfo->userId;
            $leaderId = ($userInfo->leaderId) ? $userInfo->leaderId : $userInfo->inheritedLeaderId;
            $inheritedLeaderId = $userInfo->inheritedLeaderId;

            $leaderInfo = $database->executeObject("Select * from `user` where userId = '" . $leaderId . "'");

            /*$database->executeNonQuery("INSERT INTO pages(uniqueCode, userId, pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, addDate, pageName)
            select MD5(RAND()) as uniqueCode, '".$userId."' as userId, pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, CURDATE() as addDate, pageName from pages
            where userId=0");*/
            //$pageId=$database->insertid();
            if ($leaderId == 0) {


                $id = ($inheritedLeaderId) ? $inheritedLeaderId : 1;
                $sql = "SELECT * FROM pages WHERE  userId = '" . $id . "' and status = 1 and isComplete= 1";


            } else {
                $sql = "SELECT * FROM pages WHERE status = 1 and privacy = 1 and isComplete= 1 and userId = '" . $leaderId . "'";
            }
            $pages = $database->executeObjectList($sql);

            foreach ($pages as $p) {

                $generatedVia = 2;
                $pageTemplateId = 7;

//					if ($leaderId == 236)
                if (!empty($leaderInfo)) {
                    if (($leaderInfo->registrationDate < registrationDate) && ($p->generatedVia == 0))
                        $generatedVia = 0;

                    if ($leaderInfo->inheritedLeaderId == 236) {
                        $generatedVia = $p->generatedVia;
                    }
                }

                // Page Id
                $pId = ($inheritedLeaderId) ? 0 : $p->pageId;
                if ($inheritedLeaderId || $leaderId) {
                    $id = ($inheritedLeaderId) ? $userId : $leaderId;

                    // Template Defaults
                    $sql = "SELECT * FROM `leadertemplatedefaults` WHERE `leaderId` = '" . $id . "'";
                    $leaderTemplateDefaults = $database->executeObject($sql);
                }
                $defaultProfileHeader = isset($leaderTemplateDefaults) ? $leaderTemplateDefaults->defaultProfileHeader : $p->profileHeader;
                $defaultThankYouHeader = isset($leaderTemplateDefaults) ? $leaderTemplateDefaults->defaultThankYouHeader : $p->thankyouHeader;
                $defaultProfileVideo = isset($leaderTemplateDefaults) ? $leaderTemplateDefaults->defaultProfileVideo : $p->profileVideoLink;
                $defaultThankYouVideo = isset($leaderTemplateDefaults) ? $leaderTemplateDefaults->defaultThankYouVideo : $p->thankyouVideoLink;
                $defaultText = isset($leaderTemplateDefaults) ? $leaderTemplateDefaults->defaultText : $p->profileDefaultText;

                $database->executeNonQuery("INSERT INTO pages
					(uniqueCode,
					userId,
					pageTemplateId,
					formTemplateId,
					isAutomaticallyAddNewContact,
					pageBrowserTitle,
					thankYouMessage,
					pageHTMLGenerated,
					formHTMLGenerated,
					addDate,
					pageName,
					newCode,
					leaderId,
					status,
					privacy,
					generatedVia,
					profileDefaultText,
					thankyouDefaultText,
					profileVideoLink,
					thankyouVideoLink,
					profileFontStyle,
					thankyouFontStyle,
					profileHeader,
					thankyouHeader,
					profilebgColor,
					thankyoubgColor,
					profileFontColor,
					thankyouFontColor,
					columnCount,
					oldId,
					parentId,
					accentColor,
					`order`,
					`isComplete`,
					`isDefaultVideoshow`)
					VALUES
					(MD5(RAND()),
					'" . $userId . "' ,
					'" . $p->pageTemplateId . "',
					'" . $p->formTemplateId . "',
					'" . $p->isAutomaticallyAddNewContact . "',
					'" . $p->pageBrowserTitle . "',
					'" . mysql_real_escape_string($p->thankYouMessage) . "',
					'" . mysql_real_escape_string($p->pageHTMLGenerated) . "',
					'" . mysql_real_escape_string($p->formHTMLGenerated) . "',
					CURDATE(),
					'" . $p->pageName . "',
					'" . $p->newCode . "',
					'" . $leaderId . "',
					1,
					'1',
					'" . $generatedVia . "',
					'" . mysql_real_escape_string($defaultText) . "',
					'" . mysql_real_escape_string($p->thankyouDefaultText) . "',
					'" . $defaultProfileVideo . "',
					'" . $defaultThankYouVideo . "',
					'" . $p->profileFontStyle . "',
					'" . $p->thankyouFontStyle . "',
					'" . $defaultProfileHeader . "',
					'" . $defaultThankYouHeader . "',
					'" . $p->profilebgColor . "',
					'" . $p->thankyoubgColor . "',
					'" . $p->profileFontColor . "',
					'" . $p->thankyouFontColor . "',
					'" . $p->columnCount . "',
					'" . $pId . "',
					'" . $pId . "',
					'" . $p->accentColor . "',
					'" . $p->order . "',
					'1',
					'".$p->isDefaultVideoshow."')");

                $pageId = $database->insertid();

                $count = 0;
                $newProfileId = $pageId;
                $oldProfileId = $p->pageId;

//					$questionId = $database->executeObjectList("Select `questionId` form WHERE `templateDefault` = '1' and `status` = 1");
//					foreach ($questionId as $qId)
//					{
//						$database->executeNonQuery("Insert into `pagequestions`
//						(`pageId`, `questionId`, `order`, `mandatory` ) values
//						('" . $newProfileId . "','" . $qId . "', '" . $count . "','1')");
//					}

                // if ($leaderId != 236) {

                if (($leaderInfo->registrationDate > registrationDate) && ($p->generatedVia != 0)) {
//					if ($leaderInfo->registrationDate > registrationDate) {

                    $database->executeNonQuery("Insert into `pagequestions` (`pageId`, `questionId`, `order`, `mandatory`, `enableStatus`) SELECT '" . $newProfileId . "',pq.questionId,pq.order,pq.mandatory,pq.enableStatus FROM  `pagequestions` pq WHERE pq.`pageId` = '" . $p->pageId . "' AND `status` = '1' AND pq.`questionId` IN(SELECT `questionId` FROM `questions` WHERE `templateDefault` = '1' AND `status` = '1' ORDER BY `order`)ORDER BY pq.`order` ASC");
//						SELECT '" . $newProfileId . "',`questionId`, `order`, '1', `enableStatus` from `questions` where `templateDefault` = 1 and `status` = 1 order by `order`");
                }

                $questionId = $database->executeObjectList("SELECT q.questionId,q.typeId,pq.mandatory FROM questions q JOIN pagequestions pq ON q.questionId= pq.questionId JOIN questiontype pt ON q.`typeId` = pt.`typeId` WHERE pageId='" . $p->pageId . "' AND `q`.`templateDefault` = 0 AND q.status = 1 ORDER BY pq.order ASC");


                //Select `questionId` from `pagequestions` where `pageId` = '" . $p->pageId . "' order by `order`");
                foreach ($questionId as $qId) {
                    $count++;

                    $database->executeNonQuery("Insert into `questions` (`question`,`typeId`,`order`,`label`,`templateDefault`,`status`,`datetime`,`oldId`) select `question`,`typeId`,`order`,`label`,'0',1,now(),`questionId` from `questions` where questionId = '" . $qId->questionId . "' AND `templateDefault`='0'");

                    $newQuestionId = $database->insertid();

                    $database->executeNonQuery("Insert into `pageformquestionchoices` (`questionId`,`choiceText`,`order`,`oldId`,`parentId`,`isActive`) select '" . $newQuestionId . "',`choiceText`, `order`,`choiceId`,if(`parentId`='0',`choiceId`,`parentId`),`isActive` from `pageformquestionchoices` where `isActive` = 1 and `questionId` = '" . $qId->questionId . "'");

                    $database->executeNonQuery("Insert into `pagequestions` (`pageId`, `questionId`,`order`,`mandatory` ) values ('" . $newProfileId . "','" . $newQuestionId . "', '" . $count . "','" . $qId->mandatory . "')");


                    if ($qId->typeId == 5) {

                        $this->copyquestionintoRadiolable($database, $qId, $newQuestionId);


                    }


                }

                $profileCustomRules = $database->executeObjectList("select * from `pagecustomrules` where `pageId` = '" . $p->pageId . "' and `status` = 1");
                foreach ($profileCustomRules as $pcr) {
                    $database->executeNonQuery("INSERT INTO `pagecustomrules`
						(`pageId`,`status`,`datetime`,`oldId`) values
						('" . $newProfileId . "',1,NOW(),'" . $pcr->pageCustomRulesid . "')");

//						(SELECT '" . $newProfileId . "',`status`, NOW(), pageCustomRulesid FROM `pagecustomrules` WHERE pageId='" . $profileId . "' AND `status`='1')");
                    $profileCustomRuleId = $database->insertid();
// custom rules
                    $customRule = $database->executeObjectList("SELECT * FROM pagecustomrulesdetails pcrd left join pagecustomrules pcr on pcrd.pageCustomRulesid =pcr.pageCustomRulesid WHERE pcr.`pageId`='" . $oldProfileId . "'");

                    foreach ($customRule as $c) {
                        $question_id = "''";
                        if ((!empty($c->questionId)) && (!($c->questionId == 0)) && (!(is_null($c->questionId))))
                            $question_id = "(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "')";

                        $choice_id = "''";
                        if ((!empty($c->questionChoices)) && (!($c->questionChoices == 0)) && (!(is_null($c->questionChoices))))
                            $choice_id = "(select pfqc.choiceId from pageformquestionchoices pfqc where pfqc.oldId='" . $c->questionChoices . "' AND pfqc.isactive='1' and questionId=(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "'))";

                        $plan_id = "''";
                        if ((!empty($c->planId)) && (!($c->planId == 0)) && (!(is_null($c->planId))))
                            $plan_id = "(select p.planId from plan p where p.oldId='" . $c->planId . "' AND p.userId='" . $userId . "' AND p.isactive='1' AND p.`isArchive` ='0')";

                        $stepId = "''";
                        if ((!empty($c->stepId)) && (!($c->stepId == 0)) && (!(is_null($c->stepId))))
                            $stepId = "(SELECT s.stepId FROM plansteps s JOIN plan p ON s.`planId` = p.planid WHERE s.oldId='" . $c->stepId . "' AND p.userId = '" . $userId . "' AND p.`isactive` = '1' AND p.`isArchive` ='0')";
//							$stepId = "(select s.stepId from plansteps s where s.oldId='" . $c->stepId . "')";

                        if ($leaderId == 0) {
                            $groupId = $database->executeScalar("SELECT groupId FROM groups WHERE parentId = '" . $c->groupId . "' AND userId = '" . $userId . "'");
                        } else {
                            $groupId = $c->groupId;
                        }


                        $database->executeNonQuery("INSERT INTO `pagecustomrulesdetails`
                                      (`pageCustomRulesid`,
                                       `questionId`,
                                       `questionChoices`,
                                       `groupId`,
                                       `planId`,
                                       `stepId`,
                                       `status`,
                                       `datetime`,
                                       `oldId`)
                               VALUES('" . $profileCustomRuleId . "',
                                                  " . $question_id . ",
                                                  " . $choice_id . ",
                                                  '" . $groupId . "',
                                                  " . $plan_id . ",
                                                  " . $stepId . ",
                                                  '" . $c->status . "',
                                                  '" . $c->datetime . "',
                                                  '" . $c->pageCustomRulesDetailsid . "')");

                    }
                }

                if ($leaderId == 1) {
                    $database->executeNonQuery("UPDATE pages set pageTemplateId='5' where userId='" . $userId . "' and pageBrowserTitle='Guest Profile'");
                }
            }
        }
    }

    function CheckLoginInDB($userName, $passWord)
    {
        //$username = $this->SanitizeForSQL($username);
        //$pwdmd5 = md5($password);
        $database = new database();
        $result = $database->executeObjectList("SELECT userName,password FROM user where
				userName='" . $userName . "' and
				password='" . $passWord . "'
				");
        if (empty($result)) {
            return 0;
        }

        return 1;
    }

    function GetUserContacts($user_id)
    {
        $database = new database();
        $contacts = $database->executeObjectList("SELECT * FROM contact where userId='" . $user_id . "' order by contactId desc");

        return $contacts;
    }

    function getUserForLogin($user)
    {
        $database = new database();
        $result = $database->executeObject("SELECT * FROM user where (userName collate latin1_swedish_ci ='" . $user['username'] . "'OR email='" . $user['username'] . "') and password='" . $user['password'] . "'");

        return $result;
    }

    function checkuserWithOldinfo($user)
    {

        $database = new database();
        $result = $database->executeObject("SELECT * FROM user where (userName collate latin1_swedish_ci ='" . $user['username'] . "'OR email='" . $user['username'] . "') and oldPassword='" . $user['password'] . "'");
        if ($result->userId == "") {
            return '0';
        } else {
            return '1';
        }

    }

    function getLeaderForLogin($user)
    {
        $database = new database();
        $result = $database->executeObject("SELECT * FROM user where userName collate latin1_swedish_ci ='" . $user['username'] . "' and password='" . $user['password'] . "' and leaderId = 0 ");

        return $result;
    }

    function selectForgetUsernameOrEmail($userName_email)
    {
        $database = new database();

        return $database->executeObject("SELECT userId,email,firstName,userName FROM user WHERE userName='" . $userName_email . "' OR email='" . $userName_email . "'");
    }

    function setResetVerificationCode($userId, $confirmationCode, $resetPaswordRequestDate)
    {
        $database = new database();
        $database->executeNonQuery("
									INSERT INTO forgetpasswordrequests
									SET
									userId='" . $userId . "',
									confirmationCode='" . $confirmationCode . "',
									requestDate='" . $resetPaswordRequestDate . "'
									");
        $result = $database->getAffectedRows();
        if ($result == 1) {
            return $confirmationCode;

        }

    }

    function getUserId($confirmationcode)
    {
        $database = new database();

        return $database->executeScalar("SELECT userId FROM forgetpasswordrequests WHERE confirmationCode='" . $confirmationcode . "'");
    }

    function resetPassword($newpassword, $userRecordId)
    {
        $database = new database();
        $database->executeNonQuery("UPDATE user SET password='" . $newpassword . "' WHERE userId='" . $userRecordId . "'");

        return 1;
    }

    function getUserForSignupCheck($username)
    {
        $database = new database();
        $user = $database->executeScalar("SELECT userName FROM user WHERE userName='" . $username . "'");
        if (!empty($user)) {
            return 0;
        } else {
            return 1;
        }
    }


    function getEmailForSignupCheck($email, $userId = NULL)
    {
        $database = new database();

        if (isset($userId)) {
            $emailAdd = $database->executeScalar("SELECT email FROM `user` WHERE email='" . $email . "' and
                userId !='" . $userId . "'");
            if (!empty($emailAdd)) {
                return 0;
            } else {
                return 1;
            }
        } else {
            $emailAdd = $database->executeScalar("SELECT email FROM `user` WHERE email='" . $email . "'");
            if (!empty($emailAdd)) {
                return 0;
            } else {
                return 1;
            }
        }
    }


    function VerifyRestPassWordCode($code)
    {
        $database = new database();
        $database->executeNonQuery("UPDATE forgetpasswordrequests SET isVerify=1 WHERE confirmationCode LIKE '" . $code . "'");

    }

    function isVerifiedEmail($code)
    {
        $database = new database();
        $result = $database->executeScalar("SELECT isVerifiedEmail FROM user  WHERE emailVerificationCode='" . $code . "'");
        if (!empty($result)) {
            return 1;
        } else {
            return 0;
        }

    }

    function selectUserIdByUsernameOrEmail($val)
    {
        $database = new database();
        $result = $database->executeObject("SELECT * FROM user where	userName ='" . $val . "' OR  email='" . $val . "' and isVerifiedEmail='1'");

        return $result;
    }

    function createUserPlans($code)
    {
        $database = new database();
        //$userId = $database->executeScalar("SELECT userId FROM user WHERE emailVerificationCode LIKE '".$code."'");
        $userDetails = $database->executeObject("SELECT userId,userName,leaderId, inheritedLeaderId FROM user WHERE emailVerificationCode LIKE '" . $code . "'");
        if (!empty($userDetails)) {
            $userId = $userDetails->userId;
            $username = $userDetails->userName;
            $leaderId = $userDetails->leaderId;
            $inheritedLeaderId = $userDetails->inheritedLeaderId;

            $leaderInfo = $database->executeObject("Select * from `user` where userId = '" . $leaderId . "'");

            if ($leaderId == 0) {
                $id = ($inheritedLeaderId) ? $inheritedLeaderId : 0;
                $sql = "select * from plan where userId='" . $id . "'";
                $plans = $database->executeObjectList($sql);
            } else {
                $plans = $database->executeObjectList("select * from plan where userId='" . $leaderId . "' and privacy = 1 AND isactive = 1");
            }

            foreach ($plans as $p) {

                $pId = ($inheritedLeaderId) ? 0 : $p->planId;
                $title = str_replace("'", "\'", $p->title);

                $database->executeNonQuery("INSERT INTO
					plan(userId, title, isDoubleOptIn, isactive, addDate, groupId, reminderValue, reminderUnit, parentId, leaderId, privacy,generatedVia,oldId) VALUES
					('" . $userId . "' ,
					'" . $title . "',
					'" . $p->isDoubleOptIn . "',
					'" . $p->isactive . "',
					CURDATE(),
					'" . $p->groupId . "',
					'" . $p->reminderValue . "',
					'" . $p->reminderUnit . "',
					'" . $pId . "',
					'" . $leaderId . "',
					'1',
					'2',
					'" . $pId . "')");
                $userPlanId = $database->insertid();
                $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $p->planId . "' and isactive = 1");
                foreach ($palnSteps as $pSteps) {
                    $des = str_replace("'", "\'", $pSteps->description);
                    $summary = str_replace("'", "\'", $pSteps->summary);
                    if ($pSteps->parentId == 0)
                        $parentId = $pSteps->stepId;
                    else
                        $parentId = $pSteps->parentId;;

                    $database->executeNonQuery("INSERT INTO plansteps ( planId, summary, isTask, daysAfter, order_no, description,oldId,parentId)
						VALUES('" . $userPlanId . "', '" . $summary . "', '" . $pSteps->isTask . "','" . $pSteps->daysAfter . "','" . $pSteps->order_no . "','" . $des . "','" . $pSteps->stepId . "','" . $parentId . "')");
                    $userPlanStepId = $database->insertid();
                    if ($pSteps->isTask == '0') {
                        $emailStep = $database->executeObject("select  '" . $userPlanStepId . "' as sid, subject,body, fromUser, templateId from planstepemails  where id='" . $pSteps->stepId . "'");
                        $subject = str_replace("'", "\'", $emailStep->subject);
                        $body = str_replace("'", "\'", $emailStep->body);
                        if ($pSteps->stepId == '46') {
                            $FitProfileLink = '<a href="https://zenplify.biz/' . $username . '/FitProfile" target="_blank">Click Here</a>';
                            $SkinProfileLink = '<a href="https://zenplify.biz/' . $username . '/SkinCareProfile" target="_blank">Click Here</a>';
                            $ConsultantProfileLink = '<a href="https://zenplify.biz/' . $username . '/ConsultantProfile" target="_blank">Click Here</a>';
                            //$body = implode($FitProfileLink, explode('FitProfileLink', $body, 2));
                            //$body = implode($SkinProfileLink, explode('SkinProfileLink', $body, 2));
                            //$body = implode($ConsultantProfileLink, explode('ConsultantProfileLink', $body, 2));
                            $body = str_replace('FitProfileLink', $FitProfileLink, $body);
                            $body = str_replace('SkinProfileLink', $SkinProfileLink, $body);
                            $body = str_replace('ConsultantProfileLink', $ConsultantProfileLink, $body);

                        } else if ($pSteps->oldId == '46') {
                            $userFitProfileLink = 'https://zenplify.biz/' . $username . '/FitProfile';
                            $userSkinProfileLink = 'https://zenplify.biz/' . $username . '/SkinCareProfile';
                            $userConsultantProfileLink = 'https://zenplify.biz/' . $username . '/ConsultantProfile';

                            $leaderFitProfileLink = 'https://zenplify.biz/' . $leaderInfo->userName . '/FitProfile';
                            $leaderSkinProfileLink = 'https://zenplify.biz/' . $leaderInfo->userName . '/SkinCareProfile';
                            $leaderConsultantProfileLink = 'https://zenplify.biz/' . $leaderInfo->userName . '/ConsultantProfile';

                            $body = str_replace($leaderFitProfileLink, $userFitProfileLink, $body);
                            $body = str_replace($leaderSkinProfileLink, $userSkinProfileLink, $body);
                            $body = str_replace($leaderConsultantProfileLink, $userConsultantProfileLink, $body);
                        }

                        if ($pSteps->stepId == '80') {
                            $GuestProfileLink = '<a href="https://zenplify.biz/' . $username . '/GuestProfile" target="_blank">Click Here</a>';
                            $body = str_replace('GuestProfileLink', $GuestProfileLink, $body);
                        } else if ($pSteps->oldId == '80') {
                            $userGuestProfileLink = 'https://zenplify.biz/' . $username . '/GuestProfile';

                            $leaderGuestProfileLink = 'https://zenplify.biz/' . $leaderInfo->userName . '/GuestProfile';

                            $body = str_replace($leaderGuestProfileLink, $userGuestProfileLink, $body);
                        }
                        $body = mysql_real_escape_string($body);
                        $database->executeNonQuery("INSERT INTO planstepemails(id, subject, body, fromUser, templateId) VALUES ('" . $emailStep->sid . "', '" . $subject . "', '" . $body . "', '" . $emailStep->fromUser . "', '" . $emailStep->templateId . "')");
                    }
                }
                if ($p->groupId != 0 || $p->groupId != '') {
                    $plan = new plan();
                    //echo "User Id Plan: ".$userId;
                    $plan->UpdatePlanGroup($userPlanId, $p->groupId, $userId);
                }
            }
        }
    }

    function createUserGroups($code)
    {
        $database = new database();
        //$userId = $database->executeScalar("SELECT userId FROM user WHERE emailVerificationCode LIKE '".$code."'");
        $userDetails = $database->executeObject("SELECT userId,userName,leaderId, inheritedLeaderId FROM user WHERE emailVerificationCode LIKE '" . $code . "'");
        if (!empty($userDetails)) {
            $userId = $userDetails->userId;
            $username = $userDetails->userName;
            $leaderId = $userDetails->leaderId;
            $inheritedLeaderId = $userDetails->inheritedLeaderId;
            $leaderInfo = $database->executeObject("Select * from `user` where userId = '" . $leaderId . "'");
            if ($leaderId == 0) {
                $id = ($inheritedLeaderId) ? $inheritedLeaderId : 0;
                $sql = "SELECT * FROM groups WHERE isactive = 1 AND userId = '" . $id . "'";
                $groups = $database->executeObjectList($sql);
                foreach ($groups as $g) {
                    if ($g->userId == 0 || $inheritedLeaderId != null) {
                        $generatedVia = 1;
                    } else {
                        $generatedVia = 2;
                    }
                    $gId = ($inheritedLeaderId) ? 0 : $g->groupId;
                    $database->executeNonQuery("INSERT INTO groups(userId, groupTypeId, name, description, color, orderBy, isactive, privacy, parentId, leaderId, generatedVia) VALUES('" . $userId . "' , '" . $g->groupTypeId . "', '" . $g->name . "', '" . mysql_real_escape_string($g->description) . "', '" . $g->color . "', '" . $g->orderBy . "', '1','1','" . $gId . "','" . $leaderId . "','" . $generatedVia . "')");
                    $groupId = $database->insertid();
                    $database->executeNonQuery("Insert into `groupusers` (`groupId`,`userId`,`isactive`) values ('" . $groupId . "','" . $userId . "',1)");
                }
            } else {
                if ($leaderInfo->registrationDate < registrationDate) {
//					if ($leaderId == 236) {
                    $groups = $database->executeObjectList("SELECT * FROM groups WHERE userId = 0 AND isactive= 1 UNION SELECT * FROM `groups` WHERE `isactive` = 1 and `privacy` = 1 and `groupId` IN (SELECT gu.groupId FROM `groupusers` gu JOIN groups g ON g.`groupId` = gu.`groupId` WHERE gu.`userId` = '" . $leaderId . "' AND gu.`isactive` = 1 AND g.`privacy` =1)");
                } else {
                    $groups = $database->executeObjectList("SELECT * FROM `groups` WHERE `isactive` = 1 and `privacy` = 1 and `groupId` IN (SELECT gu.groupId FROM `groupusers` gu JOIN groups g ON g.`groupId` = gu.`groupId` WHERE gu.`userId` = '" . $leaderId . "' AND gu.`isactive` = 1 AND g.`privacy` =1)");
                }


                foreach ($groups as $g) {
                    $database->executeNonQuery("Insert into `groupusers` (`groupId`,`userId`,`isactive`) values ('" . $g->groupId . "','" . $userId . "',1)");
                }
            }
//					$groups = $database->executeObjectList("SELECT * FROM groups WHERE userId = '" . $leaderId . "' and isactive = 1 and privacy = 1");
        }
    }

    function SignupCheckForUser($username, $useremail)
    {
        $database = new database();

        $user = $database->executeObject("SELECT * FROM user WHERE userName='" . $username . "' || email='" . $useremail . "'");
        if (!empty($user)) {
            return 1;
        } else {
            return 0;
        }
    }

    function resendEmailVerification($email)
    {
        $database = new database();

        return $user = $database->executeObject("SELECT * FROM user WHERE  email='" . $email . "'");
    }

    function getAdminForLogin($username, $password)
    {
        //$password='".$user['password']."';
        //$password1=$encryption->encrypt('abc*def',$password,16);
        $encryption = new encryption_class();
        $key = 'abc*def';
        $database = new database();
        $result = $database->executeObject("SELECT * FROM admin where username='" . $username . "' ");
        //return $result;
        $decrypt_pass = $encryption->decrypt($key, $result->password);
        if ($decrypt_pass != $password) {
            SendRedirect("index.php?mes=1");
            /*echo "<script language=JavaScript>
            window.location='signup.php?mes=1'
            </script>";*/
        } else {
            $_SESSION['username'] = $result->username;
            $_SESSION['password'] = $decrypt_pass;
            SendRedirect("login.php");
            exit;
        }
    }

    function getuserNameById($userId)
    {
        $database = new database();

        return $userName = $database->executeScalar("SELECT firstName FROM user WHERE  userId='" . $userId . "'");
    }

    function getLeaderTemplates()
    {
        $database = new database();

        return $database->executeObjectList("SELECT u.`signupName` AS userName, u.`userId`,up.`expiryDate` FROM `user` u JOIN `userpayments` up ON u.`userId`=up.`userId` WHERE u.leaderId =0 AND up.`expiryDate`>=CURRENT_DATE AND isAllowSignUp='2' AND u.userId NOT IN (4,2311,2312,2316,2528,2661)");
    }

    function addLeaderDefaultFields($userId)
    {
        $database = new database();
        $database->executeNonQuery("INSERT INTO leaderdefaultfields(userId,fieldCustomName,fieldMappingName) (SELECT '" . $userId . "',fieldCustomName,fieldMappingName FROM leaderdefaultfields WHERE userId = 0)");
    }

    /**
     * @param $userId
     * @param $inheritedLeaderId
     */
    function addTemplateDefaultFields($userId, $inheritedLeaderId)
    {
        $database = new database();
        $sql = "SELECT * FROM `leadertemplatedefaults` WHERE `leaderId` = '" . $inheritedLeaderId . "'";
        $record = $database->executeObject($sql);
        if (count($record) > 0) {
            $sql = "INSERT INTO `leadertemplatedefaults` (`leaderId`, `defaultText`, `defaultProfileHeader`, `defaultProfileVideo`, `defaultThankYouHeader`, `defaultThankYouVideo`) (SELECT '" . $userId . "', `defaultText`, `defaultProfileHeader`, `defaultProfileVideo`, `defaultThankYouHeader`, `defaultThankYouVideo` FROM `leadertemplatedefaults` WHERE `leaderId` = '" . $inheritedLeaderId . "')";
            $database->executeNonQuery($sql);
        } else if ($inheritedLeaderId != 236) {
            $sql = "SELECT `profileDefaultText`, `thankyouDefaultText`, `profileVideoLink`,	`thankyouVideoLink`, `profileHeader`, `thankyouHeader`
            FROM `pages` WHERE `userId` = '" . $inheritedLeaderId . "' AND `status` = 1 LIMIT 1";
            $record = $database->executeObject($sql);
            if ($record) {
                $sql = "INSERT INTO `leadertemplatedefaults` SET `leaderId` = '" . $userId . "', `defaultText` = '" . $record->profileDefaultText . "', `defaultProfileHeader` = '" . $record->profileHeader . "', `defaultProfileVideo`  = '" . $record->profileVideoLink . "', `defaultThankYouHeader` = '" . $record->thankyouHeader . "', `defaultThankYouVideo` = '" . $record->thankyouVideoLink . "'";
                $database->executeNonQuery($sql);
            }
        } else {
            $sql = "INSERT INTO `leadertemplatedefaults` (`leaderId`, `defaultText`, `defaultProfileHeader`, `defaultProfileVideo`, `defaultThankYouHeader`, `defaultThankYouVideo`) (SELECT '" . $userId . "', `profileDefaultText`, `profileHeader`, `profileVideoLink`, `thankyouHeader`, `thankyouVideoLink` FROM `pagetemplate` WHERE `pageTemplateId` = '11')";
            $database->executeNonQuery($sql);
        }

    }

    //single quote removal
    function matchCurrentPassword($POST)
    {
        $database = new database();

        return $database->executeScalar("SELECT username FROM user WHERE userId='" . $POST['userId'] . "' and `password` = '" . $POST['current_password'] . "'");
    }

    //single quote removal

    function getUserIdbyUsername($database, $signupName)
    {
        return $database->executeScalar("Select `userId` from `user` where `signupName` = '" . $signupName . "'");
    }

    function checkUserAvailable($leaderSignUp)
    {
        $database = new database();

        return $database->executeScalar("Select `userId` from `user` where `signupName` = '" . $leaderSignUp . "' and leaderId='0' and isOntraport='1'");
    }

    function checkUserAvailablity($leaderSignUp)
    {
        $database = new database();

        return $database->executeScalar("Select `userId` from `user` where `signupName` = '" . $leaderSignUp . "' and leaderId='0' and `isAllowSignUp`='2'");
    }

    function copyquestionintoRadiolable($database, $qId, $newQuestionId)

    {

        $database->executeNonQuery("Insert into `questionradiolabels` (`questionId`,`label`,`order`) select '" . $newQuestionId . "',`label`,`order`
             from `questionradiolabels` where questionId = '" . $qId->questionId . "'");

    }

// ontraport required code


    function getEmailVerificationCode($email)
    {
        $database = new database();

        return $code = $database->executeScalar("SELECT emailVerificationCode FROM `user` WHERE  email='" . $email . "' or ontraportUserName='" . $email . "'");
    }

    function updateInformation($userId, $password, $userName)
    {


        $database = new database();
        $sql = "UPDATE `user` SET `password`= '" . $password . "',userName='" . $userName . "',isVerifiedPaypal='1',isOntraport='1',isOntraportPaymentActive='1' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);
    }

    function userId($email, $code)
    {
        $database = new database();

        return $userId = $database->executeScalar("SELECT userId FROM `user` WHERE  email='" . $email . "' AND emailVerificationCode='" . $code . "' OR (ontraportUserName='" . $email . "' AND emailVerificationCode='" . $code . "')");
    }

    function selectLeaderemail($leaderId)
    {
        $database = new database();

        return $userId = $database->executeScalar("SELECT email FROM `user` WHERE   userId='" . $leaderId . "'");
    }

    function getOntraportLogin($email, $password, $token)
    {
        $database = new database();
        $result = $database->executeObject("SELECT u.`userId`,u.`leaderId`,u.firstName,u.userName  FROM `user` u JOIN `userpayments` up ON u.userId=up.userId left JOIN `usersOntraport` uop ON u.userId=uop.userId WHERE u.`email`  ='" . $email . "' AND u.`password`='" . $password . "' AND u.`isVerifiedEmail`='1' AND u.`isVerifiedPaypal`='1'");

        return $result;
    }

    function updateToken($email, $token)
    {
        $database = new database();
        $userId = $database->executeScalar("SELECT userId FROM `user` WHERE  email='" . $email . "' or ontraportUserName='" . $email . "'");
        $sql = "UPDATE `usersOntraport` SET `securityToken`= '" . $token . "' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);
    }

    function updateUserAndOntraportInfo($userId, $password, $contactId)
    {


        $database = new database();
        $sql = "UPDATE `user` SET `password`= '" . $password . "',isVerifiedPaypal='1',isOntraport='1',isOntraportPaymentActive='1' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);
        $userIdExistance = $database->executeScalar("SELECT userId FROM `usersOntraport` WHERE  userId='" . $userId . "'");
        if (!empty($contactId) && empty($userIdExistance)) {

            $database->executeNonQuery("Insert into `usersOntraport` (`userId`,`contactId`) values ('" . $userId . "','" . $contactId . "')");


        }
    }

    function addUser($email, $password, $leaderId, $userName, $first_name, $last_name)
    {
        $database = new database();
        $registrationDate = date('Y-m-d');
        $code = generateUniqueID();
        $database->executeNonQuery("Insert into `user` (userName,email,`password`,`firstName`,`lastName`,isVerifiedEmail,isVerifiedPaypal,emailVerificationCode,leaderId,isOntraport,isOntraportPaymentActive, registrationDate,ontraportUserName) values ('" . $userName . "','" . $email . "','" . $password . "','" . $first_name . "','" . $last_name . "','0','0','" . $code . "','" . $leaderId . "','1','1', '" . $registrationDate . "','" . $email . "')");
        return $code;
    }

    function getleaderId($signupName)
    {
        $database = new database();

        return $leaderId = $database->executeScalar("SELECT userId FROM `user` WHERE  signupName='" . $signupName . "' and leaderId='0'");
    }

    function  isOntraportUser($userId)
    {

        $database = new database();
        $isOntraportUser = $database->executeObject("SELECT isOntraport,isTransferredToOntraport FROM `user` WHERE  userId='" . $userId . "'");

        if ($isOntraportUser->isOntraport == 1 || $isOntraportUser->isTransferredToOntraport) {
            return '1';
        } else {
            return '0';
        }

    }

    function  isOntransferredUser($token)
    {

        $database = new database();
        $userId = $database->executeScalar("SELECT  DISTINCT(userId) FROM `usersOntraport` WHERE securityToken='" . $token . "'");
        $isOntraportUser = $database->executeObject("SELECT userId,isTransferredToOntraport FROM `user` WHERE  userId='" . $userId . "'");

        if ($isOntraportUser->isTransferredToOntraport == 1) {
            return $isOntraportUser->userId;
        } else {
            return '0';
        }

    }

    function  leaderSignupName($leaderId)
    {

        $database = new database();
        return $leaderSignupName = $database->executeScalar("SELECT signupName FROM `user` WHERE  userId='" . $leaderId . "'");

    }

    function setLoginForOntraport($userId, $status)
    {

        $database = new database();
        $sql = "UPDATE `usersOntraport` SET `isLogin`= '" . $status . "' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);

    }

    function  isOntraportUserLogin($userId)
    {

        $database = new database();
        return $isOntraportUserLogin = $database->executeScalar("SELECT isLogin FROM `usersOntraport` WHERE  userId='" . $userId . "'");

    }

    function setLogOutForOntraport($email)
    {

        $database = new database();
        $userId = $database->executeScalar("SELECT userId FROM `user` WHERE  email='" . $email . "'");
        $sql = "UPDATE `usersOntraport` SET `isLogin`= '0' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);

    }

    function getLeaderSignupByEmail($email, $token = NULL)
    {
        $database = new database();
        if ($token == "" || empty($token)) {
            $leaderId = $database->executeScalar("SELECT leaderId FROM `user` WHERE  email='" . $email . "'");
        } else {
            echo("SELECT u.leaderId FROM `user` u JOIN `usersOntraport` uo ON u.`userId`=uo.`userId` WHERE  uo.`securityToken`='" . $token . "' ORDER BY uo.userId LIMIT 1'");

            $leaderId = $database->executeScalar("SELECT u.leaderId FROM `user` u JOIN `usersOntraport` uo ON u.`userId`=uo.`userId` WHERE  uo.`securityToken`='" . $token . "' ORDER BY uo.userId LIMIT 1");
        }
        return $leaderSignupName = $this->leaderSignupName($leaderId);

    }


    function getEmailcode($token)
    {
        $database = new database();
        return $getemailCode = $database->executeScalar("SELECT u.emailVerificationCode FROM `user` u JOIN `usersOntraport` uo ON u.`userId`=uo.`userId` WHERE  uo.`securityToken`='" . $token . "' ORDER BY uo.userId LIMIT 1");

    }

    function getUserInfo($emailVerificationCode)
    {
        $database = new database();
        return $userIndo = $database->executeObject("SELECT *  FROM `user` WHERE emailVerificationCode= '" . $emailVerificationCode . "'");
    }

    //************ update ontraport user ********************//

    function udpdateUserinformation($postArray, $userId)
    {

        $firstName = $postArray['firstname'];
        $lastName = $postArray['lastname'];

        $timeZoneId = $postArray['timezone'];
        $phoneNumber = $postArray['phonenumber'];

        $consultantId = $postArray['consultant_id'];
        $title = $postArray['title'];
        $webAddress = $postArray['web'];
        $facebookAddress = $postArray['facebook_page'];
        $facebookPersonalAddress = $postArray['facebook_personalpage'];
        $facebookPartyAddress = $postArray['facebook_partypage'];


        if (!empty($postArray)) {
            $database = new database();


            $sql = "UPDATE  user
                                     SET


                                    timeZoneId='" . $timeZoneId . "',
                                    phoneNumber='" . $phoneNumber . "',
                                    firstName='" . $firstName . "',
                                    lastName='" . $lastName . "',
                                    consultantId='" . $consultantId . "',
                                    title='" . $title . "',
                                    webAddress='" . $webAddress . "',
                                    facebookAddress='" . $facebookAddress . "',
                                    facebookPersonalAddress='" . $facebookPersonalAddress . "',
                                    isProfileComplete='1',
                                    facebookPartyAddress='" . $facebookPartyAddress . "' WHERE userId='" . $userId . "'";

            $database->executeNonQuery($sql);
            session_start();
            $_SESSION["AdminFname"] = $firstName;

            return '1';
        }


    }

    function  isOntraportleader($userId)
    {

        $database = new database();
        $isOntraportleader = $database->executeScalar("SELECT signupName FROM `user` WHERE  userId='" . $userId . "' and isOntraport='1'");
        if (empty($isOntraportleader) && $isOntraportleader == '') {
            return '1';
        } else {
            return $isOntraportleader;
        }

    }

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    function updatePassword($username, $password)
    {
        $database = new database();

        $userId = $database->executeScalar("SELECT userId FROM `user` WHERE  ontraportUserName='" . $username . "' limit 1");
        $sql = "UPDATE `user` SET `password` = '" . $password . "' WHERE userId = '" . $userId . "'";
        $database->executeNonQuery($sql);
        if ($database->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }

    function updateOntraportPaymentInfo($userId, $status)
    {
        $database = new database();
        $sql = "UPDATE `user` SET `isOntraportPaymentActive`= '" . $status . "' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);

    }

    function ontraportLogin($token)
    {
        $database = new database();
        $result = $database->executeObject("SELECT u.`userId`,u.`leaderId`,u.firstName,u.userName
FROM `user` u JOIN `userpayments` up ON u.userId=up.userId LEFT JOIN
 `usersOntraport` uop ON u.userId=uop.userId
 WHERE uop.`securityToken`='" . $token . "' AND up.`expiryDate`> CURRENT_DATE() GROUP BY u.userId");
        return $result;
    }

    function updateLoginStatus($email, $status)
    {

        $database = new database();
        $userId = $database->executeScalar("SELECT userId FROM `user` WHERE  email='" . $email . "'");
        $sql = "UPDATE `usersOntraport` SET `isLogin`= '" . $status . "' WHERE userId='" . trim($userId, ' ') . "'";
        $database->executeNonQuery($sql);

    }

    function contactimportedToOntraport($email, $password)
    {


        $database = new database();
        $sql = "UPDATE `user` SET `isTransferredToOntraport`= '1',ontraportUserName='" . $email . "', `password` = '" . $password . "' WHERE email='" . $email . "'";
        $database->executeNonQuery($sql);
    }

    function insertOntraportImportUsers($userId, $contactId)
    {


        $database = new database();


        $database->executeNonQuery("Insert into `usersOntraport` (`userId`,`contactId`) values ('" . $userId . "','" . $contactId . "')");


    }

    function checkIfExist($token)
    {
        $database = new database();
        $result = $database->executeObject("SELECT  up.`expiryDate`
FROM `user` u JOIN `userpayments` up ON u.userId=up.userId LEFT JOIN
 `usersOntraport` uop ON u.userId=uop.userId
 WHERE uop.`securityToken`='" . $token . "' ORDER BY up.`paymentId` DESC LIMIT 1");
        if (empty($result)) {
            return '0';
        } else {
            return $result->expiryDate;
        }

    }

    // create signature and add against user in usersignature
    function createUserSignature($code)
    {

        $database = new database();
        $details = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber,u.inheritedLeaderId, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.leaderId,u.userId from user u where u.emailVerificationCode='" . $code . "'");
        $consultantTitleUserId = '';

        if ($details->leaderId == 0) {
            $consultantTitleUserId = $details->userId;
        } else {
            $consultantTitleUserId = $details->leaderId;
        }
        $consultantTitle = self::getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
        if (
            ($details->userId == '3736' || $details->leaderId == '3736') ||
            ($details->userId == '2306' || $details->leaderId == '2306') ||
            ($details->userId == '236' || $details->leaderId == '236') ||
            $details->inheritedLeaderId == '236' ||
            $details->inheritedLeaderId == '2306'
        ) {
            // Exclude Juice Plus users

            $arbonLogo = '<img src="https://zenplify.biz/images/GreenLogo1x1.png">';
        } else {
            $arbonLogo = '';
        }


        if ($details->userId == '236' || $details->leaderId == '236') {
            $arbonneCunsultancey = 'Arbonne Consultant';
        } else {
            $arbonneCunsultancey = '';
        }

        $signature = '<span style="font-size:14px;">Best Regards,</span><br /><br /><span style="font-size:14px;">' . $details->firstName . '</span><br />
			<div><div style="float:left;">' . $arbonLogo . '</div><div style=" margin-left:5px;float:left;font-size:12px;">' . $details->firstName . ' ' . $details->lastName;


        if ($details->title != '')
            $signature = $signature . ', ' . $details->title;
        if (strpos($details->webAddress, "http://") >= 0 || strpos($details->webAddress, "https://") >= 0)
            $web = $details->webAddress;
        else
            $web = "http://" . $details->webAddress;
        if (strpos($details->facebookAddress, "http://") >= 0 || strpos($details->facebookAddress, "https://") >= 0)
            $facebook = $details->facebookAddress;
        else
            $facebook = "http://" . $details->facebookAddress;
        $webNew = preg_replace('#^https?://#', '', $web);
        $fbNew = preg_replace('#^https?://#', '', $facebook);


        $consultantTitleId = $details->consultantId;


        $signature = $signature . '<br /><span style="color: #2E6A30;font-size:12px;"> ' . $arbonneCunsultancey . '  ' . $consultantTitleId . '</span><br /><span style="color: #2E6A30;font-size:12px;"><a style="color: #2E6A30;font-size:12px;" href="' . $web . '" target="_blank">' . $webNew . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $facebook . '" target="_blank">' . $fbNew . '</a></div></div>';

        $addSignature = $database->executeNonQuery("INSERT INTO `usersignature` (userId,signature) VALUE ('" . $details->userId . "','" . mysql_real_escape_string($signature) . "')");

        return $addSignature;
    }

    function  getLeadersTemplate()
    {


        $database = new database();

        return $database->executeObjectList("SELECT u.`signupName` AS userName,lt.`defaultText`,lt.`displayName`, u.`userId`,up.`expiryDate`
FROM `user` u JOIN `leadertemplate` lt ON u.`userId`=lt.`userId`
JOIN `userpayments` up ON u.`userId`=up.`userId` WHERE u.leaderId =0 AND
up.`expiryDate`>=CURRENT_DATE AND isAllowSignUp='2' AND u.userId NOT IN (4,2311,2312,2316,2528,2661)ORDER BY orderId ASC");

    }
}

?>

