<script type="text/javascript">
$().ready(function () {
	$("#addQuickContactForm").validate();
	$("#add_new_task").validate();
	$("#add_new_appiontment").validate();
	$("#editGroupForm").validate();
	//validate contactat form
	$("#resetPasswordForm").validate({
		rules   : {

			newPassword       : {
				required : true,
				minlength: 5
			},
			newconfirmPassword: {
				required : true,
				minlength: 5,
				equalTo  : "#newPassword"
			}
		},
		messages: {

			newPassword       : {
				required : "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			newconfirmPassword: {
				required : "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo  : "Password does not match"
			}
		}
	});
	//validate groups form
	$("#addGroupForm").validate();
	//validate contactat form
	$("#addContactForm").validate({
		rules   : {
			firstname: "required",
			lastname : "required",
			email    : {
				required: true,
				email   : true
			}
		},
		messages: {
			firstname: "Please enter First Name",
			lastname : "Please enter Last Name",
			email    : "Please enter Email Address"
		}
	});
	//validate signin form
	$("#login_user").validate();
	// validate signup form on keyup and submit
	$("#add_user").validate({
		rules   : {
			firstname            : "required",
			lastname             : "required",
			username             : {
				required : true,
				minlength: 2
			},
			password             : {
				required : true,
				minlength: 5
			},
			confirm_password     : {
				required : true,
				minlength: 5,
				equalTo  : "#password"
			},
			email                : {
				required: true,
				email   : true
			},
			emailConfirm         : {
				required: true,
				email   : true,
				equalTo : "#email"
			},
			consultant_id        : {
				required: true

			},
			/*title:{
			 required:true

			 },*/
			web                  : {
				required: true

			},
			facebook_page        : {
				required: true
			},
			facebook_personalpage: {
				required: false
			},
			facebook_partypage   : {
				required: true
			},
			company              : {
				required: true
			},
			newpassword          : {
				minlength: 5
			},
			confirm_newpassword  : {
				minlength: 5,
				equalTo  : "#newpassword"
			},
			termandconditions: "required"
		},
		messages: {
			firstname        : "Please enter your firstname",
			lastname         : "Please enter your lastname",
			username         : {
				required : "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password         : {
				required : "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password : {
				required : "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo  : "Please enter the same password as above"
			},
			emailConfirm     : { required: "Please confirm email address",
				equalTo: "Please enter the same email as above"},
			consultant_id    : {
				required: "Please provide a consultant Id"
			},
			/*title:{
			 required:"Please provide a title"

			 },*/
			email            : "Please enter a valid email address",
			company          : "Please enter your company name",
			termandconditions: "Please accept our Terms of Servce & Privacy Policy"
		}
	});
	// check for already existed useername and email
	$('#username').keyup(function () {
		var username = $(this).val();
		if(username == "" || username.length < 2) {
			$("#msg").html('');
		}
	});
	$('#username').blur(function () {
		var username = $(this).val();
		if(!/^[a-zA-Z0-9]+$/.test(username)) {
			$("#msg").html('Only Alphanumeric characters are allowed.').css('color', 'red');
			$("#userSpecialcheck").val(0);
			return false;
		}
		else if(username.trim() == "support") {
			$("#msg").html('Support cannot be set as username.').css('color', 'red');
			$("#userSpecialcheck").val(0);
			return false;
		}
		if(username == "" || username.length < 2) {
			$("#msg").html('');
		}
		else {
			// ajax call for check username already exist
			$.ajax({
				url    : "../../classes/ajax.php",
				type   : "post",
				data   : {action: '1', us: $(this).val()},
				success: function (result) {
					if(result == 0) {
						$("#msg").html('This Username is Unavailable. Please try another one.').css('color', 'red');;
						$("#usercheck").val(result);
						$("#userSpecialcheck").val(result);
					}
					else {
						$("#msg").html('Username is Available').css('color', '#20BF04');
						$("#usercheck").val(result);
						$("#userSpecialcheck").val(result);
					}
				}
			});
		}
	});
	$('#password').blur(function (e) {
		var userPassword = $('#password').val();
		if(userPassword.length > 5) {
			if(userPassword.indexOf("'") == -1) {
				$("#passwordMsg").hide();
				$("#usercheck").val(1);
			}
			else {
				$("#passwordMsg").html('Password cannot contain Single quotes').css('color', 'red').show();
				$("#usercheck").val(0);
			}
		}
		//var result = web.replace(/.*?:\/\//g, "");
		//		/$('#web').val(result);
	});
	$('#email').keyup(function () {
		var email = $(this).val();
		var isvalid = isValidEmailAddress(email);
		if(email == "" || isvalid == false) {
			$("#msge").html('');
		}
	});
	$('#email').blur(function () {
		var email = $(this).val();
		var isvalid = isValidEmailAddress(email);
		if(email == "" || isvalid == false) {
			$("#msge").html('');
		}
		else {
			// ajax call for check email already exist
			$.ajax({
				url    : "../../classes/ajax.php",
				type   : "post",
				data   : {action: '2', em: $(this).val()},
				success: function (result) {
					if(result == 0) {
						$("#msge").html('This Email Address already exists.').css('color', 'red');
						$("#emailcheck").val(result);
					}
					else {
						$("#msge").html('Email Address is Available').css('color', '#20BF04');
						$("#emailcheck").val(result);
					}
				}
			});
		}
	});
	//delete task on contact detil screen
	$('#signupName').blur(function () {
		var signupName = $(this).val();
		if(!/^[a-zA-Z0-9]+$/.test(signupName)) {
			$("#msg").html('Only Alphanumeric characters are allowed.').css('color', 'red');
			$("#signupNameCheck").val(0);
			return false;
		}
		else if(signupName.trim() == "support") {
			$("#msg").html('Support cannot be set as signup name.').css('color', 'red');
			$("#signupNameCheck").val(0);
			return false;
		}
		if(signupName == "" || signupName.length < 2) {
			$("#msg").html('');
		}
		else {
			// ajax call for check username already exist
			$.ajax({
				url    : "../../classes/ajax.php",
				type   : "post",
				data   : {action: 'checkLeaderSignupName', us: $(this).val()},
				success: function (result) {
					if(result == 0) {
						$("#msg").html('This signup Name is Unavailable. Please try another one.');
						$("#usercheck").val(result);
					}
					else {
						$("#msg").html('Signup Name is Available').css('color', '#20BF04');
						$("#usercheck").val(result);
					}
				}
			});
		}
	});
});
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	// alert( pattern.test(emailAddress) );
	return pattern.test(emailAddress);
}
;
function cancelPayment(userId, paypalprofileId) {
	var paymentConfirm = confirm('Are you sure to cancel future payments.');
	if(paymentConfirm == true) {
		window.location.href = "paypal.php?cancelPayment=1&profileId=" + paypalprofileId + "&userId=" + userId;
	}
}
</script>