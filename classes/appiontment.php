<?php
require_once ('init.php');

class appiontment{
	
	function upcomingActivitiesAppiontments($userId,$contactId){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM appointments  WHERE userId='".$userId."' AND appiontmentId IN (select appiontmentId FROM appiontmentcontacts WHERE contactId='".$contactId."')    AND endDateTime >= CURDATE()" );
		
	}
	function deleteAppiontmentOnContactDetail($appiontmentId,$contactId){
		
		$database = new database();
		$database->executeObjectList("DELETE FROM appiontmentcontacts WHERE appiontmentId='".$appiontmentId."' AND contactId='".$contactId."'");
		if($database->getAffectedRows()!='')
		{
			return 1;
		}
	}
	function GetAllAppointments($userId)
	
	{
		$database = new database();
		return $database->executeObjectList("SELECT * FROM appointments  WHERE userId='".$userId."'");
		
	}
	function GetTodaysAppointments($userId)
	
	{
		$database = new database();
		return $database->executeObjectList("SELECT * FROM appointments  WHERE userId='".$userId."' AND CURDATE()=DATE(startDateTime) LIMIT 5");
		
	}
	function editAppiontment($appiontmentId){
		$database = new database();
		return $database->executeObject("SELECT * FROM appointments WHERE appiontmentId='".$appiontmentId."'");
			
	}
	function getAppiontmentReminder($appiontmentId){
		$database = new database();
		return $database->executeObject("SELECT * FROM appointmentsreminder WHERE appointmentId='".$appiontmentId."'");
	
	}
	function getAppiontmentCustomRule($customRuleId){
		$database = new database();
		return $database->executeObject("SELECT * FROM customrules WHERE customRuleId='".$customRuleId."'");
	}
	function getAppiontmentCustomRuledaily($customRuleId){
		$database = new database();
		return $database->executeObject("SELECT * FROM customrulesdaily WHERE customRuleId='".$customRuleId."'");
			
	}
	function getAppiontmentCustomRuleWeekly($customRuleId){
		$database = new database();
		return $database->executeObject("SELECT * FROM customruleweekly WHERE customRuleId='".$customRuleId."'");
	}
	function getAppiontmentCustomRuleMonthly($customRuleId){
		$database = new database();
		return $database->executeObject("SELECT * FROM customrulemonthly WHERE customRuleId='".$customRuleId."'");
	}
	function getAppiontmentCustomRuleYearly($customRuleId){
		$database = new database();
		return $database->executeObject("SELECT * FROM customrulesyearly WHERE customRuleId='".$customRuleId."'");
	}
	function getAppiontmentContacts($appiontmentId){
			$database = new database();
			$result =$database->executeObjectList("SELECT contactId as id, CONCAT(firstName,' ',lastName) as name from contact WHERE contactId IN (SELECT contactId FROM appiontmentcontacts WHERE  appiontmentId='".$appiontmentId."')");
			$rar='';
			foreach($result as $r){
			$rar.='{"id":"'.$r->id.'","name":"'.$r->name.'"},';	
				}
	return $rar;
			}	
	
					
		
}
		

?>