<?php
    session_start();

    class settings
    {
        function getLeaderSettings($userId)
        {
            $database = new database();
            return $database->executeObject("select `signupName`, `upComingTasks` from `user` where `userId` = '".$userId."'");
        }

        function getUserForSignupCheck($signupName,$userId)
        {
            $database = new database();
            $result = $database->executeScalar("select userId from `user` where `signupName` = '".$signupName."' and `userId` != '".$userId."'");
            if(empty($result))
                return 0;
            else
                return $result;
        }

        function saveLeaderSignupName($signupName)
        {

            $database = new database();


           $checkUser= $database->executeObject("select signupName from `user` where signupName='".$signupName."' and  `userId`  NOT IN (".$_SESSION['userId'].")
           ");
           
            if (empty($checkUser->signupName)){

             $database->executeObject("Update `user` set `signupName` = '".$signupName."' where `userId` = '".$_SESSION['userId']."'");
                return '1';
        } else {

                return '0';
            }
        }

        function getLeaderDefaultFields($userId, $status = 0)
        {
            $database = new database();
            $where;
            if($status == '1')
                $where = "and `status` = '".$status."'";
            return $database->executeObjectList("select * from `leaderdefaultfields` where `userId` = '".$userId."' ".$where." order by `orderId`");
        }

        function saveLeaderDefaultFields($fieldName, $fieldId)
        {
            $database = new database;
            return $database->executeNonQuery("update `leaderdefaultfields` set `fieldCustomName` = '".$fieldName."' where `id` = '".$fieldId."'");
        }

        function toggleLeaderDefaultFields($id, $legend)
        {
            $database = new database();
            $table = ($legend == 'defaultField' ? 'leaderdefaultfields' : 'leaderquestions');
            $field = ($legend == 'defaultField' ? 'id' : 'questionId');
            return $database->executeNonQuery("update `".$table."` set `status` = IF(status=1, 0, 1) where `".$field."` = '".$id."'");
        }

        function getLeaderCustomQuestionTypes($action = 'add')
        {
            $database = new database;
			$sql = "select * from `questiontype` where `isLeaderEnable` = '1'";
			if ($action == 'edit') {
				$sql .= " OR `typeId` = '5'";
			}
            return $database->executeObjectList($sql);
        }

        function addLeaderCustomQuestions($POST)
        {
            $database = new database;
            $database->executeNonQuery("Insert into `questions` set `question` = '".addslashes($POST['txtFieldLabel'])."', `typeId` = '".$POST['ddlFieldType']."', `order` = '1'");
            $questionId = $database->insertid();
            $i = 1;
            if($POST['ddlFieldType'] == 3)
            {
                foreach($POST['txtCheckbox'] as $txtCheckBox)
                {
                    $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '".$questionId."', `choiceText` = '".addslashes($txtCheckBox)."', `order` = '".$i."'");
                    $i++;
                }
            } //checkbox
            elseif($POST['ddlFieldType'] == 4)
            {
                foreach($POST['txtRadioBtn'] as $txtRadioBtn)
                {
                    $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '".$questionId."', `choiceText` = '".addslashes($txtRadioBtn)."', `order` = '".$i."'");
                    $i++;
                }
            }
            //radiobutton
            $database->executeNonQuery("Insert into `leaderquestions` set `leaderId` = '".$_SESSION['userId']."', `questionId` = '".$questionId."', `mandatory` = '".$_POST['rdMandatory']."'");
        }

        function updateLeaderCustomQuestions($POST)
        {
            $database = new database();
            $database->executeNonQuery("Update `questions` set `question` = '".addslashes($POST['txtFieldLabel'])."', `typeId` = '".$POST['ddlFieldType']."', `order` = '1' where `questionId` = '".$POST['txtQuestionId']."'");
            $database->executeNonQuery("delete from `pageformquestionchoices` where `questionId` = '".$POST['txtQuestionId']."'");
            $i = 1;
            if($POST['ddlFieldType'] == 3)
            {
                foreach($POST['txtCheckbox'] as $txtCheckBox)
                {
                    $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '".$POST['txtQuestionId']."', `choiceText` = '".addslashes($txtCheckBox)."', `order` = '".$i."'");
                    $i++;
                }
            } //checkbox
            elseif($POST['ddlFieldType'] == 4)
            {
                foreach($POST['txtRadioBtn'] as $txtRadioBtn)
                {
                    $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '".$POST['txtQuestionId']."', `choiceText` = '".addslashes($txtRadioBtn)."', `order` = '".$i."'");
                    $i++;
                }
            }
            //radiobutton
            $database->executeNonQuery("Update `leaderquestions` set `mandatory` = '".$_POST['rdMandatory']."' where `questionId` = '".$POST['txtQuestionId']."'");
        }

        function getLeaderCustomQuestions($userId, $status = 0)
        {
            $database = new database();
            $where;
            if($status == '1')
                $where = "and `leaderquestions`.`status` = '".$status."'";

            return $database->executeObjectList("Select `questions`.`question`, `questions`.`questionId`, `questions`.`typeId`, `leaderquestions`.`status`,`leaderquestions`.`mandatory` FROM `leaderquestions` LEFT JOIN `questions` ON `leaderquestions`.`questionId` = `questions`.`questionId`  WHERE `leaderquestions`.`leaderId` = '".$userId."' ".$where." ORDER BY `questions`.`order`");
        }

        function getLeaderQuestionTypes()
        {
            $database = new database();
            return $database->executeNonQuery("Select * from `questiontype` where `isLeaderEnable` = '1'");
        }

        function getQuestionChoice($questionId)
        {
            $database = new database();
            return $database->executeObjectList("Select * from `pageformquestionchoices` where `questionId` = '".$questionId."'");
        }

        function getUpcomingTasks($userId)
        {
            $database = new database();
            return $database->executeObjectList("Select * from `tasksupcoming` where `status` = '1'");
        }

        function updateUpcomingTasks($taskId, $userId)
        {
            $database = new database();
            $database->executeNonQuery("update `user` set `upComingTasks` = '".$taskId."' where `userId` = '".$userId."'");
        }

        function getLeaderQuickLinks($userId, $status = 0)
        {
            $database = new database();
            $where;
            if($status)
                $where = " and `status` = '".$status."'";
            return $database->executeObjectList("Select * from `quicklinks` where `userId` = '".$userId."'".$where);
        }

        function toggleLeaderQuickLinks($id)
        {
            $database = new database();
            return $database->executeNonQuery("update `quicklinks` set `status` = IF(status=1, 0, 1) where `id` = '".$id."'");
        }

        function addLeaderQuickLinks($POST, $userId)
        {
            $database = new database();
            $database->executeNonQuery("Insert into `quicklinks` set `title` = '".$POST['txtTitle']."', `link` = '".$POST['txtLink']."', `userId` = '".$userId."'");
        }

        function updateLeaderQuickLinks($POST)
        {
            $database = new database();
            $database->executeNonQuery("Update `quicklinks` set `title` = '".$POST['txtEditTitle']."', `link` = '".$POST['txtEditLink']."' where `id` = '".$POST['txtEditId']."'");
        }

        function getLeaderQuestionDetails($questionId)
        {
            $database = new database();
            return $database->executeObjectList("Select `q`.`question`, `p`.`choiceId`, `p`.`choiceText`, `p`.`order` FROM `questions` `q` left join `pageformquestionchoices` `p` on `q`.`questionId` = `p`.`questionId` where `q`.`questionId` = '".$questionId."' order by `p`.`order`");
        }

        function updateDefaultFieldOrder($orders)
        {
            $database = new database();
            $where;
            $orderIds;
            $count = 0;
            $orders = str_replace(',,', ',', $orders);
            $orders = str_replace(',,', ',', $orders);
            $orders = str_replace('ldf', '', $orders);
            $orderExplode = explode(',', $orders);
            foreach($orderExplode as $oe)
            {
                if(!(empty($oe)))
                {
                    //echo $oe;
                    if(empty($orderIds))
                        $orderIds = $oe;
                    else
                        $orderIds .= ','.$oe;
                    $count++;
                    $where .= " when `id` = '".$oe."' then '".$count."'";
                }
            }
            $database->executeNonQuery("update `leaderdefaultfields` set `orderId` = case ".$where." end where `id` in (".$orderIds.")");
        }

        function updateCustomQuestionsOrder($questions)
        {
            $database = new database();
            $where;
            $questionIds;
            $count = 0;
            $questions = str_replace(',,', ',', $questions);
            $questions = str_replace(',,', ',', $questions);
            $questions = str_replace('lcq', '', $questions);
            $questionExplode = explode(',', $questions);
            foreach($questionExplode as $qe)
            {
                if(!(empty($qe)))
                {
                    if(empty($questionIds))
                        $questionIds = $qe;
                    else
                        $questionIds .= ','.$qe;
                    $count++;
                    $where .= " when `questionId` = '".$qe."' then '".$count."'";
                }
            }
            echo "update `questions` set `order` = case ".$where." end where `questionId` in (".$questionIds.")";
            $database->executeNonQuery("update `questions` set `order` = case ".$where." end where `questionId` in (".$questionIds.")");
        }
	    function updateProfileOrder($profiles)
	    {
		    $database = new database();
		    $where;
		    $profilesIds;
		    $count = 0;
		    $profiles = str_replace(',,', ',', $profiles);
		    $profiles = str_replace(',,', ',', $profiles);
		    $profiles = str_replace('lcq', '', $profiles);
		    $profilesExplode = explode(',', $profiles);
		    foreach($profilesExplode as $Pe)
		    {
			    if(!(empty($Pe)))
			    {
				    if(empty($profilesIds))
					    $questionIds = $Pe;
				    else
					    $profilesIds .= ','.$Pe;
				    $count++;
				    $where .= " when `questionId` = '".$Pe."' then '".$count."'";
			    }
		    }
		    echo "update `pages` set `order` = case ".$where." end where `pageId` in (".$profilesIds.")";
		   return $database->executeNonQuery("update `pages` set `order` = case ".$where." end where `pageId` in (".$profilesIds.")");
	    }
	    function RequestAllowsignup($database,$request,$userId){
          // echo  ("update `user` set `isAllowSignUp`='".$request."' where userId='".$userId."'");
		    return $database->executeNonQuery("update `user` set `isAllowSignUp`='".$request."' where userId='".$userId."'");
	    }
	    function SignupStatus($userId){
		    $database = new database();
		    return $database->executeScalar("select `isAllowSignUp` from `user` where `userId` = '".$userId."'");
	    }
        function getSignature($userId, $database) {
            $sql = "SELECT * FROM usersignature WHERE `userId` = '".$userId."'";
            return $database->executeObject($sql);
        } 
	 function getPermissionEmail($userId, $database) {
            $sql = "SELECT * FROM userpermissionemail WHERE `userId` = '".$userId."'";
            return $database->executeObject($sql);
        }

        function saveSignature($userId, $signature, $signatureId) {

            $database = new database();

            if ($signatureId) {
                $sql = "UPDATE usersignature SET `signature` = '".$signature."' WHERE `userSignatureId` = '".$signatureId."' LIMIT 1";
            } else {
                $sql = "INSERT INTO usersignature SET `signature` = '".$signature."', `userId` = '".$userId."'";
            }

            $database->executeNonQuery($sql);

            return array(
                'status' => 'success',
                'message' => 'Signature saved successfully'
            );
        }
      function savePermissionEmail($userId, $permissionEmail, $permissionEmailId,$permissionEmailSubject) {

            $database = new database();

            if ($permissionEmailId) {
                $sql = "UPDATE userpermissionemail SET `permissionEmailBody` = '".$permissionEmail."', `permissionEmailSubject`='".$permissionEmailSubject."' WHERE `permissionEmailId` = '".$permissionEmailId."' LIMIT 1";
            } else {
              
                $sql = "INSERT INTO userpermissionemail SET `permissionEmailBody` = '".$permissionEmail."',`permissionEmailSubject`='".$permissionEmailSubject."', `userId` = '".$userId."'";
            }

            $database->executeNonQuery($sql);

            return array(
                'status' => 'success',
                'message' => 'Permission Email saved successfully'
            );
        }

	}

?>