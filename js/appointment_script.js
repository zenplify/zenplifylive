// JavaScript Document
function timeDiff(time1, time2) {
	
    var td = time2 - time1;
	
        hours = parseInt(td / 3600);
		
        minutes = parseInt( (td - hours*3600) / 60 );
        diff = ( (hours < 10 && hours >= 0) ? ('0' + hours) : hours ) + ':' + ( (minutes < 10 && minutes >= 0) ? ('0' + minutes) : minutes );

    $('#mins').val(minutes);
	$('#hs').val(hours);
	
	
}
$(document).ready(function() {
  
  	 var datepicked = function() {
       
              var from = $('#startDate');
              var to = $('#endDate');
              var days = $('#duration');
                   
                   
                    var fromDate = from.datepicker('getDate')
     
                    var toDate = to.datepicker('getDate')
     
              if (toDate && fromDate) {
               var difference = 0;
                            var oneDay = 86400000; //ms per day
                            var difference = Math.ceil((toDate.getTime() - fromDate.getTime()) / oneDay); 
							$('select#duration_Unit').val('days');
							$('#duration').val(difference);
							
							
							
                }
     }
       
       
		$('#duration_Unit').change(function(){
			
				if($('select#duration_Unit').val()=='days')
			{
				
              	var to = $('#endDate').val();
             	var days = $('#duration').val();
				 var edate= new Date($('#endDate').datepicker('getDate').getTime() + parseInt(days*24*60*60*1000));
                 var curr_date = edate.getDate();
    			 var curr_month = edate.getMonth()+1; //Months are zero based
    			 var curr_year = edate.getFullYear();
    			 var edate='0'+curr_month + "/" + curr_date + "/" + curr_year;
				//alert(edate);
				//var mydate=dateFormat(edate,"m/dd/yyyy");;
     		$('#endDate').val(edate);
				
			}
			if($('select#duration_Unit').val()=='minutes')
			{
				var to = $('#endDate').val();
				var oldhours=to.split(':');
             	var minutes = $('#duration').val();
				if(minutes>=1440)
				{
				 	var tdate= new Date($('#endDate').datepicker('getDate').getTime() + parseInt(minutes*60*1000));
                 	var curr_date = tdate.getDate();
    			 	var curr_month = tdate.getMonth()+1; //Months are zero based
    			 	var curr_year = tdate.getFullYear();
    			 	var tdate='0'+curr_month + "/" + curr_date + "/" + curr_year;
					//alert(tdate);
					$('#endDate').val(tdate);
				}
				else
				{
					 if (minutes > 59) 
					 {
            			var hrs = parseInt(minutes / 60);
            			var mins = minutes - hrs * 60;
        			 } 
					else 
					{
            			 mins = minutes;
           				 hrs = oldhours[0];
        			}
					//alert(hrs+':'+mins); fine
					var time2 =$('#endTime').val().substring(0,5);
					var  time2 =time2.split(':');
					if(hrs>= 1 && hrs<=11)
					{
						var hours=parseInt(time2[0])+hrs;
						var units='am';
					}
					else if(hrs>= 12 && hrs<=24)
					{
						var hours=parseInt(time2[0]);
						var units='pm';
					}
					if(hrs==0)
					{
						var hours=parseInt(time2[0]);
						units=$('#endTime').val().substring(6);
					}
					
					var mints=parseInt(time2[1])+parseInt(mins);
					
					if(hours<10)
					hours='0'+hours;
					if(mints<10)
					mints='0'+mints;
					var newtime=hours+':'+mints+' '+units;
					//alert(newtime);
					//alert(newtime);
					$('#endTime').val(newtime);
				}
				// alert(etime);
				 
				 
			}
			if($('select#duration_Unit').val()=='hours')
			{
				var to = $('#endDate').val();
             	var hours = $('#duration').val();
				if (hours!=0)
				{
						if(hours>=24)
						{
							var tdate= new Date($('#endDate').datepicker('getDate').getTime() + parseInt(hours*60*60*1000));
							var curr_date = tdate.getDate();
							var curr_month = tdate.getMonth()+1; //Months are zero based
							var curr_year = tdate.getFullYear();
							var tdate='0'+curr_month + "/" + curr_date + "/" + curr_year;
							//alert(tdate);
							$('#endDate').val(tdate);
						}
						else
						{
							 
							//alert(hrs+':'+mins); fine
							//alert(hours);
							var time2 =$('#endTime').val().substring(0,5);
							var  time2 =time2.split(':');
							var units='';
							var hrs=parseInt(time2[0])+parseInt(hours);
							var mints=parseInt(time2[1]);
							//alert(hrs);
							if(hrs>24)
							{
								hrs=hrs-24;
								units=$('#endTime').val().substring(6);
								
							}
							else if(hrs==24)
							{
								hrs=parseInt(time2[0]);
								units=$('#endTime').val().substring(6);
							}
							
							
							else if(hrs > 12)
							{
								//alert(hrs);
								hrs=hrs-12;
								
								units=$('#startTime').val().substring(6);
								if(units=='am')
								units='pm'
								else
								units='am';
							}
							else if(hrs==12)
							{
								hrs=hrs;
								units=$('#endTime').val().substring(6);
								
							}
							
							
							if(hrs<10)
							hrs='0'+hrs;
							if(mints<10)
							mints='0'+mints;
							var newtime=hrs+':'+mints+' '+units;
							//alert(newtime);
							//alert(newtime);
							$('#endTime').val(newtime);
						}
						// alert(etime);
				}
				
			}
			
			
			});
		$('#duration').blur(function (){
			
			if($('select#duration_Unit').val()=='days')
			{
				
              	var to = $('#endDate').val();
             	var days = $('#duration').val();
				 var edate= new Date($('#endDate').datepicker('getDate').getTime() + days*24*60*60*1000);
                 var curr_date = edate.getDate();
    			 var curr_month = edate.getMonth()+1; //Months are zero based
    			 var curr_year = edate.getFullYear();
    			 var edate='0'+curr_month + "/" + curr_date + "/" + curr_year;
				//alert(edate);
				//var mydate=dateFormat(edate,"m/dd/yyyy");;
     		$('#endDate').val(edate);
				
			}
			if($('select#duration_Unit').val()=='minutes')
			{
				var to = $('#endDate').val();
             	var minutes = $('#duration').val();
				var oldhours=to.split(':');
				if(minutes>=1440)
				{
				 	var tdate= new Date($('#endDate').datepicker('getDate').getTime() + parseInt(minutes*60*1000));
                 	var curr_date = tdate.getDate();
    			 	var curr_month = tdate.getMonth()+1; //Months are zero based
    			 	var curr_year = tdate.getFullYear();
    			 	var tdate='0'+curr_month + "/" + curr_date + "/" + curr_year;
					//alert(tdate);
					$('#endDate').val(tdate);
				}
				else
				{
					 if (minutes > 59) 
					 {
            			var hrs = parseInt(minutes / 60);
            			var mins = minutes - hrs * 60;
        			 } 
					else 
					{
            			 mins = minutes;
           				 hrs = oldhours[0];
        			}
					//alert(hrs+':'+mins); fine
					var time2 =$('#endTime').val().substring(0,5);
					var  time2 =time2.split(':');
					if(hrs>= 1 && hrs<=11)
					{
						var hours=parseInt(time2[0])+hrs;
						var units='am';
					}
					else if(hrs>= 12 && hrs<=24)
					{
						var hours=parseInt(time2[0]);
						var units='pm';
					}
					if(hrs==0)
					{
						var hours=parseInt(time2[0]);
						units=$('#endTime').val().substring(6);
					}
					
					var mints=parseInt(time2[1])+parseInt(mins);
					
					if(hours<10)
					hours='0'+hours;
					if(mints<10)
					mints='0'+mints;
					var newtime=hours+':'+mints+' '+units;
					//alert(newtime);
					//alert(newtime);
					$('#endTime').val(newtime);
				}
				// alert(etime);
				 
				 
			}
			if($('select#duration_Unit').val()=='hours')
			{
				var to = $('#endDate').val();
             	var hours = $('#duration').val();
				if(hours!=0)
				{
						if(hours>=24)
						{
							var tdate= new Date($('#endDate').datepicker('getDate').getTime() + parseInt(hours*60*60*1000));
							var curr_date = tdate.getDate();
							var curr_month = tdate.getMonth()+1; //Months are zero based
							var curr_year = tdate.getFullYear();
							var tdate='0'+curr_month + "/" + curr_date + "/" + curr_year;
							//alert(tdate);
							$('#endDate').val(tdate);
						}
						else
						{
							 
							//alert(hrs+':'+mins); fine
							var time2 =$('#endTime').val().substring(0,5);
							var  time2 =time2.split(':');
							var units='';
							var hrs=parseInt(time2[0])+hours;
							if(hrs>24)
							{
								hrs=hrs-24;
								units=$('#endTime').val().substring(6);
								
							}
							else if(hrs==24)
							{
								hrs=parseInt(time2[0]);
								units=$('#endTime').val().substring(6);
							}
							
							
							else if(hrs> 12)
							{
								hrs=hrs-12;
								units=$('#endTime').val().substring(6);
								if(units=='am')
								units='pm'
								else
								units='am';
							}
							else if(hrs==12)
							{
								hrs=hrs;
								units=$('#endTime').val().substring(6);
								
							}
							
							
							var mints=parseInt(time2[1]);
							var newtime=hrs+':'+mints+' '+units;
							//alert(newtime);
							//alert(newtime);
							$('#endTime').val(newtime);
						}
						// alert(etime);
						 
				}
			}
		});
		
 		
  
    
});