<?php
	error_reporting(0);
	require_once("class.phpmailer.php");
	require_once("class.smtp.php");
	include_once("encryption_class.php");

	class smtpEmail extends PHPMailer
	{

		public function sendMail($database, $userEmail, $contactEmail, $firstName, $lastName, $body, $subject)
		{
			$detailDomain = $database->executeObject("Select email, mail_out_host, mail_out_port, enc_pass, mail_inc_ssl, mail_out_ssl from webmail_awm_accounts where email='".$userEmail."'");
			$encryption = new encryption_class();
			$mail = new PHPMailer();
			$mail->IsHTML(true);
			$mail->SMTPAuth = true;
			$mail->SMTPDebug = 0;
			//$enc_pass=$detailDomain->enc_pass;
			$key = 'abc*def';
			$decrypt_pass = $encryption->decrypt($key, $detailDomain->enc_pass);
			$mail->Username = "$detailDomain->email"; // your SMTP username
			$mail->Password = "$decrypt_pass"; // your SMTP password
			if($detailDomain->mail_out_ssl == 1)
			{
				$mail->Host = "ssl://"."$detailDomain->mail_out_host";
				$mail->IsSMTP();
			}
			else
			{
				$mail->Host = "$detailDomain->mail_out_host";
			}
			$mail->Port = $detailDomain->mail_out_port;
			$to = "$contactEmail";
			$from = "From:".$firstName." ".$lastName." <".$detailDomain->email.">\r\n";
			$body = "$body";
			$Subject = "$subject";
			$mail->From = $from;
			//$mail->AddReplyTo($detailDomain->email, $firstName);
			$mail->AddAddress($to, "$contactEmail");
			$mail->SetFrom($detailDomain->email, $firstName." ".$lastName);
			$mail->AddReplyTo($detailDomain->email, $firstName." ".$lastName);
			$mail->Subject = $Subject;
			$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			$mail->MsgHTML($body);
			if((!$mail->Send()) || (!$detailDomain) || (empty($detailDomain)))
			{
				//                echo "Mailer Error: " . $mail->ErrorInfo;
				//	            $headers = "From: ".$firstName." ".$lastName." <".$userEmail."> \r\n"."Reply-To: ".$userEmail."\r\n";
				//	            $headers .= "Content-Type: text/html";
				$headers = "From: ".$firstName." ".$lastName." <support@suavesol.net>\r\n"."Reply-To: ".$userEmail."\r\n";
				$headers .= "Content-Type: text/html";
				//	            $headers = "From: ".$firstName." ".$lastName . "<".$userEmail.">"."\r\n" .
				//		            "Reply-To: ".$userEmail."\r\n" ."Content-Type: text/html"."\r\n" .
				//		            "X-Mailer: PHP/" . phpversion();
				//	            $headers = 'From: '.$userEmail. "\r\n" .
				//		            'Reply-To: '.$userEmail. "\r\n" .'Content-Type: text/html'."\r\n" .
				//		            'X-Mailer: PHP/' . phpversion();
				//	            $headers  = 'MIME-Version: 1.0' . "\r\n";
				//	            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				//	            $headers .= 'To: '.$contactEmail . "\r\n";
				//	            $headers .= 'From: '.$firstName.' '.$lastName.' <faridahmed65@hotmail.com>' . "\r\n";
				mail($contactEmail, $subject, $body, $headers, "-f ".$userEmail);
				//mail($contactEmail, $subject, $body, $headers);
				//echo 'Message was not sent;
				//echo 'Mailer error:' . $mail->ErrorInfo;
			}
		}

		public function testMail()
		{
			$mail = new PHPMailer();
			//$body             = file_get_contents('contents.html');
			//$body             = eregi_replace("[\]",'',$body);
			$body = 'Hi.... got email?';
			//$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host = "smtp.live.com"; // SMTP server
			$mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
			// 1 = errors and messages
			// 2 = messages only
			$mail->SMTPAuth = true; // enable SMTP authentication
			$mail->Host = "smtp.live.com"; // sets the SMTP server
			$mail->Port = 587; // set the SMTP port for the GMAIL server
			$mail->Username = ""; // SMTP account username
			$mail->Password = " "; // SMTP account password
			$mail->SetFrom('', 'First Last');
			$mail->AddReplyTo("", "First Last");
			$mail->Subject = "PHPMailer Test Subject via smtp, basic with authentication";
			$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			$mail->MsgHTML($body);
			$address = "";
			$mail->AddAddress($address, "To Address");
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
			if(!$mail->Send())
			{
				echo "Mailer Error: ".$mail->ErrorInfo;
			}
			else
			{
				echo "Message sent!";
			}
		}
	}