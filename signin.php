<?php 
session_start();
error_reporting(0);
$_SESSION['groupsArray']='';
require_once("classes/register.php");
require_once ("classes/payment.php");
$register=new register();
$payment=new payment();

//echo "hello";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Zenplify</title>
 <link rel="stylesheet" href="css/style.css" type="text/css">
 <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
  
  <!-- validation file included  -->
		<script src="validation/lib/jquery.js" type="text/javascript"></script>
		<script type="text/javascript" src="validation/jquery.validate.js"></script>
		
		
		<link rel="stylesheet" href="js/alert/css/jquery.toastmessage.css" type="text/css">	      
 <script src="js/alert/jquery.toastmessage.js" type="text/javascript"></script> 
	
<script>
$().ready(function() {
	
	//validate signin form 

	$("#login_user").validate(); 
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
	
	 
$('#username').focus(function() { if($(this).val()=='Username')$(this).val('');});
	$('#username').blur(function() { if($(this).val()=='')$(this).val('Username');});
	//var tbname = $('#username').val();
	//var tbpass = $('#password').val();
	//$('#password').focus(function() { $(this).replaceWith('<input type="password" name="password" id="password" class="textfield required"   />');});
	//$('#password').blur(function() { $(this).val(tbpass);});
	
	$('#password').focus(function() { if($(this).val()=='Password')$(this).val('');});
	$('#password').blur(function() { if($(this).val()=='')$(this).val('Password');});
	var tbemail = $('#username_email').val();
	$('#username_email').focus(function() {if($(this).val()=='Username/Email Address') $(this).val('');});
	$('#username_email').blur(function() {if($(this).val()=='') $(this).val('Username/Email Address');});
	
	$('#forgotpass').click(function(){
		$('#login_container').slideUp('slow');
		$('#forgotpass_container').slideDown('slow');
		
		});
	$('#cancel').click(function(){
		
		$('#login_container').slideDown('slow');
		$('#forgotpass_container').slideUp('slow');
		
		});	
  });
</script>

</head>
<body>
<?php 


if ($_GET["paypal"] == 'success')
{
	$mes= 'Your PayPal Verification has been made.Now you can Login here.';
}

if ($_GET["paypal"] == 'failure')
{
	$message= 'Your PayPal Transaction has been cancelled. First verify your Paypal payment. ';
}
if ($_GET["login"] == 'failure')
{
	$message= 'Invalid Username or Password.Please try again.';
}
if ($_GET["login"] == 'notverified')
{
	$message= 'Please verify your Email Address first and then try to login';
}

if ($_GET["logout"] == 'success')
{
	$message= 'Logout Successfull!';
}
if ($_GET["pass"] == 'resetpassword')
{
	$mes= 'Your password has been changed successfully.';
}
if ($_GET["signup"] == 'signup')
{
	$mes= 'You  have been registered  successfully.';
}
if($_GET["resetpassword"]=='reset'){
	$mes= 'An Email has been sent to you on provided Email Address to reset your Password.';
	}


if(isset($_POST['submitEmailUser'])){

$userName_email = $_POST['reset_userName_Email'];
$confirmationCode=generateUniqueID();
$resetPaswordRequestDate=date("Y-m-d");

 

if(!empty($userName_email))
{
		
	$userRecord=$register->selectForgetUsernameOrEmail($userName_email);
	$userId=$userRecord->userId;
	$userEmail=$userRecord->email;
	$firstName=$userRecord->firstName;
	
}


	

	
if($userId!=""){
	$pin=$register->setResetVerificationCode($userId,$confirmationCode,$resetPaswordRequestDate);
	
	if(!empty($pin)){


	$to = $userEmail;
	$subject = "Zenplify Forgot Password";
	$message = "\nHi, ".$firstName."\n\nThis email was sent to you by the Zenplify server because someone requested a password reset for your account.\n\nTo reset your password and access your account, follow these steps: \n\n
	1. Click on the link below. If nothing happens when you click on the link, copy and paste the link into the address bar of your web browser.\n\n"."http://zenplify.biz/modules/loginProcess/password_check.php?code=".$pin."\n\n
	2. Enter your new password twice on the page and click Reset Password.\n\n
	After this, you will be logged into the system with your new password.\n\n
	Sincerely, \n\nZenplify Team";
	$from = "<support@zenplify.biz>";
	$headers = "From:" . $from;


	$sent = mail($to, $subject, $message, $headers) ;


?>
<script>
window.location.href="signup.php?resetpassword=reset";
</script>
<?php 

	}
}

else{
	
}
	$mes= 'This User is not registered for Zenplify.';

}



if(isset($_POST["submit"]))
	{
			$username=strtolower($_POST['username']);
			$admin=$register->getUserForLogin($_POST);
		 	
			if ($admin == "")
			{
				SendRedirect("signin.php?login=failure");	
				exit;
			}
			if($admin->userName==$username && $admin->password==$_POST['password'] )	
			{	
				 if($admin->isVerifiedEmail==1)
				 {
					 $_SESSION['AdminName'] = $admin->userName;
					  $_SESSION['AdminFname'] = $admin->firstName;
					 $_SESSION['userId'] = $admin->userId;
					
				
					$paymentPlanInformation=$payment->trialPeriod($_SESSION['userId']);//get payment Trial date for checking trial period
				
					//if(strtotime($paymentPlanInformation->expiryDate)<=strtotime(date('Y-m-d'))){
						// SendRedirect("modules/loginProcess/expire.php");
					//	}else{
						//SendRedirect("modules/loginProcess/trial.php");
						//}
						if($admin->isVerifiedPaypal==0){
							SendRedirect("modules/loginProcess/paymentplans.php?user_id=".$admin->userId);
							}else{
								
							if(strtotime($paymentPlanInformation->expiryDate)<=strtotime(date('Y-m-d'))){
								SendRedirect("modules/loginProcess/paymentplans.php?user_id=".$admin->userId);
								}else{
								SendRedirect("modules/loginProcess/home.php");
								}
				
							}
				 }
				 else
				 {
					 SendRedirect("signin.php?login=notverified");	
					 exit;
					 
				}
			
		}
		
	
	}
	?>

<div align="center" id="index_logo"><img src="images/logo.png" />
 <div id="login_container">
 <div style="margin-top:10px;"><?php //=($mes==""?"":$mes);
	if(!empty($mes))
{
	if($mes=='This User is not registered for Zenplify.')
	echo '<font class="message" >'.$mes;
	else
	echo '<font class="messageofsuccess"  >'.$mes;
}

	?></font></div>
 <div><font class="message" ><?php echo ($message==""?"":$message);?></font></div>
  <form name="login_user" id="login_user" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
      <table width="400px" style="margin-left:170px; padding:0;"  cellspacing="0" cellpadding="0" border="0">
          <tr>
          <td  >&nbsp;</td>
          </tr>
          
          <tr>
          	
          	<td ><input type="text" name="username" id="username" class="textfield required"   value="Username" style="padding-left:2px;" /></td>
            
          </tr>
          <tr>
          	
          	<td ><input type="password" name="password" id="password" class="textfield required"  value="Password" style="padding-left:2px;"/></td>
            
          </tr>
          <tr >
          	
          	<td ><a href="javascript:" id="forgotpass" style=" color:#555555;font-weight:bold; text-decoration:none; float:left; margin-left:112px; margin-bottom:10px; font-size:14px;">Forgot Password?</a></td>
            
          </tr>
        
     <tr>
     
     <td>
         
        <input type="submit" name="submit" id="submit_button" class="signin" value=""/>
         <a href="modules/loginProcess/signup.php"><input type="button" id="submit_button" value="" class="signup"  /></a>
     </td>
     
    </tr>
    
      </table>
      
      
      </form>
</div>
<div id="forgotpass_container" style="display:none;">

  <form name="resetPasswordForm" action="" method="post" id="resetPasswordForm">
      <table style="margin:0 auto; padding:0;"  cellspacing="0" cellpadding="0">
           <tr>
           
          <td ><h1 class="reghead" >Forgot Password</h1></td>
          </tr>
          <tr>
          	<td ><input type="text" name="reset_userName_Email" id="username_email" class="textfield" value="Username/Email Address"  /></td>
          </tr>
       <tr>
     
     
    <td><input type="submit" name="submitEmailUser" id="submit" class="submit" value=""/><input type="button" id="cancel" name="cancel"  class="cancel" value="" /></td>
     
    
    </tr>
      </table>
      </form>
</div>
</div>


</body>
</html>
