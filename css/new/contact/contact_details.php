<?php
    error_reporting(0);
    session_start();

    class contactDetails
    {

        public function GetContactPermission($database, $contactId)
        {
            $detail = $database->executeScalar("select cprs.title
												from contactpermissionrequests cpr
												left join contactpermissionrequeststatus cprs on cpr.statusId=cprs.statusId 
												where cpr.toContactId='" . $contactId . "' order by permissionRequestId desc LIMIT 1"
            );
            return $detail;
        }

        public function showContactDetail($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");

        }

        public function showContactPhoneNumber($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='" . $contactId . "'");

        }

        public function showContactEmailAddress($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM emailcontacts WHERE contactId='" . $contactId . "'");

        }

        public function showContactAddress($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactaddresses WHERE contactId='" . $contactId . "'");

        }

        public function GetEmail($database, $user_id)
        {
            $email = $database->executeScalar("SELECT email FROM user WHERE userId='" . $user_id . "'");
            return $email;
        }

        public function showContactTimeForCall($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactcalltime WHERE contactId='" . $contactId . "'");

        }

        public function showStickyNotes($database, $contactId)
        {
            return $database->executeObject("SELECT notes, noteId FROM contactnotes WHERE contactId='" . $contactId . "' AND isActive=1 ORDER BY noteId DESC");
        }

        public function upcomingActivities($database, $userId, $contactId)
        {
            return $database->executeObjectList("SELECT * FROM tasks  WHERE userId='" . $userId . "' AND taskId IN (select taskId FROM taskcontacts WHERE contactId='" . $contactId . "')  AND isCompleted!=1 AND `isArchive` = '0' order by dueDateTime ASC");
        }

        public function ActivitiesHistory($database, $userId, $contactId)
        {
            return $database->executeObjectList("SELECT * FROM tasks  WHERE userId='" . $userId . "' AND taskId IN (select taskId FROM taskcontacts WHERE contactId='" . $contactId . "')  AND isCompleted=1 AND `isArchive` = '0'  order by completedDate DESC ");

        }

        public function EmailHistory($database, $userId, $contactId)
        {
            return $database->executeObjectList("SELECT ceh.id, ceh.stepId, ceh.planId, ceh.contactId, ceh.userId, ceh.`subject`,ps.`summary`, ceh.`dateTime` FROM contactemailhistory ceh JOIN plansteps ps ON ceh.`stepId`= ps.`stepId`  WHERE
ceh.userId='".$userId."' AND ceh.contactId='".$contactId."' AND ceh.`isArchive` = '0' ORDER BY ceh.`dateTime` DESC");

        }

        public function getEmailHistory($database, $id)
        {
            return $database->executeObjectList("SELECT * FROM contactemailhistory  WHERE id='" . $id . "' AND `isArchive` = '0' order by `dateTime` DESC");

        }

        public function contactNotes($database, $contactId)
        {
            return $database->executeObjectList("SELECT noteId, cn.contactId, cn.notes, cn.addedDate, cn.lastModifiedDate, cn.isActive, CONCAT(firstName,' ',lastName) as name FROM contactnotes cn join contact c on cn.contactId=c.contactId WHERE cn.contactId='" . $contactId . "' AND cn.isActive=0");
        }

        public function getSignatures($database, $userId)
        {
            $details = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress , u.consultantId, u.title, u.leaderId,u.userId from user u where u.userId='" . $userId . "'");

            if ($details->leaderId == 0)
            {
                $consultantTitleUserId = $details->userId;
            }
            else
            {
                $consultantTitleUserId = $details->leaderId;
            }
            $consultantTitle = $database->executeScalar("SELECT `fieldCustomName` FROM `leaderdefaultfields` WHERE userId='" . $consultantTitleUserId . "' AND `fieldMappingName`='" . $fieldMappingName . "' AND `status`='1'");
            if (empty($consultantTitle) || ($details->userId == '3736' || $details->leaderId == '3736'))
            {
				// Exclude Juice Plus users
				if (($details->userId == '236' || $details->leaderId == '236') ||($details->userId == '3736' || $details->leaderId == '3736')){
					$arbonLogo = '<img src="https://zenplify.biz/images/GreenLogo1x1.png">';
				} else {
					$arbonLogo = '';
				}

                $firstName = $details->firstName;
            }
            $signature = '<span style="font-size:14px;">Best Regards,</span><br /><br /><span style="margin-left:5px;marfont-size:14px;">' . $firstName . '</span><br />
			<div><div style="float:left;">' . $arbonLogo . '</div><div style=" margin-left:5px;float:left;font-size:14px;">' . $details->firstName . ' ' . $details->lastName;

            if ($details->title != '')
                $signature = $signature . ', ' . $details->title;
            $signature = $signature . '<br /><span style="margin-left:-2px;color: #2E6A30;font-size:14px;">Arbonne Consultant ' . $details->consultantId . '</span><br /><span style="color: #2E6A30;font-size:14px;"><a style="color: #2E6A30;font-size:14px;" href="' . $details->webAddress . '" target="_blank">' . $details->webAddress . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $details->facebookAddress . '" target="_blank">' . $details->facebookAddress . '</a></div></div>';
            return $signature;
        }

        public function getContactDetails($database, $contactId, $userId)
        {
            return $database->executeObject("SELECT c.contactId, c.firstName, c.lastName FROM contact c, contactemailhistory ceh WHERE c.userId='" . $userId . "'  and  c.contactId='" . $contactId . "' and c.contactId=ceh.contactId ");

        }

        public function getcontactId2($database, $taskId, $userId)
        {
            return $database->executeObjectList("SELECT c.contactId, c.firstName, c.lastName FROM contact c, taskcontacts tc WHERE taskId='" . $taskId . "' and c.contactId=tc.contactId and c.userId='" . $userId . "' and c.isActive=1 ");

        }

        public function getcontactId3($database, $appId, $userId)
        {
            return $database->executeObjectList("SELECT c.contactId, c.firstName, c.lastName FROM contact c, appiontmentcontacts ac WHERE appiontmentId='" . $appId . "' and c.contactId=ac.contactId and c.userId='" . $userId . "' and c.isActive=1 ");

        }

        public function showPlanStepsInContactDetail($database, $contactId, $userId)
        {
            /*return $database->executeObjectList("SELECT t.taskId,p2.summary,p2.planId,p2.stepId,p3.title, t.isActive,p1.contactId, cpc.contactId as cpcContactId FROM plan p3
            left join plansteps p2 on p3.planId=p2.planId
            left join planstepusers p1 on p2.stepId=p1.stepId
                    left join contactPlansCompleted cpc on p3.planId=cpc.planId and p2.stepId = cpc.stepId and cpc.contactId ='".$contactId."'
                    left join contacttaskskipped cts on p2.planId=cts.planId and p2.stepId=cts.stepId and cts.contactId='".$contactId."'
                    left join (
    SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
    inner join tasks t on tc.taskId = t.taskId
    inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
    where tc.contactId = '".$contactId."' and t.isCompleted=0
    ) as t on p2.stepId = t.stepId and p2.planId = t.planId
                    where (p1.contactId ='".$contactId."' or cpc.contactId = '".$contactId."') AND p3.isActive=1 group by p2.planId order by p3.planId");*/
            return $database->executeObjectList("SELECT distinct(t.taskId),p1.`stepEndDate`,cts.skippId, cts.title as skipTitle,cts.contactId as skipContactId ,p2.summary,p2.planId ,p2.stepId,p3.title, t.isActive,p1.contactId,cpc.contactId as cpcContactId,p3.isDoubleOptIn,p2.isTask FROM ( select * from plan where userId = '" . $userId . "' and isActive=1 )
 p3 
		left join plansteps p2 on p3.planId=p2.planId 
		left join planstepusers p1 on p2.stepId=p1.stepId and p1.contactId='" . $contactId . "'
                
                left join contactPlansCompleted cpc on p3.planId=cpc.planId and p2.stepId = cpc.stepId and cpc.contactId ='" . $contactId . "'
                left join contacttaskskipped cts on p3.planId=cts.planId and p2.stepId=cts.stepId and cts.contactId='" . $contactId . "'
				
                left join (
SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
inner join tasks t on tc.taskId = t.taskId
inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
where tc.contactId = '" . $contactId . "' and t.isCompleted=0
) as t on p2.stepId = t.stepId and p2.planId = t.planId
                where (p1.contactId ='" . $contactId . "' or cpc.contactId = '" . $contactId . "' or cts.contactId='" . $contactId . "' ) AND p3.isActive=1 order by p3.planId, p2.order_no asc");

        }

        public function CheckLastStepOfPlan($database, $planId, $contactId)
        {
            return $database->executeObject("select * from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");

        }

        public function getPlanType($database, $planId)
        {
            return $database->executeScalar("select isDoubleOptIn from plan where planId='" . $planId . "'");

        }

        public function getContactPermissionStatus($database, $contactId)
        {
            return $database->executeScalar("select statusId from contactpermissionrequests where toContactId='" . $contactId . "'");

        }

        public function getPlanStepType($database, $stepId)
        {
            return $database->executeScalar("select istask from plansteps where stepId='" . $stepId . "'");
        }

        public function showContactGroups($database, $contactId)
        {
            return $database->executeObjectList("SELECT * FROM groups  WHERE groupId IN(SELECT groupId FROM groupcontacts WHERE groupId not in(56,82,61,76,77,78,79,80,81) and contactId='" . $contactId . "')");
        }

        public function isGroupLinked($database, $groupId, $userId)
        {
            $result = $database->executeObjectList("SELECT planId,title FROM plan WHERE groupId='" . $groupId . "' AND userId='" . $userId . "' AND isActive=1");
            return $result;
        }

        public function upcomingActivitiesAppiontments($database, $userId, $contactId)
        {
            return $database->executeObjectList("SELECT * FROM appointments  WHERE userId='" . $userId . "' AND appiontmentId IN (select appiontmentId FROM appiontmentcontacts WHERE contactId='" . $contactId . "')    AND endDateTime >= CURDATE() ");

        }

        public function editContactDetail($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");

        }

        public function editContactAddress($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactaddresses WHERE contactId='" . $contactId . "'");

        }

        public function editContactCallTime($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactcalltime WHERE contactId='" . $contactId . "'");
        }

        public function editContactGroup($database, $groupId, $contactId)
        {
            $result = $database->executeObject("SELECT * FROM groupcontacts WHERE contactId='" . $contactId . "' AND groupId='" . $groupId . "'");
            return (!empty($result) ? 1 : 0);
        }

        public function editContactPhone($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='" . $contactId . "'");

        }

        public function editContactEmail($database, $contactId)
        {
            return $database->executeObject("SELECT * FROM emailcontacts WHERE contactId='" . $contactId . "'");

        }

        public function showPlans($database, $userId)
        {
            return $database->executeObjectList("SELECT * FROM plan WHERE userId='" . $userId . "' AND isActive=1 and isArchive =0 ");
        }

        public function showPlanStepsIdsInContactDetail($database, $contactId)
        {
            $sql = "SELECT p1.stepId,p3.planId FROM plan p3
				left join plansteps p2 on p3.planId=p2.planId
				left join planstepusers p1 on p2.stepId=p1.stepId
                left join contactPlansCompleted cpc on p3.planId=cpc.planId and p2.stepId = cpc.stepId and cpc.contactId ='" . $contactId . "'
                left join (
					SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
					inner join tasks t on tc.taskId = t.taskId
					inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
					where tc.contactId = '" . $contactId . "'  and t.isCompleted=0
					)
					as t on p2.stepId = t.stepId and p2.planId = t.planId
                where (p1.contactId ='" . $contactId . "' or cpc.contactId = '" . $contactId . "') AND p3.isActive=1 group by p2.planId order by p3.planId";
            return $database->executeObjectList($sql);

        }

        public function showPlanSteps($database, $planId)
        {
            return $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $planId . "' and isactive = 1 and isArchive = 0 ORDER BY order_no ASC");
        }

        public function showGroups($database, $userId, $generatedVia)
        {
            $database = new database();
            $registrationDate = $database->executeScalar("select registrationDate from `user` where userId = '" . $userId . "'");
            if ($registrationDate > registrationDate)
            {
                return $database->executeObjectList("SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON  groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1' AND groupusers.`userId`='" . $userId . "' ORDER BY groups.`orderBy` ");
            }
            else
            {
                return $database->executeObjectList("SELECT '" . $userId . "',userId AS guser,`name`,privacy,leaderId,isactive,color,description,groupId FROM groups WHERE (userId='" . $userId . "' OR userId=0) AND groupId NOT IN(56,82,61,76,77,78,79,80,81) AND isactive = 1
UNION
SELECT groupusers.`userId`,groups.`userId` AS guser,groups.`name`,groups.`privacy`,groups.`leaderId`,groupusers.`isactive`,groups.`color`,groups.`description`,groups.`groupId` FROM `groups` JOIN `groupusers` ON groups.`groupId` = groupusers.`groupId` AND groupusers.`isactive`='1'AND groupusers.`isArchive`='0' AND groupusers.`userId`='" . $userId . "'");
//				return $database->executeObjectList("SELECT * FROM groups where (userId='" . $userId . "' OR userId=0) and groupId not in(56,82,61,76,77,78,79,80,81) ORDER BY orderBy ASC");
            }


//			if (($generatedVia == 2) or ($generatedVia == 3))
//			{
//				return $database->executeObjectList("SELECT g.* FROM `groupusers` gu   LEFT JOIN `groups` g ON g.groupId = gu.groupId WHERE gu.userId = '" . $userId . "' AND gu.`isactive` = 1");
//			}
//			else
//			{
//				return $database->executeObjectList("SELECT * FROM groups where userId='" . $userId . "' OR userId=0 and groupId not in(56,82,61,76,77,78,79,80,81) ORDER BY orderBy ASC");
//			}
        }

        public function taskContact($database, $contactId)
        {

            return $database->executeObject("SELECT * FROM contact WHERE contactId='" . $contactId . "'");
        }

        public function getContactProfiles($database, $contactId)
        {
//return $database->executeObjectList("SELECT pf.contactId,p.pageName AS pageId,p.`pageBrowserTitle`,p.generatedVia,p.pageId AS pagesId,p.newCode AS newCode  FROM `pageformreply` pf JOIN pages p ON p.pageId=pf.pageId WHERE contactId='" . $contactId . "'  group by pagesId order by pf.replyId desc");
            return $database->executeObjectList("SELECT pf.contactId,p.pageName AS pageId,p.`pageBrowserTitle`,p.generatedVia,p.pageId AS pagesId,p.newCode AS newCode  FROM `pageformreply` pf JOIN pages p ON p.pageId=pf.pageId WHERE contactId='" . $contactId . "' and pf.pageURL is not NULL  group by newCode order by pf.pageId ASC");
        }

        public function getProfileGeneratedVia($database, $contactId, $profileId)
        {
            return $database->executeScalar("SELECT p.`generatedVia`  FROM `pageformreply` pf JOIN pages p ON p.pageId=pf.pageId WHERE contactId='" . $contactId . "' AND p.`pageName`= '" . $profileId . "'");
        }

        function getProfileActive($database, $userId, $profileId, $contactId)
        {
            return $database->executeScalar("SELECT p.pageId AS pagesId  FROM `pageformreply` pf JOIN pages p ON p.pageId=pf.pageId WHERE contactId='" . $contactId . "' AND p.status='1' AND p.newCode='" . $profileId . "'  group by pagesId order by pf.replyId desc");
//
//			return $database->executeScalar("select pageId from pages where newCode = '" . $profileId . "' and userId = '" . $userId . "' and `status` = 1");
        }

        function  isEditedStep($database, $stepId)
        {

            return $database->executeScalar("SELECT oldId FROM `plansteps` WHERE stepId=(SELECT stepId FROM `plansteps`  WHERE stepId='" . $stepId . "' AND isactive='1' AND isArchive='0')");


        }

        function  getEditedstepSummry($database, $stepId)
        {

            return $database->executeScalar("SELECT summary FROM `plansteps` WHERE stepId='" . $stepId . "' AND isactive='0'");


        }

        function getCurrentSelectedPlanStep($database, $planId, $contactId)
        {
            return $database->executeObject("SELECT psu.`stepId`,ps.`daysAfter`,ps.`isTask`,psu.`stepEndDate` FROM planstepusers psu JOIN plansteps ps ON ps.`stepId` = psu.`stepId` JOIN plan p ON p.`planId` = ps.`planId` WHERE psu.contactId= '" . $contactId . "' AND p.planId = '" . $planId . "' AND ps.isactive = 1 AND ps.isArchive
            = 0");
        }
    }

?>