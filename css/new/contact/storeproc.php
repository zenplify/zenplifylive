<?php
require_once("../../classes/init.php");
require_once("../../classes/contact.php");
require_once("../../classes/profiles.php");
require_once("../../classes/contact_details.php");

$database = new database();
$contacts = new contact();
$profiles = new Profiles();
$contact_detail = new contactDetails();
header('Content-type: application/json');

$action = $_GET['action'];
switch ($action) {
    case 'view':
        contactdetails($database, $contact_detail, $profiles, $_GET['contactId'], $_GET['profileId'], $_GET['userId'], $contacts, $_GET['newCode'], $_GET['loadFirst']);
        break;
    case 'edit':
        editcontactdetails($database, $contact_detail, $profiles, $_GET['contactId'], $_GET['profileId'], $_GET['userId'], $contacts, $_GET['newCode'], $_GET['loadFirst']);
        break;
}
function contactdetails($database, $contact_detail, $profiles, $contactId, $profileId, $userId, $contacts, $newCode, $loadFirst = 0)
{
    $result = array();
    $questionAnswer = array();
    //$groups = array();
    $isActiveProfile = $contact_detail->getProfileActive($database, $userId, $newCode, $contactId);
    $isActiveProfile = true;
    if (!empty($isActiveProfile)) {
        $generatedVia = $contact_detail->getProfileGeneratedVia($database, $contactId, $profileId);
        $result["contactId"] = $contactId;
        $result["profileId"] = $profileId;
        $result["userId"] = $userId;
        $result["generatedVia"] = $generatedVia;
        $result["newCode"] = $newCode;
        $result['loadFirst'] = $loadFirst;
        $result['isLoadFirst'] = ($loadFirst) ? true : false;
        $isGroup = false;
        $isCompaigns = false;
        $isNotes = false;
        $isCurrentActivities = false;
        $isHistory = false;
        if ($profileId == "guestProfile") {
            $isGroup = true;
            $isCompaigns = true;
            $isNotes = true;
            $isCurrentActivities = true;
            $isHistory = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = true;
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $dateofbirth = $contactdetail->dateOfBirth;
            if ($dateofbirth == '0000-00-00' || $dateofbirth == NULL || $dateofbirth == '') {
                $dateOfBirth = "";
            } else {
                $dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
            }
            $contactdetail->dateOfBirth = $dateOfBirth;
            if ($contactdetail->dateOfBirth != '0000-00-00' && $contactdetail->dateOfBirth != NULL && $contactdetail->dateOfBirth != '') {
                $contactdetail->age = (getAge($contactdetail->dateOfBirth) < 0 ? '0' : getAge($contactdetail->dateOfBirth)) . ' Years';
            } else {
                $contactdetail->age = '';
            }
            $status = $contact_detail->GetContactPermission($database, $contactId);
            $contactdetail->Subscribed = ($status == "Subscribed" ? true : false);
            $contactdetail->Unsubscribed = ($status == "Unsubscribed" ? true : false);
            $contactdetail->Unprocessed = ($status == "Unprocessed" ? true : false);
            $contactdetail->Pending = ($status == "Pending" ? true : false);
            $contactdetail->Profiles = $contacts->getContactProfiles($contactId);
            $contactdetail->profiles = $contactdetail->profiles;
            $result["basicInfo"] = $contactdetail;
            //compaigns
            $stepcount = 0;
            $result['planQueryStart'] = getTimestamp();
            $showPlan = $contact_detail->showPlanStepsInContactDetail($database, $contactId, $userId);
            $result['planQueryEnd'] = getTimestamp();
            if (empty($showPlan)) {
                $compaign = "";
            } else {
                $newplanId = 0;
                $count = 0;
                $isContactUnsubscribed = $contact_detail->getContactPermissionStatus($database, $contactId);
                foreach ($showPlan as $sp) {
                    $count++;
                    $result['LastStepQueryStart'] = getTimestamp();
                    $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                    $result['LastStepQueryEnd'] = getTimestamp();
                    $contactPlansteps = array();
                    if ($newplanId != $sp->planId) {
                        if ($newplanId != 0) {
                            $compaign["step"] = $allsteps;
                            $compaigns[] = $compaign;
                        }
                        $flag = false;
                        $allsteps = array();
                        $compaign["title"] = $sp->title;
                        $newplanId = $sp->planId;
                    }
                    $isskipped = ($sp->skippId == NULL ? false : true);
                    $step["isSkipped"] = $isskipped;
                    $step["summary"] = $sp->summary;
                    $step["taskId"] = $sp->taskId;
                    $step["isTask"] = $sp->isTask;
                    $isblocked = (($stepId->stepId == $sp->stepId && $sp->isTask == 0 && $sp->isDoubleOptIn == 1 && $isContactUnsubscribed == 3) ? true : false);
                    $step["isBlocked"] = $isblocked;
                    if ($sp->isTask == '0' && !empty($sp->stepEndDate)) {
                        $nextExecutionDate = date("d-M-Y", strtotime($sp->stepEndDate));
                        $step["stepEndDate"] = $nextExecutionDate;
                    } else {
                        $step["stepEndDate"] = false;
                    }

                    $allsteps[] = $step;
                    if (!empty($stepId->stepId) && $flag == false) {
                        $compaign["Completed"] = true;
                        $flag = true;
                    } else if (empty($stepId->stepId)) {
                        $compaign["Completed"] = false;
                        $compaign["planIsDoubleOpt"] = $sp->isDoubleOptIn;
                        $isunsub = ($isContactUnsubscribed == 3 ? true : false);
                        $compaign["isContactUnsubscribed"] = $isunsub;
                    }
                }
                if ($count > 0) {
                    $compaign["step"] = $allsteps;
                    $compaigns[] = $compaign;
                }
            }
            $result["compaign"] = $compaigns;
            //groups
            $result['GroupQueryStart'] = getTimestamp();
            $contactgroups = $contact_detail->showContactGroups($database, $contactId, $generatedVia);

            $result['GroupQueryEnd'] = getTimestamp();
            if (!empty($contactgroups)) {
                $groups = array();
                $gc = array();
                foreach ($contactgroups as $cg) {
                    $linked = $contact_detail->isGroupLinked($database, $cg->groupId, $userId);
                    if (!empty($linked)) {
                        $groupflag = true;
                    } else {
                        $groupflag = false;
                    }
                    $group["groupId"] = $cg->groupId;
                    $group["name"] = $cg->name;
                    $group["color"] = $cg->color;
                    $group["groupflag"] = $groupflag;
                    if ($groupflag == true) {
                        foreach ($linked as $lg) {
                            $linktitle = ($count == 0 ? "" : ",") . $lg->title;
                            $count++;
                        }
                        $group["linked"] = $linktitle;
                    } else {
                        $group["linked"] = "false";
                    }
                    $groups[] = $group;
                }
            }
            $result["groups"] = $groups;
            $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
            if (!empty($stickyNotes))
                $result["stickyNotes"] = $stickyNotes;
            else
                $result["stickyNotes"] = '';
            /*$result['ReplyQueryStart']=getTimestamp();
                    $res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);


            $result['replyQueryEnd']=getTimestamp();
                    if(!empty($res['pageId']) && !empty($res['replyId']) )
                    {
                        $pageId=$res['pageId'];
                        $replyId=$res['replyId'];


            $result['QuestionsQueryStart']=getTimestamp();
                        $questions=$profiles->getPageQuestionAnswers($database,$pageId,$replyId);


            $result['questionsQueryEnd']=getTimestamp();

                    }

            $result["replyId"]=$replyId;*/
        } elseif ($profileId == "skinCareProfile") {
            $isCurrentActivities = true;
            $isHistory = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $condetail["profiles"] = $contactdetail->profiles;
            $condetail["Profiles"] = $contacts->getContactProfiles($contactId);
            $result["basicInfo"] = $condetail;
        } elseif ($profileId == "fitProfile") {
            $isCurrentActivities = true;
            $isHistory = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $condetail["profiles"] = $contactdetail->profiles;
            $condetail["Profiles"] = $contacts->getContactProfiles($contactId);
            $result["basicInfo"] = $condetail;
        } elseif ($profileId == "consultantProfile") {
            $isCurrentActivities = true;
            $isHistory = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $condetail["profiles"] = $contactdetail->profiles;
            $condetail["Profiles"] = $contacts->getContactProfiles($contactId);
            $result["basicInfo"] = $condetail;
        } else {
            $res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId, $newCode);
            $pageId = $res['pageId'];
            $basicInfoQuestions = $profiles->getPageQuestions($database, $pageId, 1, 1);

            $isGroup = true;
            $isCompaigns = true;
            $isNotes = true;
            $isCurrentActivities = true;
            $isHistory = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();


            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["address"] = false;
            $condetail["bestTimes"] = false;
            $condetail["isPhoneNumber"] = false;
            $condetail["isDateOfBirth"] = false;
            $condetail["isReferredBy"] = false;

            $condetail['pageId'] = $pageId;
            $condetail['contactId'] = $contactId;
            $condetail['userId'] = $userId;
            $condetail['title'] = $contactdetail->title;
            $condetail['addedDate'] = $contactdetail->addedDate;
            $condetail['gmt'] = $contactdetail->gmt;
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;

            foreach ($basicInfoQuestions as $biq) {
                $label = trim($biq->label);
                if (strpos($label, 'Best Times') !== false) {

                    $label = 'Best Times To Reach You';
                }
                switch ($label) {
//						case 'First Name':
//							$condetail["firstName"] = $contactdetail->firstName;
//							break;
//						case 'Last Name':
//							$condetail["lastName"] = $contactdetail->lastName;
//							break;
//						case 'Email':
//							$condetail["email"] = $contactdetail->email;
//							break;
                    case 'Phone number':
                        $condetail["isPhoneNumber"] = true;
                        $condetail["phoneNumber"] = $contactdetail->phoneNumber;
                        break;
                    case 'DOB':
                        $condetail["isDateOfBirth"] = true;
                        $dateofbirth = $contactdetail->dateOfBirth;
                        if ($dateofbirth == '0000-00-00' || $dateofbirth == NULL || $dateofbirth == '') {
                            $dateOfBirth = "";
                        } else {
                            $dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
                        }
                        $condetail['dateOfBirth'] = $dateOfBirth;
                        if ($dateOfBirth != '0000-00-00' && $dateOfBirth != NULL && $dateOfBirth != '') {
                            $condetail['age'] = (getAge($dateOfBirth) < 0 ? '0' : getAge($dateOfBirth)) . ' Years';
                        } else {
                            $condetail['age'] = '';
                        }
                        break;
                    case 'Referred By':
                        $condetail["isReferredBy"] = true;
                        $condetail["referredBy"] = $contactdetail->referredBy;
                        break;
                    case 'Street Address':
                        $condetail["street"] = $contactdetail->street;
                        break;
                    case 'City':
                        $condetail["city"] = $contactdetail->city;
                        break;
                    case 'State/Prov':
                        $condetail["state"] = $contactdetail->state;
                        break;
                    case 'Zip/Postal':
                        $condetail["zipcode"] = $contactdetail->zipcode;
                        break;
                    case 'Country':
                        $condetail["address"] = true;
                        $condetail["country"] = $contactdetail->country;
                        break;
                    case 'Best Times To Reach You':
                        $condetail["bestTimes"] = true;
                        $condetail["weekdays"] = $contactdetail->weekdays;
                        $condetail["weekdayEvening"] = $contactdetail->weekdayEvening;
                        $condetail["weekenddays"] = $contactdetail->weekenddays;
                        $condetail["weekenddayEvening"] = $contactdetail->weekenddayEvening;
                        break;
                }
            }

            $status = $contact_detail->GetContactPermission($database, $contactId);
            $condetail['Subscribed'] = ($status == "Subscribed" ? true : false);
            $condetail['Unsubscribed'] = ($status == "Unsubscribed" ? true : false);
            $condetail['Unprocessed'] = ($status == "Unprocessed" ? true : false);
            $condetail['Pending'] = ($status == "Pending" ? true : false);
            $condetail["profiles"] = $contactdetail->profiles;

            $condetail["Profiles"] = $contacts->getContactProfiles($contactId);

            $result["basicInfo"] = $condetail;

            //compaigns
            $stepcount = 0;
            $result['planQueryStart'] = getTimestamp();
            $showPlan = $contact_detail->showPlanStepsInContactDetail($database, $contactId, $userId);
            $result['planQueryEnd'] = getTimestamp();
            if (empty($showPlan)) {
                $compaign = "";
            } else {
                $newplanId = 0;
                $count = 0;
                $isContactUnsubscribed = $contact_detail->getContactPermissionStatus($database, $contactId);
                foreach ($showPlan as $sp) {
                    $count++;
                    $result['LastStepQueryStart'] = getTimestamp();
                    $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                    $result['LastStepQueryEnd'] = getTimestamp();
                    if ($newplanId != $sp->planId) {
                        if ($newplanId != 0) {
                            $compaign["step"] = $allsteps;
                            $compaigns[] = $compaign;
                        }
                        $flag = false;
                        $allsteps = array();
                        $compaign["title"] = $sp->title;
                        $newplanId = $sp->planId;
                    }
                    $isskipped = ($sp->skippId == NULL ? false : true);
                    $step["isSkipped"] = $isskipped;
                    $step["summary"] = $sp->summary;
                    $step["taskId"] = $sp->taskId;
                    $step["isTask"] = $sp->isTask;
                    $isblocked = (($stepId->stepId == $sp->stepId && $sp->isTask == 0 && $sp->isDoubleOptIn == 1 && $isContactUnsubscribed == 3) ? true : false);
                    $step["isBlocked"] = $isblocked;
                    if ($sp->isTask == '0' && !empty($sp->stepEndDate)) {
                        $nextExecutionDate = date("d-M-Y", strtotime($sp->stepEndDate));
                        $step["stepEndDate"] = $nextExecutionDate;
                    } else {
                        $step["stepEndDate"] = false;
                    }

                    $allsteps[] = $step;
                    if (!empty($stepId->stepId) && $flag == false) {
                        $compaign["Completed"] = true;
                        $flag = true;
                    } else if (empty($stepId->stepId)) {
                        $compaign["Completed"] = false;
                        $compaign["planIsDoubleOpt"] = $sp->isDoubleOptIn;
                        $isunsub = ($isContactUnsubscribed == 3 ? true : false);
                        $compaign["isContactUnsubscribed"] = $isunsub;
                    }
                }
                if ($count > 0) {
                    $compaign["step"] = $allsteps;
                    $compaigns[] = $compaign;
                }
            }
            $result["compaign"] = $compaigns;
            //groups
            $result['GroupQueryStart'] = getTimestamp();
            $contactgroups = $contact_detail->showContactGroups($database, $contactId, $generatedVia);

            $result['GroupQueryEnd'] = getTimestamp();
            if (!empty($contactgroups)) {
                $groups = array();
                $gc = array();
                foreach ($contactgroups as $cg) {
                    $linked = $contact_detail->isGroupLinked($database, $cg->groupId, $userId);
                    if (!empty($linked)) {
                        $groupflag = true;
                    } else {
                        $groupflag = false;
                    }
                    $group["groupId"] = $cg->groupId;
                    $group["name"] = $cg->name;
                    $group["color"] = $cg->color;
                    $group["groupflag"] = $groupflag;
                    if ($groupflag == true) {
                        foreach ($linked as $lg) {
                            $linktitle = ($count == 0 ? "" : ",") . $lg->title;
                            $count++;
                        }
                        $group["linked"] = $linktitle;
                    } else {
                        $group["linked"] = "false";
                    }
                    $groups[] = $group;
                }
            }
            $result["groups"] = $groups;
            $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
            if (!empty($stickyNotes))
                $result["stickyNotes"] = $stickyNotes;
            else
                $result["stickyNotes"] = '';

        }

        if ($loadFirst == 1) {
            if (($profileId == 'skinCareProfile') || ($profileId == 'fitProfile') || ($profileId == 'consultantProfile')) {
                $result["isGrouped"] = true;
                $result["isCompaigns"] = true;
                $result["isNotes"] = true;

                $stepcount = 0;
                $result['planQueryStart'] = getTimestamp();
                $showPlan = $contact_detail->showPlanStepsInContactDetail($database, $contactId, $userId);
                $result['planQueryEnd'] = getTimestamp();
                if (empty($showPlan)) {
                    $compaign = "";
                } else {
                    $newplanId = 0;
                    $count = 0;
                    $isContactUnsubscribed = $contact_detail->getContactPermissionStatus($database, $contactId);
                    foreach ($showPlan as $sp) {
                        $count++;
                        $result['LastStepQueryStart'] = getTimestamp();
                        $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                        $result['LastStepQueryEnd'] = getTimestamp();
                        if ($newplanId != $sp->planId) {
                            if ($newplanId != 0) {
                                $compaign["step"] = $allsteps;
                                $compaigns[] = $compaign;
                            }
                            $flag = false;
                            $allsteps = array();
                            $compaign["title"] = $sp->title;
                            $newplanId = $sp->planId;
                        }
                        $isskipped = ($sp->skippId == NULL ? false : true);
                        $step["isSkipped"] = $isskipped;
                        $step["summary"] = $sp->summary;
                        $step["taskId"] = $sp->taskId;
                        $step["isTask"] = $sp->isTask;
                        $isblocked = (($stepId->stepId == $sp->stepId && $sp->isTask == 0 && $sp->isDoubleOptIn == 1 && $isContactUnsubscribed == 3) ? true : false);
                        $step["isBlocked"] = $isblocked;
                        if ($sp->isTask == '0' && !empty($sp->stepEndDate)) {
                            $nextExecutionDate = date("d-M-Y", strtotime($sp->stepEndDate));
                            $step["stepEndDate"] = $nextExecutionDate;
                        } else {
                            $step["stepEndDate"] = false;
                        }

                        $allsteps[] = $step;
                        if (!empty($stepId->stepId) && $flag == false) {
                            $compaign["Completed"] = true;
                            $flag = true;
                        } else if (empty($stepId->stepId)) {
                            $compaign["Completed"] = false;
                            $compaign["planIsDoubleOpt"] = $sp->isDoubleOptIn;
                            $isunsub = ($isContactUnsubscribed == 3 ? true : false);
                            $compaign["isContactUnsubscribed"] = $isunsub;
                        }
                    }
                    if ($count > 0) {
                        $compaign["step"] = $allsteps;
                        $compaigns[] = $compaign;
                    }
                }
                $result["compaign"] = $compaigns;
                //groups
                $result['GroupQueryStart'] = getTimestamp();
                $contactgroups = $contact_detail->showContactGroups($database, $contactId, $generatedVia);

                $result['GroupQueryEnd'] = getTimestamp();
                if (!empty($contactgroups)) {
                    $groups = array();
                    $gc = array();
                    foreach ($contactgroups as $cg) {
                        $linked = $contact_detail->isGroupLinked($database, $cg->groupId, $userId);
                        if (!empty($linked)) {
                            $groupflag = true;
                        } else {
                            $groupflag = false;
                        }
                        $group["groupId"] = $cg->groupId;
                        $group["name"] = $cg->name;
                        $group["color"] = $cg->color;
                        $group["groupflag"] = $groupflag;
                        if ($groupflag == true) {
                            foreach ($linked as $lg) {
                                $linktitle = ($count == 0 ? "" : ",") . $lg->title;
                                $count++;
                            }
                            $group["linked"] = $linktitle;
                        } else {
                            $group["linked"] = "false";
                        }
                        $groups[] = $group;
                    }
                }
                $result["groups"] = $groups;
                $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
                if (!empty($stickyNotes))
                    $result["stickyNotes"] = $stickyNotes;
                else
                    $result["stickyNotes"] = '';
            }
        }


        $result['ReplyQueryStart'] = getTimestamp();
        $res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId, $newCode);
        $result['replyQueryEnd'] = getTimestamp();
        if (!empty($res['pageId']) && !empty($res['replyId'])) {
            $pageId = $res['pageId'];
            $replyId = $res['replyId'];
            $replyTime = $res['addDate'];
            $result['QuestionsQueryStart'] = getTimestamp();
            $questions = $profiles->getPageQuestionAnswers($database, $pageId, $replyId);
            $result['reeee'] = $questions;

            $result['questionsQueryEnd'] = getTimestamp();

        } else {
            $pageId = false;
            $replyId = false;
        }
        $result["pageId"] = $pageId;
        $result["replyId"] = $replyId;
        $replyTime = strtotime($replyTime);
        $replyTime = date("m/d/Y g:i A", $replyTime);
        $result["replyTime"] = $replyTime;
        $questionId = 0;
        //$question=array();
        $answers = array();
        $questionText = '';
        $countQ = 0;

        $alreadyPassed = false;
        foreach ($questions as $q) {
            $countQ++;
            //$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
            if ($questionId != $q->questionId && !empty($answers)) {
                //$qa->answer=$answers;
                //$questionAnswer[]=$qa;
                $questionAnswer[] = array("isChoice" => true, "answer" => $answers, "question" => $questionText);
                $answers = array();
            }
            //$qa->questioon=$q->question;
            $questionId = $q->questionId;
            $questionText = $q->question;
            if ($q->typeId == 1 || $q->typeId == 2) {
                //$qa->isChoice=false;
                if (!empty($q->answer))
                    $answer = $q->answer;
                else
                    $answer = $q->answerText;
                $questionAnswer[] = array("isChoice" => false, "answer" => $answer, "question" => $questionText);
            } else {
                //$qa->isChoice=true;
                if ($q->answer == NULL && $q->value == NULL) {
                    $ans["completeAnswer"] = $q->answerText;
                } else {
                    $ans["completeAnswer"] = $q->choiceText;
                    if ($q->value != NULL)
                        $ans["completeAnswer"] = '<b>' . $ans["completeAnswer"] . ':</b>&nbsp;&nbsp;&nbsp;' . $q->value;
                }
                if (strstr($q->choiceText, '<b>SALES CONSULTANT:</b>') or strstr($q->choiceText, '<b>TEAM LEADER:</b>') or strstr($q->choiceText, '<b>INDEPENDENT CONSULTANT:</b>')) {
                    if (!$alreadyPassed) {
                        $ans["completeAnswer"] = '<b>INDEPENDENT CONSULTANT:</b> Flexible schedule, work from home. Invite others to Discover Arbonne, mail samples, follow up by phone & mentor new Consultants. Excellent opportunity to develop marketing, recruiting, customer service & leadership skills. Training provided, no experience necessary.';
                        $answers[] = $ans;
                        $alreadyPassed = true;
                    }
                } else {
                    $answers[] = $ans;
                }
                if ($countQ == sizeof($questions)) {
                    $questionAnswer[] = array("isChoice" => true, "answer" => $answers, "question" => $questionText);
                }

            }

        }

    } else {
        $result["isGuestProfile"] = true;
        $result["contactId"] = $contactId;
        $result["profileId"] = $profileId;
        $result["userId"] = $userId;
        $result["generatedVia"] = '';
        $isGroup = true;
        $isCompaigns = true;
        $isNotes = true;
        $isCurrentActivities = true;
        $isHistory = true;
        $result["isGrouped"] = $isGroup;
        $result["isCompaigns"] = $isCompaigns;
        $result["isNotes"] = $isNotes;
        $result["isCurrentActivities"] = $isCurrentActivities;
        $result["isHistory"] = $isHistory;
        $contactdetail = $contact_detail->showContactDetail($database, $contactId);
        $dateofbirth = $contactdetail->dateOfBirth;
        $contactdetail->dateOfBirth = $dateOfBirth;
        if ($contactdetail->dateOfBirth != '0000-00-00' && $contactdetail->dateOfBirth != NULL && $contactdetail->dateOfBirth != '') {
            $contactdetail->age = (getAge($contactdetail->dateOfBirth) < 0 ? '0' : getAge($contactdetail->dateOfBirth)) . ' Years';
        } else {
            $contactdetail->age = '';
        }
        $status = $contact_detail->GetContactPermission($database, $contactId);
        $contactdetail->Subscribed = ($status == "Subscribed" ? true : false);
        $contactdetail->Unsubscribed = ($status == "Unsubscribed" ? true : false);
        $contactdetail->Unprocessed = ($status == "Unprocessed" ? true : false);
        $contactdetail->Pending = ($status == "Pending" ? true : false);
        $contactdetail->Profiles = $contacts->getContactProfiles($contactId);
        //$contactdetail = utf8_encode($contactdetail);
        $result["basicInfo"] = $contactdetail;
//			$result["basicInfo"] = array_map(utf8_encode, $result["basicInfo"]);

        //compaigns
        $stepcount = 0;
        $result['planQueryStart'] = getTimestamp();
        $showPlan = $contact_detail->showPlanStepsInContactDetail($database, $contactId, $userId);
        $result['planQueryEnd'] = getTimestamp();
        if (empty($showPlan)) {
            $compaign = "";
        } else {
            $newplanId = 0;
            $count = 0;
            $isContactUnsubscribed = $contact_detail->getContactPermissionStatus($database, $contactId);
            foreach ($showPlan as $sp) {
                $count++;
                $result['LastStepQueryStart'] = getTimestamp();
                $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                $result['LastStepQueryEnd'] = getTimestamp();
                if ($newplanId != $sp->planId) {
                    if ($newplanId != 0) {
                        $compaign["step"] = $allsteps;
                        $compaigns[] = $compaign;
                    }
                    $flag = false;
                    $allsteps = array();
                    $compaign["title"] = $sp->title;
                    $newplanId = $sp->planId;
                }
                $isskipped = ($sp->skippId == NULL ? false : true);
                $step["isSkipped"] = $isskipped;
                $step["summary"] = $sp->summary;
                $step["taskId"] = $sp->taskId;
                $step["isTask"] = $sp->isTask;
                $isblocked = (($stepId->stepId == $sp->stepId && $sp->isTask == 0 && $sp->isDoubleOptIn == 1 && $isContactUnsubscribed == 3) ? true : false);
                $step["isBlocked"] = $isblocked;
                if ($sp->isTask == '0' && !empty($sp->stepEndDate)) {
                    $nextExecutionDate = date("d-M-Y", strtotime($sp->stepEndDate));
                    $step["stepEndDate"] = $nextExecutionDate;
                } else {
                    $step["stepEndDate"] = false;
                }

                $allsteps[] = $step;
                if (!empty($stepId->stepId) && $flag == false) {
                    $compaign["Completed"] = true;
                    $flag = true;
                } else if (empty($stepId->stepId)) {
                    $compaign["Completed"] = false;
                    $compaign["planIsDoubleOpt"] = $sp->isDoubleOptIn;
                    $isunsub = ($isContactUnsubscribed == 3 ? true : false);
                    $compaign["isContactUnsubscribed"] = $isunsub;
                }
            }
            if ($count > 0) {
                $compaign["step"] = $allsteps;
                $compaigns[] = $compaign;
            }
        }
        $result["compaign"] = $compaigns;
        //groups
        $result['GroupQueryStart'] = getTimestamp();
        $contactgroups = $contact_detail->showContactGroups($database, $contactId, $generatedVia);

        $result['GroupQueryEnd'] = getTimestamp();
        if (!empty($contactgroups)) {
            $groups = array();
            $gc = array();
            foreach ($contactgroups as $cg) {
                $linked = $contact_detail->isGroupLinked($database, $cg->groupId, $userId);
                if (!empty($linked)) {
                    $groupflag = true;
                } else {
                    $groupflag = false;
                }
                $group["groupId"] = $cg->groupId;
                $group["name"] = $cg->name;
                $group["color"] = $cg->color;
                $group["groupflag"] = $groupflag;
                if ($groupflag == true) {
                    foreach ($linked as $lg) {
                        $linktitle = ($count == 0 ? "" : ",") . $lg->title;
                        $count++;
                    }
                    $group["linked"] = $linktitle;
                } else {
                    $group["linked"] = "false";
                }
                $groups[] = $group;
            }
        }
        $result["groups"] = $groups;
        $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
        if (!empty($stickyNotes))
            $result["stickyNotes"] = $stickyNotes;
        else
            $result["stickyNotes"] = '';
    }

    $result["questionAnswers"] = $questionAnswer;
    $result['historyQueryStart'] = getTimestamp();
    $result["history"] = activitiesappiontmentnoteshistory($database, $contact_detail, $profiles, $contactId, $profileId, $userId);
    $result['historyQueryEnd'] = getTimestamp();
    $result['currentQueryStart'] = getTimestamp();
    $result["currentActivities"] = currentsactivityappiontment($database, $contact_detail, $profiles, $contactId, $profileId, $userId);
    $result['currentQueryEnd'] = getTimestamp();
    echo json_encode($result);
}

function editcontactdetails($database, $contact_detail, $profiles, $contactId, $profileId, $userId, $contacts, $newCode, $loadFirst = 0)
{
    $basicInterests;
    $result = array();
    $questionAnswer = array();
    $generatedVia = $contact_detail->getProfileGeneratedVia($database, $contactId, $profileId);
    $groups = array();
    $result["contactId"] = $contactId;
    $result["profileId"] = $profileId;
    $result["userId"] = $userId;
    $result["generatedVia"] = $generatedVia;
    $result["newCode"] = $newCode;
    $result['loadFirst'] = $loadFirst;
    $result['isLoadFirst'] = ($loadFirst) ? true : false;
    $isGroup = false;
    $isCompaigns = false;
    $isNotes = false;
    $isCurrentActivities = false;
    $isHistory = false;

    $isActiveProfile = $contact_detail->getProfileActive($database, $userId, $newCode, $contactId);
    $isActiveProfile = true;
    if (!empty($isActiveProfile)) {
        if ($profileId == "guestProfile") {
            $isGroup = true;
            $isCompaigns = true;
            $isNotes = true;
            $isCurrentActivities = true;
            $isHistory = true;
            $isGuestProfile = true;
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = true;
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $dateofbirth = $contactdetail->dateOfBirth;
            if ($dateofbirth == '0000-00-00' || $dateofbirth == NULL || $dateofbirth == '') {
                $dateOfBirth = "";
            } else {
                $dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
            }
            $contactdetail->dateOfBirth = $dateOfBirth;
            if ($contactdetail->dateOfBirth != '0000-00-00' && $contactdetail->dateOfBirth != NULL && $contactdetail->dateOfBirth != '') {
                $contactdetail->age = (getAge($contactdetail->dateOfBirth) < 0 ? '0' : getAge($contactdetail->dateOfBirth)) . ' Years';
            } else {
                $contactdetail->age = '';
            }
            $status = $contact_detail->GetContactPermission($database, $contactId);
            $contactdetail->Subscribed = ($status == "Subscribed" ? true : false);
            $contactdetail->Unsubscribed = ($status == "Unsubscribed" ? true : false);
            $contactdetail->Unprocessed = ($status == "Unprocessed" ? true : false);
            $contactdetail->Pending = ($status == "Pending" ? true : false);
            $result["basicInfo"] = $contactdetail;


            $basicInterests = $contactdetail->interest;
            //compaigns
            $showPlan = $contact_detail->showPlans($database, $userId);
            $selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
            $contactPlans = array();
            $contactPlansteps = array();
            $pCounter = 0;
            $sCounter = 0;
            foreach ($selectedPlanDetail as $spd) {
                $contactPlans[$pCounter] = $spd->planId;
                $pCounter++;
            }
            foreach ($selectedPlanDetail as $sid) {
                $contactPlansteps[$sCounter] = $sid->stepId;
                $sCounter++;
            }
            foreach ($showPlan as $sp) {
                $compaign = array();
                $compaign["planId"] = $sp->planId;
                $compaign["userId"] = $sp->userId;
                $compaign["isDoubleOptIn"] = $sp->isDoubleOptIn;
                $compaign["title"] = $sp->title;
                $compaign["selectedIds"] = $contactPlansteps;






                $compaign["isChecked"] = ((in_array($sp->planId, $contactPlans)) ? true : false);
                if ($compaign["isChecked"]) {
                    $currentSelectedPlanStep = $contact_detail->getCurrentSelectedPlanStep($database, $sp->planId, $contactId);
                    If (empty($currentSelectedPlanStep->stepEndDate)){
                        $compaign["daysAfter"]==$currentSelectedPlanStep->daysAfter;
                    }else{
                        $compaign["stepEndDate"]=date("d-M-Y",strtotime($currentSelectedPlanStep->stepEndDate));
                        if ($currentSelectedPlanStep->isTask==0){
                            $compaign["isTask"]=true;
                        }else{
                            $compaign["isTask"]=false;
                        }
                    }

                    if (!empty($currentSelectedPlanStep->stepId)) {
                        $compaign["isEdited"] = '';
                    } else {
                        $compaign["stepEndDate"]=false;
                        $compaign["isEdited"] = editedStepMessage;
                    }
                }
                $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
                if (!empty($planStep)) {
                    $allstep = array();
                    $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                    $compaign["Completed"] = (!empty($stepId) ? true : false);
                    if ($compaign["Completed"]) {
                        $compaign["isEdited"] = '';
                    }
                    foreach ($planStep as $ps) {
                        $step = array();
                        $step["stepId"] = $ps->stepId;
                        $step["planId"] = $ps->planId;
                        $step["summary"] = $ps->summary;
                        $step["isTask"] = $ps->isTask;
                        $step["daysAfter"] = $ps->daysAfter;

                        $step["description"] = $ps->description;
                        $step["selected"] = ((in_array($ps->stepId, $contactPlansteps)) ? true : false);
                        $allstep[] = $step;
                    }
                    $compaign["step"] = $allstep;
                }
                $compaigns[] = $compaign;
            }
            $result["compaign"] = $compaigns;
            //editgroups

            $contactgroupsedit = $contact_detail->showGroups($database, $userId, $generatedVia);
            foreach ($contactgroupsedit as $cgedit) {
                $linkededit = $contact_detail->isGroupLinked($database, $cgedit->groupId, $userId);
                if (!empty($linkededit)) {
                    $groupflagedit = true;
                } else {
                    $groupflagedit = false;
                }
                $contactGroupedit = $contact_detail->editContactGroup($database, $cgedit->groupId, $contactId);
                $groupedit["groupId"] = $cgedit->groupId;
                $groupedit["name"] = $cgedit->name;
                $groupedit["color"] = $cgedit->color;
                $groupedit["groupflag"] = $groupflagedit;
                $groupedit["isChecked"] = ($contactGroupedit == 1 ? true : false);
                if ($groupflagedit == true) {
                    $countedit = 0;
                    foreach ($linkededit as $lgedit) {
                        $linktitleedit = ($countedit == 0 ? "" : ",") . $lgedit->title;
                        $countedit++;
                    }
                    $groupedit["linked"] = $linktitleedit;
                } else {
                    $groupedit["linked"] = "false";
                }
                $groupsedit[] = $groupedit;
            }
            $result["groups"] = $groupsedit;
            $contacttimetocall = $contact_detail->showContactTimeForCall($database, $contactId);
            //$stickyNotes=$profiles->contactNotes($contactId);
            $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
            if (!empty($stickyNotes)) {
                $result["stickyNotes"] = $stickyNotes;
            } else {
                $result["stickyNotes"] = "";
            }
        } elseif ($profileId == "skinCareProfile") {
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $result["basicInfo"] = $condetail;
        } elseif ($profileId == "fitProfile") {
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $result["basicInfo"] = $condetail;
        } elseif ($profileId == "consultantProfile") {
            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = false;
            $condetail = array();
            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;
            $condetail["phoneNumber"] = $contactdetail->phoneNumber;
            $result["basicInfo"] = $condetail;
            $res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
        } else {
            $res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId, $newCode);
            $pageId = $res['pageId'];
            $basicInfoQuestions = $profiles->getPageQuestions($database, $pageId, 1, 1);

            $isGroup = true;
            $isCompaigns = true;
            $isNotes = true;
            $isCurrentActivities = true;
            $isHistory = true;
            $isGuestProfile = false;

            $result["isGrouped"] = $isGroup;
            $result["isCompaigns"] = $isCompaigns;
            $result["isNotes"] = $isNotes;
            $result["isCurrentActivities"] = $isCurrentActivities;
            $result["isHistory"] = $isHistory;
            $result["isGuestProfile"] = $isGuestProfile;

            $condetail = array();

            $contactdetail = $contact_detail->showContactDetail($database, $contactId);
            $condetail["address"] = false;
            $condetail["bestTimes"] = false;
            $condetail["isPhoneNumber"] = false;
            $condetail["isDateOfBirth"] = false;
            $condetail["isReferredBy"] = false;


            $condetail['pageId'] = $pageId;
            $condetail['contactId'] = $contactId;
            $condetail['userId'] = $userId;
            $condetail['title'] = $contactdetail->title;
            $condetail['addedDate'] = $contactdetail->addedDate;
            $condetail['gmt'] = $contactdetail->gmt;
            $condetail["firstName"] = $contactdetail->firstName;
            $condetail["lastName"] = $contactdetail->lastName;
            $condetail["email"] = $contactdetail->email;

            foreach ($basicInfoQuestions as $biq) {
                $label = trim($biq->label);
                if (strpos($label, 'Best Times') !== false) {

                    $label = 'Best Times To Reach You';
                }
                switch ($label) {
//						case 'First Name':
//							$condetail["firstName"] = $contactdetail->firstName;
//							break;
//						case 'Last Name':
//							$condetail["lastName"] = $contactdetail->lastName;
//							break;
//						case 'Email':
//							$condetail["email"] = $contactdetail->email;
//							break;
                    case 'Phone number':
                        $condetail["isPhoneNumber"] = true;
                        $condetail["phoneNumber"] = $contactdetail->phoneNumber;
                        break;
                    case 'DOB':
                        $condetail["isDateOfBirth"] = true;
                        $dateofbirth = $contactdetail->dateOfBirth;
                        if ($dateofbirth == '0000-00-00' || $dateofbirth == NULL || $dateofbirth == '') {
                            $dateOfBirth = "";
                        } else {
                            $dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
                        }
                        $condetail['dateOfBirth'] = $dateOfBirth;
                        if ($dateOfBirth != '0000-00-00' && $dateOfBirth != NULL && $dateOfBirth != '') {
                            $condetail['age'] = (getAge($dateOfBirth) < 0 ? '0' : getAge($dateOfBirth)) . ' Years';
                        } else {
                            $condetail['age'] = '';
                        }
                        break;
                    case 'Referred By':
                        $condetail["isReferredBy"] = true;
                        $condetail["referredBy"] = $contactdetail->referredBy;
                        break;
                    case 'Street Address':
                        $condetail["street"] = $contactdetail->street;
                        break;
                    case 'City':
                        $condetail["city"] = $contactdetail->city;
                        break;
                    case 'State/Prov':
                        $condetail["state"] = $contactdetail->state;
                        break;
                    case 'Zip/Postal':
                        $condetail["zipcode"] = $contactdetail->zipcode;
                        break;
                    case 'Country':
                        $condetail["address"] = true;
                        $condetail["country"] = $contactdetail->country;
                        break;
                    case 'Best Times To Reach You':
                        $condetail["bestTimes"] = true;
                        $condetail["weekdays"] = $contactdetail->weekdays;
                        $condetail["weekdayEvening"] = $contactdetail->weekdayEvening;
                        $condetail["weekenddays"] = $contactdetail->weekenddays;
                        $condetail["weekenddayEvening"] = $contactdetail->weekenddayEvening;
                        break;
                }
            }

            $status = $contact_detail->GetContactPermission($database, $contactId);
            $condetail['Subscribed'] = ($status == "Subscribed" ? true : false);
            $condetail['Unsubscribed'] = ($status == "Unsubscribed" ? true : false);
            $condetail['Unprocessed'] = ($status == "Unprocessed" ? true : false);
            $condetail['Pending'] = ($status == "Pending" ? true : false);

            $condetail["Profiles"] = $contacts->getContactProfiles($contactId);


            $result["basicInfo"] = $condetail;
            //compaigns
            $showPlan = $contact_detail->showPlans($database, $userId);
            $selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
            $contactPlans = array();
            $contactPlansteps = array();
            $contactPlanstepsActive = '';
            $pCounter = 0;
            $sCounter = 0;
            foreach ($selectedPlanDetail as $spd) {
                $contactPlans[$pCounter] = $spd->planId;
                $pCounter++;
            }
            foreach ($selectedPlanDetail as $sid) {
                $contactPlansteps[$sCounter] = $sid->stepId;
                $sCounter++;
            }
            foreach ($showPlan as $sp) {
                $compaign = array();
                $compaign["planId"] = $sp->planId;
                $compaign["userId"] = $sp->userId;
                $compaign["isDoubleOptIn"] = $sp->isDoubleOptIn;
                $compaign["title"] = $sp->title;
                $compaign["selectedIds"] = $contactPlansteps;
                $compaign['stepEndDate']=$sp->stepEndDate;

                //
                $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
                //$selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
//                    foreach ($planStep as $ps)
//                    {
//                        $resultStep = $contact_detail->isEditedStep($database, $ps->stepId);
//
//                        $compaign["isEdited"] = ((in_array($resultStep, $contactPlansteps)) ? 'warning' : '0');
//                    }
//                    $resultStep = $contact_detail->getEditedstepSummry($database, $resultStep);
//                    if (!empty($resultSummay))
//                    {
//                        $compaign["editedStepSummary"] = $resultSummay;
//                    }
//                    else
//                    {
//                        $compaign["editedStepSummary"] = '';
//                    }
                //


                $compaign["isChecked"] = ((in_array($sp->planId, $contactPlans)) ? true : false);
                if ($compaign["isChecked"]) {
                    $currentSelectedPlanStep = $contact_detail->getCurrentSelectedPlanStep($database, $sp->planId, $contactId);
                    If (empty($currentSelectedPlanStep->stepEndDate)){
                        $compaign["daysAfter"]==$currentSelectedPlanStep->daysAfter;
                    }else{
                        $compaign["stepEndDate"]=date("d-M-Y",strtotime($currentSelectedPlanStep->stepEndDate));
                        if ($currentSelectedPlanStep->isTask==0){
                        $compaign["isTask"]=true;
                        }else{
                            $compaign["isTask"]=false;
                        }
                    }
                    if (!empty($currentSelectedPlanStep->stepId)) {
                        $compaign["isEdited"] = '';
                    } else {
                        $compaign["isEdited"] = editedStepMessage;
                    }
                }

                $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
                if (!empty($planStep)) {
                    $allstep = array();
                    $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                    $compaign["Completed"] = (!empty($stepId) ? true : false);
                    if ($compaign["Completed"]) {
                        $compaign["isEdited"] = '';
                    }

                    foreach ($planStep as $ps) {
                        $step = array();
                        $step["stepId"] = $ps->stepId;
                        $step["planId"] = $ps->planId;
                        $step["summary"] = $ps->summary;
                        $step["isTask"] = $ps->isTask;
                        $step["daysAfter"] = $ps->daysAfter;
                        $step["description"] = $ps->description;
                        $step["selected"] = ((in_array($ps->stepId, $contactPlansteps)) ? true : false);
                        $allstep[] = $step;
                    }
                    $compaign["step"] = $allstep;
                }
                $compaigns[] = $compaign;
            }
            $result["compaign"] = $compaigns;
            //editgroups

            $contactgroupsedit = $contact_detail->showGroups($database, $userId, $generatedVia);
            foreach ($contactgroupsedit as $cgedit) {
                $linkededit = $contact_detail->isGroupLinked($database, $cgedit->groupId, $userId);
                if (!empty($linkededit)) {
                    $groupflagedit = true;
                } else {
                    $groupflagedit = false;
                }
                $contactGroupedit = $contact_detail->editContactGroup($database, $cgedit->groupId, $contactId);
                $groupedit["groupId"] = $cgedit->groupId;
                $groupedit["name"] = $cgedit->name;
                $groupedit["color"] = $cgedit->color;
                $groupedit["groupflag"] = $groupflagedit;
                $groupedit["isChecked"] = ($contactGroupedit == 1 ? true : false);
                if ($groupflagedit == true) {
                    $countedit = 0;
                    foreach ($linkededit as $lgedit) {
                        $linktitleedit = ($countedit == 0 ? "" : ",") . $lgedit->title;
                        $countedit++;
                    }
                    $groupedit["linked"] = $linktitleedit;
                } else {
                    $groupedit["linked"] = "false";
                }
                $groupsedit[] = $groupedit;
            }
            $result["groups"] = $groupsedit;
            $contacttimetocall = $contact_detail->showContactTimeForCall($database, $contactId);
            //$stickyNotes=$profiles->contactNotes($contactId);
            $stickyNotes = $contact_detail->showStickyNotes($database, $contactId);
            if (!empty($stickyNotes)) {
                $result["stickyNotes"] = $stickyNotes;
            } else {
                $result["stickyNotes"] = "";
            }
//				$res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
//				$res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId, $newCode);
        }

        if ($loadFirst == 1) {
            if (($profileId == 'skinCareProfile') || ($profileId == 'fitProfile') || ($profileId == 'consultantProfile')) {
                $result["isGrouped"] = true;
                $result["isCompaigns"] = true;
                $result["isNotes"] = true;

                //compaigns
                $showPlan = $contact_detail->showPlans($database, $userId);
                $selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
                $contactPlans = array();
                $contactPlansteps = array();
                $pCounter = 0;
                $sCounter = 0;
                foreach ($selectedPlanDetail as $spd) {
                    $contactPlans[$pCounter] = $spd->planId;
                    $pCounter++;
                }
                foreach ($selectedPlanDetail as $sid) {
                    $contactPlansteps[$sCounter] = $sid->stepId;
                    $sCounter++;
                }
                foreach ($showPlan as $sp) {
                    $compaign = array();
                    $compaign["planId"] = $sp->planId;
                    $compaign["userId"] = $sp->userId;
                    $compaign["isDoubleOptIn"] = $sp->isDoubleOptIn;
                    $compaign["title"] = $sp->title;
                    $compaign["selectedIds"] = $contactPlansteps;
                    $compaign['stepEndDate']=$sp->stepEndDate;
                    // $compaign["four"] = $contactPlansteps;
//                        //
//                        $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
//                        //$selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
//                        foreach ($planStep as $ps)
//                        {
//
//                            $resultStep = $contact_detail->isEditedStep($database, $ps->stepId);
//                            $compaign["isEdited"] = ((in_array($resultStep, $contactPlansteps)) ? 'warning' : '0');
//
//                        }
//                        $resultStep = $contact_detail->getEditedstepSummry($database, $resultStep);
//                        if (!empty($resultSummay))
//                        {
//                            $compaign["editedStepSummary"] = $resultSummay;
//                        }
//                        else
//                        {
//                            $compaign["editedStepSummary"] = '';
//                        }
                    //


                    $compaign["isChecked"] = ((in_array($sp->planId, $contactPlans)) ? true : false);

                    if ($compaign["isChecked"]) {
                        $currentSelectedPlanStep = $contact_detail->getCurrentSelectedPlanStep($database, $sp->planId, $contactId);
                        If (empty($currentSelectedPlanStep->stepEndDate)){
                            $compaign["daysAfter"]==$currentSelectedPlanStep->daysAfter;
                        }else{
                            $compaign["stepEndDate"]=date("d-M-Y",strtotime($currentSelectedPlanStep->stepEndDate));
                            if ($currentSelectedPlanStep->isTask==0){
                                $compaign["isTask"]=true;
                            }else{
                                $compaign["isTask"]=false;
                            }
                        }
                        if (!empty($currentSelectedPlanStep->stepId)) {
                            $compaign["isEdited"] = '';
                        } else {
                            $compaign["isEdited"] = editedStepMessage;
                        }
                    }
                    $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
                    if (!empty($planStep)) {
                        $allstep = array();
                        $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                        $compaign["Completed"] = (!empty($stepId) ? true : false);
                        if ($compaign["Completed"]) {
                            $compaign["isEdited"] = '';
                        }
                        foreach ($planStep as $ps) {
                            $step = array();
                            $step["stepId"] = $ps->stepId;
                            $step["planId"] = $ps->planId;
                            $step["summary"] = $ps->summary;
                            $step["isTask"] = $ps->isTask;
                            $step["daysAfter"] = $ps->daysAfter;
                            // $step["four"]='four';
                            $step["description"] = $ps->description;
                            $step["selected"] = ((in_array($ps->stepId, $contactPlansteps)) ? true : false);
                            $allstep[] = $step;
                        }
                        $compaign["step"] = $allstep;
                    }
                    $compaigns[] = $compaign;
                }
                $result["compaign"] = $compaigns;
                //editgroups

                $contactgroupsedit = $contact_detail->showGroups($database, $userId, $generatedVia);
                foreach ($contactgroupsedit as $cgedit) {
                    $linkededit = $contact_detail->isGroupLinked($database, $cgedit->groupId, $userId);
                    if (!empty($linkededit)) {
                        $groupflagedit = true;
                    } else {
                        $groupflagedit = false;
                    }
                    $contactGroupedit = $contact_detail->editContactGroup($database, $cgedit->groupId, $contactId);
                    $groupedit["groupId"] = $cgedit->groupId;
                    $groupedit["name"] = $cgedit->name;
                    $groupedit["color"] = $cgedit->color;
                    $groupedit["groupflag"] = $groupflagedit;
                    $groupedit["isChecked"] = ($contactGroupedit == 1 ? true : false);
                    if ($groupflagedit == true) {
                        $countedit = 0;
                        foreach ($linkededit as $lgedit) {
                            $linktitleedit = ($countedit == 0 ? "" : ",") . $lgedit->title;
                            $countedit++;
                        }
                        $groupedit["linked"] = $linktitleedit;
                    } else {
                        $groupedit["linked"] = "false";
                    }
                    $groupsedit[] = $groupedit;
                }
                $result["groups"] = $groupsedit;
            }
        }


        //questions

//			$res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
        $res = $profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId, $newCode);
        if (!empty($res['pageId']) && !empty($res['replyId'])) {
            $pageId = $res['pageId'];
            $replyId = $res['replyId'];
            $questions = $profiles->getPageQuestionAnswersEdit($database, $pageId, $replyId);
        } else {
            $pageId = false;
            $replyId = false;
        }
        $result["pageId"] = $pageId;
        $result["replyId"] = $replyId;
        $questionId = 0;
        $choices = array();
        $choicelabel = array();
        $questionText = '';
        $typeId = '';
        $countQ = 0;
        $choiceId = '';
        $choiceIds = array();
        $counL = 0;
        $independentConsultant = false;
        $matched = '0';
        foreach ($questions as $q) {
            $countQ++;
            if ($questionId != $q->questionId && !empty($choices)) {
                $questionAnswer[] = array("isChoice" => true, "answer" => $choices, "choicelabel" => $choicelabel, "question" => $questionText, "questionId" => $questionId, "typeId" => $typeId);
                $choices = array();
                $choicelabel = array();
                $lables = array();
                $choiceIds = array();
            }
            $questionId = $q->questionId;
            $typeId = $q->typeId;
            $questionText = $q->question;
            if ($typeId == 1 || $typeId == 2) {
                //$qa->isChoice=false;
                if (!empty($q->answer))
                    $answer = $q->answer;
                else
                    $answer = $q->answerText;
                $questionAnswer[] = array("isChoice" => false, "answer" => $answer, "question" => $questionText, "questionId" => $questionId, "typeId" => $typeId);
            } else {
                if ($typeId == '5') {
                    if (empty($lables)) {
                        $lables = $profiles->getQuestionLabel($database, $questionId);
                        $countabel = sizeof($lables);
                        $i = 0;
                        foreach ($lables as $lable) {
                            $choicelabel[$i] = $lable->label;
                            $i++;
                        }
                    }
                    if ($q->choiceId != $choiceId) {
                        $ans["isNewRow"] = true;
                    } else {
                        $ans["isNewRow"] = false;
                    }
                    //$ans["answerLabel"]=$q->value;
                    $choiceId = $q->choiceId;
                    $ans["questionId"] = $questionId;
                    $ans["choiceId"] = $choiceId;
                    $ans["choiceText"] = $q->choiceText;
                    //$ans["value"]=$q->value;
                    if ($q->answer == $q->choiceId) {
                        foreach ($lables as $lable) {
                            if ($q->value == $lable->label) {
                                $ans["isChecked"] = true;
                                $ans["answerLabel"] = $lable->label;
                                //$ans["choicelabel"]=$choicelabel;
                                $choices[] = $ans;
                            }
                        }
                    }
                }
                if ($typeId == '4' || $typeId == '3') {
                    if ((($q->questionId == '42' or $q->questionId == '14' or $q->questionId == '10') and ($q->choiceId == '193' or $q->choiceId == '194' or $q->choiceId == '73' or $q->choiceId == '74' or $q->choiceId == '47' or $q->choiceId == '46'))) {
                        $matched = '1';
                        $independentConsultant = true;
                    } else {
                        $choiceId = $q->choiceId;
                        $ans["questionId"] = $questionId;
                        $ans["choiceId"] = $choiceId;
                        $ans["choiceText"] = $q->choiceText;
                        //$ans['message'] = 'nothing';
                        if ($q->answer == $q->choiceId) {
                            $ans["isChecked"] = true;
                            //Atif - Changes Start
                            if (in_array($q->choiceId, $choiceIds)) {
                                //remove the old one from choices
                                $NewChoices = $choices;
                                $choices = array();
                                $choiceIds = array();
                                foreach ($NewChoices as $pc) {
                                    if ($pc["choiceId"] == $q->choiceId) {
                                        //$ans['message'] = 'remove the old one - '.$pc["choiceId"].'  -  '.$q->choiceId.'  -  '.$q->answer;
                                        //unset($choices[$cc]);
                                    } else {
                                        if (sizeof($choices) % 3 == 0) {
                                            $pc["isNewRow"] = true;
                                        } else {
                                            $pc["isNewRow"] = false;
                                        }
                                        $choices[] = $pc;
                                        array_push($choiceIds, $pc['choiceId']);
                                        //print_r($choices);
                                    }
                                }
                            }
                            //Atif - Changes End
                        } else {
                            $ans["isChecked"] = false;
                        }
                        if (sizeof($choices) % 3 == 0) {
                            $ans["isNewRow"] = true;
                        } else {
                            $ans["isNewRow"] = false;
                        }
                        if (($q->choiceId == '203') or ($q->choiceId == '202') or ($q->choiceId == '201')) {
                            //if(strstr($basicInterests,'TEAM LEADER') || strstr($basicInterests,'SALES CONSULTANT') || ($matched == '1'))
                            if ((strstr($basicInterests, 'TEAM LEADER') || strstr($basicInterests, 'SALES CONSULTANT')) && ($matched == '1')) {
                                $ans['isChecked'] = true;
                                $matched = '0';
                            }
                        }
                        //Atif - Changes End
                        //print_r($choiceIds);
                        if (!(in_array($choiceId, $choiceIds))) {
                            $choices[] = $ans;
                            array_push($choiceIds, $q->choiceId);
                        }
                    }
                }
                if ($countQ == sizeof($questions)) {
                    $questionAnswer[] = array("isChoice" => true, "answer" => $choices, "choicelabel" => $choicelabel, "question" => $questionText, "questionId" => $questionId, "typeId" => $typeId);
                }
            }
        }
        /*$choice=array();
        $q->replyId=$replyId;
                   if($q->typeId=='4')
                    {

                        $q->isChoice=true;
                        $choices=$profiles->getQuestionChoices($database, $q->questionId);
                          $answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
                        $i=0;
                        foreach($choices as $c)
                        {
                          if($i%3==0){
                              $ans["isNewRow"]=true;
                              }else{
                                  $ans["isNewRow"]=false;
                                  }
                           foreach($answer as $a){
                                if($c->choiceText==$a->choiceText){
                                    $ans["isChecked"]=true;
                                    }else{
                                    $ans["isChecked"]=false;
                                if($ans["isChecked"]==true){break;}
                                        }
                            }
                            $ans["questionId"]=$q->questionId;
                            $ans["choiceId"]=$c->choiceId;
                            $ans["choiceText"]=$c->choiceText;
                        $choice[]=$ans;
                        $i++;
                        }

                    $q->answer=$choice;
            }
        if($q->typeId=='3')
        {
            $choices=$profiles->getQuestionChoices($database, $q->questionId);
             $answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);

                $q->isChoice=true;
                $j=0;
                foreach($choices as $c)
                {
                   if($j%3==0){
                              $ans["isNewRow"]=true;
                              }else{
                                  $ans["isNewRow"]=false;
                                  }
                    foreach($answer as $a){
                        if($c->choiceText==$a->choiceText){
                            $ans["isChecked"]=true;
                            }else{
                                        $ans["isChecked"]=false;
                                        }
                                if($ans["isChecked"]==true){break;}
                                }
                            $ans["questionId"]=$q->questionId;
                               $ans["choiceId"]=$c->choiceId;
                            $ans["choiceText"]=$c->choiceText;
                        $choice[]=$ans;
                $j++;
                }
      $q->answer=$choice;
   }
 if($q->typeId=='5')
                    {
                        $choicelabel=array();
                        $lables=$profiles->getQuestionLabel($database,$q->questionId);
                         foreach($lables as $lable)
                           {
                               $choicelabel[]=$lable->label;
                            }
                        $q->isChoice=true;
                        $choices=$profiles->getQuestionChoices($database, $q->questionId);
                          $answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);


                        foreach($choices as $c)
                        {
                        $i=0;
                        foreach($lables as $lable)
                           {

                             if($i==0){
                              $ans["isNewRow"]=true;
                              }else{
                                  $ans["isNewRow"]=false;
                                  }

                           foreach($answer as $a){
                                $ans["isChecked"]=(($lable->label==$a->value && $c->choiceId==$a->answer)?true:false);
                                if($ans["isChecked"]==true){break;}
                            }
                            $ans["questionId"]=$q->questionId;
                            $ans["choiceId"]=$c->choiceId;
                            $ans["choiceText"]=$c->choiceText;
                          $ans["answerLabel"]=$lable->label;

                        $choice[]=$ans;
                        $i++;
                        }

                    }

                    $q->answer=$choice;
                    $q->choicelabel=$choicelabel;

            }
      if($q->typeId=='2' || $q->typeId=='1')
        {
            $q->isChoice=false;
            $answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
            if(!empty($answer->answer)){
                $q->answer=$answer->answer;
            }elseif(!empty($answer->answerText)){
                $q->answer=$answer->answerText;
            }else{
                $q->answer="";
                }
        }
    $questionAnswer[]=$q;	*/

        $result["compaign"] = $compaigns;
    } else {
        $isGroup = true;
        $isCompaigns = true;
        $isNotes = true;
        $isCurrentActivities = true;
        $isHistory = true;
        $isGuestProfile = true;
        $result["isGrouped"] = $isGroup;
        $result["isCompaigns"] = $isCompaigns;
        $result["isNotes"] = $isNotes;
        $result["isCurrentActivities"] = $isCurrentActivities;
        $result["isHistory"] = $isHistory;
        $result["isGuestProfile"] = true;
        $contactdetail = $contact_detail->showContactDetail($database, $contactId);
        $dateofbirth = $contactdetail->dateOfBirth;
        if ($dateofbirth == '0000-00-00' || $dateofbirth == NULL || $dateofbirth == '') {
            $dateOfBirth = "";
        } else {
            $dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
        }
        $contactdetail->dateOfBirth = $dateOfBirth;
        if ($contactdetail->dateOfBirth != '0000-00-00' && $contactdetail->dateOfBirth != NULL && $contactdetail->dateOfBirth != '') {
            $contactdetail->age = (getAge($contactdetail->dateOfBirth) < 0 ? '0' : getAge($contactdetail->dateOfBirth)) . ' Years';
        } else {
            $contactdetail->age = '';
        }
        $status = $contact_detail->GetContactPermission($database, $contactId);
        $contactdetail->Subscribed = ($status == "Subscribed" ? true : false);
        $contactdetail->Unsubscribed = ($status == "Unsubscribed" ? true : false);
        $contactdetail->Unprocessed = ($status == "Unprocessed" ? true : false);
        $contactdetail->Pending = ($status == "Pending" ? true : false);
        $result["basicInfo"] = $contactdetail;
        $basicInterests = $contactdetail->interest;
        //compaigns
        $showPlan = $contact_detail->showPlans($database, $userId);
        $selectedPlanDetail = $contact_detail->showPlanStepsIdsInContactDetail($database, $contactId, $userId);
        $contactPlans = array();
        $contactPlansteps = array();
        $pCounter = 0;
        $sCounter = 0;
        foreach ($selectedPlanDetail as $spd) {
            $contactPlans[$pCounter] = $spd->planId;
            $pCounter++;
        }
        foreach ($selectedPlanDetail as $sid) {
            $contactPlansteps[$sCounter] = $sid->stepId;
            $sCounter++;
        }
        foreach ($showPlan as $sp) {
            $compaign = array();
            $compaign["planId"] = $sp->planId;
            $compaign["userId"] = $sp->userId;
            $compaign["isDoubleOptIn"] = $sp->isDoubleOptIn;
            $compaign["title"] = $sp->title;
            $compaign["isChecked"] = ((in_array($sp->planId, $contactPlans)) ? true : false);
            $planStep = $contact_detail->showPlanSteps($database, $sp->planId);
            if (!empty($planStep)) {
                $allstep = array();
                $stepId = $contact_detail->CheckLastStepOfPlan($database, $sp->planId, $contactId);
                $compaign["Completed"] = (!empty($stepId) ? true : false);
                if ($compaign["Completed"]) {
                    $compaign["isEdited"] = '';
                }
                foreach ($planStep as $ps) {
                    $step = array();
                    $step["stepId"] = $ps->stepId;
                    $step["planId"] = $ps->planId;
                    $step["summary"] = $ps->summary;
                    $step["isTask"] = $ps->isTask;
                    $step["daysAfter"] = $ps->daysAfter;
                    $step["description"] = $ps->description;
                    $step["selected"] = ((in_array($ps->stepId, $contactPlansteps)) ? true : false);
                    $allstep[] = $step;
                }
                $compaign["step"] = $allstep;
            }
            $compaigns[] = $compaign;
        }
        $result["compaign"] = $compaigns;
        //editgroups

        $contactgroupsedit = $contact_detail->showGroups($database, $userId, $generatedVia);
        foreach ($contactgroupsedit as $cgedit) {
            $linkededit = $contact_detail->isGroupLinked($database, $cgedit->groupId, $userId);
            if (!empty($linkededit)) {
                $groupflagedit = true;
            } else {
                $groupflagedit = false;
            }
            $contactGroupedit = $contact_detail->editContactGroup($database, $cgedit->groupId, $contactId);
            $groupedit["groupId"] = $cgedit->groupId;
            $groupedit["name"] = $cgedit->name;
            $groupedit["color"] = $cgedit->color;
            $groupedit["groupflag"] = $groupflagedit;
            $groupedit["isChecked"] = ($contactGroupedit == 1 ? true : false);
            if ($groupflagedit == true) {
                $countedit = 0;
                foreach ($linkededit as $lgedit) {
                    $linktitleedit = ($countedit == 0 ? "" : ",") . $lgedit->title;
                    $countedit++;
                }
                $groupedit["linked"] = $linktitleedit;
            } else {
                $groupedit["linked"] = "false";
            }
            $groupsedit[] = $groupedit;
        }
        $result["groups"] = $groupsedit;
    }

    $result["questionAnswers"] = $questionAnswer;
    $result["queryHistoryStart"] = getTimestamp();
    $result["history"] = activitiesappiontmentnoteshistory($database, $contact_detail, $profiles, $contactId, $profileId, $userId);
    $result["queryHistoryEnd"] = getTimestamp();
    $result["currentActivities"] = currentsactivityappiontment($database, $contact_detail, $profiles, $contactId, $profileId, $userId);
    echo json_encode($result);
}

function getAge($then)
{
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if (strtotime('+' . $age . ' years', $then_ts) > time())
        $age--;

    return $age;
}

function activitiesappiontmentnoteshistory($database, $contact_detail, $profiles, $contactId, $profileId, $userId)
{
    $notes1 = $contact_detail->contactNotes($database, $contactId);
    $activitiesCompleted1 = $contact_detail->ActivitiesHistory($database, $userId, $contactId);
    $emailHistory1 = $contact_detail->EmailHistory($database, $userId, $contactId);
    // $result["history"]="";
    $arraynotes = array();
    $arrayactivities = array();
    $arrayemail = array();
    if (!empty($notes1)) {
        foreach ($notes1 as $no) {
            $no->addedDate = $date = date('m/d/Y', strtotime($no->addedDate));
            $no->isActivity = false;
            $no->isEmail = false;
            $no->isNotes = true;
            $note[] = $no;
        }
        $arraynotes = $note;
    }
    if (!empty($activitiesCompleted1)) {
        $nodate = '1990-12-12';
        foreach ($activitiesCompleted1 as $actCmplt) {
            $upcmingactivitycontact = array();
            $datetime = $actCmplt->completedDate;
            $date = date('m/d/Y', strtotime($datetime));
            ((date('H:i:s', strtotime($datetime))) != '00:00:00' ? $time = date('g:i a', strtotime($datetime)) : $time = '');
            $contactIds2 = $contact_detail->getcontactId2($database, $actCmplt->taskId, $userId);
            foreach ($contactIds2 as $cc) {
                $contact2["contactId"] = $cc->contactId;
                $contact2["firstName"] = $cc->firstName;
                $contact2["lastName"] = $cc->lastName;
                $upcmingactivitycontact[] = $contact2;
            }
            $actCmplt->contactId = $contactId;
            if (strlen($actCmplt->title) > 22)
                $actCmplt->title = substr($actCmplt->title, 0, 22) . "...";
            else $actCmplt->title = $actCmplt->title;
            $actCmplt->dates = (strtotime($date) < strtotime($nodate) ? '' : $date);
            $actCmplt->times = $time;
            $actCmplt->contact = $upcmingactivitycontact;
            $actCmplt->isActivity = true;
            $actCmplt->isEmail = false;
            $actCmplt->isNotes = false;
            $activitiesCompleted[] = $actCmplt;
        }
        $arrayactivities = $activitiesCompleted;
    }
    if (!empty($emailHistory1)) {
        foreach ($emailHistory1 as $emailCmplt) {
            $emailHistorycontact = array();
            $datetime = $emailCmplt->dateTime;
            $date = date('m/d/Y', strtotime($datetime));
            ((date('H:i:s', strtotime($datetime))) != '00:00:00' ? $time = date('g:i a', strtotime($datetime)) : $time = '');
            $contactIds3 = $contact_detail->getContactDetails($database, $emailCmplt->contactId, $userId);
            $contact3["contactId"] = $contactIds3->contactId;
            $contact3["firstName"] = $contactIds3->firstName;
            $contact3["lastName"] = $contactIds3->lastName;
            $emailHistorycontact[] = $contact3;
            if (strlen($emailCmplt->summary) > 22)
                $emailCmplt->summary = substr($emailCmplt->summary, 0, 22) . "...";
            else $emailCmplt->summary = $emailCmplt->summary;
            $emailCmplt->dates = (strtotime($date) < strtotime($nodate) ? '' : $date);
            $emailCmplt->times = $time;
            $emailCmplt->contact = $emailHistorycontact;
            $emailCmplt->isActivity = false;
            $emailCmplt->isEmail = true;
            $emailCmplt->isNotes = false;
            $emailHistory[] = $emailCmplt;
        }
        $arrayemail = $emailHistory;
    }

    return $resultarrayhistory = array_merge($arraynotes, $arrayactivities, $arrayemail);
}

function currentsactivityappiontment($database, $contact_detail, $profiles, $contactId, $profileId, $userId)
{
    //current activities
    $arraycurrentactivitiesedit = array();
    $arrayappiontmentsedit = array();
    $activitiescu = $contact_detail->upcomingActivities($database, $userId, $contactId);
    if (!empty($activitiescu)) {
        foreach ($activitiescu as $act) {
            $upcmingactivitycontact = array();
            if ($act->isActive) {
            } else {
                $datetime = $act->dueDateTime;
                if ($act->dueDateTime == '0000-00-00 00:00:00') {
                    $date = '';
                    $time = '';
                } else {
                    $date = date('m/d/Y', strtotime($datetime));
                    ((date('H:i:s', strtotime($datetime))) != '00:00:00' ? $time = date('g:i a', strtotime($datetime)) : $time = '');
                }
                $contactIds0 = $contact_detail->getcontactId2($database, $act->taskId, $userId);
                foreach ($contactIds0 as $ci) {
                    $contact0 = array();
                    $contact0["contactId"] = $ci->contactId;
                    $contact0["firstName"] = $ci->firstName;
                    $contact0["lastName"] = $ci->lastName;
                    $upcmingactivitycontact[] = $contact0;
                }
            }
            $act->contactId = $contactId;
            if (strlen($act->title) > 22)
                $act->title = substr($act->title, 0, 22) . "...";
            else $act->title = $act->title;
            $act->reoccur = (($act->repeatTypeId != 1 && $act->planId == 0) ? true : false);
            $act->dates = $date;
            $act->times = $time;
            $act->contact = $upcmingactivitycontact;
            $act->isActivity = true;
            $act->isAppiontment = false;
            $act->isNotes = false;
            $upcmingactivity[] = $act;
        }
        $arraycurrentactivitiesedit = $upcmingactivity;
    }
    //appiontment activities
    $appiontmentactivitiesup = $contact_detail->upcomingActivitiesAppiontments($database, $userId, $contactId);
    if (!empty($appiontmentactivitiesup)) {
        foreach ($appiontmentactivitiesup as $app) {
            $upcmingappiontmentactivitiescontacts = array();
            $contactIds1 = $contact_detail->getcontactId3($database, $app->appiontmentId, $userId);
            $datetime = $app->endDateTime;
            $date = date('m/d/Y', strtotime($datetime));
            ((date('H:i:s', strtotime($datetime))) != '00:00:00' ? $time = date('g:i a', strtotime($datetime)) : $time = '');
            foreach ($contactIds1 as $ci) {
                $contact1 = array();
                $contact1["contactId"] = $ci->contactId;
                $contact1["firstName"] = $ci->firstName;
                $contact1["lastName"] = $ci->lastName;
                $upcmingappiontmentactivitiescontacts[] = $contact1;
            }
            if (strlen($app->title) > 22)
                $app->title = substr($app->title, 0, 22) . "...";
            else $app->title = $app->title;
            $app->reoccur = (($app->repeatTypeId != 1 && $app->repeatTypeId != 0) ? true : false);
            $app->dates = $date;
            $app->times = $time;
            $app->contact = $upcmingappiontmentactivitiescontacts;
            $app->isActivity = false;
            $app->isAppiontment = true;
            $app->isNotes = false;
            $upcmingappiontmentactivities[] = $app;
        }
        $arrayappiontmentsedit = $upcmingappiontmentactivities;
    }

    return $resultcurrentActivitiesArray = array_merge($arraycurrentactivitiesedit, $arrayappiontmentsedit);
}

function getTimestamp()
{
    return date("Y-m-d\TH:i:s") . substr((string)microtime(), 1, 8);
}

?>