<?php
session_start();
error_reporting(0);
require_once("../../classes/init.php");
include_once("../headerFooter/headerContact.php");
include_once("../../classes/contact_details.php");
require_once('../../classes/contact.php');
require_once('../../classes/profiles.php');
$database = new database();
$profile = new profiles();
$contact_details = new contactDetails();
$contact = new contact();

$contactId = $_GET['contactId'];
if ($contactId == '')
    $contactId = $_SESSION['contactId'];

$userId = $_SESSION['userId'];
$leaderId = $_SESSION['leaderId'];
$ContactDetail = $contact->getAllimportcontactDetails($contactId, $database);
$contactProfiles = $contact_details->getContactProfiles($database, $contactId);
$getPermissionEmail = $contact->GetPermissionEmail($userId);
$user = $contact->GetUserName($userId);
$tcpProfileId = 'guestProfile';

if (!empty($contactProfiles)) {
    foreach ($contactProfiles as $tcp) {
        $tcpNewCode = $tcp->newCode;
        $tcpProfileId = $tcp->pageId;
        break;
    }
}
?>

    <script src="../../js/script.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/handlebars.js"></script>
    <script type="text/javascript" src="../../js/helpers.js"></script>
    <script type="text/javascript">
    //$.get("http://zenplify.biz/modules/profiles/userProfile.php?code=kanzaguestprofile", function(response) { alert(response) });
    //window.location = "view-source: http://zenplify.biz/modules/profiles/userProfile.php?code=kanzaguestprofile";
    Handlebars.registerHelper('if_equal', function (str, taskId, contactId) {
        str = str.replace(/\s+/g, '');
        return (str == "0") ? '<a href="../task/edit_task.php?task_id=' + taskId + '&contactId=' + contactId + '"><img src="../../images/edit.png" title="edit" alt="edit"/></a>' : '';
    });

    function showCalander(planId) {
        console.log(planId);
        $("#executionDate" + planId).datepicker({
            dateFormat: "dd-MM-yy"
        });
    }


    function loadTabContent(tabUrl) {
        $("#preloader").show();
        jQuery.ajax({
            url: tabUrl,
            type: 'post',
            cache: false,
            data: {cid:<?php echo $contactId; ?>},
            success: function (message) {
                //jQuery("#content").empty().append(message);
                jQuery("#content").append(message);
                $("#preloader").hide();
            }
        });
    }
    function loadFilters(tabUrl) {
        var $deferredsLocal = [];
        $deferredsLocal[0] = $.ajax({ url: tabUrl, type: "POST"});
        $deferredsLocal[1] = $.ajax({url: 'templates/tabs_contact_detail.html', type: "GET", cache: true});
        $.when.apply(null, $deferredsLocal).then(function (json, htm) {
            // do something with the data and template
            //var json=eval('('+json[0]+')');
            var htmls = htm[0];
            var template = Handlebars.compile(htmls);
            var completeHtml = template(json[0]);
            //alert(completeHtml);
            //completeHtml = $('<div/>').html(completeHtml).text();
            //$('#content').empty().append(completeHtml);
            $('#content').empty();
            $('#content').html(completeHtml);
            //console.log(completeHtml);
            $('#content').find('td.question_label').each(function (index, element) {
                $(this).html($('<div/>').html($(this).html()).text());
            });
            $("#preloader").hide();
        });
    }
    function getEditUrl(profile, contactId, userId, generatedVia, newCode, loadFirst) {
        // get tab id and tab url
        $("#preloader").show();
        tabId = $(this).attr("id");
        tabUrl = "storeproc.php?action=edit&contactId=" + contactId + "&profileId=" + profile + "&userId=" + userId + "&generatedVia=" + generatedVia + "&newCode=" + newCode + "&loadFirst=" + loadFirst;

        if (profile == 'guestProfile') {
            $("[id^=tab]").removeClass("current");
            $("#tab1").addClass("current");
        }
        if (profile == 'skinCareProfile') {
            $("[id^=tab]").removeClass("current");
            $("#tab3").addClass("current");
        }
        if (profile == 'fitProfile') {
            $("[id^=tab]").removeClass("current");
            $("#tab4").addClass("current");
        }
        if (profile == 'consultantProfile') {
            $("[id^=tab]").removeClass("current");
            $("#tab5").addClass("current");
        }
        // load tab content
        loadEditScreen(tabUrl);
        return false;
    }
    function submitProfile(profile, contactId, userId, newCode, loadFirst) {
        $("#edit_contact1").css("background", "url(../../images/save-hover.png) no-repeat");
        $("#edit_contact").css("background", "url(../../images/save-hover.png) no-repeat");
        $("#edit_contact1").off('click');
        $("#edit_contact").off('click');

        $("#backgroundEditContact").show();
        var data = $('#editQuestions').serialize();
        $.post('editProfiles.php', data).done(function (data) {
            $("#backgroundEditContact").hide();
            $("#edit_contact1").hide();
            $("#edit_contact").hide();
            //alert("Data Loaded:");
            loadFilters("storeproc.php?action=view&contactId=" + contactId + "&profileId=" + profile + "&userId=" + userId + "&newCode=" + newCode + "&loadFirst=" + loadFirst);
        });
    }
    function loadEditScreen(tabUrl) {
        var $deferredsLocal = [];
        $deferredsLocal[0] = $.ajax({ url: tabUrl, type: "POST"});
        $deferredsLocal[1] = $.ajax({url: 'templates/tabs_contact_edit.html', type: "GET", cache: true});
        $.when.apply(null, $deferredsLocal).then(function (json, htm) {
            // do something with the data and template
            //var json=eval('('+json[0]+')');
            var htmls = htm[0];
            var template = Handlebars.compile(htmls);
            var completeHtml = template(json[0]);

            $('#content').empty().append(completeHtml);
            $('#content').find('td.question_label').each(function (index, element) {
                $(this).html($('<div/>').html($(this).html()).text());
            });
            $("#preloader").hide();
            $('#birthdate').datepicker();
            $('.loadDatePicker').datepicker({
                minDate: 0,
                dateFormat: "dd-MM-yy"
            });
        });
    }
    jQuery(document).ready(function () {
        $(document).on('mousedown', ":checkbox", function () {
            if ($(this).val() == '201') {
                $(":checkbox[value=193]").attr("checked", false);
                $(":checkbox[value=194]").attr("checked", false);
            }
            else if ($(this).val() == '202') {
                $(":checkbox[value=73]").attr("checked", false);
                $(":checkbox[value=74]").attr("checked", false);
            }
            else if ($(this).val() == '203') {
                $(":checkbox[value=46]").attr("checked", false);
                $(":checkbox[value=47]").attr("checked", false);
            }
        });
        $("#preloader").show();
        loadFilters("storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=<?php echo $tcpProfileId; ?>&userId=<?php echo $userId;?>&newCode=<?php echo $tcpNewCode; ?>&loadFirst=1");


        jQuery("[id^=tab]").click(function () {
            $("#preloader").show();
            // get tab id and tab url
            tabId = $(this).attr("id");
            tabUrl = $(this).attr("href");
//			tabUrl = jQuery("#" + tabId).attr("href");
            jQuery("[id^=tab]").removeClass("current");
//			jQuery("#" + tabId).addClass("current");
            $(this).addClass("current");
            // load tab content
            loadFilters(tabUrl);
            return false;
        });
//        $('#birthdate').datepicker();
        $("input[id^='step']").change(function () {
            var id = $(this).val();
            $('#step_list' + id).toggle('show');
        });
        $("#birthdate").change(function () {
            var dateOfBirth = $(this).val();
            var todaydate = new Date();
            var todaytime = todaydate.getTime();
            var birthdaytime = new Date(dateOfBirth).getTime();
            if (birthdaytime >= todaytime) {
                $("#doberror").html('Invalid Date of Birth');
                $("#dobflag").val(1);
                $("#submit").get(0).setAttribute('type', 'button');
            }
            else {
                $("#doberror").html('');
                $("#dobflag").val(0);
                $("#submit").get(0).setAttribute('type', 'submit');
            }
        });
        $(window).resize(function () {
            var left = $('.sticky_notes').css('left');
            $('.sticky_notes').css('left', (50 + 20) + '%');
            //alert('hi');
        });
        $(window).trigger('resize');
        /*$("#change").click(function(){$('#navcontainer').css({"display":"none"});$('#nav_container').show();});*/
    });
    function filDate(planId) {
        $(".checkbox").change(function () {
            if (this.checked) {


                var dConverted = $.datepicker.formatDate('dd-MM-yy', new Date())

                $("#executionDate" + planId).val(dConverted);
                console.log(planId);

            } else {
                $("#executionDate" + planId).val('');
                console.log(planId);
            }
            planId = 0;
        });

    }
    function checkforTheDate(planId) {

        var selectedstepId = $('#step_list' + planId).val();
        console.log('step Id is' + selectedstepId);
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {selectedstepId: selectedstepId, action: 'getDaysAfter'},
            success: function (data) {
                var d = $.datepicker.formatDate('dd-MM-yy', new Date())
                console.log(d);
                if ((new Date(data).getTime() >= new Date(d).getTime())) {
                    $('#showExecutionDate' + planId).show().fadeIn('300');
                    $('#executionDate' + planId).val(data);


                } else {
                    $('#showExecutionDate' + planId).hide().fadeOut('300');
                }
            }
        });

    }
    function skipTask(id) {

        //alert(id);
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {skipId: id, action: 'skipIt'},
            success: function (data) {
                if (data == 1) {
                    $("#task" + id).fadeOut('500');
                }
            }
        });
    }
    function delupcomingActivities(id) {
        //alert('u want to delete');
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {taskId: id, action: 'deleteTask', contactId: '<?php echo $contactId;?>'},
            success: function (msg) {
                // $('#record'+id).fadeOut(1000);
                $("#task" + id).fadeOut('500');
            }
        });
        //$('#record'+id).fadeOut(1000);
    }
    function delupcomingAppiontments(id) {
        //alert('u want to delete');
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {appiontmentId: id, action: 'deleteAppiontment', contactId: '<?php echo $contactId;?>'},
            success: function (msg) {
                $('#recordAppiontment' + id).fadeOut(1000);
            }
        });
    }
    function delnotes(id) {
        //alert('u want to delete');
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {notesId: id, action: 'deleteNotes', contactId: '<?php echo $contactId;?>'},
            success: function (msg) {
                $('#recordNotes' + id).fadeOut(1000);
            }
        });
    }
    function saveNotes() {
        $("#preloader").show();
        if ($('#stickyNotes').val() == "") {
            $("#preloader").hide();
        }
        else {
            $.ajax({type: "POST", url: "../../classes/ajax.php", data: {stickyNotes: $('#stickyNotes').val(), action: 'checkStickyNotes', contactId: '<?php echo $contactId;?>'},
                success: function (msg) {
                    if (msg != '') {
                        $("#preloader").hide();
                        $.trim('#stickyNotes').val(msg);
                    }
                    else {
                        //alert('error');
                    }
                }
            });
        }
    }
    function moveToHistory() {
        if ($('#stickyNotes').val() == "") {
        }
        else {
            var notesId = $('#stickyNoteId').val();
            $.ajax({
                type: "POST",
                url: "../../classes/ajax.php",
                data: {stickyNotes: $('#stickyNotes').val(), action: 'moveToHistory', contactId: '<?php echo $contactId;?>', noteId: notesId },
                success: function (msg) {
                    $('#stickyNotes').val('');
                    $('.appendnotes tr:last').after(msg);
                }
            });
        }
    }
    function editTask(editType) {
        var taskId = $("#taskId").val();
        var cid =<?php echo $contactId;?>;
        window.location.href = '../task/edit_task.php?edit_type=' + editType + '&task_id=' + taskId + '&contactId=' + cid;
    }
    function archiveContact(contactId) {
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {action: 'archiveContact', contact_id: contactId},
            success: function (msg) {
                window.location.href = "view_all_contacts.php?archive=success";
            }
        });
    }
    function markTaskCompleted(taskId) {
        var userId =<?php echo $userId; ?>;
        var contactId =<?php echo $contactId; ?>;
        $.ajax({
            type: "POST",
            url: "../../classes/ajax.php",
            data: {taskId: taskId, userId: userId, contactId: contactId, action: 'updateTaskAgainstContact'},
            success: function (data) {
                if (data == 1) {
                    $("#task" + taskId).fadeOut('500');
                }
            }
        });
    }
    function sendPermission(contactId) {
        $.ajax({ url: '../../classes/ajax.php',
            data: {action: 'sendPermissionToBlock', contactId: contactId},
            type: 'post',
            success: function (output) {
                loadPermissionManager();
                $("#to").val(output);
            }
        });
        $("#contactId").val(contactId);
    }
    function permissionMail() {
        console.log("In Permission mail function");
        var to = $("#to").val();
        var from = $("#from").val();
        var subject = $("#subject").val();
        var emailbody = $("#body").val();
        var userId = $("#userId").val();
        var contactId = $("#contactId").val();
        var emailId = $("#emailId").val();
        var emailflag = $("#emailflag").val();
        $.ajax({ url: '../../classes/ajax.php',
            data: {action: 'sendPermissionMail', to: to, from: from, subject: subject, emailbody: emailbody, userId: userId, contactId: contactId, emailId: emailId, emailflag: emailflag},
            type: 'post',
            success: function (output) {
                console.log('output is ' + output);
                if (output == 1) {
                    disablepermissionPopup();
                }
                $("#messageofsuccess").html('Thank you for confirming your Email Address and approving future Emails');
            }

        });
    }
    function blockAlert() {
        editRecur(0);
    }
    </script>
    <!--  <div id="menu_line"></div>-->
<div class="container">
    <div class="top_content">
        <h1 class="gray">Contact Detail</h1>
    </div>
    <?php
    if (!empty($ContactDetail)) {
        ?>
        <a class="editContac" style="float: right;text-decoration: none; font-size: 14px;"
           href="importDataDetail.php?contactId=<?php echo $contactId; ?>" target="_blank"> Import Contact
            Details</a>
    <?php
    }
    ?>
    <div class="sub_container">
        <div class="navcontainer" id="navcontainer">
            <ul>
                <!--				<li>-->
                <!--					<a style="border-top-left-radius:10px;" class="current" id="tab1" href="storeproc.php?action=view&contactId=-->
                <?php //echo $contactId; ?><!--&profileId=guestProfile&userId=-->
                <?php //echo $userId; ?><!--">Guest Profile</a>-->
                <!--				</li>-->
                <!--				<img src="../../images/sprater1.png">-->
                <!--				<li><a id="tab3" href="storeproc.php?action=view&contactId=-->
                <?php //echo $contactId; ?><!--&profileId=skinCareProfile&userId=-->
                <?php //echo $userId; ?><!--">Skin Profile</a></li>-->
                <!--				<img src="../../images/sprater1.png">-->
                <!--				<li><a id="tab4" href="storeproc.php?action=view&contactId=-->
                <?php //echo $contactId; ?><!--&profileId=fitProfile&userId=-->
                <?php //echo $userId; ?><!--">Fit Profile</a></li>-->
                <!--				<img src="../../images/sprater1.png">-->
                <!--				<li>-->
                <!--					<a style="padding:12px 9px 10px 11px; height:28px;" id="tab5" href="storeproc.php?action=view&contactId=-->
                <?php //echo $contactId; ?><!--&profileId=consultantProfile&userId=-->
                <?php //echo $userId; ?><!--">Consultant Profile</a>-->
                <!--				</li>-->
                <!--- custom profile -->
                <?php
                $count = 0;
                foreach ($contactProfiles as $cp) {
                    $count++;
                    $loadFirstParam = "&loadFirst=0";
                    ?>
                    <li>
                        <a style="<?php if ($count == 1) {
                            $loadFirstParam = "&loadFirst=1";
                            echo 'border-top-left-radius:10px;';
                        } ?> padding:12px 9px 10px 11px; height:28px;" id="tab6"
                           href="storeproc.php?action=view&contactId=<?php echo $contactId; ?>&newCode=<?php echo $cp->newCode; ?>&profileId=<?php echo $cp->pageId; ?>&userId=<?php echo $userId; ?>&generatedVia=<?php echo $cp->generatedVia . $loadFirstParam; ?>"><?php echo $cp->pageBrowserTitle; ?></a>
                    </li>
                    <img src="../../images/sprater1.png">
                <?php } ?>

                <!--- custom profile li end -->
                <!--
                <img  src="../../images/sprater1.png" >
                <li><a style="border-bottom-left-radius:10px;" id="tab6" href="tabs_contact_detail.php?id=6">All</a></li>
                -->
            </ul>
        </div>
        <!--<div class="navcontainer" id="nav_container" >
                        <ul>
                            <li><a style="border-top-left-radius:10px;" id="tab1" href="tabs_contact_detail-.php?id=1">Guest Profile</a></li><img  src="images/sprater1.png" >

                            <li><a id="tab3" href="tabs_contact_detail-.php?id=3">Skin Profile</a></li><img  src="images/sprater1.png" >
                            <li><a id="tab4" href="tabs_contact_detail-.php?id=4">Fit Profile</a></li><img  src="images/sprater1.png" >
                            <li><a style="padding:12px 9px 10px 11px; height:28px;" id="tab5" href="tabs_contact_detail-.php?id=5">Consultant Profile</a></li><img  src="images/sprater1.png" >
                            <li><a style="border-bottom-left-radius:10px;" id="tab6" href="tabs_contact_detail-.php?id=6">All</a></li>


                        </ul>
                    </div>-->
        <div id="backgroundEditContact"></div>
        <div id="preloader">
            <img src="../../images/loading.gif" align="absmiddle">
        </div>
        <div id="content">
        </div>
    </div>
    <div id="Popup"></div>
    <div id="popupReoccur">
        <h1 class="gray">Changing recurring task</h1>
        <input type="hidden" name="taskId" id="taskId"/>
        <span class="popupEditTask">You're changing a recurring task. Do you want to change only this task, or all future tasks?</span>
        <br/><br/>
        <span><input type="button" value="" class="onlyThisTask" onclick="editTask('1')"/> <input type="button" value=""
                                                                                                  onclick="editTask('0')"
                                                                                                  class="allFutureTask"/><input
                type="button" name="cancel" class="cancel" value="" onClick="disablereoccurPopup()"/></span>
    </div>
    <?php
    $to = $contact_details->showContactEmailAddress($database, $contactId);
    $from = $contact_details->GetEmail($database, $userId);
    ?>
    <div id="Popup"></div>
    <div id="popuppermission">
        <h1 class="gray">Email Permission Confirmation</h1>

        <form action="?action=success" method="post">
            <input type="hidden" name="contactId" id="contactId"/>
            <input type="hidden" name="emailId" id="emailId"/>
            <input type="hidden" name="emailflag" id="emailflag"/>
            <input type="hidden" name="userId" id="userId" value="<?php echo $userId; ?>"/>
            <table width="100%" border="0">
                <tr>
                    <td class="label">From</td>
                    <td><input type="text" name="from" id="from" class="textfieldbig" value="<?php echo $from; ?>"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="label">To</td>
                    <td><input type="text" name="to" id="to" class="textfieldbig" value="<?php echo $to->email; ?>"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="label">Subject</td>
                    <?php $subject = $getPermissionEmail->permissionEmailSubject;
                    $subject = str_replace("[{First Name}]", "$user->firstName", "$subject"); ?>
                    <td><input type="text" name="subject" id="subject" class="textfieldbig"
                               value="<?php echo $subject; ?>"/></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="padding-left:12px;"><textarea name="area2" id="body"
                                                             style="width:648px; height:200px;  ">
                        </textarea>

                        <div
                            style="background-color:#EAEAEA;width:640px; padding:5px; padding-bottom:0px; font-size:12px;height: 155px; overflow: auto; display: block;margin-bottom:1px;">
                            <p style="font-size:12px; width: 100%;">
                                <?php if ($leaderId == 0) {
                                    $consultantTitleUserId = $userId;
                                } else {
                                    $consultantTitleUserId = $leaderId;
                                }
                                $consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
                                //

                                $AcceptLink = "https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013";
                                $declineLink = "https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u";

                                $body = $getPermissionEmail->permissionEmailBody;

                                $body = str_replace("[{Accept Emails Link}]", "$AcceptLink", "$body");
                                $body = str_replace("[{DeclineEmails Link}]", "$declineLink", "$body");
                                $body = str_replace("[{First Name}]", "$user->firstName", "$body");
                                //
                                echo $body;
                                ?>

                            </p>
                        </div>

                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="button" name="submit" class="sendpermissionconfirmation" value=""
                               onclick="permissionMail();"/><input type="button" name="cancel" class="cancel"
                                                                   onclick="disablepermissionPopup();"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>
<?php include_once("../headerFooter/footer.php"); ?>