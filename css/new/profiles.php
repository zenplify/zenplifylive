<?php
error_reporting(1);
include_once("class.smtpmail.php");

$userId = $_SESSION['userId'];

class Profiles
{

    public function PageView($database, $pageId, $url, $code, $userId, $browserInfo)
    {
        $ip = self::get_client_ip();
        $database->executeNonQuery("INSERT INTO pageviews(pageId, IpAddress, viewDate, userId, pageURL, uniqueCode, browserInfo) VALUES ('" . $pageId . "','" . $ip . "', NOW(), '" . $userId . "', '" . $url . "', '" . $code . "', '" . $browserInfo . "')");
        $viewId = $database->insertid();

        return $viewId;
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    function curPageURL()
    {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        return $pageURL;
    }

    public function getUserDetails($database, $uerId)
    {
        $details = $database->executeObject("Select * from user where userId='" . $_SESSION['userId'] . "'");

        return $details;
    }

    function getUserDetailsfromNewCode($database, $code, $username)
    {
        return $database->executeObject("Select username, email, firstName, consultantId, webAddress, facebookAddress, pageId,  p.generatedVia, p.pageId, p.newCode, p.oldId from user u join pages p on u.userId=p.userId where newCode='" . $code . "' and u.username = '" . $username . "' and p.`status`='1'");
    }

    public function getUserDetailsfromCode($database, $code)
    {
        return $database->executeObject("Select username, email, firstName, consultantId, webAddress, facebookAddress, pageId, generatedVia, p.newCode from user u join
		pages p on  u.userId=p.userId where uniqueCode='" . $code . "' ");
    }

    public function getAllProfiles($database, $userId)
    {

        return $database->executeObjectList("SELECT p.*,pt.thumbnail,u.username FROM pages p LEFT JOIN pagetemplate pt ON p.pageTemplateId=pt.pageTemplateId LEFT JOIN `user` u ON p.userId=u.userId WHERE p.userId='" . $userId . "'
AND ((p.status = 1  AND p.isComplete= 0) OR (p.status =1 AND p.`isComplete` =1)) ORDER BY p.order ASC");

        /*return $database->executeObjectList("select p.*,pt.thumbnail,u.username from pages p left join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId LEFT JOIN user u ON p.userId=u.userId where p.userId='" . $userId . "' and (p.status = 1 or p.isComplete= 0) ORDER BY p.order ASC");*/
    }

    public function getUserPageDetails($database, $code, $user)
    {
        $sql = "select u.username, u.userId,u.leaderId AS userLeaderId, u.firstName, u.lastName, u.phoneNumber, u.webAddress,
            u.facebookAddress, u.consultantId, u.title, u.email, u.facebookPersonalAddress, u.facebookPartyAddress, p.*, pt.templateHTML from `user` u
		join pages p on p.userId=u.userId
		join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId where p.uniqueCode='" . $code . "' and p.`status`='1'";
        if (!empty($user)) {
            $sql = "select u.username, u.userId,u.leaderId AS userLeaderId, u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.email, u.facebookPersonalAddress, u.facebookPartyAddress, p.*, pt.templateHTML from user u
		join pages p on p.userId=u.userId
		join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId where p.newCode='" . $code . "' and u.username = '" . $user . "' and p.`status`='1'";
        }
        $userDetails = $database->executeObject($sql);

        return $userDetails;
    }

    public function getUserPageName($database, $code, $user)
    {
        $sql = "select pageName FROM pages where uniqueCode='" . $code . "'";
        if (!empty($user)) {
            $sql = "SELECT p.pageName FROM pages p ,`user` u WHERE p.newCode = '" . $code . "' AND p.`userId` = u.userId AND u.userName = '" . $user . "'";
        }
        //$pageName=$database->executeScalar("select pageName FROM pages where uniqueCode='".$code."'");
        $pageName = $database->executeScalar($sql);

        return $pageName;
    }

    public function getPageQuestionAnswers($database, $pageId, $replyId)
    {
        $questions = $database->executeObjectList("SELECT distinct Q.questionId, Q.typeId,Q.question, RA.answer, RA.value, RA.answerText,QC.choiceText from pageformreplyanswers RA join questions Q on RA.questionId = Q.questionId left join pageformquestionchoices QC on RA.answer = QC.choiceId JOIN pagequestions pq ON Q.`questionId` = pq.`questionId` where RA.replyId ='" . $replyId . "' AND pq.`pageId` = '" . $pageId . "' AND pq.`status` = 1 order by pq.order, Q.order ASC, QC.order ASC");

        return $questions;
    }

    public function getPageQuestionAnswersEdit($database, $pageId, $replyId)
    {

        $questions = $database->executeObjectList("SELECT distinct Q.questionId, RA.replyanswerId, Q.typeId,Q.question,QC.choiceId, QC.choiceText,RA.answer, RA.value, RA.answerText from  questions Q join pagequestions pq on Q.questionId= pq.questionId left join pageformquestionchoices QC  on Q.questionId=QC.questionId  left join pageformreplyanswers RA on RA.questionId = Q.questionId   where pq.pageId='" . $pageId . "' and replyId='" . $replyId . "' order by pq.`order`, Q.order ASC, QC.order ASC");

//
//			$questions = $database->executeObjectList("SELECT distinct Q.questionId, Q.typeId,Q.question, RA.answer, RA.value, RA.answerText,QC.choiceText from pageformreplyanswers RA join questions Q on RA.questionId = Q.questionId
// left join pageformquestionchoices QC on RA.answer = QC.choiceId where RA.replyId ='" . $replyId . "' order by Q.order ASC, QC.order ASC");

        return $questions;
//			$questions = $database->executeObjectList("SELECT distinct Q.questionId, RA.replyanswerId, Q.typeId,Q.question,QC.choiceId, QC.choiceText,RA.answer, RA.value, RA.answerText from  questions Q join pagequestions pq on Q.questionId= pq.questionId left join pageformquestionchoices QC  on Q.questionId=QC.questionId  left join pageformreplyanswers RA on RA.questionId = Q.questionId   where pq.pageId='" . $pageId . "' and replyId='" . $replyId . "'
//
// order by Q.order ASC, QC.order ASC");
//
//			return $questions;
    }

    public function getQuestionChoices($database, $questionId)
    {
        $choices = $database->executeObjectList(" SELECT * from pageformquestionchoices  pfq where questionId='" . $questionId . "' ORDER BY pfq.order");

        return $choices;
    }

    public function generateContact($database, $pageId, $userId, $firstName, $lastName, $email, $phone, $viewId, $pageurl, $code, $pagename)
    {

        $contacts = $database->executeObject("Select c.contactId,c.profiles FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE (ec.email='" . $email . "' OR c.email='" . $email . "') and c.userId='" . $userId . "'");
        $contactId = $contacts->contactId;
        $profiles = $contacts->profiles;
        $match_result = strpos($profiles, strtoupper(substr($pagename, 0, 1)));
        if ($match_result === false) {
            if (empty($profiles)) {
                $profiles = strtoupper(substr($pagename, 0, 1));
            } else {
                $profiles = $profiles . ',' . strtoupper(substr($pagename, 0, 1));
            }
        }

        if (empty($contactId)) {
            $database->executeNonQuery("INSERT INTO contact
SET
userId='" . $userId . "',
firstName='" . mysql_real_escape_string($firstName) . "',
lastName='" . mysql_real_escape_string($lastName) . "',
addedDate=now(),
isActive=1,
isGenerated=1,
email='" . $email . "',
profiles = UPPER(left('" . $pagename . "',1)),
phoneNumber='" . $phone . "'");
            $contactId = $database->insertid();
            $database->executeNonQuery("INSERT INTO emailcontacts
SET
contactId='" . $contactId . "',
email='" . $email . "',
addedDate=CURDATE()");
            $emailId = $database->insertid();
            $database->executeNonQuery("INSERT INTO contactphonenumbers
SET contactId='" . $contactId . "',
typeId=0,
phoneNumber='" . $phone . "',
isPrimary=0,
addedDate=CURDATE()");
        } else {
            $database->executeNonQuery("UPDATE contact
SET
firstName='" . mysql_real_escape_string($firstName) . "',
lastName='" . mysql_real_escape_string($lastName) . "',
isActive=1,
email='" . $email . "',
phoneNumber='" . $phone . "',
profiles = UPPER('" . $profiles . "')
WHERE contactId='" . $contactId . "'");
            $database->executeNonQuery("UPDATE contactphonenumbers SET  phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "'");
            $emailId = $database->executeScalar("SELECT emailId FROM emailcontacts WHERE contactId='" . $contactId . "'");
        }
        $database->executeNonQuery("INSERT INTO pageformreply
											SET
											pageId='" . $pageId . "',
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											email='" . $email . "',
											phoneNumber='" . $phone . "',
											isContactGenerated=1,
											contactId='" . $contactId . "',
											addDate=NOW(),
											userId='" . $userId . "',
											viewId='" . $viewId . "',
											pageURL='" . $pageurl . "',
											uniqueCode='" . $code . "'");
        $replyId = $database->insertid();
        $results = array("contactId" => $contactId, "replyId" => $replyId, "emailId" => $emailId);

        return $results;
    }

    public function generateContactCustom($database, $pageId, $userId, $firstName, $lastName, $email, $viewId, $pageurl, $code, $pagename)
    {

        $contacts = $database->executeObject("Select c.contactId,c.profiles FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE (ec.email='" . $email . "' OR c.email='" . $email . "') and c.userId='" . $userId . "'");
        $contactId = $contacts->contactId;
        $profiles = $contacts->profiles;
        $match_result = strpos($profiles, strtoupper(substr($pagename, 0, 1)));
        if ($match_result === false) {
            if (empty($profiles)) {
                $profiles = strtoupper(substr($pagename, 0, 1));
            } else {
                $profiles = $profiles . ',' . strtoupper(substr($pagename, 0, 1));
            }
        }

        if (empty($contactId)) {
            $database->executeNonQuery("INSERT INTO contact
											SET
											userId='" . $userId . "',
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											addedDate=now(),
											isActive=1,
											isGenerated=1,
											email='" . $email . "',
											profiles = UPPER(left('" . $pagename . "',1))");
            $contactId = $database->insertid();
            $database->executeNonQuery("INSERT INTO emailcontacts
											SET
											contactId='" . $contactId . "',
											email='" . $email . "',
											addedDate=CURDATE()");
            $emailId = $database->insertid();
        } else {
            $database->executeNonQuery("UPDATE contact
											SET
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											isActive=1,
											email='" . $email . "',
											profiles = UPPER('" . $profiles . "')
											WHERE contactId='" . $contactId . "'");
            $emailId = $database->executeScalar("SELECT emailId FROM emailcontacts WHERE contactId='" . $contactId . "'");
        }
        $database->executeNonQuery("INSERT INTO pageformreply
											SET
											pageId='" . $pageId . "',
											firstName='" . mysql_real_escape_string($firstName) . "',
											lastName='" . mysql_real_escape_string($lastName) . "',
											email='" . $email . "',
											phoneNumber=' ',
											isContactGenerated=1,
											contactId='" . $contactId . "',
											addDate=NOW(),
											userId='" . $userId . "',
											viewId='" . $viewId . "',
											pageURL='" . $pageurl . "',
											uniqueCode='" . $code . "'");
        $replyId = $database->insertid();
        $results = array("contactId" => $contactId, "replyId" => $replyId, "emailId" => $emailId);

        return $results;
    }

    public function addMailingAddressCustom($database, $replyId, $contactId, $userId, $birthdate, $phone, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country, $basicInfoQuestions)
    {

        $bestTime = implode(", ", $bestTimeToCall);
        $jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street . ',+' . $city . ',+' . $state . ',+' . $zipCode . ',+' . $country) . '&sensor=true');
        $obj = json_decode($jsondata, true);
        $lat = $obj['results'][0]['geometry']['location']['lat'];
        $long = $obj['results'][0]['geometry']['location']['lng'];
        $jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');
        $obj2 = json_decode($jsondata2, true);
        $offset = $obj2['rawOffset'];
        $gmtDifference = ($offset / 60) / 60;
        $birthdate = date('Y-m-d', strtotime($birthdate));

        $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
        if (in_array('Weekdays', $bestTimeToCall))
            $weekdays = 'Weekdays';
        if (in_array('Weekday Evenings', $bestTimeToCall))
            $weekdaysEvening = 'Weekday Evenings';
        if (in_array('Weekend Days', $bestTimeToCall))
            $weekends = 'Weekend Days';
        if (in_array('Weekend Evenings', $bestTimeToCall))
            $weekendsEvening = 'Weekend Evenings';

        $isDateofbirth = 0;
        $isReferredBy = 0;
        $isStreet = 0;
        $isState = 0;
        $isCity = 0;
        $isZipcode = 0;
        $isCountry = 0;
        $isBestTimeToReach = 0;
        $isPageFormReply = 0;

        $updateParamContact = '';
        $updateParamPageFormReply = '';
        $updateParamAddress = '';

        foreach ($basicInfoQuestions as $biq) {
            $label = trim($biq->label);
            if (strpos($label, 'Best Times') !== false) {
                $label = 'Best Times To Reach You';
            }
            switch ($label) {
                case 'Phone number':
                    $updateParamContact .= "phoneNumber = '" . $phone . "',";
                    break;
                case 'DOB':
                    $isDateofbirth = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "dateOfBirth='" . $birthdate . "',";
                    $updateParamPageFormReply .= "dateOfBirth='" . $birthdate . "',";
                    break;
                case
                'Referred By':
                    $isReferredBy = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "referredBy = '" . $referredBy . "',";
                    $updateParamPageFormReply .= "refferedBy='" . mysql_real_escape_string($referredBy) . "',";
                    break;
                case 'Street Address':
                    $isStreet = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "street = '" . mysql_real_escape_string($street) . "',";
                    $updateParamPageFormReply .= "address='" . mysql_real_escape_string($street) . "',";
                    $updateParamAddress .= "street='" . mysql_real_escape_string($street) . "',";
                    break;
                case 'City':
                    $isCity = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "city = '" . mysql_real_escape_string($city) . "',";
                    $updateParamPageFormReply .= "city='" . mysql_real_escape_string($city) . "',";
                    $updateParamAddress .= "city='" . mysql_real_escape_string($city) . "',";
                    break;
                case 'State/Prov':
                    $isState = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "state = '" . mysql_real_escape_string($state) . "',";
                    $updateParamPageFormReply .= "state='" . mysql_real_escape_string($state) . "',";
                    $updateParamAddress .= "state='" . mysql_real_escape_string($state) . "',";
                    break;
                case 'Zip/Postal':
                    $isZipcode = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "zipcode = '" . $zipCode . "',";
                    $updateParamPageFormReply .= "zipCode='" . $zipCode . "',";
                    $updateParamAddress .= "zipcode='" . $zipCode . "',";
                    break;
                case 'Country':
                    $isCountry = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "country = '" . $country . "',";
                    $updateParamContact .= "gmt = '" . $gmtDifference . "',";
                    $updateParamPageFormReply .= "country='" . $country . "',";
                    $updateParamAddress .= "country='" . $country . "',";
                    break;
                case 'Best Times To Reach You':
                    $isBestTimeToReach = 1;
                    $isPageFormReply = 1;
                    $updateParamContact .= "weekdays = '" . $weekdays . "',";
                    $updateParamContact .= "weekdayEvening = '" . $weekdaysEvening . "',";
                    $updateParamContact .= "weekenddays = '" . $weekends . "',";
                    $updateParamContact .= "weekenddayEvening = '" . $weekendsEvening . "',";
                    $updateParamPageFormReply .= "bestTimeToCall='" . $bestTime . "',";
                    break;
            }
        }

        $updateParamContact = substr($updateParamContact, 0, strlen($updateParamContact) - 1);
        $updateParamPageFormReply = substr($updateParamPageFormReply, 0, strlen($updateParamPageFormReply) - 1);
        $updateParamAddress = substr($updateParamAddress, 0, strlen($updateParamAddress) - 1);

        if ($isPageFormReply == 1) {
            $database->executeNonQuery("UPDATE pageformreply SET " . $updateParamPageFormReply . " WHERE replyId = '" . $replyId . "'");
        }
        if ($isCountry == 1) {
            $database->executeNonQuery("UPDATE contactaddresses SET " . $updateParamAddress . " WHERE contactId = '" . $contactId . "'");
        }
        $addressId = $database->getAffectedRows();

        if (!$addressId || empty($addressId)) {

            $database->executeNonQuery("INSERT INTO contactaddresses SET
											contactId = '" . $contactId . "',
											" . $updateParamAddress . " ,
											gmt='" . $gmtDifference . "',
											addedDate=CURDATE()");
        }

        if (!empty($updateParamContact)) {
            $database->executeNonQuery("UPDATE contact SET " . $updateParamContact . " WHERE contactId = '" . $contactId . "'");
        }
        $database->executeNonQuery("UPDATE contactcalltime SET
										weekdays = '" . $weekdays . "',
										weekdayEvening = '" . $weekdaysEvening . "',
										weekenddays = '" . $weekends . "',
										weekenddayEvening = '" . $weekendsEvening . "',
										addedDate = CURDATE()
										WHERE contactId = '" . $contactId . "'");

        $rowId = $database->getAffectedRows();

//			$database->executeNonQuery("UPDATE contact SET
//										weekdays = '" . $weekdays . "',
//										weekdayEvening = '" . $weekdaysEvening . "',
//										weekenddays = '" . $weekends . "',
//										weekenddayEvening = '" . $weekendsEvening . "',
//										addedDate = CURDATE()
//										WHERE contactId = '" . $contactId . "'");

        $rowId = $database->getAffectedRows();

        if (!$rowId || empty($rowId)) {
            $database->executeNonQuery("INSERT INTO contactcalltime SET
										contactId = '" . $contactId . "',
										weekdays = '" . $weekdays . "',
										weekdayEvening = '" . $weekdaysEvening . "',
										weekenddays = '" . $weekends . "',
										weekenddayEvening = '" . $weekendsEvening . "',
										addedDate = CURDATE()");
        }

        return 1;
    }

    public function addMailingAddress($database, $replyId, $contactId, $userId, $birthdate, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country)
    {
        $bestTime = implode(", ", $bestTimeToCall);
        $jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street . ',+' . $city . ',+' . $state . ',+' . $zipCode . ',+' . $country) . '&sensor=true');
        $obj = json_decode($jsondata, true);
        $lat = $obj['results'][0]['geometry']['location']['lat'];
        $long = $obj['results'][0]['geometry']['location']['lng'];
        $jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');
        $obj2 = json_decode($jsondata2, true);
        $offset = $obj2['rawOffset'];
        $gmtDifference = ($offset / 60) / 60;
        $birthdate = date('Y-m-d', strtotime($birthdate));
        $database->executeNonQuery("UPDATE pageformreply
SET

dateOfBirth='" . $birthdate . "',
refferedBy='" . mysql_real_escape_string($referredBy) . "',
bestTimeToCall='" . $bestTime . "',
address='" . mysql_real_escape_string($street) . "',
city='" . mysql_real_escape_string($city) . "',
zipCode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
country='" . $country . "'
WHERE replyId='" . $replyId . "'");
        $database->executeNonQuery("UPDATE contactaddresses
SET
street='" . mysql_real_escape_string($street) . "',
zipcode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
city='" . mysql_real_escape_string($city) . "',
country='" . $country . "'
WHERE contactId='" . $contactId . "'");
        $addressId = $database->getAffectedRows();
        if (!$addressId || empty($addressId)) {
            $database->executeNonQuery("INSERT INTO contactaddresses SET
contactId='" . $contactId . "',
street='" . mysql_real_escape_string($street) . "',
zipcode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
city='" . mysql_real_escape_string($city) . "',
country='" . $country . "',gmt='" . $gmtDifference . "', addedDate=CURDATE()");
        }
        $database->executeNonQuery("UPDATE contact
SET
dateOfBirth='" . $birthdate . "',
referredBy='" . mysql_real_escape_string($referredBy) . "',
street='" . mysql_real_escape_string($street) . "',
zipcode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
city='" . mysql_real_escape_string($city) . "',
country='" . $country . "',
gmt='" . $gmtDifference . "'
WHERE contactId='" . $contactId . "'");
        $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
        if (in_array('Weekdays', $bestTimeToCall))
            $weekdays = 'Weekdays';
        if (in_array('Weekday Evenings', $bestTimeToCall))
            $weekdaysEvening = 'Weekday Evenings';
        if (in_array('Weekend Days', $bestTimeToCall))
            $weekends = 'Weekend Days';
        if (in_array('Weekend Evenings', $bestTimeToCall))
            $weekendsEvening = 'Weekend Evenings';
        $database->executeNonQuery("UPDATE contactcalltime SET
weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
        $rowId = $database->getAffectedRows();
        $database->executeNonQuery("UPDATE contact SET
weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
        $rowId = $database->getAffectedRows();
        if (!$rowId || empty($rowId)) {
            $database->executeNonQuery("INSERT INTO contactcalltime SET
contactId='" . $contactId . "', weekdays='" . $weekdays . "',
weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "',
weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE()");
        }

        return 1;
    }

    public function addToGroup($database, $contactId, $group)
    {
        $userId = $database->executeScalar("select userId from contact where contactId = '" . $contactId . "'");
        $registrationDate = $database->executeScalar("select registrationDate from `user` where userId = '" . $userId . "'");

        if ($registrationDate > registrationDate) {
            $groupId = $database->executeScalar("SELECT g.groupId FROM groupusers gu JOIN groups g ON g.`groupId` = gu.groupId
WHERE (LOWER(g.name)=LOWER('" . $group . "') or gu.groupId = '" . $group . "') and  gu.userId = '" . $userId . "' AND gu.isactive = 1 AND gu.isArchive = 0");
        } else {
            $groupId = $database->executeScalar("select groupId from groups WHERE (LOWER(name)=LOWER('" . $group . "') OR groupId='" . $group . "') and userId = 0 ");
        }

        $contact = $database->executeObjectList("select contactId from groupcontacts where groupId='" . $groupId . "' and contactId='" . $contactId . "' ");
        if (empty($contact)) {
            $database->executeNonQuery("INSERT INTO groupcontacts(groupId, contactId) values ('" . $groupId . "','" . $contactId . "')");

//				$database->executeNonQuery("INSERT INTO groupcontacts(groupId, contactId)  (select groupId, '" . $contactId . "' as contactId from groups WHERE (LOWER(name)=LOWER('" . $group . "') OR groupId='" . $group . "'))");
            //$groupId=$database->insertid();
        }

        return $groupId;
    }

    public function addToGroupbyGroupId($database, $contactId, $groupId)
    {
        $contact = $database->executeObjectList("select contactId from groupcontacts where groupId='" . $groupId . "' and contactId='" . $contactId . "' ");
        if (empty($contact)) {
            $database->executeNonQuery("INSERT INTO groupcontacts(groupId, contactId) values ('" . $groupId . "','" . $contactId . "')");

//				$database->executeNonQuery("INSERT INTO groupcontacts(groupId, contactId)  (select groupId, '" . $contactId . "' as contactId from groups WHERE (LOWER(name)=LOWER('" . $group . "') OR groupId='" . $group . "'))");
            //$groupId=$database->insertid();
        }

        return $groupId;
    }

    public function addToPlan($database, $contactId, $plan, $userId)
    {
        $date = date('Y-m-d');
        $date = strtotime($date);
        $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");
        $plan = $database->executeObject("SELECT * from plan WHERE title='" . $plan . "' and userId='" . $userId . "' AND isactive = 1 AND isArchive = 0");
        $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
        if (!empty($planId2)) {
            $database->executeNonQuery("delete from contactPlansCompleted where planId='" . $planId2 . "' and contactId='" . $contactId . "'");
        }
//			print_r($plan);
        $palnStepsExists = $database->executeObjectList("SELECT *
															FROM plan p
															JOIN plansteps ps ON p.planId = ps.planId
															JOIN planstepusers psu ON ps.stepId = psu.stepId
															WHERE p.planId ='" . $plan->planId . "'
															AND psu.contactId ='" . $contactId . "' AND p.isactive = 1 AND p.isArchive = 0");
        if ((empty($palnStepsExists)) && (empty($planId2))) {
            $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
            $i = 0;
            $totalSteps = sizeof($palnSteps);
            $stepCounter = 1;
            foreach ($palnSteps as $ps) {
//
                $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                $newStepId = $database->insertid();
                $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='" . $userId . "'");
                if ($ps->isTask == 0 && $PermissionStatus != '4') {
                    if ($ps->daysAfter == 0) {
//
                        $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' ORDER BY permissionRequestId DESC LIMIT 1");
                        if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {

                            $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                            $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                            preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                            foreach ($match[1] as $key) {
                                if ($key == "First Name") {
                                    $firstname = 'firstName';
                                }
                                if ($key == "Last Name") {
                                    $lastname = 'lastName';
                                }
                                if ($key == "Phone Number") {
                                    $phonenumber = 'phoneNumber';
                                }
                                if ($key == "Company Name") {
                                    $companyname = 'companyTitle';
                                }
                                if ($key == "@Signature") {
                                    $signature = self::getSignatures($database, $userId);

                                }
                            }
                            if (!empty($firstname)) {
                                $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                            }
                            if (!empty($lastname)) {
                                $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                            }
                            if (!empty($companyname)) {
                                $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                            }
                            if (!empty($phonenumber)) {
                                $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                            }

                            $file_name = $mailBody->body;
                            $subject = $mailBody->subject;
                            $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                            $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                            $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                            $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                            $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                            $message = str_replace("\'", "'", $rplbysig);
                            $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                "Reply-To: " . $userInfo->email . "\r\n";
                            $headers .= "Content-Type: text/html";
//
                            $mail = new smtpEmail();
                            $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id, $file_name);
                            //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                            $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
    ('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                            $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                            if ($totalSteps == $stepCounter) {
                                $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                            }
                        }
                    } else {

                        break;
                    }
                } else {

                    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_real_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                    $taskId = $database->insertid();
                    $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");

                    break;
                }
                $stepCounter++;
            }
        }

        return $plan->planId;
    }

    function CheckLastStepOfPlan($planId, $contactId)
    {
        $database = new database();

        return $database->executeScalar("select planId from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");
    }

    function getSignatures($database, $userId)
    {
        $sql = "SELECT `signature` FROM `usersignature` WHERE `userId` = '" . $userId . "'";
        return $database->executeScalar($sql);
    }

    public function getUserSignature($database, $userId)
    {

        $userDetails = $database->executeObject("select u.firstName,u.email, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress , u.consultantId, u.title, u.leaderId,u.userId from user u where u.userId='" . $userId . "'");

        return $userDetails;
    }

    function getconsultantTitle($database, $userId, $fieldMappingName)
    {

        return $consultantTitle = $database->executeObject("SELECT `fieldCustomName`,`status` FROM `leaderdefaultfields` WHERE userId='" . $userId . "' AND `fieldMappingName`='" . $fieldMappingName . "'");

    }

    function sendEmailToSubscribedContacts($database, $userId, $contactId)
    {

        $date = strtotime(date('Y-m-d'));
        $userInfo = $database->executeObject("SELECT * FROM `user` WHERE `userId`='" . $userId . "'");
        $totalPlansSteps = $database->executeObjectList("SELECT * FROM `plansteps` WHERE `stepId` IN (SELECT `stepId` FROM `planstepusers` WHERE `contactId`='" . $contactId . "')");

        foreach ($totalPlansSteps as $planStep) {

            $plansteps = $database->executeObjectList("SELECT * FROM `plansteps` WHERE `planId` = '" . $planStep->planId . "' AND order_no >= '" . $planStep->order_no . "' AND isArchive = '0' AND isactive = '1' ORDER BY order_no ASC");
            $totalSteps = count($plansteps);
            $stepCounter = 1;
            foreach ($plansteps as $step) {

                $dueDate = strtotime("+" . $step->daysAfter . " day", $date); //add days in date
                $database->executeNonQuery("INSERT INTO `planstepusers` SET `stepId` = '" . $step->stepId . "',`contactId`='" . $contactId . "',`stepStartDate`='" . date('Y-m-d') . "',`stepEndDate`='" . date('Y-m-d', $dueDate) . "'");

                if ($step->isTask == 0) {

                    if ($step->daysAfter == 0) {

                        $contactemail = $database->executeScalar("SELECT `email` FROM `contact` WHERE `contactId`='" . $contactId . "'");
                        $mailBody = $database->executeObject("SELECT * FROM `planstepemails` WHERE `id` = '" . $step->stepId . "'");
                        preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                        foreach ($match[1] as $key) {
                            if ($key == "First Name") {
                                $firstname = 'firstName';
                            }
                            if ($key == "Last Name") {
                                $lastname = 'lastName';
                            }
                            if ($key == "Phone Number") {
                                $phonenumber = 'phoneNumber';
                            }
                            if ($key == "Company Name") {
                                $companyname = 'companyTitle';
                            }
                            if ($key == "@Signature") {
                                $signature = self::getSignatures($database, $userId);
                            }
                        }

                        if (!empty($firstname)) {
                            $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                        }
                        if (!empty($lastname)) {
                            $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                        }
                        if (!empty($companyname)) {
                            $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                        }
                        if (!empty($phonenumber)) {
                            $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                        }

                        $subject = $mailBody->subject;
                        $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                        $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                        $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                        $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                        $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                        $message = str_replace("\'", "'", $rplbysig);
                        $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" . "Reply-To: " . $userInfo->email . "\r\n";
                        $headers .= "Content-Type: text/html";
                        $mail = new smtpEmail();
                        $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id);

                        $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES ('" . $step->planId . "', '" . $step->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                        $database->executeNonQuery("DELETE FROM `planstepusers` where stepId='" . $step->stepId . "' and contactId='" . $contactId . "'");
                        if ($totalSteps == $stepCounter) {
                            $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $step->stepId . "','" . $step->planId . "','" . $contactId . "',CURDATE())");
                        }
                    } else {
                        break;
                    }

                } else {
                    $found = false;
                    $oldSteps = $database->executeObjectList("SELECT taskId FROM `taskcontacts` WHERE contactId = '" . $contactId . "'");
                    foreach ($oldSteps as $oldStep) {
                        $record = $database->executeScalar("SELECT taskId FROM tasks WHERE taskId = '" . $oldStep->taskId . "' AND userId = '" . $userId . "' AND title = '" . mysql_real_escape_string($step->summary) . "'");
                        if ($record) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found == false) {
                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($step->summary) . "','0','','" . mysql_real_escape_string($step->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $step->planId . "','','0','1')");
                        $taskId = $database->insertid();
                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $step->planId . "',stepId='" . $step->stepId . "'");
                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                    }
                    break;
                }
                $stepCounter++;
            }

        }
        return true;
    }

    public function getPageViews($database, $pageId)
    {
        $views = $database->executeScalar("Select count(*) as totalViews FROM  pageviews WHERE pageId='" . $pageId . "' and isCounterSet=0");

        return $views;
    }

    public function getPageGeneratedContacts($database, $pageId)
    {
        $totalContacts = $database->executeScalar("Select count(*) as totalContacts FROM pageformreply WHERE pageId='" . $pageId . "' and isContactGenerated=1  and isCounterSet=0");

        return $totalContacts;
    }

    public function editProfileLink($database, $pageId, $linkcode, $userId)
    {
        /*			$uniqueCode=$database->executeScalar("select uniqueCode from pages where pageId='".$pageId."' ");
        $page=$database->executeScalar("select pageId from pages where lower(uniqueCode)='".strtolower(trim($linkcode))."' and pageId<>'".$pageId."' ");
        if(empty($page))
        {
        $database->executeNonQuery("UPDATE pages set uniqueCode='".trim($linkcode)."' WHERE  pageId='".$pageId."' ");
        $database->executeNonQuery("INSERT INTO profilelinklog( pageId, userId, oldCode, newCode, editDateTime) VALUES ('".$pageId."','".$userId."','".$uniqueCode."','".$linkcode."',NOW())");
        return 'done';
        }
        else
        {
        return 'not done';
        }
        */
        $uniqueCode = $database->executeScalar("select uniqueCode from pages where pageId='" . $pageId . "' ");
        $page = $database->executeScalar("select pageId from pages where lower(newCode)='" . strtolower(trim($linkcode)) . "' and pageId<>'" . $pageId . "' ");
        if (empty($page)) {
            $database->executeNonQuery("UPDATE pages set newCode='" . trim($linkcode) . "' WHERE  pageId='" . $pageId . "' ");
            $database->executeNonQuery("INSERT INTO profilelinklog( pageId, userId, oldCode, newCode, editDateTime) VALUES ('" . $pageId . "','" . $userId . "','" . $uniqueCode . "','" . $linkcode . "',NOW())");

            return 'done';
        } else {
            return 'not done';
        }
    }

    public function deleteProfileLink($database, $pageId)
    {
        $page = $database->executeNonQuery("DELETE FROM pages  WHERE  pageId='" . $pageId . "'");

        return 'done';
    }

    public function resetCounter($database, $pageId)
    {
        $page = $database->executeNonQuery("UPDATE pageformreply set isCounterSet=1  WHERE  pageId='" . $pageId . "'");
        $page = $database->executeNonQuery("UPDATE pageviews set isCounterSet=1  WHERE  pageId='" . $pageId . "'");

        return 'done';
    }

//		public function checkContactReplyAgainstProfile($database, $contactId, $profileName) {
//
//			$results = $database->executeObject("SELECT pfr.pageId, pfr.replyId, pfr.addDate from pages p join pageformreply pfr on p.pageId=pfr.pageId where pfr.contactId='" . $contactId . "' and p.pageName = '" . $profileName . "' ORDER BY replyId DESC LIMIT 1");
//			$res = array("pageId" => $results->pageId, "replyId" => $results->replyId, "addDate" => $results->addDate);
//
//			return $res;
//		}

    public function checkContactReplyAgainstProfile($database, $contactId, $profileName, $newCode)
    {

        $results = $database->executeObject("SELECT pfr.pageId, pfr.replyId, pfr.addDate from pages p join pageformreply pfr on p.pageId=pfr.pageId where pfr.contactId='" . $contactId . "' and p.pageName = '" . $profileName . "' and p.newCode = '" . $newCode . "' ORDER BY replyId DESC LIMIT 1");
        $res = array("pageId" => $results->pageId, "replyId" => $results->replyId, "addDate" => $results->addDate);

        return $res;
    }

    public function getContactAnswerAgainstQuestion($database, $replyId, $questionId, $questionType)
    {
        if ($questionType == '1' || $questionType == '2') {
            $answers = $database->executeObject("SELECT answer , value, answerText from  pageformreplyanswers  where replyId='" . $replyId . "' AND questionId='" . $questionId . "'");

            return $answers;
        } else {
            $answers = $database->executeObjectList("SELECT  pc.choiceText , pfr.answer, value, pfr.answerText from pageformreplyanswers pfr join pageformquestionchoices pc on pfr.answer=pc.choiceId  where replyId='" . $replyId . "' AND pfr.questionId='" . $questionId . "'");
            if (empty($answers))
                $answers = $database->executeObjectList("SELECT  answer, value, answerText from pageformreplyanswers  where replyId='" . $replyId . "' AND questionId='" . $questionId . "'");

            return $answers;
        }
    }

    public function getQuestionLabel($database, $questionId)
    {
        $lables = $database->executeObjectList("SELECT  label from questionradiolabels ql where ql.questionId='" . $questionId . "' ORDER BY ql.order");

        return $lables;
    }

    /*function assignPlanStep($userId,$contactId,$planSteps)
    {
    $database = new database();
    //today date
    $date =date('Y-m-d');
    $date = strtotime($date);

    $database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId='".$contactId."'");
    $database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId='".$contactId."'");

    $count=sizeof($planSteps['plan']);
    if($count!=0){
    $step=0;

    while($count>0){


    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='".$planSteps['planStepDropDown'][$step]."',contactId='".$contactId."'");

    $stepDesc=$database->executeObject("SELECT * FROM plansteps WHERE stepId='".$planSteps['planStepDropDown'][$step]."'");
    $dueDate = strtotime("+".$stepDesc->daysAfter." day", $date);//add days in date
    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId	."','".$stepDesc->summary."','0','','','".date('Y-m-d',$dueDate)."','0','".$date."','0','0','".$stepDesc->planId."','','0','1')");
    $taskId=$database->insertid();
    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='".$contactId."',taskId='".$taskId."'");
    $count--;
    $step++;
    }

    }

    }*/

    public function editContact($database, $userId, $contactId, $firstName, $lastName, $email, $phone)
    {
        //echo "Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='".$email."' and c.userId='".$userId."' and contactId<>'".$contactId."'";
        $contactId2 = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='" . $email . "' and c.userId='" . $userId . "' and c.contactId<>'" . $contactId . "'");
        if (empty($contactId2)) {
            $database->executeNonQuery("UPDATE emailcontacts
											SET
											email='" . $email . "' WHERE contactId='" . $contactId . "'");
            $database->executeNonQuery("UPDATE contact SET email='" . $email . "', phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "'");
        }
        $database->executeNonQuery("UPDATE contact
											SET
											firstName='" . $firstName . "',
											lastName='" . $lastName . "' WHERE contactId='" . $contactId . "'");
        $phoneId = $database->executeScalar("SELECT phoneNumberId from contactphonenumbers where contactId='" . $contactId . "'");
        if (empty($phoneId)) {
            $database->executeNonQuery("INSERT INTO contactphonenumbers SET contactId='" . $contactId . "', phoneNumber='" . $phone . "',  addedDate=CURDATE()");
        } else {
            $database->executeNonQuery("UPDATE contactphonenumbers SET  phoneNumber='" . $phone . "' WHERE contactId='" . $contactId . "' and phoneNumberId='" . $phoneId . "' ");
        }

        return $contactId;
    }

    public function editMailingAddress($database, $contactId, $userId, $title, $birthdate, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country, $company, $jobTitle)
    {
        $bestTime = implode(", ", $bestTimeToCall);
        if ($birthdate != '')
            $birthdate = date('Y-m-d', strtotime($birthdate));
        else
            $birthdate = '0000-00-00';
        $jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street . ',+' . $city . ',+' . $state . ',+' . $zipCode . '+,' . $country) . '&sensor=true');
        $obj = json_decode($jsondata, true);
        $lat = $obj['results'][0]['geometry']['location']['lat'];
        $long = $obj['results'][0]['geometry']['location']['lng'];
        $jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');
        $obj2 = json_decode($jsondata2, true);
        $offset = $obj2['rawOffset'];
        $gmtDifference = ($offset / 60) / 60;
        $database->executeNonQuery("UPDATE contactaddresses SET
street='" . mysql_real_escape_string($street) . "',
zipcode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
city='" . mysql_real_escape_string($city) . "',
country='" . $country . "',gmt='" . $gmtDifference . "'
WHERE contactId='" . $contactId . "'");
        $addressId = $database->getAffectedRows();
        if (!$addressId || empty($addressId)) {
            $database->executeNonQuery("INSERT INTO contactaddresses SET
contactId='" . $contactId . "',
street='" . mysql_real_escape_string($street) . "',
zipcode='" . $zipCode . "',
state='" . mysql_real_escape_string($state) . "',
city='" . mysql_real_escape_string($city) . "',
country='" . $country . "', gmt='" . $gmtDifference . "' ,addedDate=CURDATE()");
        }
        $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
        if (in_array('Weekdays', $bestTimeToCall))
            $weekdays = 'Weekdays';
        if (in_array('Weekday Evenings', $bestTimeToCall))
            $weekdaysEvening = 'Weekday Evenings';
        if (in_array('Weekend Days', $bestTimeToCall))
            $weekends = 'Weekend Days';
        if (in_array('Weekend Evenings', $bestTimeToCall))
            $weekendsEvening = 'Weekend Evenings';
        $database->executeNonQuery("UPDATE contact
											SET
											 title='" . $title . "',
											companyTitle='" . mysql_real_escape_string($company) . "',
											jobTitle='" . $jobTitle . "',
											 dateOfBirth='" . $birthdate . "',
											referredBy='" . $referredBy . "',
											street='" . mysql_real_escape_string($street) . "',
											zipcode='" . $zipCode . "',
											state='" . mysql_real_escape_string($state) . "',
											city='" . mysql_real_escape_string($city) . "',
											country='" . $country . "',gmt='" . $gmtDifference . "', weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "' WHERE contactId='" . $contactId . "'");
        $database->executeNonQuery("UPDATE contactcalltime SET
			weekdays='" . $weekdays . "', weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "', weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE() WHERE contactId='" . $contactId . "'");
        $rowId = $database->getAffectedRows();
        if (!$rowId || empty($rowId)) {
            $database->executeNonQuery("INSERT INTO contactcalltime SET
					contactId='" . $contactId . "', weekdays='" . $weekdays . "',
					weekdayEvening='" . $weekdaysEvening . "', weekenddays='" . $weekends . "',
					weekenddayEvening='" . $weekendsEvening . "', addedDate=CURDATE()");
        }

        return 1;
    }

    public function editMailingAddressCustom($database, $pageId, $contactId, $userId, $title, $birthdate, $referredBy, $bestTimeToCall, $street, $city, $state, $zipCode, $country, $company, $jobTitle, $basicInfoQuestions)
    {

        $bestTime = implode(", ", $bestTimeToCall);
        if ($birthdate != '')
            $birthdate = date('Y-m-d', strtotime($birthdate));
        else
            $birthdate = '0000-00-00';

        $jsondata = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($street . ',+' . $city . ',+' . $state . ',+' . $zipCode . '+,' . $country) . '&sensor=true');
        $obj = json_decode($jsondata, true);
        $lat = $obj['results'][0]['geometry']['location']['lat'];
        $long = $obj['results'][0]['geometry']['location']['lng'];

        $jsondata2 = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=1331161200&sensor=true');

        $obj2 = json_decode($jsondata2, true);
        $offset = $obj2['rawOffset'];
        $gmtDifference = ($offset / 60) / 60;

        $weekdays = $weekdaysEvening = $weekends = $weekendsEvening = '';
        if (in_array('Weekdays', $bestTimeToCall))
            $weekdays = 'Weekdays';
        if (in_array('Weekday Evenings', $bestTimeToCall))
            $weekdaysEvening = 'Weekday Evenings';
        if (in_array('Weekend Days', $bestTimeToCall))
            $weekends = 'Weekend Days';
        if (in_array('Weekend Evenings', $bestTimeToCall))
            $weekendsEvening = 'Weekend Evenings';

        $isDateofbirth = 0;
        $isReferredBy = 0;
        $isStreet = 0;
        $isState = 0;
        $isCity = 0;
        $isZipcode = 0;
        $isCountry = 0;
        $isBestTimeToReach = 0;
        $updateParam = '';

        foreach ($basicInfoQuestions as $biq) {
            $label = trim($biq->label);
            if (strpos($label, 'Best Times') !== false) {

                $label = 'Best Times To Reach You';
            }
            switch ($label) {
                case 'DOB':
                    $isDateofbirth = 1;
                    $updateParam .= "dateOfBirth='" . $birthdate . "',";
                    break;
                case
                'Referred By':
                    $isReferredBy = 1;
                    $updateParam .= "referredBy = '" . $referredBy . "',";
                    break;
                case 'Street Address':
                    $isStreet = 1;
                    $updateParam .= "street = '" . mysql_real_escape_string($street) . "',";
                    break;
                case 'City':
                    $isCity = 1;
                    $updateParam .= "city = '" . mysql_real_escape_string($city) . "',";
                    break;
                case 'State/Prov':
                    $isState = 1;
                    $updateParam .= "state = '" . mysql_real_escape_string($state) . "',";
                    break;
                case 'Zip/Postal':
                    $isZipcode = 1;
                    $updateParam .= "zipcode = '" . $zipCode . "',";
                    break;
                case 'Country':
                    $isCountry = 1;
                    $updateParam .= "country = '" . $country . "',";
                    $updateParam .= "gmt = '" . $gmtDifference . "',";
                    break;
                case 'Best Times To Reach You':
                    $isBestTimeToReach = 1;
                    $updateParam .= "weekdays = '" . $weekdays . "',";
                    $updateParam .= "weekdayEvening = '" . $weekdaysEvening . "',";
                    $updateParam .= "weekenddays = '" . $weekends . "',";
                    $updateParam .= "weekenddayEvening = '" . $weekendsEvening . "',";
                    break;
            }
        }

        $updateParam = substr($updateParam, 0, strlen($updateParam) - 1);
        $database->executeNonQuery("UPDATE contactaddresses SET
											street = '" . mysql_real_escape_string($street) . "',
											zipcode = '" . $zipCode . "',
											state = '" . mysql_real_escape_string($state) . "',
											city = '" . mysql_real_escape_string($city) . "',
											country = '" . $country . "',gmt = '" . $gmtDifference . "'
											WHERE contactId = '" . $contactId . "'");

        $addressId = $database->getAffectedRows();

        if (!$addressId || empty($addressId)) {
            $database->executeNonQuery("INSERT INTO contactaddresses SET
											contactId = '" . $contactId . "',
											street = '" . mysql_real_escape_string($street) . "',
											zipcode = '" . $zipCode . "',
											state = '" . mysql_real_escape_string($state) . "',
											city = '" . mysql_real_escape_string($city) . "',
											country = '" . $country . "',
											gmt = '" . $gmtDifference . "',
											addedDate = CURDATE()");
        }

        if (!empty($updateParam)) {
            $database->executeNonQuery("UPDATE contact
											SET
											" . $updateParam . "
											WHERE contactId = '" . $contactId . "'");
        }
        if (!empty($isBestTimeToReach)) {
            $database->executeNonQuery("UPDATE contactcalltime SET
											weekdays = '" . $weekdays . "',
											weekdayEvening = '" . $weekdaysEvening . "',
											weekenddays = '" . $weekends . "',
											weekenddayEvening = '" . $weekendsEvening . "',
											addedDate = CURDATE()
											WHERE contactId = '" . $contactId . "'");
        }
        $rowId = $database->getAffectedRows();
        if (!$rowId || empty($rowId)) {
            $database->executeNonQuery("INSERT INTO contactcalltime SET
											contactId = '" . $contactId . "',
											weekdays = '" . $weekdays . "',
											weekdayEvening = '" . $weekdaysEvening . "',
											weekenddays = '" . $weekends . "',
											weekenddayEvening = '" . $weekendsEvening . "',
											addedDate = CURDATE()");
        }

        return 1;
    }

    public function getPageQuestions($database, $pageId, $templateDefault = '-1', $status = 0, $oldProfile = 0)
    {
        $where = '';
        if ($status === "old") {
            if ($oldProfile == 1)
                $questionOrder = 'q';
            else
                $questionOrder = 'pq';

            return $database->executeObjectList("select q .* from questions q join pagequestions pq on q . questionId = pq . questionId where pageId = '" . $pageId . "' ORDER BY " . $questionOrder . " . `order` ASC");

        } elseif ($status == 1) {
            $where = ' and `q`.`status` = 1';
        }

        if ($templateDefault == '-1') {
            return $database->executeObjectList("select q .*, pq .*, pt . `typeTitle` from questions q join pagequestions pq on q . questionId = pq . questionId JOIN questiontype pt ON q . `typeId` = pt . `typeId` where pageId = '" . $pageId . "' AND `q` . `templateDefault` = 0 " . $where . " ORDER BY pq .`order` ASC");
        } else {
            if ($status == 1)
                $where .= ' and `pq`.`status` = 1';

            return $database->executeObjectList("select q .*, pq . pageId, pq .`questionId`, pq.`order`, pq .`mandatory`, pq .`enableStatus`, pt . `typeTitle`,pq . `status` AS defaultStatus from questions q join pagequestions pq on q . questionId = pq . questionId JOIN questiontype pt ON q . `typeId` = pt . `typeId` where pageId = '" . $pageId . "' AND `q` . `templateDefault` = '" . $templateDefault . "' " . $where . " ORDER BY pq.`order` ASC");
        }
    }

    function contactNotes($contactId)
    {
        $database = new database();

        return $database->executeObject("SELECT noteId, contactId, notes, addedDate, lastModifiedDate, isActive FROM contactnotes WHERE contactId = '" . $contactId . "' AND isActive = 1 ORDER BY noteId");
    }

    function assignPlanStep($userId, $contactId, $planSteps)
    {

//			echo '<br>in assignplanstep<br>';
        $database = new database();
        //today date
        $date = date('Y-m-d');
        $date = strtotime($date);
        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId = '" . $contactId . "'");
        $x = 0;
        $contactSteps = array();
        $newContactSteps = array();
        foreach ($existingSteps as $exists) {
            $contactSteps[$x] = $exists->stepId;
            $x++;
        }

        $userInfo = $database->executeObject("SELECT * FROM user WHERE userId = '" . $userId . "'");
        //echo 'TotalPlans';
        $count = sizeof($planSteps['plan']);
        //echo '<br />';
        if ($count != 0) {
            //echo 'Count: '.$count;
            $step = 0;
            $j = 0;
            $y = 0;
            foreach ($planSteps['plan'] as $planss) {
                $i = 0;
                /*echo 'Count: '.$count;
                echo '<br />';*/
                $plan = $database->executeObject("SELECT * FROM plan WHERE planId = '" . $planss . "' AND isactive = 1 AND isArchive = 0");
                /*if($planSteps['planStepDropDown'.$planss]!='' &&  $planSteps['planStepDropDown'.$planss]!='Completed')
                {*/
                if ($planSteps['planStepDropDown' . $planss] != 'Completed') {
                    /*echo $planss;
                    echo "<br />";*/
                    $firstStepOfPlan = $database->executeScalar("SELECT ps . stepId
						FROM plansteps ps
						WHERE  ps . planId = '" . $planss . "' and
					ps . order_no = (select min(order_no) from plansteps
						where planId = '" . $planss . "' and isactive = 1 AND isArchive = 0)
						and ps . isactive = 1 AND ps . isArchive = 0");
                    if ($planSteps['planStepDropDown' . $planss] == '') {
                        $firstStepOfPlan;
                    } else {
                        $firstStepOfPlan = $planSteps['planStepDropDown' . $planss];
                    }
                    // code added for update excute now 1st step if it has days after greater then zero and checked on edit contact
                    foreach ($planSteps['isCheckedPlan'] as $excuteNow) {
                        if ($planss == $excuteNow) {
                            if (!empty($firstStepOfPlan)) {
                                $exDate = $planSteps['executionDate' . $planss];
                                $executionDate = date("Y-m-d", strtotime($exDate));
                                $currentDate = date("Y-m-d");
                                if ($executionDate == $currentDate || $executionDate < $currentDate) {
                                    $ignoreDaysAfterStepId = $firstStepOfPlan;

                                } else if ($executionDate > $currentDate) {

                                    $stepExecutionNewDate = $executionDate;
                                }
                                $checkPlanAlreadyExist = $database->executeObjectList("SELECT stepId FROM planstepusers WHERE contactId='" . $contactId . "' and stepId='" . $firstStepOfPlan . "'");
                                if (!empty($checkPlanAlreadyExist)) {
                                    // update plan steps execution date if requested form contact detail page

                                    $currentDate = date("Y-m-d");
                                    if (!empty($stepExecutionNewDate) && $stepExecutionNewDate > $currentDate) {
                                        $database->executeNonQuery("UPDATE planstepusers SET `stepEndDate`='" . $stepExecutionNewDate . "'
											WHERE contactId = '" . $contactId . "' and stepId='" . $firstStepOfPlan . "'");
                                    }
                                    // end update execution date

                                }

                            }
                        }
                    }

// code added for update excute now 1st step if it has days after greater then zero and checked on edit contact
                    $palnStepsToSkip = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no < (select order_no from plansteps where stepId = '" . $firstStepOfPlan . "' and isactive = 1 AND isArchive = 0) AND planId = '" . $planss . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                    foreach ($palnStepsToSkip as $pss) {


                        $stepSkipId = $database->executeObject("SELECT ceh . stepId cehId , cts . stepId ctsId, t . stepId tId  from plansteps ps left  join contacttaskskipped cts on ps . planId = cts . planId and ps . stepId = cts . stepId and cts . contactId = '" . $contactId . "'
								left join contactemailhistory ceh on ps . stepId = ceh . stepId and ps . planId = ceh . planId and ceh . contactId = '" . $contactId . "'
								left join(SELECT tc . taskId, t . planId, tp . stepId, t . isActive, tc . contactId FROM taskcontacts tc
								inner join tasks t on tc . taskId = t . taskId
								inner join taskplan tp on t . taskId = tp . taskId and t . planId = tp . planId
								where tc . contactId = '" . $contactId . "' and t . isCompleted = 1
								) as t on ps . stepId = t . stepId and ps . planId = t . planId
								where ps . planId = '" . $planss . "' and ps . stepId = '" . $pss->stepId . "'
								");
                        if ($stepSkipId->cehId == NULL && $stepSkipId->ctsId == NULL && $stepSkipId->tId == NULL) {
                            //echo"INSERT INTO `contacttaskskipped`(`stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES('".$pss->stepId."', '".$pss->planId."', '".$contactId."', NOW(), '".mysql_real_escape_string($pss->summary)."')";
                            $database->executeNonQuery("INSERT INTO `contacttaskskipped`(`stepId`, `planId`, `contactId`, `addDate`, `title`) VALUES('" . $pss->stepId . "', '" . $pss->planId . "', '" . $contactId . "', NOW(), '" . mysql_real_escape_string($pss->summary) . "')");
                        }
                    }

                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no >= (select order_no from plansteps where stepId = '" . $firstStepOfPlan . "' AND isactive = 1 AND isArchive = 0) AND planId = '" . $planss . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");

                    $x = 0;
                    $SkippedSteps = array();
                    foreach ($palnSteps as $ps) {
                        $SkippedSteps[$x] = $ps->stepId;
                        $x++;
                    }
                    $skipSteps = implode(",", $SkippedSteps);
                    //echo "delete from contacttaskskipped where planId = '".$planss."' and stepId IN(".$skipSteps.") and contactId = '".$contactId."'";
                    $database->executeNonQuery("delete from contacttaskskipped where planId = '" . $planss . "' and stepId IN(" . $skipSteps . ") and contactId = '" . $contactId . "'");
                    $totalSteps = sizeof($palnSteps);
                    $stepCounter = 1;
//						echo '<br>';
//						print_r($palnSteps);
//						echo '</br>';

                    foreach ($palnSteps as $ps) {

                        $newContactSteps[$y] = $ps->stepId;
                        $y++;
                        if (!in_array($ps->stepId, $contactSteps) || $ps->stepId == $ignoreDaysAfterStepId) {
//

                            $planId2 = self::CheckLastStepOfPlan($planss, $contactId);
                            if (!empty($planId2)) {
                                $database->executeNonQuery("delete from contactPlansCompleted where planId = '" . $planId2 . "' and contactId = '" . $contactId . "'");
                            }
//								echo 'not exists' . $ps->stepId;
//								echo '<br />';
                            $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId = '" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                            $dueDate = strtotime(" + " . $stepDesc->daysAfter . " day", $date); //add days in date
                            $database->executeNonQuery("INSERT INTO planstepusers SET stepId = '" . $stepDesc->stepId . "',contactId = '" . $contactId . "',stepStartDate = '" . date('Y-m-d') . "',stepEndDate = '" . date('Y-m-d', $dueDate) . "'");
                            $newStepId = $database->insertid();

                            // update plan steps execution date if requested form contact detail page

                            $currentDate = date("Y-m-d");
                            if (!empty($stepExecutionNewDate) && $stepExecutionNewDate > $currentDate) {
                                $database->executeNonQuery("UPDATE planstepusers SET `stepEndDate`='" . $stepExecutionNewDate . "'
											WHERE contactId = '" . $contactId . "' and stepId='" . $ps->stepId . "'");
                            }


                            $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='" . $userId . "'");
                            if ($ps->isTask == 0 && $PermissionStatus != '4') {
                                if ($ps->daysAfter == 0 || $ignoreDaysAfterStepId == $ps->stepId) {

//										echo "\r\nemail step";
//										echo '<br>';
//										echo 'isDouble:' . $plan->isDoubleOptIn . '\r\n';
                                    $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId = '" . $contactId . "'");
                                    if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {
//											echo 'emial allowed';
//											echo '<br>';

                                        $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId = '" . $contactId . "'");
                                        $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id = '" . $ps->stepId . "'");
                                        preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                        foreach ($match[1] as $key) {
                                            if ($key == "First Name") {
                                                $firstname = 'firstName';
                                            }
                                            if ($key == "Last Name") {
                                                $lastname = 'lastName';
                                            }
                                            if ($key == "Phone Number") {
                                                $phonenumber = 'phoneNumber';
                                            }
                                            if ($key == "Company Name") {
                                                $companyname = 'companyTitle';
                                            }
                                            if ($key == "@Signature") {
                                                $signature = self::getSignatures($database, $userId);
                                            }
                                        }
                                        if (!empty($firstname)) {
                                            $f = $database->executeScalar("SELECT firstName FROM contact where contactId = '" . $contactId . "'");
                                        }
                                        if (!empty($lastname)) {
                                            $l = $database->executeScalar("SELECT lastName FROM contact where contactId = '" . $contactId . "'");
                                        }
                                        if (!empty($companyname)) {
                                            $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId = '" . $contactId . "'");
                                        }
                                        if (!empty($phonenumber)) {
                                            $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId = '" . $contactId . "'");
                                        }

                                        //email sent from here
                                        $file_nameUnique = $mailBody->attachmentUniqueName;
                                        $file_nameOriginal = $mailBody->attachmentOriginalName;
                                        $subject = $mailBody->subject;
                                        $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                        $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                        $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                        $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                        $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                        $message = str_replace("\'", "'", $rplbysig);

                                        $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " < " . $userInfo->email . ">\r\n" .
                                            "Reply - To: " . $userInfo->email . "\r\n";

//											echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                        $mail = new smtpEmail();

                                        $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id);

                                        //mail($contactemail,$subject,$message,$headers," - f ".$userInfo->email."");
                                        $database->executeNonQuery("INSERT INTO contactemailhistory(planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
					('" . $plan->planId . "', '" . $stepDesc->stepId . "', '" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                        $database->executeNonQuery("delete from planstepusers where stepId = '" . $stepDesc->stepId . "' and contactId = '" . $contactId . "'");
                                        if ($totalSteps == $stepCounter) {
                                            $alreadyCompleted = $database->executeScalar("SELECT count(*) as exists from contactPlansCompleted where stepId = '" . $stepDesc->stepId . "' and planId = '" . $planss . "' and contactId = '" . $contactId . "'");
                                            if ($alreadyCompleted == 0) {
                                                //echo "INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES('" . $stepDesc->stepId . "', '" . $planss . "', '" . $contactId . "', CURDATE())";
                                                $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES('" . $stepDesc->stepId . "', '" . $planss . "', '" . $contactId . "', CURDATE())");
                                                $database->executeNonQuery("delete from contacttaskskipped where contactId = '" . $contactId . "' and planId = '" . $planss . "'");
                                            }
                                        }
                                    }
                                } else {
//										echo 'breaking days after not zero';
//										echo '<br>';
                                    break;
                                }
                            } else {
                                if ($ps->isTask == 1) {
//									echo " < br /> task step < br />";
                                    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, customRuleId, repeatTypeId) VALUES('" . $userId . "', '" . mysql_real_escape_string($stepDesc->summary) . "', '0', '', '" . mysql_real_escape_string($stepDesc->description) . "', '" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                                    $taskId = $database->insertid();
                                    $database->executeNonQuery("INSERT INTO taskplan set taskId = '" . $taskId . "',planId = '" . $stepDesc->planId . "',stepId = '" . $stepDesc->stepId . "'");
                                    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId = '" . $contactId . "',taskId = '" . $taskId . "'");

                                }
//									echo '<br />';
//									echo 'breaking task occured';
                                break;
                            }
                        } else {
//								echo '<br />';
//								echo 'no change';
                            break;
                        }
                        $stepCounter++;
                    }
                    $count--;
                    $step++;
                } else {
                    /*echo "PlanId".$planss;
                    echo " < br />";
                    echo "SELECT ps . stepId, ps . planId,order_no
                    FROM plansteps ps
                    WHERE  ps . planId = '".$planss."' and ps . order_no = (select max(order_no) from plansteps where planId = '".$planss."')";
                    echo " < br />";*/
                    $lastStepOfPlan = $database->executeObject("SELECT ps . stepId, ps . planId,order_no
																	FROM plansteps ps
																	WHERE  ps . planId = '" . $planss . "' and
					ps . order_no = (select max(order_no) from plansteps where planId = '" . $planss . "'
					AND isactive = 1 AND isArchive = 0) AND isactive = 1 AND isArchive = 0");
                    /*print_r($lastStepOfPlan);
                    echo " < br />";
                    echo "INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES('".$lastStepOfPlan->stepId."', '".$lastStepOfPlan->planId."', '".$contactId."', CURDATE())";*/
                    //echo "SELECT count(*) as `exists` from contactPlansCompleted where stepId = '".$lastStepOfPlan->stepId."' and planId = '".$lastStepOfPlan->planId."' and contactId = '".$contactId."'";
                    $database->executeNonQuery("UPDATE tasks SET isCompleted = 1 WHERE userId = '" . $userId . "' AND taskId IN(select taskId FROM taskcontacts WHERE contactId = '" . $contactId . "')  AND isCompleted != 1 and planId = '" . $planss . "'"); //mark incomplete steps as complete
                    //$incompleteSteps = $database->executeObjectList("SELECT * FROM tasks  WHERE userId = '".$userId."' AND taskId IN(select taskId FROM taskcontacts WHERE contactId = '".$contactId."')  AND isCompleted != 1 and planId = '".$planss."' order by dueDateTime ASC" );
                    $alreadyCompleted = $database->executeScalar("SELECT count(*) as `exists` from contactPlansCompleted where stepId = '" . $lastStepOfPlan->stepId . "' and planId = '" . $lastStepOfPlan->planId . "' and contactId = '" . $contactId . "'");
                    if ($alreadyCompleted == 0) {
                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES('" . $lastStepOfPlan->stepId . "', '" . $lastStepOfPlan->planId . "', '" . $contactId . "', CURDATE())");
                        //echo $database->insertid();
                    }
                    $database->executeNonQuery("delete from contacttaskskipped where contactId = '" . $contactId . "' and planId = '" . $planss . "'");
                    /*echo " < br>";
                    echo "Added". $lastStepOfPlan->planId;
                    echo " < br />";*/
                }
            }
        } else {
            //echo "DELETE from taskcontacts where taskId  IN(SELECT tc . taskId FROM tasks t join taskcontacts tc on t . taskId = tc . taskId where t . userId = '".$userId."' and isCompleted <> 1 and tc . contactId = '".$contactId."') and contactId = '".$contactId."'";
            $taskId2 = $database->executeObjectList("SELECT tc . taskId FROM tasks t join taskcontacts tc on t . taskId = tc . taskId where t . userId = '" . $userId . "' and isCompleted <> 1 and tc . contactId = '" . $contactId . "' and planId <> 0");
            foreach ($taskId2 as $t2) {
                //echo "DELETE from taskcontacts where taskId = '".$t2->taskId."' and contactId = '".$contactId."'";
                $database->executeNonQuery("DELETE from taskcontacts where taskId = '" . $t2->taskId . "' and contactId = '" . $contactId . "'");
                //echo "DELETE from tasks where taskId = '".$t2->taskId."'";
                $database->executeNonQuery("DELETE from tasks where taskId = '" . $t2->taskId . "'");
            }
            $database->executeNonQuery("DELETE FROM contactPlansCompleted where  contactId = '" . $contactId . "'");
            $database->executeNonQuery("delete from contacttaskskipped where contactId = '" . $contactId . "'");
        }

        $steps = array();
        $j = 0;
        foreach ($delSteps as $key => $val) {
            $steps[$j] = $val;
            $j++;
        }
        //print_r($steps);
        $newSteps = implode(',', $newContactSteps);
        if ($newSteps != '') {
            //echo "DELETE FROM  planstepusers WHERE contactId = '".$contactId."' and stepId NOT IN(".$newSteps.")";
            $database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId = '" . $contactId . "' and stepId NOT IN(" . $newSteps . ")");
            /*echo "DELETE FROM  planstepusers WHERE contactId = '".$contactId."' and stepId = '".$steps[$i]."'";
            echo " < br>";*/
            /*echo "SELECT tp . taskId, planId FROM  taskplan tp join taskcontacts tc on tp . taskId = tc . taskId  WHERE stepId NOT IN(".$newSteps.") and contactId = '".$contactId."'";
            echo " < br>";*/
            $taskInfo = $database->executeObjectList("SELECT tp . taskId, planId FROM  taskplan tp join taskcontacts tc on tp . taskId = tc . taskId  WHERE stepId NOT IN(" . $newSteps . ") and contactId = '" . $contactId . "'");
            foreach ($taskInfo as $ts) {
                /*echo "DELETE FROM tasks WHERE taskId = '".$ts->taskId."' and isCompleted <> 1";
                echo " < br>";*/
                $database->executeNonQuery("DELETE FROM tasks WHERE taskId = '" . $ts->taskId . "' and isCompleted <> 1");
                $isDeleted = $database->getAffectedRows();
                /*echo "task deleted : ".$database->getAffectedRows();
                echo '<br>';*/
                if ($database->getAffectedRows() > 0) {
                    //echo "DELETE FROM taskcontacts WHERE contactId = '".$contactId."' and taskId = '".$ts->taskId."'";
                    $database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId = '" . $contactId . "' and taskId = '" . $ts->taskId . "'");
                }
                //echo "DELETE FROM contactPlansCompleted where planId = '".$taskInfo->planId."' and contactId = '".$contactId."'";
            }
        } else {
            //echo "DELETE FROM  planstepusers WHERE contactId = '".$contactId."'";
            $database->executeNonQuery("DELETE FROM  planstepusers WHERE contactId = '" . $contactId . "'");
        }
        //print_r($newcontactSteps);
        //print_r($steps);
        if (!empty($contactSteps)) {
            for ($i = 0; $i < sizeof($steps); $i++) {
                if (!in_array($steps[$i], $newContactSteps)) {
                    /*echo "DELETE FROM  planstepusers WHERE contactId = '".$contactId."' and stepId = '".$steps[$i]."'";
                    echo " < br>";*/
                    /*echo "SELECT tp . taskId, planId FROM  taskplan tp join taskcontacts tc on tp . taskId = tc . taskId  WHERE stepId = '".$steps[$i]."' and contactId = '".$contactId."'";
                    echo " < br>";*/
                    $taskInfo = $database->executeObject("SELECT tp . taskId, planId FROM  taskplan tp join taskcontacts tc on tp . taskId = tc . taskId  WHERE stepId = '" . $steps[$i] . "' and contactId = '" . $contactId . "'");
                    foreach ($planSteps['plan'] as $planss) {
                        if ($planss == $taskInfo->taskId) {
                            if ($planSteps['planStepDropDown' . $planss] != 'Completed') {
                                /*echo "DELETE FROM tasks WHERE taskId = '".$taskInfo->taskId."' and isCompleted <> 1";
                                echo " < br>";*/
                                $database->executeNonQuery("DELETE FROM tasks WHERE taskId = '" . $taskInfo->taskId . "' and isCompleted <> 1");
                                $isDeleted = $database->getAffectedRows();
                                /*echo "task deleted : ".$database->getAffectedRows();
                                echo '<br>';*/
                                if ($database->getAffectedRows() > 0) {
                                    //echo "DELETE FROM taskcontacts WHERE contactId = '".$contactId."' and taskId = '".$taskInfo->taskId."'";
                                    $database->executeNonQuery("DELETE FROM taskcontacts WHERE contactId = '" . $contactId . "' and taskId = '" . $taskInfo->taskId . "'");
                                }
                                $database->executeNonQuery("DELETE FROM contactPlansCompleted where planId = '" . $taskInfo->planId . "' and contactId = '" . $contactId . "'");
                                $database->executeNonQuery("delete from contacttaskskipped where planId = '" . $taskInfo->planId . "' and contactId = '" . $contactId . "'");
                            }
                        }
                    }
                    //echo 'taskInfo PlanId is '.$taskInfo->planId.'<br/>';
                }
            }
        }
        if (!empty($planSteps['plan'])) {
            $NewPlans = implode(",", $planSteps['plan']);
            $database->executeNonQuery("DELETE FROM contactPlansCompleted where planId NOT IN(" . $NewPlans . ") and contactId = '" . $contactId . "'");
            $database->executeNonQuery("delete from contacttaskskipped where planId NOT IN(" . $NewPlans . ") and contactId = '" . $contactId . "'");
        }
    }

    public function EditContactPlanByGroup($database, $contactId, $groupId, $userId, $from = 0)
    {
//			echo "Edit Contact Plan By Group";
//			echo '<br>from is: '.$from.'<br>';
        $date = date('Y-m-d');
        $date = strtotime($date);
        $userInfo = $database->executeObject("SELECT * FROM user WHERE userId = '" . $userId . "'");
        /*echo "SELECT * from plan WHERE groupId = '".$groupId."' and userId = '".$userId."'";
        echo " < br>";*/
        $plan = $database->executeObject("SELECT * from plan WHERE groupId = '" . $groupId . "' and (userId = '" . $userId . "') AND isactive = 1 AND isArchive = 0");
        /*echo $plan->planId;
        echo " < br>";*/
        $planType = $database->executeScalar("select isDoubleOptIn from plan where planId = '" . $plan->planId . "'");

        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId = '" . $contactId . "'");
        $x = 0;
        $contactSteps = array();
        foreach ($existingSteps as $exists) {
            $contactSteps[$x] = $exists->stepId;
            $x++;
        }
//			echo 'All Contact Steps';
//			print_r($contactSteps);

        $ifGuestAlreadyAssigned = '';
        $groupName = $database->executeScalar("select name from groups where groupId = '" . $groupId . "'");
        if ($groupName == 'Guests') {

            $ifGuestAlreadyAssigned = $database->executeScalar("SELECT g . name as guestName FROM taskcontacts tc
                                                                    LEFT JOIN tasks t ON tc . taskId = t . taskId
                                                                    LEFT JOIN plan p ON t . planId = p . planId
                                                                    LEFT JOIN groups g ON p . groupId = g . groupId
                                                                    WHERE contactId = '" . $contactId . "' and g . name = '" . $groupName . "'
					and t . isCompleted = 0");

//				echo '<br><br>1:ifGuestAlreadyAssigned is ' . $ifGuestAlreadyAssigned;
        }
//			echo '<br><br>2:ifGuestAlreadyAssigned is ' . $ifGuestAlreadyAssigned;
//			if ($contactId == '33370') {
//				exit();
//			}
        if (($plan->planId != '' || !empty($plan->planId)) && (empty($ifGuestAlreadyAssigned))) {
            $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
            //echo "plaN FOUND";
            /*echo "planCompleted:".$planId2;
            echo " < br>";*/
            /*if(!empty($planId2))
            {
            $database->executeNonQuery("delete from contactPlansCompleted where planId = '".$planId2."' and contactId = '".$contactId."'");
            }*/
//				echo '<br>current planstepid';
//				echo '<br>';

            $currentPlanStepId = $database->executeScalar("SELECT psu . stepId FROM planstepusers psu LEFT JOIN plansteps ps ON psu . `stepId` = ps . `stepId` WHERE psu . contactId = '" . $contactId . "' AND ps . planId = '" . $plan->planId . "'");

//				echo '<br>currentplanstepid is '.$currentPlanStepId.'<br>';

            if ((empty($palnStepsExists)) && (empty($planId2))) {
                $i = 0;

                if (!empty($currentPlanStepId)) {
                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no >= (select order_no from plansteps where stepId = '" . $plan->stepId . "' AND isactive = 1 AND isArchive = 0) AND planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                } else if (empty($currentPlanStepId)) {
                    if ($from == 0) {
                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no >= (select order_no from plansteps where stepId = '" . $plan->stepId . "' AND isactive = 1 AND isArchive = 0) AND planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                    } else if ($from == 1) {
                        $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0  order by order_no asc");
                    }
                }

//					if (($from == 1) || (empty($currentPlanStepId))) {
//						echo '<br>plansteps all<br>'."SELECT * FROM plansteps WHERE planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0  order by order_no asc".'<br><br>';
//
//						$palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0  order by order_no asc");
//					}
//					else {
//						echo '<br>plansteps<br>'."SELECT * FROM plansteps WHERE order_no >= (select order_no from plansteps where stepId = '" . $plan->stepId . "' AND isactive = 1 AND isArchive = 0) AND planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc"." < br><br > ";
//						$palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no >= (select order_no from plansteps where stepId = '" . $plan->stepId . "' AND isactive = 1 AND isArchive = 0) AND planId = '" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
//					}

                //$palnSteps=$database->executeObjectList("SELECT * FROM plansteps WHERE planId = '".$plan->planId."' order by order_no asc");
                $stepno = 0;
                $allsteps = array();
                foreach ($palnSteps as $ps) {
                    $allsteps[$stepno] = $ps->stepId;
                    $stepno++;
                }
//					echo 'All Steps';
//					print_r($allsteps);
                //echo '<br />';
                //$alreadyExists=array_intersect($allsteps,$usersteps);
                //print_r($alreadyExists);
                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                foreach ($palnSteps as $ps) {
                    /*echo $ps->stepId;
                    print_r($contactSteps);
                    echo 'is first';*/
                    if (!in_array($ps->stepId, $contactSteps)) {
                        //echo 'not exists'.$ps->stepId;
                        //echo '<br />';
                        $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId = '" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                        $dueDate = strtotime(" + " . $stepDesc->daysAfter . " day", $date); //add days in date
                        $database->executeNonQuery("INSERT INTO planstepusers SET stepId = '" . $stepDesc->stepId . "',contactId = '" . $contactId . "',stepStartDate = '" . date('Y-m-d') . "',stepEndDate = '" . date('Y-m-d', $dueDate) . "'");
                        $newStepId = $database->insertid();
                        $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='" . $userId . "'");
                        if ($ps->isTask == 0 && $PermissionStatus != '4') {
                            if ($ps->daysAfter == 0) {
                                //echo "\r\nemail step";
                                //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId = '" . $contactId . "' ORDER BY permissionRequestId DESC LIMIT 1");
                                if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {
                                    //echo 'emial allowed';
                                    $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId = '" . $contactId . "'");
                                    $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id = '" . $ps->stepId . "'");
                                    preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                    foreach ($match[1] as $key) {
                                        if ($key == "First Name") {
                                            $firstname = 'firstName';
                                        }
                                        if ($key == "Last Name") {
                                            $lastname = 'lastName';
                                        }
                                        if ($key == "Phone Number") {
                                            $phonenumber = 'phoneNumber';
                                        }
                                        if ($key == "Company Name") {
                                            $companyname = 'companyTitle';
                                        }
                                        if ($key == "@Signature") {
                                            $signature = self::getSignatures($database, $userId);
                                        }
                                    }
                                    if (!empty($firstname)) {
                                        $f = $database->executeScalar("SELECT firstName FROM contact where contactId = '" . $contactId . "'");
                                    }
                                    if (!empty($lastname)) {
                                        $l = $database->executeScalar("SELECT lastName FROM contact where contactId = '" . $contactId . "'");
                                    }
                                    if (!empty($companyname)) {
                                        $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId = '" . $contactId . "'");
                                    }
                                    if (!empty($phonenumber)) {
                                        $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId = '" . $contactId . "'");
                                    }
                                    $subject = $mailBody->subject;
                                    $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                    $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                    $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                    $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                    $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                    $message = str_replace("\'", "'", $rplbysig);
                                    $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " < " . $userInfo->email . ">\r\n" .
                                        "Reply - To: " . $userInfo->email . "\r\n";
                                    $headers .= "Content - Type: text / html";
                                    // '3Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                    $mail = new smtpEmail();
                                    $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id);
                                    //mail($contactemail,$subject,$message,$headers," - f ".$userInfo->email."");
                                    $database->executeNonQuery("INSERT INTO contactemailhistory(planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
					('" . $plan->planId . "', '" . $stepDesc->stepId . "', '" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                    $database->executeNonQuery("delete from planstepusers where stepId = '" . $stepDesc->stepId . "' and contactId = '" . $contactId . "'");
                                    if ($totalSteps == $stepCounter) {
                                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES('" . $stepDesc->stepId . "', '" . $plan->planId . "', '" . $contactId . "', CURDATE())");
                                    }
                                }
                            } else {
                                //echo 'breaking days after not zero';
                                //echo '<br>';
                                break;
                            }
                        } else if ($ps->isTask == 1) {
                            //echo " < br /> task step < br />";
                            $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId, endingOn, customRuleId, repeatTypeId) VALUES('" . $userId . "', '" . mysql_real_escape_string($stepDesc->summary) . "', '0', '', '" . mysql_real_escape_string($stepDesc->description) . "', '" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                            $taskId = $database->insertid();
                            $database->executeNonQuery("INSERT INTO taskplan set taskId = '" . $taskId . "',planId = '" . $stepDesc->planId . "',stepId = '" . $stepDesc->stepId . "'");
                            $database->executeNonQuery("INSERT INTO taskcontacts SET contactId = '" . $contactId . "',taskId = '" . $taskId . "'");
                            //echo '<br />';
                            //echo 'breaking task occured';
                            break;
                        }
                    } else {
                        //echo 'Plan exists, breaking';
                        break;
                    }
                    $stepCounter++;
                }

                return $plan->planId;
            }
        }
    }

    public function EditContactPlanByPlan($database, $contactId, $planId, $userId, $from = 0)
    {
        $date = date('Y-m-d');
        $date = strtotime($date);
        $userInfo = $database->executeObject("SELECT * FROM user WHERE userId = '" . $userId . "'");
        /*echo "SELECT * from plan WHERE groupId = '".$groupId."' and userId = '".$userId."'";
        echo " < br>";*/
        $plan = $database->executeObject("SELECT * from plan WHERE planId = '" . $planId . "' and (userId = '" . $userId . "') AND isactive = 1 AND isArchive = 0");
//			echo $plan->planId;
//			echo " < br>";
        $planType = $database->executeScalar("select isDoubleOptIn from plan where planId = '" . $plan->planId . "'");
        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId = '" . $contactId . "'");
        $x = 0;
        $contactSteps = array();
        foreach ($existingSteps as $exists) {
            $contactSteps[$x] = $exists->stepId;
            $x++;
        }
//			echo 'All Contact Steps';
//			print_r($contactSteps);
        $ifGuestAlreadyAssigned;
//			$groupName = $database->executeScalar("select name from groups where groupId = '" . $groupId . "'");
//			if ($groupName == 'Guests')
//			{
//				$ifGuestAlreadyAssigned = $database->executeScalar("SELECT g . name FROM taskcontacts tc
//										LEFT JOIN tasks t ON tc.taskId = t.taskId
//										LEFT JOIN plan p ON t.planId = p.planId
//										LEFT JOIN groups g ON p.groupId = g.groupId
//										WHERE contactId = '" . $contactId . "' and g.name = '" . $groupName . "' and t.isCompleted = 0");
//			}
        if (($plan->planId != '' || !empty($plan->planId))) {
            $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
//				echo "plaN FOUND";
//				echo "planCompleted:".$planId2;
//				echo "<br>";
            /*if(!empty($planId2))
            {
                $database->executeNonQuery("delete from contactPlansCompleted where planId='".$planId2."' and contactId='".$contactId."'");
            }*/
            $currentPlanStepId = $database->executeScalar("SELECT psu.stepId FROM planstepusers psu LEFT JOIN plansteps ps ON psu.`stepId` = ps.`stepId` WHERE psu.contactId='" . $contactId . "' AND ps.planId = '" . $plan->planId . "'");
            if ((empty($palnStepsExists)) && (empty($planId2))) {
                $i = 0;
                if ($from == 1) {
                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                } else {
                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='" . $plan->stepId . "' AND isactive = 1 AND isArchive = 0) AND planId='" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                }
                //$palnSteps=$database->executeObjectList("SELECT * FROM plansteps WHERE planId='".$plan->planId."' order by order_no asc");
                $stepno = 0;
                $allsteps = array();
                foreach ($palnSteps as $ps) {
                    $allsteps[$stepno] = $ps->stepId;
                    $stepno++;
                }
//					echo 'All Steps';
//					print_r($allsteps);
//					echo '<br />';
//					$alreadyExists=array_intersect($allsteps,$usersteps);
//					print_r($alreadyExists);
                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                foreach ($palnSteps as $ps) {
//						echo $ps->stepId;
//						print_r($contactSteps);
//						echo 'is first';
                    if (!in_array($ps->stepId, $contactSteps)) {
//							echo 'not exists'.$ps->stepId;
//							echo '<br />';
                        $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                        $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                        $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                        $newStepId = $database->insertid();
                        $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='" . $userId . "'");
                        if ($ps->isTask == 0 && $PermissionStatus != '4') {
                            if ($ps->daysAfter == 0) {
//									echo "\r\nemail step";
//									echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                                if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {
                                    //echo 'emial allowed';
                                    $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                    $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                    preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                    foreach ($match[1] as $key) {
                                        if ($key == "First Name") {
                                            $firstname = 'firstName';
                                        }
                                        if ($key == "Last Name") {
                                            $lastname = 'lastName';
                                        }
                                        if ($key == "Phone Number") {
                                            $phonenumber = 'phoneNumber';
                                        }
                                        if ($key == "Company Name") {
                                            $companyname = 'companyTitle';
                                        }
                                        if ($key == "@Signature") {
                                            $signature = self::getSignatures($database, $userId);
                                        }
                                    }
                                    if (!empty($firstname)) {
                                        $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($lastname)) {
                                        $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($companyname)) {
                                        $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($phonenumber)) {
                                        $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                    }
                                    $subject = $mailBody->subject;
                                    $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                    $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                    $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                    $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                    $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                    $message = str_replace("\'", "'", $rplbysig);
                                    $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                        "Reply-To: " . $userInfo->email . "\r\n";
                                    $headers .= "Content-Type: text/html";
                                    // '3Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                    $mail = new smtpEmail();
                                    $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id);
                                    //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                    $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                    $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                    if ($totalSteps == $stepCounter) {
                                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                                    }
                                }
                            } else {
                                //echo 'breaking days after not zero';
                                //echo '<br>';
                                break;
                            }
                        } else {
                            //echo "<br /> task step<br />";
                            $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_real_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                            $taskId = $database->insertid();
                            $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                            $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                            //echo '<br />';
                            //echo 'breaking task occured';
                            break;
                        }
                    } else {
                        //echo 'Plan exists, breaking';
                        break;
                    }
                    $stepCounter++;
                }

                return $plan->planId;
            }
        }
    }

    public function EditContactPlanByStepId($database, $contactId, $planId, $stepId, $userId, $from = 0)
    {

        $date = date('Y-m-d');
        $date = strtotime($date);
        $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $userId . "'");

        $plan = $database->executeObject("SELECT * from plan WHERE planId='" . $planId . "' and (userId='" . $userId . "') AND isactive = 1 AND isArchive = 0");

        $planType = $database->executeScalar("select isDoubleOptIn from plan where planId='" . $plan->planId . "'");
        $existingSteps = $database->executeObjectList("Select stepId from planstepusers where contactId='" . $contactId . "'");
        $x = 0;
        $contactSteps = array();
        foreach ($existingSteps as $exists) {
            $contactSteps[$x] = $exists->stepId;
            $x++;
        }

        $palnStepsExists = $database->executeObjectList("SELECT * FROM plan p JOIN plansteps ps ON p.planId = ps.planId JOIN planstepusers psu ON ps.stepId = psu.stepId WHERE p.planId ='" . $plan->planId . "' AND psu.contactId ='" . $contactId . "' AND p.isactive = 1 AND p.isArchive = 0 AND ps.isactive = 1 AND ps.isArchive = 0");

        if (($plan->planId != '' || !empty($plan->planId))) {
            $planId2 = self::CheckLastStepOfPlan($plan->planId, $contactId);
//				echo "plaN FOUND";
//				echo "planCompleted:".$planId2;
//				echo "<br>";

            $currentPlanStepId = $database->executeScalar("SELECT psu.stepId FROM planstepusers psu LEFT JOIN plansteps ps ON psu.`stepId` = ps.`stepId` WHERE psu.contactId='" . $contactId . "' AND ps.planId = '" . $plan->planId . "'");
            if ((empty($palnStepsExists)) && (empty($planId2))) {
                $i = 0;
                $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE order_no>=(select order_no from plansteps where stepId='" . $stepId . "' AND isactive = 1 AND isArchive = 0) AND planId='" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");

                $stepno = 0;
                $allsteps = array();
                foreach ($palnSteps as $ps) {
                    $allsteps[$stepno] = $ps->stepId;
                    $stepno++;
                }
//					echo 'All Steps';
//					print_r($allsteps);
//					echo '<br />';
                $totalSteps = sizeof($palnSteps);
                $stepCounter = 1;
                foreach ($palnSteps as $ps) {
//						echo $ps->stepId;
//						print_r($contactSteps);
//						echo 'is first<br>';
                    if (!in_array($ps->stepId, $contactSteps)) {
//							echo 'not exists'.$ps->stepId;
//							echo '<br />';
                        $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                        $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                        $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                        $newStepId = $database->insertid();
                        $PermissionStatus = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "' AND userId='" . $userId . "'");
                        if ($ps->isTask == 0 && $PermissionStatus != '4') {
                            if ($ps->daysAfter == 0) {
//									echo "\r\nemail step";
//									echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                                $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                                if (($contactPermission == 3 && $plan->isDoubleOptIn == 1) || $plan->isDoubleOptIn == 0) {
//										echo 'email allowed';
                                    $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                    $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                    preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                    foreach ($match[1] as $key) {
                                        if ($key == "First Name") {
                                            $firstname = 'firstName';
                                        }
                                        if ($key == "Last Name") {
                                            $lastname = 'lastName';
                                        }
                                        if ($key == "Phone Number") {
                                            $phonenumber = 'phoneNumber';
                                        }
                                        if ($key == "Company Name") {
                                            $companyname = 'companyTitle';
                                        }
                                        if ($key == "@Signature") {
                                            $signature = self::getSignatures($database, $userId);
                                        }
                                    }
                                    if (!empty($firstname)) {
                                        $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($lastname)) {
                                        $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($companyname)) {
                                        $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                    }
                                    if (!empty($phonenumber)) {
                                        $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                    }
                                    $subject = $mailBody->subject;
                                    $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                    $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                    $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                    $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                    $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                    $message = str_replace("\'", "'", $rplbysig);
                                    $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                        "Reply-To: " . $userInfo->email . "\r\n";
                                    $headers .= "Content-Type: text/html";
                                    // '3Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                    $mail = new smtpEmail();
                                    $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject, $mailBody->id);
                                    //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                    $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan->planId . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $userId . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                    $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                    if ($totalSteps == $stepCounter) {
                                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                                    }
                                }
                            } else {
//									echo 'breaking days after not zero';
//									echo '<br>';
                                break;
                            }
                        } else {
                            //echo "<br /> task step<br />";
                            $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_real_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                            $taskId = $database->insertid();
                            $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                            $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
//								echo '<br />';
//								echo 'breaking task occured';
                            break;
                        }
                    } else {
//							echo 'Plan exists, breaking';
                        break;
                    }
                    $stepCounter++;
                }

                return $plan->planId;
            }
        }
    }

    function getPageTitle($database, $pageId)
    {
        return $database->executeScalar("SELECT pageBrowserTitle FROM pages where pageId='" . $pageId . "'");
    }

    function sendPermissionEmail($database, $userId, $contactId)
    {
        $uid = base62_encode($userId);
        //$uid=$userId;
        $user = $database->executeObject("SELECT firstName,lastName,email,userId,leaderId FROM user WHERE userId='" . $userId . "'");
        $contactemail = $database->executeObject("SELECT c.email, emailId, firstName FROM  contact c join emailcontacts ec on c.contactId=ec.contactId  WHERE c.contactId='" . $contactId . "'");
        $emailId = $contactemail->emailId . '';
        $contactFirstName = $contactemail->firstName;
        $to = $contactemail->email;
        $from = $user->email;
        //
        if ($user->leaderId == 0) {
            $UserId1 = $user->userId;
        } else {
            $UserId1 = $user->leaderId;
        }
        $getPermissionEmail = $database->executeObject("SELECT permissionEmailBody,permissionEmailSubject from userpermissionemail where userId='" . $user->userId . "'");
        $subject = $getPermissionEmail->permissionEmailSubject;
        $subject = str_replace("[{First Name}]", "$user->firstName", "$subject");
        $AcceptLink = "<a href='https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "'>https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=" . $uid . "&cid=" . $contactId . "</a>";
        $declineLink = "<a href='https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "'>https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=" . $uid . "&cid=" . $contactId . "</a>";

        $body = $getPermissionEmail->permissionEmailBody;
        $body = str_replace("[{Accept Emails Link}]", "$AcceptLink", "$body");
        $body = str_replace("[{DeclineEmails Link}]", "$declineLink", "$body");
        $body = str_replace("[{First Name}]", "$user->firstName", "$body");
        $headers = "From:" . $user->firstName . " " . $user->lastName . " <" . $user->email . ">\r\n" . "Reply-To: " . $user->email . "\r\n";

        $mail = new smtpEmail();
        $mail->sendMail($database, $user->email, $to, $user->firstName, $user->lastName, $body, $subject, $stepId = null,
            $contactFirstName, '1');

        if ($status == 1) {
            $flag = 0;
            $requestDate = date("Y-m-d");
            $permissionContacts = $database->executeObject("SELECT userId,toContactId FROM contactpermissionrequests WHERE userId='" . $userId . "' AND toContactid='" . $contactId . "'");
            if (empty($permissionContacts)) {
                $database->executeNonquery("INSERT INTO contactpermissionrequests SET userId='" . $userId . "',toContactId='" . $contactId . "',emailId='" . $emailId . "',requestDate='" . $requestDate . "',statusId=2");
                //return $email;
            }
        }
    }

    function getUserFitProfile($database, $userId)
    {
        $uniqueCode = $database->executeScalar("SELECT newCode FROM pages WHERE userId='" . $userId . "' and pageBrowserTitle='Fit Profile'");

        return $uniqueCode;
    }

    function getUserSkinProfile($database, $userId)
    {
        $uniqueCode = $database->executeScalar("SELECT newCode FROM pages WHERE userId='" . $userId . "' and pageBrowserTitle='Skin Care Profile'");

        return $uniqueCode;
    }

    public function CompleteContactPlanByGroup($database, $contactId, $groupId, $userId)
    {
        $date = date('Y-m-d');
        $date = strtotime($date);
        $plan = $database->executeObject("SELECT * from plan WHERE groupId='" . $groupId . "' and userId='" . $userId . "' AND isactive = 1 AND isArchive = 0");
        if ($plan->planId != '' || !empty($plan->planId)) {
            $i = 0;
            $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plan->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
            $stepno = 0;
            $totalSteps = sizeof($palnSteps);
            $stepCounter = 1;
            foreach ($palnSteps as $ps) {
                $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "' AND isactive = 1 AND isArchive = 0");
                $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                if ($ps->isTask == 0) {
                    if ($totalSteps == $stepCounter) {
                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                    }
                } else {
                    $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,completedDate, customRuleId, repeatTypeId) VALUES('" . $userId . "','" . mysql_real_escape_string($stepDesc->summary) . "','1','','" . mysql_real_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','',NOW(),'0','1')");
                    $taskId = $database->insertid();
                    $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                    $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                    if ($totalSteps == $stepCounter) {
                        $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $plan->planId . "','" . $contactId . "',CURDATE())");
                    }
                }
                $stepCounter++;
            }

            return $plan->planId;
        }
    }

    public function deleteContactPlanLinkedtoGroup($database, $contactId, $userId, $newGroups)
    {
        $contactGroupsOld = self::showContactGroups($database, $contactId);
        $i = 0;
        $contactGroups = array();
        foreach ($contactGroupsOld as $cg) {
            $contactGroups[$i] = $cg->groupId;
            $i++;
        }
        /*echo "new groups";
        print_r($newGroups);*/
        $groups = array_diff($contactGroups, $newGroups);
        /*echo "diff groups";
        print_r($groups);*/
        for ($j = 0; $j < sizeof($groups); $j++) {
            if (!in_array($groups[$j], $newGroups['groups'])) {
                $groupPlans = $database->executeObjectList("SELECT gc.groupId,planId from groupcontacts gc JOIN plan p on gc.groupId=p.groupId where gc.contactId='" . $contactId . "' and p.userId='" . $userId . "' and gc.groupId='" . $groups[$j] . "'");
                foreach ($groupPlans as $plans) {
                    $palnSteps = $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $plans->planId . "' AND isactive = 1 AND isArchive = 0 order by order_no asc");
                    foreach ($palnSteps as $ps) {
                        $database->executeNonQuery("DELETE from planstepusers where stepId='" . $ps->stepId . "' and contactId='" . $contactId . "'");
                        //echo "SELECT tc.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId  where userId='".$userId."' and tc.contactId='".$contactId."' and planId='".$plans->planId."'";
                        $tasks = $database->executeObjectList("SELECT tc.taskId from tasks t join taskcontacts tc on t.taskId=tc.taskId  where userId='" . $userId . "' and tc.contactId='" . $contactId . "' and planId='" . $plans->planId . "'");
                        foreach ($tasks as $t) {
                            //echo "DELETE from tasks where taskId='".$t->taskId."' and isCompleted<>1";
                            $database->executeNonQuery("DELETE from tasks where taskId='" . $t->taskId . "' and isCompleted<>1");
                            $isDeleted = $database->getAffectedRows();
                            /*echo "task deleted : ".$isDeleted;
                            echo '<br>';*/
                            if ($isDeleted > 0) {
                                /*echo "DELETE  FROM taskplan WHERE taskId='".$t->taskId."' and planId='".$ps->planId."' and stepId='".$ps->stepId."'";
                                echo "DELETE  FROM taskcontacts WHERE contactId='".$contactId."' and taskId='".$t->taskId."'";*/
                                $database->executeNonQuery("DELETE  FROM taskplan WHERE taskId='" . $t->taskId . "' and planId='" . $ps->planId . "' and stepId='" . $ps->stepId . "'");
                                $database->executeNonQuery("DELETE  FROM taskcontacts WHERE contactId='" . $contactId . "' and taskId='" . $t->taskId . "'");
                            }
                        }
                    }
                    $database->executeNonQuery("DELETE  FROM contactPlansCompleted WHERE planId='" . $plans->planId . "' and  contactId='" . $contactId . "'");
                    $database->executeNonQuery("delete from contacttaskskipped where contactId='" . $contactId . "' and planId='" . $plans->planId . "'");
                }
                echo "DELETE FROM groupcontacts WHERE contactId='" . $contactId . "'<br>";
                $database->executeNonQuery("DELETE FROM groupcontacts WHERE contactId='" . $contactId . "'");
            }
        }
    }

    public function showContactGroups($database, $contactId)
    {
        return $database->executeObjectList("SELECT groupId FROM groups  WHERE groupId IN(SELECT groupId FROM groupcontacts WHERE contactId='" . $contactId . "')");
    }

    function contactfillmaillog($database, $userId, $contactId, $to, $subject, $body, $headers, $additionParameters)
    {
        $database->executeNonQuery("Insert into `contactfillmaillog` set `userId` = '" . $userId . "', `contactId` = '" . $contactId . "', `to` = '" . $to . "', `subject` ='" . $subject . "', `body` = '" . mysql_real_escape_string($body) . "', `headers` = '" . $headers . "', `additionalParameter` = '" . $additionParameters . "'");
    }

    function checkProfileNameAvailability($POST)
    {
        $database = new database();
        $sql = "Select * from `pages` where `userId` = '" . $POST['userId'] . "' and `newCode` = '" . $POST['profileName'] . "'";
        if (isset($_POST['profileId']) && empty($_POST['profileId']) === false) {
            $sql .= " AND `pageId` != '" . $_POST['profileId'] . "'";
        }
        $result = $database->executeObject($sql);
        if ($result) {
            echo 0;
        } else {
            echo 1;
        }
    }

    function getProfileTemplates($database, $generatedVia, $currentTemplate = 0)
    {
        if ($currentTemplate == 0) {

            return $database->executeObjectList("select * from `pagetemplate` where `generatedVia` = '" . $generatedVia . "'");
        } else {
            return $database->executeObjectList("select * from `pagetemplate` where `generatedVia` = '" . $generatedVia . "' and pageTemplateId != '" . $currentTemplate . "'");
        }

    }

    function getProfileDetails($database, $templateId)
    {
        return $database->executeObject("select * from `pagetemplate` where `pageTemplateId` = '" . $templateId . "'");
    }

    function getHeaderSize($database, $templateId)
    {
        return $database->executeScalar("select `headerSize` from `pagetemplate` where `pageTemplateId` = '" . $templateId . "'");
    }

    function getFontStyle($database)
    {
        return $database->executeObjectList("Select * from `fontstyle` where `status` = '1'");
    }

    function getFontStyleNamebyId($database, $id)
    {
        return $database->executeScalar("Select `actualName` from `fontstyle` where `fontId` = '" . $id . "'");
    }

    function getProfileColor($database, $type)
    {
        return $database->executeObjectList("Select * from `color` where `status` = '1' and `type` in (" . $type . ")");
    }

    function getProfileColorById($database, $id)
    {
        return $database->executeScalar("Select `actualName` from `color` where `colorId` = '" . $id . "'");
    }

    function getProfileColorDetail($database, $id)
    {
        return $database->executeObject("Select `actualName`,lightColor from `color` where `colorId` = '" . $id . "'");
    }

    function getProfileDefaultTemplateQuestions($database)
    {
        return $database->executeObjectList("Select questions.* ,`questiontype`.typeTitle FROM `questions` LEFT JOIN `questiontype` ON `questions`.`typeId` = `questiontype`.`typeId` WHERE `questions`.`templateDefault`= 1 and `questions`.`status` = '1' ORDER BY `order`");
    }

    function createNewTemplate($database, $POST, $headerImage, $leaderId)
    {
        $templateHTML = $this->getProfileTemplateHTML($database, $POST['txtProfileTemplateId']);
        $database->executeNonQuery("Insert into `pagetemplate` set `templateHTML` = '" . mysql_real_escape_string($templateHTML) . "', `thumbnail` = 'none.jpg', `order` = '1', `userId` = '" . $_SESSION['userId'] . "', `leaderId` = '" . $leaderId . "', `defaultText` = '" . mysql_real_escape_string($POST['txtDefaultText']) . "', `videoLink` = '" . mysql_real_escape_string($POST['txtVideoLink']) . "', `textFontStyle` = '" . $POST['ddlProfileFontStyle'] . "', `headerImage` = '" . $headerImage . "', `bgColor` = '" . $POST['ddlBgColor'] . "', `fontColor` = '" . $POST['ddlFontColor'] . "', `generatedVia` = '" . ($leaderId == '0' ? '2' : '3') . "'");

        return $database->insertid();
    }

    function getProfileTemplateHTML($database, $templateId)
    {
        return $database->executeScalar("select `templateHTML` from `pagetemplate` where `pageTemplateId` = '" . $templateId . "'");
    }

    function createNewPage($database, $POST, $headerImage, $txtProfileTemplateId, $leaderId, $currentStepId)
    {

        $lastOrder = $database->executeScalar("SELECT `order` FROM pages WHERE userId='" . $_SESSION['userId'] . "' ORDER BY `order` DESC LIMIT 1");
        $lastOrder1 = $lastOrder++;

        $template = $database->executeObject("select * from `pagetemplate` where `pageTemplateId` = '" . $txtProfileTemplateId . "'");

        if ($headerImage = 'old') {
            $headerImage = $template->profileHeader;
        } else {
            $headerImage;
        }
        $sql = "Insert into `pages` set
			`userId` = '" . $_SESSION['userId'] . "',
			`uniqueCode` = ' ',
			`pageTemplateId` = '" . $txtProfileTemplateId . "',
			`formTemplateId` = ' ' ,
			`isAutomaticallyAddNewContact` = ' ' ,
			`pageBrowserTitle` = '" . mysql_real_escape_string($POST['txtProfileName']) . "',
			`thankYouMessage` = ' ',
			`pageHTMLGenerated` = ' ' ,
			`formHTMLGenerated` = ' ' ,
			`addDate` = CURDATE(),
			`pageName` = '" . mysql_real_escape_string($POST['txtProfileName']) . "',
			`newCode`= '" . mysql_real_escape_string($POST['txtProfileShortName']) . "',
			`leaderId` = '" . $leaderId . "',
			`generatedVia` = '" . ($leaderId == $_SESSION['userId'] ? '2' : '3') . "',
			`profileDefaultText` = '" . mysql_real_escape_string($POST['txtDefaultText']) . "',
			`profileVideoLink` = '" . mysql_real_escape_string($POST['txtVideoLink']) . "',
			`profileFontStyle` = '" . $POST['ddlProfileFontStyle'] . "',
			`profileHeader` = '" . $headerImage . "',
			`profilebgColor` = '" . $POST['ddlBgColor'] . "',
			`profileFontColor` = '" . $POST['ddlFontColor'] . "',
			`thankyouDefaultText` = '" . mysql_real_escape_string($template->thankyouDefaultText) . "',
			`thankyouVideoLink` = '" . mysql_real_escape_string($template->thankyouVideoLink) . "',
			`thankyouFontStyle` = '" . $template->thankyouFontStyle . "',
			`thankyouHeader` = '" . $template->thankyouHeader . "',
			`thankyoubgColor` = '" . $template->thankyoubgColor . "',
			`thankyouFontColor` = '" . $template->thankyouFontColor . "',
			`columnCount` = '" . $POST['ddlColumnCount'] . "',
			`accentColor` = '" . $POST['ddlAccentColor'] . "',
			`order`='" . $lastOrder1 . "',
			`status` = '1',
			`currentStepId` = '" . $currentStepId . "'";

        $database->executeNonQuery($sql);

        return $database->insertid();
    }

    function editPageDetails($database, $POST, $image, $page)
    {
        if ($page == 'profile') {
            if ($image == 'old')
                $imageParameter;
            else
                $imageParameter = "`profileHeader` = '" . $image . "',";

            $database->executeNonQuery("Update `pages` set
				`pageBrowserTitle`='" . $POST['pageBrowserTitle'] . "' ,
				`profileDefaultText` = '" . mysql_real_escape_string($POST['txtDefaultText']) . "',
				`profileVideoLink` = '" . mysql_real_escape_string($POST['txtVideoLink']) . "',
				`isDefaultVideoshow`='" . $_POST['isDefaultVideoshow'] . "',
				`profileFontStyle` = '" . $POST['ddlProfileFontStyle'] . "',
				" . $imageParameter . "
				`profilebgColor` = '" . $POST['ddlBgColor'] . "',
				`profileFontColor` = '" . $POST['ddlFontColor'] . "',
				`columnCount` = '" . $POST['ddlColumnCount'] . "',
				`accentColor` = '" . $POST['ddlAccentColor'] . "',
				`currentStepId` = '" . $POST['currentStepId'] . "',
				`newCode` = '" . $POST['txtNewCode'] . "'

				where `pageId` = '" . $POST['txtPageId'] . "'");
        } elseif ($page == 'thankyou') {
            if ($image == 'old')
                $imageParameter;
            else
                $imageParameter = "`thankyouHeader` = '" . $image . "',";

            $database->executeNonQuery("Update `pages` set
				`thankyouDefaultText` = '" . mysql_real_escape_string($POST['txtDefaultText']) . "',
				`thankyouVideoLink` = '" . mysql_real_escape_string($POST['txtVideoLink']) . "',
				`thankyouFontStyle` = '" . $POST['ddlProfileFontStyle'] . "',
				" . $imageParameter . "
				`thankyoubgColor` = '" . $POST['ddlBgColor'] . "',
				`thankyouFontColor` = '" . $POST['ddlFontColor'] . "',
				`currentStepId` = '" . $POST['currentStepId'] . "',
				`thankyouVideoVisibility`='" . $POST['videoVisibility'] . "'
				where `pageId` = '" . $POST['txtPageId'] . "'");
        }
    }

    function linkPagesWithDefaultQuestions($database, $pageId)
    {
        $database->executeNonQuery("Insert into `pagequestions` (`pageId`, `questionId`, `order`, `mandatory`, `enableStatus`) SELECT '" . $pageId . "',`questionId`, `order`, '1', `enableStatus` from `questions` where `templateDefault` = 1 and `status` = 1 order by `order`");
    }

    function addPageQuestion($database, $POST)
    {
        $database->executeNonQuery("Insert into `questions` set `question` = '" . addslashes($POST['txtFieldQuestion']) . "', `typeId` = '" . $POST['ddlQuestionType'] . "', `order` = '" . $POST['txtOrder'] . "', `label` = '" . addslashes($POST['txtFieldQuestionLabel']) . "'");
        $questionId = $database->insertid();
        $i = 1;
        if ($POST['ddlQuestionType'] == 3) {
            foreach ($POST['txtCheckbox'] as $txtCheckBox) {
                $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '" . $questionId . "', `choiceText` = '" . addslashes($txtCheckBox) . "', `order` = '" . $i . "'");
                $i++;
            }
        } //checkbox
        elseif ($POST['ddlQuestionType'] == 4) {
            foreach ($POST['txtRadioBtn'] as $txtRadioBtn) {
                $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '" . $questionId . "', `choiceText` = '" . addslashes($txtRadioBtn) . "', `order` = '" . $i . "'");
                $i++;
            }
        }
        //radiobutton
        $database->executeNonQuery("Insert into `pagequestions` (`pageId`,`questionId`, `order` ,`mandatory`) (Select '" . $POST['txtPageId'] . "', '" . $questionId . "', max(`order`)+1,`mandatory` = '" . $POST['rdMandatory'] . "' from pagequestions where `pageId` = '" . $POST['txtPageId'] . "')");

        //$database->executeNonQuery("Insert into `pagequestions` set `pageId` = '".$POST['txtPageId']."', `questionId` = '".$questionId."',");
    }

    function getPageDetails($database, $pageId)
    {
        return $database->executeObject("Select * from `pages` where `pageId` = '" . $pageId . "'");
    }

    function updateQuestionsOrder($database, $order, $orderStart)
    {
        $where;
        $orderIds;
        $count = $orderStart;
        $orderExplode = explode(',', $order);
        foreach ($orderExplode as $oe) {
            if (!(empty($oe))) {
                //echo $oe;
                if (empty($orderIds))
                    $orderIds = $oe;
                else
                    $orderIds .= ',' . $oe;
                $count++;
                $where .= " when `questionId` = '" . $oe . "' then '" . $count . "'";
            }
        }
        $database->executeNonQuery("update `pagequestions` set `order` = case " . $where . " end where `questionId` in (" . $orderIds . ")");
    }

    function updateProfileTemplate($database, $POST)
    {

        $database->executeNonQuery("Update `pages` set `pageTemplateId` = '" . $POST['txtProfileTemplateId'] . "' where `pageId` = '" . $POST['txtPageId'] . "'");

        $templateDetails = $database->executeObject("select * from pagetemplate where `pageTemplateId` = '" . $POST['txtProfileTemplateId'] . "'");

        $database->executeNonQuery("Update `pages` set
			`profileDefaultText` = '" . mysql_real_escape_string($templateDetails->profileDefaultText) . "',
			`thankyouDefaultText` = '" . mysql_real_escape_string($templateDetails->thankyouDefaultText) . "',
			`profileVideoLink` = '" . $templateDetails->profileVideoLink . "',
			`thankyouVideoLink` = '" . $templateDetails->thankyouVideoLink . "',
			`profileFontStyle` = '" . $templateDetails->profileFontStyle . "',
			`thankyouFontStyle` = '" . $templateDetails->thankyouFontStyle . "',
			`profileHeader` = '" . $templateDetails->profileHeader . "',
			`thankyouHeader` = '" . $templateDetails->thankyouHeader . "',
			`profilebgColor` = '" . $templateDetails->profilebgColor . "',
			`thankyoubgColor` = '" . $templateDetails->thankyoubgColor . "',
			`profileFontColor` = '" . $templateDetails->profileFontColor . "',
			`thankyouFontColor` = '" . $templateDetails->thankyouFontColor . "'
			where `pageId` = '" . $POST['txtPageId'] . "'");
    }

    function toggleQuestions($database, $id, $legend)
    {
        if ($legend == 'togglePageQuestions') {
            $database->executeNonQuery("update `questions` set `status` = IF(status=1, 0, 1) where `questionId` = '" . $id . "'");
        } else if ($legend == 'toggleCustomRuleQuestion') {
            $database->executeNonQuery("update `pagecustomrulesdetails` set `status` = IF(status=1, 0, 1) where `pageCustomRulesDetailsid` = '" . $id . "'");
        } else if ($legend == 'toggleDefaultQuestions') {
            $database->executeNonQuery("update `pagequestions` set `status` = IF(status=1, 0, 1) where `questionId` = '" . $id['id'] . "' and `pageId` = '" . $id['pageId'] . "'");

        }

    }

    function getQuestionDetails($database, $questionId)
    {
        return $database->executeObject("select `q`.*, `pq`.`mandatory` from `questions` `q` left join `pagequestions` `pq` on `q`.`questionId` = `pq`.`questionId` where `q`.`questionId` = '" . $questionId . "'");
    }

    function getQuestionChoiceDetails($database, $questionId)
    {
        return $database->executeObjectList("Select `q`.`question`, `p`.`choiceId`, `p`.`choiceText`, `p`.`order` FROM `questions` `q` left join `pageformquestionchoices` `p` on `q`.`questionId` = `p`.`questionId` where `q`.`questionId` = '" . $questionId . "' order by `p`.`order`");
    }

    function updatePageQuestion($database, $POST)
    {
        $database->executeNonQuery("Update `questions` set `question` = '" . addslashes($POST['txtFieldQuestion']) . "', `label` = '" . addslashes($POST['txtFieldQuestionLabel']) . "', `typeId` = '" . $POST['ddlQuestionType'] . "'  where `questionId` = '" . $POST['txtQuestionId'] . "'");
        $database->executeNonQuery("Update `pagequestions` set `mandatory` = '" . $POST['rdMandatory'] . "' where `questionId` = '" . $POST['txtQuestionId'] . "'");
        $database->executeNonQuery("delete from `pageformquestionchoices` where `questionId` = '" . $POST['txtQuestionId'] . "'");
        $database->executeNonQuery("delete from `pagecustomrulesdetails` WHERE `questionId` = '" . $POST['txtQuestionId'] . "'");
        $i = 1;
        if ($POST['ddlQuestionType'] == 3) {
            foreach ($POST['txtCheckbox'] as $txtCheckBox) {
                $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '" . $POST['txtQuestionId'] . "', `choiceText` = '" . addslashes($txtCheckBox) . "', `order` = '" . $i . "'");
                $i++;
            }
        } //checkbox
        elseif ($POST['ddlQuestionType'] == 4 || $POST['ddlQuestionType'] == 5) {
            foreach ($POST['txtRadioBtn'] as $txtRadioBtn) {
                $database->executeNonQuery("Insert into `pageformquestionchoices` set `questionId` = '" . $POST['txtQuestionId'] . "', `choiceText` = '" . addslashes($txtRadioBtn) . "', `order` = '" . $i . "'");
                $i++;
            }
        }
        //radiobutton
    }

    function getPageCustomRules($database, $pageId, $legend, $status = 'NA')
    {
        $where = '';
        if ($status == 1)
            $status = 'AND pcrd.status = 1 AND pcr.status = 1';
        else
            $status = '';

        if ($legend == 1) {
            return $database->executeObjectList("Select `pcrd`.*,g.`name` as groupName ,p.`title` as planName from `pagecustomrulesdetails` `pcrd` LEFT JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid` LEFT JOIN groups g ON pcrd.`groupId` = g.`groupId` LEFT JOIN plan p ON pcrd.`planId` = p.`planId` where `pcr`.`pageId` = '" . $pageId . "' AND  (pcrd.questionId IS NULL or pcrd.questionId =0)" . $status);
        } //checking custom groups and campaigns
        elseif ($legend == 2) {
            return $database->executeObjectList("Select `pcrd`.*,g.`name` as groupName ,p.`title` as planName from `pagecustomrulesdetails` `pcrd` JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid` LEFT JOIN groups g ON pcrd.`groupId` = g.`groupId` LEFT JOIN plan p ON pcrd.`planId` = p.`planId`  where `pcr`.`pageId` = '" . $pageId . "' AND  (pcrd.questionId IS NOT NULL and pcrd.questionId != 0 )" . $status);
        }
        //checking custom questions
    }

    function addPageCustomRules($database, $POST)
    {
        $database->executeNonQuery("Insert into `pagecustomrules` set `pageId` = '" . $POST['txtPageId'] . "'");
        $pageCustomRulesid = $database->insertid();
        foreach ($POST['chkGroup'] as $group) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `groupId` = '" . $group . "'");
        }
        foreach ($POST['chkPlan'] as $plan) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `planId` = '" . $plan . "', `stepId` = '" . $POST['ddlPlanSteps_' . $plan] . "'");
        }
    }

    public function getPlanGroup($database, $planId)
    {
        $sql = "SELECT `groupId` FROM `plan` WHERE `planId` = '" . $planId . "'";
        return $database->executeScalar($sql);
    }

    function getPageCustomQuestionRulesDetails($database, $questionId, $choices)
    {
        return $database->executeObject("Select `q`.`question`, `p`.`choiceId`, group_concat(`p`.`choiceText`) as choices, `p`.`order` FROM `questions` `q` left join `pageformquestionchoices` `p` on `q`.`questionId` = `p`.`questionId` where `q`.`questionId` = '" . $questionId . "' and p.`choiceId` in ('" . $choices . "') Group by `q`.`questionId`");
    }

    public function getPageQuestionsForCustomRules($database, $pageId)
    {

        return $database->executeObjectList("select q.*, pq.* from `questions` `q` left join `pagequestions` `pq` on `q`.`questionId` = `pq`.`questionId` where `pageId` = '" . $pageId . "' and (`q`.`typeId` = 3 or `q`.`typeId` = 4) and `q`.`templateDefault` = 0  ORDER BY `q`.`questionId`");
    }

    function addPageCustomQuestionRules($database, $POST)
    {
        $questionChoices = '';
        $pageCustomRulesid = $database->executeScalar("select `pageCustomRulesid` from `pagecustomrules` where `pageId` = '" . $POST['txtPageId'] . "'");
        $questionId = $POST['ddlCustomQuestionId'];
        foreach ($POST['questionChoice_' . $questionId] as $qc) {
            if (empty($questionChoices)) {
                $questionChoices = $qc;
            } else {
                $questionChoices = $questionChoices . ',' . $qc;
            }
        }
        foreach ($POST['chkGroup'] as $group) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `groupId` = '" . $group . "', `questionId` = '" . $questionId . "', `questionChoices` = '" . $questionChoices . "'");
        }
        foreach ($POST['chkPlan'] as $plan) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `planId` = '" . $plan . "', `stepId` = '" . $POST['ddlPlanSteps_' . $plan] . "', `questionId` = '" . $questionId . "', `questionChoices` = '" . $questionChoices . "'");
        }
    }

    function updatePageCustomQuestionRules($database, $POST)
    {
        $questionChoices;
        $pageCustomRulesid = $database->executeScalar("select `pageCustomRulesid` from `pagecustomrules` where `pageId` = '" . $POST['txtPageId'] . "'");
        $questionExplodes = explode('-', $_POST['txtEditCustomRuleQuestion']);
        $questionId = $questionExplodes[0];
        $pageCustomRulesDetailsid = $questionExplodes[2];
        $database->executeNonQuery("Delete from `pagecustomrulesdetails` WHERE `pageCustomRulesId` = '" . $pageCustomRulesid . "' AND `questionId` = '" . $questionId . "' and `pageCustomRulesDetailsid` = '" . $pageCustomRulesDetailsid . "'");
        foreach ($POST['questionChoice_' . $questionId] as $qc) {
            if (empty($questionChoices)) {
                $questionChoices = $qc;
            } else {
                $questionChoices = $questionChoices . ',' . $qc;
            }
        }
        foreach ($POST['chkGroup'] as $group) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `groupId` = '" . $group . "', `questionId` = '" . $questionId . "', `questionChoices` = '" . $questionChoices . "'");
        }
        foreach ($POST['chkPlan'] as $plan) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `planId` = '" . $plan . "', `stepId` = '" . $POST['ddlPlanSteps_' . $plan] . "', `questionId` = '" . $questionId . "', `questionChoices` = '" . $questionChoices . "'");
        }
    }

    function getCommaSeperatedIdsinCustomRules($database, $pageId, $legend)
    {
        if ($legend == 1) {
            return $database->executeScalar("SELECT GROUP_CONCAT(`pcrd`.groupId) as Ids FROM `pagecustomrulesdetails` `pcrd` LEFT JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid`  WHERE `pcr`.`pageId` = '" . $pageId . "' AND pcrd.groupId IS NOT NULL AND (pcrd.questionId IS NULL or pcrd.questionId = 0 )GROUP BY pcr.`pageCustomRulesid`");
        } elseif ($legend == 2) {
            return $database->executeObjectList("SELECT `pcrd`.planId, `pcrd`.`stepId` FROM `pagecustomrulesdetails` `pcrd` LEFT JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid`  WHERE `pcr`.`pageId` = '" . $pageId . "' AND pcrd.planId IS NOT NULL AND (pcrd.questionId IS NULL or pcrd.questionId =0)");

        }
    }

    function getCommaSeperatedIdsinCustomQuestionRules($database, $pageId, $legend, $pageCustomRulesDetailsId)
    {
        if ($legend == 1) {
            return $database->executeScalar("SELECT GROUP_CONCAT(`pcrd`.groupId) as Ids FROM `pagecustomrulesdetails` `pcrd` LEFT JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid`  WHERE `pcr`.`pageId` = '" . $pageId . "' and `pageCustomRulesDetailsId` = '" . $pageCustomRulesDetailsId . "' AND pcrd.groupId IS NOT NULL AND pcrd.questionId IS NOT NULL GROUP BY pcr.`pageCustomRulesid`");
        } elseif ($legend == 2) {
            return $database->executeObjectList("SELECT `pcrd`.planId, `pcrd`.`stepId` FROM `pagecustomrulesdetails` `pcrd` LEFT JOIN `pagecustomrules` `pcr` ON `pcrd`.`pageCustomRulesid` = `pcr`.`pageCustomRulesid`  WHERE `pcr`.`pageId` = '" . $pageId . "'  and `pageCustomRulesDetailsId` = '" . $pageCustomRulesDetailsId . "' AND pcrd.planId IS NOT NULL AND pcrd.questionId IS NOT NULL ");

        }
    }

    function updatePageCustomRules($database, $POST)
    {
        $pageCustomRulesid = $database->executeScalar("Select `pageCustomRulesid` from `pagecustomrules` where `pageId` = '" . $POST['txtPageId'] . "'");
        if (empty($pageCustomRulesid)) {
            $database->executeNonQuery("Insert into `pagecustomrules` set `pageId` = '" . $POST['txtPageId'] . "'");
        }
        $database->executeNonQuery("Delete from `pagecustomrulesdetails` WHERE `pageCustomRulesid` = '" . $pageCustomRulesid . "' AND `questionId` IS NULL");
        foreach ($POST['chkGroup'] as $group) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `groupId` = '" . $group . "'");
        }
        foreach ($POST['chkPlan'] as $plan) {
            $database->executeNonQuery("Insert into `pagecustomrulesdetails` set `pageCustomRulesid` = '" . $pageCustomRulesid . "', `planId` = '" . $plan . "', `stepId` = '" . $POST['ddlPlanSteps_' . $plan] . "'");
        }
    }

    function getPagePreviewDetails($database, $pageId, $userId)
    {
        return $database->executeObject("select u.username, u.userId, u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.email, u.facebookPersonalAddress, u.facebookPartyAddress, p.*, pt.templateHTML from user u
            join pages p on p.userId=u.userId
            join pagetemplate pt on p.pageTemplateId=pt.pageTemplateId
            where p.pageId='" . $pageId . "' and u.userId = '" . $userId . "'");
    }

    function getCustomQuestionRules($database, $questionId)
    {
        return $database->executeObjectList("select * from `pagecustomrulesdetails` WHERE questionId = '" . $questionId . "'");
    }

    function activateProfile($database, $profileId)
    {
        $database->executeNonQuery("update `pages` set `status` = 1, `isComplete` = 1,`currentStepId` = 0 where `pageId` = '" . $profileId . "'");
    }

    function archiveProfile($database, $profileId)
    {
        $database->executeNonQuery("update `pages` set `status` = '0',`isArchive`='1' where `pageId` = '" . $profileId . "'");
    }

    function setPublishedPagesForUser($database, $userId, $leaderId)
    {

        $database->executeNonQuery("insert into pages ( uniqueCode, userId, pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, addDate, pageName, newCode, leaderId, status, privacy, generatedVia, profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount) select  uniqueCode,'" . $userId . "', pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, CURDATE(), pageName, newCode, '" . $leaderId . "', status, privacy,'3', profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount from pages where userId='" . $leaderId . "' AND status='1' AND privacy='1'");

    }

    function publishProfileUser($database, $profileId, $userId, $leaderId)
    {
        $order = $database->executeScalar("select max(`order`) from pages where `userId` = '" . $userId . "'");
        $order = $order++;
        $parentId = $database->executeScalar("Select `parentId` from `pages` where `pageId` = '" . $profileId . "'");
        if ((empty($parentId)) || (is_null($parentId)) || ($parentId == 0))
            $parentId = $profileId;

        $database->executeNonQuery("insert into pages ( uniqueCode, userId, pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, addDate, pageName, newCode, leaderId, status, privacy, generatedVia, profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount, oldId, parentId, `order`,`isComplete`,`thankyouVideoVisibility`,`accentColor`,`isDefaultVideoshow`) select  uniqueCode,'" . $userId . "', pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, CURDATE(), pageName, newCode, '" . $leaderId . "', 1, 0,2, profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount,'" . $profileId . "','" . $parentId . "',`order`,`isComplete`,`thankyouVideoVisibility`,`accentColor`,`isDefaultVideoshow` from pages where `pageId` = '" . $profileId . "'");

        $count = 0;
        $newProfileId = $database->insertid();

        $database->executeNonQuery("INSERT INTO pagequestions(pageId, questionId,`order`,`mandatory`,`status`, `enableStatus`) SELECT '" . $newProfileId . "',pq.questionId,pq.`order`,pq.`mandatory`,pq.`status`,pq.`enableStatus` FROM pagequestions pq LEFT JOIN questions q ON q.`questionId` = pq.`questionId` WHERE pq.pageId = '" . $profileId . "' AND q.`templateDefault` = 1");

        //echo ("Select `questionId`,mandatory from `pagequestions` where `pageId` = '" . $profileId . "' order by `order` desc");
        $questionId = $database->executeObjectList("SELECT q.questionId, pq.mandatory FROM questions q JOIN pagequestions pq ON q.questionId= pq.questionId JOIN questiontype pt ON q.`typeId` = pt.`typeId` WHERE pageId= '" . $profileId . "' AND `q`.`templateDefault` = 0 AND q.status = 1 ORDER BY pq.order ASC");

        //("Select p.* from `pagequestions` p join questions q on p.questionId=q.questionId where p.`pageId` = '" . $profileId . "' and q.`templateDefault`='0' AND p.status='1'  order by p.`order` desc");

        foreach ($questionId as $qId) {
            $count++;

            $database->executeNonQuery("Insert into `questions` (`question`,`typeId`,`order`,`label`,`templateDefault`,`status`,`datetime`,`oldId`,`enableStatus`) select `question`,`typeId`,`order`,`label`,'0',1,now(),`questionId`,`enableStatus` from `questions` where questionId = '" . $qId->questionId . "'");

            $newQuestionId = $database->insertid();

            $database->executeNonQuery("Insert into `pageformquestionchoices` (`questionId`,`choiceText`,`order`,`oldId`,`parentId`) select '" . $newQuestionId . "',`choiceText`, `order`,`choiceId`,if(`parentId`='0',`choiceId`,`parentId`) from `pageformquestionchoices` where `questionId` = '" . $qId->questionId . "'");

// for copy of custom questions.
            $database->executeNonQuery("Insert into `questionradiolabels` (`questionId`,`label`,`order`) select '" . $newQuestionId . "',`label`,`order`
             from `questionradiolabels` where questionId = '" . $qId->questionId . "'");
// end copy of custom questions.

            $database->executeNonQuery("Insert into `pagequestions` (`pageId`, `questionId`,`order`,`mandatory`) values ('" . $newProfileId . "','" . $newQuestionId . "', '" . $count . "','" . $qId->mandatory . "')");

        }
        $database->executeNonQuery("INSERT INTO `pagecustomrules` (`pageId`,`status`,`datetime`,`oldId`) (SELECT '" . $newProfileId . "',`status`,NOW(),pageCustomRulesid FROM `pagecustomrules` WHERE pageId='" . $profileId . "' AND `status`='1')");
        $profileCustomRuleId = $database->insertid();
// custom rules
        $customRule = $database->executeObjectList("SELECT * FROM pagecustomrulesdetails pcrd left join pagecustomrules pcr on pcrd.pageCustomRulesid =pcr.pageCustomRulesid WHERE pcr.`pageId`='" . $profileId . "'");

        foreach ($customRule as $c) {
            $question_id = "''";
            if ((!empty($c->questionId)) && (!($c->questionId == 0)) && (!(is_null($c->questionId))))
                $question_id = "(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "')";

            $choice_id = "''";
            if ((!empty($c->questionChoices)) && (!($c->questionChoices == 0)) && (!(is_null($c->questionChoices))))
                $choice_id = "(select pfqc.choiceId from pageformquestionchoices pfqc where pfqc.oldId='" . $c->questionChoices . "' AND pfqc.isactive='1')";

            $plan_id = "''";
            if ((!empty($c->planId)) && (!($c->planId == 0)) && (!(is_null($c->planId))))
                $plan_id = "(select p.planId from plan p where p.oldId='" . $c->planId . "' AND p.userId='" . $userId . "' AND p.isactive='1' AND p.`isArchive` ='0')";

            $stepId = "''";
            if ((!empty($c->stepId)) && (!($c->stepId == 0)) && (!(is_null($c->stepId))))
                $stepId = "(SELECT s.stepId FROM plansteps s JOIN plan p ON s.`planId` = p.planid WHERE s.oldId='" . $c->stepId . "' AND p.userId = '" . $userId . "' AND p.`isactive` = '1' AND p.`isArchive` ='0')";
//							$stepId = "(select s.stepId from plansteps s where s.oldId='" . $c->stepId . "')";

            if ($leaderId == 0) {
                $groupId = $database->executeScalar("SELECT groupId FROM groups WHERE parentId = '" . $c->groupId . "' AND userId = '" . $userId . "'");
            } else {
                $groupId = $c->groupId;
            }
            $database->executeNonQuery("INSERT INTO `pagecustomrulesdetails`
                                      (`pageCustomRulesid`,
                                       `questionId`,
                                       `questionChoices`,
                                       `groupId`,
                                       `planId`,
                                       `stepId`,
                                       `status`,
                                       `datetime`,
                                       `oldId`)
                               VALUES('" . $profileCustomRuleId . "',
                                                  " . $question_id . ",
                                                  " . $choice_id . ",
                                                  '" . $groupId . "',
                                                  " . $plan_id . ",
                                                  " . $stepId . ",
                                                  '" . $c->status . "',
                                                  '" . $c->datetime . "',
                                                  '" . $c->pageCustomRulesDetailsid . "')");

        }

//			foreach ($customRule as $c) {
//
//				$question_id = "''";
//				if ($c->questionId != "")
//					$question_id = "(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "')";
//
//
//				$choice_id = "''";
//				if ($c->questionChoices != "")
//					$choice_id = "(select pfqc.choiceId from pageformquestionchoices pfqc where pfqc.oldId='" . $c->questionChoices . "' AND pfqc.isactive='1')";
//
//				$plan_id = "''";
//				if ($c->planId)
//					$plan_id = "(select p.planId from plan p where p.oldId='" . $c->planId . "' AND p.userId='" . $userId . "' AND p.isactive='1')";
//				$stepId = "''";
//				if ($c->stepId != "")
//					$stepId = "(select s.stepId from plansteps s where s.oldId='" . $c->stepId . "' AND s.isactive = 1 AND s.isArchive = 0)";
//
//				$database->executeNonQuery("INSERT INTO `pagecustomrulesdetails`
//                                      (`pageCustomRulesid`,
//                                       `questionId`,
//                                       `questionChoices`,
//                                       `groupId`,
//                                       `planId`,
//                                       `stepId`,
//                                       `status`,
//                                       `datetime`,
//                                       `oldId`)
//                               VALUES('" . $profileCustomRuleId . "',
//                                                  " . $question_id . ",
//                                                  " . $choice_id . ",
//                                                  '" . $c->groupId . "',
//                                                  " . $plan_id . ",
//                                                  " . $stepId . ",
//                                                  '" . $c->status . "',
//                                                  '" . $c->datetime . "',
//                                                  '" . $c->pageCustomRulesDetailsid . "')");
//			}

// custom rules
        return 'done';
    }

    function archiveProfileUser($database, $profileId, $userId, $leaderid)
    {

        $database->executeNonQuery("Update `pages` set `status` = '0',`isArchive`='1' where `userId` = '" . $userId . "' and `leaderId` = '" . $leaderid . "' and `oldId` = '" . $profileId . "' and `status` = 1");

        return 'done';
    }

    function publishProfile($database, $profileId)
    {
        $database->executeNonQuery("update `pages` set `privacy` = 1 where `pageId` = '" . $profileId . "'");
    }

    function getProfileName($database, $pageId)
    {
        return $database->executeScalar("SELECT `newCode` FROM `pages` WHERE pageId='" . $pageId . "'");
    }

    function EditProfileForUser($database, $profileId, $userId, $leaderId)
    {
        $orderBy = $database->executeScalar("select `order` from pages WHERE `userId` = '" . $userId . "' and `leaderId` = '" . $leaderId . "' and `oldId` = '" . $profileId . "'");

        $database->executeNonQuery("Update `pages` set `status` = '0' WHERE `userId` = '" . $userId . "' and `leaderId` = '" . $leaderId . "' and `oldId` = '" . $profileId . "' and `status` = 1");

        $parentId = $database->executeScalar("Select `parentId` from `pages` where `pageId` = '" . $profileId . "'");
        if ((empty($parentId)) || (is_null($parentId)) || ($parentId == 0))
            $parentId = $profileId;

        $database->executeNonQuery("insert into pages ( uniqueCode, userId, pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, addDate, pageName, newCode, leaderId, status, privacy, generatedVia, profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount, oldId, parentId,`order`,`accentColor`,`isComplete`,`thankyouVideoVisibility`,`isDefaultVideoshow`) select  uniqueCode,'" . $userId . "', pageTemplateId, formTemplateId, isAutomaticallyAddNewContact, pageBrowserTitle, thankYouMessage, pageHTMLGenerated, formHTMLGenerated, CURDATE(), pageName, newCode, '" . $leaderId . "', 1, 0,2, profileDefaultText, thankyouDefaultText, profileVideoLink, thankyouVideoLink, profileFontStyle, thankyouFontStyle, profileHeader, thankyouHeader, profilebgColor, thankyoubgColor, profileFontColor, thankyouFontColor, columnCount,'" . $profileId . "','" . $parentId . "','" . $orderBy . "',accentColor,`isComplete`,`thankyouVideoVisibility`,`isDefaultVideoshow` from pages where `pageId` = '" . $profileId . "'");

        $count = 0;
        $newProfileId = $database->insertid();

        $database->executeNonQuery("INSERT INTO pagequestions(pageId, questionId,`order`,`mandatory`,`status`, `enableStatus`) SELECT '" . $newProfileId . "',pq.questionId,pq.`order`,pq.`mandatory`,pq.`status`,pq.`enableStatus` FROM pagequestions pq LEFT JOIN questions q ON q.`questionId` = pq.`questionId` WHERE pq.pageId = '" . $profileId . "' AND q.`templateDefault` = 1");

        $questionId = $database->executeObjectList("SELECT q.questionId, pq.mandatory FROM questions q JOIN pagequestions pq ON q.questionId= pq.questionId JOIN questiontype pt ON q.`typeId` = pt.`typeId` WHERE pageId= '" . $profileId . "' AND `q`.`templateDefault` = 0 AND q.status = 1 ORDER BY pq.order ASC");

        // Select p.* from `pagequestions` p join questions q on p.questionId=q.questionId where p.`pageId` = '" . $profileId . "' and q.`templateDefault`='0' AND p.status='1'  order by p.`order` desc");
        foreach ($questionId as $qId) {
            $count++;
            //echo("Insert into `questions` (`question`,`typeId`,`order`,`label`,`templateDefault`,`status`,`datetime`,`oldId`,`enableStatus`) select `question`,`typeId`,`order`,`label`,'0',1,now(),`questionId`,`enableStatus` from `questions` where questionId = '" . $qId->questionId . "' AND `templateDefault`='0' ");
            $database->executeNonQuery("Insert into `questions` (`question`,`typeId`,`order`,`label`,`templateDefault`,`status`,`datetime`,`oldId`,`enableStatus`) select `question`,`typeId`,`order`,`label`,'0',1,now(),`questionId`,`enableStatus` from `questions` where questionId = '" . $qId->questionId . "' AND `templateDefault`='0'");

            $newQuestionId = $database->insertid();
            $database->executeNonQuery("Insert into `pageformquestionchoices` (`questionId`,`choiceText`,`order`,`oldId`,`parentId`) select '" . $newQuestionId . "',`choiceText`, `order`,`choiceId`,if(`parentId`='0',`choiceId`,`parentId`) from `pageformquestionchoices` where `questionId` = '" . $qId->questionId . "'");

// for copy of custom questions.
            $database->executeNonQuery("Insert into `questionradiolabels` (`questionId`,`label`,`order`) select '" . $newQuestionId . "',`label`,`order`
             from `questionradiolabels` where questionId = '" . $qId->questionId . "'");
// end copy of custom questions.

            $database->executeNonQuery("Insert into `pagequestions` (`pageId`, `questionId`,`order`,`mandatory`) values ('" . $newProfileId . "','" . $newQuestionId . "', '" . $count . "','" . $qId->mandatory . "')");

        }
        $database->executeNonQuery("INSERT INTO `pagecustomrules` (`pageId`,`status`,`datetime`,`oldId`) (SELECT '" . $newProfileId . "',`status`,NOW(),pageCustomRulesid FROM `pagecustomrules` WHERE pageId='" . $profileId . "' AND `status`='1')");
        $profileCustomRuleId = $database->insertid();
// custom rules
        $customRule = $database->executeObjectList("SELECT * FROM pagecustomrulesdetails pcrd left join pagecustomrules pcr on pcrd.pageCustomRulesid =pcr.pageCustomRulesid WHERE pcr.`pageId`='" . $profileId . "'");

        foreach ($customRule as $c) {
            $question_id = "''";
            if ((!empty($c->questionId)) && (!($c->questionId == 0)) && (!(is_null($c->questionId))))
                $question_id = "(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "')";

            $choice_id = "''";
            if ((!empty($c->questionChoices)) && (!($c->questionChoices == 0)) && (!(is_null($c->questionChoices))))
                $choice_id = "(select pfqc.choiceId from pageformquestionchoices pfqc where pfqc.oldId='" . $c->questionChoices . "' AND pfqc.isactive='1')";

            $plan_id = "''";
            if ((!empty($c->planId)) && (!($c->planId == 0)) && (!(is_null($c->planId))))
                $plan_id = "(select p.planId from plan p where p.oldId='" . $c->planId . "' AND p.userId='" . $userId . "' AND p.isactive='1' AND p.`isArchive` ='0')";

            $stepId = "''";
            if ((!empty($c->stepId)) && (!($c->stepId == 0)) && (!(is_null($c->stepId))))
                $stepId = "(SELECT s.stepId FROM plansteps s JOIN plan p ON s.`planId` = p.planid WHERE s.oldId='" . $c->stepId . "' AND p.userId = '" . $userId . "' AND p.`isactive` = '1' AND p.`isArchive` ='0')";
//							$stepId = "(select s.stepId from plansteps s where s.oldId='" . $c->stepId . "')";

            if ($leaderId == 0) {
                $groupId = $database->executeScalar("SELECT groupId FROM groups WHERE parentId = '" . $c->groupId . "' AND userId = '" . $userId . "'");
            } else {
                $groupId = $c->groupId;
            }
            $database->executeNonQuery("INSERT INTO `pagecustomrulesdetails`
                                      (`pageCustomRulesid`,
                                       `questionId`,
                                       `questionChoices`,
                                       `groupId`,
                                       `planId`,
                                       `stepId`,
                                       `status`,
                                       `datetime`,
                                       `oldId`)
                               VALUES('" . $profileCustomRuleId . "',
                                                  " . $question_id . ",
                                                  " . $choice_id . ",
                                                  '" . $groupId . "',
                                                  " . $plan_id . ",
                                                  " . $stepId . ",
                                                  '" . $c->status . "',
                                                  '" . $c->datetime . "',
                                                  '" . $c->pageCustomRulesDetailsid . "')");

        }

//			foreach ($customRule as $c) {
//
//				$question_id = "''";
//				if ($c->questionId != "")
//					$question_id = "(SELECT q.questionId FROM questions q LEFT JOIN pagequestions pq ON q.questionId=pq.questionId WHERE q.oldId='" . $c->questionId . "'  AND q.status='1' AND pq.pageId='" . $newProfileId . "')";
//
//
//				$choice_id = "''";
//				if ($c->questionChoices != "")
//					$choice_id = "(select pfqc.choiceId from pageformquestionchoices pfqc where pfqc.oldId='" . $c->questionChoices . "' AND pfqc.isactive='1')";
//
//				$plan_id = "''";
//				if ($c->planId)
//					$plan_id = "(select p.planId from plan p where p.oldId='" . $c->planId . "' AND p.userId='" . $userId . "' AND p.isactive='1')";
//				$stepId = "''";
//				if ($c->stepId != "")
//					$stepId = "(select s.stepId from plansteps s where s.oldId='" . $c->stepId . "' AND s.isactive = 1 AND s.isArchive = 0)";
//
//				$database->executeNonQuery("INSERT INTO `pagecustomrulesdetails`
//                                      (`pageCustomRulesid`,
//                                       `questionId`,
//                                       `questionChoices`,
//                                       `groupId`,
//                                       `planId`,
//                                       `stepId`,
//                                       `status`,
//                                       `datetime`,
//                                       `oldId`)
//                               VALUES('" . $profileCustomRuleId . "',
//                                                  " . $question_id . ",
//                                                  " . $choice_id . ",
//                                                  '" . $c->groupId . "',
//                                                  " . $plan_id . ",
//                                                  " . $stepId . ",
//                                                  '" . $c->status . "',
//                                                  '" . $c->datetime . "',
//                                                  '" . $c->pageCustomRulesDetailsid . "')");
//			}
// custom rules

//			$database->executeNonQuery("INSERT INTO `pagecustomrulesdetails` (`pageCustomRulesid`,`questionId`,`questionChoices`,`groupId`,`planId`,`stepId`,`status`,`datetime`,`oldId`) SELECT '".$profileCustomRuleId."',`questionId`,`questionChoices`,`groupId`,`planId`,`stepId`,`status`,`datetime`,`pageCustomRulesDetailsid` FROM `pagecustomrulesdetails` WHERE `questionId` IS NULL AND `pageCustomRulesid`=(select `pageCustomRulesid` FROM `pagecustomrules` WHERE pageId='".$profileId."' AND `status`='1')");
        return 'done';

    }

    function getPagecustomRulesId($database, $pageId)
    {

        return $database->executeScalar("SELECT `pagecustomrulesid` FROM `pagecustomrules` WHERE pageId='" . $pageId . "'");
    }

    function getGroupId($database, $customrulesId)
    {

        return $database->executeObjectList("SELECT `groupId` FROM `pagecustomrulesdetails` WHERE pagecustomrulesid='" . $customrulesId . "'");
    }

    function getPlanId($database, $customrulesId)
    {

        return $database->executeObjectList("SELECT `planId` FROM `pagecustomrulesdetails` WHERE pagecustomrulesid='" . $customrulesId . "'");
    }

    function getselectedtemplate($database, $pageId)
    {

        return $database->executeObject("SELECT p.`pageTemplateId`,pt.`thumbnail` FROM `pages` AS p LEFT JOIN pagetemplate AS pt ON p.`pageTemplateId`=pt.`pageTemplateId` WHERE p.`pageId`='" . $pageId . "'");
    }

    function updateProfileOrder($profiles)
    {

        $database = new database();
        $where;
        $profilesIds;
        $count = 0;
        $profiles = str_replace(',,', ',', $profiles);
        $profiles = str_replace(',,', ',', $profiles);
        $profiles = str_replace('pId', '', $profiles);
        $profilesExplode = explode(',', $profiles);
        foreach ($profilesExplode as $Pe) {

            if (!(empty($Pe))) {

                if (empty($profilesIds))
                    $profilesIds = $Pe;
                else
                    $profilesIds .= ',' . $Pe;
                $count++;
                $where .= " when `pageId` = '" . $Pe . "' then '" . $count . "'";
            }
        }

        $database->executeNonQuery("update `pages` set `order` = case " . $where . " end where `pageId` in (" . $profilesIds . ")");

    }

    function getCustomQuestionStatus($database, $pageId)
    {
        return $database->executeObjectList("SELECT pq.* FROM  `pagequestions` pq WHERE pq.`pageId` = '" . $pageId . "'  ORDER BY pq.`order` ASC");
    }

    function getcustomMailQuestionStatus($database, $pageId)
    {
        return $database->executeObjectList("SELECT pq.* FROM  `pagequestions` pq WHERE pq.`pageId` = '" . $pageId . "' AND (
										(questionId = 123 AND `status` = 1) OR
										(questionId = 58 AND `status` = 1) OR
										(questionId = 124 AND `status` = 1) OR
										(questionId = 125 AND `status` = 1) OR
										(questionId = 57 AND `status` = 1))
										ORDER BY pq.`order` ASC");
    }

    function updateCurrentStepId($database, $pageId, $currentStepId)
    {
        $database->executeNonQuery("update `pages` set `currentStepId` = '" . $currentStepId . "' where `pageId` = '" . $pageId . "'");
    }

    function markDiscard($database, $profileId, $nameProfile, $userId)
    {
        $name = 'TemP_' . $nameProfile;

        return $database->executeNonQuery("update `pages` set `newCode` = '" . $name . "',`status`='0',`isComplete`='1' where `pageId` = '" . $profileId . "' and userId='" . $userId . "'");

    }

    function checkUserExpiry($database, $userName)
    {

        return $database->executeObject("SELECT u.userId,up.`expiryDate` FROM `user` u JOIN `userpayments` up ON u.`userId`=up.`userId` WHERE u.isVerifiedEmail='1' AND u.`isVerifiedPaypal`='1' AND up.`expiryDate` >= CURDATE() AND u
            .`userName`='" . $userName . "'");

    }

    function editVideoForVENVPsTemplate($database, $editedVideo, $pageId, $userId)
    {

        return $database->executeNonQuery("update `pages` set `profileEditedVideo` = '" . $editedVideo . "' where `pageId` = '" . $pageId . "' and userId='" . $userId . "'");

    }

}

?>