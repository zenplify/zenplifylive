<?php
/**
* Template Name: Ontraport Order Form -- Zap
*/
ob_start();
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->

<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<!-- Check If user is anthentic -->
<?php if (! is_user_logged_in()): ?>
	<?php wp_redirect( home_url() ); exit; ?>
<?php endif; ?>
<body <?php body_class(); ?>>
	<header id="masthead" class="site-header" role="banner" style="height: 100px; background-size: auto !important;">
		<a class="home-link" href="javascript:;" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</a>
	</header>
	<div class="container-ontraport">
		<div id="items-container" style="padding-top: 50px;">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<div class="clear-fix"></div>			
		<?php endwhile; ?>
		</div>
	</div>
	<?php wp_footer(); ?>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
    <script>


        var contentType ="application/x-www-form-urlencoded; charset=utf-8";
 
	    if(window.XDomainRequest) //for IE8,IE9
    	   contentType = "text/plain";

        $.validator.setDefaults({
            debug: true
        });

        var emailVal = "", nameVal = "";

        var isValidEmail = false;
        var isValidName = false;

        $(document).ready(function () {

            $('#form').validate({
                onfocusout: function (element) {
                    this.element(element);
                },
                onkeyup: function (element) {

                    if ($(element).hasClass('keyup')) {
                        this.element(element);
                    }

                },
                rules: {
                    firstname: "required",
                    lastname: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    f1427: {
                        required: true
                    },
                    billing_address1: "required",
                    billing_city: "required",
                    billing_zip: "required",
                    billing_state: "required",
                    billing_country: "required",
                    payment_number: {
                        required: true,
                        creditcard: true
                    },
                    payment_code: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    payment_expire_month: "required",
                    payment_expire_year: "required"
                },
                messages: {
                    firstname: "Please enter first name.",
                    lastname: "Please enter last name.",
                    email: {
                        required: "Please enter email address.",
                        email: "Please enter a valid email address."
                    },
                    f1427: {
                        required: "Please enter unique name."
                    },
                    billing_address1: "Please enter your billing address.",
                    billing_city: "Please enter your billing city.",
                    billing_zip: "Please enter your billing zip.",
                    billing_state: "Please select your billing state.",
                    billing_country: "Please select your billing country.",
                    payment_number: {
                        required: "Please enter a credit card number.",
                        creditcard: "Please enter a valid credit card number."
                    },
                    payment_code: {
                        required: "Please enter a valid CVC.",
                        digits: "Please enter a valid CVC.",
                        minlength: "Please enter a valid CVC.",
                        maxlength: "Please enter a valid CVC."
                    },
                    payment_expire_month: "Please select a valid expiry month.",
                    payment_expire_year: "Please select a valid expiry year."
                }
            });


            $.validator.addMethod("validateUniqueName", function (value, element) {
                var data = {action: 'username', uname: value, type: 'json'};
                nameVal = value;
                if (value) {
                    $.ajax({
                        type: "POST",
                        url: 'https://zenplify.biz/modules/ontraport/check.php',
                        dataType: "json",
                        contentType: contentType,
                        data: data,
                        success: function (response) {
                            if (response == 1) {
                                // Add error class
                                $('#mr-field-element-78557934379').addClass('error');
                                // Show to error to the user
                                $('#mr-field-element-78557934379-error').html("Unique name is already in use.");
                                // Invalidate the unique name field
                                $('#mr-field-element-78557934379').attr('aria-invalid', true);
                                isValidName = false;
                            } else {
                                // Remove error class from unique name field
                                $('#mr-field-element-78557934379').removeClass('error');
                                // Add a valid class
                                $('#mr-field-element-78557934379').addClass('valid');
                                // Show success message to the user
                                $('#mr-field-element-78557934379-error').html("<span style='color: green;'>Unique name is available.</span>");
                                // Validate the unique name field
                                $('#mr-field-element-78557934379').attr('aria-invalid', false);

                                isValidName = true;
                            }
                        }
                    });
                }

            }, '');

            $(':input[name="f1427"]').rules("add", {"validateUniqueName": true});

            $.validator.addMethod("validateUniqueEmail", function (value, element) {

                var data = {action: 'email', email: value, type: 'json'};
                emailVal = value;
                $.ajax({
                    type: "POST",
                    url: 'https://zenplify.biz/modules/ontraport/check.php',
                    dataType: "json",
                    contentType: contentType,
                    data: data,
                    success: function (response) {
                        if (response == 1) {
                            // Add error class
                            $('#mr-field-element-127862499561').addClass('error');
                            // Enable Unique name field
                            $('#mr-field-element-78557934379').prop('disabled', false);
                            // Show error to the user
                            $('#mr-field-element-127862499561-error').html('<span style="color: red;">Email Address is already in use.</span>');
                            // Invalidate the email field
                            $('#mr-field-element-127862499561').attr('aria-invalid', true);
                            isValidEmail = false;
                        } else if (response == 0) {
                            
                            // Enable Unique name field
                            $('#mr-field-element-78557934379').prop('disabled', false);
                            // Remove error class from email field
                            $('#mr-field-element-127862499561').removeClass('error');
                            // Show success message to the user
                            $('#mr-field-element-127862499561-error').html('<span style="color: green;">Email Address is available.</span>');
                            // Validate the email field
                            $('#mr-field-element-127862499561').attr('aria-invalid', false);
                            isValidEmail = true;
                        }
                        else {
                            response = response.split(',');
                            // Remove error class from email field
                            $('#mr-field-element-127862499561').removeClass('error');
                            // Remove error class from unique name field
                            $('#mr-field-element-78557934379').removeClass('error');
                            // Disable the unique name field
                            $('#mr-field-element-78557934379').val(response[1]).prop('disabled', true);
                            // Validate the email field
                            $('#mr-field-element-127862499561').attr('aria-invalid', false);
                            // Hide the error message for unique name
                            $('#mr-field-element-78557934379-error').hide();
                            // Hide the error message for email
                            $('#mr-field-element-127862499561-error').hide();
                            isValidEmail = true;
                        }
                    }
                });

            }, '');

            $(':input[name="email"]').rules("add", {"validateUniqueEmail": true});

            $('#mr-field-element-421455579577').on('click', function () {
                if (isValidEmail == false || isValidName == false) {
                    if (isValidEmail == false) {
                        $(':input[name="email"]').focus();
                    } else {
                        $(':input[name="f1427"]').focus();
                    }
                    return false;
                }
                return true;
            });

        });
    </script>
</body>
</html>

