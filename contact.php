<?php
require_once ('init.php');

class contact{
	
	/********************** imran Code******************/
	
	function GetUserContacts($user_id)
	{
		$database = new database();
		$contacts=$database->executeObjectList("SELECT * FROM contact where userId='".$user_id."' AND isActive=1 order by contactId desc");
		return $contacts;
	}
	
	
	function showContact(){
		$database = new database();
		
		return 	$database->executeObjectList("SELECT * FROM contact");
		}
	
	
	//phone number for contact
	public function addContactPhoneNumberByContactId1($contactId,$phone){
		$database = new database();
		($phone[2]=='on'?$phoneNumberIsPrimary=1:$phoneNumberIsPrimary=0);
		$phoneaddedDate		=date("Y-n-j");
		
		$database->executeNonQuery("INSERT INTO contactphonenumbers 
									SET contactId='".$contactId."',
										typeId='".$phone[0]."',
										phoneNumber='".$phone[1]."',
										isPrimary='".$phoneNumberIsPrimary."',
										addedDate='".$phoneaddedDate."'");
		}
		
	public function addContactEmailByContactId1($contactId,$email){
		$database = new database();
		($email[2]=='on'?$emailIsPrimary=1:$emailIsPrimary=0);
		$emailaddedDate		=date("Y-n-j");
		
		$database->executeNonQuery("INSERT INTO emailcontacts 
									SET contactId='".$contactId."',
									typeId='".$email[0]."',
										email='".$email[1]."',
										isPrimary='".$emailIsPrimary."',
										addedDate='".$emailaddedDate."'");
		}
	
	public function addContactWebsiteByContactId1($contactId,$website){
		$database = new database();
		($website[2]=='on'?$websiteIsPrimary=1:$websiteIsPrimary=0);
		$websiteaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactwebsites 
									SET contactId='".$contactId."',
										typeId='".$website[0]."',
										website='".$website[1]."',
										isPrimary='".$websiteIsPrimary."',
										addedDate='".$websiteaddedDate."'");		
	}
	//Address detail for contact

	public function addContactAddressByContactId1($contactId,$address){
		$database = new database();
		$addressaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactaddresses 
									SET contactId='".$contactId."',
										typeId='".$address[0]. "',
										street='".$address[1]. "',
										zipcode='".$address[2]. "',
										state='".$address[3]. "',
										city='".$address[4]. "',
										country='".$address[5]. "',
										addedDate='".$addressaddedDate."'");
	}
	//Assign Group to Contact
	function assignGroupTo1($contactId,$groups){
		$database = new database();
		$count=sizeof($groups['groupcheckbox']);
		$group=0;
		while($count>0){
			$database->executeNonQuery("INSERT INTO groupcontacts 
										SET groupId='". $groups['groupcheckbox'][$group]."',
											contactId='".$contactId."'");
			$count--;
			$group++;
			}
			
	}
	//Assign Plan Steps to Contact
	function assignPlanStep1($contactId,$planSteps){
		$database = new database();
		return $count=sizeof($planSteps['planStepDropDown']);
		
		$step=0;
		while($count>0){
			$database->executeNonQuery("INSERT INTO planstepusers 
										SET stepId='". $planSteps['planStepDropDown'][$step]."',
											contactId='".$contactId."'");
			$count--;
			$step++;
			}
		
		
		}
	public function showCustomFields1($userId){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM usercontactcustomdata WHERE userId='".$userId."'");
		}
	public function showCustomFieldsById1($userId,$customFieldId){
		//return $database->executeObject
		$database = new database();
		return ("SELECT * FROM usercontactcustomdata WHERE userId='".$userId."' AND customDataId='".$customFieldId."'");
		}
	
	/*contact details	*/
	function showContactDetail1($contactId){
		$database = new database();
		return 	$database->executeObject("SELECT * FROM contact WHERE contactId='".$contactId."'");
		
		}
	function showContactPhoneNumber1($contactId){
		$database = new database();
		return 	$database->executeObjectList("SELECT * FROM contactphonenumbers WHERE contactId='".$contactId."'");
		
		
		}	
	function showContactEmailAddress1($contactId){
		$database = new database();
		return 	$database->executeObjectList("SELECT * FROM emailcontacts WHERE contactId='".$contactId."'");
		
		
		}
	function showContactWebsite1($contactId){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='".$contactId."'");
		
		
		}
	function showContactAddress1($contactId){
		$database = new database();
		return 	$database->executeObjectList("SELECT * FROM contactaddresses WHERE contactId='".$contactId."'");
		
		
		}

	function GetAllContacts($userId)
	{
		$database = new database();
		$contacts=$database->executeObjectList("SELECT c.contactId, firstName, phoneNumber FROM contact c LEFT JOIN contactphonenumbers cp on c.contactId=cp.contactId where userId='".$userId."' AND c.isActive=1");
		return $contacts;
		//echo "SELECT * FROM contact where contactId='".$userId."'";
	}
	function DeleteContact($id)
	{
		$database = new database();
		$database->executeNonQuery("delete from contact where contactId='".$id."'");
	}
	function AddNewContact($postArray)
	{
		//Add contact personal info
		$title=$postArray['title'];
		$firstname=$postArray['firstname'];
		$lastname=$postArray['lastname'];
		$companyname=$postArray['companyname'];
		$jobtitle=$postArray['jobtitle'];
		$birthday=$postArray['birthday'];
		$anniversary=$postArray['anniversary'];
		$Notes=$postArray['Notes'];
		$isActive="1";
		$user_id="1";
		
		
		
		if (!empty($postArray))
		{
			$database = new database();
			$database->executeNonQuery("INSERT INTO contact
								 	SET
									userId='".$user_id."',
									firstName='".$firstname."',
									lastName='".$lastname."',
									title='".$title."',
									companyTitle='".$companyname."',
									jobTitle='".$jobtitle."',
									dateOfBirth='".$birthday."',
									anniversary='".$anniversary."',
									addedDate=now(),
									notes='".$Notes."',
									isActive='".$isActive."'
					");
		}
		
		
		$contact_id=$database->insertid();
		
		
		
		$phoneNumbers=$postArray['numberOfPhoneNumbers'];
		if($phoneNumbers==0)
		{
				
		}
		else
		{
			$numberOfPhoneNumbers=explode(',',$phoneNumbers);
			
				for($p=0;$p<=sizeof($numberOfPhoneNumbers);$p++)
					{
						$phoneDetail=array($postArray['phoneType_'.$numberOfPhoneNumbers[$p]],
						$postArray['number_'.$numberOfPhoneNumbers[$p]],
						$postArray['phoneNumberPrimary'.$numberOfPhoneNumbers[$p]]);
						contacts::addContactPhoneNumberByContactId($contact_id,$phoneDetail);
					}
				
		}
		
		
		$emailAddress=$postArray['numberOfEmailAddress'];
		$numberOfEmailAddress=explode(',',$emailAddress);
		
		for($e=0;$e<sizeof($numberOfEmailAddress);$e++)
			{
				$emailDetail=array($postArray['emailType_'.$numberOfEmailAddress[$e]],
					$postArray['emailaddress_'.$numberOfEmailAddress[$e]],
					$postArray['primaryEmail_'.$numberOfEmailAddress[$e]]);
				//print_r($emailDetail);
				contacts::addContactEmailByContactId($contact_id,$emailDetail);
			}
			
			
		$websiteAddress=$postArray['numberOfWebsiteAddress'];
		$numberOfWebsiteAddress=explode(',',$websiteAddress);
		
		for($w=0;$w<sizeof($numberOfWebsiteAddress);$w++)
			{
				$websiteDetail=array($postArray['websiteType_'.$numberOfWebsiteAddress[$w]],
						$postArray['websiteAddress_'.$numberOfWebsiteAddress[$w]],
						$postArray['primaryWebsiteAddress_'.$numberOfWebsiteAddress[$w]]);
				//print_r($emailDetail);
				$contact->addContactWebsiteByContactId($contact_id,$websiteDetail);
			}

		$Address=$postArray['numberOfAddress'];
		$numberOfAddress=explode(',',$Address);
		
		for($a=0;$a<sizeof($numberOfAddress);$a++){
			$addressDetail=array($postArray['addressType_'.$numberOfAddress[$a]],
					$postArray['street_'.$numberOfAddress[$a]],
					$postArray['zipCode_'.$numberOfAddress[$a]],
					$postArray['state_'.$numberOfAddress[$a]],
					$postArray['city_'.$numberOfAddress[$a]],
					$postArray['country_'.$numberOfAddress[$a]]
			);
			//print_r($addressDetail);
			$contact->addContactAddressByContactId($contact_id,$addressDetail);
		}
		
	}
	/*
	function addContactPhoneNumberByContactId($contactId,$phone)
	{
		$database = new database();
		($phone[2]=='on'?$phoneNumberIsPrimary=1:$phoneNumberIsPrimary=0);
		$phoneaddedDate		=date("Y-n-j");
		$database = new database();
		$database->executeNonQuery("INSERT INTO contactphonenumbers
									SET contactId='".$contactId."',
										typeId='".$phone[0]."',
										phoneNumber='".$phone[1]."',
										isPrimary='".$phoneNumberIsPrimary."',
										addedDate='".$phoneaddedDate."'");
	}
	function addContactEmailByContactId($contactId,$email)
	{
		$database = new database();
		($email[2]=='on'?$emailIsPrimary=1:$emailIsPrimary=0);
		$emailaddedDate		=date("Y-n-j");
	
		$database->executeNonQuery("INSERT INTO emailcontacts
									SET contactId='".$contactId."',
									typeId='".$email[0]."',
										email='".$email[1]."',
										isPrimary='".$emailIsPrimary."',
										addedDate='".$emailaddedDate."'");
	}
	
	function addContactWebsiteByContactId($contactId,$website)
	{
		$database = new database();
		($website[2]=='on'?$websiteIsPrimary=1:$websiteIsPrimary=0);
		$websiteaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactwebsites
									SET contactId='".$contactId."',
										typeId='".$website[0]."',
										website='".$website[1]."',
										isPrimary='".$websiteIsPrimary."',
										addedDate='".$websiteaddedDate."'");
	}
	//Address detail for contact
	
	function addContactAddressByContactId($contactId,$address)
	{
		$database = new database();
		$addressaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactaddresses
									SET contactId='".$contactId."',
										typeId='".$address[0]. "',
										street='".$address[1]. "',
										zipcode='".$address[2]. "',
										state='".$address[3]. "',
										city='".$address[4]. "',
										country='".$address[5]. "',
										addedDate='".$addressaddedDate."'");
	}
	*/
	function GetContactDetail($contact_id)
	{
		$database = new database();
		$detail=$database->executeObject("SELECT * FROM contact WHERE contactId=".$contact_id."");
		return $detail;
	}
	function GetContactPhoneNumbers($contact_id)
	{
		$database = new database();
		$detail=$database->executeObjectList("SELECT * FROM contactphonenumbers WHERE contactId='".$contact_id."'");
		return $detail;
	}
	function GetContactEmailAddress($contact_id)
	{
		$database = new database();
		$detail=$database->executeObjectList("SELECT * FROM emailcontacts WHERE contactId='".$contact_id."'");
		return $detail;
	}
	function GetContactWebsites($contact_id)
	{
		$database = new database();
		$detail=$database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='".$contact_id."'");
		return $detail;
	}
	function GetContactUserAddress($contact_id)
	{
		$database = new database();
		$detail=$database->executeObjectList("SELECT * FROM contactaddresses WHERE contactId='".$contact_id."'");
		return $detail;
	}
	function GetAllPermissions()
	{
		$database = new database();
		$detail=$database->executeObjectList("SELECT * FROM contactpermissionrequeststatus");
		return $detail;
	}
	function GetContactPermissionStatus($contact_id)
	{
		$database = new database();
		$detail=$database->executeObject("select cpr.statusId,cprs.title as title
												from contactpermissionrequests cpr
												inner join contactpermissionrequeststatus cprs on cpr.statusId=cprs.statusId 
												where cpr.toContactId='".$contact_id."'"
											);
		//return $detail;
		return $detail->title;
	}
	
	function AddTransferContact($user_id,$contacts,$Suser)
	{
		
		
		//$Suser is send request user
		$database = new database();
		$requestDate = date("Y-m-d");

		foreach ($contacts as $id=>$val)
		{
			$database->executeNonQuery("INSERT INTO contacttransferrequest
								 SET userId='".$Suser."',
									contactId='".$val."',
									requestToUserId='".$user_id."',
									requestedDate='".$requestDate."',
									statusId='1',
									approvedDate='',
									rejectedDate='',
									newContactId=''");
		
		}
		
	}
	function GetAllTransferContacts($user_id)
	{
		$database = new database();
		$contacts=$database->executeObjectList("SELECT * FROM contacttransferrequest where requestToUserId='".$user_id."' and statusId='1'");
		return $contacts;
	
	}
	
	function GetContactName($contact_id)
	{
		$database = new database();
		//$detail=$database->executeObjectList("SELECT * FROM contact WHERE contactId='".$contact_id."'");
		$detail=$database->executeObject("SELECT firstName FROM contact WHERE contactId='".$contact_id."'");
		return $detail;
	}
	function GetUserName($user_id)
	{
		$database = new database();
		//$detail=$database->executeObjectList("SELECT * FROM contact WHERE contactId='".$contact_id."'");
		$detail=$database->executeObject("SELECT firstName,lastName FROM user WHERE userId='".$user_id."'");
		return $detail;
	}
	function AcceptTransferContact($contacts,$user_id)
	{
		$database = new database();
		$approveDate = date("Y-m-d");
		
		foreach ($contacts as $id=>$val)
		{
			//SELECT * FROM source_table WHERE id='7';
			//UPDATE temp_table SET id='100' WHERE id='7';
			//INSERT INTO source_table SELECT * FROM temp_table;
			//DROP TEMPORARY TABLE temp_table;
			
			
			
			$database->executeNonQuery("INSERT INTO contact (userId, firstName,lastName,title,companyTitle,
										jobTitle,dateOfBirth,notes,isActive)
										select '".$user_id."' as id, firstName,lastName,title,companyTitle,jobTitle,dateOfBirth,notes,isActive
										from contact where contactId='$val'");
			
			
			$contactId=$database->insertid();
			
			$database->executeNonQuery("INSERT INTO contactaddresses
			(contactId, street, zipcode, state, city, country, typeId, addedDate)
			select '".$contactId."' as id, street, zipcode, state, city, country, typeId, CURDATE() as adddate from contactaddresses where contactId='$val'");
			
			$database->executeNonQuery("INSERT INTO contactcalltime(contactId, weekdays, weekdayEvening, weekenddays, weekenddayEvening, addedDate)
			select '".$contactId."' as id, weekdays, weekdayEvening, weekenddays, weekenddayEvening, CURDATE() as adddate from contactcalltime where contactId='$val'");
			
			$database->executeNonQuery("INSERT INTO contactphonenumbers(contactId, typeId, phoneNumber, isPrimary, addedDate)
			 select '".$contactId."' as id, typeId, phoneNumber, isPrimary, CURDATE() as adddate from contactphonenumbers where contactId='$val'");
			 
			 $database->executeNonQuery("INSERT INTO contactnotes( contactId, notes, addedDate, lastModifiedDate, isActive)
			 select '".$contactId."' as id,  notes, CURDATE() as addeddate, CURDATE() as lastmodifieddate, isActive from contactnotes where contactId='$val'");
			
			$database->executeNonQuery("INSERT INTO contactwebsites(contactId, typeId, website, isprimary, addedDate)
			 select '".$contactId."' as id,  typeId, website, isprimary, CURDATE() as addeddate from contactwebsites where contactId='$val'");
			
		/*	$database->executeNonQuery("UPDATE  contact SET
														statusId='2',
												   		addedDate='".$approveDate."'
														WHERE contactId='".$contactId."'");*/
				
			
			
			$database->executeNonQuery("UPDATE  contacttransferrequest SET
														statusId='2',
												   		approvedDate='".$approveDate."'
														WHERE contactId='".$val."'");
		
		}
			/*$database->executeNonQuery("UPDATE  groups SET
														groupTypeId='".$group['groupCategory']."',
												   		name='".$group['groupTitle']."',
												   		description='".$group['groupDescription']."',
												   		color='".$group['color']."',
												   		orderBy='".$group['groupOrder']."'
												  WHERE groupId='".$groupId."'");
		return ($database->getAffectedRows()!=''?true:false);
		}*/
	}
	
	function RejectTransferContact($contacts)
	{
		$database = new database();
		$rejecttDate = date("Y-m-d");
		
		foreach ($contacts as $id=>$val)
		{
					$database->executeNonQuery("UPDATE  contacttransferrequest SET
														statusId='3',
												   		rejectedDate='".$rejecttDate."'
														WHERE contactId='".$val."'");
			
		}
		return ($database->getAffectedRows()!=''?true:false);
	}
	
	
	
	
	
	
	
	
	//Junaid code
	
	public function addContact($user){
		$database = new database();
		if(empty($user['dateOfBirth'])){
			$dateOfBirth='0000-00-00 00:00:00';
			
			}else{
		$dateOfBirth= date('Y-m-d', strtotime($user['dateOfBirth'])); // outputs 2006-01-24  
			}
		$addedDate = date("Y-m-d");
		//custom data for contact 
		$userId	= $_SESSION["userId"];
		$database->executeNonQuery("INSERT INTO contact
								  SET userId='".$userId."',
									title='".$user['title']."',
									firstName='".$user['firstname']."',
									lastName='".$user['lastname']."',
									companyTitle='".$user['companyname']."',
									jobTitle='".$user['jobtitle']."',
									dateOfBirth='".$dateOfBirth."',
									referredBy='".$user['referred_by']."',
									addedDate='".$addedDate."',
									isActive=1");	
		return $database->insertid();
		}
	
	//phone number for contact
	public function addContactPhoneNumberByContactId($contactId,$phone){
		$database = new database();
		$phoneaddedDate		=date("Y-n-j");
		
		$database->executeNonQuery("INSERT INTO contactphonenumbers 
									SET contactId='".$contactId."',
										phoneNumber='".$phone['phone']."',
										addedDate='".$phoneaddedDate."'");
		}
		
	public function addContactEmailByContactId($contactId,$email){
		$database = new database();
		$emailaddedDate		=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO emailcontacts 
									SET contactId='".$contactId."',
										email='".$email['email']."',
										addedDate='".$emailaddedDate."'");
		}
		
	public function addContactcallTimeByContactId($contactId,$calltime){
		$database = new database();
		$calltimeaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactcalltime 
									SET contactId='".$contactId."',
										weekdays='".$calltime['BestTimestoCallWeekdays']."',
										weekdayEvening='".$calltime['BestTimestoCallWeekdayEvenings']."',
										weekenddays='".$calltime['BestTimestoCallWeekendDays']."',
										weekenddayEvening='".$calltime['BestTimestoCallWeekendEvening']."',
										addedDate='".$calltimeaddedDate."'");		
	}
	//Address detail for contact

	public function addContactAddressByContactId($contactId,$address){
		$database = new database();
		$addressaddedDate	=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO contactaddresses 
									SET contactId='".$contactId."',
										street='".$address['street']. "',
										zipcode='".$address['zip']."',
										state='".$address['state']. "',
										city='".$address['city']. "',
										country='".$address['country']. "',
										addedDate='".$addressaddedDate."'");
	}
	//Assign Group to Contact
	function assignGroupTo($contactId,$groups){
		$database = new database();
		$count=sizeof($groups['groupcheckbox']);
		$group=0;
		while($count>0){
			$database->executeNonQuery("INSERT INTO groupcontacts 
										SET groupId='". $groups['groupcheckbox'][$group]."',
											contactId='".$contactId."'");
			$count--;
			$group++;
			}
			
	}
	//Assign Plan Steps to Contact
	function assignPlanStep($userId,$contactId,$planSteps){
		$database = new database();
		//today date			
		$date =date('Y-m-d');
		$date = strtotime($date);


		$count=sizeof($planSteps['plan']); 
		if($count!=0){
			$step=0;
			
			while($count>0){
				
	
				$database->executeNonQuery("INSERT INTO planstepusers SET stepId='".$planSteps['planStepDropDown'][$step]."',contactId='".$contactId."'");
				$stepDesc=$database->executeObject("SELECT * FROM plansteps WHERE stepId='".$planSteps['planStepDropDown'][$step]."'");
				$dueDate = strtotime("+".$stepDesc->daysAfter." day", $date);//add days in date
				$database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('".$userId	."','".$stepDesc->summary."','0','','','".date('Y-m-d',$dueDate)."','0','".$date."','0','0','".$stepDesc->planId."','','0','1')");
				$taskId=$database->insertid();	
				$database->executeNonQuery("INSERT INTO taskcontacts SET contactId='".$contactId."',taskId='".$taskId."'");
				$count--;
				$step++;
				}

					}
		
		}
	public function showCustomFields($userId){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM usercontactcustomdata WHERE userId='".$userId."'");
		}
	public function showCustomFieldsById($userId,$customFieldId){
		//return $database->executeObject
		$database = new database();
		return ("SELECT * FROM usercontactcustomdata WHERE userId='".$userId."' AND customDataId='".$customFieldId."'");
		}
	
	/*contact details	*/
	function showContactDetail($contactId){
		$database = new database();
		return 	$database->executeObject("SELECT * FROM contact WHERE contactId='".$contactId."'");
		
		}
	function showContactPhoneNumber($contactId){
		$database = new database();
		return 	$database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='".$contactId."'");
		
		
		}	
	function showContactEmailAddress($contactId){
		$database = new database();
		return 	$database->executeObject("SELECT * FROM emailcontacts WHERE contactId='".$contactId."'");
		
		
		}
	function showContactWebsite($contactId){
		$database = new database();
		return $database->executeObjectList("SELECT * FROM contactwebsites WHERE contactId='".$contactId."'");
		
		
		}
	function showContactAddress($contactId){
		$database = new database();
		return 	$database->executeObject("SELECT * FROM contactaddresses WHERE contactId='".$contactId."'");
		
		
		}
	function showContactTimeForCall($contactId){
			$database = new database();
			return 	$database->executeObject("SELECT * FROM contactcalltime WHERE contactId='".$contactId."'");
		
		
		}
	
//contact list for auto search
	
	function getContactList($name){
			$database=new database();
			return $database->executeObjectList("SELECT firstName ,lastName ,contactId FROM contact WHERE firstName LIKE '%$name%' OR lastName LIKE '%$name%'");
				
		}
	function editContactDetail($contactId){
		$database=new database();
			return $database->executeObject("SELECT * FROM contact WHERE contactId='".$contactId."'");
				
		}
	function editContactPhone($contactId){
		$database=new database();
			return $database->executeObject("SELECT * FROM contactphonenumbers WHERE contactId='".$contactId."'");
		
		}	
	function editContactEmail($contactId){
		$database=new database();
			return $database->executeObject("SELECT * FROM emailcontacts WHERE contactId='".$contactId."'");
		
		}		
	function editContactAddress($contactId){
		$database=new database();
			return $database->executeObject("SELECT * FROM contactaddresses WHERE contactId='".$contactId."'");
				
		}		
	function editContactCallTime($contactId){
		$database=new database();
			return $database->executeObject("SELECT * FROM contactcalltime WHERE contactId='".$contactId."'");
		}
	function editContactGroup($groupId,$contactId){
			$database=new database();
			$result=$database->executeObject("SELECT * FROM groupcontacts WHERE contactId='".$contactId."' AND groupId='".$groupId."'");
		return(!empty($result)?1:0);
		}
	//contcta for manage contacts
	function manageContacts($userId,$groupId){
		$database=new database();
		return $database->executeObjectList("SELECT * FROM contact WHERE userId='".$userId."' AND contactId NOT IN (SELECT contactId FROM groupcontacts WHERE groupId=".$groupId.")");
		
	}
	function manageGroupsContacts($userId,$groupId){
		$database=new database();
		return $database->executeObjectList("SELECT * FROM contact WHERE userId='".$userId."' AND contactId IN (SELECT contactId FROM groupcontacts WHERE groupId=".$groupId.")");
	
	}
	
	function manageAddContactsToGroup($contactId,$groupId){
		$database=new database();
		$database->executeNonquery("INSERT INTO groupcontacts SET groupId=".$groupId.",contactId=".$contactId."");
		return ($database->getAffectedRows()!=''?1:0);
	}
	function removeContactFromGroups($groupId){
		$database=new database();
		return $database->executeNonquery("DELETE FROM groupcontacts WHERE groupId='".$groupId."'");
		}
	function manageDeleteContactsFromGroup($groupId,$contactId){
		$database=new database();
		return $database->executeNonquery("DELETE FROM groupcontacts WHERE groupId=".$groupId." AND contactId=".$contactId."");
		
	}
	//add quick contact
	function addQuickContactDetail($user){
		
		$database = new database();
		
		$addedDate = date("Y-m-d");
		$dateOfBirth='0000-00-00 00:00:00';
		//custom data for contact 
		
		$database->executeNonQuery("INSERT INTO contact
								  SET userId='".$user['userid']."',
									firstName='".$user['quickfirstname']."',
									lastName='".$user['quicklastname']."',
									companyTitle='".$user['quickcompanyname']."',
									dateOfBirth='".$dateOfBirth."',
									jobTitle='".$user['quickjobtitle']."',
									addedDate='".$addedDate."',
									isActive=1");	
		$contactId =$database->insertid();
		
		
		$database->executeNonQuery("INSERT INTO contactphonenumbers 
									SET contactId='".$contactId."',
										phoneNumber='".$user['quickphone']."',
										addedDate='".$addedDate."'");
		$emailaddedDate		=date("Y-n-j");
		$database->executeNonQuery("INSERT INTO emailcontacts 
									SET contactId='".$contactId."',
										email='".$user['quickemail']."',
										addedDate='".$addedDate."'");
		return 1;								
		
		}
		function archiveContact($contactId){
			$database = new database();
			$database->executeNonQuery("UPDATE contact SET isActive=0 WHERE contactId='".$contactId."'");
			return ($database->getAffectedRows()!=''?1:0);
			}
	function addNewTask(){
		//query section for new task
			
			
		}
		function GetGroupContacts($userId,$groupId)
		{
			$database = new database();
			if(!empty($groupId))
			{
		$contacts=$database->executeObjectList("SELECT c.contactId, firstName, phoneNumber FROM contact c 
		JOIN groupcontacts gc on c.contactId=gc.contactId 
		LEFT JOIN contactphonenumbers cp on c.contactId=cp.contactId where userId='".$userId."' AND gc.groupId IN(".$groupId.") AND c.isActive=1 GROUP BY c.contactId");
			}
			else
			{
				$contacts=$database->executeObjectList("SELECT c.contactId, firstName, phoneNumber FROM contact c 
		LEFT JOIN contactphonenumbers cp on c.contactId=cp.contactId where userId='".$userId."' AND c.isActive=1");
			}
		return $contacts;
		}
		function checkContactGroups($groupId, $contactId){
		$database=new database();
		$contact=$database->executeScalar("Select contactId FROM groupcontacts WHERE groupId='".$groupId."' and contactId='".$contactId."'");
		return $contact;
		}
		function getContactForDefault($contactId){
			$database = new database();
			$result =$database->executeObject("SELECT contactId as id, firstName as name from contact WHERE contactId='".$contactId."'");
			
			$rar='{"id":"'.$result->id.'","name":"'.$result->name.'"},';	
			
	return $rar;
		
		}	
}
?>