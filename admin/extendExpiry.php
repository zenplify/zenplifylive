<?php
	require_once("../classes/init.php");
	require_once("classes/payments.php");
?>
<div style="vertical-align: middle; text-align: center;"><img src="images/loader.gif" width="20" height="20"></div>
<?php
	$database = new database();
	$paymentUser = new payments();
	$userId = $_SESSION['userId'];
	$extendedDays = $_REQUEST['editDays'];

	$userPaymentPlan = $paymentUser->getCustomplan($database);
	$planTitle = $userPaymentPlan->title;
	$planFee = $userPaymentPlan->fee;
	$planId = 4;
	$billingId = 'Custom Extended';
	$transactionId = 'Custom Extended';
	$transactionStatus = 0;
	$TransactionDetail = 'Custom Extended By Admin Panel';
	$startDate = date("Y-m-d");

	//$paymentDate = $paymentUser->getExpiryDate($database, $userId);
	$extendDate = $paymentUser->extendExpiry($database, $startDate, $userId, $planTitle, $planFee, $planId, $billingId, $transactionId, $transactionStatus, $TransactionDetail, $extendedDays);
?>
<script language="javascript">
	location.replace("userDetail.php?userId=<?php echo $userId; ?>")
</script>