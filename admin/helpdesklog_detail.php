<?php
	require_once("../classes/init.php");
	require_once("includes/header.php");
    require_once("classes/helpdesklog.php");
    $database = new database();
    $logData = new HelpDeskLog();
     $logId = $_GET['help_desk_LogId'];

?>

	<section id="main-content">
		<section class="wrapper">
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td>
			            <table cellpadding="0" cellspacing="0" border="0">

			                <tbody>
			                <tr>
			                    <td width=""></td>
			                    <td><strong><span style=" color:#555;">User's Log Details</span></strong></td>
			                </tr>
			                </tbody>
			            </table>
			        </td>
			    </tr>
			    </tbody>
			</table>
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
			    </tr>
			    </tbody>
			</table>
			<div class="row">
			    <ul class="user-detail">
			        <?php
				        $ld = $logData->singleLogDetail($database, $logId);

			        ?>
				    <table id="tblUserDetail">
			            <tr>
			                <td>
			                    <li class="user-detail-li">User Name</li>
			                </td>
			                <td>
			                    <li><?php echo $ld->user_name; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">User Email</li>
			                </td>
			                <td>
			                    <li><?php echo $ld->user_email; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Ticket ID</li>
			                </td>
			                <td>
			                    <li><?php echo $ld->ticketId; ?></li>
			                </td>
			            </tr>
                        <tr>
                            <td>
                                <li class="user-detail-li">Solved by</li>
                            </td>
                            <td>
                                <li><?php echo $ld->solvedBy; ?></li>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <li class="user-detail-li">Date Time</li>
                            </td>
                            <td>
                                <li><?php echo date("d-M-Y", strtotime($ld->dateTime));  ?></li>
                            </td>
                        </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">User's Request</li>
			                </td>
			                <td>
			                    <li><textarea cols="30" readonly rows="12" class="textArea"><?php echo $ld->ticketText; ?></textarea> </li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Support Team Comment</li>
			                </td>
			                <td>
			                    <li><textarea cols="30" readonly rows="12" class="textArea"><?php echo $ld->technicalcomment; ?></textarea></li>
			                </td>
			          </tr>

			        </table>
			    </ul>
			</div>
		</section>
	</section>
<?php require_once("includes/footer.php") ?>
<style>
    .textArea{
        margin: 0px;
        width: 400px;
        height: 130px;
        background: #F9F9F9;
        border: 1px solid #F9F9F9;
        padding: 16px;
    }
.user-detail-li{
        width: 230px !important;
        font-size: 14px;
        font-weight: bold;
    }
</style>