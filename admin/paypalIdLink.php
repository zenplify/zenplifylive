<?php
	require_once("../classes/init.php");
	require_once("classes/payments.php");
	require_once('classes/users.php');

	$database = new database();
	$payment = new payments();
	$userData = new users();

	$paypalId = str_replace('-', '%2d', $_GET['paypalId']);

	$planDetails = $payment->getPlanDetails($database, $_GET['planId']);
	$param = $paypalIdLink = $userData->paypalIdLink($database, $paypalId, 1, $_GET['userId'], $_GET['transactionId'], $_GET['planId'], $_GET['startDate'], $planDetails->duration, $planDetails->fee, $planDetails->title);
	echo $param;
?>
