<?php
	session_start();
	require_once("../classes/register.php");
	require_once("../classes/payment.php");
	$register = new register();
	$payment = new payment();
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	require_once("includes/header.php");

	if($username == "")
	{
		sendredirect("index.php");
	}

	$paymentUser = $payment->getPaidUser();
	$paymentLastSevenDays = $payment->getLastSevenDaysPaidUser();
	$paymentLastFifteenDays = $payment->getLastFifteenDaysPaidUser();
	$paymentLastThirtyOneDays = $payment->getLastThirtyOneDaysPaidUser();
?>
<!--<div align="center" id="index_logo"><img src="../images/logo.png" />-->
	<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">

							<tr>
								<td width=""></td>
								<td><strong><font color="#555">Admin Panel </font></strong></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

	<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
		</tr>
	</table>

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td height="30px" colspan="2"></td>
				</tr>
                <tr>
           <td style="font-weight:bold; color:#555;">Administrative Reports <?php
		           echo date('M').'  '.date('d').',     '.date(Y);
	           ?></td></tr>
      <tr>
					<td height="20px" colspan="2"></td>
				</tr>
				<tr>
					<td align="left" class="paidtext">Total Active Paid Users:</td>

					<td align="left" colspan="2"><font color="#555"><?php echo $paymentUser; ?></font></td>
				</tr>
				<tr>
					<td height="20px"></td>
				</tr>
				<tr>
					<td align="left" class="paidtext">Paid Users for Last 7 Days:</td>

					<td align="left" colspan="2"><font color="#555"><?php echo $paymentLastSevenDays; ?></font></td>
				</tr>
				<tr>
					<td height="20px"></td>
				</tr>
				<tr>
					<td align="left" class="paidtext">Paid Users for Last 15 Days:</td>

					<td align="left" colspan="2"><font color="#555"><?php echo $paymentLastFifteenDays; ?></font></td>
				</tr>
				<tr>
					<td height="20px"></td>
				</tr>
				<tr>
					<td align="left" class="paidtext">Paid Users for Last 31 Days:</td>

					<td align="left" colspan="2"><font color="#555"><?php echo $paymentLastThirtyOneDays; ?></font></td>
				</tr>
				<tr>
					<td height="50px"></td>
				</tr>
			</table>
<?php require_once("includes/footer.php") ?>
</div>
</html>