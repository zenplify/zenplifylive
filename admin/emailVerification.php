<?php
    require_once("../classes/init.php");
    require_once("../classes/register.php");
    require_once('classes/users.php');
    $database = new database();
    $register = new register();
    $userData = new users();

    $userId = $_GET['userId'];
    $users = $userData->verifyEmail($database, 1, $userId);
    $code = $userData->getUserCode($database, $userId);
    $register->createUserProfiles($code);
    $register->createUserPlans($code);

    echo 'done';
?>
