<?php
	require_once("../classes/init.php");
	require_once("includes/header.php");
	require_once("classes/users.php");
	require_once("classes/payments.php");
	$database = new database();
	$userData = new users();
	$payment = new payments();
	$userId = $_GET['userId'];
	$_SESSION['userId'] = $userId;

?>

	<section id="main-content">
		<section class="wrapper">
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td>
			            <table cellpadding="0" cellspacing="0" border="0">

			                <tbody>
			                <tr>
			                    <td width=""></td>
			                    <td><strong><font color="#555">User Details</font></strong></td>
			                </tr>
			                </tbody>
			            </table>
			        </td>
			    </tr>
			    </tbody>
			</table>
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
			    </tr>
			    </tbody>
			</table>
			<div class="row">
			    <ul class="user-detail">
			        <?php
				        $users = $userData->singleUserDetail($database, $userId);
				        foreach ($users as $u) {
				        $leaderName = $userData->getLeaderName($database,$u->leaderId);
			        ?>
				    <table id="tblUserDetail">
			            <tr>
			                <td>
			                    <li class="user-detail-li">Url Name</li>
			                </td>

			                <td>
			                    <li><?php echo $u->userName; ?></li>
			                </td>
			            </tr>
					    <tr>
						    <td>Leader Name</td>
						    <td><?php echo $leaderName->firstName.'    '.$leaderName->lastName; ?></td>
					    </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">First Name</li>
			                </td>
			                <td>
			                    <li><?php echo $u->firstName; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Last Name</li>
			                </td>
			                <td>
			                    <li><?php echo $u->lastName; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Email </li>
			                </td>
			                <td>
			                    <li><?php echo $u->email; ?></li>
			                </td>
			            </tr>
                        <tr>
                            <td>
                                <li class="user-detail-li">Ontraport username </li>
                            </td>
                            <td>
                                <li><?php echo $u->ontraportUserName; ?></li>
                            </td>
                        </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Password</li>
			                </td>
			                <td>
			                    <li><?php echo $u->password; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Account Verification</li>
			                </td>
			                <td>
			                    <li><?php
					                    if($u->isVerifiedEmail == 0)
					                    {
						                    ?>
						                    <span style="cursor: pointer; color:red" onclick="activateUser('<?php echo $u->userId; ?>')" id="user_<?php echo $u->userId; ?>">Click Here to Verify</span>
					                    <?php
					                    }
					                    else
					                    {
						                    echo 'Activated';
					                    } ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">PayPal Account Status</li>
			                </td>
			                <td><?php if(!empty($u->paypalProfileId) AND $u->isPaypalProfileActive == 1)
				                {
					                $user_id = $u->userId;
					                $payPallId = $u->paypalProfileId;

					                ?><span style="cursor: pointer; color:red" onclick="disablePaypal('<?php echo $user_id; ?>','<?php echo $payPallId; ?>')" id="paypalStatus_<?php echo $u->userId; ?>">Click Here to Cancel</span><?php
				                }
				                else
				                {
					                echo 'No Active Payments';
				                } ?></td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Title</li>
			                </td>
			                <td><?php echo $u->title; ?></td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Web Address</li>
			                </td>
			                <td><?php echo $u->webAddress; ?></td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Facebook Address</li>
			                </td>
			                <td><?php echo $u->facebookAddress; ?></td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Profile Generation</li>
			                </td>
			                <td>
										<span id="profileGeneration_<?php echo $u->userId; ?>" style="color: blue; cursor: pointer;" onclick="profileGeneration('<?php echo $u->userId; ?>')">
											Click Here to Generate new User Profiles
										</span>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">PayPal ID</li>
			                </td>
			                <td>
										<span id="forPaypalId">
											<?php
												$user_id = $u->userId;
												echo $payPallId = $u->paypalProfileId;
											?>
										</span>
			                            <span style="cursor: pointer; color:blue" onclick="show('barfield',true)" id="paypalidLink_<?php echo $u->userId; ?>">
				                            Link New PayPal ID
			                            </span>
			                </td>
			            </tr>
                        <tr>
                            <td>
                                <li class="exportUser">Export User</li>
                            </td>
                            <td> <img src="../images/export.png" alt="Export" onclick="exportUserforOntraport('<?php echo $user_id; ?>')" style="cursor: pointer" title="Export" id="exportUsers"/></td>
                        </tr>
			            <tr>
                        <tr>
                            <td>
                                <li class="exportUser">Recharge Form Link</li>
                            </td>
                            <td> <img src="../images/link.png" alt="Export" onclick="sendToRecharge('<?php echo $u->emailVerificationCode ?>')" style="cursor: pointer" title="Get Recharge Link" id="rechargeLink"/></td>
                        </tr>
                        <tr>
			                <td colspan="2">
										<span id="barfield" style="visibility:hidden;">
											<input id="paypalId" placeholder="PayPal ID" value="" name="paypalId" type="text">
											<input id="transcationId" placeholder="Transcation ID" value="" name="transcationId" type="text">
											<select id="planId" name="planId">
												<option value="0">Select Payment Plan</option>
												<?php
													$paymentPlans = $payment->getUseraccountpaymentplans($database);
													foreach($paymentPlans as $pp)
													{
														?>
														<option value="<?php echo $pp->planId; ?>"><?php echo ucfirst($pp->title); ?></option><?php
													}
												?>
											</select>
								            <input type="text" name="startDate" id="startDate" value="" placeholder="Start Date"/>
											<input type="button" onclick="paypalLinkId('<?php echo $u->userId; ?>')" id="button" value="Submit">
											<div id="errorPayPalLink" style="display:none; color:red">Information Missing</div>
										</span>
			                </td>
			                <td></td>
			            </tr>
					    <?php } ?>
					    <tr>
			                <table width="100%" id="tblPayment">
			                    <tr b style="border:1px solid red;background-color: #fff;color:#555; font-weight:bold; font-size:16px" width="100%">
			                        <td width="150">Plan Title</td>
			                        <td width="150">Start Date</td>
			                        <td width="150">Expiry Date</td>
			                        <td width="150">Transaction Date</td>
				                    <td width="150">Action</td>
			                    </tr>
			                </table>
			            </tr>
					    <tr>
						    <td>
                        <table width="100%" id="tblPaymentDetails">
					    <?php $usersPaymentDetail = $userData->userPaymentDetail($database, $userId);
						    foreach($usersPaymentDetail as $pay)
						    {
							    ?>
							    <tr id="payment<?php echo $pay->paymentId; ?>">
	                                <td width="150"><?php echo ucfirst($pay->choosePlanTitle); ?></td>
	                                <td width="150"><?php echo $pay->startDate; ?></td>
	                                <td width="150"><?php echo $pay->expiryDate; ?></td>
	                                <td width="150"><?php echo $pay->transactionDateTime; ?></td>
								    <td width="150"><span style="cursor: pointer;" onclick="removeSubscription('<?php echo $pay->paymentId; ?>')">Remove this Subscription</span></td>
	                            </tr>
						    <?php
						    }
					    ?>
	                        </table>
						    </td>
	                    </tr>
					    <tr>
			                <input style="float: right;" type="button" name="foo" value="Extend Trail" onclick="show('bar',true)">
			            </tr>
			            <tr>
			                <td colspan="2">
			                    <div id="bar" style="visibility:hidden;">
			                        <form method="post" name="form" action="extendExpiry.php">
			                            <input placeholder="Days" value="" name="editDays" id="editDays" type="text">
			                            <input type="submit" value="submit">
			                        </form>
			                    </div>
			                </td>
			            </tr>
			        </table>
			    </ul>
			</div>
		</section>
	</section>
<?php require_once("includes/footer.php") ?>