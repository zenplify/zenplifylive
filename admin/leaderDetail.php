<?php
	require_once("../classes/init.php");
	require_once("includes/header.php");
	require_once("classes/users.php");
	require_once("classes/payments.php");
	$database = new database();
	$userData = new users();
	$payment = new payments();
	$userId = $_GET['userId'];
	$_SESSION['userId'] = $userId;

?>

	<section id="main-content">
		<section class="wrapper">
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td>
			            <table cellpadding="0" cellspacing="0" border="0" width="100%">

			                <tbody>
			                <tr>
			                    <td width="50%"><strong>Leader Details</strong></td>
			                    <td style="text-align: right;"><img src="../images/export.png" alt="Export User" onclick="exportUserforOntraport('<?php echo $userId; ?>')" style="cursor: pointer; padding-right: 12px;" title="Export" id="exportUsers"/></td>
			                </tr>
			                </tbody>
			            </table>
			        </td>
			    </tr>
			    </tbody>
			</table>
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
			    </tr>
			    </tbody>
			</table>
			<div class="row">
			    <ul class="leader-detail">
			        <?php
				        $users = singleUserDetail($database, $userId);

			        ?>
				    <table id="tblUserDetail">
			            <tr>
			                <td>
			                    <li class="leader-detail-li">User Name</li>
			                </td>
			                <td>
			                    <li><?php echo $users->userName; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="leader-detail-li">First Name</li>
			                </td>
			                <td>
			                    <li><?php echo $users->firstName; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="leader-detail-li">Last Name</li>
			                </td>
			                <td>
			                    <li><?php echo $users->lastName; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="leader-detail-li">Email ID</li>
			                </td>
			                <td>
			                    <li><?php echo $users->email; ?></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="leader-detail-li">Password</li>
			                </td>
			                <td>
			                    <li><?php echo $users->password; ?></li>
			                </td>
			            </tr>
                        <tr>
                            <td>
                                <li class="leader-detail-li">Leader Template Url</li>
                            </td>
                            <td>
                                <li> <a style="color: blue;"
                                        href="http://techgeese.com/leader/signup/<?php echo $users->signupName; ?>"
                                        target="_blank">http://techgeese.com/leader/signup/<?php echo $users->signupName; ?></a></li>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <li class="leader-detail-li">Sales page Url</li>
                            </td>
                            <td>
                                <li> <a style="color: blue;"
                                        href="http://techgeese.com/sales/<?php echo $users->signupName; ?>"
                                        target="_blank">http://techgeese.com/sales/<?php echo $users->signupName; ?></a></li>
                            </td>
                        </tr>
                        <tr>
                            <table>
                                <tr  style="border:1px solid red;background-color: #fff;color:#555; font-weight:bold; font-size:16px" width="100%">
                            <td colspan="2"><li style="width: 100%" class="leader-detail-li">Universal Sales Page Settings<span style="font-size: 14px;font-weight:normal;"> -<a style="float: right; cursor: pointer; color: #1e90ff;" id="updateInfo" onClick="updateSalesPageInfo()">Edit</a>
                                <a style="float: right; display: none; cursor: pointer; color: #1e90ff;" id="saveInfo" onClick="saveSalesPageInfo('<?php echo $users->userId;  ?>','<?php echo $users->signupName;  ?>')">Save</a> </span></li></td>
                            <td><span id="successMsg" colspan="2" style="float: left; padding-left: 5px;  color:#008000; font-size: 14px; "></span></td>
                                </tr> </table>
                        </tr>
                        <?php  $salesPageDetail=leadersSalesPageDetail($userId); ?>
                        <tr>
                            <table id="tblUserDetail">
                                <tr>
                            <td><li class="leader-detail-li">Template Name</li></td>
                            <td><input onkeyup="changeBorder('1');" style="border: 0px; padding: 2px; width: 145px;height: 20px;" readonly type="text" value="<?php echo $salesPageDetail->displayName; ?>" id="displayName" /></td>

                        </tr>
                        <tr>
                            <td><li class="leader-detail-li">Group Name</li></td>
                            <td><input onkeyup="changeBorder('2');" style="border: 0px; padding: 2px; width: 145px;height: 20px;" readonly type="text" value="<?php echo $salesPageDetail->defaultText; ?>" id="defaultText"/> </td>
                        </tr>
                        <tr>
                            <td><li class="leader-detail-li">Display As</li></td>
                            <td id="finalValue" ><?php echo $salesPageDetail->displayName.' - '. $salesPageDetail->defaultText; ?></td>
                           </tr> </table>
                        </tr>
<!--                        <tr style="height: 32px;" >-->
<!--                            <li class="leader-detail-li">-->
<!--                            <td id="successMsg" colspan="2" style="float: left; padding-left: 5px;  color:#008000; font-size: 14px; "></td>-->
<!--                       </li> </tr>-->

					   </table>



			    </ul>
			</div>
		</section>
	</section>

<?php
function leadersSalesPageDetail($userId){
    $database = new database();
    return $leaderId = $database->executeObject("SELECT * FROM leadertemplate WHERE userId='" . $userId . "'");

}
function singleUserDetail($database, $userId)
{
    return $database->executeObject("SELECT * FROM user WHERE userId='".$userId."'");
}
?>
    <script type="text/javascript">

        function updateSalesPageInfo() {
            $('#updateInfo').hide();
            $('#saveInfo').show();

            $('#displayName').focus();
            $('#displayName').removeAttr('readonly');
            $('#defaultText').removeAttr('readonly');
            $('#displayName').css('border', '1px solid #ccc');
            $('#defaultText').css('border', '1px solid #ccc');

        }
       function saveSalesPageInfo(userId,signupName){
           var displayName= $("#displayName").val();
           var defaultText= $("#defaultText").val();
           if(displayName=='' && defaultText=='' ){
               $('#displayName').css('border', '1px solid #CD0A0A');
               $('#defaultText').css('border', '1px solid #CD0A0A');

           }else if(displayName=='' ){  $('#displayName').css('border', '1px solid #CD0A0A');}
           else if(defaultText==''){$('#defaultText').css('border', '1px solid #CD0A0A');}
          else{

               $('#displayName').css('border', '0px');
               $('#defaultText').css('border', '0px');
           $('#displayName').css('background', '#fff');
           $('#defaultText').css('background', '#fff');
           $('#displayName').attr('readonly', true);
           $('#defaultText').attr('readonly', true);
           //$('#finalValue').html(displayName+' - '+defaultText);


           var  aTagText= $("#updateInfo").text()

           $('#updateInfo').show();
           $('#saveInfo').hide();
           if(aTagText=='Add'){
               $("#updateInfo").text('Update');
           }

               // ajax call
               $.ajax({
                   type: "post",
                   url: "ajax.php",
                   data: {action: 'updateLeaderSalesPageInfor', userId: userId,signupName: signupName,displayName:displayName,defaultText:defaultText},
                   success: function (data) {
                       $('#successMsg').html(data).show('slow').delay(5000).fadeOut('slow');

               }
           });
           // ajax call end
           }
       }
        function changeBorder(checkField){

            var displayName= $("#displayName").val();
            var defaultText= $("#defaultText").val();
            $('#displayName').css('border', '1px solid #1e90ff');
            $('#defaultText').css('border', '1px solid #1e90ff');
            $('#finalValue').html(displayName+' - '+defaultText);

        }
    </script>
<?php require_once("includes/footer.php") ?>