<?php

	class users
	{

		// function to get count of users
		function countForpagination($database)
		{
			return $database->executeScalar("SELECT COUNT(*) as num FROM user");
		}

		// function to get count of users
		function countForpaginationSearch($database, $search)
		{
			return $database->executeScalar("SELECT COUNT(*) as num FROM user  WHERE  (userName LIKE '%".$search."%') OR (email LIKE '%".$search."%')   OR (phoneNumber LIKE '%".$search."%')OR (firstName LIKE '%".$search."%') OR (lastName LIKE '%".$search."%') OR (consultantId LIKE '%".$search."%') OR (title LIKE '%".$search."%') OR (webAddress LIKE '%".$search."%') OR (facebookAddress LIKE '%".$search."%') OR (emailVerificationCode LIKE '%".$search."%') OR (facebookPersonalAddress LIKE '%".$search."%') OR (facebookPartyAddress LIKE '%".$search."%') OR (paypalProfileId LIKE '%".$search."%')OR (oldUsername LIKE '%".$search."%')");
		}

		// function to get multipule user detail
		function getAllusers($database, $start, $limit)
		{
			return $database->executeObjectList("SELECT * FROM user order by userId desc limit $start ,$limit");
		}
		function getLeaderName($database,$leaderId)
		{
			return $database->executeObject("SELECT firstName,lastName FROM user where userId='".$leaderId."'");
		}


		// function to get single user detail
		function singleUserDetail($database, $userId)
		{
			return $database->executeObjectList("SELECT * FROM user WHERE userId='".$userId."'");
		}

		// function get userpayments detail
		function userPaymentDetail($database, $userId)
		{
			return $database->executeObjectList("SELECT * FROM userpayments WHERE userId='".$userId."' order by paymentId desc");
		}

		// function to get email verification
		function verifyEmail($database, $emailparameter, $userId)
		{
			return $database->executeNonQuery("UPDATE user SET isVerifiedEmail='".$emailparameter."' WHERE userId='".$userId."'");
		}

		// paypal id linked
		function paypalIdLink($database, $paypalId, $isPaypalProfileActive, $userId, $transactionId, $planId, $startDate, $duration, $fee, $title)
		{
			$startDate = date('Y-m-d', strtotime($startDate));
			$database->executeNonQuery("UPDATE user SET paypalProfileId='".$paypalId."',isPaypalProfileActive='".$isPaypalProfileActive."' WHERE userId='".$userId."'");

			$database->executeNonQuery("Insert into `userpayments` set `userId` = '".$userId."', `planId` = '".$planId."', `fee` = '".$fee."',`startDate` = '".$startDate."', `expiryDate` = DATE_ADD('".$startDate."',INTERVAL ".$duration." DAY), `choosePlanTitle` = '".$title."', `duration` = '".$duration."', `transactionId` = '".$transactionId."',`transactionDateTime` = NOW(), `transactionDetail` = 'Added from Admin Panel against profileId ".$paypalId."'");
			$expiry = date('Y-m-d',strtotime($startDate. "+".$duration." days"));
			return $startDate.'--'.$expiry.'--'.date("Y-m-d H:i:s",time());

		}

		// function to Search user
		function searchUser($database, $search, $start, $limit)
		{
			return $database->executeObjectList("SELECT * FROM user WHERE  (userName LIKE '%".$search."%') OR (email LIKE '%".$search."%')   OR (phoneNumber LIKE '%".$search."%')OR (firstName LIKE '%".$search."%') OR (lastName LIKE '%".$search."%') OR (consultantId LIKE '%".$search."%') OR (title LIKE '%".$search."%') OR (webAddress LIKE '%".$search."%') OR (facebookAddress LIKE '%".$search."%') OR (emailVerificationCode LIKE '%".$search."%') OR (facebookPersonalAddress LIKE '%".$search."%') OR (facebookPartyAddress LIKE '%".$search."%') OR (paypalProfileId LIKE '%".$search."%')OR (oldUsername LIKE '%".$search."%')order by userId desc limit $start ,$limit");
		}

		function getUserCode($database, $userId)
		{
			return $database->executeScalar("Select emailVerificationCode from `user` where userId = '".$userId."'");
		}

		// reporting module
		function thisDayUsers($database, $registrationDate)
		{
			return $getthisDayUsers = $database->executeScalar("SELECT count(registrationDate) FROM user WHERE registrationDate  >=('".$registrationDate."'-INTERVAL 3 HOUR)");
		}

		function thisWeekUsers($database, $registrationDate)
		{
			return $database->executeScalar("SELECT count(registrationDate) FROM user WHERE registrationDate='".$registrationDate."'");
		}

		function  getThisMonthSignups($database, $registrationDate)
		{
			// echo ("SELECT count(registrationDate) FROM user WHERE registrationDate  >=(''".$registrationDate."'-INTERVAL 7 DAY)");
			return $database->executeScalar("SELECT count(registrationDate) FROM user WHERE registrationDate  = ('".$registrationDate."')");
		}

		function  getSixMonthsignups($database, $registrationDate)
		{
			return $database->executeScalar("SELECT count(registrationDate) FROM user WHERE registrationDate  > ('".$registrationDate."'-INTERVAL 30 DAY) AND registrationDate <=('".$registrationDate."')") ;
		}

		function  getYearlysignups($database, $registrationDate)
		{
			return $database->executeScalar("SELECT count(registrationDate) FROM user WHERE registrationDate  >('".$registrationDate."'-INTERVAL 365 DAY) and registrationDate  <('".$registrationDate."')");
		}

		function getLastFiveUsers($database)
		{
			$lastusersDataset = $database->executeObjectList("SELECT * FROM `user`  JOIN `userpayments` ON userpayments.userId=user.userId GROUP BY user.`userId` ORDER BY registrationDate DESC LIMIT 5");
			return $lastusersDataset;
		}

		function getLastFiveMonthHistory($database)
		{
			$lastusersDataset = $database->executeObjectList("SELECT COUNT(userId) as users,SUM(fee) as fee,MONTH(startDate) AS m FROM userpayments WHERE `startDate` < NOW() AND `startDate` > DATE_ADD(NOW(), INTERVAL- 6 MONTH) GROUP BY m ORDER BY 	m DESC;");
			return $lastusersDataset;
		}
	}

?>