<?php

	class payments {

		// Function for select last date of user
		function getExpiryDate($database, $userId) {
			return $database->executeObjectList("SELECT * FROM userpayments WHERE userId='" . $userId . "'order by paymentId desc limit 1");
		}

		// function to get custom payment plan
		function getCustomplan($database) {
			return $database->executeObject("SELECT * FROM useraccountpaymentplans WHERE planId='4'");
		}

		// function to extend expiry date of user from userpayments table
		function extendExpiry($database, $startDate, $userId, $planTitle, $planFee, $planId, $billingId, $transactionId, $transactionStatus, $TransactionDetail, $extendedDays) {
			$database->executeNonQuery("INSERT INTO userpayments SET startDate='" . $startDate . "',
			    userId='" . $userId . "',
			    choosePlanTitle='" . $planTitle . "',
			    fee ='" . $planFee . "',
			    planId='" . $planId . "',
			    billingId='" . $billingId . "',
			    transactionId='" . $transactionId . "',
			    transactionStatus='" . $transactionStatus . "',
			    transactionDateTime=Now(),
			    transactionDetail='" . $TransactionDetail . "',
			    expiryDate=DATE(DATE_ADD('" . $startDate . "',INTERVAL'" . $extendedDays . "'DAY))");
		}

		// function get paid users
		function getPaidUser() {
			$database = new database();
			$resultPaidUser = $database->executeScalar("SELECT count(distinct userId) FROM userpayments WHERE planId<>1 AND userId<>0 AND CURDATE()<=expiryDate AND DATEDIFF(`expiryDate`,`startDate`)<='92' ");

			//$totalRows=mysql_num_rows($resultPaid);
			return $resultPaidUser;
		}

		// function get all users
		function  getAllActiveUsers() {
			$database = new database();
			$resultActiveUsers = $database->executeScalar("SELECT count(distinct userId) FROM userpayments WHERE planId<>1 AND userId<>0  ");

			return $resultActiveUsers;
		}

		// function get total sales
		function  getTotalSales($database) {
			$getTotalSales = $database->executeScalar("SELECT sum(fee)FROM userpayments ");

			return $getTotalSales;
		}

		// list of total upaid users and their due amount
		function unpaidUsers($database) {
			$unpaidUsers = $database->executeObjectList("SELECT * FROM userpayments WHERE  planId<4 AND userId<>0 AND fee=0 AND CURDATE()<=expiryDate");

			return $unpaidUsers;
		}

		// get user name for unpaid user
		function  userNameForUnpaid($database, $userId) {
			$userNameForUnpaid = $database->executeScalar("SELECT userName FROM user WHERE userId='" . $userId . "'");

			return $userNameForUnpaid;
		}

		// get remaining amount
		function  getPlanPrice($database, $planId) {
			$getPlanPrice = $database->executeScalar("SELECT fee  FROM useraccountpaymentplans WHERE planId='" . $planId . "'");

			return $getPlanPrice;
		}

		/////////////last five months history ////////////////////////
		function getFiveMonthsPaidUser($database) {
			$getFiveMonthsPaidUser = $database->executeScalar("SELECT count(distinct userId) FROM userpayments WHERE startDate  >=(now()-INTERVAL 150 DAY) AND planId<>1 AND userId<>0 AND CURDATE()<=expiryDate ");

			//$totalRows=mysql_num_rows($resultPaid);
			return $getFiveMonthsPaidUser;
		}

		function  getLastFiveMonthSales($database) {
			$getLastFiveMonthSales = $database->executeScalar("SELECT sum(fee)FROM userpayments WHERE startDate  >=(now()-INTERVAL 150 DAY) ");

			return $getLastFiveMonthSales;
		}

		function  getFiveMonthActiveUSers($database) {
			$getFiveMonthActiveUSers = $database->executeScalar("SELECT count(distinct userId) FROM userpayments WHERE startDate  >=(now()-INTERVAL 150 DAY) AND planId<>1 AND userId<>0  ");

			return $getFiveMonthActiveUSers;
		}

		function  getOneMonthSales($database) {
			$getOneMonthSales = $database->executeScalar("SELECT sum(fee)FROM userpayments WHERE startDate  >=(now()-INTERVAL 30 DAY) ");

			return $getOneMonthSales;
		}

		function getUseraccountpaymentplans($database) {
			return $database->executeObjectList("select * from `useraccountpaymentplans` where accountTypeId = 1");
		}

		function getPlanDetails($database, $planId) {
			return $database->executeObject("select * from `useraccountpaymentplans` where `planId` = '" . $planId . "'");
		}

		function removeSubscription($database, $paymentId) {
			$database->executeNonQuery("delete from `userpayments` where `paymentId` = '" . $paymentId . "'");
		}
	}


?>