<?php
/**
 * Created by PhpStorm.
 * User: Namoos
 * Date: 12/30/2015
 * Time: 3:22 PM
 */

require_once("../classes/init.php");
$database = new database();
$total = $database->executeScalar("SELECT count(*) FROM user WHERE `leaderId` = '0'");

$url = 'http://techgeese.com/admin/leaderSignup.php';


$perPage = 20;
$page = 1;
if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}

$totalPages = ceil($total / $perPage);

$offset = ($perPage * $page) - $perPage;
$limit = $perPage;

$sql = "SELECT CONCAT(firstName, ' ', lastName) AS `name`,`userId`, `signupName`, `isAllowSignUp` FROM `user` WHERE `leaderId` = '0' LIMIT $offset, $limit";
$leaders = $database->executeObjectList($sql);


require_once("includes/header.php");

?>
<style>
    .pagination-link {
        border: 1px solid #202020;
        padding: 4px 10px;
        margin: 1px;
        border-radius: 5px;
        color: #808080;
    }

    .active, a.pagination-link:hover {
        background-color: #202020;
        color: #fff;
    }

    #leaderTable tbody tr:nth-child(odd) {
        background: #EEE;
        color: #555;
    }

    #leaderTable tbody tr:nth-child(even), #leaderTable tbody tr:last-child {
        background: #FFF;
        color: #555;
    }

    tr td, tr th {
        font-size: 15px;
        color: #555;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                        <tr>
                            <td width=""></td>
                            <td><strong><font color="#555">Leaders</font></strong></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
            <tr>
                <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
            </tr>
            </tbody>
        </table>
        <div class="row" style="margin-top: 30px;">
            <table cellpadding="0" cellspacing="1" width="100%" id="leaderTable">
                <thead>
                <tr>
                    <th align="left">Leader Name</th>
                    <th align="left" style="padding-left: 35px;">Template Url <span
                            style="font-size: 12px; font-style: normal;">(Use this link to create new leader using selected template)</span>
                    </th>
                    <th align="center">Request Status</th>
                    <th align="center">Detail</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($leaders as $leader): ?>
                    <tr style="height: 25px;">
                        <td align="left"><?php echo $leader->name; ?></td>
                        <td align="left" style="padding-left: 35px;">
                            <?php if ($leader->isAllowSignUp > 0): ?>
                                <a style="color: blue;"
                                   href="http://techgeese.com/leader/signup/<?php echo $leader->signupName; ?>"
                                   target="_blank">http://techgeese.com/leader/signup/<?php echo $leader->signupName; ?></a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if ($leader->isAllowSignUp == 2): ?>
                                <span>Active</span>
                            <?php elseif ($leader->isAllowSignUp == 1): ?>
                                <span>Pending</span>
                            <?php
                            else: ?>
                                <span>N/A</span>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <a href="leaderDetail.php?userId=<?php echo $leader->userId; ?>"><img alt="User details"
                                                                                                  title="User details"
                                                                                                  src="images/details_open.png"/></a>
                        </td>

                    </tr>
                <?php endforeach; ?>
                <?php if ($totalPages > 0) {
                    echo '<tr style="padding-top: 18px; display: block;"><td colspan="3">';
                    for ($i = 1; $i <= $totalPages; $i++) {
                        $active = '';
                        if ($i == $page) {
                            $active = 'active';
                        }
                        echo '<a class="pagination-link ' . $active . '" href="' . $url . '?page=' . $i . '">' . $i . '</a>';
                    }
                    echo '</td></tr>';
                } ?>
                </tbody>
            </table>

        </div>
    </section>
</section>
<?php
function chekcleaderForSalesPage($userId)
{
    $database = new database();
    $leaderId = $database->executeScalar("SELECT userId FROM leadertemplate WHERE userId='" . $userId . "'");
    if (empty($leaderId) || $leaderId == "") {
        return 0;
    } else {
        return 1;
    }
}

?>
<?php require_once("includes/footer.php") ?>
