<?php
    require_once("../classes/init.php");
    require_once("classes/users.php");
    $database = new database();
    $userData = new users();
    $search = $_POST['search'];

    require_once("includes/header.php")
?>
<section id="main-content">
    <section class="wrapper">
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">

            <tbody>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">

                        <tbody>
                        <tr>
                            <td width=""></td>
                            <td><strong><font color="#555">User List </font></strong></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
            <tr>
                <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
            </tr>
            </tbody>
        </table>
        <div class="row">

            <table cellpadding="0" cellspacing="0" width="100%" id="tblUser">
                <tr>
                    <th>User Name</th>
                    <th>User Email</th>
                    <th>Email Verification</th>
                    <th>Detail</th>
                </tr>
                <?php

                    $userscont = $userData->countForpaginationSearch($database, $search);
                    $total = $userscont;
                    $adjacents = 3;
                    $targetpage = "search.php"; //your file name
                    $limit = 50; //how many items to show per page
                    $page = $_GET['page'];

                    if ($page) {
                        $start = ($page - 1) * $limit; //first item to display on this page
                    } else {
                        $start = 0;
                    }

                    /* Setup page vars for display. */
                    if ($page == 0)
                        $page = 1; //if no page var is given, default to 1.
                    $prev = $page - 1; //previous page is current page - 1
                    $next = $page + 1; //next page is current page + 1
                    $lastpage = ceil($total / $limit); //lastpage.
                    $lpm1 = $lastpage - 1; //last page minus 1

                    $users = $userData->searchUser($database, $search, $start, $limit);
                    $curnm = mysql_num_rows($users);
                    $pagination = "";
                    if ($lastpage > 1) {
                        $pagination .= "<div class='pagination1'> <ul>";
                        if ($page > $counter + 1) {
                            $pagination .= "<li><a href=\"$targetpage?page=$prev\">prev</a></li>";
                        }
                        if ($lastpage < 7 + ($adjacents * 2)) {
                            for ($counter = 1; $counter <= $lastpage; $counter++) {
                                if ($counter == $page)
                                    $pagination .= "<li><a href='#' class='active'>$counter</a></li>";
                                else
                                    $pagination .= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";
                            }
                        } elseif ($lastpage > 5 + ($adjacents * 2)) //enough pages to hide some
                        {
                            //close to beginning; only hide later pages
                            if ($page < 1 + ($adjacents * 2)) {
                                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                                    if ($counter == $page)
                                        $pagination .= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination .= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";
                                }
                                $pagination .= "<li>...</li>";
                                $pagination .= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
                                $pagination .= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>";
                            } //in middle; hide some front and some back
                            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                                $pagination .= "<li><a href=\"$targetpage?page=1\">1</a></li>";
                                $pagination .= "<li><a href=\"$targetpage?page=2\">2</a></li>";
                                $pagination .= "<li>...</li>";
                                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                                    if ($counter == $page)
                                        $pagination .= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination .= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";
                                }
                                $pagination .= "<li>...</li>";
                                $pagination .= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
                                $pagination .= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>";
                            } //close to end; only hide early pages
                            else {
                                $pagination .= "<li><a href=\"$targetpage?page=1\">1</a></li>";
                                $pagination .= "<li><a href=\"$targetpage?page=2\">2</a></li>";
                                $pagination .= "<li>...</li>";
                                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage;
                                     $counter++) {
                                    if ($counter == $page)
                                        $pagination .= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination .= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>";
                                }
                            }
                        }
                        //next button
                        if ($page < $counter - 1)
                            $pagination .= "<li><a href=\"$targetpage?page=$next\">next</a></li>";
                        else
                            $pagination .= "";
                        $pagination .= "</ul></div>\n";
                    }
                    foreach ($users as $u) {
                        ?>
                        <tr>
                            <td align="center"><?php echo $u->userName; ?></td>
                            <td align="center"><?php echo $u->email; ?></td>
                            <td align="center">
                                <?php
                                    if ($u->isVerifiedEmail == 0) {
                                        ?>
                                    <span style="cursor: pointer; color:red"
                                          onclick="activateUser('<?php echo $u->userId; ?>')"
                                          id="user_<?php echo $u->userId; ?>">Click Here to Verify</span><?php
                                    } else {
                                        echo 'Activated';
                                    }
                                ?>
                            </td>
                            <td align="center">
                                <a href="userDetail.php?userId=<?php echo $u->userId; ?>"><img alt="User details"
                                                                                               title="User details"
                                                                                               src="images/details_open.png"/></a>
                            </td>
                        </tr>
                    <?php
                    }
                ?>
            </table>
            <?php
                echo $pagination;
            ?>


        </div>
    </section>
</section>
<!--main content end--><!--right sidebar start-->
<div class="right-sidebar">
</div>
<!--right sidebar end-->
</section>
<!-- Placed js at the end of the document so the pages load faster --><!--Core js-->
<!--script for this page-->
<?php  require_once("includes/footer.php") ?>