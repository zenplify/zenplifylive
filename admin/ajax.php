<?php
require_once("../classes/init.php");
require_once("classes/payments.php");
require_once('classes/users.php');

$database = new database();
$userData = new users();
error_reporting(0);

$action = $_REQUEST["action"];

if ($action == "getJsonBetweenDates") {

    $date_from = "";
    if (isset($_REQUEST["date_from"]))
        $date_from = $_REQUEST["date_from"];

    $date_to = "";
    if (isset($_REQUEST["date_to"]))
        $date_to = $_REQUEST["date_to"];

    $explodes = explode('/', $date_from);
    $date_from_month = $explodes[0];

    $explodes = explode('/', $date_to);
    $date_to_month = $explodes[0];

    $date_from_new = strtotime('-1 day', strtotime($date_from)); //to include the selected date
    $diff = abs(strtotime($date_to) - $date_from_new);

    $days = $diff / (60 * 60 * 24);
    //$years = floor($diff / (365*60*60*24));
    //$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    //$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

//		echo $date_from_month.'-'.$date_to_month;

    $days_in_month_from = date("t", strtotime(date('Y', strtotime($date_from)) . "-" . date('m', strtotime($date_from)) . "-01"));
    $days_in_month_to = date("t", strtotime(date('Y', strtotime($date_to)) . "-" . date('m', strtotime($date_to)) . "-01"));

    $days_to_match = 30;
    if ($date_from_month === $date_to_month) {
        $days_to_match = $days_in_month_to;
    }


    if ($days == 1 || $days == 0) {
        $countValue = 8;
        $type = "hours";
        $timestamp = strtotime($date_to);
        $jsonObj = array();
        for ($i = 0; $i < $countValue; $i++) {
            $data = array();
            $registrationDate = date('Y-m-d', $timestamp);
            $timestamp -= 3 * 3600;
            $data["Day"] = $registrationDate;
            $data["signup"] = $userData->thisDayUsers($database, $registrationDate);
            $jsonObj[] = $data;
        }

    } elseif ($days > 1 && $days <= $days_to_match) {
        $countValue = $days;
        $type = "days";
        $timestamp = strtotime($date_to);;
        $jsonObj = array();
        for ($i = 0; $i < $countValue; $i++) {
            $data = array();
            $registrationDate = date('Y-m-d', $timestamp);
            $timestamp -= 24 * 3600;
            $data["Day"] = date('d', strtotime($registrationDate));
            $data["signup"] = $userData->thisWeekUsers($database, $registrationDate);
            $jsonObj[] = $data;
        }

    } elseif ($days > $days_to_match && $days <= 365) {
        $countValue = ceil($diff / (30 * 60 * 60 * 24));
//			echo 'count value '. $countValue.'<br>';

        $type = "month";
        $timestamp = strtotime($date_to);
        $jsonObj = array();
        $fromtime = strtotime($date_from);
        for ($i = 0; $i < $countValue; $i++) {
            $data = array();
            $registrationDate = date('Y-m-d', $timestamp);
            $daysInMonth = date("t", strtotime(date('Y', $timestamp) . "-" . date('m', $timestamp) . "-01"));

            $explodes = explode('-', $registrationDate);
            $registration_date_month = $explodes[1];

            if (($timestamp > $fromtime) || ($date_from_month == $registration_date_month)) {
                $data["Day"] = date('M-y', strtotime($registrationDate));
                $data["signup"] = $userData->getSixMonthsignups($database, $registrationDate);
                $jsonObj[] = $data;
            }
            $timestamp -= $daysInMonth * (24 * 3600);
        }
    } elseif ($days > 365) {
        $countValue = ceil($diff / (365 * 60 * 60 * 24));
        $type = "years";
        $timestamp = strtotime($date_to);
        $jsonObj = array();
        for ($i = 0; $i < $countValue; $i++) {
            $data = array();
            $registrationDate = date('Y-m-d', $timestamp);
            $data["Day"] = date('Y', strtotime($registrationDate));
            $data["signup"] = $userData->getYearlysignups($database, $registrationDate);
            $jsonObj[] = $data;
            $timestamp -= 365 * (24 * 3600);
        }
    }

    //printf("%d years, %d months, %d days\n", $years, $months, $days);

    echo json_encode($jsonObj);
}

if ($action == "getweekly") {


    $jsonObj = array();
    $timestamp = time();
    for ($i = 0; $i < 7; $i++) {
        $data = array();
        $registrationDate = date('Y-m-d', $timestamp);
        $timestamp -= 24 * 3600;
        $data["Day"] = $registrationDate;
        $data["signup"] = $userData->thisWeekUsers($database, $registrationDate);
        $jsonObj[] = $data;

    }
    echo json_encode($jsonObj);
}

if ($action == 'removeSubscription') {
    $payments->removeSubscription($database, $_REQUEST['paymentId']);
}


if ($action == "updateLeaderSalesPageInfor") {
    $signupName = $_POST['signupName'];
    $displayName = $_POST['displayName'];
    $defaultText = $_POST['defaultText'];
    $userId = $_POST['userId'];
    $checkUserExist = $database->executeScalar("SELECT userId FROM leadertemplate WHERE  userId='" . $userId . "'");
    $orderId = $database->executeScalar("SELECT orderId FROM leadertemplate ORDER BY orderId DESC LIMIT 1");
    $orderId = $orderId + 1;
    if (empty($checkUserExist) || $checkUserExist == "") {
        //insert
        $database->executeNonQuery("INSERT INTO leadertemplate(userId,signupName,displayName,defaultText,orderId)values ('" . $userId . "','" . $signupName . "','" . $displayName . "','" . $defaultText . "','" . $orderId . "')");
        echo 'Added successfully.';
    } else {
        //update

        $sql = "UPDATE `leadertemplate` SET `displayName`= '" . $displayName . "',`defaultText`= '" . $defaultText . "' WHERE userId='" . $userId . "'";
        $database->executeNonQuery($sql);
        echo 'Updated successfully.';
    }
    $sqlquery = "UPDATE `user` SET `isAllowSignUp`= '2' WHERE userId='" . $userId . "'";
    $database->executeNonQuery($sqlquery);
}
?>