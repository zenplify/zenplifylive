<?php
    error_reporting(0);
    require_once("../classes/init.php");
    require_once("classes/payments.php");
    require_once('classes/users.php');
    require_once("includes/header.php");
   
    $database = new database();
    $userData = new users();
    $payment = new payments();
    $paymentUser = $payment->getPaidUser($database);
    $allActiveUsers = $payment->getAllActiveUsers($database);
    $getTotalSales = $payment->getTotalSales($database);
    $getFiveMonthsPaidUser = $payment->getFiveMonthsPaidUser($database);
    $getLastFiveMonthSales = $payment->getLastFiveMonthSales($database);
    $getFiveMonthActiveUSers = $payment->getFiveMonthActiveUSers($database);
    $getOneMonthSales = $payment->getOneMonthSales($database);
	
 require_once("includes/mapsHeader.php");


?>

<section id="main-content">
  <section class="wrapper">
    <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
      <tbody>
        <tr>
          <td><table cellpadding="0" cellspacing="0" border="0">
              <tbody>
                <tr>
                  <td width=""></td>
                  <td><strong><font color="#555">Reports </font></strong></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
      <tbody>
        <tr>
          <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
        </tr>
      </tbody>
    </table>
    <div class="row">
      <div style="margin-top: 15px; margin-left: 430px; height:50px">
        <div style="display:none; width:400px; float:left;" id="selectRange" >
        <table >
          <tr>
            <td></td>
            <td ><input type="text" name="date_from" id="date_from" value="" /></td>
            <td> To </td>
            <td ><input type="text" name="date_to" id="date_to"  value=""/></td>
            <td ><input type="button" name="showsignups" id="showsignups" value="Show"/></td>
            
          </tr>
          
        </table>
        </div>
        <div style="float:right; width:120px ;margin-top:5px;">
          <select>
            <option>Choose Map</option>
            <option value="range">Select Range</option>
            <option value="twentyFourHours" selected>Last 24 Hours</option>
            <option value="oneWeek">This Week</option>
            <option value="oneMonth">This Month</option>
            <option value="sixMonth">6 Months</option>
            <option value="year">This Year</option>
          </select>
        </div>
      </div>
      <div class="twentyfourHours">
        <div id='chartContainersTwentyfourHours' style="width:700px; height:400px; float:right;"></div>
      </div>
      <div class="oneWeek">
        <div id='chartContainers' style="width:700px; height:400px; float:right;"></div>
      </div>
      <div class="oneMonth">
        <div id='chartContaineroneMonth' style="width:700px; height:400px; float:right;"></div>
      </div>
      <div class="sixMonth">
        <div id='chartContainer' style="width:700px; height:400px; float:right;"></div>
      </div>
      <div class="year">
            <div id='chartContainerTest' style="width:700px; height:400px; float:right;"></div>
      </div>
      <div class="betweenDates">
            <div id='chartContainerDates' style="width:700px; height:400px; float:right;"></div>
      </div> 
       <div style="border:1px solid #CCC; border-radius:0px;height:70px; width:380px;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td height="25px"  style="color:#FFF; background-color:#CCC; font-size:14px; padding-left:5px;">Expected revenue of this month</td>
          </tr>
          <tr>
            <td style="font-weight:bold; color:#555; font-size:20px; text-align:center; padding-top:10px;"><?php echo $getOneMonthSales; ?> </td>
          </tr>
          
        </table>
      </div>  
      <div style="border:1px solid #CCC; border-radius:0px;height:70px; width:380px; margin-top:15px;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td height="25px"  style="color:#FFF; background-color:#CCC; font-size:14px;padding-left:5px;">Total Sales</td>
          </tr>
          <tr>
            <td style="font-weight:bold; color:#555; font-size:20px; text-align:center; padding-top:10px;"><?php echo "$ ".$getTotalSales; ?> </td>
          </tr>
          
        </table>
      </div> 
      <div style="border:1px solid #CCC; border-radius:0px;height:180px; width:380px; margin-top:15px;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td height="25px"  style="color:#FFF; background-color:#CCC; font-size:16px; padding-left:5px;" colspan="3">Last 5 Sign Ups</td>
          </tr>
          <tr style="background-color:#CCC">
            <td width="50%" style="font-weight:bold; color:#555; font-size:12px; text-align:left; padding:5px; ">Customer </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; ">Payment Plan</td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; ">Payment </td>
          </tr>
          <?php
		  $users = $userData->getLastFiveUsers($database);
		  $count=1;
		  foreach($users as $u){
		  ?>
          <tr style="background-color:<?php echo ($count%2==0 ? '#CCC' : '#FFF');?>">
            <td width="50%" style="font-weight:bold; color:#555; font-size:12px; text-align:left; padding:5px; "><?php echo $u->firstName." ".$u->lastName; ?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo $u->choosePlanTitle?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo "$ ".$u->fee ?> </td>
          </tr>
		  <?php
		  $count++;
          }
		  ?>
          
        </table>
      </div>  
      <div style="border:1px solid #CCC; border-radius:0px;height:100px; width:380px; margin-top:15px;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td height="25px"  style="color:#FFF; background-color:#CCC; font-size:14px;padding-left:5px;" colspan="2">Users Report</td>
          </tr>
          <tr>
            <td style="font-weight:bold; color:#555; font-size:12px;  padding:5px;">Total Paid Users:</td>
             <td style="font-weight:bold; color:#555; font-size:12px;  padding:5px;"><?php echo $paymentUser; ?> </td>
          </tr>
          <tr>
            <td style="font-weight:bold; color:#555; font-size:12px;  padding:5px;">Total Active Users:</td>
             <td style="font-weight:bold; color:#555; font-size:12px; padding:5px;"><?php echo $allActiveUsers; ?> </td>
          </tr>
          <tr>
            <td style="font-weight:bold; color:#555; font-size:12px;  padding:5px;">List Of Active Unpaid Users:</td>
             <td style="font-weight:bold; color:#555; font-size:12px;  padding:5px;"><a href="activeUnpaidUsers.php">View</a> </td>
          </tr>
          
        </table>
      </div>  
      <div style="border:1px solid #CCC; border-radius:0px;height:155px; width:380px; margin-top:15px;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td height="25px"  style="color:#FFF; background-color:#CCC; font-size:16px;padding-left:5px;" colspan="3">Last Five Month's History</td>
          </tr>
          <tr style="background-color:#CCC">
            <td width="50%" style="font-weight:bold; color:#555; font-size:12px; text-align:left; padding:5px; ">Month </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; ">Users</td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; ">Revenue </td>
          </tr>
          <?php
		  $month_list=array("January","Feburary","March","April","May","June","July","August","september","Octuber","November","December");
		  $history = $userData->getLastFiveMonthHistory($database);
		  $record_size	= sizeof($history);
		  $last_record  = $history[$record_size-1]->m;
		  $d_count = 6;
		  $count=1;
		
		  
		  foreach($history as $h){
			  //$month = $h->m;
			  $mon=$month_list[$h->m-1];
							
		  ?>
          <tr style="background-color:<?php echo ($count%2==0 ? '#CCC' : '#FFF');?>">
            <td width="50%" style="font-weight:bold; color:#555; font-size:12px; text-align:left; padding:5px; "><?php echo $mon; ?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo $h->users;?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo "$ ".$h->fee; ?> </td>
          </tr>
		  <?php
		  $count++;
          }
		    for($i=$last_record-1;$i>=$d_count-$record_size;$i--){
		 ?>	  
		  <tr style="background-color:<?php echo ($count%2==0 ? '#CCC' : '#FFF');?>">
            <td width="50%" style="font-weight:bold; color:#555; font-size:12px; text-align:left; padding:5px; "><?php echo $month_list[$i-1]; ?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo "0"; ?> </td>
            <td width="25%" style="font-weight:bold; color:#555; font-size:12px; text-align:center; padding:5px; "><?php echo "$ 0"; ?> </td>
          </tr>
		 <?php	
		 $count++;
		 }
		  ?>
          
        </table>
      </div>   
              
     </div>
  </section>
</section>
<!--main content end--><!--right sidebar start--> 

<!--right sidebar end-->

<div style="padding-left:8%;">
  <?php require_once("includes/footer.php") ?>
</div>
<style type="text/css">
    .box {

        display: none;

    }

    .oneWeek {
       
        width: 500px;
        height: 400px;
        float: right;
    }
	.oneMonth {
       
        width: 500px;
        height: 400px;
        float: right;
    }

    .sixMonth {
       
        width: 500px;
        height: 400px;
        float: right;
    }

    .year {
       
        width: 500px;
        height: 400px;
        float: right;
    }
</style>
