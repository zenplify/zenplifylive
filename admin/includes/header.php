<?php
session_start();
require_once("../classes/register.php");
require_once("../classes/payment.php");
$register = new register();
$payment = new payment();
$username = $_SESSION['username'];
$password = $_SESSION['password'];

if ($username == "") {
    sendredirect("index.php");
}
$url = basename($_SERVER['PHP_SELF']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Zenplify Report</title>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
    <link rel="stylesheet" href="css/usersStyle.css" type="text/css">
</head>
</body>
</html>
<script type="text/javascript">
    function exportUserforOntraport(userId){


        window.location.href = '../modules/import/export_users.php?u='+userId;
    }
    function sendToRecharge(code){

        var completeUrl = 'https://zenplify.biz/recharge/'+code;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", completeUrl);
    }
    function activateUser(userId) {
        if (confirm('Are you sure you want to activate this User?')) {
            $("#user_" + userId).html('<img src="images/loader.gif" width="20" height="20">');
            $.ajax({
                type: "GET",
                url: "emailVerification.php?userId=" + userId,
                success: function (data) {
                    $("#user_" + userId).html('Activated');
                    $("#user_" + userId).css('color', 'black');
                }
            });
        }
    }
    function approveUser(userId) {
        if (confirm('Are you sure you want to approve?')) {
            $("#user_" + userId).html('<img src="images/loader.gif" width="20" height="20">');
            $.ajax({
                type: "post",
                url: "signupRequests.php",
                data: {action: 'approveSignUp', userId: userId},
                success: function (data) {
                    $("#user_" + userId).html('Approved');
                    $("#user_" + userId).css('color', 'black');
                }
            });
        }
    }
    function show(el, vis) {
        var elem = document.getElementById(el);
        elem.style.visibility = (vis) ? 'visible' : 'hidden';
        if (el == 'barfield') {
            $("#paypalId").focus();
        }
        else if (el == 'bar') {
            $("#editDays").focus();
        }
    }
    function profileGeneration(userId) {
        if (confirm('Are you sure you want to create new User Profiles?')) {
            $("#profileGeneration_" + userId).html('<img src="images/loader.gif" width="20" height="20">');
            $.ajax({
                type: "GET",
                url: "profileGeneration.php?userId=" + userId,
                success: function (data) {
                    $("#profileGeneration_" + userId).html('New User Profiles Generated');
                    $("#profileGeneration_" + userId).css('color', 'black');
                }
            });
        }
    }
    function disablePaypal(userId, paypalprofileId) {
        if (confirm('Are you sure you want to cancel future payments for this user?')) {
            $("#paypalStatus_" + userId).html('<img src="images/loader.gif" width="20" height="20">');
            $.ajax({
                type: "GET",
                url: "../modules/loginProcess/paypal.php?cancelPayment=1&profileId=" + paypalprofileId + "&userId=" + userId,
                success: function (data) {
                    $("#paypalStatus_" + userId).html('Future Payments Cancelled');
                    $("#paypalStatus_" + userId).css('color', 'black');
                }
            });
        }
    }
    function paypalLinkId(userId) {
        if (($("#paypalId").val()) && ($("#startDate").val()) && ($("select#planId").val() != "0")) {
            $("#paypalidLink_" + userId).html('<img src="images/loader.gif" width="20" height="20">');
            $("#barfield").hide();
            $("#forPaypalId").html('');
            var paypalId = $("#paypalId").val();
            paypalId = paypalId.replace("-", "%2d");
            $.ajax({
                type: "GET",
                url: "paypalIdLink.php?userId=" + userId +
                "&paypalId=" + paypalId +
                "&transactionId=" + $("#transcationId").val() +
                "&planId=" + $("select#planId").val() +
                "&startDate=" + $("#startDate").val(),
                success: function (data) {
                    var datesExplode = data.split('--');
                    $("#forPaypalId").text(paypalId);
                    $("#paypalidLink_" + userId).html('Linked');
                    $("#paypalidLink_" + userId).css('color', 'black');
                    $('#tblPaymentDetails tr:first').before('<tr><td width="150">' + $("select#planId :selected").text() + '</td><td width="150">' + datesExplode[0] + '</td><td width="150">' + datesExplode[1] + '</td><td width="150">' + datesExplode[2] + '</td></tr>');
                }
            });
        }
        else {
            $("#errorPayPalLink").show();
        }

    }
    function removeSubscription(paymentId) {
        if (confirm("Are you sure you want to remove this Subscription? This action cannot be reverted back.    ")) {
            $.ajax({
                type: "GET",
                url: "ajax.php?action=removeSubscription&paymentId=" + paymentId,
                success: function (data) {
                    $("#payment" + paymentId).fadeTo("slow", 0.00, function () { //fade
                        $(this).slideUp("slow", function () { //slide up
                            $(this).remove(); //then remove from the DOM
                        });
                    });
                }

            });
        }
    }
    $("#edit").click(function () {
        $("#hidden").show("slow");
    });
    $(document).ready(function (e) {
        $("#startDate").datepicker();
        $("#date_from").datepicker();
        $("#date_to").datepicker();
    });
</script>
</head>
<body>
<div class="container-admin">
    <div id="top_cont">
        <div id="logo">
            <img src="../images/logo.png"/>
        </div>
        <div id="search">
            <img src="../images/user-icon.png"/>
	          <span class="user_name">Welcome
                  <?php
                  if ($username != "") {
                      echo $username;
                  }
                  ?>&nbsp;!&nbsp;
	          </span> |
            <a href="logout.php" class="logout">Logout</a>
            <br/><br/>
            <a href='http://zenplify.biz/support/' target='_blank'>
                <img src="../images/help-icon.png"/>
            </a>
            <a href='http://zenplify.biz/support/' target='_blank'>
                Help
            </a>
            <br/>
            <div style="float:right; width:222px; margin-top:3px;">
                <form action="search.php" method="post" name="search">
                    <input class="searchbox2" type="text" name="search" value="<?php echo $_POST['search']; ?>"
                           placeholder="Enter text to Search"/>
                    <input style="float:left; display:none; width: 170px;" type="submit" value="Search" name="submit">
                </form>
            </div>
        </div>
    </div>
    <ul id="menu">
        <li class="next" style="z-index: 20; left: 0%;">
            <a href="login.php" class="list_button">
                <div class="left <?php echo($url == "login.php" ? 'left_selected' : ''); ?> "></div>
                <div class="middle <?php echo($url == "login.php" ? 'middle_selected' : ''); ?> ">Dashboard</div>
                <div class="right <?php echo($url == "login.php" ? 'right_selected' : ''); ?> "></div>
            </a>
        </li>
        <li class="next" style="z-index: 19; left: 12.5%;">
            <a href="users.php" class="list_button">
                <div class="left <?php echo($url == "users.php" ? 'left_selected' : ''); ?> "></div>
                <div class="middle <?php echo($url == "users.php" ? 'middle_selected' : ''); ?> ">Users</div>
                <div class="right <?php echo($url == "users.php" ? 'right_selected' : ''); ?> "></div>
            </a>
        </li>
        <li class="next" style="z-index: 18; left: 21.8%;">
            <a href="leaderSignup.php" class="list_button">
                <div class="left <?php echo($url == "leaderSignup.php" ? 'left_selected' : ''); ?> "></div>
                <div class="middle <?php echo($url == "leaderSignup.php" ? 'middle_selected' : ''); ?> ">Leaders</div>
                <div class="right <?php echo($url == "leaderSignup.php" ? 'right_selected' : ''); ?> "></div>
            </a>
        </li>
        <li class="next" style="z-index: 17; left: 32.5%;">
            <a href="leaderRequest.php" class="list_button">
                <div class="left <?php echo($url == "leaderRequest.php" ? 'left_selected' : ''); ?> "></div>
                <div class="middle <?php echo($url == "leaderRequest.php" ? 'middle_selected' : ''); ?> ">Leader
                    Request
                </div>
                <div class="right <?php echo($url == "leaderRequest.php" ? 'right_selected' : ''); ?> "></div>
            </a>
        </li>
        <li class="next" style="z-index: 16; left: 48.3%;">
            <a href="reports.php" class="list_button">
                <div class="left <?php echo($url == "reports.php" ? 'left_selected' : ''); ?> "></div>
                <div class="middle <?php echo($url == "reports.php" ? 'middle_selected' : ''); ?> ">Reports</div>
                <div class="right <?php echo($url == "reports.php" ? 'right_selected' : ''); ?> "></div>
            </a>
        </li>
    </ul>
    <div style="height: 30px;" ;>&nbsp;</div>