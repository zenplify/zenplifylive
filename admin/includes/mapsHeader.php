<link rel="stylesheet" href="js/jqx.base.css" type="text/css"/>
<!--<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>-->
<script type="text/javascript" src="js/jqxcore.js"></script>
<script type="text/javascript" src="js/jqxdata.js"></script>
<script type="text/javascript" src="js/jqxdraw.js"></script>
<script type="text/javascript" src="js/jqxchart.core.js"></script>
<script type="text/javascript">
	$(document).ready(function () {

		var sampleDataHourly = [
			<?php
			 $timestamp = time();
			 $startshowtime=0;
			 $endshowime=0;
			 for ($i = 0 ; $i < 8 ; $i++) {

			   $registrationDate= date('Y-m-d H:j:s', $timestamp);
				$timestamp -=  3*3600;

			$startshowtime=$startshowtime;
			$endshowime=$startshowtime+3;


			?>

			{Day: '<?php echo $startshowtime.":00 - ".$endshowime.":00";?>', signup:<?php echo $userData->thisDayUsers($database,$registrationDate);?>}
			<?php
			$startshowtime=$endshowime;
			echo($i<7?",":"");
			}
			?>

		];

//console.log(sampleDataHourly);
		// setup the chart
		$(".twentyfourHours").show();
		$('#chartContainersTwentyfourHours').jqxChart(setgraph(sampleDataHourly, 10, 200));
		//$('#chartContainerTest').jqxChart(settings);
		//$('#chartContaineroneMonth').jqxChart(settings);

		var sampleDataWeekly = [
			<?php
			 $timestamp = time();
			 for ($i = 0 ; $i < 7 ; $i++) {

			   $registrationDate= date('Y-m-d', $timestamp);
				$timestamp -= 24 * 3600;

			?>
			{Day: '<?php echo date("d-M-y",strtotime($registrationDate))?>', signup:<?php echo $userData->thisWeekUsers($database,$registrationDate);?>}
			<?php
			echo($i<6?",":"");
			}
			?>

		];

		// setup the chart
		$('#chartContainers').jqxChart(setgraph(sampleDataWeekly, 10, 200));
		//$('#chartContainerTest').jqxChart(settings);
		//$('#chartContaineroneMonth').jqxChart(settings);

//////////////////////////////////////////////////// for Month ///////////////////////////////////////////////////////////////////

		var sampleDataMonthly = [
			<?php
				$time= date('m-01-Y');
				$time = explode('-',$time);
				$time = $time[0]."/".$time[1]."/".$time[2];
				$timestamp = strtotime($time);

				$maxDate =date('t');
			 for ($i = 0 ; $i < $maxDate ; $i++) {

			   $registrationDate= date('Y-m-d', $timestamp);
				$timestamp += (24 * 3600);

			?>
			{Day: '<?php echo $i+1; ?>', signup:<?php echo $userData->getThisMonthSignups($database,$registrationDate);?>}
			<?php
			echo($i<$maxDate?",":"");
			}
			?>

		];
		//console.log(sampleDataMonthly);
		$('#chartContaineroneMonth').jqxChart(setgraph(sampleDataMonthly, 10, 100));

		//////////////////////////////////////////////////// for Six Month ///////////////////////////////////////////////////////////////////

		var sampleDataSixMonthly = [
			<?php
			$time= date('m-t-Y');
			$time = explode('-',$time);
			$time = $time[0]."/".$time[1]."/".$time[2];
			$timestamp = strtotime($time);
			for ($i = 0 ; $i < 6 ; $i++) {

			  $registrationDate= date('Y-m-d', $timestamp);
			  $maxDate =date('t',$timestamp);
			  $timestamp -= $maxDate *(24 * 3600);

		   ?>
			{Day: '<?php echo $i+1;?>', signup:<?php echo $userData->getSixMonthsignups($database,$registrationDate);?>}
			<?php
			echo($i<5?",":"");
			}
			?>

		];
		//  console.log(sampleDataSixMonthly);
		$('#chartContainer').jqxChart(setgraph(sampleDataSixMonthly, 100, 2000));

		//////////////////////////////////////////////////// for  Year ////////////////////////////////////////

		var sampleDataYearly = [
			<?php
			$time= date('01-31-Y');
			$time = explode('-',$time);
			$time = $time[0]."/".$time[1]."/".$time[2];
			$timestamp = strtotime($time);

			for ($i = 0 ; $i < 12 ; $i++) {

			  $registrationDate= date('Y-m-d', $timestamp);
			  $maxDate =date('t',$timestamp);
			   $timestamp += $maxDate*(24 * 3600);

		   ?>
			{Day: '<?php echo $i+1;?>', signup:<?php echo $userData->getSixMonthsignups($database,$registrationDate);?>}
			<?php
			echo($i<11?",":"");
			}
			?>

		];
		console.log(sampleDataYearly);
		$('#chartContainerTest').jqxChart(setgraph(sampleDataYearly, 250, 2000));

		/////////////// show graph between dates //////////////

		$("#showsignups").click(function () {

			var d1 = $("#date_from").val();
			var d2 = $("#date_to").val();

			var date_1 = new Date(d1).getTime();
			var date_2 = new Date(d2).getTime();
			//console.log(new Date(d1).getTime());	
			//console.log(new Date(d2).getTime());	

			var d3 = "";
			if (date_1 > date_2) {
				d3 = d2;
				d2 = d1;
				d1 = d3;
			}

			//console.log(d1);
			//console.log(d2);

			if (d1 != "" && d2 != "") {

				$.ajax({
					url    : "ajax.php",
					type   : "POST",
					data   : {action: "getJsonBetweenDates", date_from: d1, date_to: d2},
					success: function (result) {

						result = $.parseJSON(result)

						var sampleDataBetweenDates = result;
						$(".twentyfourHours").hide();
						$(".sixMonth").hide();
						$(".oneWeek").hide();
						$(".oneMonth").hide();
						$(".year").hide();
						$(".twentyfourHours").hide();

						$(".betweenDates").show();
						$('#chartContainerDates').jqxChart(setgraph(sampleDataBetweenDates, 500, 2500));

					}
				});
			}
			else {
				alert("Please select Date");
			}
		});

	});

</script>
<script type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("select").change(function () {
			$("select option:selected").each(function () {
				if ($(this).attr("value") == "oneWeek") {
					$(".sixMonth").hide();
					$(".oneMonth").hide();
					$(".year").hide();
					$(".oneWeek").show();
					$(".betweenDates").hide();
					$(".twentyfourHours").hide();
					$("#selectRange").hide();
				}
				if ($(this).attr("value") == "oneMonth") {
					$(".sixMonth").hide();
					$(".year").hide();
					$(".oneWeek").hide();
					$(".oneMonth").show();
					$(".betweenDates").hide();
					$(".twentyfourHours").hide();
					$("#selectRange").hide();
					$("#selectRange").hide();
				}
				if ($(this).attr("value") == "sixMonth") {
					$(".oneWeek").hide();
					$(".year").hide();
					$(".oneMonth").hide();
					$(".sixMonth").show();
					$(".betweenDates").hide();
					$(".twentyfourHours").hide();
					$("#selectRange").hide();
				}
				if ($(this).attr("value") == "year") {
					$(".sixMonth").hide();
					$(".oneWeek").hide();
					$(".oneMonth").hide();
					$(".year").show();
					$(".betweenDates").hide();
					$(".twentyfourHours").hide();
					$("#selectRange").hide();
				}
				if ($(this).attr("value") == "twentyFourHours") {
					$(".sixMonth").hide();
					$(".oneWeek").hide();
					$(".oneMonth").hide();
					$(".year").hide();
					$(".betweenDates").hide();
					$(".twentyfourHours").show();
					$("#selectRange").hide();
				}
				if ($(this).attr("value") == "range") {
					$("#selectRange").show();
				}
			});
		}).change();
		$(".oneWeek").hide();
		$(".sixMonth").hide();
		$(".oneMonth").hide();
		$(".year").hide();
		$(".betweenDates").hide();

	});

	function setgraph(sampleData, interval, maxval) {
		// prepare jqxChart settings
		var settings = {
			title           : "Signup Graph",
			description     : "Zenplify signup Graph for Intervals",
			enableAnimations: true,
			showLegend      : true,
			padding         : { left: 10, top: 5, right: 10, bottom: 5 },
			titlePadding    : { left: 90, top: 0, right: 0, bottom: 10 },
			source          : sampleData,
			xAxis           : {
				dataField        : 'Day',
				showTickMarks    : true,
				valuesOnTicks    : false,
				tickMarksInterval: 1,
				tickMarksColor   : '#888888',
				unitInterval     : 1,
				gridLinesInterval: 1,
				gridLinesColor   : '#888888',
				axisSize         : 'auto'
			},
			colorScheme     : 'scheme05',
			seriesGroups    : [
				{
					type      : 'line',
					showLabels: true,
					valueAxis : {
						unitInterval  : interval,
						minValue      : 0,
						maxValue      : maxval,
						description   : 'Signups',
						axisSize      : 'auto',
						tickMarksColor: '#888888'
					},
					series    : [
						{ dataField: 'signup', displayText: 'Total signups', symbolType: 'square'}
					]
				}
			]
		};

		return settings;
	}
</script>
