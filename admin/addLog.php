<?php
	require_once("../classes/init.php");
	require_once("includes/header.php");
    require_once("classes/helpdesklog.php");
    $database = new database();
    $logData = new HelpDeskLog();
    $logId = $_GET['help_desk_LogId'];
if(isset($_REQUEST['submit'])){
   $addLog=$logData->addUpdateRecord($database,$logId,$_POST);
}

?>
<style>
    .textArea{
        margin: 0px;
        width: 400px;
        height: 130px;
        background: #F9F9F9;
        border: 1px solid #ccc;
        padding: 16px;
        border-radius: 2px;
    }
    .user-detail-li{
        width: 230px !important;
        font-size: 14px;
        font-weight: bold;
    }
    .textField{
        width: 428px;
        height: 28px;
        background: #F9F9F9;
        border: 1px solid #ccc;
        border-radius: 2px;
        padding-left: 5px;
    }
    .submitButton{
        height: 30px;
        width: 71px;
        border: 1px solid #949090;
        border-radius: 4px;
        font-weight: bold;
        cursor: pointer;
    }
    .submitButton:hover{
        background-color: #A7A2A2;
    }
</style>
	<section id="main-content">
		<section class="wrapper">
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td>
			            <table cellpadding="0" cellspacing="0" border="0">

			                <tbody>
			                <tr>
			                    <td width=""></td>
			                    <td><strong><font color="#555">User's Log Details</font></strong></td>
			                </tr>
			                </tbody>
			            </table>
			        </td>
			    </tr>
			    </tbody>
			</table>
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tbody>
			    <tr>
			        <td bgcolor="#CCCCCC" height="1px;" align="right" width="100%"></td>
			    </tr>
			    </tbody>
			</table>
			<div class="row">
			    <ul class="user-detail">
			        <?php
				        $ld = $logData->singleLogDetail($database, $logId);

			        ?>
                  <form action="" method="post" name="addlog">
				    <table id="tblUserDetail">
			            <tr>
			                <td>
			                    <li class="user-detail-li">User Name</li>
			                </td>
			                <td>
			                    <li><input required="required" class="textField" type="text" name="user_name" value="<?php echo $ld->user_name; ?>"/></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">User Email</li>
			                </td>
			                <td>
			                    <li><input required="required" class="textField" type="text" name="user_email" value="<?php echo $ld->user_email; ?>"/></li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Ticket ID</li>
			                </td>
			                <td>
			                    <li><input required="required" class="textField" type="text" name="ticketId" value="<?php echo $ld->ticketId; ?>"/></li>
			                </td>
			            </tr>
                        <tr>
                            <td>
                                <li class="user-detail-li">Solved by</li>
                            </td>
                            <td>
                                <li><select style="height: 33px; width: 433px;" class="textField" name="solvedBy">

                                        <option >Select</option>
                                        <option <?php if($ld->solvedBy=='Heather Mitchel'){ echo ' selected="selected"'; } ?>  value="Heather Mitchel">Heather Mitchel</option>
                                        <option <?php if($ld->solvedBy=='JoAn Richardson'){ echo ' selected="selected"'; } ?>   value="JoAn Richardson">JoAn Richardson</option>
                                        <option <?php if($ld->solvedBy=='Atif'){ echo ' selected="selected"'; } ?>  value="Atif">Atif</option>
                                        <option <?php if($ld->solvedBy=='Mansoor'){ echo ' selected="selected"'; } ?> value="Mansoor">Mansoor</option>
                                        <option <?php if($ld->solvedBy=='Mahrukh'){ echo ' selected="selected"'; } ?>   value="Mahrukh">Mahrukh</option>
                                </select> </li>
                            </td>
                        </tr>

			            <tr>
			                <td>
			                    <li class="user-detail-li">User's Request</li>
			                </td>
			                <td>
			                    <li><textarea required="required" cols="30"  rows="12" name="userText" class="textArea"><?php echo $ld->ticketText; ?></textarea> </li>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <li class="user-detail-li">Support Team Comment</li>
			                </td>
			                <td>
			                    <li><textarea required="required" name="teamText" cols="30"  rows="12" class="textArea"><?php echo $ld->technicalcomment; ?></textarea></li>
			                </td>
			          </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input class="submitButton" type="submit" name="cancel" value="Cancel"/>
                                <?php if(empty($logId)){$text='Save';}else{$text='Update';} ?>
                                <input class="submitButton" type="submit" name="submit" value="<?php echo $text; ?>"/>

                            </td>
                        </tr>

			        </table>
                  </form>
			    </ul>
			</div>
		</section>
	</section>
<?php require_once("includes/footer.php") ?>
