<?php
	error_reporting(0);
	session_start();
	require_once('../classes/plan.php');
	require_once('../classes/contact.php');
	require_once('../classes/register.php');
	require_once('../classes/notification.php');
	require_once('../classes/init.php');
	require_once('../classes/import.php');

	$userId = $_SESSION['userId'];
	$leaderId = $_SESSION['leaderId'];
	$generatedVia = $_SESSION['generatedVia'];

	if (isset($_POST['action']) && !empty($_POST['action']))
	{
		$action = $_POST['action'];
		switch ($action)
		{
			case 'ChangePlanName' :
				ChangePlanName();
				break;
			case 'DeletePlanStep' :
				DeletePlanStep();
				break;
			case 'DeletePlan' :
				DeletePlan();
				break;
			case 'CreatePlan' :
				CreatePlan($leaderId);
				break;
			case 'EditPlan' :
				EditPlan();
				break;
			case 'ChangePlanTyep':
				ChangePlanTyep();
				break;
			case 'AddReminderInPlan' :
				AddReminderInPlan();
				break;
			case 'UpdatePlanGroup' :
				UpdatePlanGroup();
				break;
			case 'DeleteContact' :
				DeleteContact();
				break;
			case 'DisablePlan' :
				DisablePlan();
				break;
			case 'ActivatePlan' :
				ActivatePlan();
				break;
			case 'UpdateRecordsListings' :
				UpdateRecordsListings();
				break;
			case 'ImportValues' :
				ImportValues();
				break;
			case 'DeleteGroup' :
				DeleteGroup();
				break;
			case 'verifyUser' :
				verifyUser();
				break;
			case 'transferContact' :
				transferContact();
				break;
			case 'acceptTransferContact' :
				acceptTransferContact();
				break;
			case 'rejectTransferContact' :
				rejectTransferContact();
				break;
			case 'activatepayment':
				activatepayment();
				break;

			// ...etc...
		}

	}

	function activatepayment()
	{
		$payment = new payment();
		$result = $payment->deletePaypalProfile($_REQUEST['userId']);
		echo $result;
	}

	function ChangePlanName()
	{
		$name = $_POST['name'];
		$plan_id = $_POST['plan_id'];
		$pl = new plan();
		$pl->ChangePlanName($name, $plan_id);
		//echo $name."plan=".$plan_id;
		echo $name;
	}

	function DeletePlanStep()
	{
		$step_id = $_POST['step_id'];
		$pl = new plan();
		$pl->DeletePlanStep($step_id);
		//echo $pl;
	}

	function DeletePlan()
	{
		$plan_id = $_POST['plan_id'];
		$pl = new plan();
		$pl->DeletePlan($plan_id);
	}

	function CreatePlan($leaderId)
	{
		$plan_name = $_POST['p_name'];
		$userId = $_POST['userId'];
		$pl = new plan();
		$plan_id = $pl->CreatePlan($plan_name, $userId, $leaderId);
		echo $plan_id;
	}

	function EditPlan()
	{
		$database = new database();
		$notification = new notification();

		$userId = $_REQUEST['userId'];
		$plan_id = $_REQUEST['planId'];
		$plan_name = $_REQUEST['p_name'];
		$leaderId = $_REQUEST['leaderId'];

		$pl = new plan();

		$planIdUpdated = $pl->EditPlan($database, $plan_name, $plan_id, $userId, $leaderId);
		if ($userId == $leaderId)
		{
			$notification->notificationToUser($database, $userId, $plan_id, 3, 2, "Do you want to Edit the Plan \'" . $plan_name . "\'");
		}
		if ($_REQUEST['updateType'] == 'optIn')
		{
			ChangePlanTyep($planIdUpdated);
		}
		echo $planIdUpdated;
	}

	function ChangePlanTyep($plan_id = 0)
	{
		$pl = new plan();
		if ($plan_id == 0)
		{
			$plan_id = $_POST['plan_id'];
		}
		$pl->ChangePlanTyep($plan_id);
	}

	function AddReminderInPlan()
	{
		$plan_id = $_POST['plan_id'];
		$pl = new plan();
		$pl->AddReminderInPlan($plan_id);

	}

	function UpdatePlanGroup()
	{
		$plan_id = $_POST['plan_id'];
		$group_id = $_POST['group_id'];
		$user_id = $_POST['user_id'];
		$pl = new plan();
		$pl->UpdatePlanGroup($plan_id, $group_id, $user_id);

	}

	function DeleteContact()
	{
		$contact_id = $_POST['contact_id'];
		$con = new contacts();
		$con->DeleteContact($contact_id);
	}

	function ActivatePlan()
	{
		$plan_id = $_POST['plan_id'];
		$pl = new plan();
		$pl->ActivatePlan($plan_id);
	}

	function DisablePlan()
	{
		$plan_id = $_POST['plan_id'];
		$pl = new plan();
		$pl->DisablePlan($plan_id);
	}

	function UpdateRecordsListings()
	{
		echo "hellsdfasdfo";
		/*$updateRecordsArray = $_POST['recordsArray'];
		$planId = $_POST['planId'];
		$listingCounter = 1;
		//print_r($updateRecordsArray);
		$database = new database();
		foreach ($updateRecordsArray as $recordIDValue)
		{

			$database->executeNonQuery("UPDATE plansteps set order_no=". $listingCounter . " WHERE stepId = " . $recordIDValue." AND planId=".$planId );
			$listingCounter++;

			//$database->executeNonQuery("UPDATE plansteps set order_no=". $listingCounter . " WHERE (order_no = " . $recordIDValue." AND planId=".$planId.")" );
			//echo "UPDATE plansteps set order_no=". $listingCounter . " WHERE order_no = " . $recordIDValue."and planId=".$planId;
			//echo "UPDATE plansteps set order_no=". $listingCounter . " WHERE order_no = " . $recordIDValue." AND planId=".$planId;
			//$database->executeNonQuery("UPDATE plansteps set order_no=". $listingCounter . " WHERE order_no = " . $recordIDValue );
			//$listingCounter = $listingCounter + 1;
		}

		*/
	}

	function ImportValues()
	{
		//echo $userId=$_POST['userId'];
		$dbList = $_POST['dbList'];
		$csvList = $_POST['csvList'];
		$userId = $_POST['userId'];
		$dbList = substr($dbList, 0, -1);
		$dbArray = explode(",", $dbList);
		$csvList = substr($csvList, 0, -1);
		$csvArray = explode(",", $csvList);
		$imp = new import();
		echo $imp->ImportData($dbArray, $csvArray, $userId);

	}

	/*function DeleteGroup(){
		$groupId=$_GET['id'];
		require("../../classes/init.php");
		$database = new database();
		if($groupId!=''){

		$database->executeNonQuery("DELETE FROM groups  WHERE groupId = " . $groupId);

		echo 1;
		}
		}*/


	function verifyUser()
	{
		$user_name = $_POST['u_name'];
		$reg = new register();
		$data = $reg->selectUserIdByUsernameOrEmail($user_name);
		if (!empty($data))
		{
			echo $data->userId;
		}
		else
		{
			echo "";
		}
	}

	function transferContact()
	{
		$user_id = $_POST['user_id'];
		//$contact=$_POST['contact'];
		$Suser = $_POST['SUserId'];
		$contact = $_GET['con'];
		$contacts = explode(",", $contact);
		$con = new contact();
		$result = $con->AddTransferContact($user_id, $contacts, $Suser);
		//echo $contacts[0]."hello".$contacts[1];
		//echo $contact;
		//print_r($contacts);
	}

	function acceptTransferContact()
	{
		$user_id = $_POST['userId'];
		$contact = $_GET['con'];
		$contacts = explode(",", $contact);
		$con = new contact();
		//print_r($contacts);
		$result = $con->AcceptTransferContact($contacts, $user_id);
		echo $result;
	}

	function rejectTransferContact()
	{
		$contact = $_GET['con'];
		$contacts = explode(",", $contact);
		$con = new contact();
		$result = $con->RejectTransferContact($contacts);
		echo $result;
	}

?>
