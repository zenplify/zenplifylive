<?php
class Error
{
	var $errorsList; 
	var $width;

	function Error()
	{
		$this->errorsList = ""; 
		$this->width = 500;
	}
	function setWidth($width)
	{
		$this->width=$width;
	}
	function getErrors()
	{
		if (strlen($this->errorsList) > 0)
		{
			$contents = ""; 
			$contents .= "<table cellpadding=\"2\" cellspacing=\"2\"  border=\"0\" width=\"" . $this->width . "\">";
			$contents .= "<tr>";
			$contents .= "<td class=\"error\" width=\"17\"><img src=\"".$CONF["siteURL"]."images/error.gif\"></td>";
			$contents .= "<td class=\"error\" width=\"" . ($this->width-17) . "\">Following error(s) have occurred!</td>";
			$contents .= "</tr>";
			$contents .= "<tr>";
			$contents .= "<td class=\"error\" colspan=\"2\"><ul>" . $this->getErrorsList() . "</ul></td>";
			$contents .= "</tr>";
			$contents .= "</table>";
			return $contents; 
		}
		else
		{
			return "";
		}
	}
	function addError($error)
	{
		$this->errorsList .= "<li>" . $error . "</li>";
	}
	function getErrorsList()
	{
		return  $this->errorsList;
	}
	function isErrorFound()
	{
		return strlen($this->errorsList) > 0 ? true : false;
	}
	function clearErrors()
	{
		$this->errorsList = "";
	}
}
?>