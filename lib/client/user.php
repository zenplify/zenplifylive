<?php
class User
{
 function User()
 {
   //session_start();
 }
 // set User Data in Session
 function SetId($id)
 {
   $_SESSION["uid"] = $id;
 }
 function SetFirstName($firstName)
 {
   $_SESSION["fname"] = $firstName;
 }
 function SetLastName($lastName)
 {
 	$_SESSION["lname"] = $lastName;
 }
 function SetLogin($login)
 {
   $_SESSION["login"] = $login;
 }
 function SetEmail($email)
 {
   $_SESSION["email"] = $email;
 }
 // get User from Session Datas
 function getId()
 {
//   if(session_is_registered ("id"))
   if(isset($_SESSION["uid"]))
   {
	   return $_SESSION["uid"];
   }
 }
 function getFirstName()
 {
   if(isset($_SESSION["fname"]))
   {
	   return $_SESSION["fname"];
   } 
 }
 function getLastName()
 {
   if(isset($_SESSION["lname"]))
   {
 	return $_SESSION["lname"];
   }
 }
 function getLogin()
 {
   if(isset($_SESSION["login"]))
   {
	  return $_SESSION["login"];
   }
 }
 function getEmail()
 {
   if(isset($_SESSION["email"]))
   {
     return $_SESSION["email"];
   } 
 }
 function isLogin()
 {
    return isset($_SESSION["login"]);
 }
 function destroy()
 {
  if(isset($_SESSION["login"]))
  {
    session_unset();
	session_destroy(); 
  }
 }
}
?>