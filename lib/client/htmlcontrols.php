<?php
class HtmlControls {

	var $panelWidth=630;
	var $panelType=1;
	var $panelAlignment;

	var $pagingLinksWidth="100%";
	var $pagingLinksAlignment;

    var $dataStyle;
    var $currentDataStyle;

	//
	// Writes Page Title
	//
	
	function Title($title, $width=711) {
	return '<table width="711" cellpadding="0" cellspacing="0" border="0">
	<tr><td align="left"><img src="images/hd_img.jpg" /></td></tr>
	<tr>
		<td height="30" align="right" class="h4" bgcolor="#000000">'.$title.'&nbsp;&nbsp;&nbsp;</td>
	</tr>
	</table>';
	}
	function Title1($title, $width=711) {
	return '<table width="711" cellpadding="0" cellspacing="0" border="0">
	<tr><td align="right"><img src="images/hd_img.jpg" /></td></tr>
	<tr>
		<td height="30" align="left" class="h4" bgcolor="#000000">&nbsp;&nbsp;'.$title.'</td>
	</tr>
	</table>';
	}
	function getPageTitle($title, $width=711) {
	return '<table width="711" cellpadding="0" cellspacing="0" border="1">
	<tr>
		<td width="14"><img src="images/trans.gif" /></td>
		<td class="title">'.$title.'</td>
	</tr>
	</table>';
	}
	function getPageTitle1($title, $width=711) {
	return '<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="h2" align="right"><img src="images/trans.gif" height="10" /><br />'.$title.'</td>
		<td width="14"><img src="images/trans.gif" /></td>
		<td width="27"><img src="images/hd.jpg" height="39" /></td>
	</tr>
	</table>';
	}
	function PageTitle($title, $width=711) {
	return '<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="27"><img src="images/hd.jpg" height="39" /></td>
		<td width="14"><img src="images/trans.gif" /></td>
		<td class="h2"><img src="images/trans.gif" height="10" /><br />'.$title.'</td>
	</tr>
	</table>';
	}
	//
	// Writes Sub Title
	//
	function getSubTitle($title, $width="100%", $type=1) {
		return 	'<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0">
				  <tr>
					<td width="12"><img src="images/trans.gif"></td>
					<td width="22" valign="top"><img src="images/aro'.$type.'.jpg"></td>
					<td width="4"><img src="images/trans.gif"></td>
					<td  valign="top">
					<img src="images/trans.gif" height="3"><br>
					<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td class="h'.$type.'">'.$title.'</td>
					  </tr>
					</table>
					</td>
				  </tr>
				</table>
				<img src="images/trans.gif" height="3"><br>
				<table align="center" width="90%" cellpadding="0" cellspacing="0" border="0">
				  <tr>
					<td height="1" class="lineStyle1"><img src="images/trans.gif"></td>
				  </tr>
				</table>
				<img src="images/trans.gif" height="3"><br>';
	}
	
	function getFixedPanelHeader($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\">";

		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td class=\"backgr5\" height=\"2\"><img src=\"images/trans.gif\"></td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td height=\"20\" class=\"lineStyle1\"><font class=\"h5\">&nbsp;&nbsp;$title</font></td>";
		$contents .=  "<td class=\"h5\" class=\"lineStyle1\">$secontTitle</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"10\"><br>";
		return $contents;
	}
	function getFixedPanelFooter($footer="", $panelType=2)
	{
		$contents="";
		$contents .=  "<img src=\"images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		
		return $contents;
	}
	
	function getPanelHeader($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"center\" width=\"750\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td>";

		$contents .=  "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td height=\"20\" align=\"left\"><font class=\"h1\">&nbsp;&nbsp;$title</font></td>";
		$contents .=  "<td class=\"h5\">$secontTitle</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"2\"><br>";
		return $contents;
	}

	//panel footer
	function getPanelFooter($footer="", $panelType=2)
	{
		$contents =  "<img src=\"../images/trans.gif\" width=\"1\" height=\"1\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		return $contents;
	}

	function getFormPanelHeader($title, $width="560") {
		return 	'<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0" align="center">
					<tr>
						<td width="22" valign="top"><img src="images/aro5.jpg"></td>
						<td width="4"><img src="images/trans.gif"></td>
						<td  valign="top">
						<img src="images/trans.gif" height="2"><br>
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="h5">&nbsp;'.$title.'</td>
								</tr>
							</table>
							<img src="images/trans.gif" height="2"><br>
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td height="1" class="linkStyle1"><img src="images/trans.gif"></td>
								</tr>
							</table>
							<img src="images/trans.gif" height="4"><br>
						</td>
					</tr>
				</table>
				<img src="images/trans.gif" height="4"><br>
				<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0" align="center">
				  <tr>
					<td class="lineStyle1">';
	}

	function getFormPanelFooter() {
		return 	'</td>
			  </tr>
			</table>';
	}
	
	function getLinePanelHeader($title='') {
	
		return '<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
				  <tr>
					<td style="border:1px #BB520C solid;">
					 <table cellpadding="0" cellspacing="4" align="center" border="0" width="100%">
					   <tr>
						<td style="border:1px #C1B456 solid;">
						 <table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
						   <tr>
							<td height="6"><img src="images/trans.gif"></td>
						   </tr>
						   <tr>
							<td style="padding-left:20px;" class="h2">'.$title.'</td>
						   </tr>
						 </table>
						 <img src="images/trans.gif" width="1" height="10"><br>';

	}

	function getLinePanelFooter() {

		return '<img src="images/trans.gif" width="1" height="10"><br></td></tr></table></td></tr></table>
				<img src="images/trans.gif" width="1" height="10"><br>	';

	}	

	function setPanelWidth($panelWidth) {
		$this->panelWidth=$panelWidth;
	}

	function setPagingLinksWidth($pagingLinksWidth) {
		$this->pagingLinksWidth=$pagingLinksWidth;
	}

	function setDataStyle($dataStyle) {
		$this->dataStyle=$dataStyle;
	}

	function showPagingLinks($database)
    {
		$contents = '';
		$contents.=($database->isRecordFound() ? "<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"".$this->pagingLinksWidth."\" border=\"0\"><tr><td align=\"right\"><div class=\"paginationLinks\"><UL>". $database->getBackLink("Previous").($database->getTotalRecords()<=$database->pageSize?"":$database->getPagesLinks()).$database->getNextLink("Next")."</ul><SPAN class=\"fontStyle3\" style=\"font-size:11px;\">&nbsp;(" . $database->getTotalRecords() . "&nbsp;results)&nbsp;</SPAN></div></td></tr></table><img src=\"".$GLOBALS["IMAGES_URL"]."trans.gif\" width=\"1\" height=\"10\"><br>":"");
		return $contents;
    }

	function setPanelType($panelType) {
		$this->panelType=$panelType;
	}

    function getDataStyle() {
        if (strlen($this->currentDataStyle) == 0)
            $this->getNextDataStyle();
        return $this->currentDataStyle;
    }

    function getNextDataStyle() {
        if (strlen($this->currentDataStyle) == 0) {
			$this->currentDataStyle = "tbldata1";
        }
		else if(strcmp($this->currentDataStyle, "tbldata1") == 0) {
			$this->currentDataStyle = "tbldata2";
		}
		else {
			$this->currentDataStyle = "tbldata1";
		}
    }

	function getProduct() {
		global $database;
		return  '
			<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
			  <tr>
				<td  bgcolor="#ffffff">
				<img src="images/trans.gif" width="1" height="2"><br>
				<table width="119" cellpadding="0" cellspacing="0" border="0" align="center">
				  <tr>
					<td colspan="3" class="lineStyle1" height="1"><img src="images/trans.gif"></td>
				  </tr>
				  <tr>
					<td width="1" class="lineStyle1"><img src="images/trans.gif"></td>
					<td width="117" height="115" align="center"><img src="images/products/thumb/'.$database->getColumn("prod_simg").'"></td>
					<td width="1" class="lineStyle1"><img src="images/trans.gif"></td>
				  </tr>
				  <tr>
					<td colspan="3" class="lineStyle1" height="1"><img src="images/trans.gif"></td>
				  </tr>
				</table>
				<img src="images/trans.gif" height="4"><br>
				<table width="119" cellpadding="0" cellspacing="0" border="0" align="center">
				  <tr>
					<td width="6"><img src="images/trans.gif"></td>
					<td class="h5">'.substr($database->getColumn("prod_name"),0,15).'</td>
				  </tr>
				  <tr>
					<td colspan="2" height="3"><img src="images/trans.gif"></td>
				  </tr>
				  <tr>
					<td width="6"><img src="images/trans.gif"></td>
					<td height="26" class="smallText">'.substr($database->getColumn("prod_specs"),0,37).'</td>
				  </tr>
				</table>
				<img src="images/trans.gif" height="17"><br>
				<table width="119" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff">
				  <tr>
					<td height="1" class="lineStyle1"><img src="images/trans.gif"></td>
				  </tr>
				  <tr>
					<td height="6"><img src="images/trans.gif"></td>
				  </tr>
				  <tr>
					<td align="center"><a href="buy.php?prod='.$database->getColumn("prod_id").'"><img src="images/btn_buy.jpg" border="0"></a>&nbsp;<a href="detail.php?cat='.$database->getColumn("cate_id").'&scat='.$database->getColumn("scate_id").'&prod='.$database->getColumn("prod_id").'"><img src="images/btn_detail.jpg" border="0"></a></td>
				  </tr>
				  <tr>
					<td height="4"><img src="images/trans.gif"></td>
				  </tr>
				</table>
				</td>
				  </tr>
				</table>
				';
	}
}
