<? include "includes/init.php" ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title><?=$PAGE_TITLE?></title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">
<?

	$controls->writePageTitle("State Wise Grants Report", NULL);

$controls->setPanelWidth(700);
echo $controls->getSearchPanelHeader();
?>
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
              <tr>
			  <td class="titreSection">&nbsp;&nbsp;Search</td>
                <td height="50">
                  <form name=search action=<?=$_SERVER["PHP_SELF"]?> method=post>
                  <table align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="FieldCaptionFONT">enter text to search&nbsp;&nbsp;</td>
                      <td><input type="Text" name="keywords" size="30" value="<?=$_REQUEST["keywords"]?>">&nbsp;&nbsp;</td>
					  <td class="FieldCaptionFONT">In&nbsp;&nbsp;</td>
                      <td><select style="width:220px;" name="searchtype">
					  	  <option value="all" selected>All Fields</option> 
						   <?
						   for ($i=0; $i<count($searchingFields); $i+=2)
						   {
								echo "<option value=\"".$searchingFields[$i]."\" ".($searchingFields[$i] == $_REQUEST["searchtype"]?"Selected":"").">".$searchingFields[$i+1]."</option>";
						   }
						   ?>
						  </select>&nbsp;&nbsp;
                          </td>
                      <td><input type="image" src="images/btn_search.jpg" border="0" class="image" alt="Search"></td>
                  </tr></table></form></td></tr></table>
<?
	echo $controls->getSearchPanelFooter();
?>
			<br>

                  <table class="wrapper" cellspacing="1" cellpadding="2" border="0" width="100%">
                    <tr> 
                      <td width="10"><img src="images/trans.gif" width="10" height="1"></td>
					  <td>

			<?
				$controls->writeSubTitle("List of Top 100 Receipients", NULL);

		        $sorter = new Sorter();
		        $sorter->setDefaultSorting("ID asc");
		        $sorter->setPageUrl($_SERVER["PHP_SELF"]);
		        $sorter->setOrder(isset($_REQUEST["order"])?$_REQUEST["order"]:"");
		        $sorter->setSorting(isset($_REQUEST["sorting"])?$_REQUEST["sorting"]:"");
		        $sorter->setNames("#|University Name|Country|Address|City|State|Zip|Principal Researcher Name|Principal Researcher Email|Principal Researcher Telephone|Principal Researcher Department");
		        $sorter->setFields("ID|university_name|cntry_name|university_address|university_city|state_name|university_zip|Principal_Researcher_Name|Principal_Researcher_Email|Principal_Researcher_Phone|dept_name");
				
			?>
                  <table class="wrapper" cellpadding="4" cellspacing="1" border="0" width="100%">
                    <tr> 
                      <td class="title" nowrap><?= $sorter->getTableHead(1) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(2) ?></td>
					  <td class="title" nowrap><?= $sorter->getTableHead(3) ?></td>
					  <td class="title" nowrap><?= $sorter->getTableHead(4) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(5) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(6) ?></td>
					  <td class="title" nowrap><?= $sorter->getTableHead(7) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(8) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(9) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(10) ?></td>
                      <td class="title" nowrap><?= $sorter->getTableHead(11) ?></td>
                    </tr>

					<?
					$i=0;
					$database->executeQuery("select university_name, cntry_name, university_address, university_city, state_name, university_zip, Principal_Researcher_Name, Principal_Researcher_Email, Principal_Researcher_Phone, dept_name from tblucsb left outer join tbluniversities on tblucsb.university_id=tbluniversities.university_id  left outer join tblcountries on tbluniversities.cntry_id=tblcountries.cntry_id left outer join tblstates on tbluniversities.state_id=tblstates.state_id left outer join tbldepartments on tblucsb.dept_id=tbldepartments.dept_id where 1 ".getSearchCriteria($searchingFields).$sorter->getSortingOrder());
					$database->resetRecords();
					if ($database->isRecordFound()) {
					
					while ($database->getNextRecord()) {
					$i++;
					?>
                    <tr> 
                      <td id="firstcolumn" class="DataTD" nowrap><font class="DataFONT"><?=$i?>&nbsp;</font></td>
                      <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("university_name") ?></font>&nbsp;</td>
					  <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("cntry_name") ?></font>&nbsp;</td>
                      <td class="DataTD" nowrap width=300><font class="DataFONT"><?= $database->getColumn("university_address") ?></font>&nbsp;</td>
                      <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("university_city") ?></font>&nbsp;</td>
                      <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("state_name") ?></font>&nbsp;</td>
                      <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("university_zip") ?></font>&nbsp;</td>
					  <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("Principal_Researcher_Name") ?></font>&nbsp;</td>
                      <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("Principal_Researcher_Email") ?></font>&nbsp;</td>
					  <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("Principal_Researcher_Phone") ?></font>&nbsp;</td>
					  <td class="DataTD" nowrap><font class="DataFONT"><?= $database->getColumn("dept_name") ?></font>&nbsp;</td>
					  
                    </tr>
					<? } 
						} else {?>					

						<tr><td colspan="17" class="DataTD"><font class="DataFONT">No Grants Found.</font></td></tr>						
					<?	} ?>
					</table>
</td>
<td width="10"><img src="images/trans.gif" width="10" height="1"></td>				
</tr>
</table>

</body>
</html>

<?

	function getSearchCriteria($searchFields)
	{
		$criteria = "";
		// Keyword Search
		if (isset($_REQUEST["keywords"])) {
			// Build Fields Dynamically
			$fieldsNames = "";
			if ($_REQUEST["searchtype"] == "all") {
			   for ($i=0; $i<count($searchFields); $i+=2) {
					$fieldsNames.=$searchFields[$i].",";
			   }
			   $fieldsNames=substr($fieldsNames, 0, strlen($fieldsNames)-1);
			}
			else {
				$fieldsNames=$_REQUEST["searchtype"];
			}
			// Search Keywords Code
			$fields = split(",", $fieldsNames);
			$tokens = split(" ", $_REQUEST["keywords"]);
			$criteria .= " and (";
			for ($i=0; $i<count($tokens); $i++) {
				if (trim($tokens[$i]) != "") {
					$criteria .= "(";
					for ($j=0; $j<count($fields); $j++) {
						$criteria .= $fields[$j]." like '%".$tokens[$i]."%' or ";
					}
					$criteria=substr($criteria, 0, strlen($criteria)-4);
					$criteria.=") or ";
				}
			}
			$criteria=substr($criteria, 0, strlen($criteria)-4);
			$criteria.=")";
		}
		return $criteria;
	}

	function getSearchParams()
	{
		$searchParams="";
		if (isset($_REQUEST["keywords"]))
		{
			$searchParams.="&keywords=".rawurlencode($_REQUEST["keywords"]);
		}
		if (isset($_REQUEST["searchtype"]))
		{
			$searchParams.="&searchtype=".$_REQUEST["searchtype"];
		}
		return $searchParams;
	}

?>
<? include "includes/destroy.php" ?>





