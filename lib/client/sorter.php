<?php
	class Sorter {

		var $fields;
		var $names;
		var $defaultSorting;
		var $defaultOrder;
		var $order;
		var $sorting;
		var $pageUrl;
		function Sorter() {
			$this->fields=array();
			$this->names=array();
			$this->defaultSorting="";
			$this->order=0;
			$this->sorting=0;
			$this->pageUrl="";
		}
		function setNames($names) {
		    $this->names=explode('|', $names);
		}
		function setFields($fields) {
			$this->fields=explode('|', $fields);
		}
		function setDefaultSorting($defaultSorting) {
			$this->defaultSorting=$defaultSorting;
		}
		function setDefaultOrder($defaultOrder) {
			$this->defaultOrder=$defaultOrder;
		}
		function setOrder($order) {
			//0=ascending 1=descending
			$this->order=($order==""?0:$order);
		}
		function setSorting($sorting) {
			$this->sorting=($sorting==""?0:$sorting);
		}
		function setPageUrl($pageUrl) {
			$this->pageUrl=$pageUrl;
		}
		function getOrder() {
			return $this->order;
		}
		function getSorting() {
			return $this->sorting;
		}
		//###################################get Sorting Database Field##############################
		function getSortingOrder() {
			if ($this->sorting==0) {
				$this->sorting=$this->defaultSorting;
				$this->order=$this->defaultOrder;
			}
			return ($this->defaultSorting!=""|| $this->sorting!=0?" order by ":"").($this->sorting==0?$this->defaultSorting:$this->fields[$this->sorting-1]) .($this->order==1 && $this->sorting!=0?" desc":""); 
		}
		//###################################get Sorting Field Status##############################
		function getTableHead($sorting) {
			if ($this->sorting==0) {
				$this->sorting=$this->defaultSorting;
				$this->order=$this->defaultOrder;
			}
			return ($this->sorting==$sorting?"<a  href=\"".$this->pageUrl.(strpos($this->pageUrl,"?")===false?"?":"&")."order=".($this->order==0?"1":"0")."&sorting=".$sorting."\">".$this->names[$sorting-1]."</a>&nbsp;&nbsp;<a href=\"".$this->pageUrl.(strpos($this->pageUrl,"?")===false?"?":"&")."order=".($this->order==0?"1":"0")."&sorting=".$sorting."\"><img src=\"images/".($this->order==0?"sort_up.jpg":"sort_dn.jpg")."\"  hspace=\"0\" vspace=\"0\" align=\"absmiddle\" border=\"0\"></a>":"<a  href=\"".$this->pageUrl.(strpos($this->pageUrl,"?")===false?"?":"&")."order=1&sorting=".$sorting."\">".$this->names[$sorting-1]."</a>&nbsp;&nbsp;<img src=\"images/sort_no.jpg\"  hspace=\"0\" vspace=\"0\" align=\"absmiddle\" border=\"0\">"); 
		}
	}
?>