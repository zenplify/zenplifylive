<?php
class Validation 
{
	function isValidLogin($login)
	{
		$validString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
		if (strlen($login)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($login); $i++)
		{
		 if(strrpos($validString,substr($login,$i,1))===false)
			 return  false;
		}
		return 	true;
	}
	function isValidEmail($email)
	{
		$email = trim($email);
		$validString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@._-";
		if(strpos($email,"@")!=strrpos($email,"@")|| strpos($email,"@")===false ||  strpos($email,"@")==0 ||strpos($email,".")===false  ||strpos($email,".")==0 || strpos($email,"@")+1==strpos($email,".") || strrpos($email,".")==strlen($email)-1)
		{
			return false;
		}
		for ($i=0;$i<strlen($email); $i++)
		{
		  if (strrpos($validString,substr($email,$i,1))===false)
		  	return false;
		}
		return true;
	}
	  	function isValidImage($type)
   	{
		if($type == NULL || trim($type) == "")
			return false;
		$imageTypes=".gif,.jpeg,.jpg,.jpe";
			return strpos($imageTypes ,strtolower($type)) !== false;
   }
	function isEmpty($str)
    {
		return strlen(trim($str)) == 0;
    }
   	function isValidMonth($month)
   	{
   		if (!is_numeric($month))
	 	{
			return false;
		}
		if ((int)$month > 12 || (int)$month <= 0)
	 	{
		 	return false;
		}
		return true;
	}
	function isValidDay($day)		
   	{
		if (!is_numeric($day))
	 	{
			return false;
		}
		if ((int)$day > 31 || (int)$day <= 0)
		{
		 	return false;
		}
		return true;
	}
   	function isValidYear($year)		
   	{
		if (!is_numeric($year))
		{
			return false;
		}
		if ((int)$year < 1800)
		{
		 	return false;
		}
		return true;
	}
 function isNumeric($value)
 {
        $validString="1234567890";
		if (strlen($value)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($value); $i++)
		{
		 if(strrpos($validString,substr($value,$i,1))===false)
			 return  false;
		}
	return 	true;
 }
 function isFloat($value)
 {
    $validString="1234567890.";
	if (strlen($value)==0)
	{
		return false;
	}
	for ( $i=0;$i<strlen($value); $i++)
	{
	 if (strrpos($validString,substr($value,$i,1))===false)
		 return  false;
	}
	if (strpos($value,".")!=strrpos($value,".") || substr($value,0,1)=="." || substr($value,strlen($value)-1,1)==".")
	{
	 return false;
	}
	return 	true;
 }

	 function isValidDate($strdate) { 
		//Check the length of the entered Date value
		if((strlen($strdate)<10) || (strlen($strdate)>10)) { 
			return false;
		}
		//The entered value is checked for proper Date format
		if(substr_count($strdate,"/")!=2) { 
//			echo("Enter the date in 'dd/mm/yyyy' format". substr_count($strdate,"/"));
			return false;
		}
		$pos=strpos($strdate,"/");
		$month=substr($strdate,0,($pos));
		$result=ereg("^[0-9]+$",$month,$trashed);
		if (!$result) { 
			return false;
		}
		if (($month<=0) || ($month>12)) { 

			return false;
		}
		$day=substr($strdate,($pos+1),($pos));
		$result=ereg("^[0-9]+$",$day,$trashed);
		if (!$result){

			return false;
		}
		if (($day<=0) || ($day>31)) { 
			return false;
		}	
		$year = substr($strdate,($pos+4),strlen($strdate));
		$result = ereg("^[0-9]+$",$year,$trashed);
		if (!$result){
			return false;
		}
		if (($year<1900) || ($year>2200)) { 
			return false;
		}
		switch($month) { 
			case 2 :
				$isleap = ($year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0) ? true : false);
				if ($isleap && $day > 29) {
					return false;
				}
				if (!$isleap && $day > 28) {
					return false;
				}
			break;
			//** only maximum of thirty days in these months.
			case 4 : //** APRIL
			case 6 : //** JUNE
			case 9 : //** SEPTEMBER
			case 11 : //** NOVEMBER
			if ($day > 30) {
				return false;
			}
			break;
		}
		return true;
	} 
}
?>
