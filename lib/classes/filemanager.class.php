<?php
class FileManager
{
	var $folderStore;
//--------------- Functions Related to File Upload ---------------//
	function setFolderStore($folderStore) {
		$this->folderStore=$folderStore;
	}
    function isFileMissing($name) {
         if (($_FILES[$name]['tmp_name']!='none') && ($_FILES[$name]['name']!='')) {
		 	return false;
		 }
		 return true;
    }
	
    function getFileName($name) {
        return $_FILES[$name]['name'];
    }
    function getFileExtension($name) {
		return substr($_FILES[$name]['name'], strrpos($_FILES[$name]['name'], "." ));
    }
    function getFileSize($name) {
        return $_FILES[$name]['size'];
    }
	function upload($name) {
		return move_uploaded_file($_FILES[$name]['tmp_name'], $this->folderStore.$_FILES[$name]['name']);
	}
	function uploadAs($name, $newName) {
		return move_uploaded_file($_FILES[$name]['tmp_name'], $this->folderStore.$newName);
	}
	function uploadAll($name, $newName) {
		return move_uploaded_file($name, $this->folderStore.$newName);
	}
	function getFileContents($path) {
		if(is_file($path))
			return file_get_contents($path);
		else
			return "";
	}
	//--------------- Other File Management Functions ---------------//
	function makeDirectory($path) {
		return mkdir($path);
	}
	function deleteFile($path) {
		return unlink($path);
	}
	function createFile($path, $data) {
		$fp = fopen($path, "w");
		fwrite($fp, $data);
		fclose($fp);
	}
    function copyFile($source, $dest) {
        return copy($source, $dest);
    }
    function getUploadedFileSize($name) {
		$size = filesize($name);
		return ($size==""?0:$size);
    }
	function replaceFolderName($folderName, $folderId) {
		return str_replace(" ", "_", $folderName)."_".$folderId;
	}
	function getFolderPath($folder) {
		global $database;
		$docsPath=$GLOBALS["DOCS_PATH"];
		$pathSeparator=$GLOBALS["PATH_SEPERATOR"];
		$database->executeQuery("select tblfolders.folder_id as folder_id,tblfolders.folder_name as folder_name, parent.folder_id as parent_id, parent.folder_name as parent_name from tblfolders left outer join tblfolders as parent on tblfolders.folder_parent_id=parent.folder_id  where tblfolders.folder_id=".$folder);
		if ($database->getNextRecord()) {
			return $docsPath.($database->getColumn("parent_id")!=""?$this->replaceFolderName($database->getColumn("parent_name"),$database->getColumn("parent_id")).$pathSeparator:"").$this->replaceFolderName($database->getColumn("folder_name"),$database->getColumn("folder_id")).$pathSeparator;
		}
		return "null";
	}
	function ImageID()
		{
	//		$random_id_length = 5;
			//generate a random id encrypt it and store it in $rnd_id
	//		$rnd_id = crypt(uniqid(rand(),1));
			//to remove any slashes that might have come
	//		$rnd_id = strip_tags(stripslashes($rnd_id));
			//Removing any . or / and reversing the string
	//		$rnd_id = str_replace(".","",$rnd_id);
	//		$rnd_id = strrev(str_replace("/","",$rnd_id));
			//finally I take the first 10 characters from the $rnd_id 
	//		$rnd_id = substr($rnd_id,0,$random_id_length);
			return date('dmYhis');
	}
    function saveOutputImage($source, $destination, $newWidth, $newHeight, $scaleType=2) {
		$quality = 80;
        /* Check for the image's exisitance */ 
        if (!file_exists($source)) { 
            echo 'File does not exist!'; 
        }
        else {
            $size = getimagesize($source); // Get the image dimensions and mime type
			// Get scale factor
			if ($scaleType==1) { // Intelli
				$scale = ($size[1]>$size[0]?($size[1]<$newHeight?1.0:($newHeight/(float)$size[1])):($size[0]<$newWidth?1.0:($newWidth/(float)$size[0])));
			} else if ($scaleType==2) {// Width wise
				$scale = ($size[0]<$newWidth?1.0:($newWidth/(float)$size[0]));
			} else if ($scaleType==3) { // Height wise
				$scale = ($size[1]<$newHeight?1.0:($newHeight/(float)$size[1]));
			}
            $w = (integer)($size[0]*$scale); // Width divided 
            $h = (integer)($size[1]*$scale); // Height divided 
            $resize = imagecreatetruecolor($w, $h); // Create a blank image 
            /* Check quality option. If quality is greater than 100, return error */ 
            if ($quality > 100) {
                echo 'The maximum quality is 100. Quality changes only affect JPEG images.';
            }
            else {
                header('Content-Type: '.$size['mime']); // Set the mime type for the image 
                switch ($size['mime']) {
                    case 'image/jpeg':
						$im = imagecreatefromjpeg($source);
						imagecopyresampled($resize, $im, 0, 0, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
						imagejpeg($resize, $destination, $quality); // Output the new JPEG
						break;
                    case 'image/gif':
						$im = imagecreatefromgif($source);
						imagecopyresampled($resize, $im, 0, 0, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original GIF 
						imagegif($resize, $destination, $quality); // Output the new GIF
						break;
                    case 'image/png':
						$im = imagecreatefrompng($source); 
						imagecopyresampled($resize, $im, 0, 0, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original PNG 
						imagepng($resize, $destination, $quality); // Output the new PNG 
						break;
                }
                imagedestroy($im);
            } // end of if - else
        } // end of if-else of file exits
    } // end of fuction saveoutputimage()
}
?>