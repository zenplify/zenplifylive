<?php
class HtmlControls {

	var $pagingLinksWidth;
	var $pagingLinksAlignment;
	var $formPanelWidth = 931;	
	var $formPanelWidth1 = 333;	
	var $setPanelWidth = 933;	
	var $setPanelWidth1 = 933;	
	var $setPanelWidth2 = 333;	

	// Writes Sub Title 
	function getPageTitle($title, $width="695", $title2='') {
	return '<table width="730" cellpadding="0" cellspacing="0" border="0" align="left">
			<tr>
				<td width="5" align="left"><img src="images/trans.gif"></td>
				<td width="19" align="left"><img src="images1/aro.png"></td>
				<td nowrap width="1%" class="title" align="left"> 
					&nbsp;'.$title.'&nbsp;
				</td>
				<td width="99%" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr><td height="24"><img src="images/trans.gif"></td></tr>
					<tr><td height="1" bgcolor="#D1D0D0"><img src="images/trans.gif"></td></tr>
					</table>
				</td>
			</tr>
			</table>';
	}
	function getHeading($title, $width="695", $title2='') {
	return '<table width="700" cellpadding="0" cellspacing="0" border="0" align="left">
			<tr>
				<td width="19" align="left"><img src="images1/aro.png"></td>
				<td nowrap class="title1" align="left"> 
					&nbsp;'.$title.'&nbsp;
				</td>
			</tr>
			</table>';
	}
	function getListing($title, $width="695", $title2='') {
	return '<table width="739" cellpadding="0" cellspacing="0" border="0" align="left">
			<tr>
				<td width="11" height="40" align="left"><img src="images1/listing_bar1.png"></td>
				<td nowrap style="background-color:#CC2026; height="40px;" class="listingtitle" align="left"> 
					&nbsp;'.$title.'&nbsp;
				</td>
				<td width="11" height="40" align="left"><img src="images1/listing_bar2.png"></td>
			</tr>
			</table>';
	}
	
	function getPanelHeader(){
	return '<table width="100%" cellpadding="0" cellspacing="0" border="0">
           <tr><td  height="1"><img src="images/trans.gif"></td></tr>
           <tr><td  height="485"><img src="images/trans.gif" height="15"><br />';
	}
	function getPanelFooter(){
		return'<img src="images/trans.gif" height="10"><br /></td></tr>
           <tr><td  height="1"><img src="images/trans.gif"></td></tr>
           </table>';
	}

	function getHeading1($title, $width="100%") {
	return '<table width="903" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td background="images/heading.jpg" height="61" style="background-color:no-repeat" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;'.$title.'</td>
    </tr>
    </table>
	<img src="images/trans.gif" height="1" /><br />
	';
	}
	function getHeading2($title, $width="100%") {
	return '<table width="333" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td bgcolor="#CCCCCC"  height="33" style="background-repeat:no-repeat" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;'.$title.'</td>
    </tr>
    </table>
	<img src="images/trans.gif" height="1" /><br />
	';
	}
	
	function getHeading3($title, $width="100%") {
	return '<table width="90%" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td bgcolor="#CCCCCC"  height="33" style="background-repeat:no-repeat" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;'.$title.'</td>
    </tr>
    </table>
	<img src="images/trans.gif" height="1" /><br />
	';
	}

	function setFormPanelWidth($width) { 
		$this->formPanelWidth = $width;
	}

	function getFormPanelHeader($title = "") { 
		return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
			  <tr>

				<td id="frm-heading" class="frm-heading" background="images/frm-hd-top.jpg" style="background-repeat:no-repeat;background-position:right">&nbsp;</td>
			  </tr>
			</table>   
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
			  <tr>
				<td background="images/frm-pnl-bg.jpg" style="background-repeat:repeat-y;background-position:right">
				<img src="images/trans.gif" width="1" height="10" /><br />';
	}
	
	function getFormPanelFooter($contents = "") { 
		return '    <img src="images/trans.gif" width="1" height="10" /><br />
				   </td>
				  </tr>
				</table>   
				<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
				  <tr>
					<td class="frm-pnl-ftr" align="right">'.$contents.'&nbsp;</td>
				  </tr>
				</table>';
	}
	
	
	function setFormPanelWidth1($width) { 
		$this->formPanelWidth1 = $width;
	}

	function getFormPanelHeader1($title = "") { 
		return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth1.'">
			  <tr>

				<td id="frm-heading" class="frm-heading" background="images/frm-hd-top.jpg" style="background-repeat:no-repeat;background-position:right">&nbsp;</td>
			  </tr>
			</table>   
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth1.'">
			  <tr>
				<td background="images/frm-pnl-bg.jpg" style="background-repeat:repeat-y;background-position:right">
				<img src="images/trans.gif" width="1" height="10" /><br />';
	}
	
	function getFormPanelFooter1($contents = "") { 
		return '    <img src="images/trans.gif" width="1" height="10" /><br />
				   </td>
				  </tr>
				</table>   
				<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth1.'">
				  <tr>
					<td class="frm-pnl-ftr" align="right">'.$contents.'&nbsp;</td>
				  </tr>
				</table>';
	}
	//////
	//////
	function getFixedPanelHeader($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\" >";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"15\"><br>";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td  bgcolor=\"#000000\" height=\"33\" class=\"h3\"><font>&nbsp;&nbsp;$title</font></td>";
		$contents .=  "<td bgcolor=\"#000000\">$secontTitle</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"10\"><br>";
		return $contents;
	}
	function getFixedPanelFooter($footer="", $panelType=2)
	{
		$contents="";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		
		return $contents;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////
	function setPanelWidth($width) { 
		$this->setPanelWidth = $width;
	}

	function setPanelHeader($title = "") { 
	return '<table width="'.$this->setPanelWidth.'" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td background="images/images/uper_part_gray.jpg" style="background-repeat:no-repeat" height="55">
		<img src="images/images/trans.gif" height="8" /><br />
        	<table width="'.$this->setPanelWidth.'" cellpadding="0" cellspacing="0" border="0" align="center">
            <tr>
            	<td width="20"><img src="images/images/trans.gif" /></td>
				<td width="193" align="center" height="47" background="images/images/events_tab.jpg" style="background-repeat:no-repeat" class="style3">'.$title.'</td>
				<td width="720"><img src="images/images/trans.gif" /></td>
            </tr>
            </table>
        </td>
    </tr>
    </table>
    <table width="'.$this->setPanelWidth.'" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td><img src="images/images/upper_part_white.jpg" /></td>
    </tr>
	
	
	<tr>
	<td bgcolor="#FFFFFF" valign="top"  background="images/images/middle_par_white.jpg" style="background-repeat:repeat-y">';
	}

	function setPanelFooter($contents = "") { 
		return '</td></tr></table>
				<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->setPanelWidth.'">
				  <tr>
					<td><img src="images/images/lower_par_white.jpg" /></td>
				  </tr>
				</table>';
	}
	
	function setPanelWidth1($width) { 
		$this->setPanelWidth1 = $width;
	}
	function setPanelHeader1($title = "", $title2="") { 
	return '<table width="'.$this->setPanelWidth1.'" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td width="5" bgcolor="#BBBBBB" valign="top"><img src="images/images/tabcor1.png" /></td>
        <td  width="'.(($this->setPanelWidth1)-10).'" valign="top"  >
            <table width="'.(($this->setPanelWidth1)-10).'" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#BBBBBB">
              <tr>
                  <td width="7" bgcolor="#BBBBBB"  nowrap="nowrap" valign="bottom"><img src="images/images/cor1.png" /></td>
                  
                  <td width="1%" nowrap="nowrap" valign="top">
                  		<img src="images/trans.gif" height="8" /><br />
						'.($title2!=""?$title2:
                    	'<table width="100%" cellpadding="0" cellspacing="0" border="0">
                    	<tr>
                        	<td width="7" nowrap="nowrap"><img src="images/images/left_side_tab.jpg" /></td>
                            <td nowrap="nowrap" background="images/images/middle_side_tab.jpg" height="47" style="background-repeat:repeat-x;" class="style3">&nbsp;&nbsp;&nbsp;'.$title.'&nbsp;&nbsp;&nbsp;</td>
                            <td width="7" nowrap="nowrap"><img src="images/images/right_side_tab.jpg" /></td>
                        </tr>
                        <tr>
                        	<td colspan="3" bgcolor="#FFFFFF" height="7" nowrap="nowrap"><img src="images/images/trans.gif" /></td>
                        </tr>
                        </table>').
                  '</td>
                  <td width="99%" valign="top">
                  		<img src="images/trans.gif" height="55" /><br />
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td height="1" bgcolor="#FFCC00"><img src="images/images/trans.gif" height="1" /></td>
                        </tr>
                        <tr>
                            <td height="6" bgcolor="#ffffff"><img src="images/images/trans.gif" height="6" /></td>
                        </tr>
                        </table>
                  </td>
                  <td width="7" nowrap="nowrap"  valign="bottom"><img src="images/images/cor2.png" /></td>
              </tr>
              </table>
        </td>
        <td width="5" bgcolor="#BBBBBB" valign="top"><img src="images/images/tabcor2.png" /></td>
    </tr>
    </table>
    <table width="'.($this->setPanelWidth1).'" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td width="6" background="images/images/line1.png" style="background-repeat:repeat-y;"><img src="images/images/trans.gif" /></td>
        <td width="5"><img src="images/images/trans.gif" /></td>
        <td width="'.(($this->setPanelWidth1)-22).'" height="333" valign="top">';
	}
	function setPanelFooter1($contents = "") { 
		return '</td>
        <td width="5"><img src="images/images/trans.gif" /></td>
        <td width="6" background="images/images/line2.png" style="background-repeat:repeat-y;"><img src="images/images/trans.gif" /></td>
    </tr>
    </table>
    <table width="'.$this->setPanelWidth1.'" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
    	<td width="11" nowrap="nowrap"  valign="bottom"><img src="images/images/cor3.png" /></td>
        <td height="13" valign="top" background="images/images/cor5.png" style="background-repeat:repeat-x"><img src="images/images/trans.gif" /></td>
        <td width="11" nowrap="nowrap"  valign="bottom"><img src="images/images/cor4.png" /></td>
    </tr>
    </table>';
	}

	function getNavigation() {

		$pageName = substr($_SERVER["PHP_SELF"],strrpos($_SERVER["PHP_SELF"],"/")+1);	
		$page =substr($pageName, 0, strrpos($pageName, "."));

		$contents = '<font class="smallText">You are at:</font> ';
		if ($page=="scheduler") {
			$contents .= 'Home';
		}
		else if ($page=="addmeeting") {
			$contents .= '<a href="scheduler.php">Home</a> - Add Meeting';
		}
		else if ($page=="editmeeting") {
			$contents .= '<a href="scheduler.php">Home</a> - Edit Meetings';
		}
		else if ($page=="meetingdetail") {
			$contents .= '<a href="scheduler.php">Home</a> - Meeting Detail';
		}
		else if ($page=="searchrooms") {
			$contents .= '<a href="scheduler.php">Home</a> - Search Rooms';
		}
		else if ($page=="searchmeetings") {
			$contents .= '<a href="scheduler.php">Home</a> - Search Meetings';
		}
		else if ($page=="confirm") {
			$contents .= '<a href="scheduler.php">Home</a> - Confirm Meetings';
		}
		else if ($page=="reports") {
			$contents .= '<a href="scheduler.php">Home</a> - Reports';
		}
		// ADMINISTRATION LINKS
		else if ($page=="admin") {
			$contents .= '<a href="scheduler.php">Home</a> - Administration';
		}
		else if ($page=="rooms") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Rooms';
		}
		else if ($page=="addroom") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="rooms.php">Rooms</a> - Add Room';
		}
		else if ($page=="editroom") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="rooms.php">Rooms</a> - Edit Room';
		}
		else if ($page=="addressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Address Book';
		}
		else if ($page=="addaddressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="addressbook.php">Address Book</a> - Add Address Book';
		}
		else if ($page=="editaddressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="addressbook.php">Address Book</a> - Edit Address Book';
		}
		else if ($page=="roomtypes") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Rooom Types';
		}
		else if ($page=="configurations") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Configurations';
		}
		else if ($page=="formbuilder") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Form Builder';
		}
		else if ($page=="addfield") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="formbuilder.php?Cat='.($Cat_ID==""?"1":$Cat_ID).'">Form Builder</a> - Add Field';
		}
		else if ($page=="editfield") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="formbuilder.php?Cat='.($Cat_ID==""?"1":$Cat_ID).'">Form Builder</a> - Edit Field';
		}
		else if ($page=="locations") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Locations';
		}
		else if ($page=="country") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="locations.php">Locations</a> - Countries';
		}
		else if ($page=="states") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="locations.php">Locations</a> - States';
		}
		else if ($page=="usergroups") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Security Groups';
		}
		else if ($page=="users") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Users';
		}
		else if ($page=="adduser") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="users.php">Users</a> - Add User';
		}
		else if ($page=="edituser") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="users.php">Users</a> - Edit User';
		}
		else if ($page=="import") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Import';
		}
		else if ($page=="form") {
			$contents .= '<a href="form.php">Request Form</a> - Form';
		} else {
			return "";
		}
		return $contents;
	}

	function getSimplePanel($width='661', $msg='') {

	   	return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$width.'">
				  <tr>
					<td class="DataTD3"><br>
					<table align="center" cellpadding="4" cellspacing="1" border="0" width="80%">
					  <tr>
						<td align="center" class="normalText">'.$msg.'</td>
					  </tr>
					</table><br>
					</td>
				  </tr>
				</table>';
	}

	function setPagingLinksWidth($pagingLinksWidth) {
		$this->pagingLinksWidth=$pagingLinksWidth;
	}

	function showPagingLinks($database) {
		return $database->isRecordFound()?"<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"".$this->panelAlignment."\"><tr><td height=\"20\" valign=\"bottom\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"right\" border=\"0\"><tr><td>" . $database->getBackLink("<img src=\"images/back.gif\" border=\"0\">") . "&nbsp;</td><td class=\"links2\">" . $database->getPagesLinks() . "&nbsp;</td><td>" . $database->getNextLink("<img src=\"images/next.gif\" border=\"0\">") . "</td></tr></table></td></tr><tr><td class=\"links2\" align=\"right\">showing  " . $database->getStartingRecordsNo() . " - " . $database->getEndingRecordsNo() . " of " . $database->getTotalRecords() . "</td></tr></table>" : "";
    }
	
	////////////data style////////////////////
	function setDataStyle($dataStyle) {
        $this->dataStyle = $dataStyle;
    }
   	function resetStyle() {
        $this->currentDataStyle = "";
    }
    function getDataStyle() {
        if (strlen($this->currentDataStyle) == 0)
            $this->getNextDataStyle();
        return $this->currentDataStyle;
    }
    function getNextDataStyle() {
        if (strlen($this->currentDataStyle) == 0) {
			$this->currentDataStyle = "DataTD1";
        }
		else if(strcmp($this->currentDataStyle, "DataTD1") == 0) {
			$this->currentDataStyle = "DataTD2";
		}
		else {
			$this->currentDataStyle = "DataTD1";
		}
    }

}
