<?php
class HtmlControls {

	var $pagingLinksWidth;
	var $pagingLinksAlignment;
	var $panelWidth = 931;
	var $formPanelWidth = 931;

	// Writes Sub Title 
	function getPageTitle($title, $width=931, $nav='&nbsp;') { 
		return '<table width="'.$width.'" cellpadding="0" cellspacing="0" align="center" border="0">
		  <tr>
			<td width="29"><img src="images/hd-blt.jpg" /></td>
			<td bgcolor="#E4E4E4"  class="page-title">&nbsp;'.$title.'</td>
			<td background="images/hd-right.jpg" width="481" class="navigation">'.$nav.'</td>
		  </tr>
		</table>
		<img src="images/trans.gif" width="1" height="10"><br />';
	}

	function setFormPanelWidth($width) { 
		$this->formPanelWidth = $width;
	}
	
	function getFormPanelHeader($title) { 
		return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
			  <tr>
				<td width="35"><img src="images/frm-hd-blt.jpg" /></td>
				<td class="frm-heading" id="frm-heading" background="images/frm-hd-top.jpg" style="background-repeat:no-repeat;background-position:right">'.$title.'</td>
			  </tr>
			</table>   
			<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
			  <tr>
				<td background="images/frm-pnl-bg.jpg" style="background-repeat:repeat-y;background-position:right">
				<img src="images/trans.gif" width="1" height="10" /><br />';
	}
	
	function getFormPanelFooter($contents) { 
		return '    <img src="images/trans.gif" width="1" height="10" /><br />
				   </td>
				  </tr>
				</table>   
				<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->formPanelWidth.'">
				  <tr>
					<td class="frm-pnl-ftr" align="right">'.$contents.'&nbsp;</td>
				  </tr>
				</table>';
	}

	function setPanelWidth($width) { 
		$this->panelWidth = $width;
	}

	function getPanelHeader($title, $title2='') { 
	
		return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->panelWidth.'" align="center">
		  <tr>
			<td width="6"><img src="images/cor01.gif"></td>
			<td bgcolor="#DDEBFD"><img src="images/trans.gif"></td>
			<td width="6"><img src="images/cor02.gif"></td>
		  </tr>
		  <tr>
			<td height="15" colspan="3" bgcolor="#DDEBFD"><img src="images/trans.gif"></td>
		  </tr>
		</table>
		<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->panelWidth.'" align="center">
		  <tr>
			<td width="" background="images/pnl-hd-bg.jpg">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
			  <tr>
				<td align="center" width="50" height="43"><img src="images/pnl-hd-blt.jpg"></td>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
				  <tr>
					<td class="pnl-header">'.$title.'</td>
					<td class="pnl-header" align="right">'.$title2.'&nbsp;</td>
				  </tr>
				</table>
				</td>
			  </tr>
			</table>
			</td>
		  </tr>
		</table>
		<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$this->panelWidth.'" align="center">
		  <tr>
			<td valign="top">';
	}
	function getPanelFooter($show_panel=false) { 
	
		return 
		($show_panel?'<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td height="28" bgcolor="#6F6F70">&nbsp;</td></tr></table>':''
		).'
			</td>
		  </tr>
		</table>';
	}

	function getNavigation() {

		$pageName = substr($_SERVER["PHP_SELF"],strrpos($_SERVER["PHP_SELF"],"/")+1);	
		$page =substr($pageName, 0, strrpos($pageName, "."));

		$contents = '<font class="smallText">You are at:</font> ';
		if ($page=="scheduler") {
			$contents .= 'Home';
		}
		else if ($page=="addmeeting") {
			$contents .= '<a href="scheduler.php">Home</a> - Add Meeting';
		}
		else if ($page=="editmeeting") {
			$contents .= '<a href="scheduler.php">Home</a> - Edit Meetings';
		}
		else if ($page=="meetingdetail") {
			$contents .= '<a href="scheduler.php">Home</a> - Meeting Detail';
		}
		else if ($page=="searchrooms") {
			$contents .= '<a href="scheduler.php">Home</a> - Search Rooms';
		}
		else if ($page=="searchmeetings") {
			$contents .= '<a href="scheduler.php">Home</a> - Search Meetings';
		}
		else if ($page=="confirm") {
			$contents .= '<a href="scheduler.php">Home</a> - Confirm Meetings';
		}
		else if ($page=="reports") {
			$contents .= '<a href="scheduler.php">Home</a> - Reports';
		}
		// ADMINISTRATION LINKS
		else if ($page=="admin") {
			$contents .= '<a href="scheduler.php">Home</a> - Administration';
		}
		else if ($page=="rooms") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Rooms';
		}
		else if ($page=="addroom") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="rooms.php">Rooms</a> - Add Room';
		}
		else if ($page=="editroom") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="rooms.php">Rooms</a> - Edit Room';
		}
		else if ($page=="addressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Address Book';
		}
		else if ($page=="addaddressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="addressbook.php">Address Book</a> - Add Address Book';
		}
		else if ($page=="editaddressbook") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="addressbook.php">Address Book</a> - Edit Address Book';
		}
		else if ($page=="roomtypes") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Rooom Types';
		}
		else if ($page=="configurations") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Configurations';
		}
		else if ($page=="formbuilder") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - Form Builder';
		}
		else if ($page=="addfield") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="formbuilder.php?Cat='.($Cat_ID==""?"1":$Cat_ID).'">Form Builder</a> - Add Field';
		}
		else if ($page=="editfield") {
			$contents .= '<a href="scheduler.php">Home</a> - <a href="admin.php">Administration</a> - <a href="formbuilder.php?Cat='.($Cat_ID==""?"1":$Cat_ID).'">Form Builder</a> - Edit Field';
		}
		else if ($page=="locations") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Locations';
		}
		else if ($page=="country") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="locations.php">Locations</a> - Countries';
		}
		else if ($page=="states") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="locations.php">Locations</a> - States';
		}
		else if ($page=="usergroups") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Security Groups';
		}
		else if ($page=="users") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Users';
		}
		else if ($page=="adduser") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="users.php">Users</a> - Add User';
		}
		else if ($page=="edituser") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - <a href="users.php">Users</a> - Edit User';
		}
		else if ($page=="import") {
			$contents .= '<a href="index.php">Home</a> - <a href="admin.php">Administration</a> - Import';
		}
		else if ($page=="form") {
			$contents .= '<a href="eventslist.php">Events List</a> - Form';
		} else {
			return "";
		}
		return $contents;
	}

	function getSimplePanel($width='661', $msg='') {

	   	return '<table align="center" cellpadding="0" cellspacing="0" border="0" width="'.$width.'">
				  <tr>
					<td class="DataTD3"><br>
					<table align="center" cellpadding="4" cellspacing="1" border="0" width="80%">
					  <tr>
						<td align="center" class="normalText">'.$msg.'</td>
					  </tr>
					</table><br>
					</td>
				  </tr>
				</table>';
	}

	function setPagingLinksWidth($pagingLinksWidth) {
		$this->pagingLinksWidth=$pagingLinksWidth;
	}

	function showPagingLinks($database) {
		return $database->isRecordFound()?"<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" align=\"".$this->panelAlignment."\"><tr><td height=\"20\" valign=\"bottom\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"right\" border=\"0\"><tr><td>" . $database->getBackLink("<img src=\"images/back.gif\" border=\"0\">") . "&nbsp;</td><td class=\"links2\">" . $database->getPagesLinks() . "&nbsp;</td><td>" . $database->getNextLink("<img src=\"images/next.gif\" border=\"0\">") . "</td></tr></table></td></tr><tr><td class=\"links2\" align=\"right\">showing  " . $database->getStartingRecordsNo() . " - " . $database->getEndingRecordsNo() . " of " . $database->getTotalRecords() . "</td></tr></table>" : "";
    }

}
