<?php
class HtmlControls
{
	var $panelWidth;
	var $panelType=1;
	var $pagingLinksWidth;
    var $dataStyle;
    var $errorDataStyle;
	var $panelAlignment;
    var $currentDataStyle;
	var $FpanelWidth;

	function HtmlControls() {
		$this->panelWidth=650;
		$this->pagingLinksWidth=740;
		$this->dataStyle = "tbldata1,tbldata2";
		$this->errorDataStyle = "tblerrordata";
		$this->panelAlignment="center";
		$this->currentDataStyle = "";
		$this->FpanelWidth=250;
	}
	
	function setPanelWidth($panelWidth)
	{
		$this->panelWidth=$panelWidth;
	}

	function setListPanelWidth($listPanelWidth)
	{
		$this->listPanelWidth = $listPanelWidth;
	}
	function setFxdPanelWidth($FpanelWidth)
	{
		$this->FpanelWidth=$FpanelWidth;
	}
/*

	function setPanelWidth($panelWidth) {
		$this->panelWidth=$panelWidth;
	}
*/
	function Title($title, $width=750) {
	return '<table width="750" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2" align="right"><img src="../images/title_img2.jpg" /></td>
	</tr>
	<tr>
		<td width="75"><img src="../images/title_img2_line1.jpg" /></td>
		<td height="1" width="675" bgcolor="#564825"><img src="images/trans.gif" /></td>
	</tr>
	<tr>
		<td colspan="2" height="2"><img src="../images/trans.gif" /></td>
	</tr>
	<tr>
		<td colspan="2" align="right" class="h3">'.$title.'&nbsp;&nbsp;&nbsp;</td>
	</tr>
	</table>';
	}
	
	function getPageTitle($title, $width=711) {
	return '<table width="711" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="8"><img src="../images/trans.gif" /></td>
		<td width="20" align="center"><img src="../images1/aro.png" /></td>
		<td width="5"><img src="../images/trans.gif" /></td>
		<td class="h1"><img src="../images/trans.gif" height="1" /><br /><font color="#990000">'.$title.'</font></td>
	</tr>
	</table>';
	}
	function getFormTitle($title, $width=711) {
	return '<table width="615" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tblhd" height="33">&nbsp;&nbsp;&nbsp;'.$title.'</td>
	</tr>
	</table>';
	}
	function getErrorDataStyle() {
		return $this->errorDataStyle;
	}

	function setPanelAlignment($panelAlignment) {
		$this->panelAlignment = $panelAlignment;
	}

	function getFixedPanelHeader($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\" >";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"15\"><br>";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td  bgcolor=\"#EEEDEE\" height=\"33\" class=\"tblhd\"><font>&nbsp;&nbsp;$title</font></td>";
		$contents .=  "<td bgcolor=\"#EEEDEE\">$secontTitle</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"10\"><br>";
		return $contents;
	}


	function getFixedPanelFooter($footer="", $panelType=2)
	{
		$contents="";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		
		return $contents;
	}
	//panel footer
	function getFixedPanelHead1($firstTitle, $secondTitle="")
    {
        $contents = "";
        $contents.=("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"330\" height=\"26\" bgcolor=\"#000000\" align=\"center\">");
        $contents.=("<tr>");
        $contents.=("<td width=\"35\" align=\"center\" class=\"panelhd".$this->panelType."\"><img src=\"../images/bulletwt1.gif\"></td>");
        $contents.=("<td width=\"300\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" height=\"26\">");
        $contents.=("<tr>");
        $contents.=("<td class=\"panelhd".$this->panelType."\"><font color=\"#FFFFFF\">".$firstTitle."</font></td>");
        $contents.=("<td class=\"panelhd".$this->panelType."\" align=\"right\"><font color=\"#FFFFFF\">".$secondTitle."</font>&nbsp;&nbsp;</td>");
        $contents.=("</tr>");
        $contents.=("</table></td>");
        $contents.=("</tr>");
        $contents.=("</table>");
        $contents.=("<img src=\"../images/trans.gif\" height=\"4\" width=\"1\"><br>");
        $contents.=("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"330\" class=\"panelline".$this->panelType."\" align=\"center\">");
        $contents.=("<tr>");
        $contents.=("<td><table cellpadding=\"0\" cellspacing=\"1\" border=\"0\" width=\"330\" class=\"panelline".$this->panelType."\" align=\"center\">");
        $contents.=("<tr>");
        $contents.=("<td bgcolor=\"#ffffff\" align=\"center\">");
        return $contents;
    }
	function getFixedPanelFooter1($footer="")
    {
        $contents = "";
        $contents.=("</td>");
        $contents.=("</tr>");
        $contents.=("</table></td>");
        $contents.=("</tr></table>");
        $contents.=("<img src=\"../images/trans.gif\" height=\"1\" width=\"1\"><br>");
        $contents.=("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"330\" align=\"center\">");
        $contents.=("<tr>");
        $contents.=("<td height=\"26\" class=\"panelbot".$this->panelType."\">".$footer."</td>");
        $contents.=("</tr>");
        $contents.=("</table>");	   
        return $contents;
    }

	function getPanelHeader($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td class=\"h1\" style=\"padding-left:5px;\">".$title."</td>";
		$contents .=  "</tr>";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\">";

		
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"10\"><br>";
		return $contents;
	}

	//panel footer
	function getPanelFooter($footer="", $panelType=2)
	{
		$contents =  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		return $contents;
	}


	function getFixedPanel($title="", $panelType=1, $secontTitle="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->FpanelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\" >";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"15\"><br>";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->FpanelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td  bgcolor=\"#EEEDEE\" height=\"33\" class=\"tblhd\"><font>&nbsp;&nbsp;$title</font></td>";
		$contents .=  "<td bgcolor=\"#EEEDEE\">$secontTitle</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";

		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"10\"><br>";
		return $contents;
	}


	function getFixedPanelFtr($footer="", $panelType=2)
	{
		$contents="";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		
		return $contents;
	}

	// rigth aligned buttons 
	function showButtons($buttons) {
	    $contents  = "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" align=\"center\">";
	    $contents .= "<tr>";
	    $contents .= "<td align=\"right\">" . $buttons . "</td>";
	    $contents .= "</tr>";
	    $contents .= "</table>";
	    $contents .= "<img src=\"". $GLOBALS["IMAGES_URL"] ."trans.gif\" width=\"1\" height=\"5\"><br>";
	    return $contents;
	}
	// set paging links width
	function setPagingLinksWidth($pagingLinksWidth) {
		$this->pagingLinksWidth=$pagingLinksWidth;
	}

	// show database paging links
	function showPagingLinks($database)
    {
		$contents = '';
		$contents.=($database->isRecordFound()?"<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" border=\"0\"><tr><td align=\"right\"><div class=\"paginationLinks\"><UL>". $database->getBackLink("Previous").($database->getTotalRecords()<=$database->pageSize?"":$database->getPagesLinks()).$database->getNextLink("Next")."</ul><SPAN class=\"total\" style=\"font-size:11px;\">&nbsp;(" . $database->getTotalRecords() . "&nbsp;results)&nbsp;</SPAN></div></td></tr></table><img src=\"".$GLOBALS["IMAGES_URL"]."trans.gif\" width=\"1\" height=\"10\"><br>":"");
		return $contents;
    }

	function showFixedPagingLinks($database) {
        return $database->isRecordFound()? "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"".$this->pagingLinksWidth."\" align=\"".$this->panelAlignment."\"><tr><td height=\"20\" valign=\"bottom\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"right\" border=\"0\"><tr><td>" . $database->getBackLink("<img src=\"". $GLOBALS["IMAGES_URL"] ."back.gif\" border=\"0\">") . "&nbsp;</td><td class=\"paginationLinks\">" . $database->getPagesLinks() . "&nbsp;</td><td>" . $database->getNextLink("<img src=\"". $GLOBALS["IMAGES_URL"] ."next.gif\" border=\"0\">") . "</td></tr></table></td></tr><tr><td class=\"paginationLinks\" align=\"right\">showing  " . $database->getStartingRecordsNo() . " - " . $database->getEndingRecordsNo() . " of " . $database->getTotalRecords() . "</td></tr></table>" : "";
    }
	function setDataStyle($dataStyle) {
        $this->dataStyle = $dataStyle;
    }
   	function resetStyle() {
        $this->currentDataStyle = "";
    }
    function getDataStyle() {
        if (strlen($this->currentDataStyle) == 0)
            $this->getNextDataStyle();
        return $this->currentDataStyle;
    }
    function getNextDataStyle() {
        if (strlen($this->currentDataStyle) == 0) {
			$this->currentDataStyle = "tbldata1";
        }
		else if(strcmp($this->currentDataStyle, "tbldata1") == 0) {
			$this->currentDataStyle = "tbldata2";
		}
		else {
			$this->currentDataStyle = "tbldata1";
		}
    }
	function getRichTextEditor($formName, $name, $value, $width="565", $height="450")
	{
		$contents = "";
		$contents .= "<input type=\"hidden\" name=\"".$name."\" value=\"\">\n";
		$contents .= "<script language=\"javascript\">\n";
		$contents .= "var editor;\n";
		$contents .= ("function setValues()\n");
		$contents .= ("{\n");
		$contents .= ($formName . ".". $name .".value=document.getElementById('editorFrame').contentWindow.editGetHtml();\n");
		$contents .= ("}\n");
		// Function to load HTML
		$contents .= ("function editOnEditorLoaded(objEditor, id) {\n");
		$contents .= ("// save editor object for later use\n");
		$contents .= ("editor = objEditor;\n");
		$contents .= "var html='';\n";
		$tok = split("\r\n", $value);
		for ($j = 0; $j<count($tok); $j++) {
			$contents .= "html+='".addslashes($tok[$j])."';\n";
		}
		$contents .= ("// write content to editor\n");
		$contents .= ("editor.editWrite(html);\n");
		$contents .= ("}\n");
		$contents .= "</script>\n";
		//=============================== Editor Frame ===============================//
		$contents .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" height=\"400\" id=\"parenttable\">";
		$contents .= "<tr>";
		$contents .= "<td width=\"100%\" height=\"400\">";
		$contents .= "<IFRAME id=\"editorFrame\" style=\"WIDTH: 100%; HEIGHT: 100%\" src=\"editor35/editor35/pinEdit.html\" frameborder=\"0\"></IFRAME>";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "</table>";

		return $contents;
		// ################################### OLD EDITOR  ################################### //
		/*
		$contents = "";
		$contents .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
		$contents .= "var isSetFullScreen=false;\n";
		$contents .= "var oldWidth;\n";
		$contents .= "var oldHeight;\n";
		$contents .= "function RTB_offsetLeft(element,rel)\n";
		$contents .= "{\n";
		$contents .= "var parent=element.offsetParent;\n";
		$contents .= "var offset=parseFloat(element.offsetLeft);\n";
		$contents .= "while(parent.offsetParent!=null && (parent.style.position!='relative' || rel))\n";
		$contents .= "{\n";
		$contents .= "offset+=parseFloat(parent.offsetLeft);\n";
		$contents .= "parent=parent.offsetParent;\n";
		$contents .= "}\n";
		$contents .= "return offset;\n";
		$contents .= "}\n";
		$contents .= "function RTB_offsetTop(element,rel)\n";
		$contents .= "{\n";
		$contents .= "var parent=element.offsetParent;\n";
		$contents .= "var offset=parseFloat(element.offsetTop);\n";
		$contents .= "while(parent.offsetParent!=null && (parent.style.position!='relative' || rel))\n";
		$contents .= "{ \n";
		$contents .= "offset+=parseFloat(parent.offsetTop);\n";
		$contents .= "parent=parent.offsetParent;\n";
		$contents .= "}\n";
		$contents .= "return offset;\n";
		$contents .= "}\n";
		$contents .= "function RTB_FullScreen()\n";
		$contents .= "{\n";
		$contents .= "var parent = frames.message;\n";
		$contents .= "var parentTable = document.getElementById(\"parenttable\");\n";
		$contents .= "var button = document.getElementById(\"fullscreen\");\n";
		$contents .= "if (!isSetFullScreen)\n";
		$contents .= "{\n";
		$contents .= "if (document.body.clientWidth>parentTable.offsetWidth ||\n";
		$contents .= "document.documentElement.clientWidth>parentTable.offsetWidth)\n";
		$contents .= "{\n";
		$contents .= "document.body.scroll='no';\n";
		$contents .= "document.documentElement.style.overflow = 'hidden';\n";
		$contents .= "}\n";
		$contents .= "oldWidth = parentTable.style.width;\n";
		$contents .= "oldHeight = parentTable.style.height;\n";
		$contents .= "parentTable.style.height=parseFloat(document.body.clientHeight);\n";
		$contents .= "parentTable.style.width=document.body.clientWidth;\n";
		$contents .= "if (document.documentElement.clientHeight)\n";
		$contents .= "{\n";
		$contents .= "parentTable.style.height=parseFloat(document.documentElement.clientHeight)-parseFloat(document.documentElement.clientHeight)/5;\n";
		$contents .= "parentTable.style.width=parseFloat(document.documentElement.clientWidth);\n";
		$contents .= "}\n";
		$contents .= "isSetFullScreen=true;\n";
		$contents .= "button.parentNode.style.borderStyle='inset';\n";
		$contents .= "button.parentNode.style.borderWidth='2px';\n";
		$contents .= "}\n";
		$contents .= "else\n";
		$contents .= "{\n";
		$contents .= "parentTable.style.height=oldHeight;\n";
		$contents .= "parentTable.style.width=oldWidth;\n";
		$contents .= "isSetFullScreen=false;\n";
		$contents .= "document.body.scroll='yes';\n";
		$contents .= "document.documentElement.style.overflow = 'visible';\n";
		$contents .= "button.parentNode.style.borderStyle='none';\n";
		$contents .= "button.parentNode.style.borderWidth='';\n";
		$contents .= "}\n";
		$contents .= "var left = RTB_offsetLeft(parentTable,true);\n";
		$contents .= "var top = RTB_offsetTop(parentTable,true);\n";
		$contents .= "document.body.scrollTop=top;\n";
		$contents .= "document.body.scrollLeft=left;\n";
		$contents .= "document.documentElement.scrollTop=top;\n";
		$contents .= "document.documentElement.scrollLeft=left;\n";
		$contents .= "}\n";
		$contents .= "</script>\n";
		$contents .= "<script language=\"javascript\" src=\"".$GLOBALS["ADMIN_SCRIPT_URL"]."rte.js\" type=\"text/javascript\"></script>";
		$contents .= "<table width=\"100%\" border=\"0\" align=\"center\">";
		$contents .= "<tr>";
		$contents .= "<td class=\"tbldata2\" height=\"61\" width=\"100%\" valign=\"top\">";
		$contents .= "<iframe width=\"260\" height=\"165\" id=\"colourPalette\" src=\"".$GLOBALS["RTE_URL"]."/rtecolourpalette.php\" style=\"visibility:hidden; position: absolute; left: 0px; top: 0px;\" frameborder=\"0\" scrolling=\"no\"></iframe>";
		$contents .= "<table width=\"$width\" height=\"$height\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" id=\"parenttable\">";
		$contents .= "<tr>";
		$contents .= "<td height=\"50\" >";
		$contents .= "<span id=\"ToolBar1\">";
		$contents .= "<select id=\"selectTxtBlock\" name=\"selectTxtBlock\" onChange=\"FormatText('formatblock', selectTxtBlock.options[selectTxtBlock.selectedIndex].value); document.".$formName.".selectTxtBlock.options[0].selected = true;\" style=\"width:105px;\">";
		$contents .= "<option value=\"0\" selected>---- Font Styles ----</option>";
		$contents .= "<option value=\"<p>\">Normal</option>";
		$contents .= "<option value=\"<p>\">Paragraph</option>";
		$contents .= "<option value=\"<h1>\">Heading 1 <h1></option>";
		$contents .= "<option value=\"<h2>\">Heading 2 <h2></option>";
		$contents .= "<option value=\"<h3>\">Heading 3 <h3></option>";
		$contents .= "<option value=\"<h4>\">Heading 4 <h4></option>";
		$contents .= "<option value=\"<h5>\">Heading 5 <h5></option>";
		$contents .= "<option value=\"<h6>\">Heading 6 <h6></option>";
		$contents .= "</select>";

		$contents .= " <select id=\"selectFontSize\" name=\"selectFontSize\" onChange=\"FormatText('fontsize', selectFontSize.options[selectFontSize.selectedIndex].value); document.".$formName.".selectFontSize.options[0].selected = true;\" style=\"width:105px;\">";
		$contents .= "<option value=\"0\" selected>---- Font Sizes ----</option>";
		$contents .= "<option value=\"1\">10</option>";
		$contents .= "<option value=\"2\">12</option>";
		$contents .= "<option value=\"4\">14</option>";
		$contents .= "<option value=\"5\">18</option>";
		$contents .= "<option value=\"6\">24</option>";
		$contents .= "<option value=\"7\">36</option>";
		$contents .= "</select>";
		$contents .= "<img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_bold.gif\" align=\"absmiddle\" title=\"Bold\" onClick=\"FormatText('bold', '')\" style=\"cursor: pointer;\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_italic.gif\"  align=\"absmiddle\" title=\"Italic\" onClick=\"FormatText('italic', '')\" style=\"cursor: pointer;\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_underline.gif\" align=\"absmiddle\" title=\"Underline\" onClick=\"FormatText('underline', '')\" style=\"cursor: pointer;\">";
		$contents .= " <img id=\"forecolor\" src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_colour_pallete.gif\" align=\"absmiddle\" border=\"0\" title=\"Text Colour\" onClick=\"FormatText('forecolor', '')\" style=\"cursor: pointer;\">";
		$contents .= " <img id=\"backcolor\" src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_fill.gif\" align=\"absmiddle\" border=\"0\" title=\"Background Colour\" onClick=\"FormatText('backcolor', '')\" style=\"cursor: pointer;\">";
		$contents .= " <span><img id=\"fullscreen\" src=\"".$GLOBALS["RTE_IMAGES_URL"]."fullscreen.gif\" align=\"absmiddle\" border=\"0\" onClick=\"RTB_FullScreen();return false;\" style=\"cursor: pointer;\" title=\"Full Screen\"></span>";
		$contents .= "</span><br>";
		$contents .= "<span id=\"ToolBar2\">";
		$contents .= "<img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_cut.gif\" align=\"absmiddle\" onClick=\"FormatText('cut')\" style=\"cursor: pointer;\" title=\"Cut\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_copy.gif\" align=\"absmiddle\" onClick=\"FormatText('copy')\" style=\"cursor: pointer;\" title=\"Copy\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_paste.gif\" align=\"absmiddle\" onClick=\"FormatText('paste')\" style=\"cursor: pointer;\" title=\"Paste\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_undo.gif\" align=\"absmiddle\" onClick=\"FormatText('undo')\" style=\"cursor: pointer;\" title=\"Undo\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_redo.gif\" align=\"absmiddle\" onClick=\"FormatText('redo')\" style=\"cursor: pointer;\" title=\"Redo\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_left_just.gif\" align=\"absmiddle\" onClick=\"FormatText('JustifyLeft', '')\" style=\"cursor: pointer;\" title=\"Left Justify\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_centre.gif\" align=\"absmiddle\" border=\"0\" onClick=\"FormatText('JustifyCenter', '')\" style=\"cursor: pointer;\" title=\"Centre Justify\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_right_just.gif\" align=\"absmiddle\" onClick=\"FormatText('JustifyRight', '')\" style=\"cursor: pointer;\" title=\"Right Justify\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_justify.gif\" align=\"absmiddle\" onClick=\"FormatText('JustifyFull', '')\" style=\"cursor: pointer;\" title=\"Justify\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_or_list.gif\" align=\"absmiddle\" border=\"0\" onClick=\"FormatText('InsertOrderedList', '')\" style=\"cursor: pointer;\" title=\"Ordered List\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_list.gif\" align=\"absmiddle\" border=\"0\" onClick=\"FormatText('InsertUnorderedList', '')\" style=\"cursor: pointer;\" title=\"Unordered List\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_outdent.gif\" align=\"absmiddle\" onClick=\"FormatText('Outdent', '')\" style=\"cursor: pointer;\" title=\"Outdent\">";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_indent.gif\" align=\"absmiddle\" border=\"0\" onClick=\"FormatText('indent', '')\" style=\"cursor: pointer;\" title=\"Indent\">";
		$contents .= " <a href=\"javascript:openWin('".$GLOBALS["RTE_URL"]."/rtelinkwindow.php','insertLink','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=110')\"><img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_hyperlink.gif\" align=\"absmiddle\" border=\"0\" style=\"cursor: pointer;\" title=\"Add Hyperlink\"></a>";
		$contents .= " <a href=\"javascript:openWin('".$GLOBALS["RTE_URL"]."/rteimagewindow.php','insertImg','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=200')\"><img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_image.gif\" align=\"absmiddle\" border=\"0\" title=\"Add Image\" style=\"cursor: pointer;\"></a>";
		$contents .= " <a href=\"javascript:openWin('".$GLOBALS["RTE_URL"]."/rtetablewindow.php','insertTable','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=190')\"><img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_insert_table.gif\" align=\"absmiddle\" border=\"0\" title=\"Insert Table\" style=\"cursor: pointer;\"></a>";
		$contents .= " <img src=\"".$GLOBALS["RTE_IMAGES_URL"]."post_button_spell_check.gif\" align=\"absmiddle\" border=\"0\" onClick=\"checkspell()\" style=\"cursor: pointer;\" title=\"Spell Check\">";
		$contents .= "</span>";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "<tr>";
		$contents .= "<td height=\"99%\">";
		$contents .= "<input type=\"hidden\" name=\"".$name."\" value=\"\">\n";
		$contents .= "<script language=\"javascript\">\n";
		$contents .= "// Create an iframe and turn on the design mode for it\n";
		$contents .= "document.write ('<iframe id=\"message\" src=\"".$GLOBALS["RTE_URL"]."/rtetextbox1.php\" width=\"100%\" height=\"100%\" onMouseOver=\"hideColourPallete();\"></iframe>');\n";
		$contents .= "</script>\n";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "<tr>";
		$contents .= "<td class=\"tbldata2\" height=\"20\">";
		$contents .= "<input type=\"checkbox\" name=\"htmlview\" onClick=\"htmlOn = (this.checked == false); HTMLview()\"> <font class=\"normalfont\">Show HTML Code</font>";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "</table>";
		$contents .= "<script language=\"javascript\">\n";
		// Function to set values after form submits
		$contents .= ("function setValues()\n");
		$contents .= ("{\n");
		$contents .= ("if (htmlOn == false)\n");
		$contents .= ("{\n");
		$contents .= ($formName . ".". $name .".value=document.getElementById('message').contentWindow.document.body.innerHTML;\n");
		$contents .= ("}\n");
		$contents .= ("else");
		$contents .= ("{\n");
		$contents .= ($formName . ".". $name .".value=document.getElementById('message').contentWindow.document.body.innerText;\n");
		$contents .= ("}\n");
		$contents .= ("}\n");
		// Function to load HTML for editors
		$contents .= "function Start()";
		$contents .= "{";
		$contents .= "document.getElementById('message').contentWindow.document.designMode = 'On';\n";
		$contents .= "var html;";
		$contents .= "html = \"\";";
		$tok = split("\r\n", $value);
		for ($j = 0; $j<count($tok); $j++)
		{
			$contents .= "html+='".addslashes($tok[$j])."';\n";
		}
		$contents .= "if (document.getElementById('message').contentWindow.document.body) {\n";
		$contents .= "document.getElementById('message').contentWindow.document.body.innerHTML=html;\n";
		$contents .= "} else {\n";
		$contents .= "document.getElementById('message').contentWindow.document.body.innerHTML=html;\n";
		$contents .= "}\n";
		$contents .= "}\n";
		$contents .= "window.onload=Start;";
		$contents .= "</script>\n";
		// JavaScript check
		$contents .= "<noscript>";
		$contents .= "<br>";
		$contents .= "<br>";
		$contents .= "JavaScript must be enabled on your web browser!";
		$contents .= "</noscript>";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "</table>";
		return $contents;
		*/
		// ################################### OLD EDITOR EDIT ################################### //
	}
}
?>