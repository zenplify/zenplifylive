<?php
class Admin
{
	function Admin()
	{
    	session_start();
	}
	// set Admin Data in Session
	function setId($id)
	{
		$_SESSION["admId"] = $id;
	}
	function setName($name)
	{
		$_SESSION["admName"] = $name;
	}
	function setLogin($login)
	{
	  $_SESSION["admLogin"] = $login;
	}
	function setRights($rigthts)
	{
	  $_SESSION["admRights"] = $rigthts;
	}
	// get Admin from Session Datas
	function getId()
	{
		return (isset($_SESSION["admId"]) ? $_SESSION["admId"] : "");
	}
	function getName()
	{
		return (isset($_SESSION["admName"]) ? $_SESSION["admName"] : "");
	}
	function getLogin()
	{
		return (isset($_SESSION["admLogin"]) ? $_SESSION["admLogin"] : "");
	}
	function getRights()
	{
		return (isset($_SESSION["admRights"]) ? $_SESSION["admRights"] : "");
	}	
	function isLogin()
	{
		return isset($_SESSION["admLogin"]);
	}
	function destroy()
	{
		session_unset();
		session_destroy(); 
	}
}

?>