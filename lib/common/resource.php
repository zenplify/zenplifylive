<?php
class Resource
{
	function getParameter($name) {
		return isset($_REQUEST[$name])?stripslashes($_REQUEST[$name]):"";
	}
	function getFileContents($path) {
		if (is_file($path))
			return file_get_contents($path);
		else
			return "";
	}
	
	function getMessageSubject($subjectNo) {
		switch($subjectNo) {
			case 0:
				return "Suggestions";
			case 1:
				return "Need Further Information";
			case 2:
				return "About Prices";
			case 3:
				return "For Advertisement";
			case 4:
				return "Others";
		}
		return "Unknown";
	}
	function getCurrentDate($format) {
		// d for day(1-31)
		// D for 3 letter day name
		// m for month(1-12)
		// M for 3 letter month name
		// F for full month name
		// Y for year(4 digit) and y for (2 digit)
		return date($format);
	}
	function ImageID()
	{
		$random_id_length = 5;
		//generate a random id encrypt it and store it in $rnd_id
		$rnd_id = crypt(uniqid(rand(),1));
		//to remove any slashes that might have come
		$rnd_id = strip_tags(stripslashes($rnd_id));
		//Removing any . or / and reversing the string
		$rnd_id = str_replace(".","",$rnd_id);
		$rnd_id = strrev(str_replace("/","",$rnd_id));
		//finally I take the first 10 characters from the $rnd_id 
//		$rnd_id = substr($rnd_id,0,$random_id_length);
		return $rnd_id;
	}
	function getCurrentYear() {
		return date("Y");
	}
	function getMonthName($month) {
		$months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		return $months[$month-1];
	}
	function getFullMonthName($month) {
		$months = array("January", "February", "March", "April", "May", "June", "July", "August", "Septemper", "October", "November", "December");
		return $months[$month-1];
	}
	function getDayName($day) {
		$week = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		return $week[$day-1];
	}
	function generateUniqueID() {
		$random_id_length = 10;
		//generate a random id encrypt it and store it in $rnd_id
		$rnd_id = crypt(uniqid(rand(),1));
		//to remove any slashes that might have come
		$rnd_id = strip_tags(stripslashes($rnd_id));
		//Removing any . or / and reversing the string
		$rnd_id = str_replace(".","",$rnd_id);
		$rnd_id = strrev(str_replace("/","",$rnd_id));
		//finally I take the first 10 characters from the $rnd_id 
		$rnd_id = substr($rnd_id,0,$random_id_length);
		return $rnd_id;
	}

	

}
?>