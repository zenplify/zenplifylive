<?php
class Database
{
	var $connection;
	var $result;
	var $row;
	var $currentPage=1,$pageSize=25,$noOfPages=0,$noOfRecords=0,$pointer=0,$count=0;
	var $scroll=1,$noOfScrolls=0,$scrollSize=10;
	var $url="";
	var $isRecordFound=false;
	
/////////////////////////////CONSTRUCTOR FOR CONNECTION///////////////////////////////	
	function Database($host, $db, $user, $pass) {

		$this->connection = @mysql_pconnect($host.":3306", $user, $pass);

		if (!$this->connection) {
	        echo "Unable to connect to DB: " . mysql_error();
    	    exit;
	    }
		if (!@mysql_select_db($db, $this->connection)) {
        	echo "Unable to select $databaseName: " . mysql_error();
	        exit;
    	}
	}
	//////////////////////////////////////IS RECORD FOUND///////////////////////////////	
	function isRecordFound()
	{
		return $this->isRecordFound;
	}
	///////////////////////////// SELECT RECORDS FROM DATABASE /////////////////////////////////	
	function executeQuery($sqlquery) { 
		global $CONF;
		$this->result = mysql_query($sqlquery);
		if (!$this->result) {
			///////////////////////////////////
			$errors = "";
			$errors .= $sqlquery;
			$errors .= mysql_error( $this->connection );
			trigger_error( mysql_error( $this->connection ), E_USER_NOTICE );
			//echo "<pre>" . $this->_sql . "</pre>\n";
			if (function_exists( 'debug_backtrace' )) {
				foreach( debug_backtrace() as $back) {
					if (@$back['file']) {
						echo '<br />'.$back['file'].':'.$back['line'];
						$errors .=  "\r\n".$back['file'].':'.$back['line'];
					}
				}
			}
//			mail("test@xcel-soft.com",  $_SESSION[$CONF["COMPANY_SESSION_VAR"]]["Name"]." - ".$_SESSION[$CONF["EVENT_SESSION_VAR"]]["Name"] , $errors, "From: ".$_SESSION[$CONF["COMPANY_SESSION_VAR"]]["Email"]."\r\n");
			return false;
	    }
		$this->isRecordFound = mysql_fetch_array($this->result, MYSQL_BOTH) ? true: false;
		if($this->isRecordFound)
			mysql_data_seek($this->result,0);
	}
	/////////////////////////////UPDATE RECORDS IN DATABASE/////////////////////////////////	
	function executeUpdate($sqlquery)
	{
		global $CONF;
		$res = mysql_query($sqlquery);

		if (!$res) {
			$errors = "";			
			$errors .= $sqlquery;			
			$errors .= mysql_error( $this->connection );
			trigger_error( mysql_error( $this->connection ), E_USER_NOTICE );
			//echo "<pre>" . $this->_sql . "</pre>\n";
			if (function_exists( 'debug_backtrace' )) {
				foreach( debug_backtrace() as $back) {
					if (@$back['file']) {
						echo '<br />'.$back['file'].':'.$back['line'];
						$errors .=  "\r\n".$back['file'].':'.$back['line'];
					}
				}
			}
//			mail("test@xcel-soft.com",  $_SESSION[$CONF["COMPANY_SESSION_VAR"]]["Name"]." - ".$_SESSION[$CONF["EVENT_SESSION_VAR"]]["Name"] , $errors, "From: ".$_SESSION[$CONF["COMPANY_SESSION_VAR"]]["Email"]."\r\n");
			return false;
		}
		return mysql_affected_rows();
	}
	////////////////////////////Get the Last inserted Auto incremented field Id /////////////////
	function getInsertedId()
	{
		return mysql_insert_id($this->connection);
	}
	/////////////////////////////MOVE POINTER TO NEXT RECORD///////////////////////////////	
	function getNextRecord()
	{
		return $this->row = mysql_fetch_array($this->result, MYSQL_BOTH);
	}
	/////////////////////////////////////GET COLUMN////////////////////////////////////////	
	function getColumn($col)
	{
		return $this->row[$col];
	}
	/////////////////////////////////////GET Int COLUMN////////////////////////////////////////
	function getIntColumn($col)
	{
		return (int) $this->row[$col];
	}
	/////////////////////////////////////GET ROUND COLUMN////////////////////////////////////////
	function getRoundColumn($col, $precision=2)
	{
		return number_format($this->row[$col], $precision,".",",");
	}
	/////////////////////////////////////SET PAGE SIZE////////////////////////////////////
	function setScrollSize($scrollSize)
	{
		$this->scrollSize=$scrollSize;
	}
	/////////////////////////////////////SET PAGE SIZE////////////////////////////////////
	function setPageSize($pageSize)
	{
		$this->pageSize=$pageSize;
	}
	////////////////////////////////////SET URL FOR PAGING /////////////////////////////////
	function setURL($url,$currentPage)
	{
		$this->currentPage= (is_null($currentPage) || trim($currentPage) == "" )? 1 : (int)$currentPage;
		$this->url=$url;
		$this->resetRecords();
	}
	/////////////////////////////////////GET PAGES LINKS////////////////////////////////////
	function getPagesLinks()
	{
		$page = "";
		for($i=($this->scroll*$this->scrollSize)-($this->scrollSize-1);($i<=$this->scroll*$this->scrollSize) && ($i<=$this->noOfPages);$i++)
			$page= $this->currentPage == $i ? $page . "<b>(" . $i . ")</b>&nbsp;" : $page . "&nbsp;<a href=" . $this->url . (strpos($this->url,"?")!==false?"&":"?") . "p=" . $i . ">" . $i . "</a>&nbsp;";
		return $page;
	}
	/////////////////////////////////////GET NEXT SCROLL LINK////////////////////////////////////	
	function getNextLink($st)
	{
		return $this->noOfScrolls > $this->scroll ? "<a href=" . $this->url . (strpos($this->url,"?")!==false?"&":"?") . "p=" . ((($this->scroll+1)*$this->scrollSize)-($this->scrollSize-1)) . ">" . $st . "</a>":"";	
	}
	/////////////////////////////////////GET BACK SCROLL LINK////////////////////////////////////	
	function getBackLink($st)
	{
		return $this->scroll > 1?"<a href=" . $this->url . (strpos($this->url,"?")!==false?"&":"?") . "p=" . ((($this->scroll-1)*$this->scrollSize)-($this->scrollSize-1)) . ">" . $st . "</a>":"";
	}
	//get current record no
	function getRecordNo()
	{
//		return $this->result.getRow();
	}
	/////////////////////////////////////GET TOTAL NUMBER OF RECORDS////////////////////////////////////			
	function getTotalRecords()
	{
		return 	$this->noOfRecords;
	}
	/////////////////////////////////////IN CASE OF PAGING, GET STARTING RECORD NUMBER ////////////////////////////////////			
	function getStartingRecordsNo()
	{
		return 	$this->pointer;
	}
	/////////////////////////////////////IN CASE OF PAGING, GET ENDING RECORD NUMBER ////////////////////////////////////		
	function getEndingRecordsNo()
	{
		return 	$this->pointer + $this->pageSize-1 <= $this->noOfRecords ? $this->pointer + $this->pageSize-1 : $this->noOfRecords;
	}
	/////////////////////////////////////GET NEXT PAGING RECORD////////////////////////////////////		
	function getNextPagingRecord()
	{
		$this->count++;
		return ($this->count <= $this->pageSize &&  $this->row = mysql_fetch_array($this->result, MYSQL_BOTH) );
	}
	/////////////////////////////////////SET PAGING POSITION////////////////////////////////////	
	function resetRecords()
	{
		$this->noOfRecords = mysql_num_rows($this->result);
		$this->noOfPages=ceil((float)$this->noOfRecords/$this->pageSize);// For on of pages
		$this->noOfScrolls=ceil((float)$this->noOfPages/$this->scrollSize);// for no of scrolls
		$this->scroll=ceil((float)$this->currentPage/$this->scrollSize);// current scroll
		if ($this->currentPage < 1 || $this->currentPage > $this->noOfPages)
		{
			$this->currentPage=1;
			$this->scroll=1;
		}
		$this->pointer = ($this->currentPage*$this->pageSize)-($this->pageSize-1);// move the pointer to current page
		if($this->pointer > 1)
			mysql_data_seek($this->result,$this->pointer-1);
		else if($this->isRecordFound)
			mysql_data_seek($this->result,0);
		$this->count = 0;
	}
	///////////////////////////////////// COMMIT FUNCTION////////////////////////////////////		
	function commit()
	{
		mysql_query("commit");
	}
	///////////////////////////////////// ROLLBACK FUNCTION////////////////////////////////////		
	function rollBack()
	{
		mysql_query("rollback");
	}
	/////////////////////////////////////CLOSE DATABASE CONNECTION////////////////////////////////////		
	function close()
	{
		if($this->result)
		{
			mysql_free_result($this->result);
		}
		mysql_close($this->connection);
	}
	///////////////////////////////////// REPLACE STRINGS ////////////////////////////////////		
	function replace($str,$findChar="",$repStr="")
	{

		if($findChar != "" && $repStr != "")
			return (string) str_replace($findChar,$repStr,$str);
		else
			return (string) str_replace($findChar,$repStr,$this->replaceDoubleQuote($this->replaceSingleQuote($str)));

//		return mysql_real_escape_string($str);
	}
	function replaceSingleQuote($str,$findChar="'",$repStr="''")
	{
		return (string) str_replace($findChar,$repStr,$str); 
	}
	function replaceDoubleQuote($str,$findChar="\"",$repStr="&quot;")
	{
		return (string) str_replace($findChar,$repStr,$str); 
	}
	///////////////////////////////////// Get Round Values ////////////////////////////////////		
	function getRoundValue($value, $precision=2)
	{
		return number_format($value, $precision,".",",");
	}
}
?>