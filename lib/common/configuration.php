<?php
	
	$PATH_SEPERATOR="\\";
	$PATH="K:".$PATH_SEPERATOR."xampp".$PATH_SEPERATOR."htdocs".$PATH_SEPERATOR."biznbuy".$PATH_SEPERATOR;
	$URL="http://dbs/biznbuy/";
	$SSL_URL="http://dbs/biznbuy/";
	
	/*
	$PATH_SEPERATOR="/";
	$URL="http://www.xcel-soft.com/a1oriental";
	$PATH_SEPERATOR="/";
	$PATH=$PATH_SEPERATOR."home".$PATH_SEPERATOR."a1oriental".$PATH_SEPERATOR."public_html".$PATH_SEPERATOR;
	*/
	
	function getURL($page)
	{
		return $this->URLS[strtolower($page)] == ""?"":(strpos("[".$page."]", $this->SECURE_PAGES) !== FALSE ? $this->SSL_URL : $this->URL) . $this->URLS[strtolower($page)];
	}

	$PAGE_TITLE = "Biz and Buy";

	$IMAGES_URL=$URL."images/";
	$DATA_PATH = $PATH."data/";
	
	$CATE_DATA_PATH = $PATH."categories".$PATH_SEPERATOR;

	$EDUCATION_PATH = $PATH."education/";
	$EDUCATION_PATHA = $PATH."education1/";
	$FAQS_DATA_PATH = $PATH."artic".$PATH_SEPERATOR;
	$FAQS_DATA_URL = $URL."data/articles".$PATH_SEPERATOR;

	$ADMIN_SCRIPT_URL=$ADMIN_URL."js/";
	$ADMIN_IMAGES_URL=$IMAGES_URL."admin/";
	$ADMIN_SCRIPT_URL=$ADMIN_URL."js/";
	$ADMIN_STYLE_URL=$ADMIN_URL."style/";
	$ADMIN_URL=$URL."admin/";

	$CURRENCY="$";

	$DTMFORMAT = '%m/%d/%Y %r';

	$DATE_FORMAT1 = '%M %d, %Y';
	$DATE_FORMAT2 = '%m/%d/%Y';
	//$DATE_FORMAT3 = '%m/%d/%Y';
	/*
	$CONF["dateFormat1"] = '%m/%d/%Y';

	$CONF["dateFormat2"] = 'l, F d, Y';
	
	$CONF["dateTimeFormat1"] = 'm/d/Y h:i A';
		
	$CONF["dateTimeFormat2"] = 'l, F d, Y h:i A';
	*/

	// New Configurations
	$IMAGES_PATH=$PATH."images".$PATH_SEPERATOR;
	
	$HTML_BANNERS_PATH=$IMAGES_URL."banners/html/";
	$IMAGE_BANNERS_PATH=$IMAGES_URL."banners/image/";
	$FLASH_BANNERS_PATH=$IMAGES_URL."banners/flash/";

	$TEMP_IMAGES_URL = $IMAGES_URL."temp/";
	$TEMP_IMAGES_PATH = $IMAGES_PATH."temp".$PATH_SEPERATOR;
	$ARTICLES_DATA_PATH = $DATA_PATH."articles".$PATH_SEPERATOR;
	$ARTICLES_DATA_URL = $URL."data/articles".$PATH_SEPERATOR;

	$PRODUCTS_DRAG_IMAGES_URL = $IMAGES_URL."products/drag/";
	$PRODUCTS_DRAG_IMAGES_PATH = $IMAGES_PATH."products/drag/";

	$PRODUCTS_THUMB_IMAGES_URL = $IMAGES_URL."products/thumb/";
	$PRODUCTS_THUMB_IMAGES_PATH = $IMAGES_PATH."products/thumb/";
	$PHOTOS_THUMB_IMAGES_URL = $IMAGES_URL."ads/thumb/";
	$PHOTOS_THUMB_IMAGES_PATH = $IMAGES_PATH."ads/thumb/";

	$PRODUCTS_ENLARGED_IMAGES_URL = $IMAGES_URL."products/enlarged/";
	$PRODUCTS_ENLARGED_IMAGES_PATH = $IMAGES_PATH."products/enlarged/";
	$PHOTOS_ENLARGED_IMAGES_URL = $IMAGES_URL."photos/enlarged/";
	$PHOTOS_ENLARGED_IMAGES_PATH = $IMAGES_PATH."photos/enlarged/";
	$GALLERY_THUMB_IMAGES=$IMAGES_URL."photos/thumb/";
	$GALLERY_ENLARGED_IMAGES=$IMAGES_URL."photos/enlarged/";

	$PRODUCTS_DRAG_IMAGES_WIDTH=55;
	$PRODUCTS_DRAG_IMAGES_HEIGHT=55;

	$PRODUCTS_THUMB_IMAGES_WIDTH=115;
	$PRODUCTS_THUMB_IMAGES_HEIGHT=115;
	$PHOTOS_THUMB_IMAGES_WIDTH=100;
	$PHOTOS_THUMB_IMAGES_HEIGHT=100;

	$PRODUCTS_ENLARGED_IMAGES_WIDTH=690;
	$PRODUCTS_ENLARGED_IMAGES_HEIGHT=690;
	$PHOTOS_ENLARGED_IMAGES_WIDTH=300;
	$PHOTOS_ENLARGED_IMAGES_HEIGHT=225;
?>