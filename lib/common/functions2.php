<?php
/* Common Functions */

define( "_NOTRIM", 0x0001 );
define( "_ALLOWHTML", 0x0002 );
define( "_ALLOWRAW", 0x0004 );


function getParam( &$arr, $name, $def=null, $mask=0 ) {
	static $noHtmlFilter 	= null;
	static $safeHtmlFilter 	= null;

	$return = null;
	if (isset( $arr[$name] )) {
		$return = $arr[$name];

		if (is_string( $return )) {
			// trim data
			if (!($mask&_NOTRIM)) {
				$return = trim( $return );
			}

			if ($mask&_ALLOWRAW) {
				// do nothing
			} else if ($mask&_ALLOWHTML) {
				// do nothing - compatibility mode
			} else {
				// send to inputfilter
				if (is_null( $noHtmlFilter )) {
					$noHtmlFilter = new InputFilter( /* $tags, $attr, $tag_method, $attr_method, $xss_auto */ );
				}
				$return = $noHtmlFilter->process( $return );

				if (!empty($return) && is_numeric($def)) {
				// if value is defined and default value is numeric set variable type to integer
					$return = intval($return);
				}
			}

			// account for magic quotes setting
			if (!get_magic_quotes_gpc()) {
				$return = addslashes( $return );
			}
		}

		return $return;
	} else {
		return $def;
	}
}

function base62_encode($num){
        static $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
        $str = '';
        $x=0;
 
        do {
                $idx = ($num % 62);
                $str .= $chars[$idx];
                $num = floor($num/62);
        } while ($num > 0);
        return strrev($str);
}
 
function base62_decode($str){
        static $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($str === '0'){ return 0; }
 
        $len = strlen($str);
        $num = strpos($chars, $str[$len-1]);
        for ($i=$len-2,$j=1; $i>=0; $i--,$j++){
                $cval = strpos($chars, $str[$i]);
                $num += pow(62, $j)*$cval;
        }
        return $num;
}

//redirect
function SendRedirect($loc) { 
	if (headers_sent()) {
		echo "<script>document.location.href='$loc';</script>\n";
	} else {
		@ob_end_clean(); // clear output buffer
		header( 'HTTP/1.1 301 Moved Permanently' );
		header( "Location: ". $loc );
	}
	exit;
}

function getCurrency()
{
	return "Rs.";

}

function getPrice($price, $pricetype)
{
	if ($pricetype == '1')
	
		return "Rs.".number_format($price, 2);
	
	return "Call for Price";	

}

function getRoundValue($value)
{
	/*
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
	DecimalFormat df = (DecimalFormat) nf;
	df.setMinimumFractionDigits(2);
	df.setMaximumFractionDigits(2);
	df.setDecimalSeparatorAlwaysShown(true);
	return df.format(Double.valueOf(value));
	*/
	return $value;
}

function getMonthName($month) {
	$months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	return $months[$month-1];
}

function getFullMonthName($month) {
	$months = array("January", "February", "March", "April", "May", "June", "July", "August", "Septemper", "October", "November", "December");
	return $months[$month-1];
}

function getMessageSubject($subjectNo)
{
	switch ($subjectNo)
	{
		case 0:
			return "Suggestions";
		case 1:
			return "Fraud Item";	
		case 2:
			return "Need Further Information";
		case 3:
			return "Problem with the Buyer";
		case 4:
			return "Problem with the Seller";
		case 5:
			return "Registration/User Problem";
	}
	return "Unknown";
}

function getAdImage($ad, $t='s')
{ 
	global $CONF;
	$img = "";
	switch ($t)	{ 
		case 's':
			$img = $ad->simg_id;
			break;
		case 'f':
			$img = $ad->fimg_id;
			break;
		case 'e':
			$img = $ad->eimg_id;
			break;
	}
	return '<a href="addetails.php?ad='.$ad->ad_id.'" title="'.$ad->ad_title.'"><img src="'.$CONF["siteurl"].'images/ads/'.$ad->ad_path.$ad->ad_id."/".$img.'" alt="'.$ad->ad_title.'" /></a>';
}

function getCondition($type)
{
	switch ($type)
	{
	case 0:
		return "New";
	case 1:
		return "Used";
	case 2:
		return "Good";
	case 3:
		return "Fair";
	case 4:
		return "Poor";
	case 5:
		return "Other";
	}
	return "Unknown";
}

	function getQtyUnit($unit)
	{
		switch ($unit)
		{
		case 0:
			return "Item";
		case 1:
			return "Lot";
		case 2:
			return "Set";
		case 3:
			return "Case";
		case 4:
			return "Carton";
		case 5:
			return "Container";
		case 6:
			return "Dozen";
		case 7:
			return "Palette";
		case 8:
			return "Piece";
		case 9:
			return "Skid";
		case 10:
			return "Truckload";
		case 12:
			return "Other";
		}
		return "Unknown";
	}

	function getFileContents($filename)
	{
		return file_get_contents($filename);
	}
	
	function getShippingLocation($type)
	{
		switch ($type)
		{
		case 0:
			return "Internationally (World Wide)";
		case 1:
			return "Within the Country of the Seller Only";
		case 2:
			return "Within the United States Only";
		default:
			return "Not Applicable";
		}
	}
	// Banner Zones //
	function getBannerPlacement($type) { 
		switch ($type) { 
			case 0:
				return "Top";
			case 1:
				return "Side";
			case 2:
				return "Bottom";
			default:
				return "Unknown";
		}
	}

	// Banner Zones //
	function getBannerZoneType($type) { 
		switch ($type) { 
			case 0:
				return "Page";
			case 1:
				return "Pop Up";
			case 2:
				return "Float";
			default:
				return "Unknown";
		}
	}

	// Banner Zones //
	function getBannerType($type) { 
		switch ($type) { 
			case 0:
				return "HTML Banner";
			case 1:
				return "Image Banner";
			case 2:
				return "Flash Banner";
			default:
				return "Unknown";
		}
	}
	
	function getParameter($val) { 
		return $val;
	}

	function getSellingMethod($type)
	{
		switch ($type)
		{
		case 1:
			return "Open Contact Info";
		case 2:
			return "Online Negotiations";
		default:
			return "";
		}
	}

function formatURLKey($string)
{
	// Single letters
	$single_fr = explode(" ", "À Á Â Ã Ä Å &#260; &#258; Ç &#262; &#268; &#270; &#272; Ð È É Ê Ë &#280; &#282; &#286; Ì Í Î Ï &#304; &#321; &#317; &#313; Ñ &#323; &#327; Ò Ó Ô Õ Ö Ø &#336; &#340; &#344;  &#346; &#350; &#356; &#354; Ù Ú Û Ü &#366; &#368; Ý  &#377; &#379; à á â ã ä å &#261; &#259; ç &#263; &#269; &#271; &#273; è é ê ë &#281; &#283; &#287; ì í î ï &#305; &#322; &#318; &#314; ñ &#324; &#328; ð ò ó ô õ ö ø &#337; &#341; &#345; &#347;  &#351; &#357; &#355; ù ú û ü &#367; &#369; ý ÿ  &#378; &#380;");
	$single_to = explode(" ", "A A A A A A A A C C C D D D E E E E E E G I I I I I L L L N N N O O O O O O O R R S S S T T U U U U U U Y Z Z Z a a a a a a a a c c c d d e e e e e e g i i i i i l l l n n n o o o o o o o o r r s s s t t u u u u u u y y z z z");
	$single = array();
	for ($i=0; $i<count($single_fr); $i++) {
		$single[$single_fr[$i]] = $single_to[$i];
	}
	// Ligatures
	$ligatures = array("Æ"=>"Ae", "æ"=>"ae", ""=>"Oe", ""=>"oe", "ß"=>"ss");
	// German umlauts
	$umlauts = array("Ä"=>"Ae", "ä"=>"ae", "Ö"=>"Oe", "ö"=>"oe", "Ü"=>"Ue", "ü"=>"ue");
	// Replace
	$replacements = array_merge($single, $ligatures);
	if ($german) { 
		$replacements = array_merge($replacements, $umlauts);
	}
	$string = strtr($string, $replacements);

	$string = preg_replace('#[^0-9a-z]+#i', '-', $string);
	$string = strtolower($string);
	$string = trim($string, '-');

	return $string;
} 
	
function saveOutputImage($source, $destination, $newWidth, $newHeight, $scaleType=1) { 

	$quality = 80;
	/* Check for the image's exisitance */ 
	if (!file_exists($source)) { 
		echo 'File does not exist!'; 
	}
	else { 
		$size = getimagesize($source); // Get the image dimensions and mime type
		// Get scale factor
		if ($scaleType==1) { // Intelli
			$scale = ($size[1]>$size[0]?($size[1]<$newHeight?1.0:($newHeight/(float)$size[1])):($size[0]<$newWidth?1.0:($newWidth/(float)$size[0])));
		} else if ($scaleType==2) {// Width wise
			$scale = ($size[0]<$newWidth?1.0:($newWidth/(float)$size[0]));
		} else if ($scaleType==3) { // Height wise
			$scale = ($size[1]<$newHeight?1.0:($newHeight/(float)$size[1]));
		}
		$w = (integer)($size[0]*$scale); // Width divided 
		$h = (integer)($size[1]*$scale); // Height divided 
		$ny = round(abs($newHeight - $h) / 2);
		$ny = ($ny < 0 ? 0 : $ny);
		$nx = round(abs($newWidth - $w) / 2);
		$nx = ($nx < 0 ? 0 : $nx);
		$new = imagecreatetruecolor($newWidth, $newHeight);
		$white = imagecolorallocate($new, 255, 255, 255);
		imagefill($new, 0, 0, $white);
		/* Check quality option. If quality is greater than 100, return error */ 
		if ($quality > 100) {
			echo 'The maximum quality is 100. Quality changes only affect JPEG images.';
		}
		else {
			switch ($size['mime']) {
				case 'image/jpeg':
					$im = imagecreatefromjpeg($source);
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagejpeg($new, $destination, $quality); // Output the new JPEG
					break;
				case 'image/gif':
					$im = imagecreatefromgif($source);
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagegif($new, $destination, $quality); // Output the new GIF
					break;
				case 'image/png':
					$im = imagecreatefrompng($source); 
					imagecopyresampled($new, $im, $nx, $ny, 0, 0, $w, $h, $size[0], $size[1]); // Resample the original JPEG
					imagepng($new, $destination, 9); // Output the new PNG 
					break;
			}
			imagedestroy($im);
		} // end of if - else
	} // end of if-else of file exits
} // end of fuction saveoutputimage()

function getStepName($step)
	{
		switch ($step)
		{
		case 1:
			return "Ad Details";
		case 2:
			return "Ad Preview";
		case 3:
			return "Payment Details";
		case 4:
			return "Confirmation";
		default:
			return "Unknown";
		}
	}
	function getStepWidth($step)
	{
		switch ($step)
		{
		case 1:
			return 100;
		case 2:
			return 145;
		case 3:
			return 110;
		case 4:
			return 140;
		case 5:
			return 120;
		default:
			return 0;
		}
	}	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>