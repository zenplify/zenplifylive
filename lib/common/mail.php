<?php
class Mail
{
    var $to;
    var $subject;
    var $body;
	var $mimeVersion;
	var $contentType;
	var $cc;
	var $bcc;
	
    function Mail($to="", $from="", $cc="", $bcc="", $subject="", $body="", $mimeVersion="1.0", $contentType="text/plain")
    {
		$this->to = $to;
		$this->subject=$subject;
		$this->body=$body;
		$this->mimeVersion=$mimeVersion;
		$this->contentType=$contentType;
		$this->from=$from;
		$this->cc=$cc;
		$this->bcc=$bcc;
    }

    function setTo($to)
    {
        $this->to = $to;
    }
    function setMimeVersion($mimeVersion)
    {
		$this->mimeVersion=$mimeVersion;
	}
    function setContentType($contentType)
    {
		$this->contentType=$contentType;
    }
    function setFrom($from)
    {
		$this->from=$from;
    }
    function setCc($cc)
    {
		$this->cc=$cc;
    }
    function setBcc($bcc)
    {
		$this->bcc=$bcc;
	}
    function setSubject($subject)
    {
        $this->subject = $subject;
    }
    function setBody($body)
    {
        $this->body = $body;
    }
    function send()
    {
		$headers="";
		$headers .= "MIME-Version: ".$this->mimeVersion." \r\n";
		$headers .= "Content-type: ".$this->contentType."; charset=iso-8859-1\r\n";
		$headers .= "From: ".$this->from." \r\n";
		$headers .= "Cc: ".$this->cc." \r\n";
		$headers .= "Bcc: ".$this->bcc." \r\n";
		mail($this->to, $this->subject, $this->body, $headers);
    }
}
?>