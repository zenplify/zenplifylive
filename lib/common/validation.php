<?php
class Validation 
{
	function isValidLogin($login)
	{
		$validString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
		if (strlen($login)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($login); $i++)
		{
		 if(strrpos($validString,substr($login,$i,1))===false)
			 return  false;
		}
		return 	true;
	}
	function isValidEmail($email)
	{
		$email = trim($email);
		$validString="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@._-";
		if(strpos($email,"@")!=strrpos($email,"@")|| strpos($email,"@")===false ||  strpos($email,"@")==0 ||strpos($email,".")===false  ||strpos($email,".")==0 || strpos($email,"@")+1==strpos($email,".") || strrpos($email,".")==strlen($email)-1)
		{
			return false;
		}
		for ($i=0;$i<strlen($email); $i++)
		{
		  if (strrpos($validString,substr($email,$i,1))===false)
		  	return false;
		}
		return true;
	}
	function isEmpty($str)
    {
		return strlen(trim($str)) == 0;
    }
   	function isValidMonth($month)
   	{
   		if (!is_numeric($month))
	 	{
			return false;
		}
		if ((int)$month > 12 || (int)$month <= 0)
	 	{
		 	return false;
		}
		return true;
	}
	function isValidDay($day)
   	{
		if (!is_numeric($day))
	 	{
			return false;
		}
		if ((int)$day > 31 || (int)$day <= 0)
		{
		 	return false;
		}
		return true;
	}
   	function isValidYear($year)
   	{
		if (!is_numeric($year))
		{
			return false;
		}
		if ((int)$year < 1800)
		{
		 	return false;
		}
		return true;
	}
    function isPreviousMonth($month, $year)
    {
        return ($month < (date('m') + 1)) && (date('Y')==$year);
    }
    function isValidCreditCardNumber($ccno)
    {
        $validString="1234567890-";
        for ($i=0; $i < strlen($ccno); $i++)
        {
			 if(strrpos($validString,substr($ccno,$i,1))===false)
                return false;
        }
        return true;
    }
	function isValidDate($month,$day,$year)
	{
		if (($year%4==0 && $day>29 && $month==2)||($year%4!=0 && $day>28 && $month==2) ||(($month==4 || $month==6 || $month==9 || $month==11) && $day>30) )
		{
		 	return false;
		}
		return true;
   	}
	function isNumeric($value)
 	{
        $validString="1234567890";
		if (strlen($value)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($value); $i++)
		{
		 if(strrpos($validString,substr($value,$i,1))===false)
			 return  false;
		}
		return 	true;
	}
	 function isFloat($value)
	 {
	    $validString="1234567890.";
		if (strlen($value)==0)
		{
			return false;
		}
		for ( $i=0;$i<strlen($value); $i++)
		{
		 if (strrpos($validString,substr($value,$i,1))===false)
			 return  false;
		}
		if (strpos($value,".")!=strrpos($value,".") || substr($value,0,1)=="." || substr($value,strlen($value)-1,1)==".")
		{
		 return false;
		}
		return 	true;
	 }
	 function isValidImage($type)
	 {
			if($type == NULL || trim($type) == "")
				return false;
			$imageTypes=".gif,.jpeg,.jpg,.jpe,.png";
				return strpos($imageTypes ,strtolower($type)) !== false;
	 }
}
?>
