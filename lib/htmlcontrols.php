<?php
class HtmlControls
{
	var $panelWidth;
	var $pagingLinksWidth;
    var $dataStyle;
    var $errorDataStyle;
	var $panelAlignment;
    var $currentDataStyle;
	
	function HtmlControls() {
		$this->panelWidth=440;
		$this->pagingLinksWidth=780;
		$this->dataStyle = "tbldata2,tbldata3";
		$this->errorDataStyle = "tblerrordata";
		$this->panelAlignment="center";
		$this->currentDataStyle = "";
	}
	
	function setPanelWidth($panelWidth) {
		$this->panelWidth=$panelWidth;
	}

	// page title 
	function getPageTitle($pageTitle) { 
	
		return '';
		$contents .=  "<div class=\"pageTitle\">";
		$contents .=  "<h1>".$pageTitle."</h1>";
		$contents .=  "</div>";
		$contents .= '<div class="clr"></div>';		
		return $contents;
	}
	
	function getErrorDataStyle() {
		return $this->errorDataStyle;
	}
	
	function setPanelAlignment($panelAlignment) {
		$this->panelAlignment = $panelAlignment;
	}
	
	function getFixedPanelHeader($title="")
	{
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td align=\"center\">";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td class=\"background3\" height=\"2\"><img src=\"../images/trans.gif\"></td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td height=\"26\" class=\"paneltitle\">&nbsp;&nbsp;$title</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		return $contents;
	}
	
	// Panel footer
	function getFixedPanelFooter($footer="") {
		$contents="";
		$contents .=  "<img src=\"../images/trans.gif\" width=\"1\" height=\"5\"><br>";
		$contents .=  "<table align=\"".$this->panelAlignment."\" width=\"".$this->panelWidth."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$contents .=  "<tr>";
		$contents .=  "<td class=\"background3\" height=\"2\"><img src=\"../images/trans.gif\"></td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		$contents .=  "</td>";
		$contents .=  "</tr>";
		$contents .=  "</table>";
		return $contents;
	}
	function getPanelHeader($title="") {

    	$contents .= '<div class="mconredline">';
		$contents .= '<div class="context">'.$title.'</div>';
		$contents .= '</div>';
		
		
		/*
		$contents .= '<div class="panelHead">';
		$contents .= '<div class="corLeft"></div>';
		$contents .= '<h2 style="text-align:left">'.$title.'</h2>';
		$contents .= '<div class="corRight"></div>';
		$contents .= '</div>';
		$contents .= '<div class="clr"></div>';
		*/

		return $contents;
	}
	// Panel footer
	function getPanelFooter($footer="") {
		
		$contents .= '<div class="mconredlinefoot"></div>';
		/*
		$contents .= '<div class="panelFoot">';
		
		$contents .= '<div class="panelFootContents">';
		*/
		
		$contents .= $footer;
		/*
		$contents .= '</div>';		
	
		$contents .= '</div>';
		
		
		$contents .= '<div class="clr"></div>';		
		/*
		if (strlen($footer)>0) {
			$contents .=  "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$contents .=  "<tr>";
			$contents .=  "<td width=\"100%\" >$footer</td>";
			$contents .=  "</tr>";
			$contents .=  "</table>";
		}
		*/
		return $contents;
	}

	// rigth aligned buttons 
	function showButtons($buttons) {
	    $contents  = "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" align=\"center\">";
	    $contents .= "<tr>";
	    $contents .= "<td align=\"right\">" . $buttons . "</td>";
	    $contents .= "</tr>";
	    $contents .= "</table>";
	    $contents .= "<img src=\"". $GLOBALS["IMAGES_URL"] ."trans.gif\" width=\"1\" height=\"5\"><br>";
	    return $contents;
	}
	// set paging links width
	function setPagingLinksWidth($pagingLinksWidth) {
		$this->pagingLinksWidth=$pagingLinksWidth;
	}

	// show database paging links
	function showPagingLinks($database) {
		$contents .= '<div class="PaginationLinks">';
		$contents .= '<div class="PagesCounter">'.$database->writePagesCounter().'</div>';
		$contents .= '<div class="PagesLinks">'.$database->writePagesLinks().'</div>';
		$contents .= '<div class="clr"></div>';
		$contents .= '</div>';		
		return $contents;
    }

	function showFixedPagingLinks($database) {
        return $database->isRecordFound()? "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"".$this->pagingLinksWidth."\" align=\"".$this->panelAlignment."\"><tr><td height=\"20\" valign=\"bottom\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"right\" border=\"0\"><tr><td>" . $database->getBackLink("<img src=\"". $GLOBALS["IMAGES_URL"] ."back.gif\" border=\"0\">") . "&nbsp;</td><td class=\"linksstyle1\">" . $database->getPagesLinks() . "&nbsp;</td><td>" . $database->getNextLink("<img src=\"". $GLOBALS["IMAGES_URL"] ."next.gif\" border=\"0\">") . "</td></tr></table></td></tr><tr><td class=\"linksstyle1\" align=\"right\">showing  " . $database->getStartingRecordsNo() . " - " . $database->getEndingRecordsNo() . " of " . $database->getTotalRecords() . "</td></tr></table>" : "";
    }
	function setDataStyle($dataStyle) {
        $this->dataStyle = $dataStyle;
    }
   	function resetStyle() {
        $this->currentDataStyle = "";
    }
    function getDataStyle() {
        if (strlen($this->currentDataStyle) == 0)
            $this->getNextDataStyle();
        return $this->currentDataStyle;
    }
    function getNextDataStyle() {
        if (strlen($this->currentDataStyle) == 0) {
			$this->currentDataStyle = "tbldata2";
        }
		else if(strcmp($this->currentDataStyle, "tbldata2") == 0) {
			$this->currentDataStyle = "tbldata3";
		}
		else {
			$this->currentDataStyle = "tbldata2";
		}
    }
	function getRichTextEditor($formName, $name, $value, $width="460px", $height="520px") {
		//=============================== Detect  Browser ===============================//
		$contents .= "<script language=\"javascript\" >";
		$contents .= "if (!browser.isIE6up) {";
		$contents .= "document.write('<font class=error>Internet Explorer 6 or greater must be installed on this system to use the advanced HTML editor.</font>');";
		$contents .= "}";
		$contents .= "</script>\n";
		//=============================== Editor Style Sheet ===============================//
		$style="";
		if (strpos(strtolower($value),"<style>")===false) {
			$style.="<style>";
			$style.=".normal1 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="font-weight: normal;";
			$style.="color: #047631;";
			$style.="}";
			$style.=".normal2 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="font-weight: normal;";
			$style.="color: #2B2B2B";
			$style.="}";
			$style.=".normal3 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="font-weight: normal;";
			$style.="color: #D71920";
			$style.="}";
			$style.=".normal4 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="font-weight: normal;";
			$style.="color: #93C150";
			$style.="}";
			$style.=".heading1 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 12px;";
			$style.="font-weight: bold;";
			$style.="color: #047631;";
			$style.="}";
			$style.=".heading2 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 12px;";
			$style.="font-weight: bold;";
			$style.="color: #2B2B2B";
			$style.="}";
			$style.=".heading3 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 12px;";
			$style.="font-weight: bold;";
			$style.="color: #D71920";
			$style.="}";
			$style.=".heading4 {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 12px;";
			$style.="font-weight: bold;";
			$style.="color: #93C150";
			$style.="}";
			$style.=".links1,.links1 a:link,.links1 a:active,.links1 a:visited {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="color: #047631;";
			$style.="text-decoration: underline;";
			$style.="}";
			$style.=".links1 a:hover {";
			$style.="text-decoration: none;";
			$style.="}";
			$style.=".links2,.links2 a:link,.links2 a:active,.links2 a:visited {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="color: #2E3092;";
			$style.="text-decoration: underline;";
			$style.="}";
			$style.=".links2 a:hover {";
			$style.="text-decoration: none;";
			$style.="}";
			$style.=".links3,.links3 a:link,.links3 a:active,.links3 a:visited {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="color: #D71920;";
			$style.="text-decoration: underline;";
			$style.="}";
			$style.=".links3 a:hover {";
			$style.="text-decoration: none;";
			$style.="}";
			$style.=".links4,.links4 a:link,.links4 a:active,.links4 a:visited {";
			$style.="font-family: Arial, Helvetica, sans-serif;";
			$style.="font-size: 11px;";
			$style.="color: #93C150;";
			$style.="text-decoration: underline;";
			$style.="}";
			$style.=".links4 a:hover {";
			$style.="text-decoration: none;";
			$style.="}";
			$style.=".border1 {";
			$style.="border: 1px solid #D3E8B4;";
			$style.="}";
			$style.=".border2 {";
			$style.="border: 1px solid #C3C3C3;";
			$style.="}";
			$style.=".border3 {";
			$style.="border: 1px solid #007330;";
			$style.="}";
			$style.=".border4 {";
			$style.="border: 1px solid #2E3092;";
			$style.="}";
			$style.=".bg1 {";
			$style.="background-color: #EFF8E5;";
			$style.="}";
			$style.=".bg2 {";
			$style.="background-color: #F8F8F8";
			$style.="}";
			$style.="</style>";
		}
		$value=$style.$value;
		$contents = "";
		$contents .= "<input type=\"hidden\" name=\"".$name."\" value=\"\">\n";
		$contents .= "<script language=\"javascript\">\n";
		$contents .= "var editor;\n";
		$contents .= ("function setValues()\n");
		$contents .= ("{\n");
		$contents .= ($formName . ".". $name .".value=document.getElementById('editorFrame').contentWindow.editGetHtml();\n");
		$contents .= ("}\n");
		// Function to load HTML
		$contents .= ("function editOnEditorLoaded(objEditor, id) {\n");
		$contents .= ("// save editor object for later use\n");
		$contents .= ("editor = objEditor;\n");
		$contents .= "var html='';\n";
		$tok = split("\r\n", $value);
		for ($j = 0; $j<count($tok); $j++) {
			$contents .= "html+='".addslashes($tok[$j])."';\n";
		}
		$contents .= ("// write content to editor\n");
		$contents .= ("editor.editWrite(html);\n");
		$contents .= ("}\n");
		$contents .= "</script>\n";
		//=============================== Editor Frame ===============================//
		$contents .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" height=\"400\" id=\"parenttable\">";
		$contents .= "<tr>";
		$contents .= "<td width=\"100%\" height=\"400\">";
		$contents .= "<IFRAME id=\"editorFrame\" style=\"WIDTH: 100%; HEIGHT: 100%\" src=\"editor/pinEdit.html\" frameborder=\"0\"></IFRAME>";
		$contents .= "</td>";
		$contents .= "</tr>";
		$contents .= "</table>";
		return $contents;
	}
}
?>