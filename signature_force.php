<?php
/**
 * Created by PhpStorm.
 * User: Namoos
 * Date: 10/14/2015
 * Time: 11:03 AM
 */

require_once("classes/init.php");

$database = new database();

// get users
$users = getAllUsers($database);
// insert in signature table
foreach ($users as $user) {
    $signature= getSignatures($database,$user->userId);
    addSignature($database,$signature,$user->userId);

}
// get all users
function getAllUsers($database) {

    $sql="SELECT userId from `user` where userId ='3868'";
	return $database->executeObjectList($sql);
}


// get old signature
function getSignatures($database, $userId)
{
    $details = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.leaderId,u.userId from user u where u.userId='" . $userId . "'");
    $consultantTitleUserId = '';
    if ($details->leaderId == 0)
    {
        $consultantTitleUserId = $details->userId;
    }
    else
    {
        $consultantTitleUserId = $details->leaderId;
    }
    $consultantTitle = $database->executeObject("SELECT `fieldCustomName`,`status` FROM `leaderdefaultfields` WHERE userId='" . $consultantTitleUserId . "' AND `fieldMappingName`='consultantId' ");
    if (empty($consultantTitle->fieldCustomName) || ($details->userId == '3736' || $details->leaderId == '3736' || $details->userId == '236' || $details->leaderId == '236'))
    {
        // arbonne logo for VENVPS and rebeccawest users
        if (($details->userId == '236' || $details->leaderId == '236') ||($details->userId == '3736' || $details->leaderId == '3736')) {
            $arbonLogo = '<img src="https://zenplify.biz/images/GreenLogo1x1.png">';
            $arbonsLogo='<div style="float:left;">' . $arbonLogo . '</div>';
        }
    }
    if ($details->userId == '236' || $details->leaderId == '236') {
        $arbonneCunsultancey='Arbonne Consultant';
    }

    $signature = '<span style="font-size:14px;">Best Regards,</span><br /><span style="font-size:14px;">' . $details->firstName . '</span>
			<div>'.$arbonsLogo.'<div style="float:left;font-size:12px;">' . $details->firstName . ' ' . $details->lastName;


    if ($details->title != '')
        $signature = $signature . ', ' . $details->title;
    if (strpos($details->webAddress, "http://") >= 0 || strpos($details->webAddress, "https://") >= 0)
        $web = $details->webAddress;
    else
        $web = "http://" . $details->webAddress;
    if (strpos($details->facebookAddress, "http://") >= 0 || strpos($details->facebookAddress, "https://") >= 0)
        $facebook = $details->facebookAddress;
    else
        $facebook = "http://" . $details->facebookAddress;
    $webNew = preg_replace('#^https?://#', '', $web);
    $fbNew = preg_replace('#^https?://#', '', $facebook);



    $consultantTitleId= $details->consultantId;



    $signature = $signature . '<br /><span style="color: #2E6A30;font-size:12px;">'.$arbonneCunsultancey.'' . $consultantTitleId . '</span><br /><span style="color: #2E6A30;font-size:12px;"><a style="color: #2E6A30;font-size:12px;" href="' . $web . '" target="_blank">' . $webNew . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $facebook . '" target="_blank">' . $fbNew . '</a></div></div>';

    return $signature;
}


// insert signature in new table
function addSignature($database,$signature,$userId){

   return $addSignature= $database->executeNonQuery("INSERT INTO `usersignature` (userId,signature) VALUE ('".$userId."','".mysql_real_escape_string($signature)."')");

}
echo 'complete';