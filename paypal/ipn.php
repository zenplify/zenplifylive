<?php


// STEP 1: Read POST data
 
// reading posted data from directly from $_POST causes serialization 
// issues with array data in POST
// reading raw POST data from input stream instead. 
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
  $keyval = explode ('=', $keyval);
  if (count($keyval) == 2)
     $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
   $get_magic_quotes_exists = true;
} 
foreach ($myPost as $key => $value) {        
   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
        $value = urlencode(stripslashes($value)); 
   } else {
        $value = urlencode($value);
   }
   $req .= "&$key=$value";
}
 
 
// STEP 2: Post IPN data back to paypal to validate
// https://www.paypal.com/cgi-bin/webscr

 
$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
// In wamp like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
// of the certificate as shown below.
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if( !($res = curl_exec($ch)) ) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);
 
 $to1 = 'atif@suavesolutions.net'; //to address
	$subject1 = "payment varification status";
	$message1 =$res ;
	$from1 = "<support@zenplify.com>";
	$headers1 = "From:".$from1 ;
	
	
	 mail($to1, $subject1, $message1, $headers1) ;
 
// STEP 3: Inspect IPN validation result and act accordingly
 
if (strcmp ($res, "VERIFIED") == 0) {
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment
 
    // assign posted variables to local variables
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
    $item_name = $_POST['item_name'];
	$item_number = $_POST['item_number'];
    $payment_status = $_POST['payment_status'];
    $payment_amount = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
	$payment_date =$_POST['payment_date']; 
    $txn_id = $_POST['txn_id'];
    $receiver_email = $_POST['receiver_email'];
    $payer_email = $_POST['payer_email'];
	$payer_id = $_POST['payer_id'];
	$payer_status = $_POST['payer_status'];
	$custom=$_POST['custom'];
	$startDate=date("m/d/Y");
	$expirydate=Date('m/d/Y', strtotime("+ 7 days"));
	
	$postString = '';
	foreach ($_POST as $k => $v) {
		$postString.= $k." = ".$v."\n";
	}
	
	if($payment_amount<20){
		$to_trial = 'atif@suavesolutions.net'; //to address
		$subject_trial = "Welcome to Zenplify Trial";
		$message_trial ="Hi ".$userDetail->firstName.","."\n\nThank you for verifying your PayPal account. Your 7-Day Free Trial is now active and will expire on ".$expirydate." \n\nSincerely,\n\nZenplify Team  ---- \n\n Payer ID = ".$payer_id."\n\nCustom = ".$custom."\n\nPOST\n\n".$postString;
		$from_trial = "<support@zenplify.biz>";
		$headers_trial = "From:".$from_trial ;
		 mail($to_trial, $subject_trial, $message_trial, $headers_trial) ;

		}else{
			$to_pay = 'atif@suavesolutions.net';//to address
			$subject_pay = "Zenplify Payment Alert";
		$message_pay ="Hi ".$userDetail->firstName.","."\n\nYour Payment for the Zenplify Subscription  has been charged by PayPal.Details are below:\n\nPlan Name: ".$item_name." \nAmount: $".$payment_amount."\nStart Date: ".$startDate ."\nExpiry Date: ".$payment_date."\n\nSincerely,\n\nZenplify Team"."\n\nPOST\n\n".$postString;
			$headers_pay = "From: <support@zenplify.biz>" ;
			 mail($to_pay, $subject_pay, $message_pay, $headers_pay) ;

			}

	 
	
} else if (strcmp ($res, "INVALID") == 0) {
    // log for manual investigation
}
?>