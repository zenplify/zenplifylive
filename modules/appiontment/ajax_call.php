
<?php
include('../../classes/init.php');
include('../../classes/task.php');

$database = new database();

 $action=$_GET["action"];
if($action=="add")
{
	$userId			=  $_POST['userId'];	
 	$title			=	$_POST['name'];
	$location 		=$_POST['location'];
	($_POST['allDayEvent']=='on'?$allDayEvent=1:$allDayEvent=0);
	
	$startDate= date('Y-m-d', strtotime($_POST['startDate']));
	$startTime= date('H:i:s', strtotime($_POST['startTime']));
	$startDate=$startDate.' '.$startTime;
	$endDate= date('Y-m-d', strtotime($_POST['endDate']));
	$endTime=date('H:i:s', strtotime($_POST['endTime']));
	$endDate=$endDate.' '.$endTime;
	$notes			=	$_POST['notes'];
	$parentId		=  $_POST['parentId'];
	$stdate		=  $_POST['stdate'];
	$eddate		=  $_POST['eddate'];
	
	//reminder
	
	$reminder		=	$_POST['reminder'];
	$reminderValue	=	$_POST['reminderQuantity'];
	$reminderValueType	=	$_POST['reminderUnit'];
	$contactId     =$_POST['contact_id'];
	switch($reminderValueType){
		case 'minutes':
			  $NumberOfMinutes=$reminderValue;
			  break;
		case 'hours':
			  $NumberOfMinutes=$reminderValue*60;
			break;
		case 'days':
		 	 $NumberOfMinutes=$reminderValue*60*24;
			break;
		default:
		 	$NumberOfMinutes=0;
		 }
	
	$appiontmentOccur			=	$_POST['occur'];
	$appiontmentOccurFrequency	=	$_POST['frequency'];
	//custom year months data for occur
	$appiontmentOccurYearly	=	$_POST['everyyear'];
	$yearMonths			=	explode(',',$_GET['yearMonth']);
	//custom month data for occur
	$appiontmentOccurMonthly	=	$_POST['everyMonth'];
	$monthDays 		    =	explode(',',$_GET['monthDay']);
	$monthsWeek			=	$_POST['monthsWeek'];
	$monthsWeekDays		=	$_POST['monthsWeekDays'];
	//custom week data for occur
	$appiontmentOccurWeekly	=	$_POST['everyWeeks'];
	$weekDays=array($_POST['sun'],$_POST['mon'],$_POST['tues'],$_POST['wed'],$_POST['thr'],$_POST['fri'],$_POST['sat']);
	//custom daily data for occur
	$appiontmentOccurDaily		=	$_POST['everyDay'];
	$notOnWeekEnd		=	$_POST['notOnWeekEnd'];
	if($notOnWeekEnd=='on'){$isNotweekEnd=1;}else{$isNotweekEnd=0;}
	
	
	switch($appiontmentOccurFrequency){
		case 1:
			$every=(empty($appiontmentOccurDaily)?1:$appiontmentOccurDaily);
			break;
		case 2:
			$every=(empty($appiontmentOccurWeekly)?1:$appiontmentOccurWeekly);
			break;
		case 3:
		 	$every=(empty($appiontmentOccurMonthly)?1:$appiontmentOccurMonthly);
			break;
		case 4:
			$every=(empty($appiontmentOccurYearly)?1:$appiontmentOccurYearly);
			break;
		default:
		 	$every=0;
		 }
	
	
	
		if(!empty($_POST['endingOn'])){
		  $endingOn = date('Y-m-d', strtotime($_POST['endingOn']));
	     }else{
		   $endingOn="";
		}
	
		$contact			=	$_POST['contact'];
		$contactId			=	$_POST['contactId'];
		$addedDate			=	date("Y-n-j");

	//query section for new task
	
	$database->executeNonQuery("INSERT INTO appointments
	(userId, title,isAllDayEvent,startDateTime,endDateTime,endingOn,location,repeatTypeId,customRuleId,notes,reoccurrenceId, addedDate, isActive,parentId,stDate,edDate) 
	VALUES 
	('".$userId	."','".$title."','".$allDayEvent."','".$startDate."','".$endDate."','".$endingOn."','".$location."','".$appiontmentOccur."','0','".$notes."','0','".$addedDate."','0','".$parentId."','".$stdate."','".$eddate."')");	
	
	//this is last inserted id of task
	
	$appionmentId=$database->insertid();
	
	//Insert task reminder 
	
	$database->executeNonQuery("INSERT INTO appointmentsreminder(appointmentId, reminderTypeId, numberOfMinutes, customValue, customUnit) VALUES ('".$appionmentId."','".$reminder."','".$NumberOfMinutes."','".$reminderValue."','".$reminderValueType."')");
	
	//custom occurence 
	switch($appiontmentOccur){
		case 6:
		$database->executeNonQuery("INSERT INTO customrules(frequency, every) VALUES ('".$appiontmentOccurFrequency."','".$every."')");
		//this is last inserted id of task
	$customRuledId=$database->insertid();
	
	if($appiontmentOccurFrequency==1){
		
										
			$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
										VALUES (".$customRuledId.",".$isNotweekEnd.")");	
	}
	if($appiontmentOccurFrequency==2){
			
			$database->executeNonQuery("INSERT INTO customruleweekly
										(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThrusday, isFriday, isSaturday) 
										VALUES 
										(".$customRuledId.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."','".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");
	}
	if($appiontmentOccurFrequency==3){
	
		
		 	$database->executeNonQuery("INSERT INTO customrulemonthly
			(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
			VALUES 
			(".$customRuledId.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
			 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
			 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
			 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
			 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
			 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
			 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
			 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
			 ".$monthsWeek.",".$monthsWeekDays.")");
	}
	if($appiontmentOccurFrequency==4){
	
		
			$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
			VALUES (".$customRuledId.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
			".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
			".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
			".($yearMonths[11]!=0?1:0).")");
		
		 }
		
		break;
		}
		

		$contactIdsForAppiontment=explode(',',$_POST['contactName']);
		
		for($i=0;$i<sizeof($contactIdsForAppiontment);$i++){
		
			$database->executeNonQuery("INSERT INTO appiontmentcontacts(appiontmentId, contactId) VALUES (".$appionmentId.",'".$contactIdsForAppiontment[$i]."')");
			
		}
		$database->executeNonQuery("UPDATE 	appointments SET customRuleId='".$customRuledId."' WHERE appiontmentId='".$appionmentId."'");
	
	
	echo 1;
	
}


if($action=="edit"){
		$userId			=  	$_POST['userId'];	
		$appiontmentId 	=	$_POST['appiontmentId'];
 		$title			=	$_POST['name'];
		$location 		=	$_POST['location'];
		
	($_POST['allDayEvent']=='on'?$allDayEvent=1:$allDayEvent=0);
	
	$startDate= date('Y-m-d', strtotime($_POST['startDate']));
	$startTime= date('H:i:s', strtotime($_POST['startTime']));
	$startDate=$startDate.' '.$startTime;
	$endDate= date('Y-m-d', strtotime($_POST['endDate']));
	$endTime=date('H:i:s', strtotime($_POST['endTime']));
	$endDate=$endDate.' '.$endTime;
	$notes			=	$_POST['notes'];
		//completed date
	
	
	//$dueTime	= 	$_POST['dueTime'];
	$dueDateTime= date('Y-m-d', strtotime($_POST['dueDate'])); // outputs 2006-01-24
	$reminder		=	$_POST['reminder'];
	$reminderValue	=	$_POST['reminderQuantity'];
	$reminderValueType	=	$_POST['reminderUnit'];
	$contactId     =$_POST['contact_id'];
	switch($reminderValueType){
		case 'minutes':
			  $NumberOfMinutes=$reminderValue;
			  break;
		case 'hours':
			  $NumberOfMinutes=$reminderValue*60;
			break;
		case 'days':
		 	 $NumberOfMinutes=$reminderValue*60*24;
			break;
		default:
		 	$NumberOfMinutes=0;
		 }
	
	$taskOccur			=	$_POST['occur'];
	$taskOccurFrequency	=	$_POST['frequency'];
	//custom year months data for occur
	$taskOccurYearly	=	$_POST['everyyear'];
	$yearMonths			=	explode(',',$_GET['yearMonth']);
	//custom month data for occur
	$taskOccurMonthly	=	$_POST['everyMonth'];
	$monthDays 		    =	explode(',',$_GET['monthDay']);
	$monthsWeek			=	$_POST['monthsWeek'];
	$monthsWeekDays		=	$_POST['monthsWeekDays'];
	//custom week data for occur
	$taskOccurWeekly	=	$_POST['everyWeeks'];
	$weekDays=array($_POST['sun'],$_POST['mon'],$_POST['tues'],$_POST['wed'],$_POST['thr'],$_POST['fri'],$_POST['sat']);
	//custom daily data for occur
	$taskOccurDaily		=	$_POST['everyDay'];
	$notOnWeekEnd		=	$_POST['notOnWeekEnd'];
	
	if($notOnWeekEnd=='on'){$isNotweekEnd=1;}else{$isNotweekEnd=0;}
	
	
	
	switch($taskOccurFrequency){
		case 1:
			$every=((empty($taskOccurDaily) || $taskOccurDaily == " ")?1:$taskOccurDaily);
			break;
		case 2:
			$every=((empty($taskOccurWeekly) || $taskOccurWeekly == " ")?1:$taskOccurWeekly);
			break;
		case 3:
		 	$every=((empty($taskOccurMonthly) || $taskOccurMonthly == " ")?1:$taskOccurMonthly);
			break;
		case 4:
			$every=((empty($taskOccurYearly) || $taskOccurYearly == " ")?1:$taskOccurYearly);
			break;
		default:
		 	$every=0;
		 }
	
	if(!empty($_POST['endingOn'])){
		$endingOn = date('Y-m-d', strtotime($_POST['endingOn']));
	}else{
		$endingOn="";
		}
		
	$contact			=	$_POST['contact'];
	//$contactId			=	$_POST['contactId'];
	$addedDate			=	date("Y-n-j");

	//query section for new task
	
	$database->executeNonQuery("UPDATE appointments SET userId='".$userId	."', title='".$title."', isAllDayEvent='".$allDayEvent."', startDateTime='".$startDate."', endDateTime='".$endDate."',endingOn='".$endingOn."',location='".$location."', notes='".$notes."', reoccurrenceId='0', addedDate='".$addedDate."', isActive='0',repeatTypeId='".$taskOccur."' WHERE appiontmentId='".$appiontmentId."'");  
	
$appiontmentToEdit=$database->executeObject("SELECT * FROM  appointments WHERE appiontmentId='".$appiontmentId."'");

	
	
	
	//this is last inserted id of task
	
	//$taskId=$database->insertid();
	
	//Insert task reminder 
	$customRuledId=$appiontmentToEdit->customRuleId;
	
	if($customRuledId==0){
		switch($taskOccur){
		case 6:
		$database->executeNonQuery("INSERT INTO customrules(frequency, every) VALUES ('".$taskOccurFrequency."','".$every."')");
		//this is last inserted id of task
	$customRuledID=$database->insertid();
	
			if($taskOccurFrequency==1){
					$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
												VALUES (".$customRuledID.",".$isNotweekEnd.")");	
				
			}
			if($taskOccurFrequency==2){
				$database->executeNonQuery("INSERT INTO customruleweekly
												(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThrusday, isFriday, isSaturday) 
												VALUES 
												(".$customRuledID.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."','".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");								
				
				
				
			}
			if($taskOccurFrequency==3){
				
							$database->executeNonQuery("INSERT INTO customrulemonthly
					(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
					VALUES 
					(".$customRuledID.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
					 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
					 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
					 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
					 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
					 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
					 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
					 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
					 ".$monthsWeek.",".$monthsWeekDays.")");
				
			}
			if($taskOccurFrequency==4){
					
							$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
					VALUES (".$customRuledID.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
					".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
					".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
					".($yearMonths[11]!=0?1:0).")");
					
				
				 }
		
		break;
		}
		$database->executeNonQuery("UPDATE 	appointments SET customRuleId='".$customRuledID."' WHERE appiontmentId='".$appiontmentId."'");
		//echo ("UPDATE 	appointments SET customRuleId='".$customRuledID."' WHERE appiontmentId='".$appiontmentId."'");
		}else{
			$customRuledToEdit=$database->executeObject("SELECT * FROM  customrules  WHERE customRuleId='".$customRuledId."'");	
	if($taskOccurFrequency != $customRuledToEdit->frequency){
		
			switch($customRuledToEdit->frequency){
				case 1:
				$database->executeNonQuery("DELETE  FROM  customrulesdaily WHERE customRuleId='".$customRuledId."'");
				break;
				case 2:
				$database->executeNonQuery("DELETE  FROM  customruleweekly WHERE customRuleId='".$customRuledId."'");
				break;
				case 3:
				$database->executeNonQuery("DELETE  FROM  customrulemonthly WHERE customRuleId='".$customRuledId."'");
				break;
				case 4:
				$database->executeNonQuery("DELETE  FROM  customrulesyearly WHERE customRuleId='".$customRuledId."'");
				break;
				
				}
			}		
	
		//custom occurence 
	switch($taskOccur){
		case 6:
		$database->executeNonQuery("UPDATE customrules SET frequency='".$taskOccurFrequency."', every='".$every."' WHERE customRuleId='".$customRuledId."'");
		//this is last inserted id of task
	
	
	if($taskOccurFrequency==1){
		if($taskOccurFrequency != $customRuledToEdit->frequency){
			$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
										VALUES (".$customRuledId.",".$isNotweekEnd.")");	
		}else{
			$database->executeNonQuery("UPDATE customrulesdaily SET  isNotweekEnd='".$isNotweekEnd."' WHERE customRuleId='".$customRuledId."'");	
		}
	}
	if($taskOccurFrequency==2){
		if($taskOccurFrequency != $customRuledToEdit->frequency){
		$database->executeNonQuery("INSERT INTO customruleweekly
										(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThrusday, isFriday, isSaturday) 
										VALUES 
										(".$customRuledId.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."','".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");								
		
		
		}else{
			$database->executeNonQuery("UPDATE customruleweekly
										SET  isSunday='".($weekDays[0]=='on'?1:0)."', isMonday='".($weekDays[1]=='on'?1:0)."', isTuesday='".($weekDays[2]=='on'?1:0)."', isWednesday='".($weekDays[3]=='on'?1:0)."', isThrusday= '".($weekDays[4]=='on'?1:0)."', isFriday='".($weekDays[5]=='on'?1:0)."', isSaturday='".($weekDays[6]=='on'?1:0)."' WHERE customRuleId='".$customRuledId."'"); 
		
		}
	}
	if($taskOccurFrequency==3){
		
		if($taskOccurFrequency != $customRuledToEdit->frequency){
					$database->executeNonQuery("INSERT INTO customrulemonthly
			(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
			VALUES 
			(".$customRuledId.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
			 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
			 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
			 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
			 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
			 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
			 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
			 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
			 ".$monthsWeek.",".$monthsWeekDays.")");
		}else{
		 			$database->executeNonQuery("UPDATE customrulemonthly SET isDays='1', is1=".($monthDays[0]!=0?1:0).", is2=".($monthDays[2]!=0?1:0).", is3=".($monthDays[3]!=0?1:0).", is4=".($monthDays[3]!=0?1:0).", is5=".($monthDays[4]!=0?1:0).", is6=".($monthDays[5]!=0?1:0).", is7=".($monthDays[6]!=0?1:0).", is8=".($monthDays[6]!=0?1:0).", is9=".($monthDays[8]!=0?1:0).", is10=".($monthDays[9]!=0?1:0).", is11=".($monthDays[10]!=0?1:0).", is12=".($monthDays[11]!=0?1:0).", is13=".($monthDays[12]!=0?1:0).", is14=".($monthDays[13]!=0?1:0).", is15=".($monthDays[14]!=0?1:0).", is16=".($monthDays[15]!=0?1:0).", is17=".($monthDays[16]!=0?1:0).", is18=".($monthDays[17]!=0?1:0).", is19=".($monthDays[18]!=0?1:0).", is20=".($monthDays[19]!=0?1:0).", is21=".($monthDays[20]!=0?1:0).", is22=".($monthDays[21]!=0?1:0).", is23=".($monthDays[22]!=0?1:0).", is24=".($monthDays[23]!=0?1:0).", is25=".($monthDays[24]!=0?1:0).", is26=".($monthDays[25]!=0?1:0).", is27=".($monthDays[26]!=0?1:0).", is28=".($monthDays[27]!=0?1:0).", is29=".($monthDays[28]!=0?1:0).", is30=".($monthDays[29]!=0?1:0).", is31=".($monthDays[30]!=0?1:0).", weekOfMonth=".$monthsWeek.", dayOfWeek=".$monthsWeekDays." WHERE customRuleId='".$customRuledId."'"); 
		}
	}
	if($taskOccurFrequency==4){
			
			if($taskOccurFrequency != $customRuledToEdit->frequency){
					$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
			VALUES (".$customRuledId.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
			".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
			".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
			".($yearMonths[11]!=0?1:0).")");
			
			}else{
		
					$database->executeNonQuery("UPDATE customrulesyearly SET isJan=".($yearMonths[0]!=0?1:0).", isFeb=".($yearMonths[1]!=0?1:0).", isMar=".($yearMonths[2]!=0?1:0).", isApril=".($yearMonths[3]!=0?1:0).", isMay=".($yearMonths[4]!=0?1:0).", isJune=".($yearMonths[5]!=0?1:0).", isJuly=".($yearMonths[6]!=0?1:0).", isAug=".($yearMonths[7]!=0?1:0).", isSep=".($yearMonths[8]!=0?1:0).",isOct=".($yearMonths[9]!=0?1:0).",isNov=".($yearMonths[10]!=0?1:0).", isDec=".($yearMonths[11]!=0?1:0)." WHERE customRuleId='".$customRuledId."'"); 
			
			}
		 }
		
		break;
		}
		}
	
	$database->executeNonQuery("UPDATE appointmentsreminder SET appointmentId='".$appiontmentId."', reminderTypeId='".$reminder."', numberOfMinutes='".$NumberOfMinutes."', customValue='".$reminderValue."', customUnit='".$reminderValueType."' WHERE appointmentId='".$appiontmentId."'" );
	
	
	
		
		$contactIdsForAppiontment=explode(',',$_POST['contactName']);
		if(empty($contactIdsForAppiontment)){
		$database->executeNonQuery("DELETE FROM appiontmentcontacts  WHERE appiontmentId='".$appiontmentId."'");
			
			}else{
		$database->executeNonQuery("DELETE FROM appiontmentcontacts  WHERE appiontmentId='".$appiontmentId."'");
		
		for($i=0;$i<sizeof($contactIdsForAppiontment);$i++){
			
		$database->executeNonQuery("INSERT INTO appiontmentcontacts(appiontmentId, contactId) VALUES (".$appiontmentId.",'".$contactIdsForAppiontment[$i]."')");
			
			}
			}
	echo '1';
	}
?>