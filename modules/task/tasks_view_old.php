<?php 
error_reporting(0);
session_start();
include_once("../headerFooter/header.php");
require_once ("../../classes/task.php");

$userId=$_SESSION['userId'];

$task = new task();
$tasks = $task->GetAllUserTask($userId);
$thisWeekTask=$task->getThisWeekTask($userId);
$reminderTask=$task->GetPlanReminderTasks($userId);

?>
 <script type="text/javascript" src="../../js/jquery-1.8.3.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

 <script type="text/javascript" charset="utf-8">

			 			  var contactArray=new Array();
   
			$(document).ready( function () {
				
			
			        $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra']})
					.tablesorterPager({container: $("#pager")}); 
	// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function(){
			// When value of the input is not blank
			if( $(this).val() != "")
			{
				// Show only matching TR, hide rest of them
				$("#contact_table tbody>tr").hide();
				$("#contact_table td:contains-ci('" + $(this).val() + "')").parent("tr").show();
			}
			else
			{
				// When there is no input or clean again, show everything back
				$("#contact_table tbody>tr").show();
			}
		});

		
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"], 
	{
	    "contains-ci": function(elem, i, match, array) 
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
	function check(field)
	{
		$("#contact_table tbody>tr").hide();

		var list = new Array();
		var j=0;
	 	for (i=0;i<field.length;i++)
	 		{
				if (field[i].checked==true)
				{	
					list[j]=field[i].value;	
					j++;
					
				}
			
	 		}
		var arrayLength=list.length;
		if(arrayLength!=0)
			{
				for (k=0;k<arrayLength;k++)
	 				{
						$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
	 				}
			}
		else
			{
				$("#contact_table tbody>tr").show();
			}
 		//alert (list.length);		


 	}
	
		    	 
				
	       
			    					
			function deleteTask(id){

				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{deleteId:id,action:'deleteIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
	
            }
				function skipTask(id){

				//alert(id);
				
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{skipId:id,action:'skipIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
				
            }
            </script>
 
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <a href="new_task.php"><input type="button" id="new_task" value=""  /></a>
    
     </div>
       <div class="sub_container">
          <div class="col_table">
             <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
  				<thead>    
                <tr class="header">
                    <th class="first" >&nbsp;Done</th>
                    <th class="center big">Summary <img src="../../images/bg.gif" /></th>
                    <th class="center">Priority <img src="../../images/bg.gif" /></th>
                    <th class="center">Due Date <img src="../../images/bg.gif" /></th>
                    <th class="center">Who <img src="../../images/bg.gif" /></th>
                    <th class="center smallcol">Actions</th>
                    <th class="last"></th>
                </tr>
                </thead>
				<thead>
													<tr>
						    	                		<td class="taskDiff" colspan="7"> Today</td>
									                    
				</thead>					                </tr>
 				<tbody>
				<?php 
				$counttoday=0;
					foreach ($tasks as $row)
					{
						
						//echo "<tr class=\"odd_gradeX\" id=\"2\">";
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						$dateTime=$row->dueDateTime;
						$contactId=$task->getcontactId($id);
						
						
						//$dueDate=date("Y-m-d", strtotime($dateTime));
						
						$todays_date = date("Y-m-d");
						
						//$today = strtotime($todays_date);
						//$expiration_date = strtotime($dueDate);
					if(strtotime($todays_date)==strtotime($dateTime))
						{
							$counttoday++;
							?>
													
													
													<?php 
													echo "<tr class=\"odd_gradeX\" id='task".$id."'>";
													//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
													echo "<td><input type=\"checkbox\"></td>";
													echo "<td><a href=edit_task.php?task_id=".$id.">".$row->title."</td>";
													echo "<td>".$preority->priority."</td>";
													echo "<td>".date("d/m/Y", strtotime($dateTime))."</td>";
													echo "<td>";?>
                                                    <?php foreach($contactId as $ci){
														$task_contact=$task->taskContact($ci->contactId);
														echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}?>
                                                    
													
													<?php "</td>";?>
													
												<td><a href="edit_task.php?task_id=<?php echo $id; ?>">
												<img src="../../images/edit.png" title="Edit" alt="Edit" />
												</a>
												<img onclick=<?php echo ($row->planId==0?"deleteTask('$id')":"skipTask('$id')")?> src="<?php echo ($row->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($row->planId==0?'Archive':'Skip')?>" title="<?php echo ($row->planId==0?'Archive':'Skip')?>" />
												</td>
												<td>&nbsp;</td>
												<?php 	echo "<td></td>";
												echo "</tr>";
						}
					}
					
					
				if($counttoday==0 )
				{?>
				<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>	
					
				<?php }?>
                </tbody>
				<thead>									<tr>
						    	                		<td class="taskDiff" colspan="7"> Overdue - Tasks with deadline in past</td>
									                    
									                </tr>
                </thead>
<tbody>
				<?php 
				$countoverdue=0;
				$nodate='1990-12-12';
					foreach ($tasks as $row)
					{
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						$dateTime=$row->dueDateTime;
						$contactId=$task->getcontactId($id);
						
						//strtotime($dateTime);
						
						//$dueDate=date("Y-m-d", strtotime($dateTime));
						
						$todays_date = date("Y-m-d");
						$no_date = '0000-00-00 00:00:00';
						
						
					//	echo $today = strtotime($todays_date);
						$expiration_date = strtotime($dueDate);
						
					if(strtotime($todays_date)>strtotime($dateTime)&&strtotime($no_date)!=strtotime($dateTime))
						{
							$countoverdue++;
							?>
													
													
													<?php 
													echo "<tr class=\"odd_gradeX\" id='task".$id."'>";
													//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
													echo "<td><input type=\"checkbox\"></td>";
													echo "<td><a href=edit_task.php?task_id=".$id.">".$row->title."</td>";
													echo "<td>".$preority->priority."</td>";
													echo "<td>".( strtotime($dateTime)< strtotime($nodate)?'0000-00-00':date("d/m/Y", strtotime($dateTime)))."</td>";
													echo "<td>";?>
                                                    <?php foreach($contactId as $ci){
														$task_contact=$task->taskContact($ci->contactId);
														echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}?>
                                                    
													
													<?php "</td>";?>
													
												<td><a href="edit_task.php?task_id=<?php echo $id; ?>">
												<img src="../../images/edit.png" title="Edit" alt="Edit" />
												</a>
												<img onclick=<?php echo ($row->planId==0?"deleteTask('$id')":"skipTask('$id')")?> src="<?php echo ($row->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($row->planId==0?'Archive':'Skip')?>" title="<?php echo ($row->planId==0?'Archive':'Skip')?>" />
												</td>
												<td>&nbsp;</td>
												<?php 	echo "<td></td>";
												echo "</tr>";
						}
					}
					
				if($countoverdue==0)
				{?>
				<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>	
					
				<?php }?>
                </tbody>
                <thead>
													<tr>
						    	                		<td class="taskDiff" colspan="7"> General Items - Tasks with no dates</td>
									                    
									                </tr>
                </thead>
                <tbody>
				<?php 
				$countgeneral==0;
					foreach ($tasks as $row)
					{
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						$dateTime=$row->dueDateTime;
					$contactId=$task->getcontactId($id);
						
						$no_date = '0000-00-00 00:00:00';
						
					if(strtotime($no_date)==strtotime($dateTime))
						{$countgeneral++;
													echo "<tr class=\"odd_gradeX\" id='task".$id."'>";
													echo "<td><input type=\"checkbox\"></td>";
													echo "<td><a href=edit_task.php?task_id=".$id.">".$row->title."</td>";
													echo "<td>".$preority->priority."</td>";
													echo "<td>"."	"."</td>";
													echo "<td>";?>
                                                    <?php foreach($contactId as $ci){
														$task_contact=$task->taskContact($ci->contactId);
														echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}?>
                                                    
													
													<?php "</td>";?>
													
												<td><a href="edit_task.php?task_id=<?php echo $id; ?>">
												<img src="../../images/edit.png" title="Edit" alt="Edit" />
												</a>
												<img onclick=<?php echo ($row->planId==0?"deleteTask('$id')":"skipTask('$id')")?> src="<?php echo ($row->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($row->planId==0?'Archive':'Skip')?>" title="<?php echo ($row->planId==0?'Archive':'Skip')?>" />
												</td>
												<td>&nbsp;</td>
												<?php 	echo "<td></td>";
												echo "</tr>";
						}
					}
						
				if($countgeneral==0)
				{?>
				<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>	
					
				<?php }?>
                </tbody>
				<thead>	
				
													<tr>
						    	                		<td class="taskDiff" colspan="7"> Reminder Tasks</td>
									                    
									                </tr>
				</thead>
                <tbody>
				<?php if(empty($reminderTask))
				{?>
				<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>	
					
				<?php }?>
                
				<?php 
					foreach ($reminderTask as $row)
					{
						//echo "<tr class=\"odd_gradeX\" id=\"2\">";
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						//$dateTime=$row->dueDateTime;
						$contactId=$task->getcontactId($id);
						
						
						//	$no_date = '0000-00-00 00:00:00';
						
						
							?>
													
													
													<?php 
													echo "<tr class=\"odd_gradeX\" id='task".$id."'>";
													//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
													echo "<td><input type=\"checkbox\"></td>";
													echo "<td><a href=edit_task.php?task_id=".$id.">".$row->title."</td>";
													echo "<td>".$preority->priority."</td>";
													echo "<td>"."	"."</td>";
													echo "<td>";?>
                                                    <?php foreach($contactId as $ci){
														$task_contact=$task->taskContact($ci->contactId);
														echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}?>
                                                    
													
													<?php "</td>";?>
													
												<td><a href="edit_task.php?task_id=<?php echo $id; ?>">
												<img src="../../images/edit.png" title="Edit" alt="Edit" />
												</a>
												<img onclick=<?php echo ($row->planId==0?"deleteTask('$id')":"skipTask('$id')")?> src="<?php echo ($row->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($row->planId==0?'Archive':'Skip')?>" title="<?php echo ($row->planId==0?'Archive':'Skip')?>" />
												</td>
												<td>&nbsp;</td>
												<?php 	echo "<td></td>";
												echo "</tr>";
						
					}
					
					
				?>
				</tbody>
               </table>
                <table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="float:right; margin-top:10px;">
                               
                                   
                                    <div class="pager" id="pager">
	<form>
		<img class="first_page" src="../../images/paging_far_left.gif" />
		<img class="prev_page" src="../../images/paging_far_left.gif" />
		<input type="text" class="pagedisplay" value="1/1" />
		<img class="next_page" src="../../images/paging_far_right.gif" />
		<img class="last_page" src="../../images/paging_far_right.gif" />
		
        <select id="pagesize" name="pagesize" class="pagesize"><option value="50" selected="selected">50</option><option value="100">100</option><option value="250">250</option></select>       
	</form>
</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                
       		</div>
           <div class="col_search">
                <p class="search_header">Find Tasks</p>
           		<input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
                 <br />
                
                
                 <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <!--
                <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <img src="images/check.png" /> Include Custome Fields-->
               <!-- <p class="search_header small">By the Group <img src="images/arrow.png" class="arrow" /></p>-->
           </div>
           
       </div>
      
     <?php 
		include_once("../headerFooter/footer.php");
	?>