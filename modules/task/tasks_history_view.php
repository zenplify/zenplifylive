<?php 
error_reporting(0);
session_start();
include_once("../headerFooter/header.php");
require_once ("../../classes/task.php");

$userId=$_SESSION['userId'];

$task = new task();

$tasks = $task->GetAllUserTaskHistory($userId);
?>
 
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

 <script type="text/javascript" charset="utf-8">
			
			$(document).ready( function () {
				 $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
					.tablesorterPager({container: $("#pager"),size:50}); 
			//UncheckAll();
					//get_group_contacts();
			       /* $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra']})
					.tablesorterPager({container: $("#pager"),size:50}); 
			      $("#pagesize").change(function(){
						
						
						
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {action:'pagination',records:$(this).val(),userId:<?php //echo $userId;?>},
   success: function(data){
	   $("#tableBody").html(data)
	   
	  
   }
 });
						});*/
			});
			
   var contactArray=new Array();
   $(document).ready(function(){
		// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function(){
			// When value of the input is not blank
			if( $(this).val() != "")
			{
				// Show only matching TR, hide rest of them
				$("#contact_table tbody>tr").hide();
				$("#contact_table td:contains-ci('" + $(this).val() + "')").parent("tr").show();
			}
			else
			{
				// When there is no input or clean again, show everything back
				$("#contact_table tbody>tr").show();
			}
		});

		
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"], 
	{
	    "contains-ci": function(elem, i, match, array) 
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
	function check(field)
	{
		$("#contact_table tbody>tr").hide();

		var list = new Array();
		var j=0;
	 	for (i=0;i<field.length;i++)
	 		{
				if (field[i].checked==true)
				{	
					list[j]=field[i].value;	
					j++;
					
				}
			
	 		}
		var arrayLength=list.length;
		if(arrayLength!=0)
			{
				for (k=0;k<arrayLength;k++)
	 				{
						$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
	 				}
			}
		else
			{
				$("#contact_table tbody>tr").show();
			}
 		//alert (list.length);		


 	}
		function deleteTask(id){

				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{deleteId:id,action:'deleteIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
	
            }
				function skipTask(id){

				alert(id);
				
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{skipId:id,action:'skipIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
				
            }
    
			</script>
 
<!--  <div id="menu_line"></div>-->
  <div class="container">

     <div class="top_content">
     <h1 class="gray">Tasks History</h1>
    
     </div>
       <div class="sub_container">
          <div class="col_table">
             <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
  				<thead>    
                <tr class="header">
                    <th class="first" >&nbsp;</th>
                    <th class="center big">Summary <img src="../../images/bg.gif" /></th>
                    <th class="center">Priority <img src="../../images/bg.gif" /></th>
                    <th class="center">Completed Date <img src="../../images/bg.gif" /></th>
                    <th class="center">Who <img src="../../images/bg.gif" /></th>
                    <th class="center smallcol">Actions</th>
                    <th class="last"></th>
                </tr>
                </thead >
				<tbody id="tableBody">
				
                <?php
				
				 if(empty($tasks))
				{?>
				<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>	
					
				<?php }
				$nodate='1990-12-12';
				foreach ($tasks as $row)
					{
						//echo "<tr class=\"odd_gradeX\" id=\"2\">";
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						$contactId=$task->getcontactId($id);
						$dateTime=$row->completedDate;
						echo "<tr class=\"odd_gradeX\" id='task".$id."'>";
							//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
							echo "<td>&nbsp;</td>";
							echo "<td><a class='cross' href='edit_task.php?task_id=".$id."'>";
							if(strlen($row->title)>22) echo substr($row->title,0,22)."..."; else echo $row->title;
							echo "</a></td>";
							echo "<td class='cross'>".$preority->priority."</td>";
							echo "<td class='cross'>".( strtotime($dateTime)< strtotime($nodate)?'':date("m/d/Y", strtotime($row->completedDate)))."</td><td>";?>
							 <?php foreach($contactId as $ci){
														$task_contact=$task->taskContact($ci->contactId);
														echo "<a class='cross' href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}?>
                                                    
													
													<?php "</td>";?>
                           <td ><a href="edit_task.php?task_id=<?php echo $id; ?>" >
												<img src="../../images/edit.png" title="Edit" alt="Edit" />
												</a>
												<img onclick="deleteTask('<?php echo $id ?>')" src="../../images/Archive-icon.png" alt="Archive" title="Archive" />
												</td>
												<td>&nbsp;</td>
												<?php 	echo "<td></td>";
												echo "</tr>";
						
					}
				?>
				</tbody>
               </table>
                <table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="float:right; margin-top:10px;">
                               
                                   <div id="pager" class="pager" style="float:right; margin-top:5px; ">
			<form>
				<img src="../../images/paging_far_left.gif" class="first"/>
					<img src="../../images/paging_left.gif" class="prev"/>
					<input type="text" class="pagedisplay"/>
					<img src="../../images/paging_right.gif" class="next"/>
					<img src="../../images/paging_far_right.gif" class="last"/>
					
					
					<select id="pagesize" name="pagesize" class="pagesize">
						
						<option value="50">50 </option>
						<option value="100">100 </option>
						<option value="250">250 </option>
					
						
					</select>
			</form>
		
		</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                
       		</div>
           <div class="col_search">
                <p class="search_header">Find Tasks</p>
           		<input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
                 <br />
                
               
           </div>
           
       </div>
      
     <?php 
		include_once("../headerFooter/footer.php");
	?>