<?php 
error_reporting(0);
session_start();

include_once("../headerFooter/header.php");
include('../../classes/task.php');
include('../../classes/contact.php');



//include('../../modules/login_process/logincheck.php');

$task =new task();
$contact= new contact();
$contact_id=$_GET['contact_id'];
//$contactId= $_GET['contactId'];
$taskPriority	=	$task->taskPriority();
$reminder		=	$task->reminder();
$occur			=	$task->occur();

?>

	<script type="text/javascript" src="../../js/src/jquery.tokeninput.js"></script>

    <link rel="stylesheet" href="../../css/styles/token-input.css" type="text/css" />
        <script src="../../js/zen_jscript.js" type="text/javascript"></script>
      <script type="text/javascript" src="../../js/script.js"  ></script>
	
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />

<!--    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>-->
<!--    <script src="../../js/timepicker/jquery-ui-timepicker-addon.js"></script>-->
    <script src="../../validation/jquery.validate.js" ></script>
     
      
      <script>
      $(document).ready(function() {
		 
        $('#dueTime').timepicker({
			hourGrid: 4,
			minuteGrid: 10,
			timeFormat: 'hh:mm tt'
			
			});

        $( "#dueDate" ).datepicker({
			dateFormat:"mm/dd/yy"
			});
		
        $( "#endDate" ).datepicker();
		

        $( "#completeDate" ).datepicker();
        $( "#completeTime" ).timepicker({
			hourGrid: 4,
			minuteGrid: 10,
			timeFormat: 'hh:mm tt'
			});
        

		$("#complete").change(function(){

			if ($(this).is(':checked')) {
			    $("#checkdatetime").show();
			} else {
			    $("#checkdatetime").hide();
				$("#changeTheDate").css('display','none');;
				$("#changeTheTime").css('display','none');;
			} 
			});
		$("#changeDate").click(function(){
			
			$("#changeTheDate").toggle();
			$("#changeTheTime").toggle();

			});
        });
      
      </script>
      
<script type="text/javascript">
function addCustom(){
	
	var opt=document.getElementById('reminder').value;
	
	
	
	if(opt==6){
		
		document.getElementById('customField')
		.innerHTML='<input type="text" name="value" style="width:63px;">'+' '+'<select name="reminderValueType"><option value="minutes">Minute(s)</option><option value="hours">Hour(s)</option><option value="days">Day(s)</option></select>';
		}
		else if(opt==1){
		document.getElementById('customField').innerHTML='No Reminder Will Be Generated';
		
		}
		else{
		document.getElementById('customField').innerHTML='Before Activity is due';
			}
	
	}
$(document).ready(function() {
	
   $('#occur').change(function(){
	   var opt=$('#occur').val();
	
	//this work for monthly weekly ,every day ,every week end
	
	 if(opt==1){//if occur once
				$('#otherOccurField').hide();
				$('#customOccurFrequencyField').hide();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				$("#customOccurMonthDaysField").hide();
				$("#customOccurMonthDaysOfWeekField").hide(); 
	    }
		if(opt==2)
		{
	   			$('#otherOccurField').show();
	   			$('#customOccurFrequencyField').hide();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				$("#customOccurMonthDaysField").hide();
				$("#customOccurMonthDaysOfWeekField").hide();
				
		}
		if(opt==6)
		{
	   			$('#otherOccurField').show();
	   			$('#customOccurFrequencyField').show();
				$('#customOccurEveryDayField').show();
				$('#customOccurWeekEndField').show();
				
				
		}
	   }); 
	  
	  //this function works for custom fileds
	   
	$('#customOccurFrequencyField').change(function(){
		var subOpt=$('#frequency').val();
		
		//if custom days are selected
		if(subOpt==1){
				$('#customOccurEveryDayField').show();
				$('#customOccurWeekEndField').show();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
			}
		//if custom weeks are 	selected
		if(subOpt==2){
				$('#customOccurEveryWeekField').show();
				$('#customOccurWeekDaysField').slideDown('fast');
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				
				
			}
		//if custom month is selected	
		if(subOpt==3){
				$('#customOccurEveryMonthField').show();
				$('#customOccurMonthDayField').show();
				$("#customOccurMonthDaysField").show(); //Slide Down Effect
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();

			}
		//if custom year is selected	
		if(subOpt==4){
				$('#customOccurEveryYearField').show();
				$('#customOccurYearMonthsField').show();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				
			}		
				
		});  
		
		//this function work on radio button in monthly custom fields
		
		$('#DaysOfWeek').click(function(){
			$("#customOccurMonthDaysOfWeekField").slideDown("fast"); //Slide Down Effect
			$("#customOccurMonthDaysField").hide(); //Slide Down Effect
			
			});
		$('#Days').click(function(){
			$("#customOccurMonthDaysOfWeekField").hide(); //Slide Down Effect
			$("#customOccurMonthDaysField").slideDown(); //Slide Down Effect
			
			});	
			
		 
});


$(document).ready(function() {
//this code is for getting value of months button click
var sizeOfArray=30;
var monthDays=new Array(sizeOfArray);
var i;
for(i=0;i<=sizeOfArray;i++){
	monthDays[i]=0;
	}

  $('input[class="buttonstyle"]').click(function(){
	var bv=$('#toggle').val();
	var b=$(this).val();
	 if(bv==1){
	$(this).css('background-color','#09C');	 
	$('#toggle').val('0');
	monthDays[b-1]=0;
		 }else{
			  
	$(this).css('background-color','#CCC');
	$(this).css('border-color','#CCC');
	$('#toggle').val('1');    
	$('#buttonvalue').val(b);
	monthDays[b-1]=b;
	
		 }
	});
 

//this code is for getting value of years month button click
var sizeOfMonth=12;
var yearMonths=new Array(sizeOfMonth);
var j;
for(j=0;j<=sizeOfMonth;j++){
	yearMonths[j]=0;
	}
  
  $('input[class="monthButtonstyle"]').click(function(){
	var bv=$('#monthToggle').val();
	var b=$(this).val();
	 if(bv==1){
		$(this).css('background-color','#09C');	 
		$('#monthToggle').val('0');
		yearMonths[b-1]=0;
	  }else{
		$(this).css('background-color','#CCC');
		$(this).css('border-color','#CCC');
		$('#monthToggle').val('1');    
		$('#monthButtonValue').val(b);
		yearMonths[b-1]=b;
		 }
	  });
	  
	  $("#add_new_task").on("submit",function(e){
		var dueDate=$('#dueDate').val();
		var occer= $('#form_Occurs').val();
		var flag1;
		if(dueDate=="" && occer!=1){
		$("#required1").html('Due Date Required');
		flag=false;
		}
		
		  if($("#name").val()=="" || (dueDate=="" && occer!=1)){
			 
			 
			  }else{
		
			   addTask();
				  }
    
    e.preventDefault();
});

	 function addTask(){
	 // alert(yearMonths[0]+yearMonths[1]+yearMonths[2]+yearMonths[3]+yearMonths[4]+yearMonths[5]+yearMonths[6]);
	$.ajax({    
        type    : "POST",
        url     : "ajax_call.php?monthDay="+monthDays+"&yearMonth="+yearMonths+"&action=add",
        data    : $('#add_new_task').serialize(),                
		success: function(data) {
			
			 window.location.href='tasks_view.php';
			
			}
    	});
		
	 }
 
	  
    $('#saveQuickContact').click(function(){	
	  
	 if($("#quickfirstname").val()=="" || $('#quicklastname').val()=="" || $('#quickemail').val()==""){
		/*$("#quick_first_name").html('This field is required.');
		$("#quick_last_name").html('This field is required.');
		$("#quick_email").html('This field is required.')*/;
		$('#error').html('Please fill all required fields');
		 }else{
			  $('#error').html('');
		$("#quick_first_name").css('display','none');
		$("#quick_last_name").css('display','none');
		
		$("#quick_email").css('display','none'); 
	// alert(yearMonths[0]+yearMonths[1]+yearMonths[2]+yearMonths[3]+yearMonths[4]+yearMonths[5]+yearMonths[6]);
	$.ajax({    
        type    : "POST",
        url     : "../../classes/ajax.php?action=addQuickContact",
        data    : $('#addQuickContactForm').serialize(),                
		success: function(data) {
			
			if(data==1){
			$('.messageofsuccess').html('Contact has been added successfully');
			//disablePopup();
			}
		}
		});
		 }
	 });

});


function removeField(id){
	
			//this is use to ensure that this is phoneNumber 
			name='#test'+id;
					$(name).remove();
			
		}	

</script>
   
  </head>
  

  <?php 
  include('../../classes/resource.php');
  ?>
  <div class="container">
  <div id="success"></div>
<!--  <div id="menu_line"></div>-->
  
    <?php if(!empty($contact_id))
			{ $task_contact=$task->taskContact($contact_id);
			
			}?>
            
  
      <h1 class="gray">New Task</h1>
      <form name="add_new_task" id="add_new_task" action="" method="post" autocomplete="off">
  	<input type="hidden" name="userId" value="<?php echo $_SESSION["userId"];?>">    
      <table cellspacing="0" cellpadding="0" width="70%">
          <tr>
          	<td class="label">Name</td>
            <td style="width:80%"><input type="text" name="name" class="textfield required"  id="name" value="" /><div class="errorrequired" id="required"></div></td>
          </tr>
          
          <tr>
          	<td class="label">Completed</td>
            <td><input type="checkbox" name="completed"  class="checkboxstyle" id="complete"  style="margin-bottom:0px !important;"  /><label id="checkdatetime" style="display: none;font-size:12px;"> On <?php echo date("F jS  Y - g:i a").' <span id="changeDate" style="font-weight:bold; color:#0000EE;  cursor:pointer; padding-left:5px;" >Change Date</span>'; ?></label></td>
          </tr>
          
          <tr id="changeTheDate" style="display: none;">
          	<td class="label">Date</td>
            <td><input type="text" name="completeDate" class="textfield" id="completeDate"  value="<?php echo date("m/d/Y");?>"/></td>
          </tr>
          <tr id="changeTheTime" style="display: none;">
          	<td class="label">Time</td>
            <td><input type="text" name="completeTime" class="textfield" id="completeTime" value="<?php echo date("g:i a");?>"  /></td>
          </tr>
          	<td class="label">Priority</td>
            <td><select  class="SelectField" name="priorityId">
          		<option value="0">None</option>
		  <?php foreach($taskPriority as $tp){
			  
                     echo '<option value="'.$tp->priorityId.'">'.$tp->priority.'</option>';
					 }   ?>
          
          
          </select></td>
          </tr>
          <tr>
          
          	<td class="label">Notes</td>
            <td><textarea class="textarea" name="notes"></textarea></td>
          </tr>
           <tr>
          	<td class="label">Due Date</td>
            <td>
            <input type="text" name="dueDate" class="textfield  " id="dueDate"  /><label class="errorrequired" id="required1"></label>
            
            </td>
          </tr>
           <tr>
          	<td class="label">Due Time</td>
            <td><input type="text" name="dueTime" class="textfield " id="dueTime" /></td>
          </tr>
          <tr>
         <td class="label">Reminder</td>
          <td><select name="reminder" id="form_reminder_Interval" class="SelectField" onChange="if (this.value == '6') {displayReminder();} else if(this.value != '-1'){hideReminder();} else {hideReminder();}">
          <option value="1">None</option>
          <option value="2">10 Minutes</option>
          <option value="3">30 Minutes</option>
          <option value="4">1 Hour</option>
          <option value="5">1 Day</option>
          <option value="6">Custom</option>
          </select>
        
          <p class="reminder"><input type="text" class="textfieldExSmall" id="form_reminder_Quantity" size="3" name="reminderQuantity"> 
          <select id="form_reminder_Unit" name="reminderUnit" class="SelectFieldSmall">
        
            <option value="minutes">Minutes</option>
            <option value="hours">Hours</option>
            <option value="days">Day(s)</option>
        
    </select></p><br /></td>
          </tr>
          <tr>
          <td class="label">Occurs</td>
          <td><select class="SelectField" name="occur" id="form_Occurs" onChange="if (this.value == '6') {displayCustom();} else if(this.value != '1'){displaySimple();} else {hideAll();}">
        <?php 
						foreach($occur as $o){
							echo '<option value="'.$o->repeatTypeId.'" '.($o->repeatTypeId==1? "selected" : "").'>'.$o->type.'</option>';
                        	
							}
						?>
            
        
    </select>
    
    <p class="custom">Frequency <select id="Frequency" name="frequency" class="SelectFieldSmall" onChange="if (this.value=='1'){ displayDaily();} else if(this.value=='2'){displayWeekly();} else if(this.value=='3'){displayMonthly();} else if(this.value=='4'){displayYearly();} ">
        
   	<option value="1">Daily</option>
    <option value="2">Weekly</option>
    <option value="3">Monthly</option>
    <option value="4">Yearly</option>
        
    </select>
     </p>
    
    <div  class="customfields" id="customOccurDaysField" style="display:none;" >
    <input type="text" size="3" name="everyDay" id="DayInterval" class="textfieldExSmall custom_days"> day(s) <br> <br>
    <input type="checkbox" name="notOnWeekEnd" id="form_Recurrence_Day Not Weekends" class="checkbox"> 
    <label >Not On Weekends</label><br /><br />
   
    </div>
    
     <div  class="customfields" id="customOccurWeekDaysField" style="display:none;" >
     <input type="text" size="3" name="everyWeeks" id="WeekDayInterval" class="textfieldExSmall custom_weekdays" > Week(s) day <br> <br>
                    <input type="checkbox"  name="sun" >
                    <label>Sunday</label><br />
                    <input type="checkbox"  name="mon" >
                    <label>Monday</label><br />
                    <input type="checkbox"  name="tues" >
                    <label>Tuesday</label><br />
                    <input type="checkbox"  name="wed" >
                    <label>Wednesday</label><br />
                    <input type="checkbox"  name="thr" >
                    <label>Thursday</label><br />
                    <input type="checkbox"  name="fri" >
                    <label>Friday</label><br />
                    <input type="checkbox"  name="sat" >
                    <label>Saturday</label>
                    
      </div>
    
    <div class="customfields"  id="customOccurMonthDayField"  style="display:none;">
    <input type="text" size="3" name="everyMonth" id="MonthsInterval" class="textfieldExSmall custom_months"> month(s) <br> <br>
                    <input type="radio" checked="checked" name="monthDay" id="Days" value="yes">
                    <label>Day(s)</label>
                    <!--<input type="radio"  name="monthDay" id="DaysOfWeek" value="no" >
                    <label>Day Of The Week</label>-->
                </div>
                
                <div class="customfields" id="customOccurMonthDaysOfWeekField" style="display:none;" >
                    <select name="monthsWeek" >
                			<option value="1"  selected="selected">First</option>
                        	<option value="2">Second</option>
                            <option value="3">Third</option>
                            <option value="4">Fourth</option>
                            <option value="5">Last</option>
                    </select>
                    <select  name="monthsWeekDays">
                			<option value="1" selected="selected">Sunday</option>
                        	<option value="2">Monday</option>
                            <option value="3">Tuesday</option>
                            <option value="4">Wednesday</option>
                            <option value="5">Thursday</option>
                            <option value="6">Friday</option>
                            <option value="7">Saturday</option>
                    </select>
                    
                </div>
                 <div class="customfields"  id="customOccurMonthDaysField" style="display:none;" >
	                    <input type="hidden" id="buttonvalue"/>
                        <input type="hidden" id="toggle"/>
                        <table width="400" height="114" border="0">
                          <tr>
                            <td><input class="buttonstyle" name="day_1"  type="button" value="1"></td>
                            <td><input class="buttonstyle" name="day_2" type="button" value="2"></td>
                            <td><input class="buttonstyle" name="day_3" type="button" value="3"></td>
                            <td><input class="buttonstyle" name="day_4" type="button" value="4"></td>
                            <td><input class="buttonstyle" name="day_5" type="button" value="5"></td>
                            <td><input class="buttonstyle" name="day_6" type="button" value="6"></td>
                            <td><input class="buttonstyle" name="day_7" type="button" value="7"></td>
                            <td><input class="buttonstyle" name="day_8" type="button" value="8"></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_9"  type="button" value="9"></td>
                            <td><input class="buttonstyle" name="day_10" type="button" value="10"></td>
                            <td><input class="buttonstyle" name="day_11" type="button" value="11"></td>
                            <td><input class="buttonstyle" name="day_12" type="button" value="12"></td>
                            <td><input class="buttonstyle" name="day_13" type="button" value="13"></td>
                            <td><input class="buttonstyle" name="day_14" type="button" value="14"></td>
                            <td><input class="buttonstyle" name="day_15" type="button" value="15"></td>
                            <td><input class="buttonstyle" name="day_16" type="button" value="16"></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_17" type="button" value="17"></td>
                            <td><input class="buttonstyle" name="day_18" type="button" value="18"></td>
                            <td><input class="buttonstyle" name="day_19" type="button" value="19"></td>
                            <td><input class="buttonstyle" name="day_20" type="button" value="20"></td>
                            <td><input class="buttonstyle" name="day_21" type="button" value="21"></td>
                            <td><input class="buttonstyle" name="day_22" type="button" value="22"></td>
                            <td><input class="buttonstyle" name="day_23" type="button" value="23"></td>
                            <td><input class="buttonstyle" name="day_24" type="button" value="24"></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_25" type="button" value="25"></td>
                            <td><input class="buttonstyle" name="day_26" type="button" value="26"></td>
                            <td><input class="buttonstyle" name="day_27" type="button" value="27"></td>
                            <td><input class="buttonstyle" name="day_28" type="button" value="28"></td>
                            <td><input class="buttonstyle" name="day_29" type="button" value="29"></td>
                            <td><input class="buttonstyle" name="day_30" type="button" value="30"></td>
                            <td><input class="buttonstyle" name="day_31" type="button" value="31"></td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>

                    
                </div>
    
                <div class="customfields" id="customOccurYearMonths_Field" style="display:none;" >
                 <input type="text" size="3" name="everyyear" id="YearsInterval" class="textfieldExSmall custom_years"> Year(s) <br> <br>
   
	                <input type="hidden" id="monthButtonValue"/>
                    <input type="hidden" id="monthToggle"/>
                    <table width="256" height="81" border="0">
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="1" id="jan"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="2" id="feb"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="3" id="mar"></td>

                        <td><input class="monthButtonstyle" name="month[]" type="button" value="4" id="apr"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="5" id="may"></td>
                      </tr>
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="6" id="jun"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="7" id="jul"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="8" id="aug"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="9" id="sep"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="10" id="oct"></td>
                      </tr>
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="11" id="Nov"></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="12" id="dec"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>

                </div>
                <p class="ending">Ending On <input type="text" name="endingOn" id="endDate" class="textfieldSmall"/></p>
                 <div class="customfields" id="endingOn" style="display:none;">
    Ending On <input type="text" name="endingOns" id="endDsate" class="textfieldSmall"/>
    </div>
    </td></tr>
    
    
                		<?php 
					/*	foreach($occur as $o){
							echo '<option value="'.$o->repeatTypeId.'" '.($o->repeatTypeId==1? "selected" : "").'>'.$o->type.'</option>';
                        	
							}*/
						?>
                        	
        
    <tr>
    <td colspan="2" class="task_col">Who is the Task for?</td>
    <td></td>
    </tr>
    <tr> <td>&nbsp;</td> <td>&nbsp;</td></tr>
    
    <tr >
    <td id="contactsForTask"></td>
    <td>&nbsp;</td>
    </tr>
    
     <tr>
    <td class="label">
    <?php 
	
	(!empty($contact_id)?$data='['.$contact->getContactForDefault($contact_id).']':$data='');?>
     <div>
        
        
        <script type="text/javascript">
        $(document).ready(function() {
            $("#contactName").tokenInput("php-example.php", {
                preventDuplicates: true,
				<?php if($data==""){}else{?>
				prePopulate:<?php echo $data;?>
				<?php }?>
            });
		
        });
       
        </script>
    </div>
    <input type="text" name="contactName" id="contactName" class="textfieldSmall contact"/>
    </td>
    <td class="label"><a href="javascript:" onclick=loadform()>Add Quick Contact</a></td>
    </tr>
   
   
      <tr> <td>&nbsp;</td> <td>&nbsp;</td></tr>
      
     <tr>
     
     <td class="label">&nbsp;</td>
    <td><input type="submit" name="save" id="save" class="save" value=""/><input type="button" name="cancel" class="cancel" value="" onClick="history.go(-1)"/></td>
    </tr>
      </table>
      </form>
      </div>
      <div id="Popup"></div>
<div id="popupQuickquote" >
    <h1 class="gray">Add Quick Contact</h1>
    <p class="messageofsuccess"></p><p class="errorrequired" id="error" style="margin-left:0px;"></p>
<form method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="addQuickContactForm" name="addQuickContactForm">
    	<input type="hidden" name="userId" value="<?php echo $_SESSION["userId"];?>"> 
      <table cellspacing="0" cellpadding="0" width="100%">
          
          <tr>
          	<td class="label" style="width:20% !important;">First Name</td>
            <td width="80%">
            <input type="text" name="quickfirstname" id="quickfirstname" class="textfield required"  style="float:left;" />
            <label id="quick_first_name" style="font-size:12px; color:#F00; display:block;"></label>
            </td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Last Name</td>
            <td><input type="text" name="quicklastname" id="quicklastname" class="textfield required"  style="float:left;" />
            <label id="quick_last_name" style="font-size:12px; color:#F00; display:block;"></label>
            </td>
          
          </tr>
          <tr>
          <td class="label" style="width:0% !important;">Email</td>
          <td >
          <input class="textfield required email" name="quickemail" id="quickemail" type="text" style="float:left;">
          <label id="quick_email" style="font-size:12px; color:#F00; display:block;"></label>
          </td>
         </tr>
         <tr>
          <td class="label" style="width:0% !important;">Phone</td>
          <td  >
          <input class="textfield" name="quickphone" type="text" style="float:left;"></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Company Name</td>
            <td><input type="text" name="quickcompanyname" class="textfield"  style="float:left;"/></td>
          </tr>
          <tr>	
            <td class="label" style="width:0% !important;">Job Title</td>
            <td><input type="text" name="quickjobtitle" class="textfield"   style="float:left;"/></td>
          </tr>
          
          
       <tr>
     
     <td ><input type="hidden" name="action" value="addQuickContact"></td>
     <td ><input type="hidden" name="userid" value="<?php echo $_SESSION["userId"];?>"</td>
    </tr>
    <tr>
     
    <td >&nbsp;</td>
     <td >&nbsp;</td>
    </tr>
   <tr>
     
     <td >&nbsp;</td>
    <td><input type="button" name="saveQuickContact" id="saveQuickContact" class="save" value=""/><input type="button" name="cancel" class="cancel" value="" onClick="disablePopup()"/>
    
    </td>
    </tr>
    
         
        </tbody>
        </table>
        </div>
 
      </form>  
</div> 
      <?php include_once("../headerFooter/footer.php");?>
	  
  
   