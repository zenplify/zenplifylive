  <?php 
  error_reporting(0);
session_start();
include_once("../headerFooter/header.php");
include('../../classes/task.php');
include('../../classes/contact.php');



//include('../../modules/login_process/logincheck.php');
 $taskId=$_GET['task_id'];
 $edit_type=$_GET['edit_type'];
$task =new task();
$taskdetail= new task();
$contact= new contact();
//$contactId= $_GET['contactId'];
$taskPriority	=	$task->taskPriority();
$reminder		=	$task->reminder();
$occur			=	$task->occur();
$taskDetail		=   $taskdetail->TaskEdit($taskId);
$taskReminder	=	$taskdetail->getTaskReminder($taskId);
?>
<script type="text/javascript" src="../../js/src/jquery.tokeninput.js"></script>

    <link rel="stylesheet" href="../../css/styles/token-input.css" type="text/css" />
    <link rel="stylesheet" href="../../css/styles/token-input-facebook.css" type="text/css" />

 <!--<script type="text/javascript" src="../../js/jquery.js"></script>
<script type='text/javascript' src='../../js/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../css/jquery.autocomplete.css" />
 -->
<!--<script type="text/javascript">
$().ready(function() {
	
	$("#contactName").autocomplete("../../misc/getContactList.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		
		//minChars: 0,
		multiple: true,
		highlight: false,
		multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#contactName").result(function(event, data, formatted) {


		$("#contactId").val(data[1]);
	});
	$("#addanother").click(function(){
	var contactId=$('#contactId').val();
	$("<tr id='"+$('#contactId').val()+"'><td class='label'>"+$('#contactName').val()+"<input type='hidden' name='task"+$('#contactId').val()+"' value='"+$('#contactId').val()+"'></td><td class='label'>"+"<img src='../../images/delete.png' alt='delete' title='delete' class='linkButton' onclick=\"removeContact('"+contactId+"')\" />"+"</td></tr>").appendTo("#contactsForTask");
	$("#contactName").val('')
	$("#contactId").val('')
	
		});
});
function removeContact(id){
	$('#'+id).fadeOut(1000);
	}
</script>-->
        <script src="../../js/zen_jscript.js" type="text/javascript"></script>
     
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
   
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="../../js/timepicker/jquery-ui-timepicker-addon.js"></script>
      <script type="text/javascript" src="../../js/script.js"  ></script>
	
 <link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">	      
	  <script src="../../js/alert/jquery.toastmessage.js" type="text/javascript"></script> 
      <script>
      $(document).ready(function() {
        $('#dueTime').timepicker({
			hourGrid: 4,
			minuteGrid: 10,
			timeFormat: 'hh:mm tt'
			});

        $( "#dueDate" ).datepicker();
		
        $( "#endDate" ).datepicker();
		

        $( "#completeDate" ).datepicker({
			dateFormat:'mm/dd/yy'
			});
        $( "#completeTime" ).timepicker({
			hourGrid: 4,
			minuteGrid: 10,
			timeFormat: 'hh:mm tt'
			});
        

		$("#complete").change(function(){

			if ($(this).is(':checked')) {
			    $("#checkdatetime").show();
			} else {
			    $("#checkdatetime").hide();
				$("#changeTheDate").css('display','none');
			$("#changeTheTime").css('display','none');
			} 
			});
		$("#changeDate").click(function(){
			
			$("#changeTheDate").toggle();
			$("#changeTheTime").toggle();

			});
        });
      
      </script>
      
<script type="text/javascript">
/*function addCustom(){
	
	var opt=document.getElementById('reminder').value;
	
	
	
	if(opt==6){
		
		document.getElementById('customField')
		.innerHTML='<input type="text" name="value" style="width:63px;">'+' '+'<select name="reminderValueType"><option value="minutes">Minute(s)</option><option value="hours">Hour(s)</option><option value="days">Day(s)</option></select>';
		}
		else if(opt==1){
		document.getElementById('customField').innerHTML='No Reminder Will Be Generated';
		
		}
		else{
		document.getElementById('customField').innerHTML='Before Activity is due';
			}
	
	}*/
$(document).ready(function() {
	
   $('#occur').change(function(){
	   var opt=$('#occur').val();
	
	//this work for monthly weekly ,every day ,every week end
	
	 if(opt==1){//if occur once
				$('#otherOccurField').hide();
				$('#customOccurFrequencyField').hide();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				$("#customOccurMonthDaysField").hide();
				$("#customOccurMonthDaysOfWeekField").hide(); 
	    }
		if(opt==2)
		{
	   			$('#otherOccurField').show();
	   			$('#customOccurFrequencyField').hide();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				$("#customOccurMonthDaysField").hide();
				$("#customOccurMonthDaysOfWeekField").hide();
				
		}
		if(opt==6)
		{
	   			$('#otherOccurField').show();
	   			$('#customOccurFrequencyField').show();
				$('#customOccurEveryDayField').show();
				$('#customOccurWeekEndField').show();
				
				
		}
	   }); 
	  
	  //this function works for custom fileds
	   
	$('#customOccurFrequencyField').change(function(){
		var subOpt=$('#frequency').val();
		
		//if custom days are selected
		if(subOpt==1){
				$('#customOccurEveryDayField').show();
				$('#customOccurWeekEndField').show();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
			}
		//if custom weeks are 	selected
		if(subOpt==2){
				$('#customOccurEveryWeekField').show();
				$('#customOccurWeekDaysField').slideDown('fast');
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();
				
				
			}
		//if custom month is selected	
		if(subOpt==3){
				$('#customOccurEveryMonthField').show();
				$('#customOccurMonthDayField').show();
				$("#customOccurMonthDaysField").show(); //Slide Down Effect
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryYearField').hide();
				$('#customOccurYearMonthsField').hide();

			}
		//if custom year is selected	
		if(subOpt==4){
				$('#customOccurEveryYearField').show();
				$('#customOccurYearMonthsField').show();
				$('#customOccurEveryDayField').hide();
				$('#customOccurWeekEndField').hide();
				$('#customOccurEveryWeekField').hide();
				$('#customOccurWeekDaysField').hide();
				$('#customOccurEveryMonthField').hide();
				$('#customOccurMonthDayField').hide();
				$("#customOccurMonthDaysField").hide();
				
			}		
				
		});  
		
		//this function work on radio button in monthly custom fields
		
		$('#DaysOfWeek').click(function(){
			$("#customOccurMonthDaysOfWeekField").slideDown("fast"); //Slide Down Effect
			$("#customOccurMonthDaysField").hide(); //Slide Down Effect
			
			});
		$('#Days').click(function(){
			$("#customOccurMonthDaysOfWeekField").hide(); //Slide Down Effect
			$("#customOccurMonthDaysField").slideDown(); //Slide Down Effect
			
			});	
			
		 
});


$(document).ready(function() {
//this code is for getting value of months button click
var sizeOfArray=30;
var monthDays=new Array(sizeOfArray);
var i;
for(i=0;i<=sizeOfArray;i++){
	monthDays[i]=0;
	}

  $('input[class="buttonstyle"]').click(function(){
	var bv=$('#toggle').val();
	var b=$(this).val();
	 if(bv==1){
	$(this).css('background-color','#09C');	 
	$('#toggle').val('0');
	monthDays[b-1]=0;
		 }else{
			  
	$(this).css('background-color','#CCC');
	$(this).css('border-color','#CCC');
	$('#toggle').val('1');    
	$('#buttonvalue').val(b);
	monthDays[b-1]=b;
	
		 }
	});
 

//this code is for getting value of years month button click
var sizeOfMonth=12;
var yearMonths=new Array(sizeOfMonth);
var j;
for(j=0;j<=sizeOfMonth;j++){
	yearMonths[j]=0;
	}
  
  $('input[class="monthButtonstyle"]').click(function(){
	 
	var bv=$('#monthToggle').val();
	var b=$(this).val();
	 if(bv==1){
		$(this).css('background-color','#09C');	 
		$('#monthToggle').val('0');
		yearMonths[b-1]=0;
	  }else{
		$(this).css('background-color','#CCC');
		$(this).css('border-color','#CCC');
		$('#monthToggle').val('1');    
		$('#monthButtonValue').val(b);
		yearMonths[b-1]=b;
		 }
	  });
	  var edit_type=$("#edit_type").val();
	  if(edit_type==1){
	  $('#update').click(function(){
		var dueDate=$('#dueDate').val();
		var occer= $('#form_Occurs').val();
		
		
			
	 // alert(yearMonths[0]+yearMonths[1]+yearMonths[2]+yearMonths[3]+yearMonths[4]+yearMonths[5]+yearMonths[6]);
	 
		
	$.ajax({    
        type    : "POST",
        url     : "ajax_call.php?monthDay="+monthDays+"&yearMonth="+yearMonths+"&action=add",
        data    : $('#task').serialize(),                
		success: function(data) {
			//alert(data);
			window.location.href="tasks_view.php";
			}
    	});	
		
 });
	  }else{
		  
	$('#update').click(function(){	
	
	 // alert(yearMonths[0]+yearMonths[1]+yearMonths[2]+yearMonths[3]+yearMonths[4]+yearMonths[5]+yearMonths[6]);
	
		 
	$.ajax({    
        type    : "POST",
        url     : "ajax_call.php?monthDay="+monthDays+"&yearMonth="+yearMonths+"&action=edit",
        data    : $('#task').serialize(),                
		success: function(data) {
			//alert(data);
			window.location.href="tasks_view.php";
			
			}
    	});	
	
 });	  
		  }
  $('#saveQuickContact').click(function(){	
	  
	 if($("#quickfirstname").val()=="" || $('#quicklastname').val()=="" || $('#quickemail').val()==""){
		/*$("#quick_first_name").html('This field is required.');
		$("#quick_last_name").html('This field is required.');
		$("#quick_email").html('This field is required.');*/
		$('#error').html('Please fill all required fields');
		 }else{
			  $('#error').html('');
		$("#quick_first_name").css('display','none');
		$("#quick_last_name").css('display','none');
		$("#quick_email").css('display','none'); 
	// alert(yearMonths[0]+yearMonths[1]+yearMonths[2]+yearMonths[3]+yearMonths[4]+yearMonths[5]+yearMonths[6]);
	$.ajax({    
        type    : "POST",
        url     : "../../classes/ajax.php?action=addQuickContact",
        data    : $('#addQuickContactForm').serialize(),                
		success: function(data) {
			
			if(data==1){
			$('.messageofsuccess').html('Contact has been added successfully');
			//disablePopup();
			}
		}
		});
		 }
	 });

});

function removeField(id){
	
			//this is use to ensure that this is phoneNumber 
			name='#test'+id;
					$(name).remove();
			
		}	

</script>
   
  </head>
  
  <body>
  <div class="container">
  <div id="success"></div>
<!--  <div id="menu_line"></div>-->
  
  
      <h1 class="gray">Edit Task</h1>
      <form name="add_new_task" id="task" action="" method="post" autocomplete="off">
  	<input type="hidden" name="userId" value="<?php echo $_SESSION["userId"];?>"> 
    <input type="hidden" name="contact_id" id="contact_id" value="<?php echo $_REQUEST['contactId'];?>">
    <input type="hidden" name="taskId" id="taskId" value="<?php echo $_GET['task_id'];?>">
    <input type="hidden" name="edit_type" id="edit_type" value="<?php echo $_GET['edit_type'];?>">
     <input type="hidden" name="planStep" id="planStep" value="<?php echo $taskDetail->planId;?>">
     
       
      <table cellspacing="0" cellpadding="0" width="50%">
          <tr>
          	<td class="label">Name</td>
            <td><input type="text" name="name" class="textfield" value="<?php echo $taskDetail->title; ?>"  /></td>
          </tr>
          <tr>
          	<td class="label">Completed</td>
            <td><input type="checkbox" name="completed"  class="checkboxstyle" id="complete"  style="margin-bottom:0px !important;"  <?php echo($taskDetail->isCompleted==0?'':'checked'); ?> /><label id="checkdatetime" style="display: none;font-size:12px;"> On <?php echo date("F jS  Y - g:i a").' <span id="changeDate" style="font-weight:bold; color:#0000EE;  cursor:pointer; padding-left:5px;" >Change Date</span>'; ?></label></td>
          </tr>
          
          
          <tr id="changeTheDate" style="display: none;">
          	<td class="label">Date</td>
            <td><input type="text" name="completeDate" class="textfield" id="completeDate"  value="<?php echo date("m/d/Y");?>"/></td>
          </tr>
          <tr id="changeTheTime" style="display: none;">
          	<td class="label">Time</td>
            <td><input type="text" name="completeTime" class="textfield" id="completeTime" value="<?php echo date("g:i a");?>" /></td>
          </tr>
          	<td class="label">Priority</td>
            <td><select  class="SelectField" name="priorityId">
          		<option value="0">None</option>
		  <?php foreach($taskPriority as $tp){
			  
                     echo '<option value="'.$tp->priorityId.'" '.($taskDetail->priorityId==$tp->priorityId?"selected":"").'>'.$tp->priority.'</option>';
					 }   ?>
          
          
          </select></td>
          </tr>
          <tr>
          
          	<td class="label">Notes</td>
            <td><textarea class="textarea" name="notes"><?php echo $taskDetail->notes; ?></textarea></td>
          </tr>
           <tr>
           <?php 
		   if(date('Y',strtotime($taskDetail->dueDateTime))<=1980){
			   $date="";
			   $time="";
			   }else{
		   $dateTime=$taskDetail->dueDateTime;
		  // $date = date('m/d/Y', strtotime($dateTime));
			//$time = date('h:i a', strtotime($dateTime));
			((date('H:i:s', strtotime($dateTime)))!='00:00:00'?$time=date('g:i a', strtotime($dateTime)):$time='');
		   ((date('Y-m-d', strtotime($dateTime)))!='0000-00-00'?$date=date('m/d/Y', strtotime($dateTime)):$date='');
			   }
		   ?>
          	
            <td class="label">Due Date</td>
            <td><input type="text" name="dueDate" class="textfield" id="dueDate" value="<?php echo $date; ?>" /></td>
          </tr>
           <tr>
          	<td class="label">Due Time</td>
            <td><input type="text" name="dueTime" class="textfield" id="dueTime"  value="<?php echo $time; ?>"/></td>
          </tr>
          <tr>
         <td class="label">Reminder</td>
          <td><select name="reminder" id="form_reminder_Interval" class="SelectField" onChange="if (this.value == '6') {displayReminder();} else if(this.value != '-1'){hideReminder();} else {hideReminder();}">
          
          <?php foreach($reminder as $r){
          echo '<option value="'.$r->reminderTypeId.'" '.($taskReminder->reminderTypeId==$r->reminderTypeId?"selected":"").' >'.$r->numberOfMinutes.' '.$r->text.'</option>';
          }?>
          
          </select>
        
          <p class="reminder"   <?php echo($taskReminder->reminderTypeId==6?"style='display:block !important;'":'');?>><input type="text" class="textfieldExSmall" id="form_reminder_Quantity" size="3" name="reminderQuantity" value="<?php echo($taskReminder->reminderTypeId==6?$taskReminder->customValue:'');?>"> 
          <select id="form_reminder_Unit" name="reminderUnit" class="SelectFieldSmall">
        
            <option value="minutes" <?php echo($taskReminder->customUnit=="minutes"?"selected":"");?> >Minutes</option>
            <option value="hours" <?php echo($taskReminder->customUnit=="hours"?"selected":"");?>>Hours</option>
            <option value="days" <?php echo($taskReminder->customUnit=="hours"?"selected":"");?>>Day(s)</option>
        
    </select></p><br /></td>
          </tr>
          
          <?php if($taskDetail->planId==0 || $taskDetail->planId==NULL || $taskDetail->planId=='') {?>
          <tr>
          <td class="label">Occurs</td>
          <td><select class="SelectField" name="occur" id="form_Occurs" onChange="if (this.value == '6') {displayCustom();} else if(this.value != '1'){displaySimple();} else {hideAll();}">
        <?php 
		
			if($edit_type!=1){
						foreach($occur as $o){
							echo '<option value="'.$o->repeatTypeId.'"  '.($taskDetail->repeatTypeId==$o->repeatTypeId? "selected" : "").'>'.$o->type.'</option>';
                        	
						}
			}else{
				foreach($occur as $o){
							echo '<option value="'.$o->repeatTypeId.'"  >'.$o->type.'</option>';
                        	
						}
				}
						?>
            
        
    </select>
     <?php 
	 $customRuleId=$taskDetail->customRuleId;
	if($edit_type!=1){
	$taskCustomRule	=	$taskdetail->getTaskCustomRule($customRuleId);
	

	
	if($taskCustomRule->frequency==1){
	$taskCustomRuleDaily= $taskdetail->getTaskCustomRuledaily($customRuleId);}
	if($taskCustomRule->frequency==2){
	$taskCustomRuleWeekly= $taskdetail->getTaskCustomRuleWeekly($customRuleId);}
	if($taskCustomRule->frequency==3){
	$taskCustomRuleMonthly= $taskdetail->getTaskCustomRuleMonthly($customRuleId);}
	if($taskCustomRule->frequency==4){
	$taskCustomRuleYearly= $taskdetail->getTaskCustomRuleYearly($customRuleId);}
	
	
	
	
	
	?>
    <p class="custom" <?php echo ($taskDetail->repeatTypeId==6? 'style="display:block !important;"' : "");?>>Frequency <select id="Frequency" name="frequency" class="SelectFieldSmall" onChange="if (this.value=='1'){ displayDaily();} else if(this.value=='2'){displayWeekly();} else if(this.value=='3'){displayMonthly();} else if(this.value=='4'){displayYearly();} ">
        
   	<option value="1" <?php echo ($taskCustomRule->frequency==1? "selected" : "");?>>Daily</option>
    <option value="2" <?php echo ($taskCustomRule->frequency==2? "selected" : "");?>>Weekly</option>
    <option value="3" <?php echo ($taskCustomRule->frequency==3? "selected" : "");?>>Monthly</option>
    <option value="4" <?php echo ($taskCustomRule->frequency==4? "selected" : "");?>>Yearly</option>
        
    </select>
     </p>
   <?php }?>
    <div  class="customfields displayNone" id="customOccurDaysField"  <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==1? 'style="display:block !important;"' : "");?>>
    <input type="text" size="3" name="everyDay" id="DayInterval" class="textfieldExSmall custom_days" value=" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==1? $taskCustomRule->every : "");?>"> day(s) <br> <br>
    <input type="checkbox" name="notOnWeekEnd" id="form_Recurrence_Day Not Weekends" class="checkbox" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==1? ($taskCustomRuleDaily->isNotweekEnd==1)?"checked":"" : "");?>> 
    <label >Not On Weekends</label><br /><br />
   
    </div>
    
     <div  class="customfields displayNone" id="customOccurWeekDaysField"  <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==2? 'style="display:block !important;"' : "");?>>
     <input type="text" size="3" name="everyWeeks" id="WeekDayInterval" class="textfieldExSmall custom_weekdays" value=" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==2? $taskCustomRule->every : "");?>"> Week(s) day <br> <br>
                    <input type="checkbox"  name="sun"  <?php echo ($taskCustomRuleWeekly->isSunday==1?"checked":"");?>>
                    <label>Sunday</label><br />
                    <input type="checkbox"  name="mon" <?php echo ($taskCustomRuleWeekly->isMonday==1?"checked":"");?>>
                    <label>Monday</label><br />
                    <input type="checkbox"  name="tues" <?php echo ($taskCustomRuleWeekly->isTuesday==1?"checked":"");?>>
                    <label>Tuesday</label><br />
                    <input type="checkbox"  name="wed" <?php echo ($taskCustomRuleWeekly->isWednesday==1?"checked":"");?>>
                    <label>Wednesday</label><br />
                    <input type="checkbox"  name="thr" <?php echo ($taskCustomRuleWeekly->isThursday==1?"checked":"");?>>
                    <label>Thursday</label><br />
                    <input type="checkbox"  name="fri" <?php echo ($taskCustomRuleWeekly->isFriday==1?"checked":"");?>>
                    <label>Friday</label><br />
                    <input type="checkbox"  name="sat" <?php echo ($taskCustomRuleWeekly->isSaturday==1?"checked":"");?>>
                    <label>Saturday</label>
                    
      </div>
    
    <div class="customfields displayNone"  id="customOccurMonthDayField"   <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==3? 'style="display:block !important;"' : "");?>>
    <input type="text" size="3" name="everyMonth" id="MonthsInterval" class="textfieldExSmall custom_months" value=" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==3? $taskCustomRule->every : "");?>"> month(s) <br> <br>
                    <input type="radio" checked="checked" name="monthDay" id="Days" value="yes">
                    <label>Day(s)</label>
                    <!--<input type="radio"  name="monthDay" id="DaysOfWeek" value="no" >
                    <label>Day Of The Week</label>-->
                </div>
                
                <div class="customfields" style="display:none;" id="customOccurMonthDaysOfWeekField"  >
                    <select name="monthsWeek" >
                			<option value="1" <?php echo($taskCustomRuleMonthly->weekOfMonth==1?"selected":""); ?>>First</option>
                        	<option value="2" <?php echo($taskCustomRuleMonthly->weekOfMonth==2?"selected":""); ?>>Second</option>
                            <option value="3" <?php echo($taskCustomRuleMonthly->weekOfMonth==3?"selected":""); ?>>Third</option>
                            <option value="4" <?php echo($taskCustomRuleMonthly->weekOfMonth==4?"selected":""); ?>>Fourth</option>
                            <option value="5" <?php echo($taskCustomRuleMonthly->weekOfMonth==5?"selected":""); ?>>Last</option>
                    </select>
                    <select  name="monthsWeekDays">
                			<option value="1" <?php echo($taskCustomRuleMonthly->dayOfWeek==1?"selected":""); ?>>Sunday</option>
                        	<option value="2" <?php echo($taskCustomRuleMonthly->dayOfWeek==2?"selected":""); ?>>Monday</option>
                            <option value="3" <?php echo($taskCustomRuleMonthly->dayOfWeek==3?"selected":""); ?>>Tuesday</option>
                            <option value="4" <?php echo($taskCustomRuleMonthly->dayOfWeek==4?"selected":""); ?>>Wednesday</option>
                            <option value="5" <?php echo($taskCustomRuleMonthly->dayOfWeek==5?"selected":""); ?>>Thursday</option>
                            <option value="6" <?php echo($taskCustomRuleMonthly->dayOfWeek==6?"selected":""); ?>>Friday</option>
                            <option value="7" <?php echo($taskCustomRuleMonthly->dayOfWeek==7?"selected":""); ?>>Saturday</option>
                    </select>
                    
                </div>
                 <div class="customfields displayNone"  id="customOccurMonthDaysField"  <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==3? 'style="display:block !important;"' : "");?>>
	                    <input type="hidden" id="buttonvalue"/>
                        <input type="hidden" id="toggle"/>
                        <table width="400" height="114" border="0">
                          <tr>
                            <td><input class="buttonstyle" name="day_1"  type="button" value="1" <?php echo($taskCustomRuleMonthly->is1==1?'style="background-color:#CCC !important;"':""); ?> ></td>
                            <td><input class="buttonstyle" name="day_2" type="button" value="2" <?php echo($taskCustomRuleMonthly->is2==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_3" type="button" value="3" <?php echo($taskCustomRuleMonthly->is3==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_4" type="button" value="4" <?php echo($taskCustomRuleMonthly->is4==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_5" type="button" value="5" <?php echo($taskCustomRuleMonthly->is5==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_6" type="button" value="6" <?php echo($taskCustomRuleMonthly->is6==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_7" type="button" value="7" <?php echo($taskCustomRuleMonthly->is7==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_8" type="button" value="8" <?php echo($taskCustomRuleMonthly->is8==1?'style="background-color:#CCC !important;"':""); ?>></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_9"  type="button" value="9" <?php echo($taskCustomRuleMonthly->is9==9?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_10" type="button" value="10" <?php echo($taskCustomRuleMonthly->is10==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_11" type="button" value="11" <?php echo($taskCustomRuleMonthly->is11==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_12" type="button" value="12" <?php echo($taskCustomRuleMonthly->is12==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_13" type="button" value="13" <?php echo($taskCustomRuleMonthly->is13==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_14" type="button" value="14" <?php echo($taskCustomRuleMonthly->is14==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_15" type="button" value="15" <?php echo($taskCustomRuleMonthly->is15==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_16" type="button" value="16" <?php echo($taskCustomRuleMonthly->is16==1?'style="background-color:#CCC !important;"':""); ?>></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_17" type="button" value="17" <?php echo($taskCustomRuleMonthly->is17==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_18" type="button" value="18" <?php echo($taskCustomRuleMonthly->is16==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_19" type="button" value="19" <?php echo($taskCustomRuleMonthly->is19==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_20" type="button" value="20" <?php echo($taskCustomRuleMonthly->is20==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_21" type="button" value="21" <?php echo($taskCustomRuleMonthly->is21==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_22" type="button" value="22" <?php echo($taskCustomRuleMonthly->is22==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_23" type="button" value="23" <?php echo($taskCustomRuleMonthly->is23==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_24" type="button" value="24" <?php echo($taskCustomRuleMonthly->is24==1?'style="background-color:#CCC !important;"':""); ?>></td>
                          </tr>
                          <tr>
                            <td><input class="buttonstyle" name="day_25" type="button" value="25" <?php echo($taskCustomRuleMonthly->is25==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_26" type="button" value="26" <?php echo($taskCustomRuleMonthly->is26==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_27" type="button" value="27" <?php echo($taskCustomRuleMonthly->is27==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_28" type="button" value="28" <?php echo($taskCustomRuleMonthly->is28==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_29" type="button" value="29" <?php echo($taskCustomRuleMonthly->is29==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_30" type="button" value="30" <?php echo($taskCustomRuleMonthly->is30==1?'style="background-color:#CCC !important;"':""); ?>></td>
                            <td><input class="buttonstyle" name="day_31" type="button" value="31" taskCustomRuleYearly></td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>

                    
                </div>
    
                <div class="customfields displayNone" id="customOccurYearMonths_Field" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==4? 'style="display:block !important;"' : "");?> >
                 <input type="text" size="3" name="everyyear" id="YearsInterval" class="textfieldExSmall custom_years" value=" <?php echo ($taskDetail->repeatTypeId==6 && $taskCustomRule->frequency==4? $taskCustomRule->every : "");?>"> Year(s) <br> <br>
   
	                <input type="hidden" id="monthButtonValue"/>
                    <input type="hidden" id="monthToggle"/>
                    <table width="256" height="81" border="0">
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="1" id="jan" <?php echo($taskCustomRuleYearly->isJan==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="2" id="feb" <?php echo($taskCustomRuleYearly->isFeb==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="3" id="mar" <?php echo($taskCustomRuleYearly->isMar==1?'style="background-color:#CCC !important;"':""); ?>></td>

                        <td><input class="monthButtonstyle" name="month[]" type="button" value="4" id="apr" <?php echo($taskCustomRuleYearly->isApril==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="5" id="may" <?php echo($taskCustomRuleYearly->isMay==1?'style="background-color:#CCC !important;"':""); ?>></td>
                      </tr>
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="6" id="jun" <?php echo($taskCustomRuleYearly->isJune==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="7" id="jul" <?php echo($taskCustomRuleYearly->isJuly==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="8" id="aug" <?php echo($taskCustomRuleYearly->isAug==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="9" id="sep" <?php echo($taskCustomRuleYearly->isSep==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="10" id="oct" <?php echo($taskCustomRuleYearly->isOct==1?'style="background-color:#CCC !important;"':""); ?>></td>
                      </tr>
                      <tr>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="11" id="Nov" <?php echo($taskCustomRuleYearly->isNov==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td><input class="monthButtonstyle" name="month[]" type="button" value="12" id="dec" <?php echo($taskCustomRuleYearly->isDec==1?'style="background-color:#CCC !important;"':""); ?>></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>

                </div>
              <?php
               if(date('Y',strtotime($taskDetail->endingOn))<=1980){
			   $date="";
			  
			   }else{
		   ((date('Y-m-d H:i:s', strtotime($taskDetail->endingOn)))!='0000-00-00 00:00:00'?$date=date('m/d/Y', strtotime($taskDetail->endingOn)):$date='');
			   }
               ?>
			    <p class="ending" <?php echo ($taskDetail->repeatTypeId!=1? 'style="display:block !important;"' : "");?>>Ending On <input type="text" name="endingOn" id="endDate" class="textfieldSmall" <?php echo ($taskDetail->repeatTypeId!=1 ? 'value="'.$date.'"' : "");?>/></p>
                
                 <div class="customfields" id="endingOn" style="display:none;">
    Ending On <input type="text" name="endingOns" id="endDsate" class="textfieldSmall"/>
    </div>
    </td></tr>
    
    
                		<?php }
					/*	foreach($occur as $o){
							echo '<option value="'.$o->repeatTypeId.'" '.($o->repeatTypeId==1? "selected" : "").'>'.$o->type.'</option>';
                        	
							}*/
						?>
                        	    	
        
    <tr>
    <td colspan="2" class="task_col">Who is the Task for?</td>
    <td></td>
    </tr>
    <tr> <td>&nbsp;</td> <td>&nbsp;</td></tr>
    <tr >
    <td id="contactsForTask"></td>
    <td>&nbsp;</td>
    </tr>
    <?php 
	$data='['.$task->gettaskContacts($taskId).']';?>
    <script type="text/javascript">
        $(document).ready(function() {
			
        //result=$("#taskcontact").val();
			
            $("#contactName").tokenInput("php-example.php", {
				
                preventDuplicates: true,
				prePopulate:<?php echo $data;?>
            });
	    });
        </script>
     <tr>
      <?php if($taskDetail->planId!=0){ ?>
      <td class="label">Contact Name</td><td class="label">
      <?php
		  $contactName=$task->getcontactId($taskId);
		  foreach($contactName as $contact){
			  echo $contact->firstName." ".$contact->lastName;
			  } ?> </td><?php }if($taskDetail->planId==0 || $taskDetail->planId==''){?>
   <td class="label"> <input type="text" name="contactName" id="contactName" class="textfieldSmall contact"  />
   </td>
    <td class="label"><a href="javascript:" onclick=loadform()>Add Quick Contact</a></td><?php }?>

    </tr>
                    
   
    
      <tr> <td>&nbsp;</td> <td>&nbsp;</td></tr>
     <tr>
     
     <td class="label">&nbsp;</td>
    <td><input type="button" name="save" id="update" class="update" value=""/><input type="button" name="cancel" class="cancel" value="" onClick="history.go(-1)"/></td>
    </tr>
      </table>
      </form>
      </div>
       <div id="Popup"></div>
<div id="popupQuickquote" >
    <h1 class="gray">Add Quick Contact</h1>
    <p class="messageofsuccess"></p><p class="errorrequired" id="error" style="margin-left:0px;"></p>
<form method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="addQuickContactForm" name="addQuickContactForm">
     <table cellspacing="0" cellpadding="0" width="100%">
          
          <tr>
          	<td class="label" style="width:20% !important;">First Name</td>
            <td width="80%">
            <input type="text" name="quickfirstname" id="quickfirstname" class="textfield required"  style="float:left;" />
            <label id="quick_first_name" style="font-size:12px; color:#F00; display:block;"></label>
            </td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Last Name</td>
            <td><input type="text" name="quicklastname" id="quicklastname" class="textfield required"  style="float:left;" />
            <label id="quick_last_name" style="font-size:12px; color:#F00; display:block;"></label>
            </td>
          
          </tr>
          <tr>
          <td class="label" style="width:0% !important;">Email</td>
          <td >
          <input class="textfield required email" name="quickemail" id="quickemail" type="text" style="float:left;">
          <label id="quick_email" style="font-size:12px; color:#F00; display:block;"></label>
          </td>
         </tr>
         <tr>
          <td class="label" style="width:0% !important;">Phone</td>
          <td  >
          <input class="textfield" name="quickphone" type="text" style="float:left;"></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Company Name</td>
            <td><input type="text" name="quickcompanyname" class="textfield"  style="float:left;"/></td>
          </tr>
          <tr>	
            <td class="label" style="width:0% !important;">Job Title</td>
            <td><input type="text" name="quickjobtitle" class="textfield"   style="float:left;"/></td>
          </tr>
          
          
       <tr>
     
     <td ><input type="hidden" name="action" value="addQuickContact"></td>
     <td ><input type="hidden" name="userid" value="<?php echo $_SESSION["userId"];?>"</td>
    </tr>
    <tr>
     
    <td >&nbsp;</td>
     <td >&nbsp;</td>
    </tr>
   <tr>
     
     <td >&nbsp;</td>
    <td><input type="button" name="saveQuickContact" id="saveQuickContact" class="save" value=""/><input type="button" name="cancel" class="cancel" value="" onClick="disablePopup()"/>
    
    </td>
    </tr>
    
         
        </tbody>
        </table>
        </div>
 
      </form>  
</div> 
      </div>
      <?php include_once("../headerFooter/footer.php");?>