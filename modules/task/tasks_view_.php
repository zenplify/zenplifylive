<?php 
error_reporting(0);
session_start();
include_once("../headerFooter/header.php");
require_once ("../../classes/task.php");

$userId=$_SESSION['userId'];

$task = new task();
$tasks = $task->GetAllUserTask($userId);
?>
 <script type="text/javascript" src="../../js/jquery-1.8.3.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

 <script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				
			
			        $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra']})
					.tablesorterPager({container: $("#pager")}); 
			});
			</script>
 
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <a href="new_task.php"><input type="button" id="new_task" value=""  /></a>
    
     </div>
       <div class="sub_container">
          <div class="col_table">
             <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
  				<thead>    
                <tr class="header">
                    <th class="first" >&nbsp;Done</th>
                    <th class="center big">Summary <img src="../../images/bg.gif" /></th>
                    <th class="center">Priority <img src="../../images/bg.gif" /></th>
                    <th class="center">Due Date <img src="../../images/bg.gif" /></th>
                    <th class="center">Who <img src="../../images/bg.gif" /></th>
                    <th class="center smallcol">Actions</th>
                    <th class="last"></th>
                </tr>
                </thead>
				<tbody>
				<tr>
                    <td class="taskDiff"> Today</td>
                    <td class="taskDiff"></td>
                    <td class="taskDiff"></td>
                    <td class="taskDiff"></td>
                    <td class="taskDiff"></td>
                    <td class="taskDiff"></td>
                    <td class="taskDiff"></td>
                </tr>
                
				<?php 
	
					foreach ($tasks as $row)
					{
						//echo "<tr class=\"odd_gradeX\" id=\"2\">";
						$id=$row->taskId;
						
						$preority=$task->GetTaskPriority($row->priorityId);
						$dateTime=$row->dueDateTime;
						echo "<tr class=\"odd_gradeX\" id=\"".$id."\">";
							//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
							echo "<td><input type=\"checkbox\"></td>";
							echo "<td>".$row->title."</td>";
							echo "<td>".$preority->priority."</td>";
							echo "<td>".date("d/m/Y", strtotime($dateTime))."</td>";
							echo "<td></td>";
							echo "<td><a href=\"edit_task.php\"><img src=\"../../images/edit.png\" title=\"Edit\" alt=\"Edit\" /></a><a href=\"javascript:\"><img src=\"../../images/Skip.png\" alt=\"Skip\" title=\"Skip\" /></a></td><td>&nbsp";
							echo "<td></td>";
						echo "</tr>";
						
					}
				?>
				</tbody>
               </table>
                <table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="float:right; margin-top:10px;">
                               
                                   
                                    <div class="pager" id="pager">
	<form>
		<img class="first_page" src="images/paging_far_left.gif" />
		<img class="prev_page" src="images/paging_far_left.gif" />
		<input type="text" class="pagedisplay" value="1/1" />
		<img class="next_page" src="images/paging_far_right.gif" />
		<img class="last_page" src="images/paging_far_right.gif" />
		
        <select id="pagesize" name="pagesize" class="pagesize"><option value="10" selected="selected">10</option><option value="100">100</option><option value="250">250</option></select>       
	</form>
</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                <div class="empty"></div>
                
       		</div>
           <div class="col_search">
                <p class="search_header">Find Tasks</p>
           		<input type="text" name="searchContact" class="searchbox2" id="searchbox3" title="Search Contacts" />
                 <br />
                
                
                 <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <!--
                <a href="tasks_history_view.php" style="text-decoration:none;"><p class="search_header">View Tasks History</p></a>
                <img src="images/check.png" /> Include Custome Fields-->
               <!-- <p class="search_header small">By the Group <img src="images/arrow.png" class="arrow" /></p>-->
           </div>
           
       </div>
      
     <?php 
		include_once("../headerFooter/footer.php");
	?>