<?php
include('../../classes/init.php');
include('../../classes/task.php');
include('../../classes/profiles.php');
$database = new database();
$profiles= new Profiles();
$taskObj= new task();
$action=$_GET["action"];
if($action=="add")
{
	$userId			=  $_POST['userId'];	
 	$title			=	$_POST['name'];
	($_POST['completed']=='on'?$isCompleted=1:$isCompleted=0);
	$priorityId		=	$_POST['priorityId'];
	$notes			=	$_POST['notes'];
	//completed date
	$cdate= date('Y-m-d', strtotime($_POST['completeDate']));
	if($isCompleted==1){
		$completedDate=$cdate;
		}else{$completedDate=0;}
	//$dueTime	= 	$_POST['dueTime'];
	if($_POST['dueDate']==''){
		$dueDate='0000-00-00';
	}else{
	$dueDate= date('Y-m-d', strtotime($_POST['dueDate'])); // outputs 2006-01-24
	}
	
	if($_POST['dueTime']==''){
		$dueTime='00:00:00';
	}else{
		// 12-hour time to 24-hour time 

	$dueTime= date("H:i:s", strtotime($_POST['dueTime'])); // outputs 2006-01-24

	}
	if($isCompleted==1 ){
		if($_POST['completeDate']!='')
		$completeDate= date('Y-m-d', strtotime($_POST['completeDate'])); // outputs 2006-01-24
		else
		$completeDate='0000-00-00';
		if($_POST['completeTime']!='')
		$completeTime= date("H:i:s", strtotime($_POST['completeTime'])); // outputs 2006-01-24
		else
		$completeTime='00:00:00';

		}else{
			$completeDate='0000-00-00';
			$completeTime='00:00:00';
			}
	

	$dueDateTime=$dueDate." ".$dueTime;
	$completedDateTime=$completeDate." ".$completeTime;
	$reminder		=	$_POST['reminder'];
	$reminderValue	=	$_POST['reminderQuantity'];
	$reminderValueType	=	$_POST['reminderUnit'];
	$contactId     =$_POST['contact_id'];
	switch($reminderValueType){
		case 'minutes':
			  $NumberOfMinutes=$reminderValue;
			  break;
		case 'hours':
			  $NumberOfMinutes=$reminderValue*60;
			break;
		case 'days':
		 	 $NumberOfMinutes=$reminderValue*60*24;
			break;
		default:
		 	$NumberOfMinutes=0;
		 }
	
	$taskOccur			=	$_POST['occur'];
	$taskOccurFrequency	=	$_POST['frequency'];
	//custom year months data for occur
	$taskOccurYearly	=	$_POST['everyyear'];
	$yearMonths			=	explode(',',$_GET['yearMonth']);
	//custom month data for occur
	$taskOccurMonthly	=	$_POST['everyMonth'];
	$monthDays 		    =	explode(',',$_GET['monthDay']);
	$monthsWeek			=	$_POST['monthsWeek'];
	$monthsWeekDays		=	$_POST['monthsWeekDays'];
	//custom week data for occur
	$taskOccurWeekly	=	$_POST['everyWeeks'];
	$weekDays=array($_POST['sun'],$_POST['mon'],$_POST['tues'],$_POST['wed'],$_POST['thr'],$_POST['fri'],$_POST['sat']);
	//custom daily data for occur
	$taskOccurDaily		=	$_POST['everyDay'];
	$notOnWeekEnd		=	$_POST['notOnWeekEnd'];
	
	switch($notOnWeekEnd){
		case 'on':
			$isNotweekEnd=1;
		break;
		case 'off':
			 $isNotweekEnd=0;
		break;
		}
	
	switch($taskOccurFrequency){
		case 1:
			$every=$taskOccurDaily;
			break;
		case 2:
			$every=$taskOccurWeekly;
			break;
		case 3:
		 	$every=$taskOccurMonthly;
			break;
		case 4:
			$every=$taskOccurYearly;
			break;
		default:
		 	$every=0;
		 }
	if($_POST['endingOn']==''){
		$endingOn='0000-00-00';
	}else{
	$endingOn= date('Y-m-d', strtotime($_POST['endingOn'])); // outputs 2006-01-24
	}
	
	$contact			=	$_POST['contact'];
	//$contactId			=	$_POST['contactId'];
	$addedDate			=	date("Y-n-j");

	//query section for new task
	
	$database->executeNonQuery("INSERT INTO tasks
	(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,completedDate,customRuleId, repeatTypeId) 
	VALUES 
	('".$userId	."','".$title."','".$isCompleted."','".$priorityId."','".$notes."','".$dueDateTime."','0','".$addedDate."','0','0','0','".$endingOn."',completedDate='".$completedDateTime."','0','".$taskOccur."')");	
	
	//this is last inserted id of task
	
	$taskId=$database->insertid();
	
	//Insert task reminder 
	
	$database->executeNonQuery("INSERT INTO taskreminder(taskId, reminderTypeId, numberOfMinutes, customValue, customUnit) VALUES ('".$taskId."','".$reminder."','".$NumberOfMinutes."','".$reminderValue."','".$reminderValueType."')");
	
	//custom occurence 
	switch($taskOccur){
		case 6:
		$database->executeNonQuery("INSERT INTO customrules(frequency, every) VALUES ('".$taskOccurFrequency."','".$every."')");
		//this is last inserted id of task
	$customRuledId=$database->insertid();
	
	if($taskOccurFrequency==1){
		
			$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
										VALUES (".$customRuledId.",".$isNotweekEnd.")");	
	}
	if($taskOccurFrequency==2){
			$database->executeNonQuery("INSERT INTO customruleweekly
										(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday) 
										VALUES 
										(".$customRuledId.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."',
										 '".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");
	}
	if($taskOccurFrequency==3){
	
		
		 	$database->executeNonQuery("INSERT INTO customrulemonthly
			(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
			VALUES 
			(".$customRuledId.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
			 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
			 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
			 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
			 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
			 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
			 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
			 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
			 ".$monthsWeek.",".$monthsWeekDays.")");
	}
	if($taskOccurFrequency==4){
	
		
			$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
			VALUES (".$customRuledId.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
			".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
			".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
			".($yearMonths[11]!=0?1:0).")");
		
		 }
		
		break;
		}
		
		$contactIdsForTask=explode(',',$_POST['contactName']);
		
		for($i=0;$i<sizeof($contactIdsForTask);$i++){
		
		$database->executeNonQuery("INSERT INTO taskcontacts(taskId, contactId) VALUES (".$taskId.",'".$contactIdsForTask[$i]."')");
		
			}
		$database->executeNonQuery("UPDATE 	tasks SET customRuleId='".$customRuledId."' WHERE taskId='".$taskId."'");
		
		
		if($isCompleted==1)
		{
			
			
			 $PlanTask=$taskObj->markTaskCompleted($userId,$taskId,$contactIdsForTask,$completeDate,$completeTime);
		}
	
	echo 1;
}


if($action=="edit"){
	
	
		$userId		=  $_POST['userId'];	
		$taskId  	=	$_POST['taskId'];
 	$title			=	$_POST['name'];
	echo 'iscompleted'.$_POST['completed'];
	$contact_id     =$_POST['contact_id'];
	$plan_id=$database->executeScalar("SELECT planId FROM tasks WHERE  taskId='".$taskId."'");
	$taskToEdit=$database->executeObject("SELECT * from tasks where taskId='".$taskId."'");
	//echo 'conatctId'.$contact_id;
	($_POST['completed']=='on'?$isCompleted=1:$isCompleted=0);
	$priorityId		=	$_POST['priorityId'];
	$notes			=	$_POST['notes'];
	//completed date
	$cdate= date('Y-m-d', strtotime($_POST['completeDate']));
	if($isCompleted==1){
		$completedDate=$cdate;
		}else{$completedDate=0;}
	//$dueTime	= 	$_POST['dueTime'];
if($_POST['dueDate']==''){
		$dueDate='0000-00-00';
	}else{
	$dueDate= date('Y-m-d', strtotime($_POST['dueDate'])); // outputs 2006-01-24
	}
	
	if($_POST['dueTime']==''){
		$dueTime='00:00:00';
	}else{
		// 12-hour time to 24-hour time 

	$dueTime= date("H:i:s", strtotime($_POST['dueTime'])); 

	}
	
	
	$oldIsCompleted = $database->executeScalar("SELECT isCompleted FROM tasks WHERE  taskId='".$taskId."'");
	
	if($isCompleted==1 && $oldIsCompleted == 0)
	{
		
			if($_POST['completeDate']!='')
			$completeDate= date('Y-m-d', strtotime($_POST['completeDate'])); // outputs 2006-01-24
			else
			$completeDate='0000-00-00';
			if($_POST['completeTime']!='')
			$completeTime= date("H:i:s", strtotime($_POST['completeTime'])); // outputs 2006-01-24
			else
			$completeTime='00:00:00';
			//echo "SELECT planId FROM tasks WHERE  taskId='".$taskId."'";
			$plan_id=$database->executeScalar("SELECT planId FROM tasks WHERE  taskId='".$taskId."'");
			
			//echo 'PlanId'.$plan_id;
			$contactIdsForTask=implode(',',$_POST['contactName']);
			
			 //$PlanTask=$taskObj->markTaskCompleted($userId,$taskId,$contactIdsForTask);
			
		
		
		

	}else
	{
			$completeDate='0000-00-00';
			$completeTime='00:00:00';
	}
	

	$dueDateTime=$dueDate." ".$dueTime;
	$completedDateTime=$completeDate." ".$completeTime;
	
	$reminder		=	$_POST['reminder'];
	$reminderValue	=	$_POST['reminderQuantity'];
	$reminderValueType	=	$_POST['reminderUnit'];
	$contactId     =$_POST['contact_id'];
	switch($reminderValueType)
	{
		case 'minutes':
			  $NumberOfMinutes=$reminderValue;
			  break;
		case 'hours':
			  $NumberOfMinutes=$reminderValue*60;
			break;
		case 'days':
		 	 $NumberOfMinutes=$reminderValue*60*24;
			break;
		default:
		 	$NumberOfMinutes=0;
	}
	
	$taskOccur			=	$_POST['occur'];
	$taskOccurFrequency	=	$_POST['frequency'];
	//custom year months data for occur
	$taskOccurYearly	=	$_POST['everyyear'];
	$yearMonths			=	explode(',',$_GET['yearMonth']);
	//custom month data for occur
	$taskOccurMonthly	=	$_POST['everyMonth'];
	$monthDays 		    =	explode(',',$_GET['monthDay']);
	$monthsWeek			=	$_POST['monthsWeek'];
	$monthsWeekDays		=	$_POST['monthsWeekDays'];
	//custom week data for occur
	$taskOccurWeekly	=	$_POST['everyWeeks'];
	$weekDays=array($_POST['sun'],$_POST['mon'],$_POST['tues'],$_POST['wed'],$_POST['thr'],$_POST['fri'],$_POST['sat']);
	//custom daily data for occur
	$taskOccurDaily		=	$_POST['everyDay'];
	$notOnWeekEnd		=	$_POST['notOnWeekEnd'];
	
	switch($notOnWeekEnd){
		case 'on':
			$isNotweekEnd=1;
		break;
		case 'off':
			 $isNotweekEnd=0;
		break;
		}
	
	switch($taskOccurFrequency){
		case 1:
			$every=$taskOccurDaily;
			break;
		case 2:
			$every=$taskOccurWeekly;
			break;
		case 3:
		 	$every=$taskOccurMonthly;
			break;
		case 4:
			$every=$taskOccurYearly;
			break;
		default:
		 	$every=0;
		 }
	
	if($_POST['endingOn']==''){
		$endingOn='0000-00-00';
	}else{
	$endingOn= date('Y-m-d', strtotime($_POST['endingOn'])); // outputs 2006-01-24
	}
	$contact			=	$_POST['contact'];
	//$contactId			=	$_POST['contactId'];
	$addedDate			=	date("Y-n-j");

	
	//query section for new task
	//echo "UPDATE tasks SET userId='".$userId	."', title='".$title."', isCompleted='".$isCompleted."', priorityId='".$priorityId."', notes='".$notes."', dueDateTime='".$dueDateTime."',addedDate='".$addedDate."',endingOn='".$endingOn."',completedDate=NOW(),repeatTypeId='".$taskOccur."'WHERE taskId='".$taskId."'";
	$database->executeNonQuery("UPDATE tasks SET userId='".$userId	."', title='".$title."', priorityId='".$priorityId."', notes='".mysql_real_escape_string($notes)."', dueDateTime='".$dueDateTime."', addedDate='".$addedDate."',endingOn='".$endingOn."',repeatTypeId='".$taskOccur."'WHERE taskId='".$taskId."'"); 
	if($isCompleted==1 && $oldIsCompleted == 0)
	{
		
			$contactIdsForTask=implode(',',$_POST['contactName']);
			$PlanTask=$taskObj->markTaskCompleted($userId,$taskId,$contactIdsForTask,$completeDate,$completeTime);
			 
	}
	else
	{
		if($plan_id==0 || $plan_id==NULL || $plan_id=='')
		{
				$contactIdsForTask=explode(',',$_POST['contactName']);
				if(empty($contactIdsForTask))
				{
					$database->executeNonQuery("DELETE FROM taskcontacts  WHERE taskId='".$taskId."'");
					
				}
				else
				{
					$database->executeNonQuery("DELETE FROM taskcontacts  WHERE taskId='".$taskId."'");
				
					for($i=0;$i<sizeof($contactIdsForTask);$i++)
					{
					
						$database->executeNonQuery("INSERT INTO taskcontacts(taskId, contactId) VALUES (".$taskId.",'".$contactIdsForTask[$i]."')");
				
					}
				}
		}
	}
	
	//this is last inserted id of task
	
	//$taskId=$database->insertid();
	
	//Insert task reminder 
	$customRuledId=$taskToEdit->customRuleId;
	$database->executeNonQuery("UPDATE taskreminder SET taskId='".$taskId."', reminderTypeId='".$reminder."', numberOfMinutes='".$NumberOfMinutes."', customValue='".$reminderValue."', customUnit='".$reminderValueType."' WHERE taskId='".$taskId."'" );
	
	//custom occurence 
	if(($taskToEdit->planId==0 || $taskToEdit->planId==NULL || $taskToEdit->planId=='') && ($customRuledId!=0 || $customRuledId!=''))
	{
		switch($taskOccur)
		{
			case 6:
			$database->executeNonQuery("UPDATE customrules SET frequency='".$taskOccurFrequency."', every='".$every."' WHERE customRuleId='".$customRuledId."'");
			//this is last inserted id of task
		
		
			if($taskOccurFrequency==1){
			$database->executeNonQuery("DELETE FROM customrulesdaily WHERE customRuleId='".$customRuledId."'");
			$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
											VALUES (".$customRuledId.",".$isNotweekEnd.")");	
				/*$database->executeNonQuery("UPDATE customrulesdaily SET  isNotweekEnd='".$isNotweekEnd."' WHERE customRuleId='".$customRuledId."'");	*/
			}
			if($taskOccurFrequency==2){
			$database->executeNonQuery("DELETE FROM customruleweekly WHERE customRuleId='".$customRuledId."'");
			$database->executeNonQuery("INSERT INTO customruleweekly
											(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday) 
											VALUES 
											(".$customRuledId.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."',
											 '".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");	
				/*$database->executeNonQuery("UPDATE customruleweekly
											SET  isSunday='".($weekDays[0]=='on'?1:0)."', isMonday='".($weekDays[1]=='on'?1:0)."', isTuesday=,'".($weekDays[2]=='on'?1:0)."', isWednesday='".($weekDays[3]=='on'?1:0)."', isThursday= '".($weekDays[4]=='on'?1:0)."', isFriday='".($weekDays[5]=='on'?1:0)."', isSaturday='".($weekDays[6]=='on'?1:0)."' WHERE customRuleId='".$customRuledId."'"); */
											
									
			}
			if($taskOccurFrequency==3){
				$database->executeNonQuery("DELETE FROM customrulemonthly WHERE customRuleId='".$customRuledId."'");
				$database->executeNonQuery("INSERT INTO customrulemonthly
				(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
				VALUES 
				(".$customRuledId.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
				 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
				 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
				 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
				 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
				 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
				 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
				 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
				 ".$monthsWeek.",".$monthsWeekDays.")");
			/*
				$database->executeNonQuery("UPDATE customrulemonthly SET isDays='1', is1=".($monthDays[0]!=0?1:0).", is2=".($monthDays[2]!=0?1:0).", is3=".($monthDays[3]!=0?1:0).", is4=".($monthDays[3]!=0?1:0).", is5=".($monthDays[4]!=0?1:0).", is6=".($monthDays[5]!=0?1:0).", is7=".($monthDays[6]!=0?1:0).", is8=".($monthDays[6]!=0?1:0).", is9=".($monthDays[8]!=0?1:0).", is10=".($monthDays[9]!=0?1:0).", is11=".($monthDays[10]!=0?1:0).", is12=".($monthDays[11]!=0?1:0).", is13=".($monthDays[12]!=0?1:0).", is14=".($monthDays[13]!=0?1:0).", is15=".($monthDays[14]!=0?1:0).", is16=".($monthDays[15]!=0?1:0).", is17=".($monthDays[16]!=0?1:0).", is18=".($monthDays[17]!=0?1:0).", is19=".($monthDays[18]!=0?1:0).", is20=".($monthDays[19]!=0?1:0).", is21=".($monthDays[20]!=0?1:0).", is22=".($monthDays[21]!=0?1:0).", is23=".($monthDays[22]!=0?1:0).", is24=".($monthDays[23]!=0?1:0).", is25=".($monthDays[24]!=0?1:0).", is26=".($monthDays[25]!=0?1:0).", is27=".($monthDays[26]!=0?1:0).", is28=".($monthDays[27]!=0?1:0).", is29=".($monthDays[28]!=0?1:0).", is30=".($monthDays[29]!=0?1:0).", is31=".($monthDays[30]!=0?1:0).", weekOfMonth=".$monthsWeek.", dayOfWeek=".$monthsWeekDays." WHERE customRuleId='".$customRuledId."'"); 
				*/
			}
			if($taskOccurFrequency==4){
		
			$database->executeNonQuery("DELETE FROM customrulemonthly WHERE customRuleId='".$customRuledId."'");
			/*$database->executeNonQuery("UPDATE customrulesyearly SET isJan=".($yearMonths[0]!=0?1:0).", isFeb=".($yearMonths[1]!=0?1:0).", isMar=".($yearMonths[2]!=0?1:0).", isApril=".($yearMonths[3]!=0?1:0).", isMay=".($yearMonths[4]!=0?1:0).", isJune=".($yearMonths[5]!=0?1:0).", isJuly=".($yearMonths[6]!=0?1:0).", isAug=".($yearMonths[7]!=0?1:0).", isSep=".($yearMonths[8]!=0?1:0).",isOct=".($yearMonths[9]!=0?1:0).",isNov=".($yearMonths[10]!=0?1:0).", isDec=".($yearMonths[11]!=0?1:0)." WHERE customRuleId='".$customRuledId."'"); */
			$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
				VALUES (".$customRuledId.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
				".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
				".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
				".($yearMonths[11]!=0?1:0).")");
				
			
			 }
		
			break;
		}
	}
	else
	{
		$database->executeNonQuery("INSERT INTO customrules(frequency, every) VALUES ('".$taskOccurFrequency."','".$every."')");
		//this is last inserted id of task
	$customRuledId=$database->insertid();
	
	if($taskOccurFrequency==1){
		
			$database->executeNonQuery("INSERT INTO customrulesdaily(customRuleId, isNotweekEnd) 
										VALUES (".$customRuledId.",".$isNotweekEnd.")");	
	}
	if($taskOccurFrequency==2){
			$database->executeNonQuery("INSERT INTO customruleweekly
										(customRuleId, isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday) 
										VALUES 
										(".$customRuledId.",'".($weekDays[0]=='on'?1:0)."','".($weekDays[1]=='on'?1:0)."','".($weekDays[2]=='on'?1:0)."','".($weekDays[3]=='on'?1:0)."','".($weekDays[4]=='on'?1:0)."',
										 '".($weekDays[5]=='on'?1:0)."','".($weekDays[6]=='on'?1:0)."')");
	}
	if($taskOccurFrequency==3){
	
		
		 	$database->executeNonQuery("INSERT INTO customrulemonthly
			(customRuleId, isDays, is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12, is13, is14, is15, is16, is17, is18, is19, is20, is21, is22, is23, is24, is25, is26, is27, is28, is29, is30, is31, weekOfMonth, dayOfWeek) 
			VALUES 
			(".$customRuledId.",'1',".($monthDays[0]!=0?1:0).",".($monthDays[1]!=0?1:0).",".($monthDays[2]!=0?1:0).",
			 ".($monthDays[3]!=0?1:0).",".($monthDays[4]!=0?1:0).",".($monthDays[5]!=0?1:0).",".($monthDays[6]!=0?1:0).",
			 ".($monthDays[7]!=0?1:0).",".($monthDays[8]!=0?1:0).",".($monthDays[9]!=0?1:0).",".($monthDays[10]!=0?1:0).",
			 ".($monthDays[11]!=0?1:0).",".($monthDays[12]!=0?1:0).",".($monthDays[13]!=0?1:0).",".($monthDays[14]!=0?1:0).",
			 ".($monthDays[15]!=0?1:0).",".($monthDays[16]!=0?1:0).",".($monthDays[17]!=0?1:0).",".($monthDays[18]!=0?1:0).",
			 ".($monthDays[19]!=0?1:0).",".($monthDays[20]!=0?1:0).",".($monthDays[21]!=0?1:0).",".($monthDays[22]!=0?1:0).",
			 ".($monthDays[23]!=0?1:0).",".($monthDays[24]!=0?1:0).",".($monthDays[25]!=0?1:0).",".($monthDays[26]!=0?1:0).",
			 ".($monthDays[27]!=0?1:0).",".($monthDays[28]!=0?1:0).",".($monthDays[29]!=0?1:0).",".($monthDays[30]!=0?1:0).",
			 ".$monthsWeek.",".$monthsWeekDays.")");
	}
	if($taskOccurFrequency==4){
	
		
			$database->executeNonQuery("INSERT INTO customrulesyearly( customRuleId, isJan, isFeb, isMar, isApril, isMay, isJune, isJuly, isAug, isSep,isOct ,isNov, isDec) 
			VALUES (".$customRuledId.",".($yearMonths[0]!=0?1:0).",".($yearMonths[1]!=0?1:0).",".($yearMonths[2]!=0?1:0).",
			".($yearMonths[3]!=0?1:0).",".($yearMonths[4]!=0?1:0).",".($yearMonths[5]!=0?1:0).",".($yearMonths[6]!=0?1:0).",
			".($yearMonths[7]!=0?1:0).",".($yearMonths[8]!=0?1:0).",".($yearMonths[9]!=0?1:0).",".($yearMonths[10]!=0?1:0).",
			".($yearMonths[11]!=0?1:0).")");
		
		 }
		$database->executeNonQuery("UPDATE tasks SET customRuleId='".$customRuledId."' WHERE taskId='".$taskId."'"); 

	}
		
		
	echo '1';
	}
?>