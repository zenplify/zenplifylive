<?php
session_start();
//$userId=$_SESSION['userId'];

include_once("dbconfig.php");
include_once("functions.php");
//weekly dats to show appiontment
function week_of_month($date) {
    $date_parts = explode('-', $date);
    $date_parts[2] = '01';
    $first_of_month = implode('-', $date_parts);
    $day_of_first = date('N', strtotime($first_of_month));
    $day_of_month = date('j', strtotime($date));
    return floor(($day_of_first + $day_of_month - 2) / 7) + 1;
}
function days_in_month($month, $year) 
{ 

return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
} 
function getDaysExceptWeekend(){
 $workdays = array();
$type = CAL_GREGORIAN;
$month = date('n'); // Month ID, 1 through to 12.
$year = date('Y'); // Year in 4 digit 2009 format.
$day=date('d');
$day_count = cal_days_in_month($type, $month, $year); // Get the amount of days
 
//loop through all days
for ($i = $day; $i <= $day_count; $i++) {
 
		$date = $year.'/'.$month.'/'.$i; //format date
		$get_name = date('l', strtotime($date)); //get week day
		$day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
 
		//if not a weekend add day to array
		if($day_name != 'Sun' && $day_name != 'Sat'){
			$workdays[] = $i;
		}
 
}
return $workdays;
}
//get weekend days
function getWeekendDays(){
 $workdays = array();
$type = CAL_GREGORIAN;
$month = date('n'); // Month ID, 1 through to 12.
$year = date('Y'); // Year in 4 digit 2009 format.
$day=date('d');
$day_count = cal_days_in_month($type, $month, $year); // Get the amount of days
 
//loop through all days
for ($i = $day; $i <= $day_count; $i++) {
 
		$date = $year.'/'.$month.'/'.$i; //format date
		$get_name = date('l', strtotime($date)); //get week day
		$day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
 
		//if not a weekend add day to array
		if($day_name != 'Mon' && $day_name != 'Tue'&&$day_name != 'Wed' && $day_name != 'Thu'&&$day_name != 'Fri'){
			$workdays[] = $i;
		}
 
}
return $workdays;
}
 function getDateForSpecificDayBetweenDates($startDate, $endDate, $weekdayNumber)
{
    $startDate = strtotime($startDate);
    $endDate = strtotime($endDate);

    $dateArr = array();

    do
    {
        if(date("w", $startDate) != $weekdayNumber)
        {
            $startDate += (24 * 3600); // add 1 day
        }
    } while(date("w", $startDate) != $weekdayNumber);


    while($startDate <= $endDate)
    {
        $dateArr[] = date('Y-m-d', $startDate);
        $startDate += (7 * 24 * 3600); // add 7 days
    }

    return($dateArr);
}
function addCalendar($st, $et, $sub, $ade){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "insert into `appointments` (`userId`,`title`, `startDateTime`, `endDateTime`, `isAllDayEvent`) values 
    		('"
      .$_SESSION['userId']."', '"	
      .mysql_real_escape_string($sub)."', '"
      .php2MySqlTime(js2PhpTime($st))."', '"
      .php2MySqlTime(js2PhpTime($et))."', '"
      .mysql_real_escape_string($ade)."' )";
    //echo($sql);
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = mysql_insert_id();
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}


function addDetailedCalendar($st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "insert into `jqcalendar` (`subject`, `starttime`, `endtime`, `isalldayevent`, `description`, `location`, `color`) values ('"
      .mysql_real_escape_string($sub)."', '"
      .php2MySqlTime(js2PhpTime($st))."', '"
      .php2MySqlTime(js2PhpTime($et))."', '"
      .mysql_real_escape_string($ade)."', '"
      .mysql_real_escape_string($dscr)."', '"
      .mysql_real_escape_string($loc)."', '"
      .mysql_real_escape_string($color)."' )";
    //echo($sql);
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = mysql_insert_id();
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function listCalendarByRange($sd, $ed, $srch){
  $ret = array();
  $ret['events'] = array();
  $ret["issort"] =true;
  $ret["start"] = php2JsTime($sd);
  $ret["end"] = php2JsTime($ed);
  $ret['error'] = null;
  try{
    $db = new DBConnection();
    $db->getConnection();
	if($srch==""){
	$sql = "select * from `appointments` where `startDateTime` between '"
      .php2MySqlTime($sd)."' and '". php2MySqlTime($ed)."' AND `userId`=".$_SESSION['userId'];
		}else{
	$sql = "select * from `appointments` where `startDateTime` between '"
      .php2MySqlTime($sd)."' and '". php2MySqlTime($ed)."' AND `userId`=".$_SESSION['userId']." AND  title LIKE '%".$srch."%'" ;		
    	}
	$handle = mysql_query($sql);
    //echo $sql;
    while ($row = mysql_fetch_object($handle)) {
      //$ret['events'][] = $row;
      //$attends = $row->AttendeeNames;
      //if($row->OtherAttendee){
      //  $attends .= $row->OtherAttendee;
      //}
      //echo $row->StartTime;$dateArr = getDateForSpecificDayBetweenDates('2010-01-01', '2011-12-31', 0);

//print "<pre>";
//print_r($dateArr);

    	
    	 if($row->repeatTypeId==6){

    	 	$result=mysql_query("select * from `customrules` where `customRuleId`='".$row->customRuleId."'");
    	 	$frequency=mysql_fetch_row($result);
			
    	 	if($frequency[1]==1){
    	 		$dailyresult=mysql_query("select * from customrulesdaily where customRuleId='".$row->customRuleId."'");
    	 		$dailyResult=mysql_fetch_row($dailyresult); 
				
				//get no of days  between start and end date
				
				$startDate =strtotime($row->startDateTime); // or your date as well
     			$endDate = strtotime($row->endDateTime);
				$endingOn= strtotime($row->endingOn);
     			$datediff = $endDate - $startDate;
     			$noOfDaysDailyRepeat=floor($datediff/(60*60*24));//no of days repeat
				$repeatAfterDays=$frequency[2];//repeat after days	    	 		
				$curYear=date('Y',$startDate);
				$curMonth=date('m',$startDate);
				$curDay=date('d',$startDate);
				$loopInterval=floor(($endingOn-$startDate)/(60*60*24));
				$monthdays=getDaysExceptWeekend();
				$monthSize=days_in_month($curMonth,$curYear);
   			if($dailyResult[2]==1){//if not weekend
    	 	$st=$curDay;
			for($m=1;$m<=$loopInterval-$repeatAfterDays;$m++){
				
				$ed=$st+$noOfDaysDailyRepeat;
						$ret['events'][] = array(
    	 						$row->appiontmentId,
    	 						$row->title,
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$curMonth.'-'.$st)),
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$curMonth.'-'.$ed)),
    	 						$row->isAllDayEvent,
    	 						0, //more than one day event
    	 						//$row->InstanceType,
    	 						0,//Recurring event,
    	 						0,
    	 						1,//editable
    	 						$row->location,
    	 						$row->repeatTypeId//$attends
    	 				);
					$st=$st+$repeatAfterDays;	
						}
    	 	
			}else{
				
				$st=$curDay;
			for($m=1;$m<=$loopInterval-$repeatAfterDays;$m++){
				
				$ed=$st+$noOfDaysDailyRepeat;
						$ret['events'][] = array(
    	 						$row->appiontmentId,
    	 						$row->title,
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$curMonth.'-'.$st)),
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$curMonth.'-'.$ed)),
    	 						$row->isAllDayEvent,
    	 						0, //more than one day event
    	 						//$row->InstanceType,
    	 						0,//Recurring event,
    	 						0,
    	 						1,//editable
    	 						$row->location,
    	 						$row->repeatTypeId//$attends
    	 				);
					$st=$st+$repeatAfterDays;	
						}
				}
			
    	 	}
			if($frequency[1]==2){
    	 		$weeklyresult=mysql_query("select * from customruleweekly where customRuleId='".$row->customRuleId."'");
    	 		$weeklyResult=mysql_fetch_row($weeklyresult);
    	 		$endingOn=$row->endingOn;
				$endingOndate=explode(' ',$endingOn);
				$endingOnDate=explode('-',$endingOndate[0]);
				$endingOnYear=$endingOnDate[0];
				$endingOnMonth=$endingOnDate[1];
				$endingOnDay=$endingOnDate[2];
				
				$startDate =strtotime($row->startDateTime); // or your date as well
     			$endDate = strtotime($row->endDateTime);
				$endingOn= strtotime($row->endingOn);
     			   	 		
				$curYear=date('Y',$startDate);
				$curMonth=date('m',$startDate);
				$curDay=date('d',$startDate);
				$monthdays=getDaysExceptWeekend();
				$monthSize=days_in_month($curMonth,$curYear);
   			
				for($w=2;$w<=8;$w++){
					if($weeklyResult[$w]==1){
						$dateArr = getDateForSpecificDayBetweenDates($curYear.'-'.$curMonth.'-'.'1',$curYear.'-'.$curMonth.'-'.$monthSize , $w-2);					
									for($k=1;$k<=sizeof($dateArr);$k++){
							$ret['events'][] = array(
									$row->appiontmentId,
									$row->title,
									php2JsTime(mySql2PhpTime($dateArr[$k-1])),
									php2JsTime(mySql2PhpTime($dateArr[$k-1])),
									$row->isAllDayEvent,
									0, //more than one day event
									//$row->InstanceType,
									0,//Recurring event,
									0,
									1,//editable
									$row->location,
									$row->repeatTypeId//$attends
							);
							}
	
							
	
						}
				}
			
    	 	}
    	 	if($frequency[1]==3){
    	 		$monthresult=mysql_query("select * from `customrulemonthly` where `customRuleId`='".$row->customRuleId."'");
    	 		$monthlyResult=mysql_fetch_row($monthresult);
    	 		$month=1;
				$curYear=date('Y');
				$curMonth=date('m');
				for($noOfMonths=$curMonth+1;$noOfMonths<=$curMonth+$frequency[2];$noOfMonths++)
    	 		{
				for($i=3;$i<33;$i++){
    	 	
    	 			if($monthlyResult[$i]==1){
    	 				$ret['events'][] = array(
    	 						$row->appiontmentId,
    	 						$row->title,
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$noOfMonths.'-'.$month)),
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$noOfMonths.'-'.$month)),
							
    	 						$row->isAllDayEvent,
    	 						0, //more than one day event
    	 						//$row->InstanceType,
    	 						0,//Recurring event,
    	 						0,
    	 						1,//editable
    	 						$row->location,
    	 						$row->repeatTypeId//$attends
    	 				);
    	 				 
    	 			}
    	 	
    	 			$month++;
    	 		}
			}
    	 	}
    	 	if($frequency[1]==4){
    	 	$monthresult=mysql_query("select * from customrulesyearly where `customRuleId`='".$row->customRuleId."'");
    	 		$monthlyResult=mysql_fetch_row($monthresult);
    	 		$month=1;
				$curYear=date('Y');
				$curMonth=date('m');
				$monthSize=days_in_month($curMonth,$curYear);
    	 		for($j=2;$j<14;$j++){
    	 	
    	 			if($monthlyResult[$j]==1){
    	 				for($m=1;$m<=$monthSize;$m++){
						$ret['events'][] = array(
    	 						$row->appiontmentId,
    	 						$row->title,
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$month.'-'.$m)),
    	 						php2JsTime(mySql2PhpTime($curYear.'-'.$month.'-'.$m)),
    	 						$row->isAllDayEvent,
    	 						0, //more than one day event
    	 						//$row->InstanceType,
    	 						0,//Recurring event,
    	 						0,
    	 						1,//editable
    	 						$row->location,
    	 						$row->repeatTypeId//$attends
    	 				);
						}
    	 			}
    	 	
    	 			$month++;
    	 		}
    	 	}
    	 	}elseif($row->repeatTypeId==5){
				$endingOn=$row->endingOn;
				$endingOndate=explode(' ',$endingOn);
				$endingOnDate=explode('-',$endingOndate[0]);
				$endingOnYear=$endingOnDate[0];
				$endingOnMonth=$endingOnDate[1];
				$endingOnDay=$endingOnDate[2];
				
				$startDateTime=$row->startDateTime;
				$date=explode(' ',$startDateTime);
				$startDate=explode('-',$date[0]);
				$startYear=$startDate[0];
				$startMonth=$startDate[1];
				$startDay=$startDate[2];
				
				$endDateTime=$row->endDateTime;
				$enddate=explode(' ',$endDateTime);
				$EndDate=explode('-',$enddate[0]);
				$endYear=$EndDate[0];
				$endMonth=$EndDate[1];
				$endDay=$EndDate[2];
				$days=$endDay-$startDay;
				
   				
				$fromDate = $date[0];
				$toDate =$endingOndate[0];
				$dateMonthYearArr = array();
				$fromDateTS = strtotime($date[0]);
				$toDateTS = strtotime($endingOndate[0]);
				
				for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
				// use date() and $currentDateTS to format the dates in between
				$currentDateStr = date('Y-m-d',$currentDateTS);
				$dateMonthYearArr[] = $currentDateStr;
			
				}
				for($ed=0;$ed<sizeof($dateMonthYearArr);$ed++){
					
					
					$d=strtotime($dateMonthYearArr[$ed]);
					$std=date('Y-m-d',$d);
					$date=strtotime("+".$days." day", $d);
					$end=date('Y-m-d',$date);
					
					$ret['events'][] = array($row->appiontmentId,$row->title,
    	 						php2JsTime(mySql2PhpTime($std)),
    	 						php2JsTime(mySql2PhpTime($end)),
    	 						$row->isAllDayEvent,0,0,0,1,$row->location,$row->repeatTypeId);
				}
			}elseif($row->repeatTypeId==4){
				
				$endingOn=$row->endingOn;
				$endingOndate=explode(' ',$endingOn);
				$endingOnDate=explode('-',$endingOndate[0]);
				$endingOnYear=$endingOnDate[0];
				$endingOnMonth=$endingOnDate[1];
				$endingOnDay=$endingOnDate[2];
				
				$startDateTime=$row->startDateTime;
				$date=explode(' ',$startDateTime);
				$startDate=explode('-',$date[0]);
				$startYear=$startDate[0];
				$startMonth=$startDate[1];
				$startDay=$startDate[2];
				
				$endDateTime=$row->endDateTime;
				$enddate=explode(' ',$endDateTime);
				$EndDate=explode('-',$enddate[0]);
				$endYear=$EndDate[0];
				$endMonth=$EndDate[1];
				$endDay=$EndDate[2];
				$days=$endDay-$startDay;
				
   				
				$fromDate = $date[0];
				$toDate =$endingOndate[0];
				$dateMonthYearArr = array();
				$fromDateTS = strtotime($date[0]);
				$toDateTS = strtotime($endingOndate[0]);
				
				for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
				// use date() and $currentDateTS to format the dates in between
				$currentDateStr = date('Y-m-d',$currentDateTS);
				$dateMonthYearArr[] = $currentDateStr;
			
				}
				for($ed=0;$ed<sizeof($dateMonthYearArr);$ed++){
					
					
					$d=strtotime($dateMonthYearArr[$ed]);
					$std=date('Y-m-d',$d);
					$date=strtotime("+".$days." day", $d);
					$end=date('Y-m-d',$date);
					
					$ret['events'][] = array($row->appiontmentId,$row->title,
    	 						php2JsTime(mySql2PhpTime($std)),
    	 						php2JsTime(mySql2PhpTime($end)),
    	 						$row->isAllDayEvent,0,0,0,1,$row->location,$row->repeatTypeId);
					
					}
				
			}elseif($row->repeatTypeId==2){
				$endingOn=$row->endingOn;
				$endingOndate=explode(' ',$endingOn);
				$endingOnDate=explode('-',$endingOndate[0]);
				$endingOnYear=$endingOnDate[0];
				$endingOnMonth=$endingOnDate[1];
				$endingOnDay=$endingOnDate[2];
				
				$startDateTime=$row->startDateTime;
				$date=explode(' ',$startDateTime);
				$startDate=explode('-',$date[0]);
				$startYear=$startDate[0];
				$startMonth=$startDate[1];
				$startDay=$startDate[2];
				
				$endDateTime=$row->endDateTime;
				$enddate=explode(' ',$endDateTime);
				$EndDate=explode('-',$enddate[0]);
				$endYear=$EndDate[0];
				$endMonth=$EndDate[1];
				$endDay=$EndDate[2];
				$days=$endDay-$startDay;
				
   				
				$fromDate = $date[0];
				$toDate =$endingOndate[0];
				$dateMonthYearArr = array();
				$fromDateTS = strtotime($date[0]);
				$toDateTS = strtotime($endingOndate[0]);
				
				for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
				// use date() and $currentDateTS to format the dates in between
				$currentDateStr = date('Y-m-d',$currentDateTS);
				$dateMonthYearArr[] = $currentDateStr;
			
				}
				for($ede=0;$ede<sizeof($dateMonthYearArr);$ede=$ede+7){
					
					
					$dw=strtotime($dateMonthYearArr[$ede]);
					$stdw=date('Y-m-d',$dw);
					$date=strtotime("+".$days." day", $dw);
					$endw=date('Y-m-d',$date);
					
					$ret['events'][] = array($row->appiontmentId,$row->title,
    	 						php2JsTime(mySql2PhpTime($stdw)),
    	 						php2JsTime(mySql2PhpTime($endw)),
    	 						$row->isAllDayEvent,0,0,0,1,$row->location,$row->repeatTypeId);
				}
			}elseif($row->repeatTypeId==3){//shows monthly its work fine
				
				$endingOn=$row->endingOn;
				$endingOndate=explode(' ',$endingOn);
				$endingOnDate=explode('-',$endingOndate[0]);
				$endingOnYear=$endingOnDate[0];
				$endingOnMonth=$endingOnDate[1];
				$endingOnDay=$endingOnDate[2];
				
				$startDateTime=$row->startDateTime;
				$date=explode(' ',$startDateTime);
				$startDate=explode('-',$date[0]);
				$startYear=$startDate[0];
				$startMonth=$startDate[1];
				$startDay=$startDate[2];
				
				$endDateTime=$row->endDateTime;
				$enddate=explode(' ',$endDateTime);
				$EndDate=explode('-',$enddate[0]);
				$endYear=$EndDate[0];
				$endMonth=$EndDate[1];
				$endDay=$EndDate[2];
				
				
   				$date1 = strtotime($row->startDateTime);
				$date2 = strtotime($row->endingOn);
				$months = 0;
				$sm=1;
				$sy=$startYear;
				while (($date1 = strtotime('+1 MONTH', $date1)) <= $date2)
    					$months++;
				
				for($m=$startMonth;$m<=($months+($startMonth-1));$m++){
					if(($m/12)==0){
						$sm=1;
						$sy++;
						}else{
							$sm=$m;
							}
						$ret['events'][] = array($row->appiontmentId,$row->title,
    	 						php2JsTime(mySql2PhpTime($sy.'-'.$sm.'-'.$startDay)),
    	 						php2JsTime(mySql2PhpTime($sy.'-'.$sm.'-'.$endDay)),
    	 						$row->isAllDayEvent,0,0,0,1,$row->location,$row->repeatTypeId);
					
					
					}
						
							
						
						
			}else{
    	 	
    	$ret['events'][] = array(
        $row->appiontmentId,
        $row->title,
        php2JsTime(mySql2PhpTime($row->startDateTime)),
        php2JsTime(mySql2PhpTime($row->endDateTime)),
        $row->isAllDayEvent,
        0, //more than one day event
        //$row->InstanceType,
        0,//Recurring event,
        0,
        1,//editable
        $row->location, 
        $row->repeatTypeId//$attends
      );
    	 }
    }
	}catch(Exception $e){
     $ret['error'] = $e->getMessage();
  }
  return $ret;
}

function listCalendar($day, $type, $srch){
  $phpTime = js2PhpTime($day);
  //echo $phpTime . "+" . $type;
  switch($type){
    case "month":
      $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime)+1, 1, date("Y", $phpTime));
      break;
    case "week":
      //suppose first day of a week is monday 
      $monday  =  date("d", $phpTime) - date('N', $phpTime) + 1;
      //echo date('N', $phpTime);
      $st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
      $et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
      break;
    case "day":
      $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
      break;
  }
  //echo $st . "--" . $et;
  return listCalendarByRange($st, $et,$srch);
}

function updateCalendar($id, $st, $et){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "update `appointments` set"
      . " `startDateTime`='" . php2MySqlTime(js2PhpTime($st)) . "', "
      . " `endDateTime`='" . php2MySqlTime(js2PhpTime($et)) . "' "
      . "where `appiontmentId`=" . $id;
    //echo $sql;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function updateDetailedCalendar($id, $st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "update `appointments` set"
      . " `startDateTime`='" . php2MySqlTime(js2PhpTime($st)) . "', "
      . " `endDateTime`='" . php2MySqlTime(js2PhpTime($et)) . "', "
      . " `title`='" . mysql_real_escape_string($sub) . "', "
      . " `isAllDayEvent`='" . mysql_real_escape_string($ade) . "', "
      . " `notes`='" . mysql_real_escape_string($dscr) . "', "
      . " `location`='" . mysql_real_escape_string($loc) . "'"
      . "where `appiontmentId`=" . $id;
    //echo $sql;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function removeCalendar($id){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "delete from `appointments` where `appiontmentId`=" . $id;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}




header('Content-type:text/javascript;charset=UTF-8');
$method = $_GET["method"];

$srch=$_GET["srch"];

switch ($method) {
    case "add":
        $ret = addCalendar($_POST["CalendarStartTime"], $_POST["CalendarEndTime"], $_POST["CalendarTitle"], $_POST["IsAllDayEvent"]);
        break;
    case "list":
	
        $ret = listCalendar($_POST["showdate"], $_POST["viewtype"],$srch);
        break;
    case "update":
        $ret = updateCalendar($_POST["calendarId"], $_POST["CalendarStartTime"], $_POST["CalendarEndTime"]);
        break; 
    case "remove":
        $ret = removeCalendar( $_POST["calendarId"]);
        break;
    case "adddetails":
        $st = $_POST["stpartdate"] . " " . $_POST["stparttime"];
        $et = $_POST["etpartdate"] . " " . $_POST["etparttime"];
        if(isset($_GET["id"])){
            $ret = updateDetailedCalendar($_GET["id"], $st, $et, 
                $_POST["Subject"], isset($_POST["IsAllDayEvent"])?1:0, $_POST["Description"], 
                $_POST["Location"], $_POST["colorvalue"], $_POST["timezone"]);
        }else{
            $ret = addDetailedCalendar($st, $et,                    
                $_POST["Subject"], isset($_POST["IsAllDayEvent"])?1:0, $_POST["Description"], 
                $_POST["Location"], $_POST["colorvalue"], $_POST["timezone"]);
        }        
        break; 


}
echo json_encode($ret); 



?>