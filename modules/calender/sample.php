<?php 
//include('../../classes/resource.php');
error_reporting(0);
session_start();
include_once("../headerFooter/headercalendar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <title>	My Calendar </title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link href="css/dailog.css" rel="stylesheet" type="text/css" />
    <link href="css/calendar.css" rel="stylesheet" type="text/css" /> 
    <link href="css/dp.css" rel="stylesheet" type="text/css" />   
    <link href="css/alert.css" rel="stylesheet" type="text/css" /> 
    <link href="css/main.css" rel="stylesheet" type="text/css" /> 
    
<script type="text/javascript" src="../../js/script.js"></script>
    <script src="src/jquery.js" type="text/javascript"></script>  
    
    <script src="src/Plugins/Common.js" type="text/javascript"></script>    
    <script src="src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>     
    <script src="src/Plugins/jquery.datepicker.js" type="text/javascript"></script>

    <script src="src/Plugins/jquery.alert.js" type="text/javascript"></script>    
    <script src="src/Plugins/jquery.ifrmdailog.js" defer="defer" type="text/javascript"></script>
    <script src="src/Plugins/wdCalendar_lang_US.js" type="text/javascript"></script>    
    <script src="src/Plugins/jquery.calendar.js" type="text/javascript"></script>   
    
    <script type="text/javascript">
        $(document).ready(function() {   
		  
           var view="month";          
           
            var DATA_FEED_URL = "php/datafeed.db.php";
            var op = {
                view: view,
                theme:3,
                showday: new Date(),
                EditCmdhandler:Edit,
                DeleteCmdhandler:Delete,
                ViewCmdhandler:View,    
                onWeekOrMonthToDay:wtd,
                onBeforeRequestData: cal_beforerequest,
                onAfterRequestData: cal_afterrequest,
                onRequestDataError: cal_onerror, 
                autoload:true,
                url: DATA_FEED_URL + "?method=list",  
                quickAddUrl: DATA_FEED_URL + "?method=add", 
                quickUpdateUrl: DATA_FEED_URL + "?method=update",
                quickDeleteUrl: DATA_FEED_URL + "?method=remove"        
            };
            var $dv = $("#calhead");
            var _MH = document.documentElement.clientHeight;
            var dvH = $dv.height() + 2;
            op.height = _MH - dvH;
            op.eventItems =[];

            var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
            if (p && p.datestrshow) {
                $("#txtdatetimeshow").text(p.datestrshow);
            }
            $("#caltoolbar").noSelect();
            
            $("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: $("#txtdatetimeshow"),
            onReturn:function(r){                          
                            var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                            if (p && p.datestrshow) {
                                $("#txtdatetimeshow").text(p.datestrshow);
                            }
                     } 
            });
            function cal_beforerequest(type)
            {
                var t="Loading data...";
                switch(type)
                {
                    case 1:
                        t="Loading data...";
                        break;
                    case 2:                      
                    case 3:  
                    case 4:    
                        t="The request is being processed ...";                                   
                        break;
                }
                $("#errorpannel").hide();
                $("#loadingpannel").html(t).show();    
            }
            function cal_afterrequest(type)
            {
                switch(type)
                {
                    case 1:
                        $("#loadingpannel").hide();
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $("#loadingpannel").html("Success!");
                        window.setTimeout(function(){ $("#loadingpannel").hide();},2000);
                    break;
                }              
               
            }
            function cal_onerror(type,data)
            {
                $("#errorpannel").show();
            }
            function Edit(data)
            {
               var eurl="edit.db.php?id={0}&start={2}&end={3}&isallday={4}&title={1}";   
                if(data)
                {
                    var url = StrFormat(eurl,data);
                    OpenModelWindow(url,{ width: 600, height: 400, caption:"Edit Appiontment",onclose:function(){
                       $("#gridcontainer").reload();
                    }});
                }
            }    
            function View(data)
            {
                var str = "";
                $.each(data, function(i, item){
                    str += "[" + i + "]: " + item + "\n";
                });
                alert(str);               
            }    
            function Delete(data,callback)
            {           
                
                $.alerts.okButton="Ok";  
                $.alerts.cancelButton="Cancel";  
                hiConfirm("Are You Sure to Delete this Appiontment", 'Confirm',function(r){ r && callback(0);});           
            }
            function wtd(p)
            {
               if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $("#showdaybtn").addClass("fcurrent");
            }
            //to show day view
            $("#showdaybtn").click(function(e) {
                //document.location.href="#day";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("day").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //to show week view
            $("#showweekbtn").click(function(e) {
                //document.location.href="#week";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("week").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //to show month view
            $("#showmonthbtn").click(function(e) {
                //document.location.href="#month";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("month").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
            $("#showreflashbtn").click(function(e){
                $("#gridcontainer").reload();
            });
            
            //Add a new event
            $("#faddbtn").click(function(e) {
                var url ="edit.db.php";
                OpenModelWindow(url,{ width: 500, height: 400, caption: "Create New Calendar"});
            });
            //go to today
            $("#showtodaybtn").click(function(e) {
                var p = $("#gridcontainer").gotoDate().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }


            });
            //previous date range
            $("#sfprevbtn").click(function(e) {
				//alert(op);
				op['url'] = DATA_FEED_URL + "?method=list&val=1";
                var p = $("#gridcontainer").previousRange().BcalGetOp();
                if (p && p.datestrshow) {
					 //$("#gridcontainer").reload();
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //next date range
            $("#sfnextbtn").click(function(e) {
                var p = $("#gridcontainer").nextRange().BcalGetOp();
				if (p && p.datestrshow) {
				    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
        });
		function editTask(editType){
				var appiontmentId=$("#taskId").val();
				
			
				 var sd = new Date($("#taskstdate").val());
				 var startdate=[ sd.getFullYear(), sd.getMonth()+1,sd.getDate()].join('-');
				 var ed = new Date($("#taskeddate").val());
				 var enddate=[ ed.getFullYear(), ed.getMonth()+1,ed.getDate()].join('-');
  
				if(editType==1){
					window.location.href='../appiontment/edit_appiontment.php?edit_type='+editType+'&appId='+appiontmentId+'&stdate='+startdate+'&eddate='+enddate;
				}else{
					
					window.location.href='../appiontment/edit_appiontment.php?edit_type='+editType+'&appId='+appiontmentId+'&stdate='+startdate+'&eddate='+enddate;
					}
				}
		/*function deleteReoccurTask(deleteType){
			var appiontmentId=$("#taskIdToDelete").val();
			
			if(deleteType==1){
					//reoccuer
				}else{
					$.ajax({    
			type    : "POST",
			url     : "php/datafeed.db.php?method=remove",
			data    :{calendarId:appiontmentId},                
			success: function(data) {
				alert(data);
				}
    		});	
					
					}
			}		*/
    </script>    
</head>
<body>
<?php include('../../modules/dashboards/header.php'); ?>

<div class="container">
<div style="float:left">
<h1 class="gray" style="width:100px">Calendar </h1>
</div>
<div>

<div style="float:right; padding-top:15px;"><p id="manage"><a href="../appiontment/new_appiontment.php"><input type="button" name="manage_contacts" class="ad_appointment" value="" /></a></p>
</div><div style="clear:both"></div>
 <div style="float:right;">   
      <div id="calhead" style="padding-left:1px;padding-right:1px;">          
              <div class="cHead"><div class="ftitle">My Calendar</div>
            <div id="loadingpannel" class="ptogtitle loadicon" style="display: none;">Loading data...</div>
             <div id="errorpannel" class="ptogtitle loaderror" style="display: none;">Sorry, could not load your data, please try again later</div>
            </div>          
            
            <div id="caltoolbar" class="ctoolbar">
              
            <div class="btnseparator"></div>
             
            <div id="showdaybtn" class="fbutton">
                <div><span title='Day' class="showdayview">Day</span></div>
            </div>
              <div  id="showweekbtn" class="fbutton">
                <div><span title='Week' class="showweekview">Week</span></div>
            </div>
              <div  id="showmonthbtn" class="fbutton fcurrent">
                <div><span title='Month' class="showmonthview">Month</span></div>

            </div>
            <div class="btnseparator"></div>
              <div  id="showreflashbtn" class="fbutton">
                <div><span title='Refresh view' class="showdayflash">Refresh</span></div>
                </div>
             <div class="btnseparator"></div>
            <div id="sfprevbtn" title="Prev"  class="fbutton">
              <span class="fprev"></span>

            </div>
            <div id="sfnextbtn" title="Next" class="fbutton">
                <span class="fnext"></span>
            </div>
            <div class="fshowdatep fbutton">
                    <div>
                        <input type="hidden" name="txtshow" id="hdtxtshow" />
                        <span id="txtdatetimeshow"><?php echo date('M').' '.date('Y');?></span>

                    </div>
            </div>
            
            <div class="clear"></div>
            </div>
      </div>
      <div style="padding:1px;">

        <div class="t1 chromeColor">
            &nbsp;</div>
        <div class="t2 chromeColor">
            &nbsp;</div>
        <div id="dvCalMain" class="calmain printborder">
            <div id="gridcontainer" style="overflow-y: visible;">
            </div>
        </div>
        <div class="t2 chromeColor">

            &nbsp;</div>
        <div class="t1 chromeColor">
            &nbsp;
        </div>   
        </div>
   </div>  
  </div>
  <div id="Popup"></div>

<div id="popupReoccur" >
  <h1 class="gray">Changing a recurring Appiontment</h1>
  <input type="hidden" name="taskId" id="taskId" />
    <input type="hidden" name="taskstdate" id="taskstdate" />
      <input type="hidden" name="taskeddate" id="taskeddate" />
  <span class="popupEditTask">You're changing a recurring appiontment. Do you want to change only this Appiontment, or all future Appiontments?</span>
  <br /><br />
  <span ><input type="button" value="" class="onlyThisAppiontment" onclick="editTask('1')" /> <input type="button" value="" onclick="editTask('0')" class="allFutureAppiontment"/><input type="button" name="cancel" class="cancel" value="" onClick="disablereoccurPopup()"/></span>


           
</div> 
<div id="popupDeleteReoccur" >
  <h1 class="gray">Deleteing  recurring task</h1>
  <input type="hidden" name="taskId" id="taskIdToDelete" />
  <span class="popupEditTask">You're deleting a recurring task. Do you want to delete only this task, or all future tasks?</span>
  <br /><br />
  <span ><input type="button" value="" class="onlyThisTask" onclick="deleteReoccurTask('1')" /> <input type="button" value="" onclick="deleteReoccurTask('0')" class="allFutureTask"/><input type="button" name="cancel" class="cancel" value="" onClick="disabledeletePopup()"/></span>
</div> 

<!--<div class="col_search" style="width:185px !important;">
     <p class="search_header">Find Appiontment</p>
     <input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" style="width:182px !important;" />
</div>-->
   
<?php include_once("../headerFooter/footer.php");?>