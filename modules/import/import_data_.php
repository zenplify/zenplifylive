<?php
session_start();
//Condition will be true if institute logged in
if(isset($_SESSION['institutefolder']))
{

		$instituteid=$_SESSION['instituteid'];
		$institutefolder=$_SESSION['institutefolder'];
		$usertype=$_SESSION['usertype'];
		$classid=$_SESSION['classid'];
		$sectionid=$_SESSION['sectionid'];
		
		extract($_REQUEST);
		$xmlTag=$_REQUEST['xmlTag'];
		$_SESSION['importXMLTagName']=$_REQUEST['xmlTag'];
		
		$file = split(",",fgets(fopen("../../../path.txt",'r')));
		require_once ($file[0]);//class configuration.php is included
		require_once ($file[1]);//class messages.php is included
		$conf = configuration::getInstance();
		
		//Include Local Classes
		require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['InstitutesFolder'].$institutefolder.$conf->paths['institutesupportclasses'].$conf->paths['clsInstituteSettings'] );
		$instConfs = instituteconfs::getInstance();
		$instituteId=$instConfs->settings['InstituteId'];
		$srcDir=$conf->paths['L4i'];
		
		
}
else
{
	  header('Location:error.php');
}
$_SESSION['import_error_file_path']=$conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportmodules']."import";
$_SESSION['instituteFolderPath']=$srcDir.$conf->paths['InstitutesFolder'].$institutefolder;
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportclasses'].$conf->paths['clsClass_db']);
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportclasses'].$conf->paths['clsImport']);
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint'].$conf->paths['cssConfig']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TaleemPortal</title>
<link href="<?php echo $srcDir.$conf->paths['InstitutesFolder'].$institutefolder;?>css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/internal/prototype.js"></script>


<?php echo loadCSS($srcDir);
?>


</head>
<body id="inner">
				<div id="wrapper_l">
					  <div id="logo">
						<h1>taleem Portal of Pakistan school management system</h1>
						<div class="subnav"></div>
					  </div>
				</div>  
	<div id="wrapper">
	 
		<div id="lower_body_in">
				<?php			
					include($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint']."left_menu.php"); 
				?>
				

			<div class="right">
				<div class="bpanel">
					<?php 
				$className=class_form::GetClassName($classid);
				$sectionName=section_form::GetSectionName($sectionid);
 				echo "<h2>Import Data </h2>"; // Students in Class".$className."/".$sectionName."</h2>";
				?>
					  
					  
					  
			   <?php
					echo "<img src=".$srcDir.$conf->paths['supportimage']."loadingAnimation1.gif id=\"loading_image\" style=\"display:none\" />";
				?>
					 
							<br class="clear" /><br />
					  <div id="show_file" style="margin:0px 0px 0px 0px;display:none;">
					 	 <a href="javascript:void(0)" onclick="window.open('importError.txt')"><b> <u>Open File</u> </b></a>
					  </div>
					  <div class="grid" id="step1">
					  <div id="message1"></div>
						   <form  name="import_form"action="<?php $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
						 <?php 
						 $csvPath=$_SESSION['csvPath'];
						//echo $csvPath;
						 $imp=new import();
						 echo $imp->MappingFields("import.xml",$csvPath,$xmlTag);
						
						//unlink($csvPath);
						 ?>
						 
						 <br /><br />
						 <div style="margin-right:auto;margin-left:auto;width:70%">
							<div class="back_btn" ><input  type="button" value="Cancel" name="back" onclick="history.go(-2);"/></div>
				 			<div class="gw_btn" ><input  type="button" value="Import" name="submit_map" onclick="GetMappingValues()"/></div>
				 			<?php if($_REQUEST['xmlTag']=="Student") { ?>	
				 			<div class="gw_btn" id="go_to_section" style="display:none;"><input  type="button" value="Go to Section" name="go_to_section" onclick="window.location='../branch/student/view_all_students.php?section_id=<?php echo $sectionid; ?>'"/></div>
				 			<?php }?>
				 		</div>
						 </form>
						 
						</div><!--grid-->
							
		
						<br class="clear" />
				  </div><!--bpanel-->
				  <div class="bbpanel"></div>
			 </div><!--right-->
		 </div><!--lower_body_in-->
			<br class="clear" /><br /><br class="clear" /><br /><br class="clear" /><br />
		 	<br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br />
	 		<?php 
			include($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint']."footer.php");
			?>
	</div><!--wrapper-->
</body>
</html>

<script type="text/javascript">





function GetFileContent()
{
	document.getElementById("step1").style.display="none";
	
}	
/*
function getSeletedIndex(si)
{
//alert(si);
}	
*/
function GetMappingValues()
{
	
	var elementCount=document.forms['import_form'].elements.length;
	//alert(document.getElementById("dbField").length);
	//alert(document.getElementById("csvField").length);
		
 	
	//var tableArray=new Array();
	//tableArray=tableList.split(',');
	//alert(tableArray.length);
	var is_header=0;
	if(document.forms[0].is_header.checked)
	{
		//alert("checked");
		var is_header=1;
	}
	var ddCount=document.forms[0].dbField.length;
	//alert(ddCount);
	var dbList=new Array();
	var csvList=new Array();

	for(i=0;i<ddCount;i++)
	{
		var sidb = document.forms[0].dbField[i];
		var sicsv = document.forms[0].csvField[i];

		if(sidb.selectedIndex!=0 && sicsv.selectedIndex!=0)
		{
			///Append Table Name with Key get from DropDown Value
			//var dbvalue = sidb.options[sidb.selectedIndex].text+":"+sidb.options[sidb.selectedIndex].value;
			var dbvalue =  sidb.options[sidb.selectedIndex].value;
			var csvvalue = sicsv.options[sicsv.selectedIndex].text;
			dbList+=dbvalue+',';
			csvList+=(sicsv.selectedIndex-1)+',';
		 }
		
	 }
	 
	 // alert(dbList);
	 // alert(csvList);
	 
	 var dbFieldCount=document.forms[0].dbField.length;
	 var rNSelect=0;
	 var nameSelect=0;
	 //alert(dbFieldCount);

	 var xmlTag= '<?php echo $xmlTag ?>';
	 
	 if(dbList!="" && csvList!="")
	 {
	 	
	 	for(i=0;i<dbFieldCount;i++)
	 	{
	 		var dbf=document.forms[0].dbField[i];	 	
	 		if(dbf.options[dbf.selectedIndex].text=="Roll Number")
	 		{
	 			rNSelect=1;
	 		}
	 		
	 	}
	 	if(rNSelect=='0' && xmlTag!='Staff')
	 	{
	 		alert("Please select Roll Number");
	 	}
	 	
	 	else
	 	{
			ImportValues(dbList,csvList,is_header);
		}
	 }
	 else
	 {
	 	alert("Please map  values");
	 }

}

function ImportValues(dbList,csvList,is_header)
{
		var url='<?php echo $srcDir.$conf->paths['supportpagesint'];?>ajax.php';//method=ImportValues&dbList='+dbList+'&csvList='+csvList+'&ra='+Math.random();		
				
		window.scrollTo(0,0);/// focus on top page
		document.getElementById('loading_image').style.display="block";
		
		
		update_duplicate=document.getElementById('update_duplicate').value;
		//alert('123'); 
		new Ajax.Request(url, 
	    {
			  method: 'get',
			  parameters: 'method=ImportValues&dbList='+dbList+'&csvList='+csvList+'&is_header='+is_header+'&update_duplicate='+update_duplicate+'&ra='+Math.random(),
			  onComplete:CompleteImport  });
}

function CompleteImport(response)
{
	//alert('hi');
	//document.getElementById('message1').innerHTML=response.responseText;
	document.getElementById('loading_image').style.display="none";

	var xmlTag= '<?php echo $xmlTag ?>'; 
	//alert(xmlTag);
	if(xmlTag=="Student")
	{	
		document.getElementById('show_file').style.display="block";
		document.getElementById('go_to_section').style.display="block";
	}
	//document.getElementById('show_file').innerHTML=response.responseText;
	//document.write(response.responseText));
	alert(response.responseText);
	
	var p='<?php echo $p?>';
	var classid='<?php echo $classid?>';
//	var url="<?php echo $srcDir.$conf->paths['supportmodules'];?>branch/section/view_all_sections.php?p="+p+"&class_id="+classid;
//	alert(url);
	//window.location.href = "<?php echo $srcDir.$conf->paths['supportmodules'];?>branch/section/view_all_sections.php?p="+p+"&class_id="+classid;
}
</script>
