<?php
	session_start();
	include_once("../headerFooter/header.php");
	require_once('../../classes/contact.php');
	require_once('../../classes/init.php');
	$userId=$_SESSION['userId'];
	$contact=new contact();
	$database= new database();
	$contactId=$_REQUEST['contactId'];
	//$data=$contact->GetUserContacts(46);
	//$permissions=$contact->GetAllPermissions();
	$ContactDetail=$contact->getAllimportcontactDetails($contactId,$database);

?>


<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

<script type="text/javascript" src="../../js/alert/jquery.toastmessage.js"></script>
<link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">


<script type="text/javascript" charset="utf-8">
	$(document).ready( function () {


		$("#contact_table")
			.tablesorter({widthFixed: true, widgets: ['zebra']})
			.tablesorterPager({container: $("#pager"),size:50});
	});
</script>

<!--  <div id="menu_line"></div>-->
<div class="container">
<div class="top_content">
	<h1 class="gray">Import Contact Details</h1>
</div>
<table cellspacing="0" cellpadding="0" style="padding-top:30px;">
<tbody>
<tr>
<td class="label">Name</td>
	<td class="label"><?php $name=$contact->GetContactName($contactId);
			echo $name->firstName.'   '.$name->lastName;
		?></td>
</tr>
<tr>
	<td class="label">Email</td>
	<td class="label"><?php echo $name->email; ?></td>
</tr>
</tbody>
</table>



<div class="sub_container">
	<div class="col_table" >

		<form name="view_contact" method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="view_contact">





			<table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
				<thead>
				<tr class="header">

					<th style="padding-left: 25px;" class="center big">Label Text <img src="../../images/bg.gif" /></th>

					<th class="center exbig">Label Response <img src="../../images/bg.gif" /></th>

				</tr>
				</thead>

				<tbody>

				<?php

						foreach ($ContactDetail as $cd)
						{
							//echo "<tr class=\"odd_gradeX\" id=\"2\">";
							$contact_id=$cd->contactId;



							echo "<tr class=\"odd_gradeX\" id=\"".$contact_id."\">";

//
							echo "<td style='padding-left: 25px;'>".$cd->lebalText."</td>";

							echo "<td>".$cd->responseText."</td>";
							echo "</tr>";

						}


				//		echo '<html><head><META http-equiv="refresh" content="0;URL=http://'.$_SERVER['SERVER_NAME'].'/modules/contact/view_all_contacts.php"></head></html>';
				?>

				</tbody>

			</table>
		</form>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
			<tbody><tr>
				<td>

				</td>
				<td>
				<div style="margin-top:10px; float:right;">


						<div class="pager" id="pager">
							<form>
								<img class="first_page" src="../../images/paging_far_left.gif">
								<img class="prev_page" src="../../images/paging_left.gif">
								<input type="text" class="pagedisplay" value="1/1">
								<img class="next_page" src="../../images/paging_right.gif">
								<img class="last_page" src="../../images/paging_far_right.gif">

								<select id="pagesize" name="pagesize" class="pagesize"><option value="10" selected="selected">10</option><option value="100">100</option><option value="250">250</option></select>
							</form>
						</div></div></td>


			</tr>
			</tbody></table>

	</div>



</div>



<?php
	include_once("../headerFooter/footer.php");
?>


