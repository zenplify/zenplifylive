<?php
	error_reporting(0);
	//ini_set('max_execution_time', 600);
	ini_set('max_execution_time', 0);
	//error_reporting(E_ALL);
	//error_reporting(1);
	//ini_set('display_errors', 'On');

	session_start();
	require_once '../../classes/import.php';
	require_once '../../classes/init.php';
	require_once '../../classes/profiles.php';
	include_once("../headerFooter/header.php");

	$userId = $_SESSION['userId'];
	$database = new database();
	$profiles = new Profiles();


	require_once 'Excel/reader.php';

	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('CP1251');


	$contactDetailLabels = array("Title", "First Name", "Last Name", "Company", "Job Title", "Birthday", "Referred by", "Interest Level", "Home Phone", "E-mail Address",
		"Best Times to Call", "Home Street", "Home City", "Home State", "Home Postal Code", "Home Country", "Permission State", "Groups", "Notes");
	if (isset($_REQUEST['submit'])) {

		if ($_FILES["userfile"]["type"] == 'application/vnd.ms-excel') ;
		{

			if ($_FILES['userfile']['tmp_name']) {


				$data->read($_FILES['userfile']['tmp_name']);
				if (count($data->sheets[0]["cells"]) <= 1000) {
					for ($filex = 1; $filex <= count($data->sheets[0]["cells"]); $filex++) {

						if ($filex == 1) {
							$detailLabelCount = 0;
							$contactDetails = array();
							while ($detailLabelCount < 100) {
								//echo $questLabelCount;
								//echo '<br>';
								$label = $data->sheets[0]["cells"][$filex][$detailLabelCount];
								//echo '<br>';
								if (in_array($label, $contactDetailLabels)) {
									$label = str_replace(" ", "", $label);
									$contactDetails[$label] = $detailLabelCount;
								}

								$detailLabelCount++;


							}

							$qCounts = array();
							$questionIds = array();
							$pageIds = array();
							$userPages = $database->executeObjectList("SELECT pageId FROM pages WHERE userId='" . $userId . "' order BY pageId DESC LIMIT 4");
							$pageCounter = 0;
							$qc = 0;
							$x = 0;
							//echo $data[99];
							foreach ($userPages as $userP) {


								$pageIds[$pageCounter] = $userP->pageId;
								$pageCounter++;
							}
							//print_r($pageIds);
							//echo '<br>';
							for ($pc = 0; $pc < sizeof($pageIds); $pc++) {
								$questions = $profiles->getPageQuestions($database, $pageIds[$pc]);
								$pq = 0;
								$pageQuestions = array();
								foreach ($questions as $quest) {
									$pageQuestions[$pq] = $quest->questionId;
									$pq++;
								}
								//print_r($pageQuestions);
								$questLabelCount = 95;

								$j = 0;
								while ($questLabelCount < 172) {
									//echo $questLabelCount;
									//echo '<br>';
									$label = $data->sheets[0]["cells"][$filex][$questLabelCount];
									//echo '<br>';
									if ($questLabelCount >= 127 && $questLabelCount <= 131) {
										$label = 'OPP Goals';
									}

									$label = strtolower($label);
									//echo '<br>';
									//echo "SELECT questionId,typeId from questions where LOWER(label)='".mysql_real_escape_string($label)."'";
									$qData = $database->executeObject("SELECT questionId,typeId from questions where LOWER(label)='" . mysql_real_escape_string($label) . "'");

									$pageId = $pageIds[$pc];
									if (!empty($qData) && (in_array($qData->questionId, $pageQuestions))) {
										$questionIds[$x] = array('pageId' => $pageId, 'qId' => $qData->questionId, 'typeId' => $qData->typeId, 'colNo' => $questLabelCount);
										$j++;
										$x++;
									}

									$questLabelCount++;


								}

								$qCounts[$qc] = $j;
								$qc++;
							}
							//print_r($questionIds);
							//echo $choice=$data->sheets[0]["cells"][2][78];
							//die();
						}
						else {


							$addedDate = date("Y-m-d");
							//$dateOfBirth= date('Y-m-d', strtotime($data->sheets[0]["cells"][$filex][54]));
							//echo "FileDate:";
							$columnNo = $contactDetails['Birthday'];
							if ($data->sheets[0]["cells"][$filex][$columnNo] != '') {
								$dob = $data->sheets[0]["cells"][$filex][$columnNo];
								$parts = explode('/', $dob);
								//print_r($parts);
								/*echo "<br />";
								echo "after explode:";*/
								$dd = $parts[0] - 1;
								$dateOfBirth = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
								/*echo "<br />";
								echo "final:";*/
								$dateOfBirth = date('Y-m-d', strtotime($dateOfBirth));
							}
							$columnNo = $contactDetails['InterestLevel'];
							if ($data->sheets[0]["cells"][$filex][$columnNo] != '')
								$interest = $data->sheets[0]["cells"][$filex][$columnNo];
							else
								$interest = 'No Interest';
							//echo $dateOfBirth;

							//die();
							/*
							echo "INSERT INTO contact
											  SET userId='".$userId."',
												title='".$data->sheets[0]["cells"][$filex][1]."',
												firstName='".mysql_real_escape_string($data->sheets[0]["cells"][$filex][2])."',
												lastName='".mysql_real_escape_string($data->sheets[0]["cells"][$filex][4])."',
												companyTitle='".mysql_real_escape_string($data->sheets[0]["cells"][$filex][7])."',
												jobTitle='".mysql_real_escape_string($data->sheets[0]["cells"][$filex][9])."',
												dateOfBirth='".$dateOfBirth."',
												referredBy='".mysql_real_escape_string($data->sheets[0]["cells"][$filex][99])."',
												addedDate='".$addedDate."',
												isActive=1, interest='".$interest."'";*/

							$contactId = $database->executeScalar("Select c.contactId FROM contact c left join emailcontacts ec on c.contactId=ec.contactId WHERE ec.email='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['E-mailAddress']]) . "' and c.userId='" . $userId . "'");
							//echo die();


							if (empty($contactId)) {
								$database->executeNonQuery("INSERT INTO contact SET
								userId='" . $userId . "',
								title='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['Title']]) . "',
								firstName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['FirstName']])) . "',
								lastName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['LastName']])) . "',
								companyTitle='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['Company']])) . "',
								jobTitle='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['JobTitle']])) . "',
								dateOfBirth='" . $dateOfBirth . "',
								referredBy='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['Referredby']])) . "',
								addedDate='" . $addedDate . "',
								isActive=1, interest='" . $interest . "'");
								$contactId = $database->insertid();

								$phoneaddedDate = date("Y-n-j");

								$database->executeNonQuery("INSERT INTO contactphonenumbers SET
								contactId='" . $contactId . "',
								phoneNumber='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomePhone']]) . "',
								addedDate='" . $phoneaddedDate . "'");

								$emailaddedDate = date("Y-n-j");
								$database->executeNonQuery("INSERT INTO emailcontacts SET
								contactId='" . $contactId . "',
								email='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['E-mailAddress']]) . "',
								addedDate='" . $emailaddedDate . "'");

								$contactEmailId = $database->insertid();
								$calltime = explode(', ', trim($data->sheets[0]["cells"][$filex][$contactDetails['BestTimestoCall']]));
								$calltimeaddedDate = date("Y-n-j");
								$database->executeNonQuery("INSERT INTO contactcalltime
																SET contactId='" . $contactId . "',
																	weekdays='" . $calltime[0] . "',
																	weekdayEvening='" . $calltime[1] . "',
																	weekenddays='" . $calltime[2] . "',
																	weekenddayEvening='" . $calltime[3] . "',
																	addedDate='" . $calltimeaddedDate . "'");


								$addressaddedDate = date("Y-n-j");
								$database->executeNonQuery("INSERT INTO contactaddresses SET
								contactId='" . $contactId . "',
								street='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeStreet']]) . "',
								zipcode='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomePostalCode']]) . "',
								state='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeState']]) . "',
								city='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeCity']]) . "',
								country='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeCountry']]) . "',
								addedDate='" . $addressaddedDate . "'");
								$permissions = $data->sheets[0]["cells"][$filex][$contactDetails['PermissionState']];
								if ($permissions == 'Subscribed')
									$statusId = 3;
								else
									$statusId = 1;
								$database->executeNonQuery("INSERT INTO contactpermissionrequests
																SET userId='" . $userId . "',
																	toContactId='" . $contactId . "',
																	emailId='" . $contactEmailId . "',
																	requestDate=CURDATE(),
																	statusId='" . $statusId . "'");
							}
							else {
								$database->executeNonQuery("UPDATE contact SET
								userId='" . $userId . "',
								title='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['Title']]) . "',
								firstName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['FirstName']])) . "',
								lastName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['LastName']])) . "',
								companyTitle='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['Company']])) . "',
								jobTitle='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['JobTitle']])) . "',
								dateOfBirth='" . $dateOfBirth . "',
								referredBy='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['Referredby']])) . "',
								addedDate='" . $addedDate . "',
								isActive=1,
								interest='" . $interest . "'
								WHERE contactId='" . $contactId . "'");
								//$contactId= $database->insertid();

								$phoneaddedDate = date("Y-n-j");

								$database->executeNonQuery("UPDATE contactphonenumbers SET
														phoneNumber='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomePhone']]) . "'
														WHERE contactId='" . $contactId . "'");

								$emailaddedDate = date("Y-n-j");
								$contactEmailId = $database->executeScalar("Select emailId FROM emailcontacts
															 WHERE contactId='" . $contactId . "'");

								$calltime = explode(', ', trim($data->sheets[0]["cells"][$filex][$contactDetails['BestTimestoCall']]));
								$calltimeaddedDate = date("Y-n-j");
								$database->executeNonQuery("UPDATE contactcalltime SET
													weekdays='" . $calltime[0] . "',
													weekdayEvening='" . $calltime[1] . "',
													weekenddays='" . $calltime[2] . "',
													weekenddayEvening='" . $calltime[3] . "' WHERE contactId='" . $contactId . "'");


								$addressaddedDate = date("Y-n-j");
								$database->executeNonQuery("UPDATE contactaddresses SET
								street='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeStreet']]) . "',
								zipcode='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomePostalCode']]) . "',
								state='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeState']]) . "',
								city='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeCity']]) . "',
								country='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomeCountry']]) . "'
								WHERE contactId='" . $contactId . "'");

								$permissions = trim($data->sheets[0]["cells"][$filex][$contactDetails['PermissionState']]);
								if ($permissions == 'Subscribed')
									$statusId = 3;
								else
									$statusId = 1;


								$database->executeNonQuery("UPDATE contactpermissionrequests SET
								statusId='" . $statusId . "' WHERE toContactId='" . $contactId . "' AND userId='" . $userId . "'");

							}

							$groups = explode(";", trim($data->sheets[0]["cells"][$filex][$contactDetails['Groups']]));
							//print_r($groups);
							for ($gCount = 0; $gCount < sizeof($groups); $gCount++) {
								$gruoupTitle = $groups[$gCount];
								if ($gruoupTitle == 'Fit Kit PURCHASE')
									$gruoupTitle = 'Purchased a Fit Kit';

								$groupId = $database->executeScalar("SELECT groupId from groups WHERE name='" . $gruoupTitle . "'");
								if (!empty($groupId)) {
									//echo "INSERT INTO groupcontacts SET groupId='".$groupId."', contactId='".$contactId."'";
									$database->executeNonQuery("INSERT INTO groupcontacts SET groupId='" . $groupId . "', contactId='" . $contactId . "'");
									$CompletePlanId = $profiles->CompleteContactPlanByGroup($database, $contactId, $groupId, $userId);
								}
							}


							$p = 0;
							//echo '<br>';
							while ($p < sizeof($pageIds)) {
								//echo 'P:'.$p;
								//echo '<br>';
								/*echo "INSERT INTO pageformreply
																		SET
																		pageId='".$pageIds[$p]."',
																		firstName='".mysql_real_escape_string($data[1])."',
																		lastName='".mysql_real_escape_string($data[3])."',
																		email='".$data[58]."',
																		phoneNumber='".$data[38]."',
																		isContactGenerated=1,
																		contactId='".$contactId."',
																		addDate=CURDATE()";
																		echo '<br>';*/
								$database->executeNonQuery("INSERT INTO pageformreply SET
								pageId='" . $pageIds[$p] . "',
								firstName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['FirstName']])) . "',
								lastName='" . mysql_real_escape_string(trim($data->sheets[0]["cells"][$filex][$contactDetails['LastName']])) . "',
								email='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['E-mailAddress']]) . "',
								phoneNumber='" . trim($data->sheets[0]["cells"][$filex][$contactDetails['HomePhone']]) . "',
								isContactGenerated=1,
								contactId='" . $contactId . "',
								addDate=CURDATE()");

								$replyId = $database->insertid();


								/*echo $pages;
								echo '<br>';*/
								if ($p == 0)
									$q = 0;


								$colCount = 95;
								//echo 'qcount:'.$q;
								//echo '<br>LoopQ: ';
								//echo 'questionCount'.$qCounts[$p];
								$qend = $qCounts[$p] + $q;
								//echo '<br>';

								while ($colCount < 172 && $q < $qend) {


									/*echo 'q'.$q;
									echo '<br>';*/

									$newCount = $questionIds[$q]['colNo'];
									if ($data->sheets[0]["cells"][$filex][$newCount] != '' || !empty($data->sheets[0]["cells"][$filex][$newCount])) {
										$answers = explode(', ', $data->sheets[0]["cells"][$filex][$newCount]);
										if ($questionIds[$q]['qId'] == 11 || $questionIds[$q]['qId'] == 12) {
											$newCount = $questionIds[$q]['colNo'];
											$answers = explode(', ', $data->sheets[0]["cells"][$filex][$newCount]);
											//echo 'answers:';
											//print_r($answers);
											//echo '<br>';
										}
										//echo "SELECT choiceId from pageformquestionchoices where choiceText ='".$data[$colCount]."'  and questionId='".$questionIds[$q]['qId']."'";
										if ($questionIds[$q]['typeId'] != 1 || $questionIds[$q]['typeId'] != 2) {

											$answers = explode(', ', $data->sheets[0]["cells"][$filex][$newCount]);
											if ($questionIds[$q]['qId'] == 10 || $questionIds[$q]['qId'] == 14 || $questionIds[$q]['qId'] == 42) {
												$answers = explode(', ', $interest);
												//print_r($answers);
											}
											if ($questionIds[$q]['qId'] == 38) {
												$answers = explode(', ', $data->sheets[0]["cells"][$filex][$newCount]);
												//print_r($answers);
											}

											for ($ans = 0; $ans < sizeof($answers); $ans++) {

												$choice = $database->executeScalar("SELECT choiceId from pageformquestionchoices where choiceText ='" . mysql_real_escape_string($answers[$ans]) . "' and questionId='" . $questionIds[$q]['qId'] . "'");
												//echo '';
												//echo "SELECT  replyId from pageformreplyanswers where replyId='".$replyId."' and  questionId='".$questionIds[$q]['qId']."' and (answer='".mysql_real_escape_string($choice)."' OR answerText= '".mysql_real_escape_string($answers[$ans])."')  and  contactId='".$contactId."'";
												$alreadyExists = $database->executeScalar("SELECT  replyId from pageformreplyanswers where replyId='" . $replyId . "' and  questionId='" . $questionIds[$q]['qId'] . "' and (answer='" . mysql_real_escape_string($choice) . "' OR answerText= '" . mysql_real_escape_string($answers[$ans]) . "')  and  contactId='" . $contactId . "'");
												if (empty($alreadyExists)) {
													if (!empty($choice))
														$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId . "','" . $questionIds[$q]['qId'] . "', '" . mysql_real_escape_string($choice) . "', '" . $contactId . "')");
													else
														$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, contactId,  answerText) VALUES ('" . $replyId . "','" . $questionIds[$q]['qId'] . "',  '" . $contactId . "','" . mysql_real_escape_string($answers[$ans]) . "')");
												}
											}

										}
										else {
											$newCount = $questionIds[$q]['colNo'];
											$choice = $data->sheets[0]["cells"][$filex][$newCount];
											if ($questionIds[$q]['qId'] == 13) {
												$newCount = $questionIds[$q]['colNo'];
												$choice = $data->sheets[0]["cells"][$filex][$newCount];
												//echo 'Choice'.$choice;
												//echo '<br>';
											}
											if ($questionIds[$q]['qId'] == 44) {
												//$newCount=$questionIds[$q]['colNo'];
												$choice = $data->sheets[0]["cells"][$filex][$contactDetails['Notes']];
												//echo 'Choice'.$choice;
												//echo '<br>';
											}
											//echo "SELECT  replyId from pageformreplyanswers where replyId='".$replyId."' and  questionId='".$questionIds[$q]['qId']."' and (answer='".mysql_real_escape_string($choice)."' OR answerText= '".mysql_real_escape_string($answers[$ans])."')  and  contactId='".$contactId."'";
											$alreadyExists = $database->executeScalar("SELECT  replyId from pageformreplyanswers where replyId='" . $replyId . "' and  questionId='" . $questionIds[$q]['qId'] . "' and (answer='" . mysql_real_escape_string($choice) . "' OR answerText= '" . mysql_real_escape_string($answers[$ans]) . "')  and  contactId='" . $contactId . "'");
											if (empty($alreadyExists)) {
												//echo "INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$questionIds[$q]['qId']."', '".mysql_real_escape_string($choice)."', '".$contactId."')";
												$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId . "','" . $questionIds[$q]['qId'] . "', '" . mysql_real_escape_string($choice) . "', '" . $contactId . "')");
											}
										}


									}
									$colCount++;
									$q++;
								}

								if ($p == 0) {
									$answers = explode(', ', $interest);
									for ($ans = 0; $ans < sizeof($answers); $ans++) {
										$choice = $database->executeScalar("SELECT choiceId from pageformquestionchoices where choiceText ='" . mysql_real_escape_string($answers[$ans]) . "' and questionId='42'");
										if (!empty($choice))
											$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId . "','42', '" . mysql_real_escape_string($choice) . "', '" . $contactId . "')");
										else
											$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, contactId,  answerText) VALUES ('" . $replyId . "','42',  '" . $contactId . "','" . mysql_real_escape_string($answers[$ans]) . "')");
									}

									$choice = $data->sheets[0]["cells"][$filex][$contactDetails['Notes']];

									$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId . "','44', '" . mysql_real_escape_string($choice) . "', '" . $contactId . "')");

								}
								if ($p == 1) {
									$answers = explode(', ', $interest);
									for ($ans = 0; $ans < sizeof($answers); $ans++) {
										$choice = $database->executeScalar("SELECT choiceId from pageformquestionchoices where choiceText ='" . mysql_real_escape_string($answers[$ans]) . "' and questionId='14'");
										if (!empty($choice))
											$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId . "','14', '" . mysql_real_escape_string($choice) . "', '" . $contactId . "')");
										else
											$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, contactId,  answerText) VALUES ('" . $replyId . "','14',  '" . $contactId . "','" . mysql_real_escape_string($answers[$ans]) . "')");
									}

								}
								$p++;
							}


						}
					}

					$userEmail = $database->executeScalar("SELECT email FROM user WHERE userId=" . $userId);


					$to1 = $userEmail; //to address
					$subject1 = "Welcome to Zenplify";
					$message1 = "Contacts import successfully";
					$from1 = "<support@zenplify.biz>";
					$headers1 = "From:" . $from1;


					$sent = mail($to1, $subject1, $message1, $headers1);
					//echo '<META http-equiv="refresh" content="0;URL=http://zenplify.biz/contact/view_all_contacts.php">';
					if ($sent == 1) {

						//echo '<META http-equiv="refresh" content="0;URL=http://zenplify.biz/modules/contact/view_all_contacts.php">';
						sendredirect('../contact/view_all_contacts.php');
					}

				}
				else {
					echo "Your file can have 1000 records only";
				}


			}
		}
	}






?>

<div class="container">

	<h1 class="gray">Import Contacts</h1></br></br>
	<form action="importcontactdetail.php" method="post" enctype="multipart/form-data">
		<?php
			$html .= "<table  align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$html .= "<tr>";
			$html .= "<td><h5>Browse and select XLS file to Upload: &nbsp; </h5></td>";
			$html .= "<td>";
			$html .= "<input type=\"file\" name=\"userfile\" id=\"userfile\" />";
			$html .= "</td>";
			$html .= "</tr>";
			$html .= "</table>";
			echo $html;
		?>
		<div style="float:left; clear:both">
			</br></br>
			<div style="margin-left:242px">
				<input type="submit" name="submit" id="upload" value="" onclick="return CheckEmptyFields()"/> <input type="button" class="cancel" value="" id="cancel" onClick="history.go(-1)" style="float:none;"/>
			</div>
		</div>
	</form>
	<div class="empty"></div>
	<div class="empty"></div>
	<div class="empty"></div>
	<div class="empty"></div>
	<div class="empty"></div>
	<div class="empty"></div>
	<div class="empty"></div>

	<?php
		include_once("../headerFooter/footer.php");
	?>
	<script type="text/javascript">


		function CheckEmptyFields() {
			var elementCount = document.forms[0].elements.length;
//alert(elementCount);
			for (i = 0; i < elementCount; i++) {
				if (document.forms[0].elements[i].type == "file") {
					if (document.forms[0].elements[i].value == "") {
						alert("Please select xls file");
						return false;

					}
				}
			}
			return true;
		}
	</script>