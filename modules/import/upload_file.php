<?php
session_start();
//Condition will be true if institute logged in
if(isset($_SESSION['institutefolder']))
{

		$instituteid=$_SESSION['instituteid'];
		$institutefolder=$_SESSION['institutefolder'];
		$usertype=$_SESSION['usertype'];
		$class_id=$_SESSION['classid'];
		
		extract($_REQUEST);
				
		$xmlTag=$_REQUEST['xmlTag'];
		
		$file = split(",",fgets(fopen("../../../path.txt",'r')));
		require_once ($file[0]);//class configuration.php is included
		require_once ($file[1]);//class messages.php is included
		$conf = configuration::getInstance();
		
		//Include Local Classes
		require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['InstitutesFolder'].$institutefolder.$conf->paths['institutesupportclasses'].$conf->paths['clsInstituteSettings'] );
		$instConfs = instituteconfs::getInstance();
		$instituteId=$instConfs->settings['InstituteId'];
		$srcDir=$conf->paths['L4i'];
		$_SESSION["srcDir"]=$srcDir."/src/";
		
		
}
else
{
	  header('Location:error.php');
}
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportclasses'].$conf->paths['clsClass_db']);
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportclasses'].$conf->paths['clsImport']);
require_once ($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint'].$conf->paths['cssConfig']);

if($_REQUEST['xmlTag']=="Student")
{
	$_SESSION['sectionid']=$_REQUEST['section_id'];	
	$section_id=$_SESSION['sectionid'];
	
}
elseif($_REQUEST['xmlTag']=="ExamMarks")
{
	$_SESSION['importExamId']=$_REQUEST['exam_id'];
	
}
elseif($_REQUEST['xmlTag']=="AcademicTermResult")
{
	$_SESSION['impExamType']=$_REQUEST['examType'];
	$_SESSION['impDataType']=$_REQUEST['dataType'];
	$_SESSION['impAcademicTermId']=$_REQUEST['academicTerm'];
	$_SESSION['importXMLTagName']=$_REQUEST['xmlTag'];
	
	
}
elseif($_REQUEST['xmlTag']=="Staff")
{
	$_SESSION['impBranchId']=$_REQUEST['branch_id'];			
}
?>
<?php
$documentPath=$srcDir.$conf->paths['InstitutesFolder'].$institutefolder.$instConfs->settings['documentPath'];
//echo "dPath:".$dPath;echo "<br/>";
if(isset($_POST["submit"]))
{	
	
	$fileName = $_FILES['userfile']['name'];
	$tmpName  = $_FILES['userfile']['tmp_name'];
	$fileSize = $_FILES['userfile']['size'];
	$fileType = $_FILES['userfile']['type'];
	//echo "File Name".$fileName;
	//echo "Temp Name".$tmpName;
	$fp = fopen($tmpName, 'r');
	$content = fread($fp, filesize($tmpName));
	$content = addslashes($content);

		
	/////-----------Create Temprary file 
	$csvPath = tempnam($documentPath, "csvtemp");
	//echo $csvPath;
	/////---------- Copy Actual file data into Temprary file
	if (!copy($tmpName, $csvPath)) {
		//echo "failed to copy $file...\n";
	}
	$_SESSION['csvPath']=$csvPath;
	header('Location:import_data.php?xmlTag='.$xmlTag);
	//die();	
	//unlink($csvPath);

}			 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TaleemPortal</title>
<link href="<?php echo $srcDir.$conf->paths['InstitutesFolder'].$institutefolder;?>css/style.css" rel="stylesheet" type="text/css" />


<?php echo loadCSS($srcDir);
?>



</head>
<body id="inner">
				<div id="wrapper_l">
					  <div id="logo">
						<h1>taleem Portal of Pakistan school management system</h1>
						<div class="subnav"></div>
					  </div>
				</div>  
	<div id="wrapper">
	 
		<div id="lower_body_in">
				<?php			
					include($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint']."left_menu.php"); 
				?>
			<div class="right">
				<div class="bpanel">
				<?php 
				$className=class_form::GetClassName($class_id);
				$sectionName=section_form::GetSectionName($section_id);
 				echo "<h2>Import Students in Class".$className."/".$sectionName."</h2>";
				?>
			
					  <br class="clear" /><br />
					  <div class="grid" id="step1">
				<form action="<?php $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
							<?php					
						$imp=new import();
						echo $imp->UploadFile();
							?>
						
						<br class="clear" />	
						<div style="margin-right:auto;margin-left:auto;width:40%">
							<!--div class="back_btn" ><input  type="button" value="Back" name="back" onclick="window.location='../branch/section/view_all_sections.php?class_id=<?php echo $class_id; ?>'"/></div-->
							<div class="back_btn" ><input  type="button" value="Back" name="back" onclick="history.go(-1);"/></div>
				 	        <div class="gw_btn"><input type="submit" name="submit" value="Upload"  onclick="return CheckEmptyFields()" /></div>	
				 		</div>	
				</form>
						</div><!--grid-->
						
						<br class="clear" /><br />
				  </div><!--bpanel-->
				  <div class="bbpanel"></div>
			 </div><!--right-->
		 </div><!--lower_body_in-->
	  
			<br class="clear" /><br /><br class="clear" /><br /><br class="clear" /><br />
		 	<br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br /> <br class="clear" /><br />
	 		<?php 
			include($conf->paths['rootPath'].$conf->paths['AppFolder'].$conf->paths['supportpagesint']."footer.php");
			?>
	</div><!--wrapper-->
</body>
</html>

<script type="text/javascript">


function CheckEmptyFields()
{
	var elementCount=document.forms[0].elements.length;
//alert(elementCount);
	for(i=0;i<elementCount;i++)
	{
		if(document.forms[0].elements[i].type=="file")
		{
			if(document.forms[0].elements[i].value=="")
			{
			alert("Please select csv file");
			return false;
			
			}
		}
	}
return true;
}


function GetFileContent()
{
	document.getElementById("step1").style.display="none";
	
}	

function getSeletedIndex(si)
{
//alert(si);
}	

function ImportValues()
{
//alert("hi");
var elementCount=document.forms['import_form'].elements.length;
alert(document.getElementById("dbFields").length);
alert(document.getElementById(csvFields[2]).length);
//alert(document.getElementById("csvFields").options[1].value);

for(i=0;i<elementCount;i++)
{
	//alert((document.forms['import_form'].elements[dbFields].length);
	
	//alert("hi");
	
	
}

//alert(document.getElementById("dbFields").length);
//alert(document.getElementById("csvFields").length);
//alert(document.getElementById(dbFields[2]).value);

}
</script>
