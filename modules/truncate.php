<?php 
include_once("../classes/init.php");
$database=new database();
$database->executeNonQuery("TRUNCATE TABLE  contactnotes");
$database->executeNonQuery("TRUNCATE TABLE  contactpermissionrequests");
$database->executeNonQuery("TRUNCATE TABLE  contactphonenumbers");
$database->executeNonQuery("TRUNCATE TABLE  contacttransferrequest");
$database->executeNonQuery("TRUNCATE TABLE  emailcontacts");
$database->executeNonQuery("TRUNCATE TABLE  forgetpasswordrequests");
$database->executeNonQuery("TRUNCATE TABLE  groupcontacts");
$database->executeNonQuery("TRUNCATE TABLE  pageformreply");
$database->executeNonQuery("TRUNCATE TABLE  pageformreplyanswers");
$database->executeNonQuery("TRUNCATE TABLE  pageviews");
$database->executeNonQuery("TRUNCATE TABLE  planstepusers");
$database->executeNonQuery("TRUNCATE TABLE  taskcontacts");
$database->executeNonQuery("TRUNCATE TABLE  tasks");
$database->executeNonQuery("TRUNCATE TABLE  user");
$database->executeNonQuery("TRUNCATE TABLE  webmail_awm_accounts");
$database->executeNonQuery("DELETE FROM pages where userId<>0");
$database->executeNonQuery("DELETE FROM plan where userId<>0 and planId<'38' and planId>'44'");
$database->executeNonQuery("TRUNCATE TABLE  planstepemails");
$database->executeNonQuery("DELETE FROM plansteps where planId <'38' and planId > '44' and stepId<>0");
$database->executeNonQuery("DELETE FROM pagequestions where pageId > 4 ");
$database->executeNonQuery("TRUNCATE TABLE  userpayments");
/*$database->executeNonQuery("TRUNCATE TABLE  webmail_adav_addressbooks"); 
$database->executeNonQuery("TRUNCATE TABLE  webmail_adav_calendars");
$database->executeNonQuery("TRUNCATE TABLE  webmail_adav_principals");
$database->executeNonQuery("TRUNCATE TABLE  webmail_adav_cards");
$database->executeNonQuery("TRUNCATE TABLE  webmail_awm_folders");
$database->executeNonQuery("TRUNCATE TABLE  webmail_adav_principals"); 
$database->executeNonQuery("TRUNCATE TABLE  webmail_awm_messages");
$database->executeNonQuery("TRUNCATE TABLE  webmail_awm_reads");
$database->executeNonQuery("TRUNCATE TABLE  webmail_a_users");
*/
?>