<?php
	error_reporting(0);
	session_start();
	include_once("../loginProcess/logincheck.php");

	$userId = $_SESSION['userId'];
	$leaderId = $_SESSION['leaderId'];

	if ($userId == $leaderId)
	{
		$signedIn = 'leader';
	}
	else
	{
		$signedIn = 'user';
	}

	$page = $_SERVER['PHP_SELF'];
	$home = strpos($page, 'home.php');
	$allcontacts = strpos($page, 'view_all_contacts.php');
	$newcontact = strpos($page, 'new_contact.php');
	$contact_detail = strpos($page, 'contact_detail.php');
	$contact_archive = strpos($page, 'view_archived_contacts.php');
	$tranfer_contact = strpos($page, 'transfer_contact_requests.php');
	$groupview = strpos($page, 'groups_view.php');
	$new_group = strpos($page, 'new_group.php');
	$edit_group = strpos($page, 'edit_group.php');
	$manage_contacts = strpos($page, 'manage_contacts.php');
	$permission = strpos($page, 'permission_manager.php');
	$import_contact = strpos($page, 'import_contact.php');
	$import_data = strpos($page, 'import_data.php');
	$calendar = strpos($page, 'sample.php');
	$Calendar_new_appiontment = strpos($page, 'new_appiontment.php');
	$edit_app = strpos($page, 'edit_appiontment.php');
	$task = strpos($page, 'tasks_view.php');
	$new_task = strpos($page, 'new_task.php');
	$edit_task = strpos($page, 'edit_task.php');
	$tasks_history_view = strpos($page, 'tasks_history_view.php');
	$plans_view = strpos($page, 'plans_view.php');
	$create_plan_detail = strpos($page, 'create_plan_detail.php');
	$follow_new_task_step = strpos($page, 'new_task_step.php');
	$messages_login = strpos($page, 'messages.php');
	$messages_mail = strpos($page, 'webmail.php');
	$list_mail = strpos($page, 'list_mailer.php');
	$compose_mail = strpos($page, 'compose_list_mail.php');
	$setting = strpos($page, 'settings.php');


?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <title>Zenplify</title>
      <link rel="stylesheet" href="../../css/style.css" type="text/css">
      <script src="../../js/jquery.min.js" type="text/javascript"></script>
      <script src="../../js/watermark.js" type="text/javascript"></script>
      <script src="../../js/zen_jscript.js" type="text/javascript"></script>
       <link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
	 <script type="text/javascript" src="../../js/modernizr.custom.79639.js"></script>
    <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="../../js/timepicker/jquery-ui-timepicker-addon.js"></script>
	
<script type="text/javascript">
var contactArray=new Array();
$(document).ready(function(){
$("#kwd_search1").css('background','url("../../images/search-icon.png")no-repeat center right');
// Write on keyup event of keyword input element
$("#kwd_search1").keyup(function(){
$("#kwd_search1").css('background','url("../../images/wait.gif")no-repeat center right');
// When value of the input is not blank
var size=$(this).val().length;
var srch=$(this).val();
if(size>=3){

<?php if($allcontacts==true){ ?>

$.ajax({
type: "POST",
url: "../../classes/ajax.php",
//$("#kwd_search1").css('background','url("../../images/wait.gif")');
data: {action: 'searchInfo',searchdata:srch,userId:<?php echo $userId;?>},
success: function(data){
$("#kwd_search1").css('background','url("../../images/search-icon.png")no-repeat center right');

$("#tableBody").html(data);
$("#contact_table")

.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
.tablesorterPager({container: $("#pager"),size:50});
}

});
<?php } else { ?>
window.location="../contact/view_all_contacts.php?data="+srch;
<?php } ?>

}

else if(size==0){
$.ajax({
type: "POST",
url: "../../classes/ajax.php",
data: {action: 'searchInfo',searchdata:srch,userId:<?php echo $userId;?>},
success: function(data){
$("#kwd_search1").css('background','url("../../images/search-icon.png")no-repeat center right');

<?php if($allcontacts==true){ ?>
$("#tableBody").html(data);
$("#contact_table")

.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
.tablesorterPager({container: $("#pager"),size:50});
<?php } else { ?>
window.location="../contact/view_all_contacts.php?data="+data;
<?php } ?>
}
});
}
});

});

</script>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">

</head>

<body>
<div class="container">
    <div id="top_cont">
        <div id="logo"><img src="../../images/logo.png"/></div>
        <div id="search">
            <img src="../../images/user-icon.png"/>
            <span class="user_name">Welcome <?php echo $_SESSION["AdminFname"]; ?></a> !</span>
            <br/><br/>
            <a  class="header-link" href="<?php echo 'http://' . $_SERVER['SERVER_NAME'] . '/modules/loginProcess/edit_user.php'; ?>">
                Update Account</a>
            | <a href='https://zenplify.biz/support/' class="header-link" target='_blank'>Help</a>
            | <a href="<?php echo 'http://' . $_SERVER['SERVER_NAME'] . '/modules/loginProcess/logout.php'; ?>"
                 class="logout header-link">Logout</a>
            <br/>
            <input type="text" name="kwd_search1" class="searchbox2" id="kwd_search1" title="Search Contacts"
                   value="<?php if (isset($_REQUEST['data']))
                       echo $_REQUEST['data']; ?>"/>
        </div>
    </div>
	<ul id="menu">
		<li class="next">
			<a href="../loginProcess/home.php" class="list_button">
				<?php if ($home == false)
				{ ?>
					<div class="left"></div>
					<div class="middle">Dashboard</div>
					<div class="right"></div>
				<?php }
				else
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">Dashboard</div>
					<div class="right right_selected"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../contact/view_all_contacts.php" class="list_button">
				<?php if ($contact_archive == true || $allcontacts == true || $newcontact == true || $contact_detail == true || $manage_contacts == true || $groupview == true || $new_group == true || $edit_group == true || $import_contact == true || $import_data == true || $tranfer_contact == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">Contacts</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">Contacts</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../webmail/messages.php" class="list_button">
				<?php if ($messages_login == true || $messages_mail == true || $list_mail == true || $compose_mail || $permission == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">&nbsp;&nbsp;Messages&nbsp;&nbsp;</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">&nbsp;&nbsp;Messages&nbsp;&nbsp;</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../calender/sample.php" class="list_button">
				<?php if ($calendar == true || $Calendar_new_appiontment == true || $edit_app == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">Calendar</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">Calendar</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../task/tasks_view.php" class="list_button">
				<?php if ($task == true || $tasks_history_view == true || $new_task == true || $edit_task == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">To Do</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">To Do</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../profiles/profiles_view.php" class="list_button">
				<?php if ($profiles_view == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">Profiles</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">Profiles</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<a href="../plan/plans_view.php" class="list_button">
				<?php if ($plans_view == true || $create_plan_detail == true || $follow_new_task_step == true)
				{ ?>
					<div class="left left_selected"></div>
					<div class="middle middle_selected">Campaigns</div>
					<div class="right right_selected"></div>
				<?php }
				else
				{ ?>
					<div class="left"></div>
					<div class="middle">Campaigns</div>
					<div class="right"></div>
				<?php } ?>
			</a>
		</li>
		<li class="next">
			<?php
				if($signedIn == 'leader'){
			?><a href="../settings/settings.php" class="list_button"><?php
					}
					else{
				?><a href="../settings/archive.php" class="list_button"><?php
						}
					?>
					<?php if ($setting == true || $todo == true || $themes == true || $archive == true || $quicklinks == true)
					{
						?>
						<div class="left left_selected"></div>
						<div class="middle middle_selected">Settings</div>
						<div class="right right_selected"></div>
					<?php
					}
					else
					{
						?>
						<div class="left"></div>
						<div class="middle">Settings</div>
						<div class="right"></div>
					<?php } ?>
				</a>
		</li>
	</ul>
	<?php
		//		echo $signedIn.' signed in '.$userId;
		// for generated via
		if (!isset($userId))
		{
			$generatedVia = 1;
		}
		elseif ($leaderId == $userId)
		{
			$generatedVia = 2;
		}
		elseif ($userId != $leaderId)
		{
			$generatedVia = 3;
		}
		else
		{
			$generatedVia = 0;
		}

	?>
</div>
   <style>
       .header-link{
           text-decoration: none;
           color: #9a9a9a;
       }
   </style>