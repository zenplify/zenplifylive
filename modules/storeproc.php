<?php
require_once ("../../classes/init.php");
require_once("../../classes/contact_details.php");
require_once("../../classes/profiles.php");

$database=new database();
$contact_detail= new contactDetails();
$profiles=new Profiles();

header('Content-type: application/json');
error_reporting(0);

  $action = $_GET['action'];  

    switch ($action)  {
    	 case 'view':
	     contactdetails($database,$contact_detail,$profiles,$_GET['contactId'],$_GET['profileId'],$_GET['userId']);
	     break;
		 case 'edit':
	     editcontactdetails($database,$contact_detail,$profiles,$_GET['contactId'],$_GET['profileId'],$_GET['userId']);
	     break;
	}
	
	function contactdetails($database,$contact_detail,$profiles,$contactId,$profileId,$userId){
	$result=array();
	$questionAnswer=array();
	$groups=array();
	$result["contactId"]=$contactId;
	$result["profileId"]=$profileId;
	$result["userId"]=$userId;
	$isGroup=false;
	$isCompaigns=false;
	$isNotes=false;
	$isCurrentActivities=false;
	$isHistory=false;
	
	
	if($profileId == "guestProfile"){
	$isGroup=true;	
	$isCompaigns=true;
	$isNotes=true;
	$isCurrentActivities=true;
	$isHistory=true;
	$result["isGrouped"]=$isGroup;
	$result["isCompaigns"]=$isCompaigns;
	$result["isNotes"]=$isNotes;
	$result["isCurrentActivities"]=$isCurrentActivities;
	$result["isHistory"]=$isHistory;
	$result["isGuestProfile"]=true;
	
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$dateofbirth = $contactdetail->dateOfBirth;
			if($dateofbirth=='0000-00-00'){
			$dateOfBirth="";
			}else{
			$dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
			}
		$contactdetail->dateOfBirth=$dateOfBirth;
			
		if($dateofbirth!='0000-00-00'){
				$contactdetail->age=(getAge($contactdetail->dateOfBirth)<0?'0':getAge($contactdetail->dateOfBirth)).' Years';
				}
				
		$status=$contact_detail->GetContactPermission($database,$contactId);
		
		$contactdetail->Subscribed=($status=="Subscribed"?true:false);
		$contactdetail->Unsubscribed=($status=="Unsubscribed"?true:false);
		$contactdetail->Unprocessed=($status=="Unprocessed"?true:false);
		$contactdetail->Pending=($status=="Pending"?true:false);
	
		$result["basicInfo"]=$contactdetail;
	//compaigns
		$stepcount=0;
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['planQueryStart']=$d->format("Y-m-d H:i:s.u");
			$showPlan=$contact_detail->showPlanStepsInContactDetail($database,$contactId,$userId);
			$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['planQueryEnd']=$d->format("Y-m-d H:i:s.u");
				if(empty($showPlan)){
					$compaign="";
				}else{
					
				 $newplanId=0;
				 $count=0;
				 $isContactUnsubscribed=$contact_detail->getContactPermissionStatus($database,$contactId);
						
					foreach($showPlan as $sp){
						$count++;
						$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['LastStepQueryStart']=$d->format("Y-m-d H:i:s.u");
						$stepId=$contact_detail->CheckLastStepOfPlan($database,$sp->planId,$contactId);
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['LastStepQueryEnd']=$d->format("Y-m-d H:i:s.u");
				 		
						if($newplanId!=$sp->planId)
                        {
                          if($newplanId != 0)
							{ 
								$compaign["step"]=$allsteps;
							
								$compaigns[]=$compaign;
							}
							
							$flag=false;

							 
                            $allsteps=array();
							$compaign["title"]=$sp->title;
							$newplanId=$sp->planId;
                        }
							
							$isskipped=($sp->skippId==NULL?false:true);
							$step["isSkipped"]=$isskipped;
							$step["summary"]=$sp->summary;
							$step["taskId"]=$sp->taskId;
							$step["isTask"]=$sp->isTask;
							$isblocked=(($stepId->stepId==$sp->stepId && $sp->isTask==0 && $sp->isDoubleOptIn==1 && $isContactUnsubscribed==3)?true:false);
							$step["isBlocked"]=$isblocked;
							$allsteps[]=$step;
							
						if(!empty($stepId->stepId) && $flag==false){
							$compaign["Completed"]=true;
						   $flag=true; }else if(empty($stepId->stepId))
						{
						$compaign["Completed"]=false;
						$compaign["planIsDoubleOpt"]=$sp->isDoubleOptIn;
						$isunsub=($isContactUnsubscribed==3?true:false);
						$compaign["isContactUnsubscribed"]=$isunsub;
						
						}	
							
					}
					
					if($count > 0)
					{
					$compaign["step"]=$allsteps;
					$compaigns[]=$compaign;
					}
		}
		$result["compaign"]=$compaigns;	
	//groups
	$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['GroupQueryStart']=$d->format("Y-m-d H:i:s.u");
		$contactgroups=$contact_detail->showContactGroups($database,$contactId);
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['GroupQueryEnd']=$d->format("Y-m-d H:i:s.u");
			if(!empty($contactgroups)){
			foreach($contactgroups as $cg){
			
			$linked=$contact_detail->isGroupLinked($database,$cg->groupId,$userId);
			if(!empty($linked)){$groupflag=true;}else{$groupflag=false;}
			$group["groupId"]=$cg->groupId;
			$group["name"]= $cg->name;
			$group["color"]=$cg->color;
			$group["groupflag"]=$groupflag;
			if($groupflag==true){
			foreach($linked as $lg){$linktitle=($count==0?"":",").$lg->title; $count++;}
				$group["linked"]=$linktitle;
			}else{
				$group["linked"]="false";
			}
			 $groups[]=$group;
			 }
			}
		$result["groups"]=$groups;
		
			$stickyNotes=$contact_detail->showStickyNotes($database,$contactId);
			$result["stickyNotes"]=$stickyNotes;
				$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['ReplyQueryStart']=$d->format("Y-m-d H:i:s.u");
				$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['replyQueryEnd']=$d->format("Y-m-d H:i:s.u");	
				if(!empty($res['pageId']) && !empty($res['replyId']) )
				{
					$pageId=$res['pageId'];
					$replyId=$res['replyId'];
					$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['QuestionsQueryStart']=$d->format("Y-m-d H:i:s.u");
					$questions=$profiles->getPageQuestions($database,$pageId);
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

		$result["basicInfo"]['questionsQueryEnd']=$d->format("Y-m-d H:i:s.u");
										
				}
		
		$result["replyId"]=$replyId;

		}elseif($profileId=="skinCareProfile"){
			$result["isGrouped"]=$isGroup;
			$result["isCompaigns"]=$isCompaigns;
			$result["isNotes"]=$isNotes;
			$result["isCurrentActivities"]=$isCurrentActivities;
			$result["isHistory"]=$isHistory;
			$result["isGuestProfile"]=false;	
		$condetail=array();
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$condetail["firstName"]=$contactdetail->firstName;
		$condetail["lastName"]=$contactdetail->lastName;
		$condetail["email"]=$contactdetail->email;
		$condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
		
		
		
		$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		//print_r($results);
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
			$result["replyId"]=$replyId;

		}elseif($profileId=="fitProfile"){
			$result["isGrouped"]=$isGroup;
			$result["isCompaigns"]=$isCompaigns;
			$result["isNotes"]=$isNotes;
			$result["isCurrentActivities"]=$isCurrentActivities;
			$result["isHistory"]=$isHistory;
			$result["isGuestProfile"]=false;
			$condetail=array();
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$condetail["firstName"]=$contactdetail->firstName;
		$condetail["lastName"]=$contactdetail->lastName;
		$condetail["email"]=$contactdetail->email;
		$condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
			$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		//print_r($results);
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
			$result["replyId"]=$replyId;

			}elseif($profileId=="consultantProfile"){
				$result["isGrouped"]=$isGroup;
				$result["isCompaigns"]=$isCompaigns;
			$result["isNotes"]=$isNotes;
			$result["isCurrentActivities"]=$isCurrentActivities;
			$result["isHistory"]=$isHistory;
			$result["isGuestProfile"]=false;
				$condetail=array();
		        $contactdetail=$contact_detail->showContactDetail($database,$contactId);
		        $condetail["firstName"]=$contactdetail->firstName;
				$condetail["lastName"]=$contactdetail->lastName;
				 $condetail["email"]=$contactdetail->email;
		        $condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
				$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
				$result["replyId"]=$replyId;

				}
		
    	foreach($questions as $q)
		{
			
			      
				$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if($q->typeId==1 || $q->typeId==2)
				{
					$q->isChoice=false;	
							if(!empty($answer->answer))
							
							$q->answer=$answer->answer;
							else
							$q->answer=$answer->answerText;
						
				}
				else
				{   $q->isChoice=true;
				foreach($answer as $a)
					{
					
					if($a->answer==NULL && $a->value==NULL)
					{
					$a->completeAnswer=$a->answerText;
					}
					else
					{
					$a->completeAnswer=$a->choiceText;
					if($a->value!=NULL)
					$a->completeAnswer='<b>'.$a->completeAnswer.':</b>&nbsp;&nbsp;&nbsp;'.$a->value;
					
					}
					
					}
					$q->answer=$answer;
						
					}
				
				
			
				
       $questionAnswer[]=$q;
		}
        
			
		$result["questionAnswers"]=$questionAnswer;
	
	
	$result["history"]=activitiesappiontmentnoteshistory($database,$contact_detail,$profiles,$contactId,$profileId,$userId); 
	
	$result["currentActivities"]=currentsactivityappiontment($database,$contact_detail,$profiles,$contactId,$profileId,$userId); 	  
		  
	echo json_encode($result);
	}
	
	function editcontactdetails($database,$contact_detail,$profiles,$contactId,$profileId,$userId){
	$result=array();
	$questionAnswer=array();
	$groups=array();
	$result["contactId"]=$contactId;
	$result["profileId"]=$profileId;
	$result["userId"]=$userId;
	$isGroup=false;
	$isCompaigns=false;
	$isNotes=false;
	$isCurrentActivities=false;
	$isHistory=false;
	
	if($profileId == "guestProfile"){
		$isGroup=true;	
		$isCompaigns=true;
		$isNotes=true;
		$isCurrentActivities=true;
		$isHistory=true;
		$isGuestProfile=true;
		$result["isGrouped"]=$isGroup;
		$result["isCompaigns"]=$isCompaigns;
		$result["isNotes"]=$isNotes;
		$result["isCurrentActivities"]=$isCurrentActivities;
		$result["isHistory"]=$isHistory;
		$result["isGuestProfile"]=true;
	
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$dateofbirth = $contactdetail->dateOfBirth;
			if($dateofbirth=='0000-00-00'){
			$dateOfBirth="";
			}else{
			$dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
			}
		$contactdetail->dateOfBirth=$dateOfBirth;
			
		if($dateofbirth!='0000-00-00'){
				$contactdetail->age=(getAge($contactdetail->dateOfBirth)<0?'0':getAge($contactdetail->dateOfBirth)).' Years';
				}
				
		$status=$contact_detail->GetContactPermission($database,$contactId);
		
		$contactdetail->Subscribed=($status=="Subscribed"?true:false);
		$contactdetail->Unsubscribed=($status=="Unsubscribed"?true:false);
		$contactdetail->Unprocessed=($status=="Unprocessed"?true:false);
		$contactdetail->Pending=($status=="Pending"?true:false);
	
		$result["basicInfo"]=$contactdetail;
	//compaigns
		
		$showPlan=$contact_detail->showPlans($database,$userId);
		$selectedPlanDetail=$contact_detail->showPlanStepsIdsInContactDetail($database,$contactId,$userId);
			$contactPlans=array();
			$contactPlansteps=array();
			$pCounter=0;
			$sCounter=0;
					foreach($selectedPlanDetail as $spd){
						$contactPlans[$pCounter]=$spd->planId;
						$pCounter++;
					}
					foreach($selectedPlanDetail as $sid){
						$contactPlansteps[$sCounter]=$sid->stepId;
						$sCounter++;
					}
		foreach($showPlan as $sp){
			$compaign=array();
			$compaign["planId"]=$sp->planId;
			$compaign["userId"]=$sp->userId;
			$compaign["isDoubleOptIn"]=$sp->isDoubleOptIn;
			$compaign["title"]=$sp->title;
			$compaign["isChecked"]=((in_array($sp->planId,$contactPlans))?true:false);
			$planStep=$contact_detail->showPlanSteps($database,$sp->planId);
					if(!empty($planStep)){
						$allstep=array();
						$stepId=$contact_detail->CheckLastStepOfPlan($database,$sp->planId,$contactId);
						$compaign["Completed"]=(!empty($stepId)?true:false);
						foreach($planStep as $ps){
							$step=array();
							$step["stepId"]=$ps->stepId;
							$step["planId"]=$ps->planId;
							$step["summary"]=$ps->summary;
							$step["isTask"]=$ps->isTask;
							$step["description"]=$ps->description;
							$step["selected"]=((in_array($ps->stepId,$contactPlansteps))?true:false);
							$allstep[]=$step;
							}
						$compaign["step"]=$allstep;
					}	
			$compaigns[]=$compaign;
			}
		$result["compaign"]=$compaigns;	
	//editgroups
	
		$contactgroupsedit=$contact_detail->showGroups($database,$contactId);
		foreach($contactgroupsedit as $cgedit){
			
			$linkededit=$contact_detail->isGroupLinked($database,$cgedit->groupId,$userId);
			if(!empty($linkededit)){$groupflagedit=true;}else{$groupflagedit=false;}
			$contactGroupedit=$contact_detail->editContactGroup($database,$cgedit->groupId,$contactId);
			$groupedit["groupId"]=$cgedit->groupId;
			$groupedit["name"]= $cgedit->name;
			$groupedit["color"]=$cgedit->color;
			$groupedit["groupflag"]=$groupflagedit;
			$groupedit["isChecked"]=($contactGroupedit==1?true:false);
			if($groupflagedit==true){
				$countedit=0;
					foreach($linkededit as $lgedit){$linktitleedit=($countedit==0?"":",").$lgedit->title; $countedit++;}
				$groupedit["linked"]=$linktitleedit;
			}else{
				$groupedit["linked"]="false";
			}
			 $groupsedit[]=$groupedit;
			 }
			
		$result["groups"]=$groupsedit;
			
			$contacttimetocall=$contact_detail->showContactTimeForCall($database,$contactId);
			//$stickyNotes=$profiles->contactNotes($contactId);
			$stickyNotes=$contact_detail->showStickyNotes($database,$contactId);
			if(!empty($stickyNotes)){
				$result["stickyNotes"]=$stickyNotes;
			}else{
				$result["stickyNotes"]="";
				}
				$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
			
				if(!empty($res['pageId']) && !empty($res['replyId']) )
				{
					$pageId=$res['pageId'];
					$replyId=$res['replyId'];
					$questions=$profiles->getPageQuestions($database,$pageId);
										
				}
		
		$result["replyId"]=$replyId;

		}elseif($profileId=="skinCareProfile"){
			$result["isGrouped"]=$isGroup;
			$result["isCompaigns"]=$isCompaigns;
			$result["isNotes"]=$isNotes;
			$result["isCurrentActivities"]=$isCurrentActivities;
			$result["isHistory"]=$isHistory;
			$result["isGuestProfile"]=false;
			
		$condetail=array();
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$condetail["firstName"]=$contactdetail->firstName;
		$condetail["lastName"]=$contactdetail->lastName;
		$condetail["email"]=$contactdetail->email;
		$condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
		
		
		
		$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		//print_r($results);
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
			$result["replyId"]=$replyId;

		}elseif($profileId=="fitProfile"){
			$result["isGrouped"]=$isGroup;
			$result["isCompaigns"]=$isCompaigns;
			$result["isNotes"]=$isNotes;
			$result["isCurrentActivities"]=$isCurrentActivities;
			$result["isHistory"]=$isHistory;
			$result["isGuestProfile"]=false;
			$condetail=array();
		$contactdetail=$contact_detail->showContactDetail($database,$contactId);
		$condetail["firstName"]=$contactdetail->firstName;
		$condetail["lastName"]=$contactdetail->lastName;
		$condetail["email"]=$contactdetail->email;
		$condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
			$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		//print_r($results);
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
			$result["replyId"]=$replyId;

			}elseif(	$profileId=="consultantProfile"){
				$result["isGrouped"]=$isGroup;
				$result["isCompaigns"]=$isCompaigns;
				$result["isNotes"]=$isNotes;
				$result["isCurrentActivities"]=$isCurrentActivities;
				$result["isHistory"]=$isHistory;
				$result["isGuestProfile"]=false;
				$condetail=array();
		        $contactdetail=$contact_detail->showContactDetail($database,$contactId);
		        $condetail["firstName"]=$contactdetail->firstName;
				$condetail["lastName"]=$contactdetail->lastName;
				$condetail["email"]=$contactdetail->email;
		        $condetail["phoneNumber"]=$contactdetail->phoneNumber;
		
		$result["basicInfo"]=$condetail;
				$res=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileId);
		
			if(!empty($res['pageId']) && !empty($res['replyId']) )
			{
				$pageId=$res['pageId'];
				$replyId=$res['replyId'];
				$questions=$profiles->getPageQuestions($database,$pageId);
				
			}
				$result["replyId"]=$replyId;

				}
		//questions
    	$questions=$profiles->getPageQuestions($database,$pageId);
		foreach($questions as $q)
   		{
			$choice=array();
                       $q->replyId=$replyId;
					   if($q->typeId=='4')
                		{
							
							$q->isChoice=true;
                    		$choices=$profiles->getQuestionChoices($database, $q->questionId);
          					$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
							$i=0;
							foreach($choices as $c)
							{ 
							  if($i%3==0){
								  $ans["isNewRow"]=true;
								  }else{
									  $ans["isNewRow"]=false;
									  }
							   foreach($answer as $a){
									if($c->choiceText==$a->choiceText){
										$ans["isChecked"]=true;
										}else{
										$ans["isChecked"]=false;
									if($ans["isChecked"]==true){break;}	
											}
								}
								$ans["questionId"]=$q->questionId;
								$ans["choiceId"]=$c->choiceId;
								$ans["choiceText"]=$c->choiceText;
							$choice[]=$ans;
							$i++;
							} 
						
						$q->answer=$choice;	
				}
            if($q->typeId=='3')
            {
                $choices=$profiles->getQuestionChoices($database, $q->questionId);
         		$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
					
					$q->isChoice=true;
                    $j=0; 
                    foreach($choices as $c)
                    {
                       if($j%3==0){
								  $ans["isNewRow"]=true;
								  }else{
									  $ans["isNewRow"]=false;
									  }
						foreach($answer as $a){
							if($c->choiceText==$a->choiceText){
								$ans["isChecked"]=true;
								}else{
											$ans["isChecked"]=false;
											}
									if($ans["isChecked"]==true){break;}
									}
								$ans["questionId"]=$q->questionId;	
                       			$ans["choiceId"]=$c->choiceId;
								$ans["choiceText"]=$c->choiceText;
							$choice[]=$ans;
                    $j++;
					} 
          $q->answer=$choice;
       }
	 if($q->typeId=='5')
                		{  
							$choicelabel=array();
							$lables=$profiles->getQuestionLabel($database,$q->questionId);
							 foreach($lables as $lable)
							   {
								   $choicelabel[]=$lable->label;
								}
							$q->isChoice=true;
                    		$choices=$profiles->getQuestionChoices($database, $q->questionId);
          					$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
							
						  	
							foreach($choices as $c)
							{ 
							$i=0;
							foreach($lables as $lable)
							   {
								  
								 if($i==0){
								  $ans["isNewRow"]=true;
								  }else{
									  $ans["isNewRow"]=false;
									  }
							  
							   foreach($answer as $a){
									$ans["isChecked"]=(($lable->label==$a->value && $c->choiceId==$a->answer)?true:false);
									if($ans["isChecked"]==true){break;}
								}
								$ans["questionId"]=$q->questionId;
								$ans["choiceId"]=$c->choiceId;
								$ans["choiceText"]=$c->choiceText;
							  $ans["answerLabel"]=$lable->label;
							  
							$choice[]=$ans;
							$i++;
							}
								
						} 
						
						$q->answer=$choice;
						$q->choicelabel=$choicelabel;
							
				}
          if($q->typeId=='2' || $q->typeId=='1')
            {
				$q->isChoice=false;
                $answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if(!empty($answer->answer)){
					$q->answer=$answer->answer;
				}elseif(!empty($answer->answerText)){
					$q->answer=$answer->answerText;
				}else{
					$q->answer="";
					}
			}
		$questionAnswer[]=$q;	
		}
       
		
        
			
		$result["questionAnswers"]=$questionAnswer;
	$result["history"]=activitiesappiontmentnoteshistory($database,$contact_detail,$profiles,$contactId,$profileId,$userId); 
	
	$result["currentActivities"]=currentsactivityappiontment($database,$contact_detail,$profiles,$contactId,$profileId,$userId); 	  
	echo json_encode($result);
	}		


function getAge($then) {
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
    return $age;
}
function activitiesappiontmentnoteshistory($database,$contact_detail,$profiles,$contactId,$profileId,$userId){
			
	$notes1=$contact_detail->contactNotes($database,$contactId);
	$activitiesCompleted1=$contact_detail->ActivitiesHistory($database,$userId,$contactId);
	$emailHistory1=$contact_detail->EmailHistory($database,$userId,$contactId);
        // $result["history"]="";
		 $arraynotes=array();
		 $arrayactivities=array();
		 $arrayemail=array();
		 
		 if(!empty($notes1)){ 
		 foreach ($notes1 as $no){
			 				$no->addedDate=$date = date('m/d/Y', strtotime($no->addedDate));
							$no->isActivity=false;
							$no->isEmail=false;
							$no->isNotes=true;
		 $note[] = $no;	
		 }
		  $arraynotes=$note;
		  }
		  if(!empty($activitiesCompleted1)){
			  $nodate='1990-12-12';
			  foreach ($activitiesCompleted1 as $actCmplt){
				  $upcmingactivitycontact=array();
					$datetime = $actCmplt->completedDate;
					$date = date('m/d/Y', strtotime($datetime));
					((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
					$contactIds2=$contact_detail->getcontactId2($database,$actCmplt->taskId,$userId);
				
						   foreach($contactIds2 as $cc){
							   
								$contact2["contactId"]=$cc->contactId;
								$contact2["firstName"]=$cc->firstName;
								$contact2["lastName"]=$cc->lastName;
								$upcmingactivitycontact[]=$contact2;		
							}
					$actCmplt->contactId=$contactId;
					if(strlen($actCmplt->title)>22) $actCmplt->title=substr($actCmplt->title,0,22)."..."; else $actCmplt->title=$actCmplt->title;
					$actCmplt->dates=( strtotime($date)< strtotime($nodate)?'':$date);
					$actCmplt->times=$time;
					$actCmplt->contact=$upcmingactivitycontact;
					$actCmplt->isActivity=true;
					$actCmplt->isEmail=false;
					$actCmplt->isNotes=false;
					$activitiesCompleted[] = $actCmplt;			  
				 } 
			  $arrayactivities=$activitiesCompleted;
		  }
		  	if(!empty($emailHistory1)){
					 foreach ($emailHistory1 as $emailCmplt){
						 $emailHistorycontact=array();
							$datetime = $emailCmplt->dateTime;
							$date = date('m/d/Y', strtotime($datetime));
							((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
					
							$contactIds3=$contact_detail->getContactDetails($database,$emailCmplt->contactId,$userId);
									$contact3["contactId"]=$contactIds3->contactId;
									$contact3["firstName"]=$contactIds3->firstName;
									$contact3["lastName"]=$contactIds3->lastName;
									$emailHistorycontact[]=$contact3;
									
						   if(strlen($emailCmplt->subject)>22) $emailCmplt->subject=substr($emailCmplt->subject,0,22)."..."; else $emailCmplt->subject=$emailCmplt->subject;
							$emailCmplt->dates=( strtotime($date)< strtotime($nodate)?'':$date);
							$emailCmplt->times=$time;
							$emailCmplt->contact=$emailHistorycontact;
							$emailCmplt->isActivity=false;
							$emailCmplt->isEmail=true;
							$emailCmplt->isNotes=false;
							$emailHistory[] = $emailCmplt;			
						   
						
				 }
				  $arrayemail=$emailHistory;
			}
		return $resultarrayhistory=array_merge($arraynotes,$arrayactivities,$arrayemail);
}
function currentsactivityappiontment($database,$contact_detail,$profiles,$contactId,$profileId,$userId){
//current activities	  
		 $arraycurrentactivitiesedit=array();
		 $arrayappiontmentsedit=array();
	
	$activitiescu=$contact_detail->upcomingActivities($database,$userId,$contactId);
	if(!empty($activitiescu)){
		foreach ($activitiescu as $act){
			$upcmingactivitycontact=array();
			 if($act->isActive){
				 
				 }else{
			 	$datetime = $act->dueDateTime;
			  if($act->dueDateTime=='0000-00-00 00:00:00')
			 {
				 $date='';
				 $time='';
				
			 }
			 else
			 {
				 $date = date('m/d/Y', strtotime($datetime));
				((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
			
			}
			
			$contactIds0=$contact_detail->getcontactId2($database,$act->taskId,$userId);
			foreach($contactIds0 as $ci){
				$contact0=array();
				$contact0["contactId"]=$ci->contactId;
				$contact0["firstName"]=$ci->firstName;
				$contact0["lastName"]=$ci->lastName;
				
				$upcmingactivitycontact[]=$contact0;
				}
		}
				$act->contactId=$contactId;
				if(strlen($act->title)>22) $act->title=substr($act->title,0,22)."..."; else $act->title=$act->title;
				$act->reoccur=(($act->repeatTypeId!=1 && $act->planId==0 )?true:false);
				$act->dates=$date;
				$act->times=$time;
				$act->contact=$upcmingactivitycontact;
				$act->isActivity=true;
				$act->isAppiontment=false;
				$act->isNotes=false;
				$upcmingactivity[]=	$act;
				}
				$arraycurrentactivitiesedit=$upcmingactivity;
	}
		//appiontment activities
		$appiontmentactivitiesup=$contact_detail->upcomingActivitiesAppiontments($database,$userId,$contactId);
			if(!empty($appiontmentactivitiesup)){
				foreach ($appiontmentactivitiesup as $app){
					$upcmingappiontmentactivitiescontacts=array();
						$contactIds1=$contact_detail->getcontactId3($database,$app->appiontmentId,$userId);
						$datetime = $app->endDateTime;
						$date = date('m/d/Y', strtotime($datetime));
					((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
					
							   foreach($contactIds1 as $ci){
								$contact1=array();
								$contact1["contactId"]=$ci->contactId;
								$contact1["firstName"]=$ci->firstName;
								$contact1["lastName"]=$ci->lastName;	
								$upcmingappiontmentactivitiescontacts[]=$contact1;
								}
				if(strlen($app->title)>22) $app->title=substr($app->title,0,22)."..."; else $app->title=$app->title;
				$app->reoccur=(($app->repeatTypeId!=1 && $app->repeatTypeId!=0 )?true:false);
				$app->dates=$date;
				$app->times=$time;
				$app->contact=$upcmingappiontmentactivitiescontacts;
				$app->isActivity=false;
				$app->isAppiontment=true;
				$app->isNotes=false;
				$upcmingappiontmentactivities[] = $app;	
							} 
				$arrayappiontmentsedit=$upcmingappiontmentactivities;
		}
		return $resultcurrentActivitiesArray=array_merge($arraycurrentactivitiesedit,$arrayappiontmentsedit);
		  
	}		
?>		