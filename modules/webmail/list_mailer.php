<?php 
session_start();
error_reporting(0);
include_once("../../classes/init.php");
include_once("../../classes/messages.php");
require_once('../../classes/group.php');
$userId=$_SESSION['userId'];
$group=new group();
$messages=new messages();
$database= new database();

include_once("../headerFooter/header.php");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html>
<head>
<meta http-equiv="Pragma" content="cache" /><meta http-equiv="Cache-Control" content="public" />
<title>User Messages</title>
<script>
$(document).ready(function() {
   
   $("#backgroundPopup").click(function(){
			
			disablePopupGroup();
		});
  

});
function showinterests()
{
	$('#okInterest').removeAttr('disabled');
	$('input:checkbox').removeAttr('checked');
	loadpopupInterest();
	centerPopupInterest();

}
function showgroups()
{
	$('#okGroup').removeAttr('disabled');
	$('input:checkbox').removeAttr('checked');
	loadpopupGroup();
	centerPopupGroup();
}
function showDomain()
{
	document.getElementById('dom').style.display='block';
	var email=$('#email').val();
	var domain=email.split('@');
	
	$('#domain').val(domain[1]);
	document.getElementById('note').style.display='none';
}
function hideDomain(){
	var value=$('#domains').val();
	if(value!='yahoo.com')
	document.getElementById('note').style.display='none';
	  document.getElementById('dom').style.display='none';
	$('#domain').val(value);
	
	
	}
	
function showNote(){
	document.getElementById('note').style.display='inline';
	document.getElementById('dom').style.display='none';}

function validate(){
	if($('#email').val()=='')
	{
		$("#required1").html('Please enter email');
		flag1=false;
	}
	else
	{
		$("#required1").html('');
		flag1=true;
	}
	if($('#password').val()=='')
	{
		$("#required2").html('Please enter password');
		flag2=false;
	}
	else
	{
		$("#required2").html('');
		flag2=true;
	}
	if($('#domains').val()=='')
	{
		$("#required3").html('Please Select Domain');
		flag3=false;
	}
	else if($('#domains').val()=='other')
	{
		$("#required3").html('');
		flag3=true;
		if($('#domain').val()=='')
		{
			$("#required4").html('Please enter Domain name');
			flag4=false;
		}
		else
		{
			$("#required4").html('');
			flag4=true;
		}
		if($('#incoming').val()=='')
		{
			$("#required5").html('Please enter incomig mail server');
			flag5=false;
		}
		else
		{
			$("#required5").html('');
			flag5=true;
		}
		if($('#outgoing').val()=='')
		{
			$("#required6").html('Please enter outgoing mail server');
			flag6=false;
		}
		else
		{
			$("#required6").html('');
			flag6=true;
		}
		if(flag1==true && flag2==true && flag3==true && flag4==true && flag5==true && flag6==true)
		{
			document.login.submit();
		}
		else
		{
			return false;
		}
	}
	else
	{
		if($('#domains').val()!='')
		{
		  $("#required3").html('');
		  flag3=true;
		}
		if(flag1==true && flag2==true && flag3==true)
		{
			document.login.submit();
		}
		else
		{
			return false;
		}
	}
	}
	
function hideGroups(){
	disablePopupGroup();}

function hideInterests(){
	disablePopupInterest();}

function selectMailInterests(){
	$('#okInterest').attr('disabled','disabled');
	 var data = $('#selectInterests').serialize();
				$.post('selectInterests.php', data).done(function(data) {
					var oldData=$('#selectedInterest').html();
					var oldData=oldData+data;
					$('#selectedInterest').html(oldData);
					disablePopupInterest();
					getMailCount();
					//alert("Data Loaded:");
					
					});
	}

function selectMailGroups(){
	$('#okGroup').attr('disabled','disabled');
	 var data = $('#selectGroups').serialize();
				$.post('selectGropus.php', data).done(function(data) {
					var oldData=$('#selectedGroup').html();
					var oldData=oldData+data;
					$('#selectedGroup').html(oldData);
					disablePopupGroup();
					getMailCount();
					//alert("Data Loaded:");
					
					});

        
    
	}

function getMailCount(){
	 var data = $('#emailCount').serialize();
	 $("#preloader").show();
	$.post('totalEmails.php', data).done(function(data) {
					$("#preloader").hide();
					$('#emailCounts').html(data);
					
					//alert("Data Loaded:");
					
					});}

function delGroup(liId){
	
	/*var whichli=$('#delit').closest('li');
	 whichli.remove();*/
	 var li = document.getElementById(liId);
    	li.parentNode.removeChild(li);
		getMailCount();
	}

function delInterest(liId){
	
	/*var whichli=$('#delit').closest('li');
	 whichli.remove();*/
	 var li = document.getElementById(liId);
    	li.parentNode.removeChild(li);
		getMailCount();
	}

function goToPermissionPage()
{
	window.location='../contact/permission_manager.php';
}
function ComposeMail()
{
	 var data = $('#emailCount').serialize();
	
	$.post('getToAddress.php', data).done(function(data) {
					
					//alert("Data Loaded:");
					if(data!='')
					{
					window.location='compose_list_mail.php';
					}
					
					});
}

</script>
</head>
<body>
<div class="container">
<div class="top_content">
     <h1 class="gray">List Mailer</h1>
     </div>
 <div class="sub_container">
 
  
  <?php
  if(isset($_REQUEST['error']))
  {
	  if($_REQUEST['error']=='Authentication failed')
	  echo '<p class="errorrequired">Wrong username or password</p>';
	  else
	   echo '<p class="errorrequired">'.$_REQUEST['error'].'</p>';
  }
 ?>
  <div id="backgroundPopup"></div>
  <div id="popupGroups">
  <form id="selectGroups" name="selectGroups">
  <h1>Select Groups To Email</h1>
  <div id="groups_list">
                	<ul>
                    <?php $groups=$group->showGroups($userId); 
						
					foreach ($groups as $rowGroup)
					{
						echo '<li style="background-color:'.$rowGroup->color.'">';
						echo '<input type="checkbox" name="groups[]" id="groups" value="'.$rowGroup->groupId.'"/>';
						echo'<label>'.$rowGroup->name.'</label></li>';
					}
					?>
                   
                    
                    </ul>
                </div>
           <br />
      <input type="button" class="okGroup" id="okGroup" onclick="selectMailGroups()" /> <input type="button" id="cancel" style="margin-left:10px;" onclick="hideGroups()" />
  </form>
  </div>
  <div id="PopupInterests">
  <form id="selectInterests" name="selectInterests">
  <h1>Select Interests To Email</h1>
  <div id="interest_list">
        <ul>
            <li style="display:none;"><input type="checkbox" name="interests[]" value="leader"   id="interests" /><label>Leader</label></li>
            <li style="display:none;"><input type="checkbox" name="interests[]" value="Consultant"  id="interests" /><label>Consultant</label></li>
            <li><input type="checkbox" name="interests[]" value="Host"  id="interests"  /><label>Host</label></li> <!-- id = 50 -->
            <li><input type="checkbox" name="interests[]" value="Client"  id="interests"  /><label>Client</label></li> <!-- id = 51 -->
            <li><input type="checkbox" name="interests[]" value="Independent Consultant"  id="interests" /><label>Independent Consultant</label></li> <!-- id = 52 -->
        </ul>
    </div>
           <br />
      <input type="button" class="okInterest" id="okInterest" onclick="selectMailInterests()" /> <input type="button" id="cancel" style="margin-left:10px;" onclick="hideInterests()" />
  </form>
  </div>
 <form  method="post" name="emailCount" id="emailCount" >
<table border="0" cellpadding="0" width="80%">
<tr>
<td  class="label" style="width: 21.45%;">Add People to The Email: </td>
<td>
<div id="preloader" style="display:none;">
	<img src="../../images/loading.png" align="absmiddle">					
</div>
<div class="lislmailer_des">
	<div><h1>Add People to The Email</h1><p style="font-size:12px; color:#555">You must select group(s) to add people to your email.</p></div>
	<div id="groups_list">
		<ul id="selectedGroup"></ul>
	</div><br />
    <a href="javascript:void(0)" onclick="showgroups()">
	    <input type="button" class="addPeople" value="" />
	</a>

	<div><p style="font-size:12px; color:#555">You must select Interest Level(s) to add people to your email.</p></div>
	<div id="interest_list">
		<ul id="selectedInterest"></ul>
	</div><br />
    <a href="javascript:void(0)" onclick="showinterests()">
	    <input type="button" class="addPeople" value="" />
	</a>
</div>
</td></tr>
<tr><td></td><td></td></tr>
<tr><td  class="label" style="width: 21.45%;" >Verify Permission & Recipient List: </td>
<td><div class="lislmailer_des"><div><h1>Verify Permission & Recipient List</h1><p style="font-size:12px; color:#555">You must have permission from the recipents to send them email with the list mailer. To get permission from someone, go to the permission manager and send them a Permission Confirmation.</p></div><div id="emailCounts"></div></div></td></tr>


<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>    
     
<td>&nbsp;</td><td><input type="button" name="submitEmailUser" id="submitbtn" class="next_Survey" value=""  onclick="ComposeMail();"/><input type="button" onclick="history.go(-1)" value="" class="cancel" name="cancel"></td>
     
    
    </tr>
</table>

<div class="empty"></div>

<div class="empty"></div>
</div>
<?php 
		include_once("../headerFooter/footer.php");
	?></body>
</html>