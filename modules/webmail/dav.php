<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */


defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'/'));

require_once WM_ROOTPATH.'libraries/afterlogic/api.php';

/* Mapping PHP errors to exceptions */
function exception_error_handler($errno, $errstr, $errfile, $errline )
{
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler("exception_error_handler");
@set_time_limit(3000);

// CApi::$bUseDbLog = false;

$server = new afterlogic\DAV\Server(
	substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'],'/'.basename(__FILE__))).'/'.basename(__FILE__).'/');

$server->exec();