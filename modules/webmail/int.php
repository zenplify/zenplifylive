<?php
// Example of logging into WebMail account using email and password for incorporating into another web application
 
// determining main directory
defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'\\'));
 
// utilizing WebMail Pro API
include_once WM_ROOTPATH.'libraries\afterlogic\api.php';
//include_once 'libraries/afterlogic/api.php';
if (class_exists('CApi') && CApi::IsValid())
{
	
	// Getting required API class
  $oApiDomainsManager = CApi::Manager('domains');
 
  // Creating domain object
  $oDomain = new CDomain('gmail.com');
 
  // Additional modification of domain details
  $oDomain->IncomingMailProtocol = EMailProtocol::IMAP4;
  $oDomain->IncomingMailServer = 'pop.gmail.com';
  $oDomain->OutgoingMailServer = 'smtp.gmail.com';
 
  if ($oApiDomainsManager->CreateDomain($oDomain))
  {
    echo 'Domain '.$oDomain->Name.' is created successfully.';
  }
  else
  {
    echo $oApiDomainsManager->GetLastErrorMessage();
  }
  // data for logging into account
  $sEmail = $_POST['email'];
  $sPassword = $_POST['password'];
 
  // Getting required API class
  $oApiWebMailManager = CApi::Manager('webmail');
 
  // attempting to obtain object for account we're trying to log into
  $oAccount = $oApiWebMailManager->LoginToAccount($sEmail, $sPassword);
  if ($oAccount)
  {
    // populating session data from the account
    $oAccount->FillSession();
 
    // redirecting to WebMail
    $oApiWebMailManager->JumpToWebMail('../webmail/webmail.php?check=1');
  }
  else
  {
    // login error
    echo $oApiWebMailManager->GetLastErrorMessage();
  }
}
else
{
  echo 'WebMail API not allowed';
}