<?php
// Example of logging into WebMail account using email and password for incorporating into another web application

// determining main directory
	session_start();
	defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__) . '\\'));

// utilizing WebMail Pro API
//include_once WM_ROOTPATH.'libraries\afterlogic\api.php';
	include_once 'libraries/afterlogic/api.php';
	if (class_exists('CApi') && CApi::IsValid()) {
		// data for logging into account
		$sEmail = $_SESSION['email'];
		$sPassword = $_SESSION['password'];

		// Getting required API class
		$oApiWebMailManager = CApi::Manager('webmail');

		// attempting to obtain object for account we're trying to log into
		$oAccount = $oApiWebMailManager->LoginToAccount($sEmail, $sPassword);
		if ($oAccount) {
			// populating session data from the account
			$oAccount->FillSession();

			// redirecting to WebMail
			$oApiWebMailManager->JumpToWebMail('../webmail/webmail.php?start=1&to=' . $_SESSION['toemails']);
		}
		else {
			// login error
			echo $oApiWebMailManager->GetLastErrorMessage();
		}
	}
	else {
		echo 'WebMail API not allowed';
	}
?>