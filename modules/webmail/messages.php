<?php
	session_start();
	$_SESSION['selectedGroup'] = '';
	error_reporting(0);
	include_once("../../classes/init.php");
	include_once("../../classes/messages.php");
	include_once("encryption_class.php");
	include_once("../../classes/profiles.php");
	$userId = $_SESSION['userId'];
	$encryption = new encryption_class();
	$messages = new messages();
	$database = new database();
	$email = $messages->GetEmail($database, $userId);
	$domains = $messages->getDomains($database);

	include_once("../headerFooter/header.php");
	$userAccount = $messages->getUserEmailAccount($database, $email);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html>
<head>
	<meta http-equiv="Pragma" content="cache"/>
	<meta http-equiv="Cache-Control" content="public"/>
	<title>User Messages</title>
	<script>
		$(document).ready(function () {
			document.getElementById('dom').style.display = 'none';
			document.getElementById('note').style.display = 'none';


		});
		function showDomain() {
			document.getElementById('dom').style.display = 'block';
			var email = $('#email').val();
			var domain = email.split('@');

			$('#domain').val(domain[1]);
			document.getElementById('note').style.display = 'none';
		}
		function hideDomain() {
			var value = $('#domains').val();
			if (value != 'yahoo.com')
				document.getElementById('note').style.display = 'none';
			document.getElementById('dom').style.display = 'none';
			$('#domain').val(value);


		}
		function showNote() {
			document.getElementById('note').style.display = 'inline';
			document.getElementById('dom').style.display = 'none';
		}
		function validate() {
			if ($('#email').val() == '') {
				$("#required1").html('Please enter email');
				flag1 = false;
			}
			else {
				$("#required1").html('');
				flag1 = true;
			}
			if ($('#password').val() == '') {
				$("#required2").html('Please enter password');
				flag2 = false;
			}
			else {
				$("#required2").html('');
				flag2 = true;
			}
			if ($('#domains').val() == '') {
				$("#required3").html('Please Select Domain');
				flag3 = false;
			}
			else if ($('#domains').val() == 'other') {
				$("#required3").html('');
				flag3 = true;
				if ($('#domain').val() == '') {
					$("#required4").html('Please enter Domain name');
					flag4 = false;
				}
				else {
					$("#required4").html('');
					flag4 = true;
				}
				if ($('#incoming').val() == '') {
					$("#required5").html('Please enter incomig mail server');
					flag5 = false;
				}
				else {
					$("#required5").html('');
					flag5 = true;
				}
				if ($('#outgoing').val() == '') {
					$("#required6").html('Please enter outgoing mail server');
					flag6 = false;
				}
				else {
					$("#required6").html('');
					flag6 = true;
				}
				if (flag1 == true && flag2 == true && flag3 == true && flag4 == true && flag5 == true && flag6 == true) {
					document.login.submit();
				}
				else {
					return false;
				}
			}
			else {
				if ($('#domains').val() != '') {
					$("#required3").html('');
					flag3 = true;
				}
				if (flag1 == true && flag2 == true && flag3 == true) {
					document.login.submit();
				}
				else {
					return false;
				}
			}

		}
	</script>
</head>
<body>
<div class="container">
	<div class="top_content">
		<?php
			if (!empty($userAccount)) {
				?><a href="list_mailer.php"><input type="button" value="" class="listMailer"/></a>
			<?php
			}
			else {
				?>

				<input type="button" class="listMailer_disable" value="">
			<?php } ?>
		<a href="../contact/permission_manager.php"><input type="button" id="permission_manager2" value=""></a>

		<br/>

		<div style="float:left; width:100%; margin-top:4px; margin-bottom:3px"><font class="messageofsuccess" id="messageofsuccess"></font></div>
	</div>
	<div class="sub_container">


		<?php
			if (isset($_REQUEST['error'])) {
				if ($_REQUEST['error'] == 'Authentication failed')
					echo '<p class="errorrequired">Wrong username or password</p>';
				else
					echo '<p class="errorrequired">' . $_REQUEST['error'] . '</p>';
			}


			if (empty($userAccount)) {
				?>
				<form action="CreateUserAccount.php" method="post" name="login">
					<table border="0" cellpadding="0" width="80%">
						<tr>
							<td class="label" style="width: 21.45%;">E-mail:</td>
							<td>
								<input type="text" class="textfield" id="email" name="email" value="<?php echo $email; ?>" readonly="readonly"><label class="errorrequired" id="required1"></label>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="label" style="width: 21.45%;">Password:</td>
							<td><input type="password" class="textfield" name="password" id="password"><label class="errorrequired" id="required2"></td>
						</tr>


						<tr>
							<td class="label" style="width: 21.45%;">Select Domain :</td>
							<td>
								<select name="domains" id="domains" class="SelectField" style="margin:0 30px 10px 10px;" onchange="if (this.value == 'other') {showDomain();} else if(this.value != ''){showNote();}  else {hideDomain();}">
									<option value="">Select Domain</option>
									<?php foreach ($domains as $dm) {
										echo '<option value="' . $dm->name . '">' . $dm->name . '</option>';
									}
										echo '<option value="other">Other</option>';?>
								</select> <label class="errorrequired" id="required3"></label>
								<span id="note" style="font-size:11px; margin:0 0 10px 10px;"></br>Note: Please allow PoP3 access from your email accout.</span></td>
						</tr>
					</table>
					<table border="0" cellpadding="0" width="85%" id="dom">
						<tr>
							<td class="label" style="width: 170px;">Domain Name:</td>
							<td><input type="text" class="textfield" name="domain" id="domain"><label class="errorrequired" id="required4"></label></td>
						</tr>

						<tr>
							<td class="label">Incoming mail (POP3) :</td>
							<td><input type="text" class="textfield" name="incoming" id="incoming"><label class="errorrequired" id="required5"></label></td>
						</tr>


						<tr>
							<td class="label">Outgoing mail:</td>
							<td><input type="text" class="textfield" name="outgoing" id="outgoing"><label class="errorrequired" id="required6"></label></td>
						</tr>


					</table>
					<table border="0" cellpadding="0" width="80%">
						<tr>

							<td>&nbsp;</td>
							<td><input type="button" name="submitEmailUser" id="submitbtn" class="submit" value="" style="margin-left:168px;" onclick="validate();"/></td>


						</tr>
					</table>
				</form>
			<?php
			}
			else {
				session_start();

// Store credentials in session
				$_SESSION['email'] = $userAccount->mail_inc_login;
				$key = 'abc*def';
				$decrypt_pass = $encryption->decrypt($key, $userAccount->enc_pass);
				$errors = $encryption->errors;
				$_SESSION['password'] = $decrypt_pass;
				if (isset($_REQUEST['start']) && $_REQUEST['start'] == 1) {
					$_SESSION['toemails'] = $_REQUEST['to'];
					?>
					<iframe src="listmail-frame.php" style="width: 990px; height: 700px; border:none;"></iframe>

				<?php
				}
				else {
					?>

					<iframe src="webmail-frame.php" style="width: 990px; height: 700px; border:none;"></iframe>
				<?php
				}


			} ?>
	</div>
<?php
	include_once("../headerFooter/footer.php");
?></body>
</html>