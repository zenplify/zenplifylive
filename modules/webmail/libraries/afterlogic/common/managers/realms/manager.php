<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Realms
 */
class CApiRealmsManager extends AApiManagerWithStorage
{
	/**
	 * @var array
	 */
	static $aRealmNameCache = array();

	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct(CApiGlobalManager &$oManager, $sForcedStorage = '')
	{
		parent::__construct('realms', $oManager, $sForcedStorage);

		$this->inc('classes.realm');
	}

	/**
	 * @param int $iPage
	 * @param int $iRealmsPerPage
	 * @param string $sOrderBy = 'Login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return array | false [Id => [Login, Description]]
	 */
	public function GetRealmList($iPage, $iRealmsPerPage, $sOrderBy = 'Login', $bOrderType = true, $sSearchDesc = '')
	{
		$aResult = false;
		try
		{
			$aResult = $this->oStorage->GetRealmList($iPage, $iRealmsPerPage, $sOrderBy, $bOrderType, $sSearchDesc);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $aResult;
	}

	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return int | false
	 */
	public function GetRealmCount($sSearchDesc = '')
	{
		$iResult = false;
		try
		{
			$iResult = $this->oStorage->GetRealmCount($sSearchDesc);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $iResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return CRealm
	 */
	public function GetRealmAllocatedSize($iRealmId)
	{
		$iResult = 0;
		try
		{
			$iResult = $this->oStorage->GetRealmAllocatedSize($iRealmId);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $iResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return CRealm
	 */
	public function GetRealmById($iRealmId)
	{
		$oRealm = null;
		try
		{
			$oRealm = $this->oStorage->GetRealmById($iRealmId);
			if ($oRealm)
			{
				$oRealm->AllocatedSpaceInMB = $this->GetRealmAllocatedSize($iRealmId);
				$oRealm->FlushObsolete('AllocatedSpaceInMB');
			}
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $oRealm;
	}

	/**
	 * @param string $sRealmLogin
	 * @param string $sRealmPassword = null
	 *
	 * @return int
	 */
	public function GetRealmIdByLogin($sRealmLogin, $sRealmPassword = null)
	{
		$iRealmId = 0;
		try
		{
			if (!empty($sRealmLogin))
			{
				$iRealmId = $this->oStorage->GetRealmIdByLogin($sRealmLogin, $sRealmPassword);
			}
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $iRealmId;
	}

	/**
	 * @param int $iIdRealm
	 * @param bool $bUseCache = false
	 *
	 * @return string
	 */
	public function GetRealmLoginById($iIdRealm, $bUseCache = false)
	{
		$sResult = '';
		try
		{
			if (0 < $iIdRealm)
			{
				if ($bUseCache && !empty(self::$aRealmNameCache[$iIdRealm]))
				{
					return self::$aRealmNameCache[$iIdRealm];
				}

				$sResult = $this->oStorage->GetRealmLoginById($iIdRealm);
				if ($bUseCache && !empty($sResult))
				{
					self::$aRealmNameCache[$iIdRealm] = $sResult;
				}
			}
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $sResult;
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return bool
	 */
	public function RealmExists(CRealm $oRealm)
	{
		$bResult = false;
		try
		{
			$bResult = $this->oStorage->RealmExists($oRealm);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $bResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return array
	 */
	public function GetRealmDomains($iRealmId)
	{
		$mResult = false;
		try
		{
			$mResult = $this->oStorage->GetRealmDomains($iRealmId);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $mResult;
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return bool
	 */
	public function CreateRealm(CRealm &$oRealm)
	{
		$bResult = false;
		try
		{
			if ($oRealm->Validate())
			{
				if (!$this->RealmExists($oRealm))
				{
					if (0 < $oRealm->IdChannel && CApi::GetConf('realm', false))
					{
						/* @var $oChannelsApi CApiChannelsManager */
						$oChannelsApi = CApi::Manager('channels');
						if ($oChannelsApi)
						{
							/* @var $oChannel CChannel */
							$oChannel = $oChannelsApi->GetChannelById($oRealm->IdChannel);
							if (!$oChannel)
							{
								throw new CApiManagerException(Errs::ChannelsManager_ChannelDoesNotExist);
							}
						}
						else
						{
							$oRealm->IdChannel = 0;
						}
					}
					else
					{
						$oRealm->IdChannel = 0;
					}

					if (!$this->oStorage->CreateRealm($oRealm))
					{
						throw new CApiManagerException(Errs::RealmsManager_RealmCreateFailed);
					}
				}
				else
				{
					throw new CApiManagerException(Errs::RealmsManager_RealmAlreadyExists);
				}
			}

			$bResult = true;
		}
		catch (CApiBaseException $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}

		return $bResult;
	}

	/**
	 * @param CRealm $oRealm
	 */
	public function UpdateRealm(CRealm $oRealm)
	{
		$bResult = false;
		try
		{
			if ($oRealm->Validate())
			{
				if (null !== $oRealm->GetObsoleteValue('QuotaInMB'))
				{
					$iQuota = $oRealm->QuotaInMB;
					if (0 < $iQuota)
					{
						$iSize = $this->GetRealmAllocatedSize($oRealm->IdRealm);
						if ($iSize > $iQuota)
						{
							throw new CApiManagerException(Errs::RealmsManager_QuotaLimitExided);
						}
					}
				}

				if (!$this->oStorage->UpdateRealm($oRealm))
				{
					throw new CApiManagerException(Errs::RealmsManager_RealmUpdateFailed);
				}

				if (null !== $oRealm->GetObsoleteValue('IsDisabled'))
				{
					/* @var $oDomainsApi CApiDomainsManager */
					$oDomainsApi = CApi::Manager('domains');
					if (!$oDomainsApi->EnableOrDisableDomainsByRealmId($oRealm->IdRealm, !$oRealm->IsDisabled))
					{
						$oException = $oDomainsApi->GetLastException();
						if ($oException)
						{
							throw $oException;
						}
					}
				}
			}

			$bResult = true;
		}
		catch (CApiBaseException $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}
		return $bResult;
	}

	/**
	 * @param int $iChannelId
	 * @return array
	 */
	public function GetRealmsIdsByChannelId($iChannelId)
	{
		$aResult = false;
		try
		{
			$aResult = $this->oStorage->GetRealmsIdsByChannelId($iChannelId);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $aResult;
	}

	/**
	 * @param int $iChannelId
	 * @return bool
	 */
	public function DeleteRealmsByChannelId($iChannelId)
	{
		$iResult = 1;

		$aRealmsIds = $this->GetRealmsIdsByChannelId($iChannelId);

		if (is_array($aRealmsIds))
		{
			foreach ($aRealmsIds as $iRealmId)
			{
				$oRealm = $this->GetRealmById($iRealmId);
				if ($oRealm)
				{
					$iResult &= $this->DeleteRealm($oRealm);
				}
			}
		}

		return (bool) $iResult;
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return bool
	 */
	public function DeleteRealm(CRealm $oRealm)
	{
		$bResult = false;
		try
		{
			if ($oRealm)
			{
				/* @var $oDomainsApi CApiDomainsManager */
				$oDomainsApi = CApi::Manager('domains');
				if (!$oDomainsApi->DeleteDomainsByRealmId($oRealm->IdRealm, true))
				{
					$oException = $oDomainsApi->GetLastException();
					if ($oException)
					{
						throw $oException;
					}
				}

				$bResult = $this->oStorage->DeleteRealm($oRealm->IdRealm);
			}
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $bResult;
	}
}
