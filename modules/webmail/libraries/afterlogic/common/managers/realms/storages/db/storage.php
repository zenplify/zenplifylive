<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Realms
 */
class CApiRealmsDbStorage extends CApiRealmsStorage
{
	/**
	 * @var CDbStorage $oConnection
	 */
	protected $oConnection;

	/**
	 * @var CApiRealmsCommandCreatorMySQL
	 */
	protected $oCommandCreator;

	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct(CApiGlobalManager &$oManager)
	{
		parent::__construct('db', $oManager);

		$this->oConnection =& $oManager->GetConnection();
		$this->oCommandCreator =& $oManager->GetCommandCreator(
			$this, array(EDbType::MySQL => 'CApiRealmsCommandCreatorMySQL')
		);
	}

	/**
	 * @param int $iPage
	 * @param int $iRealmsPerPage
	 * @param string $sOrderBy = 'login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return array | false [Id => [Login, Description]]
	 */
	public function GetRealmList($iPage, $iRealmsPerPage, $sOrderBy = 'Login', $bOrderType = true, $sSearchDesc = '')
	{
		$aRealms = false;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetRealmList($iPage, $iRealmsPerPage,
				$this->dbOrderBy($sOrderBy), $bOrderType, $sSearchDesc)))
		{
			$oRow = null;
			$aRealms = array();
			while (false !== ($oRow = $this->oConnection->GetNextRecord()))
			{
				$aRealms[$oRow->id_realm] = array($oRow->login, $oRow->description);
			}
		}

		$this->throwDbExceptionIfExist();
		return $aRealms;
	}

	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return int | false
	 */
	public function GetRealmCount($sSearchDesc = '')
	{
		$iResultCount = false;
		if ($this->oConnection->Execute($this->oCommandCreator->GetRealmCount($sSearchDesc)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$iResultCount = (int) $oRow->realms_count;
			}
		}

		$this->throwDbExceptionIfExist();
		return $iResultCount;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return int
	 */
	public function GetRealmAllocatedSize($iRealmId)
	{
		$iResult = 0;
		if ($this->oConnection->Execute($this->oCommandCreator->GetRealmAllocatedSize($iRealmId)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$iResult = (int) $oRow->allocated_size;
				$iResult = (int) ($iResult / 1024);
			}
		}

		$this->throwDbExceptionIfExist();
		return $iResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return CRealm
	 */
	public function GetRealmById($iRealmId)
	{
		$oRealm = null;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetRealmById($iRealmId)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$oRealm = new CRealm();
				$oRealm->InitByDbRow($oRow);
			}
		}

		$this->throwDbExceptionIfExist();
		return $oRealm;
	}

	/**
	 * @param string $sRealmLogin
	 * @param string $sRealmPassword = null
	 *
	 * @return int
	 */
	public function GetRealmIdByLogin($sRealmLogin, $sRealmPassword = null)
	{
		$iRealmId = 0;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetRealmIdByLogin($sRealmLogin, $sRealmPassword)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$iRealmId = (int) $oRow->id_realm;
			}
		}

		$this->throwDbExceptionIfExist();
		return $iRealmId;
	}

	/**
	 * @param int $iIdRealm
	 *
	 * @return string
	 */
	public function GetRealmLoginById($iIdRealm)
	{
		$sResult = '';
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetRealmLoginById($iIdRealm)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$sResult = (string) $oRow->login;
			}
		}

		$this->throwDbExceptionIfExist();
		return $sResult;
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return array
	 */
	public function GetRealmsIdsByChannelId($iChannelId)
	{
		$aRealmsIds = false;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetRealmsIdsByChannelId($iChannelId)))
		{
			$oRow = null;
			$aRealmsIds = array();
			while (false !== ($oRow = $this->oConnection->GetNextRecord()))
			{
				$aRealmsIds[] = $oRow->id_realm;
			}
		}

		$this->throwDbExceptionIfExist();
		return $aRealmsIds;
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return bool
	 */
	public function RealmExists(CRealm $oRealm)
	{
		$bResult = false;
		$niExceptRealmId = (0 < $oRealm->IdRealm) ? $oRealm->IdRealm : null;

		if ($this->oConnection->Execute(
			$this->oCommandCreator->RealmExists($oRealm->Login, $niExceptRealmId)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow && 0 < (int) $oRow->realms_count)
			{
				$bResult = true;
			}
		}

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return array
	 */
	public function GetRealmDomains($iRealmId)
	{
		$mResult = false;
		if ($this->oConnection->Execute($this->oCommandCreator->GetRealmDomains($iRealmId)))
		{
			$oRow = null;
			$mResult = array();
			while (false !== ($oRow = $this->oConnection->GetNextRecord()))
			{
				$mResult[$oRow->id_domain] = $oRow->name;
			}
		}

		$this->throwDbExceptionIfExist();
		return $mResult;
	}

	/**
	 * @param CRealm $oRealm
	 */
	public function CreateRealm(CRealm &$oRealm)
	{
		$bResult = false;
		if ($this->oConnection->Execute($this->oCommandCreator->CreateRealm($oRealm)))
		{
			$bResult = true;
			$oRealm->IdRealm = $this->oConnection->GetLastInsertId();
		}

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param CRealm $oRealm
	 */
	public function UpdateRealm(CRealm $oRealm)
	{
		$bResult = $this->oConnection->Execute($this->oCommandCreator->UpdateRealm($oRealm));

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return bool
	 */
	public function DeleteRealm($iRealmId)
	{
		return $this->DeleteRealms(array($iRealmId));
	}

	/**
	 * @param array $aRealmIds
	 *
	 * @return bool
	 */
	public function DeleteRealms(array $aRealmIds)
	{
		$bResult = $this->oConnection->Execute(
			$this->oCommandCreator->DeleteRealms($aRealmIds));

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param string $sOrderBy
	 * @return string
	 */
	protected function dbOrderBy($sOrderBy)
	{
		$sResult = $sOrderBy;
		switch ($sOrderBy)
		{
			case 'Description':
				$sResult = 'description';
				break;
			case 'Login':
				$sResult = 'login';
				break;
		}
		return $sResult;
	}
}