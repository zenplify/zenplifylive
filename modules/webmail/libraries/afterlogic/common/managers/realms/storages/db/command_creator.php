<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Realms
 */
class CApiRealmsCommandCreator extends api_CommandCreator
{
	/**
	 * @param int $iRealmId
	 *
	 * @return string
	 */
	public function GetRealmAllocatedSize($iRealmId)
	{
		$sSql = 'SELECT SUM(quota) as allocated_size FROM %sawm_accounts WHERE id_realm = %d';

		return sprintf($sSql, $this->Prefix(), $iRealmId);
	}

	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return string
	 */
	public function GetRealmCount($sSearchDesc = '')
	{
		$sWhere = '';
		if (!empty($sSearchDesc))
		{
			$sWhere = ' WHERE login LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%').
				' OR description LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%');;
		}

		$sSql = 'SELECT COUNT(id_realm) as realms_count FROM %sawm_realms%s';

		return sprintf($sSql, $this->Prefix(), $sWhere);
	}

	/**
	 * @param string $sWhere
	 *
	 * @return string
	 */
	protected function getRealmByWhere($sWhere)
	{
		return api_AContainer::DbGetObjectSqlString(
			$sWhere, $this->Prefix().'awm_realms', CRealm::GetStaticMap(), $this->oHelper);
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return string
	 */
	public function GetRealmById($iRealmId)
	{
		return $this->getRealmByWhere(sprintf('%s = %d',
			$this->escapeColumn('id_realm'), $iRealmId));
	}

	/**
	 * @param string $sRealmLogin
	 * @param string $sRealmPassword = null
	 *
	 * @return string
	 */
	public function GetRealmIdByLogin($sRealmLogin, $sRealmPassword = null)
	{
		$sAdd = '';
		if (null !== $sRealmPassword)
		{
			$sAdd = sprintf(' AND login_enabled = 1 AND disabled = 0 AND %s = %s',
				$this->escapeColumn('password'), $this->escapeString(
					CRealm::HashPassword($sRealmPassword)));
		}

		$sSql = 'SELECT id_realm FROM %sawm_realms WHERE %s = %s%s';
		return sprintf($sSql, $this->Prefix(), $this->escapeColumn('login'), $this->escapeString($sRealmLogin), $sAdd);
	}

	/**
	 * @param int $iIdRealm
	 *
	 * @return string
	 */
	public function GetRealmLoginById($iIdRealm)
	{
		$sSql = 'SELECT %s FROM %sawm_realms WHERE %s = %d';
		return sprintf($sSql, $this->escapeColumn('login'), $this->Prefix(), $this->escapeColumn('id_realm'), $iIdRealm);
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return string
	 */
	public function GetRealmsIdsByChannelId($iChannelId)
	{
		$sSql = 'SELECT id_realm FROM %sawm_realms WHERE id_channel = %d';

		return sprintf($sSql, $this->Prefix(), $iChannelId);
	}

	/**
	 * @param int $iIdRealm
	 *
	 * @return string
	 */
	function GetRealmDomains($iRealmId)
	{
		$sSql = 'SELECT id_domain, name FROM %sawm_domains WHERE id_realm = %d';

		return sprintf($sSql, $this->Prefix(), $iRealmId);
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return string
	 */
	function CreateRealm(CRealm $oRealm)
	{
		return api_AContainer::DbCreateObjectSqlString($this->Prefix().'awm_realms', $oRealm, $this->oHelper);
	}

	/**
	 * @param CRealm $oRealm
	 *
	 * @return string
	 */
	function UpdateRealm(CRealm $oRealm)
	{
		$aResult = api_AContainer::DbUpdateArray($oRealm, $this->oHelper);

		$sSql = 'UPDATE %sawm_realms SET %s WHERE id_realm = %d';
		return sprintf($sSql, $this->Prefix(), implode(', ', $aResult), $oRealm->IdRealm);
	}

	/**
	 * @param string $sLogin
	 * @param int $niExceptRealmId = null
	 *
	 * @return string
	 */
	public function RealmExists($sLogin, $niExceptRealmId = null)
	{
		$sAddWhere = (is_integer($niExceptRealmId)) ? ' AND id_realm <> '.$niExceptRealmId : '';

		$sSql = 'SELECT COUNT(id_realm) as realms_count FROM %sawm_realms WHERE login = %s%s';

		return sprintf($sSql, $this->Prefix(), $this->escapeString(strtolower($sLogin)), $sAddWhere);
	}

	/**
	 * @param array $aRealmIds
	 *
	 * @return string
	 */
	function DeleteRealms($aRealmIds)
	{
		$aIds = api_Utils::SetTypeArrayValue($aRealmIds, 'int');

		$sSql = 'DELETE FROM %sawm_realms WHERE id_realm in (%s)';
		return sprintf($sSql, $this->Prefix(), implode(',', $aIds));
	}
}

/**
 * @package Realms
 */
class CApiRealmsCommandCreatorMySQL extends CApiRealmsCommandCreator
{
	/**
	 * @param int $iPage
	 * @param int $iRealmsPerPage
	 * @param string $sOrderBy = 'login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return string
	 */
	public function GetRealmList($iPage, $iRealmsPerPage, $sOrderBy = 'login', $bOrderType = true, $sSearchDesc = '')
	{
		$sWhere = '';
		if (!empty($sSearchDesc))
		{
			$sWhere = ' WHERE login LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%').
				' OR description LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%');
		}

		$sOrderBy = empty($sOrderBy) ? 'login' : $sOrderBy;

		$sSql = 'SELECT id_realm, login, description FROM %sawm_realms %s ORDER BY %s %s LIMIT %d, %d';

		$sSql = sprintf($sSql, $this->Prefix(), $sWhere, $sOrderBy,
			((bool) $bOrderType) ? 'ASC' : 'DESC', ($iPage > 0) ? ($iPage - 1) * $iRealmsPerPage : 0,
			$iRealmsPerPage);

		return $sSql;
	}
}
