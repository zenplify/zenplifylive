<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Calendar
 */
class CApiCalendarManager extends AApiManagerWithStorage
{
	/*
	 * @var $ApiUsersManager CApiUsersManager
	 */
	protected $ApiUsersManager;

	/*
	 * @var $ApiCollaborationManager CApiCollaborationManager
	 */
	public $ApiCollaborationManager;

	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct(CApiGlobalManager &$oManager, $sForcedStorage = '')
	{
		parent::__construct('calendar', $oManager, $sForcedStorage);

		$this->inc('classes.enum');
		$this->inc('classes.helper');
		$this->inc('classes.colors');
		$this->inc('classes.calendar');
		$this->inc('classes.event');
		$this->inc('classes.exclusion');
		$this->inc('classes.parser');

		$this->ApiUsersManager = CApi::Manager('users');
		$this->ApiCollaborationManager = CApi::Manager('collaboration');
	}

	public function GetCalendarAccess($oAccount, $sCalendarId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->GetCalendarAccess($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function GetPublicUser()
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->GetPublicUser();
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function GetPublicAccount()
	{
		$oAccount = new CAccount(new CDomain());
		$oAccount->Email = $this->GetPublicUser();
		return $oAccount;
	}

	// Calendars
	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function GetCalendar($oAccount, $sCalendarId)
	{
		$oResult = false;
		try
		{
			$oResult = $this->oStorage->GetCalendar($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param string $sCalendarId
	 */
	public function GetPublicCalendar($sCalendarId)
	{
		return $this->GetCalendar($this->GetPublicAccount(), $sCalendarId);
	}

	/**
	 * @param string $sHash
	 */
	public function GetPublicCalendarByHash($sHash)
	{
		$sCalendarId = CCalendarHelper::decryptId($sHash);
		return $this->GetPublicCalendar($sCalendarId);
	}

	/**
	 * @param string $sCalendarId
	 */
	public function GetPublicCalendarHash($oAccount, $sCalendarId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->GetPublicCalendarHash($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetUserCalendars($oAccount)
	{
		return $this->oStorage->GetCalendars($oAccount);
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetSharedCalendars($oAccount)
	{
		return $this->oStorage->GetCalendarsShared($oAccount);
	}

	public function ___qSortCallback ($a, $b)
	{
		return ($a['is_default'] === '1' ? -1 : 1);
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetCalendars($oAccount)
	{
		$oResult = null;
		try
		{
			$oResult = array();
			$oResult['user'] = array();
			$oResult['shared'] = array();

			$oCalendarsUser = $this->oStorage->GetCalendars($oAccount);
			$oCalendarsShared = array();
			if ($oAccount->User->GetCapa('CAL_SHARING'))
			{
				$oCalendarsShared = $this->oStorage->GetCalendarsShared($oAccount);
			}
			$oCalendars = array_merge($oCalendarsUser, $oCalendarsShared);

			foreach ($oCalendars as $oCalendar)
			{
				$sCalendarActive = '1';
				$sCalendarIdForCookie = str_replace('.', '_', $oCalendar->Id);
				if (isset($_COOKIE[$sCalendarIdForCookie]))
				{
					$sCalendarActive = $_COOKIE[$sCalendarIdForCookie];
				}

				$aCalendar = array(
					'calendar_id' => $oCalendar->Id,
					'url' => $oCalendar->Url,
					'ics_download' => 'export.php?calendar&calendar_id='.$oCalendar->Id,
					'active' => $sCalendarActive,
					'color' => CCalendarColors::GetColorNumber($oCalendar->Color),
					'description' => $oCalendar->Description,
					'principal_id' => $this->oStorage->GetMainPrincipalUrl($oCalendar->Principals[0]),
					'name' => $oCalendar->DisplayName,
					'user_id' => $oAccount->IdUser,
					'ical_hash' => null,
					'owner' => $oCalendar->Owner,
					'is_default' => $oCalendar->IsDefault ? '1' : '0',
				);

				if ($oCalendar->Shared)
				{
					$aCalendar['publication_hash'] = null;
					$aCalendar['publication_level'] = '0';
					$aCalendar['shares'] = array();
					$aCalendar['sharing_active'] = '1';
					$aCalendar['sharing_id'] = $oCalendar->Id;
					$aCalendar['sharing_level'] = $oCalendar->SharingLevel;
					$aCalendar['is_default'] = '0';
					$oResult['shared'][$oCalendar->Id] = $aCalendar;
				}
				else
				{
					$aCalendarUsers = $this->oStorage->GetCalendarUsers($oAccount, $oCalendar);

					$aShares = array();
					$sPubHash = null;
					$sPubLevel = null;

					foreach ($aCalendarUsers as $aCalendarUser)
					{
						if ($aCalendarUser['email'] == $this->oStorage->GetPublicUser())
						{
							$sPubLevel = '1';
							$sPubHash = $this->oStorage->GetPublicCalendarHash($oAccount, $oCalendar->Id);
						}
						else
						{
							$aShares[] = array(
								'id_share' => $aCalendarUser['email'],
								'email_to_user' => $aCalendarUser['email'],
								'access_level' => (int)$aCalendarUser['access_level']
							);
						}
					}

					$aCalendar['publication_hash'] = $sPubHash;
					$aCalendar['publication_level'] = $sPubLevel;
					$aCalendar['shares'] = $aShares;
					$aCalendar['sharing_active'] = null;
					$aCalendar['sharing_id'] = null;
					$aCalendar['sharing_level'] = null;

					$oResult['user'][$oCalendar->Id] = $aCalendar;
				}
			}
			uasort($oResult['user'], array(&$this, '___qSortCallback'));

		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function CreateCalendar($oAccount, $sName, $sDescription, $iOrder, $sColor)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->CreateCalendar($oAccount, $sName, $sDescription, $iOrder, $sColor);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function UpdateCalendar($oAccount, $sCalendarId, $sName, $sDescription, $iOrder, $sColor)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->UpdateCalendar($oAccount, $sCalendarId, $sName, $sDescription, $iOrder, $sColor);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param string $sCalendarId
	 * @param int $iVisible
	 */
	public function UpdateCalendarVisible($sCalendarId, $iVisible)
	{
		$oResult = null;
		try
		{
			$this->oStorage->UpdateCalendarVisible($sCalendarId, $iVisible);
			$oResult = true;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sColor
	 */
	public function UpdateCalendarColor($oAccount, $sCalendarId, $sColor)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->UpdateCalendarColor($oAccount, $sCalendarId, $sColor);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function DeleteCalendar($oAccount, $sCalendarId)
	{
		$oResult = null;
		try
		{
			if (\afterlogic\DAV\Constants::CALENDAR_DEFAULT_NAME !== basename($sCalendarId))
			{
				$oResult = $this->oStorage->DeleteCalendar($oAccount, $sCalendarId);
			}
			else
			{
				$oResult = false;
			}
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function UnsubscibeCalendar($oAccount, $sCalendarId)
	{
		$oResult = null;
		if ($oAccount->User->GetCapa('CAL_SHARING'))
		{
			try
			{
				$oResult = $this->oStorage->UnsubscibeCalendar($oAccount, $sCalendarId);
			}
			catch (Exception $oException)
			{
				$oResult = false;
				$this->setLastException($oException);
			}
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sUserId
	 * @param int $iPerms
	 */
	public function UpdateCalendarShare($oAccount, $sCalendarId, $sUserId, $iPerms = ECalendarPermission::RemovePermission)
	{
		$oResult = null;
		if ($oAccount->User->GetCapa('CAL_SHARING'))
		{
			try
			{
				$oResult = $this->oStorage->UpdateCalendarShare($oAccount, $sCalendarId, $sUserId, $iPerms);
			}
			catch (Exception $oException)
			{
				$oResult = false;
				$this->setLastException($oException);
			}
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function PublicCalendar($oAccount, $sCalendarId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->PublicCalendar($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function UnPublicCalendar($oAccount, $sCalendarId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->UnPublicCalendar($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sUserId
	 */
	public function DeleteCalendarShare($oAccount, $sCalendarId, $sUserId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->UpdateCalendarShare($oAccount, $sCalendarId, $sUserId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param FileInfo $oCalendar
	 */
	public function GetCalendarUsers($oAccount, $oCalendar)
	{
		$oResult = null;
		if ($oAccount->User->GetCapa('CAL_SHARING'))
		{
			try
			{
				$oResult = $this->oStorage->GetCalendarUsers($oAccount, $oCalendar);
			}
			catch (Exception $oException)
			{
				$oResult = false;
				$this->setLastException($oException);
			}
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @return string
	 */
	public function ExportCalendarToIcs($oAccount, $sCalendarId)
	{
		$mResult = null;
		try
		{
			$mResult = $this->oStorage->ExportCalendarToIcs($oAccount, $sCalendarId);
		}
		catch (Exception $oException)
		{
			$mResult = false;
			$this->setLastException($oException);
		}
		return $mResult;
	}


	// Events

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetEvents($oAccount, $sCalendarId, $dStart = null, $dFinish = null)
	{
		$oResult = null;
		try
		{
			$oResult = array(
				'events' => array(),
				'exclusions' => array(),
				'reminders' => array(),
				'appointments' => array()
			);

			if ($dStart != null) $dStart = $dStart . 'T000000Z';
			if ($dFinish != null) $dFinish = $dFinish . 'T235959Z';

			$oCalendarInfo = $this->oStorage->GetCalendar($oAccount, $sCalendarId);
			if ($oCalendarInfo)
			{
				$aEvents = $this->oStorage->GetEvents($oAccount, $sCalendarId, $dStart, $dFinish);
				if (is_array($aEvents))
				{
					foreach ($aEvents as $oEvent)
					{
						try
						{
							$parsedData = CalendarParser::GetData($oAccount, $oCalendarInfo, $oEvent);

							$oResult['events'] = array_merge(
									$oResult['events'], $parsedData['events']);
							$oResult['exclusions'] = array_merge(
									$oResult['exclusions'], $parsedData['exclusions']);
							$oResult['reminders'] = array_merge(
									$oResult['reminders'], $parsedData['reminders']);
							$oResult['appointments'] = array_merge(
									$oResult['appointments'], $parsedData['appointments']);
						}
						catch(Exception $ex)
						{
							CApi::Log("Error: Can't parse VCALEDAR object: \r\n" .
									$oEvent->serialize());
						}
					}
				}
			}

			return $oResult;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetPublicEvents($sCalendarId, $dStart = null, $dFinish = null)
	{
		return $this->GetEvents($this->GetPublicAccount(), $sCalendarId, $dStart, $dFinish);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function GetEvent($oAccount, $sCalendarId, $sEventId)
	{
		$mResult = null;
		try
		{
			$mResult = array();
			$aData = $this->oStorage->GetEvent($oAccount, $sCalendarId, $sEventId);
			if ($aData !== false)
			{
				$oCalendar = $this->oStorage->GetCalendar($oAccount, $sCalendarId);
				$mResult = CalendarParser::GetData($oAccount, $oCalendar, $aData['vcal']);
			}
		}
		catch (Exception $oException)
		{
			$mResult = false;
			$this->setLastException($oException);
		}
		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param array $sData
	 */
	public function CreateEventData($oAccount, $sCalendarId, $sEventId, $sData)
	{
		$oResult = null;
		try
		{
			$vCal = \Sabre\VObject\Reader::read($sData);
			if ($vCal && $vCal->VEVENT)
			{
				if (empty($sEventId) && $vCal->VEVENT[0]->UID)
				{
					$sEventId = $vCal->VEVENT[0]->UID;
				}

				$oResult = $this->oStorage->CreateEvent($oAccount, $sCalendarId, $sEventId, $vCal);
			}
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}
	
	/**
	 * @param CAccount $oAccount
	 * @param CEvent $oEvent
	 */
	public function CreateEvent($oAccount, $oEvent)
	{
		$oResult = null;
		try
		{
			$vCal = new \Sabre\VObject\Component('VCALENDAR');
			$vCal->VERSION = "2.0";
			$vCal->CALSCALE = 'GREGORIAN';
			
			$vCal->VEVENT = new \Sabre\VObject\Component('VEVENT');
			$vCal->VEVENT->SEQUENCE = 1;
			$vCal->VEVENT->TRANSP = 'OPAQUE';

			$oDTstamp = new \Sabre\VObject\Property\DateTime('DTSTAMP');
			$oDTstamp->setDateTime(new DateTime('now'), \Sabre\VObject\Property\DateTime::UTC);
			$vCal->VEVENT->add($oDTstamp);

			$this->oStorage->Init($oAccount);

			$oEvent->IdEvent = \Sabre\DAV\UUIDUtil::getUUID();
			CCalendarHelper::PopulateVCalendar($oAccount, $oEvent, $vCal->VEVENT);
			
			$oResult = $this->oStorage->CreateEvent($oAccount, $oEvent->IdCalendar, $oEvent->IdEvent, $vCal);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param CEvent $oEvent
	 */
	public function UpdateEvent($oAccount, $oEvent)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $oEvent->IdCalendar, $oEvent->IdEvent);
			if ($aData !== false)
			{
				$vCal = $aData['vcal'];

				if ($vCal)
				{
					$iIndex = CCalendarHelper::GetBaseVEventIndex($vCal->VEVENT);
					if ($iIndex !== false)
					{
						CCalendarHelper::PopulateVCalendar($oAccount, $oEvent, $vCal->VEVENT[$iIndex]);
					}
				}
				$oResult = $this->oStorage->UpdateEvent($oAccount, $oEvent->IdCalendar, $oEvent->IdEvent, $vCal);
			}
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function MoveEvent($oAccount, $sCalendarId, $sNewCalendarId, $sEventId)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $sCalendarId, $sEventId);
			if ($aData !== false)
			{
				$oResult = $this->oStorage->MoveEvent($oAccount, $sCalendarId, $sNewCalendarId, $sEventId, $aData['data']);
				return true;
			}
			return false;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function DeleteEvent($oAccount, $sCalendarId, $sEventId)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $sCalendarId, $sEventId);
			if ($aData !== false)
			{
				$vCal = $aData['vcal'];

				if ($vCal)
				{
					$iIndex = CCalendarHelper::GetBaseVEventIndex($vCal->VEVENT);
					if ($iIndex !== false)
					{
						$vEvent = $vCal->VEVENT[$iIndex];

						$sOrganizer = null;
						if (isset($vEvent->ORGANIZER))
						{
							$sOrganizer = str_replace('mailto:', '', strtolower($vEvent->ORGANIZER->value));
						}

						if (isset($sOrganizer))
						{
							if ($sOrganizer == $oAccount->Email)
							{
								$aAttendees = $vEvent->ATTENDEE;
								if (isset($aAttendees))
								{
									foreach($aAttendees as $oAttendee)
									{
										$sEmail = str_replace('mailto:', '', strtolower($oAttendee->value));
//										$PARTSTAT = $oAttendee->offsetGet('PARTSTAT');
//										if (!isset($PARTSTAT))
//										{
											$sMethod = 'REQUEST';
											$vCal->METHOD = 'CANCEL';
											$sBody = $vCal->serialize();
											unset($vCal->METHOD);
											
											$sSubject = $vEvent->SUMMARY->value . ': Canceled';

											CCalendarHelper::SendAppointmentMessage($oAccount, $sEmail, $sSubject,
													$sBody, $sMethod);
//										}
									}
								}
							}
							else
							{
								$vEvent->{'LAST-MODIFIED'} = gmdate("Ymd\THis\Z");
								unset($vEvent->ATTENDEE);
								$attendee = new \Sabre\VObject\Property('ATTENDEE');
								$attendee->value = 'mailto:'.$oAccount->Email;
								$attendee->offsetSet('PARTSTAT', 'DECLINED');
								$sSubject = $vEvent->SUMMARY->value . ': Declined';
								$attendee->offsetSet('RESPONDED-AT', gmdate("Ymd\THis\Z"));
								$vEvent->add($attendee);

								$sMethod = 'REPLY';
								$vCal->METHOD = $sMethod;
								$sBody = $vCal->serialize();
								unset($vCal->METHOD);

								CCalendarHelper::SendAppointmentMessage($oAccount, $sOrganizer, $sSubject,
										$sBody, $sMethod);
							}
						}
					}
				}
				$oResult = $this->oStorage->DeleteEvent($oAccount, $sCalendarId, $sEventId);
			}
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param string $iRecurrenceId
	 */
	public function GetExclusion($oAccount, $sCalendarId, $sEventId, $iRecurrenceId)
	{
		$oResult = null;
		try
		{
			$aEvent = $this->GetEvent($oAccount, $sCalendarId, $sEventId);
			if (is_array($aEvent) && isset($aEvent['exclusions']))
			{
				$aExclusions = $aEvent['exclusions'];
				foreach ($aExclusions as $aExclusion)
				{
					if ($iRecurrenceId == $aExclusion['id_recurrence'])
					{
						return $aExclusion;
					}
				}
			}
			return array();
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param CExclusion $oExclusion
	 * @param CEvent $oEvent
	 */
	public function UpdateExclusion($oAccount, $oExclusion, $oEvent)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $oEvent->IdCalendar, $oEvent->IdEvent);
			if ($aData !== false)
			{
				$vCal = $aData['vcal'];
				$index = CCalendarHelper::GetBaseVEventIndex($vCal->VEVENT);
				if ($index !== false)
				{
					unset($vCal->VEVENT[$index]->{'LAST-MODIFIED'});
					$oLastModified = new \Sabre\VObject\Property\DateTime('LAST-MODIFIED');
					$oLastModified->setDateTime(new DateTime('now'), \Sabre\VObject\Property\DateTime::UTC);
					$vCal->VEVENT[$index]->add($oLastModified);

					$oDTExdate = CCalendarHelper::PrepareDateTime(
							$oExclusion->IdRecurrence,
							$oExclusion->StartTime, 
							$this->oStorage->TimeZone
					);
					$oExdate = new \Sabre\VObject\Property\DateTime('EXDATE');
					$oExdate->setDateTime($oDTExdate, \Sabre\VObject\Property\DateTime::UTC);

					$mIndex = CCalendarHelper::isRecurrenceExists($oAccount, $vCal->VEVENT,
							$oExclusion->IdRecurrence);
					if ($oExclusion->Deleted)
					{
						$vCal->VEVENT[$index]->add($oExdate);

						if (false !== $mIndex)
						{
							$vEvents = $vCal->VEVENT;
							unset($vCal->VEVENT);

							foreach($vEvents as $vEvent)
							{
								if ($vEvent->{'RECURRENCE-ID'})
								{
									$iRecurrenceId = CCalendarHelper::GetStrDate($vEvent->{'RECURRENCE-ID'},
											$oAccount->GetDefaultStrTimeZone(), 'Ymd');
									if ($iRecurrenceId == (int) $oExclusion->IdRecurrence)
									{
										continue;
									}
								}
								$vCal->add($vEvent);
							}
						}
					}
					else
					{
						$recurEvent = null;
						if ($mIndex === false)
						{
							$recurEvent = new \Sabre\VObject\Component('VEVENT');
							$recurEvent->SEQUENCE = 1;
							$recurEvent->TRANSP = 'OPAQUE';
							$recurEvent->{'RECURRENCE-ID'} = $oExdate->value;
							$vCal->add($recurEvent);
						}
						else if (isset($vCal->VEVENT[$mIndex]))
						{
							$recurEvent = $vCal->VEVENT[$mIndex];
						}

						if ($recurEvent)
						{
							CCalendarHelper::PopulateVCalendar($oAccount, $oEvent, $recurEvent);
						}
					}

					return $this->oStorage->UpdateEvent($oAccount, $oEvent->IdCalendar, $oEvent->IdEvent, $vCal);

				}
			}
			return false;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param string $iRecurrenceId
	 */
	public function DeleteExclusion($oAccount, $sCalendarId, $sEventId, $iRecurrenceId)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $sCalendarId, $sEventId);
			if ($aData !== false)
			{
				$vCal = $aData['vcal'];

				$vEvents = $vCal->VEVENT;
				unset($vCal->VEVENT);

				foreach($vEvents as $vEvent)
				{
					if ($vEvent->{'RECURRENCE-ID'})
					{
						$iServerRecurrenceId = CCalendarHelper::GetStrDate($vEvent->{'RECURRENCE-ID'},
								$oAccount->GetDefaultStrTimeZone(), 'Ymd');
						if ($iRecurrenceId == $iServerRecurrenceId)
						{
							continue;
						}
					}
					$vCal->add($vEvent);
				}
				return $this->oStorage->UpdateEvent($oAccount, $sCalendarId, $sEventId, $vCal);
			}
			return false;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetAlarms($oAccount, $sCalendarId, $dStart = null, $dFinish = null)
	{
		$oResult = null;
		try
		{
			$oResult = array();
			$oResult['data'] = array();
			$oResult['events'] = array();
			$oResult['exclusions'] = array();
			$oResult['reminders'] = array();
			$oResult['appointments'] = array();

			$oCalendarInfo = $this->oStorage->GetCalendar($oAccount, $sCalendarId);
			if ($oCalendarInfo)
			{
				$aEvents = $this->oStorage->GetAlarms($oAccount, $sCalendarId, $dStart, $dFinish);
				foreach ($aEvents as $oEvent)
				{
					$parsedData = CalendarParser::GetData($oAccount, $oCalendarInfo, $oEvent);

					$oResult['data']  = array_merge($oResult['data'], $parsedData['data']);
					$oResult['events'] = array_merge($oResult['events'], $parsedData['events']);
					$oResult['exclusions'] = array_merge($oResult['exclusions'], $parsedData['exclusions']);
					$oResult['reminders'] = array_merge($oResult['reminders'], $parsedData['reminders']);
					$oResult['appointments'] = array_merge($oResult['appointments'], $parsedData['appointments']);
				}
			}
			return $oResult;
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function GetReminders($start = null, $end = null)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->GetReminders($start, $end);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function DeleteReminder($eventId)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->DeleteReminder($eventId);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function DeleteReminderByCalendar($calendarUri)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->DeleteReminderByCalendar($calendarUri);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	public function UpdateReminder($sEmail, $sCalendarUri, $sEventId, $sData)
	{
		$oResult = null;
		try
		{
			$oResult = $this->oStorage->UpdateReminder($sEmail, $sCalendarUri, $sEventId, $sData);
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}
	
	/**
	 * @param CAccount $oAccount
	 * @param string $sData
	 * @param string $mFromEmail
	 *
	 * @return bool
	 */
	public function PreprocessICS($oAccount, $sData, $mFromEmail)
	{
		$iAccountId = $this->ApiUsersManager->GetDefaultAccountId($oAccount->IdUser);

		/* @var $oAccount CAccount */
		$oAccount = $this->ApiUsersManager->GetAccountById($iAccountId);

		$mResult = null;
		try
		{
			$mResult = false;
			$vCal = \Sabre\VObject\Reader::read($sData);
			if ($vCal)
			{
				$oMethod = null;
				$sMethod = 'SAVE';
				if (isset($vCal->METHOD))
				{
					$oMethod = $vCal->METHOD;
					$sMethod = $oMethod->value;
					if ($sMethod !== 'REQUEST' && $sMethod !== 'REPLY' && $sMethod !== 'CANCEL')
					{
						return false;
					}
				}

				$vEvents = $vCal->getBaseComponents('VEVENT');
				if (isset($vEvents) && count($vEvents) > 0)
				{
					$vEvent = $vEvents[0];
					$sEventId = $vEvent->UID->value;
					$sCalId = '';

					$oCals = $this->oStorage->GetCalendars($oAccount);
					$aServerData = false;
					$mResult = array();

					$mResult['Calendars'] = array();
					if (is_array($oCals))
					{
						foreach ($oCals as $sKey => $oCal)
						{
							$mResult['Calendars'][$sKey] = $oCal->DisplayName;
							$aServerData = $this->oStorage->GetEvent($oAccount, $oCal->Id, $sEventId);
							if ($aServerData !== false)
							{
								$aServerData['url'] = $oCal->Id;
								break;
							}
						}
					}

					$oResultCal = $vCal;
					$oResultEvent = $vEvent;

					if ($aServerData !== false)
					{
						$sCalId = $aServerData['url'];
						$serverCal = $aServerData['vcal'];
						if (isset($oMethod))
						{
							$serverCal->METHOD = $oMethod;
						}
						$serverEvents = $serverCal->getBaseComponents('VEVENT');
						$serverEvent = $serverEvents[0];

						if (isset($vEvent->{'LAST-MODIFIED'}) && isset($serverEvent->{'LAST-MODIFIED'}))
						{
							$eventLastModified = $vEvent->{'LAST-MODIFIED'}->getDateTime();
							$srvEventLastModified = $serverEvent->{'LAST-MODIFIED'}->getDateTime();
							if ($srvEventLastModified >= $eventLastModified)
							{
								$oResultCal = $serverCal;
								$oResultEvent = $serverEvent;
							}
							else
							{
								if (isset($sMethod))
								{
									if ($sMethod == 'REPLY')
									{
										$oResultCal = $serverCal;
										$oResultEvent = $serverEvent;
										if (isset($vEvent->ATTENDEE))
										{
											$attendee = $vEvent->ATTENDEE[0];
											$sEmail = str_replace('mailto:', '', strtolower($attendee->value));
											if (isset($oResultEvent->ATTENDEE))
											{
												foreach ($oResultEvent->ATTENDEE as $resultAttendee)
												{
													$sResultEmail = str_replace('mailto:', '', strtolower($resultAttendee->value));
													if ($sResultEmail == $sEmail)
													{
														$PARTSTAT = $attendee->offsetGet('PARTSTAT');
														if (isset($PARTSTAT))
														{
															$resultAttendee->offsetSet('PARTSTAT', $PARTSTAT->value);
														}
														break;
													}
												}
											}
										}
										unset($oResultCal->METHOD);
										$oResultEvent->{'LAST-MODIFIED'} = gmdate("Ymd\THis\Z");
										$this->oStorage->UpdateEventData($oAccount, $sCalId, $sEventId, $oResultCal->serialize());
										$oResultCal->METHOD = $sMethod;
									}
									else if ($sMethod == 'CANCEL')
									{
										$this->DeleteEvent($oAccount, $sCalId, $sEventId);
									}
								}
							}
						}
					}
					$mResult['UID'] = $sEventId;
					$mResult['Body'] = $oResultCal->serialize();
					$mResult['Action'] = $sMethod;

					$mResult['Location'] = isset($oResultEvent->LOCATION) ?
							$oResultEvent->LOCATION->value : '';
					$mResult['Description'] = isset($oResultEvent->DESCRIPTION) ?
							$oResultEvent->DESCRIPTION->value : '';
					$mResult['When'] = CCalendarHelper::GetStrDate($oResultEvent->DTSTART,
							$oAccount->GetDefaultStrTimeZone(), 'D, M d, Y, H:i');
					$mResult['CalendarId'] = $sCalId;

					if (isset($oResultEvent->ATTENDEE))
					{
						foreach($oResultEvent->ATTENDEE as $attendee)
						{
							$sEmail = str_replace('mailto:', '', strtolower($attendee->value));
							$sAccountEmail = $oAccount->Email;
							if ($sMethod == 'REPLY')
							{
								$sAccountEmail = $mFromEmail;
							}
							if ($sAccountEmail == $sEmail)
							{
								$PARTSTAT = $attendee->offsetGet('PARTSTAT');
								if (isset($PARTSTAT))
								{
									$mResult['Action'] = $sMethod.'-'.$PARTSTAT->value;
								}
							}
						}
					}
				}
			}

		}
		catch (Exception $oException)
		{
			$mResult = false;
			$this->setLastException($oException);
		}

		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sAction
	 * @param string $sCalendarId
	 * @param string $sData
	 */
	public function ProcessICS($oAccount, $sAction, $sCalendarId, $sData)
	{
		$iAccountId = $this->ApiUsersManager->GetDefaultAccountId($oAccount->IdUser);

		/* @var $oAccount CAccount */
		$oAccount = $this->ApiUsersManager->GetAccountById($iAccountId);

		$bResult = null;
		$sEventId = null;
		try
		{
			$bResult = false;
			$sTo = $sSubject = $sBody = $sSummary = '';

			$vCal = \Sabre\VObject\Reader::read($sData);
			if ($vCal)
			{
				$sMethod = $sMethodOriginal = $vCal->METHOD->value;
				$vEvents = $vCal->getBaseComponents('VEVENT');

				if (isset($vEvents) && count($vEvents) > 0)
				{
					$vEvent = $vEvents[0];
					$sEventId = $vEvent->UID->value;
					if (isset($vEvent->SUMMARY))
					{
						$sSummary = $vEvent->SUMMARY->value;
					}
					if (isset($vEvent->ORGANIZER))
					{
						$sTo = str_replace('mailto:', '', strtolower($vEvent->ORGANIZER->value));
					}
					if (strtoupper($sMethod) === 'REQUEST')
					{
						$sMethod = 'REPLY';
						$sSubject = $sSummary;

						unset($vEvent->ATTENDEE);
						$attendee = new \Sabre\VObject\Property('ATTENDEE');
						$attendee->value = 'mailto:'.$oAccount->Email;

						switch (strtoupper($sAction))
						{
							case 'ACCEPTED':
								$attendee->offsetSet('PARTSTAT', 'ACCEPTED');
								$sSubject .= ': Accepted';
								break;
							case 'DECLINED':
								$attendee->offsetSet('PARTSTAT', 'DECLINED');
								$sSubject .= ': Declined';
								break;
							case 'TENTATIVE':
								$attendee->offsetSet('PARTSTAT', 'TENTATIVE');
								$sSubject .= ': Tentative';
								break;
						}
						$attendee->offsetSet('RESPONDED-AT', gmdate("Ymd\THis\Z"));
						$vEvent->add($attendee);
					}

					$vCal->METHOD = $sMethod;
					$vEvent->{'LAST-MODIFIED'} = gmdate("Ymd\THis\Z");

					$sBody = $vCal->serialize();

					if ($sCalendarId !== false)
					{
						unset($vCal->METHOD);
						if (strtoupper($sAction) == 'DECLINED' || strtoupper($sMethod) == 'CANCEL')
						{
							$this->DeleteEvent($oAccount, $sCalendarId, $sEventId);
						}
						else
						{
							$this->oStorage->UpdateEventData($oAccount, $sCalendarId, $sEventId, $vCal->serialize());
						}
					}

					if (strtoupper($sMethodOriginal) == 'REQUEST' && (strtoupper($sAction) !== 'DECLINED'))
					{
						if (!empty($sTo) && !empty($sBody))
						{
							$bResult = CCalendarHelper::SendAppointmentMessage($oAccount, $sTo, $sSubject, $sBody, $sMethod);
						}
					}
					else
					{
						$bResult = true;
					}
				}
			}

			if (!$bResult)
			{
				CApi::Log('IcsProcessAppointment FALSE result!', ELogLevel::Error);
				CApi::Log('Email: '.$oAccount->Email.', Action: '. $sAction.', Data:', ELogLevel::Error);
				CApi::Log($sData, ELogLevel::Error);
			}
			else
			{
				$bResult = $sEventId;
			}

			return $bResult;
		}
		catch (Exception $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}
		return $bResult;
	}

	public function UpdateAppointment($oAccount, $sCalendarId, $sEventId, $sAction)
	{
		$oResult = null;
		try
		{
			$aData = $this->oStorage->GetEvent($oAccount, $sCalendarId, $sEventId);
			if ($aData !== false)
			{
				$vCal = $aData['vcal'];
				$vCal->METHOD = 'REQUEST';
				return $this->ProcessICS($oAccount, $sAction, $sCalendarId, $vCal->serialize());
			}
		}
		catch (Exception $oException)
		{
			$oResult = false;
			$this->setLastException($oException);
		}
		return $oResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @return bool
	 */
	public function ClearAllCalendars($oAccount)
	{
		$bResult = false;
		try
		{
			$bResult = $this->oStorage->ClearAllCalendars($oAccount);
		}
		catch (CApiBaseException $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}
		return $bResult;
	}
}
