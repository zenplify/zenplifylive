<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

/**
 * @package Calendar
 * @subpackage Classes
 */
class CCalendar
{
	public $Id;
	public $Url;
	public $IsDefault;
	public $DisplayName;
	public $CTag;
	public $ETag;
	public $Description;
	public $Color;
	public $Order;
	public $Shared;
	public $Owner;
	public $Principals;
	public $SharingLevel;
	public $RealUrl;

	function __construct($sId, $sDisplayName = null, $sCTag = null, $sETag = null, $sDescription = null,
			$sColor = null, $sOrder = null)
	{
		$this->Id = rtrim(urldecode($sId), '/');

		if (basename($sId) == \afterlogic\DAV\Constants::ADDRESSBOOK_DEFAULT_NAME)
		{
			$this->IsDefault = true;
		}
		else
		{
			$this->IsDefault = false;
		}

		$this->DisplayName = $sDisplayName;
		$this->CTag = $sCTag;
		$this->ETag = $sETag;
		$this->Description = $sDescription;
		$this->Color = $sColor;
		$this->Order = $sOrder;
		$this->Shared = false;
		$this->Owner = '';
		$this->Principals = array();
		$this->SharingLevel = null;
		$this->Shares = array();
	}
}