<?php

/**
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @property mixed $IdEvent
 * @property mixed $IdCalendar
 * @property string $StartDate
 * @property string $StartTime
 * @property string $EndDate
 * @property string $EndTime
 * @property string $EventStartTime
 * @property bool $AllDay
 * @property string $Name
 * @property string $Description
 * @property bool $IsRepeat
 * @property string $RepeatPeriod
 * @property string $RepeatTimes
 * @property string $RepeatUntil
 * @property string $RepeatOrder
 * @property string $RepeatEnd
 * @property string $RepeatWeekNum
 * @property bool $RepeatSun
 * @property bool $RepeatMon
 * @property bool $RepeatTue
 * @property bool $RepeatWed
 * @property bool $RepeatThu
 * @property bool $RepeatFri
 * @property bool $RepeatSat
 * @property string $ReminderOffset
 * @property array $AppointmentsToDelete;
 * @property array $AppointmentsToSave;
 *
 * @property string $EventStartTime;
 * @property bool $Deleted;
 *
 * @package Calendar
 * @subpackage Classes
 */
class CEvent
{
	public $IdEvent;
	public $IdCalendar;
	public $StartDate;
	public $StartTime;
	public $EndDate;
	public $EndTime;
	public $AllDay;
	public $Name;
	public $Description;
	public $Location;

	public $IsRepeat;
	public $RRule;

	public $ReminderOffset;

	public $AppointmentsToDelete;
	public $AppointmentsToSave;

	public $EventStartTime;
    public $Deleted;

	public function __construct($Account)
	{
		$this->IdEvent		  = null;
		$this->IdCalendar	  = null;
		$this->StartDate	  = null;
		$this->StartTime	  = null;
		$this->EndDate		  = null;
		$this->EndTime		  = null;
		$this->AllDay		  = false;
		$this->Name			  = null;
		$this->Description	  = null;
		$this->Location		  = null;
		$this->IsRepeat		  = false;
		
		$this->RRule		  = null;
		
		$this->ReminderOffset = null;
		$this->AppointmentsToDelete = null;
		$this->AppointmentsToSave = null;

		$this->EventStartTime = null;
		$this->Deleted = null;
	}
}

class CRRule
{
	public $Period;
	public $Times;
	public $Until;
	public $Order;
	public $End;
	public $WeekNum;
	public $Sun;
	public $Mon;
	public $Tue;
	public $Wed;
	public $Thu;
	public $Fri;
	public $Sat;	
	
	protected $Account;
	protected $Parent;
	
	public function __construct($oAccount, $Parent = null)
	{
		$this->Period	  = null;
		$this->Times	  = null;
		$this->Until	  = null;
		$this->Order	  = null;
		$this->End		  = null;
		$this->WeekNum	  = null;
		$this->Sun		  = false;
		$this->Mon		  = false;
		$this->Tue		  = false;
		$this->Wed		  = false;
		$this->Thu		  = false;
		$this->Fri		  = false;
		$this->Sat	  	  = false;		
		
		$this->Account = $oAccount;
		$this->Parent = $Parent;
	}	
	
	public function GetStartTime()
	{
		$Result = null;
		if (isset($this->Parent))
		{
			$Result = $this->Parent->StartTime;
		}
		return $Result;
	}
	
	public function GetTimeZone()
	{
		return $this->Account->GetDefaultStrTimeZone();
	}

	public function ToArray()
	{
		return array(
			'repeat_period' => (string) $this->Period,
			'repeat_order' => $this->Order,
			'repeat_end' => !isset($this->End) ? '0' : $this->End,
			'repeat_until' => $this->Until,
			'week_number' => $this->WeekNum,
			'repeat_num' => $this->Times,
			'sun' => $this->Sun ? '1' : '0',
			'mon' => $this->Mon ? '1' : '0',
			'tue' => $this->Tue ? '1' : '0',
			'wed' => $this->Wed ? '1' : '0',
			'thu' => $this->Thu ? '1' : '0',
			'fri' => $this->Fri ? '1' : '0',
			'sat' => $this->Sat ? '1' : '0',
		);
	}
	
	public function ToString()
	{
		$aPeriods = array(
			EPeriod::Secondly,
			EPeriod::Minutely,
			EPeriod::Hourly,
			EPeriod::Daily,
			EPeriod::Weekly,
			EPeriod::Monthly,
			EPeriod::Yearly
		);
		
		$sRule = $sFreq = $sUntil = '';
		$iInterval = $iCount = $iEnd = 0;

		if (null !== $this->Period)
		{
			$iPeriod = (int) $this->Period;
			$sFreq = strtoupper($aPeriods[$iPeriod + 3]);

			$weekNumber = null;
			if (($iPeriod == 2 || $iPeriod == 3))
			{
				if (null !== $this->WeekNum)
				{
					$weekNumber = (int) $this->WeekNum;
					$weekNumber = ($weekNumber < 0 || $weekNumber > 4) ? 0 : $weekNumber;
				}
				else
				{
					$weekNumber = null;
				}
			}
			else
			{
				$weekNumber = null;
			}
			if (null !== $this->Times)
			{
				$iCount = (int) $this->Times;
			}
			if (null !== $this->Until)
			{
				$oDTUntil = CCalendarHelper::PrepareDateTime($this->Until, $this->GetStartTime(), 
						$this->GetTimeZone());
				$oUntil = new \Sabre\VObject\Property\DateTime('UNTIL');
				$oUntil->setDateTime($oDTUntil, \Sabre\VObject\Property\DateTime::UTC);
				$sUntil = $oUntil->value;
			}
			if (null !== $this->Order)
			{
				$iInterval = (int) $this->Order;
			}
			if (null !== $this->End)
			{
				$iEnd = (int) $this->End;
				if ($iEnd < 0 || $iEnd > 3)
				{
					$iEnd = 0;
				}
			}

			if($iEnd == 0)
			{
				$sRule = 'FREQ=' . $sFreq . ';INTERVAL=' . $iInterval;
			}
			else if ($iEnd == 1)
			{
				$sRule = 'FREQ=' . $sFreq . ";INTERVAL=" . $iInterval .";COUNT=" . $iCount;
			}
			else if ($iEnd == 2)
			{
				$sRule = 'FREQ=' . $sFreq . ";INTERVAL=" . $iInterval . ";UNTIL=" . $sUntil;
			}

			$aByDays = array();
			$sByDay = '';
			if ($sFreq == 'WEEKLY' || $sFreq == 'MONTHLY' || $sFreq == 'YEARLY')
			{
				if ($this->Sun) $aByDays[] = 'SU';
				if ($this->Mon) $aByDays[] = 'MO';
				if ($this->Tue) $aByDays[] = 'TU';
				if ($this->Wed) $aByDays[] = 'WE';
				if ($this->Thu) $aByDays[] = 'TH';
				if ($this->Fri) $aByDays[] = 'FR';
				if ($this->Sat) $aByDays[] = 'SA';
			}
			if (count($aByDays) > 0)
			{
				$sByDay = implode(',', $aByDays);
				if ($sFreq == 'WEEKLY')
				{
					$sRule .= ';BYDAY='.$sByDay;
				}
				if (($sFreq === 'MONTHLY' || $sFreq === 'YEARLY') && isset($weekNumber))
				{
					if ($weekNumber == 0) $sByDay = '1'.$sByDay;
					if ($weekNumber == 1) $sByDay = '2'.$sByDay;
					if ($weekNumber == 2) $sByDay = '3'.$sByDay;
					if ($weekNumber == 3) $sByDay = '4'.$sByDay;
					if ($weekNumber == 4) $sByDay = '-1'.$sByDay;
					$sRule .= ';BYDAY='.$sByDay;
				}
			}
		}
        return $sRule;		
	}
}
