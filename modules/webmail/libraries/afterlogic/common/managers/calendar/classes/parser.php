<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

/**
 * @package Calendar
 * @subpackage Classes
 */
class CalendarParser
{
	public static function GetData($oAccount, $oCalendar, $vCal)
	{
		$aResult = array(
			'data' => array(),
			'events' => array(),
			'exclusions' => array(),
			'reminders' => array(),
			'appointments' => array()
		);
		$ApiCollaborationManager = CApi::Manager('collaboration');

		if (isset($vCal))
		{
			$vBaseEvents = $vCal->getBaseComponents('VEVENT');
			if (isset($vBaseEvents[0]) && $vBaseEvents[0]->UID)
			{
				$BaseEvent = $vBaseEvents[0];
				$sEventId = $BaseEvent->UID->value;
				$aResult['data'][$sEventId] = $vCal;

				$bIsAppointment = false;

				// Appointments
				if ($ApiCollaborationManager &&
						$ApiCollaborationManager->IsCalendarAppointmentsSupported())
				{
					if ($BaseEvent->ATTENDEE)
					{
						$aResult['appointments'][$sEventId] = self::GetAppointments($BaseEvent);

						if ($BaseEvent->ORGANIZER)
						{
							$oCalendar->Owner = str_replace('mailto:', '',
									strtolower($BaseEvent->ORGANIZER->value));
						}
						if ($oCalendar->Owner !== $oAccount->Email)
						{
							$bIsAppointment = true;
						}
					}
				}

				// Reminders
				if ($BaseEvent->VALARM)
				{
					$aResult['reminders'][$sEventId] = self::GetAlarms($BaseEvent);
				}

				$aSimpleEvent = self::GetSimpleEvent($oAccount, $oCalendar, $BaseEvent);

				$aResult['events'][$sEventId] = $aSimpleEvent;
				$aResult['events'][$sEventId]['appointment'] = $bIsAppointment;
				$aResult['events'][$sEventId]['appointment_access'] = '0';

				$sWeekDays = array(0, 0, 0, 0, 0, 0, 0);
				$bEventRepeats = false;
				if (isset($BaseEvent->RRULE))
				{
					$oRRule = self::GetRRule($oAccount, $vCal, $sEventId);
					if (isset($oRRule))
					{
						$bEventRepeats = true;
						
						$aRRULE = $oRRule->ToArray();
						$aResult['events'][$sEventId] = array_merge(
								$aResult['events'][$sEventId], $aRRULE);

						$sWeekDays = array(
							$aRRULE['sun'],
							$aRRULE['mon'],
							$aRRULE['tue'],
							$aRRULE['wed'],
							$aRRULE['thu'],
							$aRRULE['fri'],
							$aRRULE['sat']
						);
					}
				}

				$aResult['events'][$sEventId]['event_repeats'] = $bEventRepeats ? '1' : '0';

				if (isset($BaseEvent->EXDATE))
				{
					$aResult['events'][$sEventId]['excluded'] = '1';
					foreach ($BaseEvent->EXDATE as $exdate)
					{
						$recurrenceId = CCalendarHelper::GetStrDate($exdate,
								$oAccount->GetDefaultStrTimeZone(), 'Ymd');

//							$repeatId = DAV_Convert_Utils::_getRepeatIdExclution($exdate, $vCal, $eventId);
						$repeatId = CCalendarHelper::getRepeatIdExclution(
								new DateTime($aResult['events'][$sEventId]['event_dtstart']),
								$exdate->value,
								$aResult['events'][$sEventId]['repeat_order'],
								$aResult['events'][$sEventId]['repeat_period'],
								$sWeekDays,
								isset($aResult['events'][$sEventId]['week_number'])?
									$aResult['events'][$sEventId]['week_number']:'0'
						);

						$sExId = $sEventId.'_'.$repeatId.'_'.$recurrenceId;

						$aResult['exclusions'][$sExId] = $aSimpleEvent;
						$aResult['exclusions'][$sExId]['id_recurrence'] = $recurrenceId;
						$aResult['exclusions'][$sExId]['id_repeat'] = $repeatId;
						$aResult['exclusions'][$sExId]['is_deleted'] = '1';
					}
				}
			}

			if (isset($vCal->VEVENT))
			{
				foreach ($vCal->VEVENT as $vEvent)
				{
					$sEventId = $vEvent->UID->value;
					if (isset($vEvent->{'RECURRENCE-ID'}) &&
						!empty($aResult['events'][$sEventId]['event_repeats']))
					{
						$aResult['events'][$sEventId]['excluded'] = '1';

						$sWeekDays = array(
							$aResult['events'][$sEventId]['sun'],
							$aResult['events'][$sEventId]['mon'],
							$aResult['events'][$sEventId]['tue'],
							$aResult['events'][$sEventId]['wed'],
							$aResult['events'][$sEventId]['thu'],
							$aResult['events'][$sEventId]['fri'],
							$aResult['events'][$sEventId]['sat']
						);

						$recurrenceId = CCalendarHelper::GetStrDate($vEvent->{'RECURRENCE-ID'},
								$oAccount->GetDefaultStrTimeZone(), 'Ymd');
//						$repeatId = DAV_Convert_Utils::_getRepeatIdExclution($event->{'RECURRENCE-ID'}, $vCal, $eventId);

						$repeatId = CCalendarHelper::getRepeatIdExclution(
								new DateTime($aResult['events'][$sEventId]['event_dtstart']),
								$vEvent->{'RECURRENCE-ID'}->value,
								$aResult['events'][$sEventId]['repeat_order'],
								$aResult['events'][$sEventId]['repeat_period'],
								$sWeekDays,
								isset($aResult['events'][$sEventId]['week_number'])?
										$aResult['events'][$sEventId]['week_number']:'0');

						$sExId = $sEventId . '_' . $repeatId . '_' . $recurrenceId;

						$aResult['exclusions'][$sExId] = self::GetSimpleEvent($oAccount,
								$oCalendar, $vEvent);

						$aResult['exclusions'][$sExId]['id_recurrence'] = $recurrenceId;
						$aResult['exclusions'][$sExId]['id_repeat'] = $repeatId;
						$aResult['exclusions'][$sExId]['is_deleted'] = '0';
					}
				}
			}
		}

		return $aResult;
	}

	public static function GetSimpleEvent($oAccount, $oCalendar, $oEvent)
	{
		$eventAllDay = '0';
		$event_dtstart = null;
		if (isset($oEvent->DTSTART))
		{
			$event_dtstart = $oEvent->DTSTART->value;
			$dateParam = $oEvent->DTSTART->offsetGet('value');
			if ($dateParam && strtoupper($dateParam->value) == 'DATE')
			{
				$eventAllDay = '1';
				$event_dtstart = $oEvent->DTSTART->value . 'T000000Z';
			}
		}
		
		$sEventDTStart = CCalendarHelper::GetStrDate($oEvent->DTSTART,
				$oAccount->GetDefaultStrTimeZone());

		$oEventDTEnd = CCalendarHelper::GetDateTime($oEvent->DTEND,
				$oAccount->GetDefaultStrTimeZone());
		
		if ($eventAllDay === '1')
		{
			$oEventDTEnd->sub(new DateInterval('P1D'));
		}
		$sEventDTEnd = CCalendarHelper::DateTimeToStr($oEventDTEnd);

		$aResult = array(
			'calendar_id' => $oCalendar->Id,
			'allday_flag' => (int) $eventAllDay,
			'event_allday' => $eventAllDay,
			'event_id' => $oEvent->UID->value,
			'event_name' => $oEvent->SUMMARY ? $oEvent->SUMMARY->value : '',
			'event_text' => $oEvent->DESCRIPTION ? $oEvent->DESCRIPTION->value : '',
			'event_location' => $oEvent->LOCATION ? $oEvent->LOCATION->value : '',
			'event_timefrom' => $sEventDTStart,
			'event_timetill' => $sEventDTEnd,
			'event_dtstart' => $event_dtstart,
			'event_owner' => $oCalendar->Owner
		);

		return $aResult;
	}

	public function GetEvents()
	{
		;
	}

	public function GetExclusions()
	{
		;
	}

	public static function GetAlarms($Event)
	{
		$aResult = array();
		
		if ($Event->VALARM)
		{
			$sEventId = $Event->UID->value;
			$aOffsets = array();
			foreach($Event->VALARM as $vAlarm)
			{
				if (isset($vAlarm->TRIGGER))
				{
					$aOffsets[] = CCalendarHelper::GetOffsetInMinutes($vAlarm->TRIGGER->value);
				}
			}

			rsort($aOffsets);

			$iReminderId = 1;
			foreach ($aOffsets as $Offset)
			{
				$aResult[$iReminderId] = array(
					'id_reminder' => $iReminderId,
					'id_event' => $sEventId,
					'offset' => CCalendarHelper::GetOffsetInStr($Offset)
				);
				$iReminderId++;
			}
		}	
		
		return $aResult;
	}

	public static function GetAppointments($BaseEvent)
	{
		$aResult = array();
		foreach($BaseEvent->ATTENDEE as $oAttendee)
		{
			$iStatus = 0;
			$oPartstat = '';
			if ($oAttendee->offsetExists('PARTSTAT'))
			{
				$oPartstat = $oAttendee->offsetGet('PARTSTAT');
				switch (strtoupper($oPartstat->value))
				{
					case 'ACCEPTED':
						$iStatus = 1;
						break;
					case 'DECLINED':
						$iStatus = 2;
						break;
					case 'TENTATIVE':
						$iStatus = 3;
						break;
				}
			}
			$sEmail = '';
			if ($oAttendee->offsetExists('EMAIL'))
			{
				$sEmail = $oAttendee->offsetGet('EMAIL')->value;
			}
			else
			{
				$sEmail = str_replace('mailto:', '', strtolower($oAttendee->value));
			}

			$ApiUsersManager = CApi::Manager('users');
			$oAcct = $ApiUsersManager->GetAccountOnLogin($sEmail);
			$iUserId = 0;
			if (isset($oAcct))
			{
				$iUserId = $oAcct->IdUser;
			}
			$iAccessType = 0;
			$aResult[$sEmail] = array(
					'accessType' => $iAccessType,
					'appointmentId' => $sEmail,
					'email' => $sEmail,
					'eventId' => $BaseEvent->UID->value,
					'status' => $iStatus,
					'userId' => $iUserId
			);
		}
		return $aResult;
	}

	public static function GetRRule($oAccount, $oVCal, $sUid)
	{
		$oResult = null;

		$aPeriods = array(
			EPeriod::Secondly,
			EPeriod::Minutely,
			EPeriod::Hourly,
			EPeriod::Daily,
			EPeriod::Weekly,
			EPeriod::Monthly,
			EPeriod::Yearly
		);

		$oRecur = new \Sabre\VObject\RecurrenceIterator($oVCal, $sUid);

		if (isset($oRecur))
		{
			$oResult = new CRRule($oAccount);
			if (isset($oRecur->frequency))
			{
				$isPosiblePeriod = array_search($oRecur->frequency, $aPeriods);
				if ($isPosiblePeriod !== false)
				{
					$oResult->Period = $isPosiblePeriod - 3;
				}
			}
			if (isset($oRecur->bySetPos))
			{
				$oResult->WeekNum = $oRecur->bySetPos;
			}
			if (isset($oRecur->interval))
			{
				$oResult->Order = $oRecur->interval;
			}
			if (isset($oRecur->count))
			{
				$oResult->Times = $oRecur->count;
			}
			if (isset($oRecur->until))
			{
				$oRecur->until->setTimezone(new DateTimeZone($oAccount->GetDefaultStrTimeZone()));
				$oResult->Until = $oRecur->until->format('Y-m-d H:i:s');
			}
			if (isset($oResult->Times))
			{
				$oResult->End = 1;
			}
			if (isset($oResult->Until))
			{
				$oResult->End = 2;
			}
			if (isset($oRecur->byDay))
			{
				foreach ($oRecur->byDay as $day)
				{
					if (strlen($day) > 2)
					{
						$num = (int)substr($day, 0, -2);

						if ($num == 1) $oResult->WeekNum = 0;
						if ($num == 2) $oResult->WeekNum = 1;
						if ($num == 3) $oResult->WeekNum = 2;
						if ($num == 4) $oResult->WeekNum = 3;
						if ($num == -1) $oResult->WeekNum = 4;
					}

					if (strpos($day, 'SU') !== false) $oResult->Sun = true;
					if (strpos($day, 'MO') !== false) $oResult->Mon = true;
					if (strpos($day, 'TU') !== false) $oResult->Tue = true;
					if (strpos($day, 'WE') !== false) $oResult->Wed = true;
					if (strpos($day, 'TH') !== false) $oResult->Thu = true;
					if (strpos($day, 'FR') !== false) $oResult->Fri = true;
					if (strpos($day, 'SA') !== false) $oResult->Sat = true;
				}
			}
		}
		return $oResult;
	}
}