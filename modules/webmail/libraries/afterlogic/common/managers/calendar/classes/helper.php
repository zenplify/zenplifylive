<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

/**
 * @package Calendar
 * @subpackage Classes
 */
class CCalendarHelper
{
	const encryptionKey = 'udMP69';

	public static function GetReminderOffset($sValue)
	{
        $sValue = str_replace('-', '', $sValue);
		$oInterval = new DateInterval($sValue);

		return ($oInterval->s)+($oInterval->i*60)+($oInterval->h*60*60)+
				($oInterval->d*60*60*24)+($oInterval->m*60*60*24*30)+
					($oInterval->y*60*60*24*365);
	}
	
	public static function GetActualReminderTime($oEvent, $oNowDT, $oStartDT)
	{
		$aReminders = CalendarParser::GetAlarms($oEvent);
		
		$iNowTS = $oNowDT->getTimestamp();
		
		$iStartEventTS = $oStartDT->getTimestamp();

		$aRemindersTime = array();
		foreach ($aReminders as $iReminder)
		{
			$iReminderOffset = CCalendarHelper::GetReminderOffset($iReminder['offset']);
			$aRemindersTime[] = $iStartEventTS - $iReminderOffset;
		}
		sort($aRemindersTime);
		foreach ($aRemindersTime as $iReminder)
		{
			if ($iReminder > $iNowTS)
			{
				return $iReminder;
			}
		}
		return false;
	}

	public static function GetNextRepeat(DateTime $sDtStart, $sRepeatOrder, $sRepeatPeriod,
			$sWeekDays, $WeekNumber)
	{
		$iRepeatOrder = (int)$sRepeatOrder;
		$iRepeatPeriod = (int)$sRepeatPeriod;
		$iWeekNumber = (int)$WeekNumber + 1;
		switch($iRepeatPeriod)
		{
			case 0:
				$sDtStart->add(new DateInterval('P'.$iRepeatOrder.'D'));
				return $sDtStart;
				break;
			case 1:
				$startWeekDay = $sDtStart->format('w');

				$i = $startWeekDay+1;
				$daysNum = 0;
				do
				{
					$daysNum++;
					if ($i > 6) $i = 0;
					if ($sWeekDays[$i] == '1')
					{
						$sDtStart->add(new DateInterval('P'.$daysNum.'D'));
						return $sDtStart;
					}
					$i++;
				}
				while(true);
				break;
			case 2:
				$day_of_week = $sDtStart->format('w');

				$sDtStart->add(new DateInterval('P'.$iRepeatOrder.'M'));

				$month = $sDtStart->format('n');
				$year = $sDtStart->format('Y');

				$dt = $day_of_week + 7 * $iWeekNumber - 6 - date('w',mktime(0,0,0,$month,1,$year));
				$day = 1;
				if (date('w', mktime(0,0,0,$month,1,$year)) <= $day_of_week)
				{

					$day = $dt;
				}
				else
				{
					$day = $dt + 7;
				}
				$sDtStart->setDate($year, $month, $day);
				return $sDtStart;
				break;
			case 3:
				$day_of_week = $sDtStart->format('w');
				$sDtStart->add(new DateInterval('P'.$iRepeatOrder.'Y'));
				$month = $sDtStart->format('n');
				$year = $sDtStart->format('Y');

				$dt = $day_of_week + 7 * $iWeekNumber - 6 - date('w',mktime(0,0,0,$month,1,$year));
				$day = 1;
				if (date('w', mktime(0,0,0,$month,1,$year)) <= $day_of_week)
				{

					$day = $dt;
				}
				else
				{
					$day = $dt + 7;
				}
				$sDtStart->setDate($year, $month, $day);
				return $sDtStart;
				break;

		}

	}

	public static function getRepeatIdExclution(DateTime $sDtStart, $sDtEx, $sRepeatOrder, $sRepeatPeriod,
			$sWeekDays, $WeekNumber)
	{
		$sDtEx = \Sabre\VObject\DateTimeParser::parse($sDtEx);
		if ($sDtStart->format('Ymd') == $sDtEx->format('Ymd'))
		{
			return 0;
		}

		$result = $sDtStart;
		$repeatNum = 0;
		do
		{
			$repeatNum++;
			$result = self::GetNextRepeat($result, $sRepeatOrder, $sRepeatPeriod, $sWeekDays, $WeekNumber);
			if ($result->format('Ymd') == $sDtEx->format('Ymd'))
			{
				break;
			}
			if ($repeatNum >= 200)
			{
				$repeatNum = 0;
				break;
			}
		}
		while(true);

		return $repeatNum;
	}

	public static function _getNextRepeat(DateTime $sDtStart, $vCal, $sUid = null)
	{
		$oRecur = new \Sabre\VObject\RecurrenceIterator($vCal, $sUid);
		$oRecur->fastForward($sDtStart);
		return $oRecur->currentDate;
	}

	public static function _getRepeatIdExclution($oDtEx, $vCal, $uid)
	{
		$dt = null;
		if ($oDtEx instanceof \Sabre\VObject\Property\DateTime)
		{
			$dt = $oDtEx->getDateTime();
		}
		if ($oDtEx instanceof \Sabre\VObject\Property\MultiDateTime)
		{
			$dt = $oDtEx->getDateTimes();
			$dt = current($dt);
		}
		$oRecur = new \Sabre\VObject\RecurrenceIterator($vCal, $uid);
		if (isset($dt))
		{
			$oRecur->fastForward($dt);
		}
		return $oRecur->counter;
	}

	/**
	 * @param int $data
	 * @param int $min
	 * @param int $max
	 * @return bool
	 */
	public static function validate($data, $min, $max)
	{
		if (null === $data) return false;
		$data = round($data);
		return (isset($min) && isset($max)) ? ($min <= $data && $data <= $max) : ($data > 0);
	}


	public static function encryptId($int)
	{
//		return api_Utils::UrlSafeBase64Encode($int.'-'.substr(sha1($int.self::encryptionKey), 0, 6));
		return self::alphaID($int);
	}

	public static function decryptId($string)
	{
/*
		$parts = explode('-', api_Utils::UrlSafeBase64Decode($string));
		if (count($parts) != 2)
		{
			return 0;
		}

		$int = $parts[0];
		return substr(sha1($int.self::encryptionKey), 0, 6) === $parts[1] ? (int)$int : 0;
*/
		return self::alphaID($string, true);


	}

	public static function alphaID($in, $to_num = false)
	{
	  $index = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	  $base  = strlen($index);

	  if ($to_num)
	  {
		// Digital number  <<--  alphabet letter code
		$in  = strrev($in);
		$out = 0;
		$len = strlen($in) - 1;
		for ($t = 0; $t <= $len; $t++)
		{
		  $bcpow = pow($base, $len - $t);
		  $out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
		}

		$out = sprintf('%F', $out);
		$out = substr($out, 0, strpos($out, '.'));
	  }
	  else
	  {
		// Digital number  -->>  alphabet letter code

		$out = "";
		for ($t = floor(log($in, $base)); $t >= 0; $t--)
		{
		  $bcp = pow($base, $t);
		  $a   = floor($in / $bcp) % $base;
		  $out = $out . substr($index, $a, 1);
		  $in  = $in - ($a * $bcp);
		}
		$out = strrev($out); // reverse
	  }

	  return $out;
	}

	public static function PrepareDateTime($sDate, $sTime, $sTimeZone, $bAllday = false, $bEndTime = false)
	{
		if (!empty($sDate))
		{
			$sDate = urldecode(trim($sDate));

			$year = substr($sDate, 0, 4);
			$month = substr($sDate, 4, 2);
			$day = substr($sDate, 6, 2);

			$hour = $min = '00';
			if (!$bAllday && !empty($sTime))
			{
				$sTime = trim(urldecode($sTime));
				list($hour, $min) = explode(':', $sTime);
			}

			$sDateTime = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':00';
			$oDateTime = new DateTime($sDateTime, new DateTimeZone($sTimeZone));
			if ($bAllday && $bEndTime)
			{
				$oDateTime->add(new DateInterval('P1D'));
			}

			return $oDateTime;
		}
		return null;
	}

	public static function GetDateTime($dt, $sTimeZone = 'UTC')
	{
		$result = null;
		if ($dt instanceof \Sabre\VObject\Property\DateTime)
		{
			$result = $dt->getDateTime();
		}
		if ($dt instanceof \Sabre\VObject\Property\MultiDateTime)
		{
			$result = $dt->getDateTimes();
			$result = current($result);
		}
		if (isset($result))
		{
			$result->setTimezone(new DateTimeZone($sTimeZone));
		}
		return $result;
	}
			
	public static function GetStrDate($dt, $sTimeZone, $format = 'Y-m-d H:i:s')
	{
		$result = null;
		$oDateTime = self::GetDateTime($dt, $sTimeZone);
		if ($oDateTime)
		{
			$result = $oDateTime->format($format);
		}
		return $result;
	}

	public static function DateTimeToStr($dt, $format = 'Y-m-d H:i:s')
	{
			return $dt->format($format);
	}

	public static function isRecurrenceExists($oAccount, $vEvent, $sRecurrenceId)
	{
		$mResult = false;
		foreach($vEvent as $mKey => $event)
		{
			if (isset($event->{'RECURRENCE-ID'}))
			{
				$recurrenceId = self::GetStrDate($event->{'RECURRENCE-ID'},
						$oAccount->GetDefaultStrTimeZone(), 'Ymd');

				if ($recurrenceId == $sRecurrenceId)
				{
					$mResult = $mKey;
					break;
				}
			}
		}

		return $mResult;
	}

    /**
	 * @param string $sOffset
	 * @return string
	 */
	public static function GetOffsetInMinutes($sOffset)
	{
 		$aIntervals = array(5,10,15,30,60,120,180,240,300,360,420,480,540,600,660,720,1080,1440,2880,4320,5760,10080,20160);
		$iMinutes = 0;
		try
		{
			$oInterval = new DateInterval(ltrim($sOffset, '-'));
			$iMinutes = $oInterval->i + $oInterval->h*60 + $oInterval->d*24*60;
		}
		catch (Exception $ex)
		{
			$iMinutes = 15;
		}
		if (!in_array($iMinutes, $aIntervals))
		{
			$iMinutes = 15;
		}

		return $iMinutes;
	}
	
    /**
	 * @param string $sOffset
	 * @return string
	 */
	public static function GetOffsetInStr($iMinutes)
	{
		return '-PT' . $iMinutes . 'M';
	}
	
	

	public static function GetBaseVEventIndex($vEvents)
	{
		$mResult = false;
		$iIndex = -1;
		foreach($vEvents as $vEvent)
		{
			$iIndex++;
			if (empty($vEvent->{'RECURRENCE-ID'})) break;
		}
		if ($iIndex >= 0)
		{
			$mResult = $iIndex;
		}
		else
		{
			$mResult = false;
		}
		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sTo
	 * @param string $sSubject
	 * @param string $sBody
	 * @param string $sMethod
	 * @return WebMailMessage
	 */
	public static function BuildAppointmentMessage($oAccount, $sTo, $sSubject, $sBody, $sMethod = null)
	{
		if (!defined('WM_ROOTPATH'))
		{
			return false;
		}

		require_once WM_ROOTPATH.'common/inc_constants.php';
		require_once WM_ROOTPATH.'common/class_mailprocessor.php';
		require_once WM_ROOTPATH.'common/class_folders.php';

		$oMessage = null;
		if ($oAccount && !empty($sTo) && !empty($sBody))
		{
			$oMessage = new WebMailMessage();
			$GLOBALS[MailDefaultCharset] = CPAGE_UTF8;
			$GLOBALS[MailInputCharset] = CPAGE_UTF8;
			$GLOBALS[MailOutputCharset] = APP_DEFAULT_OUTPUT_CHARSET;

			$oMessage->Headers->SetHeaderByName(MIMEConst_MimeVersion, '1.0');
			$oMessage->Headers->SetHeaderByName(MIMEConst_XMailer, CApi::GetConf('webmail.xmailer-value', 'PHP'));

			$sIp = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
			if (null !== $sIp)
			{
				$oMessage->Headers->SetHeaderByName(MIMEConst_XOriginatingIp, $sIp);
			}

			$sServerAddr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['SERVER_NAME'] : 'cantgetservername';
			$oMessage->Headers->SetHeaderByName(MIMEConst_MessageID,
				'<'.substr(session_id(), 0, 7).'.'.md5(time()).'@'. $sServerAddr .'>');

			$emailAccount = $oAccount->Email;

			$oMessage->SetFromAsString($emailAccount);
			$oMessage->SetToAsString($sTo);
			$oMessage->SetSubject($sSubject);
			$oMessage->SetDate(new CDateTime(time()));

			if (!empty($sMethod))
			{
				$oMessage->TextBodies->CustomContentType = 'text/calendar; method='.$sMethod.'; charset="utf-8"';
			}
			else
			{
				$oMessage->TextBodies->CustomContentType = 'text/calendar; charset="utf-8"';
			}
			$oMessage->TextBodies->CustomContentTransferEncoding = MIMEConst_8bit;
			$oMessage->TextBodies->PlainTextBodyPart = $sBody;

			$oMessage->Attachments->AddFromBinaryBody($sBody, 'invite.ics', 'application/ics', false);
		}

		return $oMessage;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sTo
	 * @param string $sSubject
	 * @param string $sBody
	 * @param string $sMethod
	 * @return WebMailMessage
	 */
	public static function SendAppointmentMessage($oAccount, $sTo, $sSubject, $sBody, $sMethod)
	{
		if (!defined('WM_ROOTPATH'))
		{
			return false;
		}

		require_once WM_ROOTPATH.'common/class_smtp.php';

		$oMessage = self::BuildAppointmentMessage($oAccount, $sTo, $sSubject, $sBody, $sMethod);

		CApi::Plugin()->RunHook('webmail-change-appointment-message-before-send',
			array(&$oMessage, &$oAccount));

		if ($oMessage)
		{
			$oMessage->Flags |= MESSAGEFLAGS_Seen;
			$oMessage->OriginalMailMessage = $oMessage->ToMailString(true);
			CApi::Log('IcsProcessAppointmentSendOriginalMailMessage');
			CApi::Log($oMessage->OriginalMailMessage);

			return CSmtp::SendMail($oAccount, $oMessage, null, null);
		}

		return false;
	}

	public static function PopulateVCalendar($oAccount, $oEvent, &$vEvent)
	{
		$oLastModified = new \Sabre\VObject\Property\DateTime('LAST-MODIFIED');
		$oLastModified->setDateTime(new DateTime('now'), \Sabre\VObject\Property\DateTime::UTC);
		$vEvent->{'LAST-MODIFIED'} = $oLastModified;
		
		$vCal =& $vEvent->parent;

		$sEventUID = $oEvent->IdEvent;
		$vEvent->UID = $sEventUID;

		if (!empty($oEvent->StartDate) && !empty($oEvent->StartTime) &&
				!empty($oEvent->EndDate) && !empty($oEvent->EndTime))
		{
			$bAllday = (!empty($oEvent->AllDay)) ? true : false;

			$oDTStart = self::PrepareDateTime(
					$oEvent->StartDate,
					$oEvent->StartTime,
					$oAccount->GetDefaultStrTimeZone(),
					$bAllday
			);
			$oDTEnd = self::PrepareDateTime(
					$oEvent->EndDate,
					$oEvent->EndTime,
					$oAccount->GetDefaultStrTimeZone(),
					$bAllday,
					true
			);

			if (isset($oDTStart))
			{
				unset($vEvent->DTSTART);
				$dtstart = new \Sabre\VObject\Property\DateTime('DTSTART');
				if ($bAllday)
				{
					$dtstart->setDateTime($oDTStart, \Sabre\VObject\Property\DateTime::DATE);
				}
				else
				{
					$dtstart->setDateTime($oDTStart, \Sabre\VObject\Property\DateTime::UTC);
				}
				$vEvent->add($dtstart);
			}
			if (isset($oDTEnd))
			{
				unset($vEvent->DTEND);
				$dtend = new \Sabre\VObject\Property\DateTime('DTEND');
				if ($bAllday)
				{
					$dtend->setDateTime($oDTEnd, \Sabre\VObject\Property\DateTime::DATE);
				}
				else
				{
					$dtend->setDateTime($oDTEnd, \Sabre\VObject\Property\DateTime::UTC);
				}
				$vEvent->add($dtend);
			}
		}

		if (isset($oEvent->Name))
		{
			$vEvent->SUMMARY = $oEvent->Name;
		}
		if (isset($oEvent->Description))
		{
			$vEvent->DESCRIPTION = $oEvent->Description;
		}
		if (isset($oEvent->Location))
		{
			$vEvent->LOCATION = $oEvent->Location;
		}

		if ($oEvent->IsRepeat)
		{
			$sRRULE = '';
			if ($vEvent->RRULE && null == $oEvent->RRule)
			{
				$Repeat = CalendarParser::GetRRule($oAccount, $vCal, $sEventUID);
				$sRRULE = $Repeat->ToString();
			}
			else 
			{
				$sRRULE = $oEvent->RRule->ToString();
			}
			if (trim($sRRULE) != '')
			{
				$oRRule = new \Sabre\VObject\Property('RRULE');
				$oRRule->value = $sRRULE;
				$vEvent->RRULE = $oRRule;
			}
		}
		else
		{
			unset($vEvent->RRULE);
		}

		if (isset($oEvent->Name))
		{
			unset($vEvent->VALARM);
			if (!empty($oEvent->ReminderOffset))
			{
				foreach ($oEvent->ReminderOffset as $sOffset)
				{
					$vAlarm = new \Sabre\VObject\Component('VALARM');
					$vAlarm->TRIGGER = $sOffset;
					$vAlarm->DESCRIPTION = 'Alarm';
					$vAlarm->ACTION = 'DISPLAY';

					$vEvent->add($vAlarm);
				}
			}
		}
		
		// Appointments
		$ApiCollaborationManager = CApi::Manager('collaboration');
		if ($ApiCollaborationManager &&
				$ApiCollaborationManager->IsCalendarAppointmentsSupported())
		{
			$aAppointmentsToDelete = array();
			if (isset($oEvent->AppointmentsToDelete))
			{
				$aAppointmentsToDelete = $oEvent->AppointmentsToDelete;
			}
			$aAppointmentsToSave = array();
			if (isset($oEvent->AppointmentsToSave))
			{
				$aAppointmentsToSave = $oEvent->AppointmentsToSave;
			}
			$aEmails = array();
			if ($vEvent->ATTENDEE)
			{
				$aAttendees = $vEvent->ATTENDEE;

				if (count($aAttendees) <= count($aAppointmentsToDelete))
				{
					unset($vEvent->ORGANIZER);
				}

				unset($vEvent->ATTENDEE);
				foreach($aAttendees as $oAttendee)
				{
					$sEmail = str_replace('mailto:', '', strtolower($oAttendee->value));
					if (!in_array($sEmail, $aAppointmentsToDelete))
					{
						$tmpAttendee = new \Sabre\VObject\Property('ATTENDEE');
						$tmpAttendee->value = $oAttendee->value;
						$vEvent->add($tmpAttendee);
						$aEmails[] = $sEmail;
					}
					else
					{
						$PARTSTAT = $oAttendee->offsetGet('PARTSTAT');
						if (!isset($PARTSTAT) ||
								(isset($PARTSTAT) && $PARTSTAT->value != 'DECLINED'))
						{
							$sMethod = 'CANCEL';
							$vCal->METHOD = $sMethod;
							$sBody = $vCal->serialize();
							$sSubject = $vEvent->SUMMARY->value . ': Canceled';
							unset($vCal->METHOD);
							self::SendAppointmentMessage($oAccount, $sEmail, $sSubject,
									$sBody, $sMethod);
						}
					}
				}
			}
			if (count($aAppointmentsToSave) > 0)
			{
				$vEvent->ORGANIZER = 'mailto:'.$oAccount->Email;
				foreach($aAppointmentsToSave as $sAppointmentSave)
				{
					if (!in_array($sAppointmentSave, $aEmails))
					{
						$oAttendee = new \Sabre\VObject\Property('ATTENDEE');
						$oAttendee->value = 'mailto:'.$sAppointmentSave;

						$vEvent->add($oAttendee);
					}
				}
			}
			$aAttendees = $vEvent->ATTENDEE;
			if (isset($aAttendees))
			{
				foreach($aAttendees as $oAttendee)
				{
					$sEmail = str_replace('mailto:', '', strtolower($oAttendee->value));
					$PARTSTAT = $oAttendee->offsetGet('PARTSTAT');
					if (!isset($PARTSTAT) || (isset($PARTSTAT) && $PARTSTAT->value != 'DECLINED'))
					{
						$oAttendee->offsetSet('PARTSTAT', 'NEEDS-ACTION');
						$oAttendee->offsetSet('RSVP', 'TRUE');

						$sMethod = 'REQUEST';
						$vCal->METHOD = $sMethod;
						$sBody = $vCal->serialize();
						unset($vCal->METHOD);
						self::SendAppointmentMessage($oAccount, $sEmail, $vEvent->SUMMARY->value,
								$sBody, $sMethod);
					}
				}
			}
		}
	}

}