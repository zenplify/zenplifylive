<?php

/**
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @property mixed $IdRecurrence;
 * @property mixed $IdRepeat
 * @property string $StartTime;
 * @property bool $Deleted;
 *
 * @package Calendar
 * @subpackage Classes
 */
class CExclusion
{
	public $IdRecurrence;
	public $IdRepeat;
	public $StartTime;
    public $Deleted;

	public function __construct()
	{
		$this->IdRecurrence = null;
		$this->IdRepeat   = null;
		$this->StartTime  = null;
		$this->Deleted    = null;
	}
}
