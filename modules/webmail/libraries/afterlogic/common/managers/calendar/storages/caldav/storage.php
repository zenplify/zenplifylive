<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Calendar
 */
class CApiCalendarCaldavStorage extends CApiCalendarStorage
{
	/**
	 * @var api_Settings
	 */
	protected $Settings;

	/**
	 * @var DAVClient $Dav
	 */
	protected $Dav;

	/**
	 * @var string
	 */
	public $CalendarHomeSet;

	/**
	 * @var string
	 */
	public $Principal;

	/**
	 * @var string
	 */
	protected $Url;

	/**
	 * @var bool
	 */
	protected $Connected;

	/**
	 * @var string
	 */
	protected $User;

	/**
	 * @var string
	 */
	public $TimeZone;

	/**
	 * @var string
	 */
	protected $DbPrefix;

	/**
	 * @var PDO
	 */
	protected $Pdo;

	/*
	 * @var CAccount
	 */
	protected $Account;

	/*
	 * @var array
	 */
	protected $CacheUserCalendars;

	/*
	 * @var array
	 */
	protected $CacheSharedCalendars;

	/**
	 * @var CApiDavManager
	 */
	protected $ApiDavManager;

	/**
	 * @param CApiGlobalManager $oManager
	 */
	public function __construct(CApiGlobalManager &$oManager)
	{
		parent::__construct('caldav', $oManager);

		CApi::Inc('common.dav.client');

		$this->Settings = CApi::GetSettings();
		$this->Dav = null;
		$this->User = null;
		$this->CalendarHomeSet = null;
		$this->Principal = '';
		$this->Url = '';
		$this->Connected = false;
		$this->DbPrefix = $this->Settings->GetConf('Common/DBPrefix');
		$this->Pdo = CApi::GetPDO();
		$this->Account = null;

		$this->ApiDavManager = CApi::Manager('dav');

		$this->CacheUserCalendars = array();
		$this->CacheSharedCalendars = array();
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function Init($oAccount)
	{
		if ($oAccount != null && $this->Dav == null && ($this->User != $oAccount->Email ||
			$this->Account->Email != $oAccount->Email))
		{
			$this->Account = $oAccount;
			$this->User = $oAccount->Email;
			$this->TimeZone = $oAccount->GetDefaultStrTimeZone();

			$this->Url = $this->ApiDavManager ? $this->ApiDavManager->GetServerUrl($oAccount) : '';

			$this->Dav = new DAVClient($this->Url, $this->User, $oAccount->IncomingMailPassword);

			if ($this->Dav->Connect())
			{
				$this->Connected = true;
				$this->Principal = $this->Dav->GetCurrentPrincipal();
				$this->CalendarHomeSet = $this->Dav->GetCalendarHomeSet($this->Principal);
			}
		}
	}

	public function IsConnected()
	{
		return $this->Connected;
	}

	/**
	 * @param string $sEmail
	 * @param string $sPassword
	 */
	public function InitByEmail($sEmail, $sPassword='******')
	{
		$oAccount = new CAccount(new CDomain());
		$oAccount->Email = $sEmail;
		$oAccount->IncomingMailPassword = $sPassword;

		$this->Init($oAccount);
	}

	/**
	 * @param CalendarInfo  $oCalendar
	 */
	public function InitCalendar(&$oCalendar)
	{
		$oCalendar->Url = $oCalendar->Id;
		$oCalendar->Shared = true;
		$sRelativeUsername = '';

		$sMainPrincipal = $this->GetMainPrincipalUrl($oCalendar->Principals[0]);

		$sRelativeUsername = basename(urldecode($sMainPrincipal));

		$username = '';
		$rubbish = '';
		list($username, $rubbish) = explode(DAV_EMAIL_DEV, $this->Dav->GetUser());
		if ($sRelativeUsername != '')
		{
			list($sRelativeUsername, $domain) = explode(DAV_EMAIL_DEV, $sRelativeUsername);
		}

		$matches = array();
		if ((strcmp($sRelativeUsername, $username) != 0) &&
				preg_match('/(.+)-([A-Za-z0-9]{8})/', $sRelativeUsername, $matches))
		{
			$sRelativeUsername = $matches[1];
		}

		if(strcmp($sRelativeUsername, $username) == 0)
		{
			$oCalendar->Shared = false;
			$oCalendar->Owner = $this->Dav->GetUser();
		}
		else
		{
			if($sRelativeUsername != '' && $domain != '')
			{
				$oCalendar->Owner = $sRelativeUsername . '@' . $domain;
			}
			else
			{
				$oCalendar->Owner = $this->Dav->GetUser();
			}
		}
	}

	public function GetCalendarAccess($oAccount, $sCalendarId)
	{
		return 0; //FULL_CONTOL
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function GetCalendar($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$aFolders = array();
		if (count($this->CacheUserCalendars) > 0 && isset($this->CacheUserCalendars[$sCalendarId]))
		{
			$aFolders[$sCalendarId] = $this->CacheUserCalendars[$sCalendarId];
		}
		else if (count($this->CacheSharedCalendars) > 0 && isset($this->CacheSharedCalendars[$sCalendarId]))
		{
			$aFolders[$sCalendarId] = $this->CacheSharedCalendars[$sCalendarId];
		}
		else
		{
			$aFolders = $this->Dav->GetCalendars($sCalendarId);
		}

		if (count($aFolders) > 0)
		{
			reset($aFolders);
			$result = current($aFolders);

			if (count($result->Principals) > 0)
			{
				$result->Principals = array(
					$result->Principals[0].'/calendar-proxy-read',
					$result->Principals[0].'/calendar-proxy-write'
				);
			}
			$result->Shared = true;

			$aCalendarUsers = array();
			if ($this->Dav->isCustomServer)
			{
				$this->InitCalendar($result);
				$aCalendarUsers = $this->GetCalendarUsers($oAccount, $result);
			}

			$sPubLevel = null;
			$sPubHash = null;
			$shares = array();

			foreach ($aCalendarUsers as $aCalendarUser)
			{
				if ($aCalendarUser['email'] == $this->GetPublicUser())
				{
					$sPubLevel = '1';
					$sPubHash = $this->GetPublicCalendarHash($oAccount, $sCalendarId);
				}
				else
				{
					$shares[] = array(
						'id_share' => $aCalendarUser['email'],
						'email_to_user' => $aCalendarUser['email'],
						'access_level' => (int) $aCalendarUser['access_level']
					);
				}
			}
			$result->Shares = $shares;
			$result->PubLevel = $sPubLevel;
			$result->PubHash = $sPubHash;

			return $result;
		}
		return null;
	}

	public function GetPublicUser()
	{
		if (!$this->Pdo)
		{
			throw new CApiBaseException(Errs::Db_PdoExceptionError);
		}
		$principalBackend = new \afterlogic\DAV\Principal\Backend\PDO($this->Pdo, $this->DbPrefix);
		return $principalBackend->getOrCreatePublicPrincipal();
	}

	/*
	 * @param string $sHash
	 */
	public function GetPublicCalendarByHash($sHash)
	{
		$this->InitByEmail($this->GetPublicUser());
		if ($this->Dav->isCustomServer && $this->ApiDavManager)
		{
			$sHash = CCalendarHelper::decryptId($sHash);
			$sPubCalendar = $this->Dav->GetRootUrl($this->ApiDavManager->GetServerUrl()) .
					'delegation/'.$sHash.'/calendar/';
			return $this->GetPublicCalendar($sPubCalendar);
		}
		return false;
	}

	/*
	 * @param string $sCalendarId
	 */
	public function GetPublicCalendarHash($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);
		if ($this->Dav->isCustomServer)
		{
			if (!$this->Pdo)
			{
				throw new CApiBaseException(Errs::Db_PdoExceptionError);
			}
			$delegatesPdo = new \afterlogic\DAV_Delegates\Backend\PDO($this->Pdo, $this->DbPrefix);
			$iCalendarId = $delegatesPdo->getCalendarForUser($oAccount->Email, $sCalendarId);
			if ($iCalendarId !== false)
			{
				return CCalendarHelper::encryptId($iCalendarId);
			}
			else return false;
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetCalendars($oAccount)
	{
		$this->Init($oAccount);

		$aResult = array();
		if (count($this->CacheUserCalendars) > 0)
		{
			$aResult = $this->CacheUserCalendars;
		}
		else
		{
			$aCalendars = $this->Dav->GetCalendars($this->CalendarHomeSet);
			foreach($aCalendars as $oCalendar)
			{
				$this->InitCalendar($oCalendar);
				$aResult[] = $oCalendar;
			}

			$this->CacheUserCalendars = $aResult;
		}
		return $aResult;
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetCalendarsShared($oAccount)
	{
		$this->Init($oAccount);

		$aResult = array();
		if ($this->Dav->isCustomServer)
		{
			$aProxes = $this->Dav->GetCalendarProxiedFor($this->Principal);
			foreach ($aProxes as $aProxy)
			{
				$aCalendars = array();
				if (count($this->CacheSharedCalendars) > 0)
				{
					$aResult = $this->CacheSharedCalendars;
				}
				else
				{
					$aCalendars = $this->Dav->GetCalendars($aProxy['href']);
					$this->CacheSharedCalendars = $aResult;
				}

				foreach ($aCalendars as $oCalendar)
				{
					$this->InitCalendar($oCalendar);
					$oCalendar->SharingLevel = ($aProxy['mode'] == 'write' ? 1 : 2);
					$aResult[] = $oCalendar;
				}
			}
		}
		return $aResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function CreateCalendar($oAccount, $sName, $sDescription, $iOrder, $sColor)
	{
		$this->Init($oAccount);

		$sSystemName = \Sabre\DAV\UUIDUtil::getUUID();
		$sCalendarHomeSet = $this->CalendarHomeSet;

		$res = $this->Dav->CreateCalendar($sSystemName, $sName, $sDescription, $iOrder, $sColor,
				$sCalendarHomeSet);
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return $sCalendarHomeSet.$sSystemName;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function UpdateCalendar($oAccount, $sCalendarId, $sName, $sDescription, $iOrder,
			$sColor)
	{
		$this->Init($oAccount);

		$res = $this->Dav->UpdateCalendar($sCalendarId, $sName, $sDescription, $iOrder, $sColor);
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sColor
	 */
	public function UpdateCalendarColor($oAccount, $sCalendarId, $sColor)
	{
		$this->Init($oAccount);

		$res = $this->Dav->UpdateCalendarColor($sCalendarId, $sColor);
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return true;
	}

	/**
	 * @param string $sCalendarId
	 * @param int $iVisible
	 */
	public function UpdateCalendarVisible($sCalendarId, $iVisible)
	{
		@setcookie($sCalendarId, $iVisible, time() + 86400);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function DeleteCalendar($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$res = $this->Dav->DeleteItem($sCalendarId);
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}

		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @return bool
	 */
	public function ClearAllCalendars($oAccount)
	{
		$this->Init($oAccount);

		$aCalendars = $this->Dav->GetCalendars($this->CalendarHomeSet);
		foreach ($aCalendars as $oCalendar)
		{
			if ($oCalendar)
			{
				$this->Dav->DeleteItem($oCalendar->Id);
			}
		}

		$this->Server->GetDelegatesBackend()->DeleteAllUsersShares($oAccount->Email);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sUserId
	 * @param int $iPerms
	 */
	public function UpdateCalendarShare($oAccount, $sCalendarId, $sUserId, $iPerms = ECalendarPermission::RemovePermission)
	{
		$this->Init($oAccount);

		if ($this->Dav->isCustomServer)
		{
			$oCalendar = $this->GetCalendar($oAccount, $sCalendarId);

			if ($oCalendar)
			{
				if (count($oCalendar->Principals) > 0)
				{
					if (!$this->Pdo)
					{
						throw new CApiBaseException(Errs::Db_PdoExceptionError);
					}
					$shareBackend = new \afterlogic\DAV\Delegates\Backend\PDO($this->Pdo, $this->DbPrefix);
					$calendarUri = $shareBackend->UpdateShare($sCalendarId, $oAccount->Email, $sUserId, $iPerms);
					if ($calendarUri)
					{
						$this->DeleteReminders($calendarUri);
						$this->CreateReminders($sUserId, $calendarUri);
					}
				}
			}
			return true;
		}
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function PublicCalendar($oAccount, $sCalendarId)
	{
		return $this->UpdateCalendarShare($oAccount, $sCalendarId, $this->GetPublicUser(), ECalendarPermission::Read);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function UnPublicCalendar($oAccount, $sCalendarId)
	{
		return $this->UpdateCalendarShare($oAccount, $sCalendarId, $this->GetPublicUser());
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $oCalendar
	 */
	public function GetCalendarUsers($oAccount, $oCalendar)
	{
		$this->Init($oAccount);

		if ($oCalendar != null)
		{
			if (count($oCalendar->Principals) > 0)
			{
				$principal = str_replace('/calendar-proxy-write', '', $oCalendar->Principals[0]);
				$principal = str_replace('/calendar-proxy-read', '', $principal);
				$principalUri = 'principals/' . basename($principal);
				$calendarUri = basename($oCalendar->Id);

				if (!$this->Pdo)
				{
					throw new CApiBaseException(Errs::Db_PdoExceptionError);
				}
				$delegatesPdo = new \afterlogic\DAV\Delegates\Backend\PDO($this->Pdo, $this->DbPrefix);
				$res = $delegatesPdo->getCalendarUsers($principalUri, $calendarUri) ;

				$result = array();
				foreach($res as $row)
				{
					$result[] = array(
						'id_share' => basename($row['uri']),
						'email' => basename($row['uri']),
						'access_level' => $row['mode']
					);
				}

				return $result;
			}
		}
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetEvents($oAccount, $sCalendarId, $dStart, $dFinish)
	{
		$this->Init($oAccount);

		$aResult = array();

		$events = $this->Dav->GetEvents($sCalendarId, $dStart, $dFinish);
		if ($events !== false && is_array($events))
		{
			foreach($events as $event)
			{
				if (isset($event['data']))
				{
					$aResult[] = \Sabre\VObject\Reader::read($event['data']);
				}
			}
		}
		return $aResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function GetEvent($oAccount, $sCalendarId, $sEventId)
	{
		$this->Init($oAccount);

		$event = $this->Dav->GetEventByUid($sCalendarId, $sEventId);
		if ($event !== false && is_array($event))
		{
			$event['vcal'] = \Sabre\VObject\Reader::read($event['data']);
			return $event;
		}
		return false;
	}

	/**
	}
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param \Sabre\VObject\Component_VCalendar $vCal
	 */
	public function CreateEvent($oAccount, $sCalendarId, $sEventId, $vCal)
	{
		$this->Init($oAccount);

		$res = $this->Dav->CreateItem($sCalendarId.'/'.$sEventId.'.ics', $vCal->serialize());

		if (is_array($res))
		{
			if ($res['statusCode'] > 400)
			{
				CApi::Log('DAV: CreateEvent error result:');
				CApi::LogObject($res);
			}
			else
			{
				return $sEventId;
			}
		}
		else
		{
			CApi::Log('DAV: CreateEvent error false result');
		}

		return null;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param string $sData
	 */
	public function UpdateEventData($oAccount, $sCalendarId, $sEventId, $sData)
	{
		$this->Init($oAccount);

		$iStatusCode = 401;
		$mResult = false;

		$aData = $this->GetEvent($oAccount, $sCalendarId, $sEventId);
		if ($aData !== false)
		{
			$mResult = $this->Dav->UpdateItem($sCalendarId.'/'.$aData['href'], $sData);
			$iStatusCode = $mResult['statusCode'];
		}
		else
		{
			$mResult = $this->Dav->CreateItem($sCalendarId.'/'.$sEventId.'.ics', $sData);
			$iStatusCode = $mResult['statusCode'];
		}

		if ($iStatusCode > 400)
		{
			CApi::LogObject($mResult);
			return false;
		}
		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param array $vCal
	 */
	public function UpdateEvent($oAccount, $sCalendarId, $sEventId, $vCal)
	{
		$this->Init($oAccount);

		$res = $this->Dav->UpdateItem($sCalendarId.'/'.$sEventId.'.ics', $vCal->serialize());
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return true;
	}

	public function MoveEvent($oAccount, $sCalendarId, $sNewCalendarId, $sEventId, $sData)
	{
		$this->Init($oAccount);

		$res = $this->Dav->MoveItem($sCalendarId.'/'.$sEventId, $sNewCalendarId.'/'.$sEventId.'.ics');
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function DeleteEvent($oAccount, $sCalendarId, $sEventId)
	{
		$this->Init($oAccount);

		$res = $this->Dav->DeleteItem($sCalendarId.'/'.$sEventId);
		if ($res['statusCode'] > 400)
		{
			CApi::LogObject($res);
			return false;
		}
		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetAlarms($oAccount, $sCalendarId, $dStart, $dFinish)
	{
		$this->Init($oAccount);

		$result = array();
		$result['data'] = array();
		$result['events'] = array();
		$result['exclusions'] = array();
		$result['reminders'] = array();
		$result['appointments'] = array();

		$events = $this->Dav->GetEvents($sCalendarId, $dStart, $dFinish);

		if ($events !== false && is_array($events))
		{
			foreach($events as $event)
			{
				if (isset($event['data']))
				{
					$oCalendar = $this->GetCalendar($oAccount, $sCalendarId);
					$vCal = \Sabre\VObject\Reader::read($event['data']);
					$Data = $this->ParseIcal($oCalendar, $vCal, true);

					$result['data'] = array_merge($result['data'], $Data['data']);
					$result['events'] = array_merge($result['events'], $Data['events']);
					$result['exclusions'] = array_merge($result['exclusions'], $Data['exclusions']);
					$result['reminders'] = array_merge($result['reminders'], $Data['reminders']);
					$result['appointments'] = array_merge($result['appointments'], $Data['appointments']);
				}
			}
		}
		else
		{
			$result = array();
		}
		return $result;
	}

	public function GetReminders($start, $end)
	{
		if (!$this->Pdo)
		{
			throw new CApiBaseException(Errs::Db_PdoExceptionError);
		}
		$remindersBackend = new \afterlogic\DAV\Reminders\Backend\PDO($this->Pdo, $this->DbPrefix);
		return $remindersBackend->getReminders($start, $end);
	}

	public function AddReminder($oAccount, $calendarUri, $eventId, $time = null, $starttime = null)
	{
		if (!$this->Pdo)
		{
			throw new CApiBaseException(Errs::Db_PdoExceptionError);
		}
		$remindersBackend = new \afterlogic\DAV\Reminders\Backend\PDO($this->Pdo, $this->DbPrefix);
		return $remindersBackend->addReminder($oAccount->Email, $calendarUri, $eventId, $time, $starttime);
	}

	public function DeleteReminder($oAccount, $eventId)
	{
		if (!$this->Pdo)
		{
			throw new CApiBaseException(Errs::Db_PdoExceptionError);
		}
		$remindersBackend = new \afterlogic\DAV\Reminders\Backend\PDO($this->Pdo, $this->DbPrefix);
		return $remindersBackend->deleteReminder($eventId);
	}

	public function DeleteReminderByCalendar($oAccount, $calendarUri)
	{
		if (!$this->Pdo)
		{
			throw new CApiBaseException(Errs::Db_PdoExceptionError);
		}
		$remindersBackend = new \afterlogic\DAV\Reminders\Backend\PDO($this->Pdo, $this->DbPrefix);
		return $remindersBackend->deleteReminderByCalendar($calendarUri);
	}

	public function GetMainPrincipalUrl($principal)
	{
		$principal = str_replace('/calendar-proxy-read', '', rtrim($principal, '/'));
		$principal = str_replace('/calendar-proxy-write', '', $principal);
		$principal = rtrim($principal, '/user');
		return $principal;
	}
}