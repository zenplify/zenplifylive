<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Calendar
 */
class CApiCalendarSabredavStorage extends CApiCalendarStorage
{
	/**
	 * @var api_Settings
	 */
	protected $Settings;

	/**
	 * @var string
	 */
	public $CalendarHomeSet;

	/**
	 * @var string
	 */
	public $Principal;

	/**
	 * @var array
	 */
	public $principalInfo;

	/**
	 * @var bool
	 */
	protected $Connected;

	/**
	 * @var string
	 */
	protected $User;

	/**
	 * @var string
	 */
	public $TimeZone;

	/**
	 * @var string
	 */
	protected $DbPrefix;

	/*
	 * @var CAccount
	 */
	public $Account;

	/*
	 * @var array
	 */
	protected $CacheUserCalendars;

	/*
	 * @var array
	 */
	protected $CacheSharedCalendars;

	/**
	 * @var \afterlogic\DAV\Server
	 */
	protected $Server;

	/**
	 * @param CApiGlobalManager $oManager
	 */
	public function __construct(CApiGlobalManager &$oManager)
	{
		parent::__construct('sabredav', $oManager);

		CApi::Inc('common.dav.client');

		$this->Server = new \afterlogic\DAV\Server();
		$this->Settings = \CApi::GetSettings();
		$this->User = null;
		$this->Principal = '';
		$this->principalInfo = array();

		$this->Connected = false;
		$this->DbPrefix = $this->Settings->GetConf('Common/DBPrefix');
		$this->Account = null;

		$this->CacheUserCalendars = array();
		$this->CacheSharedCalendars = array();
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function Init($oAccount)
	{
		if ($oAccount != null && ($this->User != $oAccount->Email ||
			$this->Account->Email != $oAccount->Email))
		{
			$this->Account = $oAccount;
			$this->User = $oAccount->Email;
			$this->TimeZone = $oAccount->GetDefaultStrTimeZone();

			if ($oAccount)
			{
				$oPdo = \CApi::GetPDO();
				$oHelper = new \afterlogic\DAV\Auth\Backend\Helper($oPdo, $this->DbPrefix);
				$oHelper->CheckPrincipals($oAccount->Email);

				$this->Connected = true;

	            $oPrincipal = null;
				$oPrincipalCollection = $this->Server->tree->getNodeForPath('principals');
				if ($oPrincipalCollection->childExists($this->User))
				{
					$oPrincipal = $oPrincipalCollection->getChild($this->User);
				}
				if (isset($oPrincipal))
				{
					$aProperties = $oPrincipal->getProperties(array('uri'));
					$this->principalInfo = $aProperties;
					$this->Principal = $aProperties['uri'];
				}
				$this->CalendarHomeSet = '';

			}
		}
	}

	public function IsConnected()
	{
		return $this->Connected;
	}

	public function GetCalendarAccess($oAccount, $sCalendarId)
	{
		$mResult = ECalendarPermission::Read;
		$oCalendarInfo = $this->GetCalendar($oAccount, $sCalendarId);
		if ($oCalendarInfo)
		{
			if ($oCalendarInfo->Shared)
			{
				$mResult = $oCalendarInfo->SharingLevel;
			}
			else
			{
				$mResult = ECalendarPermission::Full;
			}

		}
		return $mResult;
	}

	protected function GetCalDAVCalendar($sCalendarId)
	{
		$oCalendar = false;

		$sCalendarIdBase = basename($sCalendarId);
		if ($sCalendarIdBase == 'calendar')
		{
			$sCalendarId = basename($sCalendarIdBase);
		}
		else
		{
			$sCalendarId = $sCalendarIdBase;
		}

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		if ($oCalendars->childExists($sCalendarId))
		{
			$oCalendar = $oCalendars->getChild($sCalendarId);
		}
		else
		{
			$oCalendars = new \afterlogic\DAV\Delegates\DelegatedCalendars($this->Server->GetPrincipalBackend(),
					$this->Server->GetCaldavBackend(), $this->Server->GetDelegatesBackend(), $this->Principal);
			if ($oCalendars->childExists($sCalendarId))
			{
				$oCalendar = $oCalendars->getChild($sCalendarId);
			}
		}
		return $oCalendar;
	}

	public function GetCalendarHome($oCalendar, $aProps)
	{
		$sResult = '';
		if ($oCalendar instanceof \afterlogic\DAV\Delegates\Calendar)
		{
			$sResult = $aProps['id'];
		}
		else if ($oCalendar instanceof \Sabre\CalDAV\Calendar)
		{
			$sResult = $aProps['uri'];
		}

		return $sResult;
	}

	public function GetCalendarInfo($oCalendar)
	{
		if (!($oCalendar instanceof \Sabre\CalDAV\Calendar) &&
			!($oCalendar instanceof \afterlogic\DAV\Delegates\Calendar))
		{
			return false;
		}
		$aProps = $oCalendar->getProperties(
				array(
					'id',
					'uri',
					'principaluri',
					'{DAV:}displayname',
					'{'.\Sabre\CalDAV\Plugin::NS_CALENDARSERVER.'}getctag',
					'{'.\Sabre\CalDAV\Plugin::NS_CALDAV.'}calendar-description',
					'{http://apple.com/ns/ical/}calendar-color',
					'{http://apple.com/ns/ical/}calendar-order'
				)
		);

		$sCalendarHome = $this->GetCalendarHome($oCalendar, $aProps);
		$oCalendarInfo = new CCalendar($sCalendarHome);

		if ($oCalendar instanceof \afterlogic\DAV\Delegates\Calendar)
		{
			$oCalendarInfo->Shared = true;
			$oCalendarInfo->SharingLevel = $oCalendar->GetMode();
		}

		$oCalendarInfo->DisplayName = $aProps['{DAV:}displayname'];
		if (isset($aProps['{'.\Sabre\CalDAV\Plugin::NS_CALENDARSERVER.'}getctag']))
		{
			$oCalendarInfo->CTag = $aProps['{'.\Sabre\CalDAV\Plugin::NS_CALENDARSERVER.'}getctag'];
		}
		if (isset($aProps['{'.\Sabre\CalDAV\Plugin::NS_CALDAV.'}calendar-description']))
		{
			$oCalendarInfo->Description = $aProps['{'.\Sabre\CalDAV\Plugin::NS_CALDAV.'}calendar-description'];
		}
		if (isset($aProps['{http://apple.com/ns/ical/}calendar-color']))
		{
			$oCalendarInfo->Color = $aProps['{http://apple.com/ns/ical/}calendar-color'];
		}
		if (isset($aProps['{http://apple.com/ns/ical/}calendar-order']))
		{
			$oCalendarInfo->Order = $aProps['{http://apple.com/ns/ical/}calendar-order'];
		}
		$oCalendarInfo->Principals = array($aProps['principaluri']);
		$sUri = $aProps['uri'];

		$sRelativeUser = '';
		$sMainPrincipal = $this->GetMainPrincipalUrl($oCalendarInfo->Principals[0]);
		$sRelativeUser = basename(urldecode($sMainPrincipal));

		list($sUser, $sDomain) = explode(DAV_EMAIL_DEV, $this->User);
		if ($sRelativeUser != '')
		{
			list($sRelativeUser, $sRelativeDomain) = explode(DAV_EMAIL_DEV, $sRelativeUser);
		}

		if ((strcmp($sRelativeUser, $sUser) != 0) &&
				preg_match('/(.+)-([A-Za-z0-9]{8})/', $sRelativeUser, $matches))
		{
			$sRelativeUser = $matches[1];
		}

		if(strcmp($sRelativeUser, $sUser) == 0)
		{
			$oCalendarInfo->Shared = false;
			$oCalendarInfo->Owner = $this->User;
			$oCalendarInfo->Url = '/calendars/'.$this->User.'/'.$oCalendarInfo->Id;
		}
		else
		{
			$oCalendarInfo->Shared = true;
			$oCalendarInfo->Url = '/delegation/'.$oCalendarInfo->Id.'/calendar';
			if($sRelativeUser != '' && $sRelativeDomain != '')
			{
				$oCalendarInfo->Owner = $sRelativeUser . DAV_EMAIL_DEV . $sRelativeDomain;
			}
			else
			{
				$oCalendarInfo->Owner = $this->User;
			}
		}
		
		$oCalendarInfo->RealUrl = 'calendars/'.$oCalendarInfo->Owner.'/'.$sUri;

		return $oCalendarInfo;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function GetCalendar($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$oCalendar = null;
		$oCalendarInfo = false;
		if (count($this->CacheUserCalendars) > 0 && isset($this->CacheUserCalendars[$sCalendarId]))
		{
			$oCalendarInfo = $this->CacheUserCalendars[$sCalendarId];
		}
		else if (count($this->CacheSharedCalendars) > 0 && isset($this->CacheSharedCalendars[$sCalendarId]))
		{
			$oCalendarInfo = $this->CacheSharedCalendars[$sCalendarId];
		}
		else
		{
			$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
			if ($oCalendar)
			{
				$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
			}
		}
		if ($oCalendarInfo)
		{
			$aCalendarUsers = $this->GetCalendarUsers($oAccount, $oCalendarInfo);

			$sPubLevel = null;
			$sPubHash = null;
			$aShares = array();

			foreach ($aCalendarUsers as $aCalendarUser)
			{
				if ($aCalendarUser['email'] == $this->GetPublicUser())
				{
					$sPubLevel = '1';
					$sPubHash = $this->GetPublicCalendarHash($oAccount, $sCalendarId);
				}
				else
				{
					$aShares[] = array(
						'id_share' => $aCalendarUser['email'],
						'email_to_user' => $aCalendarUser['email'],
						'access_level' => (int) $aCalendarUser['access_level']
					);
				}
			}
			$oCalendarInfo->Shares = $aShares;
			$oCalendarInfo->PubLevel = $sPubLevel;
			$oCalendarInfo->PubHash = $sPubHash;
		}
		return $oCalendarInfo;
	}

	public function GetPublicUser()
	{
		return $this->Server->GetPrincipalBackend()->getOrCreatePublicPrincipal();
	}

	/*
	 * @param string $sCalendarId
	 */
	public function GetPublicCalendarHash($oAccount, $sCalendarId)
	{
		$mResult = false;
		$this->Init($oAccount);

		$iCalendarId = $this->Server->GetDelegatesBackend()->getCalendarForUser($oAccount->Email, $sCalendarId);
		if ($iCalendarId !== false)
		{
			$mResult = CCalendarHelper::encryptId($iCalendarId);
		}
		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetCalendars($oAccount)
	{
		$this->Init($oAccount);

		$aCalendars = array();
		if (count($this->CacheUserCalendars) > 0)
		{
			$aCalendars = $this->CacheUserCalendars;
		}
		else
		{
			$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);

			foreach ($oCalendars->getChildren() as $oCalendar)
			{
				$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
				if ($oCalendarInfo)
				{
					$aCalendars[$oCalendarInfo->Id] = $oCalendarInfo;
				}
			}

			$this->CacheUserCalendars = $aCalendars;
		}
 		return $aCalendars;
	}

	/**
	 * @param CAccount $oAccount
	 */
	public function GetCalendarsShared($oAccount)
	{
		$this->Init($oAccount);

		$aCalendars = array();
		if (count($this->CacheSharedCalendars) > 0)
		{
			$aCalendars = $this->CacheSharedCalendars;
		}
		else
		{
			$oCalendars = new \afterlogic\DAV\Delegates\DelegatedCalendars($this->Server->GetPrincipalBackend(),
					$this->Server->GetCaldavBackend(), $this->Server->GetDelegatesBackend(), $this->Principal);

			foreach ($oCalendars->getChildren() as $oCalendar)
			{
				$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
				if($oCalendarInfo)
				{
					$aCalendars[$oCalendarInfo->Id] = $oCalendarInfo;
				}
			}
			$this->CacheSharedCalendars = $aCalendars;
		}

		return $aCalendars;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function CreateCalendar($oAccount, $sName, $sDescription, $iOrder, $sColor)
	{
		$this->Init($oAccount);

		$sSystemName = \Sabre\DAV\UUIDUtil::getUUID();

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
        $aResourceType = array(
			'{DAV:}collection',
			'{urn:ietf:params:xml:ns:caldav}calendar'
		);
		$aProperties = array(
			'{DAV:}displayname' => $sName,
			'{'.\Sabre\CalDAV\Plugin::NS_CALENDARSERVER.'}getctag' => 1,
			'{'.\Sabre\CalDAV\Plugin::NS_CALDAV.'}calendar-description' => $sDescription,
			'{http://apple.com/ns/ical/}calendar-color' => $sColor,
			'{http://apple.com/ns/ical/}calendar-order' => $iOrder
		);
		$oCalendars->createExtendedCollection($sSystemName, $aResourceType, $aProperties);
		return $this->CalendarHomeSet.$sSystemName;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sName
	 * @param string $sDescription
	 * @param int $iOrder
	 * @param string $sColor
	 */
	public function UpdateCalendar($oAccount, $sCalendarId, $sName, $sDescription, $iOrder,
			$sColor)
	{
		$this->Init($oAccount);

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		if ($oCalendars->childExists($sCalendarId))
		{
			$oCalendar = $oCalendars->getChild($sCalendarId);
			if ($oCalendar)
			{
				$aProperties = array(
					'{DAV:}displayname' => $sName,
					'{'.\Sabre\CalDAV\Plugin::NS_CALDAV.'}calendar-description' => $sDescription,
					'{http://apple.com/ns/ical/}calendar-color' => $sColor,
					'{http://apple.com/ns/ical/}calendar-order' => $iOrder
				);
				$oCalendar->updateProperties($aProperties);
				return true;
			}
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sColor
	 */
	public function UpdateCalendarColor($oAccount, $sCalendarId, $sColor)
	{
		$this->Init($oAccount);

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		if ($oCalendars->childExists($sCalendarId))
		{
			$oCalendar = $oCalendars->getChild($sCalendarId);
			if ($oCalendar)
			{
				$aProperties = array(
					'{http://apple.com/ns/ical/}calendar-color' => $sColor,
				);
				$oCalendar->updateProperties($aProperties);
				return true;
			}
		}
		return false;
	}

	/**
	 * @param string $sCalendarId
	 * @param int $iVisible
	 */
	public function UpdateCalendarVisible($sCalendarId, $iVisible)
	{
		@setcookie($sCalendarId, $iVisible, time() + 86400);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function DeleteCalendar($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		if ($oCalendars->childExists($sCalendarId))
		{
			$oCalendar = $oCalendars->getChild($sCalendarId);
			if ($oCalendar)
			{
				$oCalendar->delete();

				$this->DeleteReminderByCalendar($oCalendar);

				return true;
			}
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 * @return bool
	 */
	public function ClearAllCalendars($oAccount)
	{
		$this->Init($oAccount);

		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		foreach ($oCalendars->getChildren() as $oCalendar)
		{
			if ($oCalendar instanceof \Sabre\CalDAV\Calendar)
			{
				$oCalendar->delete();
			}
		}
//		$this->Server->GetCacheBackend()->deleteRemindersCacheByUser($oAccount->Email);
		$this->Server->GetDelegatesBackend()->DeleteAllUsersShares($oAccount->Email);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 *
	 * @return bool
	 */
	public function UnsubscibeCalendar($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalendar($oAccount, $sCalendarId);

		if ($oCalendar)
		{
			if (count($oCalendar->Principals) > 0)
			{
				$this->Server->GetDelegatesBackend()->UnsubscribeCalendar($sCalendarId, $oAccount->Email);
			}
		}

		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sUserId
	 * @param int $iPerm
	 *
	 * @return bool
	 */
	public function UpdateCalendarShare($oAccount, $sCalendarId, $sUserId, $iPerms = ECalendarPermission::RemovePermission)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalendar($oAccount, $sCalendarId);

		if ($oCalendar)
		{
			if (count($oCalendar->Principals) > 0)
			{
				$calendarUri = $this->Server->GetDelegatesBackend()->UpdateShare($sCalendarId, $oAccount->Email, $sUserId, $iPerms);
			}
		}

		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function PublicCalendar($oAccount, $sCalendarId)
	{
		return $this->UpdateCalendarShare($oAccount, $sCalendarId, $this->GetPublicUser(), ECalendarPermission::Read);
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 */
	public function UnPublicCalendar($oAccount, $sCalendarId)
	{
		return $this->UpdateCalendarShare($oAccount, $sCalendarId, $this->GetPublicUser());
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $oCalendar
	 */
	public function GetCalendarUsers($oAccount, $oCalendar)
	{
		$aResult = array();
		$this->Init($oAccount);

		if ($oCalendar != null)
		{
			if (count($oCalendar->Principals) > 0)
			{
				$principal = str_replace('/calendar-proxy-write', '', $oCalendar->Principals[0]);
				$principal = str_replace('/calendar-proxy-read', '', $principal);
				$principalUri = 'principals/' . basename($principal);
				$calendarUri = basename($oCalendar->Id);

				$res = $this->Server->GetDelegatesBackend()->getCalendarUsers($principalUri, $calendarUri) ;

				foreach($res as $row)
				{
					$aResult[] = array(
						'id_share' => basename($row['uri']),
						'email' => basename($row['uri']),
						'access_level' => $row['mode']
					);
				}
			}
		}
		return $aResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @return string | bool
	 */
	public function ExportCalendarToIcs($oAccount, $sCalendarId)
	{
		$this->Init($oAccount);

		$mResult = false;
		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
		if ($oCalendar)
		{
			$calendar = new \Sabre\VObject\Component('vcalendar');
			$calendar->version = '2.0';
			if (\Sabre\DAV\Server::$exposeVersion) {
				$calendar->prodid = '-//SabreDAV//SabreDAV ' . \Sabre\DAV\Version::VERSION . '//EN';
			} else {
				$calendar->prodid = '-//SabreDAV//SabreDAV//EN';
			}
			$calendar->calscale = 'GREGORIAN';

			$collectedTimezones = array();

			$timezones = array();
			$objects = array();

			foreach ($oCalendar->getChildren() as $oChild)
			{
				$nodeComp = \Sabre\VObject\Reader::read($oChild->get());
				foreach($nodeComp->children() as $child)
				{
					switch($child->name)
					{
						case 'VEVENT' :
						case 'VTODO' :
						case 'VJOURNAL' :
							$objects[] = $child;
							break;

						// VTIMEZONE is special, because we need to filter out the duplicates
						case 'VTIMEZONE' :
							// Naively just checking tzid.
							if (in_array((string)$child->TZID, $collectedTimezones)) continue;

							$timezones[] = $child;
							$collectedTimezones[] = $child->TZID;
							break;

					}
				}
			}
			foreach($timezones as $tz) $calendar->add($tz);
			foreach($objects as $obj) $calendar->add($obj);

			$mResult = $calendar->serialize();
		}

		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetEvents($oAccount, $sCalendarId, $dStart, $dFinish)
	{
		$this->Init($oAccount);

		$mResult = false;
		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);

		if ($oCalendar)
		{
			$mResult = array();

			foreach ($oCalendar->getChildren() as $oCalendarChild)
			{
				if ($oCalendarChild instanceof \Sabre\CalDAV\CalendarObject)
				{
					$sData = $oCalendarChild->get();
					$vCal = \Sabre\VObject\Reader::read($sData);
					$vCalCopy = clone $vCal;
					unset($vCalCopy->VEVENT);
					foreach ($vCal->VEVENT as $vEvent)
					{
						if (isset($vEvent->DTSTART, $vEvent->DTEND) &&
							$vEvent->isInTimeRange(new DateTime($dStart), new DateTime($dFinish)))
						{
							$vCalCopy->add($vEvent);
						}
					}
					$mResult[] = $vCalCopy;
				}
			}
		}

		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function GetEvent($oAccount, $sCalendarId, $sEventId)
	{
		$this->Init($oAccount);

		$mResult = false;
		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);

		if ($oCalendar)
		{
			foreach ($oCalendar->getChildren() as $oCalendarChild)
			{
				if ($oCalendarChild instanceof \Sabre\CalDAV\CalendarObject)
				{
					$sData = $oCalendarChild->get();
					$vCal = \Sabre\VObject\Reader::read($sData);
					foreach ($vCal->VEVENT as $vEvent)
					{
						$aUids = $vEvent->select('UID');
						foreach($aUids as $oUid)
						{
							if ($oUid->value == $sEventId)
							{
								$mResult['url'] = $oCalendarChild->getName();
								$mResult['href'] = $oCalendarChild->getName();
								$mResult['data'] = $sData;
								$mResult['vcal'] = $vCal;

								return $mResult;
							}
						}
					}
				}
			}
		}
		return $mResult;
	}

	/**
	}
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param \Sabre\VObject\Component_VCalendar $vCal
	 */
	public function CreateEvent($oAccount, $sCalendarId, $sEventId, $vCal)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
		if ($oCalendar)
		{
			$sData = $vCal->serialize();
			$oCalendar->createFile($sEventId.'.ics', $sData);

			$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
			$this->UpdateReminder($oCalendarInfo->Owner, $oCalendarInfo->RealUrl, $sEventId, $sData);

			return $sEventId;
		}

		return null;
	}


	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param string $sData
	 */
	public function UpdateEventData($oAccount, $sCalendarId, $sEventId, $sData)
	{
		$this->Init($oAccount);

		$aData = $this->GetEvent($oAccount, $sCalendarId, $sEventId);
		$oCalendars = new \Sabre\CalDAV\UserCalendars($this->Server->GetCaldavBackend(), $this->principalInfo);
		$oCalendar = $oCalendars->getChild($sCalendarId);
		if ($oCalendar)
		{
			$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
			if ($aData !== false)
			{
				$oCalendarChild = $oCalendar->getChild($aData['href']);
				if ($oCalendarChild)
				{
					$oCalendarChild->put($sData);
					return true;
				}
			}
			else
			{
				$oCalendar->createFile($sEventId.'.ics', $sData);

				$this->UpdateReminder($oCalendarInfo->Owner, $oCalendarInfo->RealUrl, $sEventId, $sData);

				return true;
			}
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 * @param array $vCal
	 */
	public function UpdateEvent($oAccount, $sCalendarId, $sEventId, $vCal)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
		if ($oCalendar)
		{
			$oCalendarChild = $oCalendar->getChild($sEventId . '.ics');
			$sData = $vCal->serialize();
			$oCalendarChild->put($sData);

			$oCalendarInfo = $this->GetCalendarInfo($oCalendar);
			$this->UpdateReminder($oCalendarInfo->Owner, $oCalendarInfo->RealUrl, $sEventId, $sData);

			return true;
		}
		return false;
	}

	public function MoveEvent($oAccount, $sCalendarId, $sNewCalendarId, $sEventId, $sData)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
		if ($oCalendar)
		{
			$oCalendarChild = $oCalendar->getChild($sEventId . '.ics');

			$oNewCalendar = $this->GetCalDAVCalendar($sNewCalendarId);
			if ($oNewCalendar)
			{
				$oNewCalendarInfo = $this->GetCalendarInfo($oNewCalendar);
				$oNewCalendar->createFile($sEventId . '.ics', $sData);
				$oCalendarChild->delete();

				$this->DeleteReminder($sEventId);
				$this->UpdateReminder($oNewCalendarInfo->Owner, $oNewCalendarInfo->RealUrl, $sEventId, $sData);
		
				return true;
			}
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $sEventId
	 */
	public function DeleteEvent($oAccount, $sCalendarId, $sEventId)
	{
		$this->Init($oAccount);

		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);
		if ($oCalendar)
		{
			$oCalendarChild = $oCalendar->getChild($sEventId.'.ics');
			$oCalendarChild->delete();

			$this->DeleteReminder($sEventId);

			return true;
		}
		return false;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sCalendarId
	 * @param string $dStart
	 * @param string $dFinish
	 */
	public function GetAlarms($oAccount, $sCalendarId, $dStart, $dFinish)
	{
		$this->Init($oAccount);

		$mResult = false;
		$oCalendar = $this->GetCalDAVCalendar($sCalendarId);

		if ($oCalendar)
		{
			$mResult = array();

			foreach ($oCalendar->getChildren() as $oCalendarChild)
			{
				if ($oCalendarChild instanceof \Sabre\CalDAV\CalendarObject)
				{
					$sData = $oCalendarChild->get();
					$vCal = \Sabre\VObject\Reader::read($sData);
					$vCalCopy = clone $vCal;
					unset($vCalCopy->VEVENT);
					foreach ($vCal->VEVENT as $vEvent)
					{
						if (isset($vEvent->VALARM, $vEvent->DTSTART, $vEvent->DTEND) &&
							$vEvent->isInTimeRange(new DateTime($dStart), new DateTime($dFinish)))
						{
							$vCalCopy->add($vEvent);
						}
					}
					$mResult[] = $vCalCopy;
				}
			}
		}

		return $mResult;
	}

	public function GetMainPrincipalUrl($principal)
	{
		$principal = str_replace('/calendar-proxy-read', '', rtrim($principal, '/'));
		$principal = str_replace('/calendar-proxy-write', '', $principal);
		$principal = rtrim($principal, '/user');
		return $principal;
	}
	
	public function GetReminders($start, $end)
	{
		$bResult = false;
		$pluginReminders = $this->Server->getPlugin('reminders');
		if (isset($pluginReminders))
		{
			$bResult = $pluginReminders->getReminders($start, $end);
		}
		
		return $bResult;
	}

	public function AddReminder($sEmail, $calendarUri, $eventId, $time = null, $starttime = null)
	{
		$bResult = false;
		$pluginReminders = $this->Server->getPlugin('reminders');
		if (isset($pluginReminders))
		{
			$bResult = $pluginReminders->addReminder($sEmail, $calendarUri, $eventId, $time, $starttime);
		}
		
		return $bResult;
	}
	

	public function UpdateReminder($sEmail, $sCalendarUri, $sEventId, $sData)
	{
		$pluginReminders = $this->Server->getPlugin('reminders');
		if (isset($pluginReminders))
		{
			trim($sCalendarUri, '/');
			$pluginReminders->updateReminder($sCalendarUri . '/' . $sEventId . '.ics', $sData, $sEmail);
		}
	}

	public function DeleteReminder($sEventId)
	{
		$pluginReminders = $this->Server->getPlugin('reminders');
		$pluginReminders->deleteReminder($sEventId);
	}

	public function DeleteReminderByCalendar($sCalendarUri)
	{
		$pluginReminders = $this->Server->getPlugin('reminders');
		$pluginReminders->deleteReminderByCalendar($sCalendarUri);
	}
}
