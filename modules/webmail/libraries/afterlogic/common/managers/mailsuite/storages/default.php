<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Mailsuite
 */
class CApiMailsuiteStorage extends AApiManagerStorage
{
	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct($sStorageName, CApiGlobalManager &$oManager)
	{
		parent::__construct('mailsuite', $sStorageName, $oManager);
	}

	/**
	 * @param int $iMailingListId
	 * @return CMailingList
	 */
	public function GetMailingListById($iMailingListId)
	{
		return null;
	}

	/**
	 * @param CMailingList $oMailingList
	 * @return bool
	 */
	public function CreateMailingList(CMailingList &$oMailingList)
	{
		return false;
	}

	/**
	 * @param CMailingList $oMailingList
	 * @return bool
	 */
	public function UpdateMailingList(CMailingList $oMailingList)
	{
		return false;
	}

	/**
	 * @param int $iMailingListId
	 * @return bool
	 */
	public function DeleteMailingListById($iMailingListId)
	{
		return false;
	}

	/**
	 * @param CMailingList $oMailingList
	 * @return bool
	 */
	public function DeleteMailingList(CMailingList $oMailingList)
	{
		return false;
	}

	/**
	 * @param CMailAliases $oMailAliases
	 */
	public function InitMailAliases(CMailAliases &$oMailAliases)
	{
	}

	/**
	 * @param CMailAliases $oMailAliases
	 */
	public function UpdateMailAliases(CMailAliases $oMailAliases)
	{
		return false;
	}

	/**
	 * @param CMailForwards $oMailForwards
	 */
	public function InitMailForwards(CMailForwards &$oMailForwards)
	{
	}

	/**
	 * @param CMailForwards $oMailForwards
	 */
	public function UpdateMailForwards(CMailForwards $oMailForwards)
	{
		return false;
	}
}
