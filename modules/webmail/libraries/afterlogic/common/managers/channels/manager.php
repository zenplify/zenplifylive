<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Channels
 */
class CApiChannelsManager extends AApiManagerWithStorage
{
	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct(CApiGlobalManager &$oManager, $sForcedStorage = '')
	{
		parent::__construct('channels', $oManager, $sForcedStorage);

		$this->inc('classes.channel');
	}

	/**
	 * @return CChannel
	 */
	public function NewChannel()
	{
		return new CChannel();
	}

	/**
	 * @param int $iPage
	 * @param int $iChannelsPerPage
	 * @param string $sOrderBy = 'Login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return array | false [Id => [Login, Description]]
	 */
	public function GetChannelList($iPage, $iChannelsPerPage, $sOrderBy = 'Login', $bOrderType = true, $sSearchDesc = '')
	{
		$aResult = false;
		try
		{
			$aResult = $this->oStorage->GetChannelList($iPage, $iChannelsPerPage, $sOrderBy, $bOrderType, $sSearchDesc);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $aResult;
	}

	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return int | false
	 */
	public function GetChannelCount($sSearchDesc = '')
	{
		$iResult = false;
		try
		{
			$iResult = $this->oStorage->GetChannelCount($sSearchDesc);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $iResult;
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return CChannel
	 */
	public function GetChannelById($iChannelId)
	{
		$oChannel = null;
		try
		{
			$oChannel = $this->oStorage->GetChannelById($iChannelId);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $oChannel;
	}

	/**
	 * @param string $sChannelLogin
	 *
	 * @return int
	 */
	public function GetChannelIdByLogin($sChannelLogin)
	{
		$iChannelId = 0;
		try
		{
			$iChannelId = $this->oStorage->GetChannelIdByLogin($sChannelLogin);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $iChannelId;
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return bool
	 */
	public function ChannelExists(CChannel $oChannel)
	{
		$bResult = false;
		try
		{
			$bResult = $this->oStorage->ChannelExists($oChannel);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}
		return $bResult;
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return bool
	 */
	public function CreateChannel(CChannel &$oChannel)
	{
		$bResult = false;
		try
		{
			if ($oChannel->Validate())
			{
				if (!$this->ChannelExists($oChannel))
				{
					$oChannel->Password = md5($oChannel->Login.mt_rand(1000, 9000).microtime(true));
					if (!$this->oStorage->CreateChannel($oChannel))
					{
						throw new CApiManagerException(Errs::ChannelsManager_ChannelCreateFailed);
					}
				}
				else
				{
					throw new CApiManagerException(Errs::ChannelsManager_ChannelAlreadyExists);
				}
			}

			$bResult = true;
		}
		catch (CApiBaseException $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}

		return $bResult;
	}

	/**
	 * @param CChannel $oChannel
	 */
	public function UpdateChannel(CChannel $oChannel)
	{
		$bResult = false;
		try
		{
			if ($oChannel->Validate())
			{
				if (!$this->ChannelExists($oChannel))
				{
					if (!$this->oStorage->UpdateChannel($oChannel))
					{
						throw new CApiManagerException(Errs::ChannelsManager_ChannelUpdateFailed);
					}
				}
				else
				{
					throw new CApiManagerException(Errs::ChannelsManager_ChannelDoesNotExist);
				}
			}

			$bResult = true;
		}
		catch (CApiBaseException $oException)
		{
			$bResult = false;
			$this->setLastException($oException);
		}
		return $bResult;
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return bool
	 */
	public function DeleteChannel(CChannel $oChannel)
	{
		$bResult = false;
		try
		{
			/* @var $oRealmsApi CApiRealmsManager */
			$oRealmsApi = CApi::Manager('realms');
			if (!$oRealmsApi->DeleteRealmsByChannelId($oChannel->IdChannel, true))
			{
				$oException = $oRealmsApi->GetLastException();
				if ($oException)
				{
					throw $oException;
				}
			}

			$bResult = $this->oStorage->DeleteChannel($oChannel->IdChannel);
		}
		catch (CApiBaseException $oException)
		{
			$this->setLastException($oException);
		}

		return $bResult;
	}
}