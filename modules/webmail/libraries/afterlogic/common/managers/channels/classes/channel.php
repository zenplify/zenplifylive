<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @property int $IdChannel
 * @property string $Login
 * @property string $Password
 * @property string $Description
 *
 * @package Channels
 * @subpackage Classes
 */
class CChannel extends api_AContainer
{
	public function __construct()
	{
		parent::__construct(get_class($this), 'IdChannel');

		$this->__USE_TRIM_IN_STRINGS__ = true;

		$this->SetDefaults(array(
			'IdChannel'		=> 0,
			'Login'			=> '',
			'Password'		=> '',
			'Description'	=> ''
		));

		$this->SetLower(array('Login'));
	}

	/**
	 * @return bool
	 */
	public function Validate()
	{
		switch (true)
		{
			case !api_Validate::IsValidChannelLogin($this->Login):
				throw new CApiValidationException(Errs::Validation_InvalidRealmName);
			case api_Validate::IsEmpty($this->Login):
				throw new CApiValidationException(Errs::Validation_FieldIsEmpty, null, array(
					'{{ClassName}}' => 'CChannel', '{{ClassField}}' => 'Login'));
		}

		return true;
	}

	/**
	 * @return array
	 */
	public function GetMap()
	{
		return self::GetStaticMap();
	}

	/**
	 * @return array
	 */
	public static function GetStaticMap()
	{
		return array(
			'IdChannel'		=> array('int', 'id_channel', false, false),
			'Login'			=> array('string(255)', 'login', true, false),
			'Password'		=> array('string(100)', 'password', true, false),
			'Description'	=> array('string(255)', 'description')
		);
	}
}
