<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Channels
 */
class CApiChannelsDbStorage extends CApiChannelsStorage
{
	/**
	 * @var CDbStorage $oConnection
	 */
	protected $oConnection;

	/**
	 * @var CApiChannelsCommandCreatorMySQL
	 */
	protected $oCommandCreator;

	/**
	 * @param CApiGlobalManager &$oManager
	 */
	public function __construct(CApiGlobalManager &$oManager)
	{
		parent::__construct('db', $oManager);

		$this->oConnection =& $oManager->GetConnection();
		$this->oCommandCreator =& $oManager->GetCommandCreator(
			$this, array(EDbType::MySQL => 'CApiChannelsCommandCreatorMySQL')
		);
	}


	/**
	 * @param int $iPage
	 * @param int $iChannelsPerPage
	 * @param string $sOrderBy = 'login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return array | false [Id => [Login, Description]]
	 */
	public function GetChannelList($iPage, $iChannelsPerPage, $sOrderBy = 'Login', $bOrderType = true, $sSearchDesc = '')
	{
		$aChannels = false;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetChannelList($iPage, $iChannelsPerPage,
				$this->dbOrderBy($sOrderBy), $bOrderType, $sSearchDesc)))
		{
			$oRow = null;
			$aChannels = array();
			while (false !== ($oRow = $this->oConnection->GetNextRecord()))
			{
				$aChannels[$oRow->id_channel] = array($oRow->login, $oRow->description);
			}
		}

		$this->throwDbExceptionIfExist();
		return $aChannels;
	}

	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return int | false
	 */
	public function GetChannelCount($sSearchDesc = '')
	{
		$iResultCount = false;
		if ($this->oConnection->Execute($this->oCommandCreator->GetChannelCount($sSearchDesc)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$iResultCount = (int) $oRow->channels_count;
			}
		}

		$this->throwDbExceptionIfExist();
		return $iResultCount;
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return CChannel | null
	 */
	public function GetChannelById($iChannelId)
	{
		$oChannel = null;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetChannelById($iChannelId)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$oChannel = new CChannel();
				$oChannel->InitByDbRow($oRow);
			}
		}

		$this->throwDbExceptionIfExist();
		return $oChannel;
	}

	/**
	 * @param string $sChannelLogin
	 *
	 * @return int
	 */
	public function GetChannelIdByLogin($sChannelLogin)
	{
		$iChannelId = 0;
		if ($this->oConnection->Execute(
			$this->oCommandCreator->GetChannelIdByLogin($sChannelLogin)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow)
			{
				$iChannelId = $oRow->id_channel;
			}
		}

		$this->throwDbExceptionIfExist();
		return $iChannelId;
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return bool
	 */
	public function ChannelExists(CChannel $oChannel)
	{
		$bResult = false;
		$niExceptRealmId = (0 < $oChannel->IdChannel) ? $oChannel->IdChannel : null;

		if ($this->oConnection->Execute(
			$this->oCommandCreator->ChannelExists($oChannel->Login, $niExceptRealmId)))
		{
			$oRow = $this->oConnection->GetNextRecord();
			if ($oRow && 0 < (int) $oRow->channels_count)
			{
				$bResult = true;
			}
		}

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param CChannel $oChannel
	 */
	public function CreateChannel(CChannel &$oChannel)
	{
		$bResult = false;
		if ($this->oConnection->Execute($this->oCommandCreator->CreateChannel($oChannel)))
		{
			$bResult = true;
			$oChannel->IdChannel = $this->oConnection->GetLastInsertId();
		}

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param CChannel $oChannel
	 */
	public function UpdateChannel(CChannel $oChannel)
	{
		$bResult = $this->oConnection->Execute($this->oCommandCreator->UpdateChannel($oChannel));

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return bool
	 */
	public function DeleteChannel($iChannelId)
	{
		return $this->DeleteChannels(array($iChannelId));
	}

	/**
	 * @param array $aChannelsIds
	 *
	 * @return bool
	 */
	public function DeleteChannels(array $aChannelsIds)
	{
		$bResult = $this->oConnection->Execute(
			$this->oCommandCreator->DeleteChannels($aChannelsIds));

		$this->throwDbExceptionIfExist();
		return $bResult;
	}

	/**
	 * @param string $sOrderBy
	 * @return string
	 */
	protected function dbOrderBy($sOrderBy)
	{
		$sResult = $sOrderBy;
		switch ($sOrderBy)
		{
			case 'Description':
				$sResult = 'description';
				break;
			case 'Login':
				$sResult = 'login';
				break;
		}
		return $sResult;
	}
}