<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in LICENSE.txt
 *
 */

/**
 * @package Channels
 */
class CApiChannelsCommandCreator extends api_CommandCreator
{
	/**
	 * @param string $sSearchDesc = ''
	 *
	 * @return string
	 */
	public function GetChannelCount($sSearchDesc = '')
	{
		$sWhere = '';
		if (!empty($sSearchDesc))
		{
			$sWhere = ' WHERE login LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%').
				' OR description LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%');;
		}

		$sSql = 'SELECT COUNT(id_channel) as channels_count FROM %sawm_channels%s';

		return sprintf($sSql, $this->Prefix(), $sWhere);
	}

	/**
	 * @param string $sLogin
	 *
	 * @return string
	 */
	public function GetChannelIdByLogin($sLogin)
	{
		$sSql = 'SELECT id_channel FROM %sawm_channels WHERE login = %s';
		return sprintf($sSql, $this->Prefix(), $this->escapeString($sLogin));
	}

	/**
	 * @param string $sWhere
	 *
	 * @return string
	 */
	protected function getChannelByWhere($sWhere)
	{
		return api_AContainer::DbGetObjectSqlString(
			$sWhere, $this->Prefix().'awm_channels', CChannel::GetStaticMap(), $this->oHelper);
	}

	/**
	 * @param int $iChannelId
	 *
	 * @return string
	 */
	public function GetChannelById($iChannelId)
	{
		return $this->getChannelByWhere(sprintf('%s = %d',
			$this->escapeColumn('id_channel'), $iChannelId));
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return string
	 */
	function CreateChannel(CChannel $oChannel)
	{
		return api_AContainer::DbCreateObjectSqlString($this->Prefix().'awm_channels', $oChannel, $this->oHelper);
	}

	/**
	 * @param CChannel $oChannel
	 *
	 * @return string
	 */
	function UpdateChannel(CChannel $oChannel)
	{
		$aResult = api_AContainer::DbUpdateArray($oChannel, $this->oHelper);

		$sSql = 'UPDATE %sawm_channels SET %s WHERE id_channel = %d';
		return sprintf($sSql, $this->Prefix(), implode(', ', $aResult), $oChannel->IdChannel);
	}

	/**
	 * @param string $sLogin
	 * @param int $niExceptRealmId = null
	 *
	 * @return string
	 */
	public function ChannelExists($sLogin, $niExceptRealmId = null)
	{
		$sAddWhere = (is_integer($niExceptRealmId)) ? ' AND id_channel <> '.$niExceptRealmId : '';

		$sSql = 'SELECT COUNT(id_channel) as channels_count FROM %sawm_channels WHERE login = %s%s';

		return sprintf($sSql, $this->Prefix(), $this->escapeString(strtolower($sLogin)), $sAddWhere);
	}

	/**
	 * @param array $aChannelsIds
	 *
	 * @return string
	 */
	function DeleteChannels($aChannelsIds)
	{
		$aIds = api_Utils::SetTypeArrayValue($aChannelsIds, 'int');

		$sSql = 'DELETE FROM %sawm_channels WHERE id_channel in (%s)';
		return sprintf($sSql, $this->Prefix(), implode(',', $aIds));
	}
}

/**
 * @package Channels
 */
class CApiChannelsCommandCreatorMySQL extends CApiChannelsCommandCreator
{
	/**
	 * @param int $iPage
	 * @param int $iChannelsPerPage
	 * @param string $sOrderBy = 'login'
	 * @param bool $bOrderType = true
	 * @param string $sSearchDesc = ''
	 *
	 * @return string
	 */
	public function GetChannelList($iPage, $iChannelsPerPage, $sOrderBy = 'login', $bOrderType = true, $sSearchDesc = '')
	{
		$sWhere = '';
		if (!empty($sSearchDesc))
		{
			$sWhere = ' WHERE login LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%').
				' OR description LIKE '.$this->escapeString('%'.strtolower($sSearchDesc).'%');
		}

		$sOrderBy = empty($sOrderBy) ? 'login' : $sOrderBy;

		$sSql = 'SELECT id_channel, login, description FROM %sawm_channels %s ORDER BY %s %s LIMIT %d, %d';

		$sSql = sprintf($sSql, $this->Prefix(), $sWhere, $sOrderBy,
			((bool) $bOrderType) ? 'ASC' : 'DESC', ($iPage > 0) ? ($iPage - 1) * $iChannelsPerPage : 0,
			$iChannelsPerPage);

		return $sSql;
	}
}
