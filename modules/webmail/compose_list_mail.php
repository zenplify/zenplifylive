<?php
	session_start();
	$_SESSION['selectedGroup'] = '';
	error_reporting(0);
	include_once("../../classes/init.php");
	include_once("../../classes/messages.php");
	include_once("encryption_class.php");
	$userId = $_SESSION['userId'];
	$encryption = new encryption_class();
	$messages = new messages();
	$database = new database();
	$email = $messages->GetEmail($database, $userId);
	$domains = $messages->getDomains($database);
	$userAccount = $messages->getUserEmailAccount($database, $email);
	include_once("../headerFooter/header.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html>
	<head>
		<meta http-equiv="Pragma" content="cache"/>
		<meta http-equiv="Cache-Control" content="public"/>
		<title>User Messages</title>
		<script>
			$(document).ready(function () {
			});
			function showDomain() {
				document.getElementById('dom').style.display = 'block';
				var email = $('#email').val();
				var domain = email.split('@');

				$('#domain').val(domain[1]);
				document.getElementById('note').style.display = 'none';
			}
			function hideDomain() {
				var value = $('#domains').val();
				if (value != 'yahoo.com')
					document.getElementById('note').style.display = 'none';
				document.getElementById('dom').style.display = 'none';
				$('#domain').val(value);


			}
			function showNote() {
				document.getElementById('note').style.display = 'inline';
				document.getElementById('dom').style.display = 'none';
			}

		</script>
	</head>
	<body>
	<div class="container">
		<div class="top_content">
			<a href="list_mailer.php"><input type="button" value="" class="listMailer"/></a>
			<a href="../contact/permission_manager.php"><input type="button" id="permission_manager2" value=""></a>

			<br/>

			<div style="float:left; width:100%; margin-top:4px; margin-bottom:3px"><font class="messageofsuccess" id="messageofsuccess"></font></div>
		</div>
		<div class="sub_container">


			<?php
				session_start();

				// Store credentials in session
				$_SESSION['email'] = $userAccount->mail_inc_login;
				$key = 'abc*def';
				$decrypt_pass = $encryption->decrypt($key, $userAccount->enc_pass);
				$errors = $encryption->errors;
				$_SESSION['password'] = $decrypt_pass;
			?>
			<iframe src="listmail-frame.php" style="width: 990px; height: 700px; border:none;"></iframe>
			<?php ?>

		</div>
	<?php
		include_once("../headerFooter/footer.php");
	?></body>
</html>