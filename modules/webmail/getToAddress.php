<?php 
session_start();
error_reporting(1);
include_once("../../classes/init.php");
require_once('../../classes/messages.php');
require_once('../../classes/contact.php');
$database=new database();
$messages=new messages();
$contact=new contact();
$userId=$_SESSION['userId'];
$groups=implode(',',$_POST['groupId']);
$interests=implode(',',$_POST['interestId']);

$interests = str_replace('50','Host',$interests);
$interests = str_replace('51','Client',$interests);
$interests = str_replace('52','Independent Consultant',$interests);

$groupData=$contact->GetGroupContacts($userId,$groups);
$interestData = $contact->GetlevelContacts($userId,$interests);


if((!empty($groups)) || (!empty($interests)))
{
	$i=0;
	if(!empty($groups)){
		foreach ($groupData as $rowGroup)
		{
			$status=$messages->getContactRequestStatus($database,$rowGroup->contactId);
			
			if($status=='Subscribed')
			{
				$email=$messages->showContactEmailAddress($database,$rowGroup->contactId);
				
				$emails[$i]=$email;
				$i++;
			}}
	}

	if(!empty($interests)){
		foreach ($interestData as $rowInterest)
		{
			$status=$messages->getContactRequestStatus($database,$rowInterest->contactId);
			
			if($status=='Subscribed')
			{
				$email=$messages->showContactEmailAddress($database,$rowInterest->contactId);
				if(!in_array($email,$emails)){
					$emails[$i]=$email;
					$i++;
				}
			}}
	}
	
	if(!empty($emails)){
		$emailIds=implode(',',$emails);	}
	else{
		$emailIds='';}
	
	echo $emailIds;
	$_SESSION['toemails']=$emailIds;
}
else
{
	echo '';
}
?>