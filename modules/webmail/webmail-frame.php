<?php

// Example of logging into WebMail account using email and password for incorporating into another web application
 
// determining main directory
session_start();
defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'\\'));
 
// utilizing WebMail Pro API
//include_once WM_ROOTPATH.'libraries\afterlogic\api.php';
//echo WM_ROOTPATH.'libraries\afterlogic\api.php';
include_once 'libraries/afterlogic/api.php';
if (class_exists('CApi') && CApi::IsValid())
{
  // data for logging into account
  $sEmail = $_SESSION['email'];
  $sPassword = $_SESSION['password'];
 
  // Getting required API class
  $oApiWebMailManager = CApi::Manager('webmail');
 
  // attempting to obtain object for account we're trying to log into
  $oAccount = $oApiWebMailManager->LoginToAccount($sEmail, $sPassword);
  if ($oAccount)
  {
    // populating session data from the account
    $oAccount->FillSession();
 
    // redirecting to WebMail
    $oApiWebMailManager->JumpToWebMail('../webmail/webmail.php?check=1');
  }
  else
  {
    // login error
    //echo $oApiWebMailManager->GetLastErrorMessage();
	$error=$oApiWebMailManager->GetLastErrorMessage();
	if($error=='The username or password you entered is incorrect')
	{
		echo $error2='<p ><span style="color:red">'.$error.'</span>
		<br />Please <a href="messages2.php" target="_parent">Click here </a>to Update Password</p>';
		//echo $error2;
		
	}
	else
	{
		echo $error;
	}
	
  }
}
else
{
  echo 'WebMail API not allowed';
}
?>