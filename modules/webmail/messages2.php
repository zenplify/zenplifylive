<?php 
session_start();
$_SESSION['selectedGroup']='';
error_reporting(0);
include_once("../../classes/init.php");
include_once("../../classes/messages.php");
include_once("encryption_class.php");
include_once("../../classes/profiles.php");
$userId=$_SESSION['userId'];
$encryption=new encryption_class();
$messages=new messages();
$database= new database();
$email=$messages->GetEmail($database,$userId);
$domains=$messages->getDomains($database);

			include_once("../headerFooter/header.php");
 			$userAccount=$messages->getUserEmailAccount($database,$email);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html>
<head>
<meta http-equiv="Pragma" content="cache" /><meta http-equiv="Cache-Control" content="public" />
<title>User Messages</title>
<script>
$(document).ready(function() {
   document.getElementById('dom').style.display='none';
   document.getElementById('note').style.display='none';
  

});
function showDomain()
{
	document.getElementById('dom').style.display='block';
	var email=$('#email').val();
	var domain=email.split('@');
	
	$('#domain').val(domain[1]);
	document.getElementById('note').style.display='none';
}
function hideDomain()
{
	var value=$('#domains').val();
	if(value!='yahoo.com')
	document.getElementById('note').style.display='none';
	  document.getElementById('dom').style.display='none';
	$('#domain').val(value);
	
	
	
}
function showNote()
{
	document.getElementById('note').style.display='inline';
	document.getElementById('dom').style.display='none';
}
function validate()
{
	if($('#email').val()=='')
	{
		$("#required1").html('Please enter email');
		flag1=false;
	}
	else
	{
		$("#required1").html('');
		flag1=true;
	}
	if($('#password').val()=='')
	{
		$("#required2").html('Please enter password');
		flag2=false;
	}
	else
	{
		$("#required2").html('');
		flag2=true;
	}
	if($('#domains').val()=='')
	{
		$("#required3").html('Please Select Domain');
		flag3=false;
	}
	else if($('#domains').val()=='other')
	{
		$("#required3").html('');
		flag3=true;
		if($('#domain').val()=='')
		{
			$("#required4").html('Please enter Domain name');
			flag4=false;
		}
		else
		{
			$("#required4").html('');
			flag4=true;
		}
		if($('#incoming').val()=='')
		{
			$("#required5").html('Please enter incomig mail server');
			flag5=false;
		}
		else
		{
			$("#required5").html('');
			flag5=true;
		}
		if($('#outgoing').val()=='')
		{
			$("#required6").html('Please enter outgoing mail server');
			flag6=false;
		}
		else
		{
			$("#required6").html('');
			flag6=true;
		}
		if(flag1==true && flag2==true && flag3==true && flag4==true && flag5==true && flag6==true)
		{
			document.login.submit();
		}
		else
		{
			return false;
		}
	}
	else
	{
		if($('#domains').val()!='')
		{
		  $("#required3").html('');
		  flag3=true;
		}
		if(flag1==true && flag2==true && flag3==true)
		{
			document.login.submit();
		}
		else
		{
			return false;
		}
	}
	
}
function showform()
{
	$('#editPassword').css('display','block');
}
</script>
</head>
<body>
<div class="container">
<div class="top_content">
<?php 
 if(!empty($userAccount))
 {
 ?><a href="list_mailer.php"><input type="button" value="" class="listMailer" /></a>
  <?php } else {?>
 
 <input type="button" class="listMailer_disable" value="" >
  <?php } ?>
<a href="../contact/permission_manager.php"><input type="button"  id="permission_manager2" value="" ></a>

<br />
<div style="float:left; width:100%; margin-top:4px; margin-bottom:3px"><font class="messageofsuccess" id="messageofsuccess" ></font></div>  
</div>
 <div class="sub_container">
  
  

 <div id="editPassword">
  <form action="UpdateUserAccount.php" method="post" name="login"   >
<table border="0" cellpadding="0" width="80%">
<tr>
<td class="label" style="width: 21.45%;">E-mail: </td><td><input type="text" class="textfield"  id="email" name="email" value="<?php  echo $email; ?>" readonly="readonly"><label class="errorrequired" id="required1"></label></td></tr>
<tr><td></td><td></td></tr>
<tr>
<td class="label" style="width: 21.45%;">Password: </td>
<td><input type="password" class="textfield" name="password" id="password"><label class="errorrequired" id="required2" /></td>
</tr>

</table>
<table border="0" cellpadding="0" width="80%">
<tr>    
     
<td>&nbsp;</td><td><input type="button" name="submitbtn" id="submitbtn" class="submit" value="" style="margin-left:168px;" onclick="validate();"/></td>
     
    
    </tr>
</table>
</form>
</div>


</div>
<?php 
		include_once("../headerFooter/footer.php");
	?></body>
</html>