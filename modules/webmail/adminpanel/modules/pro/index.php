<?php

//$bDisabled = true;
$iSortIndex = 80;
$sCurrentModule = 'CProModule';
class CProModule extends ap_Module
{
	/**
	* @var CApiDomainsManager
	*/
	protected $oDomainsApi;

	/**
	 * @var CApiWebmailManager
	 */
	protected $oWebmailApi;

	/**
	 * @var bool
	 */
	private $bHasWebmail;

	/**
	 * @var CApiLicensingManager
	 */
	protected $oLicApi;

	/**
	 * @var CApiRealmsManager
	 */
	protected $oRealmsApi;

	/**
	 * @var CApiChannelsManager
	 */
	protected $oChannelsApi;

	/**
	 * @var CApiUsersManager
	 */
	protected $oUsersApi;

	/**
	 * @var CApiCollaborationManager
	 */
	protected $oCollaborationApi;

	/**
	 * @param CAdminPanel $oAdminPanel
	 * @param string $sPath
	 * @return CProModule
	 */
	public function __construct(CAdminPanel &$oAdminPanel, $sPath)
	{
		parent::__construct($oAdminPanel, $sPath);

		$this->oDomainsApi = CApi::Manager('domains');
		$this->oLicApi = CApi::Manager('licensing');
		$this->oRealmsApi = CApi::Manager('realms');
		$this->oChannelsApi = CApi::Manager('channels');
		$this->oUsersApi = CApi::Manager('users');
		$this->oWebmailApi = CApi::Manager('webmail');
		$this->oCollaborationApi = CApi::Manager('collaboration');

		$this->bHasWebmail = false;

		$this->aTabs[] = AP_TAB_SERVICES;
		$this->aTabs[] = AP_TAB_SYSTEM;

		if ($oAdminPanel->RType)
		{
			$this->aTabs[] = AP_TAB_REALMS;
			$this->aTabs[] = AP_TAB_CHANNELS;
		}

		$this->aTabs[] = AP_TAB_DOMAINS;
		$this->aTabs[] = AP_TAB_USERS;

		$this->aQueryActions[] = 'new';
		$this->aQueryActions[] = 'edit';

		$this->oPopulateData = new CProPopulateData($this);
		$this->oStandardPostAction = new CProPostAction($this);
		$this->oStandardPopAction = new CProPopAction($this);
		$this->oTableAjaxAction = new CProAjaxAction($this);

		$aTabs =& $oAdminPanel->GetTabs();
		array_push($aTabs,
			array('Users', AP_TAB_USERS),
			array('Realms', AP_TAB_REALMS),
			array('Channels', AP_TAB_CHANNELS)
		);
	}

	/**
	 * @param CAdminPanel $oAdminPanel
	 */
	public function InitAdminPanel(CAdminPanel &$oAdminPanel)
	{
		$oAdminPanel->PType = true;
		$oAdminPanel->LType = $this->oLicApi->IsValidKey();

		$this->bHasWebmail = $oAdminPanel->IsModuleInit('CWebMailModule');

		if (!$oAdminPanel->LType)
		{
			$oAdminPanel->RemoveTabs(
				array(AP_TAB_SERVICES, AP_TAB_DOMAINS, AP_TAB_USERS, AP_TAB_REALMS, AP_TAB_CHANNELS)
			);
		}

		if (!$oAdminPanel->RType)
		{
			$oAdminPanel->RemoveTabs(
				array(AP_TAB_REALMS, AP_TAB_CHANNELS)
			);
		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initStandardMenuByTab($sTab, ap_Screen &$oScreen)
	{
		switch ($sTab)
		{
			case AP_TAB_SYSTEM:
				$oScreen->AddMenuItem(PRO_MODE_LICESING, PRO_MODE_LICESING_NAME,
					$this->sPath.'/templates/licensing.php',
					array(),
					'db');
				break;

			case AP_TAB_SERVICES:
				if (CApi::IsValidFullSupportPhpVersion())
				{
					$oScreen->AddMenuItem(WM_MODE_DAV, WM_MODE_DAV_NAME,
						$this->sPath.'/templates/dav.php',
						array(),
						WM_MODE_LOGGING);
				}

				break;

		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableTopMenu($sTab, ap_Screen &$oScreen)
	{
		switch ($sTab)
		{
			case AP_TAB_DOMAINS:
				$this->JsAddFile('domains.js');

				$oScreen->AddTopMenuButton('New Domain', 'new-domain.png', 'IdDomainsNewDomainButton');
				$oScreen->AddTopMenuButton('Delete', 'delete.gif', 'IdDomainsDeleteButton');
				break;

			case AP_TAB_REALMS:
				$this->JsAddFile('realms.js');

				$oScreen->AddTopMenuButton('New Realm', 'new_contact.gif', 'IdRealmsNewRealmButton');
				$oScreen->AddTopMenuButton('Delete', 'delete.gif', 'IdRealmsDeleteButton');
				break;

			case AP_TAB_CHANNELS:
				$this->JsAddFile('channels.js');

				$oScreen->AddTopMenuButton('New Channel', 'new_contact.gif', 'IdChannelsNewChannelButton');
				$oScreen->AddTopMenuButton('Delete', 'delete.gif', 'IdChannelsDeleteButton');
				break;

			case AP_TAB_USERS:
				$this->JsAddFile('users.js');

				$oScreen->AddTopMenuButton('New User', 'user_new.png', 'IdUsersNewUserButton');
				$oScreen->AddTopMenuButton('Enable', 'user_enable.png', 'IdUsersEnableUserButton');
				$oScreen->AddTopMenuButton('Disable', 'user_disable.png', 'IdUsersDisableUserButton');
				$oScreen->AddTopMenuButton('Delete', 'delete.gif', 'IdUsersDeleteButton');
				break;
		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableListHeaders($sTab, ap_Screen &$oScreen)
	{
		$oScreen->SetEmptySearch(AP_LANG_RESULTEMPTY);
		switch ($sTab)
		{
			case AP_TAB_REALMS:
				$oScreen->ClearHeaders();
				$oScreen->SetEmptyList(PRO_LANG_NOREALMS);
				$oScreen->SetEmptySearch(PRO_LANG_REALMS_RESULTEMPTY);

				$oScreen->AddHeader('Name', 120);
				$oScreen->AddHeader('Description', 120, true);
				break;

			case AP_TAB_CHANNELS:
				$oScreen->ClearHeaders();
				$oScreen->SetEmptyList(PRO_LANG_NOCHANNELS);
				$oScreen->SetEmptySearch(PRO_LANG_CHANNELS_RESULTEMPTY);

				$oScreen->AddHeader('Name', 120);
				$oScreen->AddHeader('Description', 120, true);
				break;

			case AP_TAB_USERS:
				$oScreen->ClearHeaders();
				$oScreen->AddHeader('Type', 42);
				$oScreen->AddHeader('Email', 150, true);
				$oScreen->AddHeader('Friendly name', 95);
				$oScreen->SetEmptyList(CM_LANG_NOUSERSINDOMAIN);
				$oScreen->SetEmptySearch(CM_LANG_USERS_RESULTEMPTY);
				break;
		}

	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableList($sTab, ap_Screen &$oScreen)
	{
		if (AP_TAB_REALMS === $sTab)
		{
			$sSearch = $oScreen->GetSearchDesc();

			$iCount = $this->oRealmsApi->GetRealmCount($sSearch);
			$oScreen->SetAllListCount($iCount);

			$oScreen->SetLowToolBar('realms: '.(empty($sSearch) ? $iCount : $this->oRealmsApi->GetRealmCount()));

			$sOrderBy = $oScreen->GetOrderBy();
			$sOrderBy = 'Name' === $sOrderBy ? 'Login' : 'Description';

			$aRealmList = $this->oRealmsApi->GetRealmList(
				$oScreen->GetPage(), $oScreen->GetLinesPerPage(),
				$sOrderBy, $oScreen->GetOrderType(), $sSearch);

			if (is_array($aRealmList) && 0 < count($aRealmList))
			{
				foreach ($aRealmList as $iRealmId => $aRealmArray)
				{
					$oScreen->AddListItem($iRealmId, array(
						'Name' => $aRealmArray[0],
						'Description' => $aRealmArray[1]
					));
				}
			}
		}
		else if (AP_TAB_CHANNELS === $sTab)
		{
			$sSearch = $oScreen->GetSearchDesc();

			$iCount = $this->oChannelsApi->GetChannelCount($sSearch);
			$oScreen->SetAllListCount($iCount);

			$oScreen->SetLowToolBar('channels: '.(empty($sSearch) ? $iCount : $this->oChannelsApi->GetChannelCount()));

			$sOrderBy = $oScreen->GetOrderBy();
			$sOrderBy = 'Name' === $sOrderBy ? 'Login' : 'Description';

			$aChannelList = $this->oChannelsApi->GetChannelList(
				$oScreen->GetPage(), $oScreen->GetLinesPerPage(),
				$sOrderBy, $oScreen->GetOrderType(), $sSearch);

			if (is_array($aChannelList) && 0 < count($aChannelList))
			{
				foreach ($aChannelList as $iChannelId => $aChannelArray)
				{
					$oScreen->AddListItem($iChannelId, array(
						'Name' => $aChannelArray[0],
						'Description' => $aChannelArray[1]
					));
				}
			}
		}
		else if (AP_TAB_USERS === $sTab)
		{
			$mDomainIndex = $oScreen->GetFilterIndex();

			if ($this->oAdminPanel->HasAccessDomain($mDomainIndex))
			{
				$sSearchDesc = $oScreen->GetSearchDesc();
				$iSearchCount = $this->oUsersApi->GetUserCount($mDomainIndex, $sSearchDesc);
				$oScreen->SetAllListCount($iSearchCount);

				$oScreen->SetLowToolBar('users: '.((empty($sSearchDesc))
					? $iSearchCount : $this->oUsersApi->GetUserCount($mDomainIndex)));

				$aUsersList = $this->oUsersApi->GetUserList(
					$mDomainIndex, $oScreen->GetPage(), $oScreen->GetLinesPerPage(),
					$oScreen->GetOrderBy(), $oScreen->GetOrderType(), $oScreen->GetSearchDesc());

				if (is_array($aUsersList) && 0 < count($aUsersList))
				{
					foreach ($aUsersList as $iUserId => $aUserArray)
					{
						$oScreen->AddListItem($iUserId, array(
							'Type' => ($aUserArray[0])
								? '<img src="static/images/icons/M.gif">'
								: (($aUserArray[3])
									? '<img src="static/images/icons/U_disable.gif">'
									: '<img src="static/images/icons/U.gif">'
							),
							'Email' => $aUserArray[1],
							'Friendly name' => $aUserArray[2]
						));
					}
				}
			}
		}
	}

	/**
	* @param string $sTab
	* @param ap_Screen $oScreen
	*/
	protected function initTableListFilter($sTab, ap_Screen &$oScreen)
	{
		if (AP_TAB_USERS === $sTab)
		{
			$iRealmId = $this->oAdminPanel->RType() ? $this->oAdminPanel->RealmId() : 0;

			$oScreen->InitFilter('Domains');
			if ($this->oAdminPanel->HasAccessDomain(0) && !$this->oAdminPanel->XType && 0 === $iRealmId)
			{
				$oScreen->AddFilter('0', AP_WITHOUT_DOMAIN_NAME, 'domWebMail');
			}

			$aFilters = $this->oDomainsApi->GetFilterList($iRealmId);
			if (is_array($aFilters))
			{
				foreach ($aFilters as $mIndex => $aValues)
				{
					if (is_array($aValues) && 1 < count($aValues) &&
						$this->oAdminPanel->HasAccessDomain($mIndex))
					{
						$oScreen->AddFilter($mIndex, $aValues[1],
							$aValues[0] ? 'domWebMail' : 'domWebMail'); // domMailSuite
					}
				}
			}
		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableMainSwitchers($sTab, ap_Screen &$oScreen)
	{
		$sMainAction = $this->getQueryAction();
		if (AP_TAB_REALMS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'new':
					$oScreen->Main->AddTopSwitcher($this->sPath.'/templates/main-top-new-realm.php');

					$oScreen->Main->AddSwitcher(
						PRO_SWITCHER_MODE_REALM, PRO_SWITCHER_MODE_REALM_NAME,
						$this->sPath.'/templates/main-realm.php');
					break;

				case 'edit':
					$iRealmId = isset($_GET['uid']) ? (int) $_GET['uid'] : null;

					$oRealm =& $this->oAdminPanel->GetMainObject('realm_edit');
					if (!$oRealm && null !== $iRealmId && 0 < $iRealmId)
					{
						$oRealm = $this->GetRealmById($iRealmId);
						if ($oRealm)
						{
							$this->oAdminPanel->SetMainObject('realm_edit', $oRealm);
						}
					}

					if ($oRealm)
					{
						$oScreen->Main->AddSwitcher(
							PRO_SWITCHER_MODE_REALM, PRO_SWITCHER_MODE_REALM_NAME,
							$this->sPath.'/templates/main-realm.php');
					}
					break;
			}
		}
		else if (AP_TAB_CHANNELS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'new':
					$oScreen->Main->AddTopSwitcher($this->sPath.'/templates/main-top-new-channel.php');

					$oScreen->Main->AddSwitcher(
						PRO_SWITCHER_MODE_CHANNEL, PRO_SWITCHER_MODE_CHANNEL_NAME,
						$this->sPath.'/templates/main-channel.php');
					break;

				case 'edit':
					$iChannelId = isset($_GET['uid']) ? (int) $_GET['uid'] : null;

					$oChannel =& $this->oAdminPanel->GetMainObject('channel_edit');
					if (!$oChannel && null !== $iChannelId && 0 < $iChannelId)
					{
						$oChannel = $this->GetChannelById($iChannelId);
						if ($oChannel)
						{
							$this->oAdminPanel->SetMainObject('channel_edit', $oChannel);
						}
					}

					if ($oChannel)
					{
						$oScreen->Main->AddSwitcher(
							PRO_SWITCHER_MODE_CHANNEL, PRO_SWITCHER_MODE_CHANNEL_NAME,
							$this->sPath.'/templates/main-channel.php');
					}
					break;
			}
		}
		else if (AP_TAB_DOMAINS === $sTab && $this->bHasWebmail)
		{
			switch ($sMainAction)
			{
				case 'new':
					$oScreen->Main->AddTopSwitcher($this->sPath.'/templates/main-top-new-domain.php');

					if (!$this->oAdminPanel->RType() || 0 < $this->oAdminPanel->RealmId())
					{
						$oScreen->Data->SetValue('classHideRealmName', 'wm_hide');
					}

					$oScreen->Main->AddSwitcher(
						CM_SWITCHER_MODE_NEW_DOMAIN, CM_SWITCHER_MODE_NEW_DOMAIN_NAME,
						$this->sPath.'/templates/main-new-domain.php');
					break;

				case 'edit':
					$oDomain =& $this->oAdminPanel->GetMainObject('domain_edit');
					if ($oDomain)
					{
						if (CApi::IsValidFullSupportPhpVersion())
						{
							$oScreen->Main->AddSwitcher(
								PRO_SWITCHER_MODE_EDIT_CALENDAR, PRO_SWITCHER_MODE_EDIT_CALENDAR_NAME,
								$this->sPath.'/templates/main-edit-domain-calendar.php');
						}

						$oScreen->Main->AddSwitcher(
							WM_SWITCHER_MODE_EDIT_DOMAIN_WEBMAIL, WM_SWITCHER_MODE_EDIT_DOMAIN_WEBMAIL_NAME,
							$this->sPath.'/templates/main-edit-domain-webmail.php');

						if (CApi::IsValidFullSupportPhpVersion() && $this->oCollaborationApi && $this->oCollaborationApi->IsContactsGlobalSupported())
						{
							$oScreen->Main->AddSwitcher(
								WM_SWITCHER_MODE_EDIT_DOMAIN_ADDRESS_BOOK, WM_SWITCHER_MODE_EDIT_DOMAIN_ADDRESS_BOOK_NAME,
								$this->sPath.'/templates/main-edit-domain-address-book.php');
						}
					}
					break;
			}
		}
		else if (AP_TAB_USERS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'new':

					$mDomainIndex = $oScreen->GetFilterIndex();
					if ($this->oAdminPanel->HasAccessDomain($mDomainIndex))
					{
						$oScreen->Main->AddTopSwitcher($this->sPath.'/templates/main-top-new-user.php');

						$oDomain = $this->GetDomain((int) $mDomainIndex);
						if ($oDomain)
						{
							$this->oAdminPanel->SetMainObject('domain_filter', $oDomain);

							if ($oDomain->IsDefaultDomain)
							{
								$oScreen->Main->AddSwitcher(
									CM_SWITCHER_MODE_NEW_USER, CM_SWITCHER_MODE_NEW_USER_NAME,
									$this->sPath.'/templates/main-new-user-external.php');

								$oScreen->Main->AddSwitcher(
									CM_SWITCHER_MODE_NEW_USER, CM_SWITCHER_MODE_NEW_USER_NAME,
									$this->sPath.'/templates/main-edit-user-webmail-inc-server.php');

								$oScreen->Main->AddSwitcher(
									CM_SWITCHER_MODE_NEW_USER, CM_SWITCHER_MODE_NEW_USER_NAME,
									$this->sPath.'/templates/main-edit-user-webmail-out-server.php');
							}
							else
							{
								$oScreen->Main->AddSwitcher(
									CM_SWITCHER_MODE_NEW_USER, CM_SWITCHER_MODE_NEW_USER_NAME,
									$this->sPath.'/templates/main-new-user.php');
							}
						}
					}
					break;

				case 'edit':
					$iAccountId = isset($_GET['uid']) ? (int) $_GET['uid'] : null;

					$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
					if (!$oAccount && null !== $iAccountId && 0 < $iAccountId)
					{
						$oAccount = $this->GetAccount($iAccountId);
						if ($oAccount)
						{
							$this->oAdminPanel->SetMainObject('account_edit', $oAccount);
						}
					}

					if ($oAccount)
					{
						$oScreen->Main->AddSwitcher(
							CM_SWITCHER_MODE_EDIT_USER_GENERAL, CM_SWITCHER_MODE_EDIT_USER_GENERAL_NAME,
							$this->sPath.'/templates/main-edit-user-general.php');
					}
					break;
			}
		}
	}


	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableMainSwitchersPost($sTab, ap_Screen &$oScreen)
	{
		$sMainAction = $this->getQueryAction();
		if (AP_TAB_DOMAINS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'new':
					$oScreen->Main->AddSwitcher(
						CM_SWITCHER_MODE_NEW_DOMAIN, CM_SWITCHER_MODE_NEW_DOMAIN_NAME,
						$this->sPath.'/templates/main-new-domain-post.php');
					break;
			}
		}
		else if (AP_TAB_USERS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'edit':
					$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
					if ($oAccount)
					{
						$oScreen->Main->AddSwitcher(
							WM_SWITCHER_MODE_EDIT_USERS_GENERAL, WM_SWITCHER_MODE_EDIT_USERS_GENERAL_NAME,
							$this->sPath.'/templates/main-edit-user-general-webmail.php');
					}
					break;
			}
		}

	}

	/**
	 * @return void
	 */
	protected function initInclude()
	{
		include $this->sPath.'/inc/constants.php';
		include $this->sPath.'/inc/populate.php';
		include $this->sPath.'/inc/post.php';
		include $this->sPath.'/inc/pop.php';
		include $this->sPath.'/inc/ajax.php';
	}

	/**
	 * @return string
	 */
	public function GetLicenseKey()
	{
		return $this->oLicApi->GetLicenseKey();
	}

	/**
	 * @param string $sKey
	 * @return bool
	 */
	public function UpdateLicenseKey($sKey)
	{
		return $this->oLicApi->UpdateLicenseKey($sKey);
	}

	/**
	 * @return int
	 */
	public function GetCurrentNumberOfUsers()
	{
		$iCnt = $this->oLicApi->GetCurrentNumberOfUsers();
		return null === $iCnt ? 0 : $iCnt;
	}

	/**
	 * @return int
	 */
	public function GetUserNumberLimit()
	{
		return $this->oLicApi->GetUserNumberLimitAsString();
	}

	/**
	 * @return bool
	 */
	public function IsTrial()
	{
		return in_array($this->oLicApi->GetLicenseType(), array(10, 11));
	}

	/**
	 * @param array $aRealmsIds
	 * @return bool
	 */
	public function DeleteRealms($aRealmsIds)
	{
		$iResult = 1;
		if ($this->oRealmsApi)
		{
			foreach ($aRealmsIds as $iRealmId)
			{
				$oRealm = $this->oRealmsApi->GetRealmById($iRealmId);
				if ($oRealm instanceof CRealm)
				{
					$iResult &= $this->DeleteRealm($oRealm);
				}
				unset($oRealm);
			}
		}
		return (bool) $iResult;
	}

	/**
	 * @param array $aChannelsIds
	 * @return bool
	 */
	public function DeleteChannels($aChannelsIds)
	{
		$iResult = 1;
		if ($this->oChannelsApi)
		{
			foreach ($aChannelsIds as $iChannelId)
			{
				$oChannel = $this->oChannelsApi->GetChannelById($iChannelId);
				if ($oChannel instanceof CChannel)
				{
					$iResult &= $this->DeleteChannel($oChannel);
				}
				unset($oChannel);
			}
		}
		return (bool) $iResult;
	}

	/**
	 * @param CRealm $oRealm
	 * @return bool
	 */
	public function DeleteRealm($oRealm)
	{
		return $this->oRealmsApi->DeleteRealm($oRealm);
	}

	/**
	 * @param CChannel $oChannel
	 * @return bool
	 */
	public function DeleteChannel($oChannel)
	{
		return $this->oChannelsApi->DeleteChannel($oChannel);
	}

	/**
	 * @param CRealm $oRealm
	 * @return bool
	 */
	public function CreateRealm(CRealm $oRealm)
	{
		if (!$this->oRealmsApi->CreateRealm($oRealm))
		{
			$this->lastErrorCode = $this->oRealmsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oRealmsApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param CChannel $oChannel
	 * @return bool
	 */
	public function CreateChannel(CChannel $oChannel)
	{
		if (!$this->oChannelsApi->CreateChannel($oChannel))
		{
			$this->lastErrorCode = $this->oChannelsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oChannelsApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param int $iRealmId
	 *
	 * @return array | false
	 */
	public function GetRealmDomains($iRealmId)
	{
		return $this->oRealmsApi->GetRealmDomains($iRealmId);
	}

	/**
	 * @param int $iRealmId
	 * @return CRealm
	 */
	public function GetRealmById($iRealmId)
	{
		return $this->oRealmsApi->GetRealmById($iRealmId);
	}

	/**
	 * @param int $iChannelId
	 * @return CCannel
	 */
	public function GetChannelById($iChannelId)
	{
		return $this->oChannelsApi->GetChannelById($iChannelId);
	}

	/**
	 * @param CRealm $oRealm
	 * @return bool
	 */
	public function UpdateRealm(CRealm $oRealm)
	{
		if (!$this->oRealmsApi->UpdateRealm($oRealm))
		{
			$this->lastErrorCode = $this->oRealmsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oRealmsApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param CChannel $oChannel
	 * @return bool
	 */
	public function UpdateChannel(CChannel $oChannel)
	{
		if (!$this->oChannelsApi->UpdateChannel($oChannel))
		{
			$this->lastErrorCode = $this->oChannelsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oChannelsApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	* @param CDomain &$oAccount
	* @return bool
	*/
	public function CreateAccount(CAccount &$oAccount)
	{
		if (!$this->oUsersApi->CreateAccount($oAccount, !$oAccount->IsInternal))
		{
			$this->lastErrorCode = $this->oUsersApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oUsersApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param array $aAccountsIds
	 * @param bool $bIsEnabled
	 * @return bool
	 */
	public function EnableAccounts($aAccountsIds, $bIsEnabled)
	{
		if (!$this->oUsersApi->EnableAccounts($aAccountsIds, $bIsEnabled))
		{
			$this->lastErrorCode = $this->oUsersApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oUsersApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param CDomain &$oAccount
	 * @return bool
	 */
	public function UpdateAccount(CAccount &$oAccount)
	{
		if (!$this->oUsersApi->UpdateAccount($oAccount))
		{
			$this->lastErrorCode = $this->oUsersApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oUsersApi->GetLastErrorMessage();
			return false;
		}
		return true;
	}

	/**
	 * @param CAccount $oAccount
	 * @return bool
	 */
	public function AccountExists(CAccount $oAccount)
	{
		return $this->oUsersApi->AccountExists($oAccount);
	}

	/**
	 * @param array $aAccountsIds
	 * @return bool
	 */
	public function DeleteAccounts($aAccountsIds)
	{
		$iResult = 1;
		foreach ($aAccountsIds as $iAccountId)
		{
			$iResult &= $this->oUsersApi->DeleteAccountById($iAccountId);
		}
		return (bool) $iResult;
	}

	/**
	 * @param int $iAccountId
	 * @return CAccount
	 */
	public function GetAccount($iAccountId)
	{
		if (is_numeric($iAccountId) && 0 < $iAccountId)
		{
			return $this->oUsersApi->GetAccountById($iAccountId);
		}
		return null;
	}

	/**
	* @param int $iDomainId
	* @return CDomain
	*/
	public function GetDomain($iDomainId)
	{
		if (0 === $iDomainId)
		{
			return $this->oDomainsApi->GetDefaultDomain();
		}
		return $this->oDomainsApi->GetDomainById($iDomainId);
	}

	/**
	* @param CDomain &$oDomain
	* @return bool
	*/
	public function CreateDomain(CDomain &$oDomain)
	{
		if (!$this->oDomainsApi->CreateDomain($oDomain))
		{
			$this->lastErrorCode = $this->oDomainsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oDomainsApi->GetLastErrorMessage();
			return false;
		}

		CSession::Set(AP_SESS_DOMAIN_NEXT_EDIT_ID, $oDomain->IdDomain);
		return true;
	}

	/**
	 * @param string $sRealmName
	 *
	 * @return int
	 */
	public function GetRealmIdByName($sRealmName)
	{
		return $this->oRealmsApi->GetRealmIdByLogin($sRealmName);
	}

	/**
	* @param array $aDomainsIds
	* @return bool
	*/
	public function DeleteDomains($aDomainsIds)
	{
		if (!$this->oDomainsApi->DeleteDomains($aDomainsIds))
		{
			$this->lastErrorCode = $this->oDomainsApi->GetLastErrorCode();
			$this->lastErrorMessage = $this->oDomainsApi->GetLastErrorMessage();
			return false;
		}

		return true;
	}

	/**
	* @param string $sLogin
	* @param string $sPassword
	 *
	* @return int
	*/
	public function GetRealmIdByLoginPassword($sLogin, $sPassword)
	{
		return $this->oRealmsApi->GetRealmIdByLogin($sLogin, $sPassword);
	}
}
