$(function() {

	$('#IdRealmsNewRealmButton').click(function(){
		document.location = AP_INDEX + '?new';
	});

	$('#IdRealmsDeleteButton').click(function(){
		var oChecked = $('#table_form input:checkbox[name="chCollection[]"]:checked');
		if (0 < oChecked.length)
		{
			if (confirm('Delete the selected realms with all the contained domains and users?')) {
				$('#table_form #action').val('delete');
				$('#table_form').submit();
			}
		}
		else
		{
			OnlineMsgError('No realms selected.');
		}
	});

});
