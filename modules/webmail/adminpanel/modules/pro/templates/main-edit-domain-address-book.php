<table class="wm_contacts_view">
	<tr>
		<td class="wm_field_title">
			<span id="selGlobalAddressBook_label">
				Global address book
			</span>
		</td>
		<td class="wm_field_value">
			<select name="selGlobalAddressBook" class="wm_select override" id="selGlobalAddressBook">
				<option value="<?php echo EnumConvert::ToPost(EContactsGABVisibility::Off, 'EContactsGABVisibility');
					?>" <?php $this->Data->PrintSelectedValue('optGlobalAddressBookOff'); ?>>Off</option>
				<option value="<?php echo EnumConvert::ToPost(EContactsGABVisibility::DomainWide, 'EContactsGABVisibility');
					?>" <?php $this->Data->PrintSelectedValue('optGlobalAddressBookDomain'); ?>>Domain wide</option>
				<?php if ($this->Data->GetValue('bRType')) { ?>
					<option value="<?php echo EnumConvert::ToPost(EContactsGABVisibility::RealmWide, 'EContactsGABVisibility');
						?>" <?php $this->Data->PrintSelectedValue('optGlobalAddressBookRealm'); ?>>Realm wide</option>
				<?php } else { ?>
				<option value="<?php echo EnumConvert::ToPost(EContactsGABVisibility::SystemWide, 'EContactsGABVisibility');
					?>" <?php $this->Data->PrintSelectedValue('optGlobalAddressBookSystem'); ?>>System wide</option>
				<?php } ?>
			</select>
		</td>
	</tr>
</table>