<table class="wm_contacts_view">
	<tr>
		<td class="wm_field_value" colspan="2">
			<input type="checkbox" class="wm_checkbox override" value="1"
				name="chAllowUsersAddNewAccounts" id="chAllowUsersAddNewAccounts"
				<?php $this->Data->PrintCheckedValue('chAllowUsersAddNewAccounts'); ?> />

			<label id="chAllowUsersAddNewAccounts_label" for="chAllowUsersAddNewAccounts">
				Allow users to add external mailboxes
			</label>
		</td>
	</tr>
</table>