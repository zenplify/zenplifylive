<tr>
	<td colspan="2">
		<div class="wm_safety_info">
			Allows you to enter the license key for the product and displays the current details on the license.
		</div>
	</td>
</tr>
<tr>
	<td align="left" width="200">
		<span id="txtLicenseKey_label">License&nbsp;key</span>
	</td>
	<td>
		<input type="text" class="wm_input" name="txtLicenseKey" id="txtLicenseKey" value="<?php $this->Data->PrintInputValue('txtLicenseKey') ?>" size="60" />
	</td>
</tr>
<tr>
	<td align="left">
		<span id="txtCurrentNumberOfUsers_label">Current&nbsp;number&nbsp;of&nbsp;users</span>
	</td>
	<td>
		<b><?php $this->Data->PrintValue('txtCurrentNumberOfUsers') ?></b>
	</td>
</tr>
<tr>
	<td align="left">
		<span id="txtLicenseType_label">License&nbsp;type</span>
	</td>
	<td>
		<b><?php $this->Data->PrintValue('txtLicenseType') ?></b>
	</td>
</tr>

<tr class="<?php $this->Data->PrintValue('classHideTrialText') ?>">
	<td>
	</td>
	<td>
		This is a trial version. You can purchase a license and get your permanent key at
		<a href="http://www.afterlogic.com/purchase/webmail-pro" target="_black">http://www.afterlogic.com/purchase/webmail-pro</a>
	</td>
</tr>




