<table class="wm_contacts_view">
	<tr>
		<td class="wm_field_value" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="chEnableUser" id="chEnableUser" <?php $this->Data->PrintCheckedValue('chEnableUser') ?> />
			<label for="chEnableUser" id="chEnableUser_label">User Enabled</label>
		</td>
	</tr>
	<tr><td colspan="2"><br /></td></tr>
	<tr>
		<td class="wm_field_title">
			<?php if ($this->Data->GetValueAsBool('domainIsInternal')) { ?>
				User
			<?php } else { ?>
				Login
			<?php } ?>
		</td>
		<td class="wm_field_value">
			<input name="hiddenDomainId" type="hidden" id="hiddenDomainId" value="<?php $this->Data->PrintInputValue('hiddenDomainId') ?>" />
			<input name="hiddenAccountId" type="hidden" id="hiddenAccountId" value="<?php $this->Data->PrintInputValue('hiddenAccountId') ?>" />
			<input name="hiddenUserId" type="hidden" id="hiddenUserId" value="<?php $this->Data->PrintInputValue('hiddenUserId') ?>" />
			<?php $this->Data->PrintValue('txtEditLogin') ?>
		</td>
	</tr>
	<tr><td colspan="2"></td></tr>
	<tr>
		<td class="wm_field_title">
			Friendly name
		</td>
		<td class="wm_field_value">
			<input name="txtFullName" type="text" id="txtFullName" class="wm_input"
				style="width: 150px" maxlength="100" value="<?php $this->Data->PrintInputValue('txtFullName') ?>" />
		</td>
	</tr>
</table>