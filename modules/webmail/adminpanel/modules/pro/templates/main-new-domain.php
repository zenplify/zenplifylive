<table class="wm_contacts_view">
	<tr>
		<td class="wm_field_title">
			Domain name *
		</td>
		<td class="wm_field_value">
			<input name="txtDomainName" type="text" id="txtDomainName" class="wm_input" style="width: 200px" maxlength="50" value="" />
		</td>
	</tr>

	<tr class="<?php $this->Data->PrintInputValue('classHideRealmName') ?>">
		<td class="wm_field_title">
			Realm name
		</td>
		<td class="wm_field_value">
			<input name="txtRealmName" type="text" id="txtRealmName" class="wm_input" style="width: 200px" value="" />

			<div class="wm_information_com">
				Leave blank to place the domain in default realm.
			</div>
		</td>
	</tr>
</table>