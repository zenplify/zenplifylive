	<tr>
		<td width="100"></td>
		<td></td>
	</tr>

	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableMobileSync" id="ch_EnableMobileSync" <?php $this->Data->PrintCheckedValue('ch_EnableMobileSync') ?>/>
			<label for="ch_EnableMobileSync">
				<?php echo CApi::I18N('ADMIN_PANEL/ENABLE_MOBILE_SYNC'); ?>
			</label>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left">
			<span>DAV server URL</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="50" name="text_DAVUrl" id="text_DAVUrl" value="<?php $this->Data->PrintInputValue('text_DAVUrl') ?>" />
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
				Specifies the external URL to the built-in DAV server.
				The CalDAV and CardDAV clients such as iPhone will access users' calendars via this URL.
				When "Enable Mobile Sync" is checked, the webmail also displays this URL to users in Settings/Mobile Sync area of their accounts.
				<br />
				<br />
				The webmail does its best to autodetect this URL during the installation but you may wish to change it to somewhat as simple as some.host.com with URL rewriting (e.g. mod_rewrite).
				This makes it easier to configure iOS devices like iPhone as they don't like some.host.com/some/stuff/after/hostname as DAV server URLs.
			</div>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left">
			<span>IMAP host name</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="50" name="text_IMAPHostName"
				id="text_IMAPHostName" value="<?php $this->Data->PrintInputValue('text_IMAPHostName') ?>" />
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
				Specifies the external host name of the local IMAP server (for iOS profile).
				When the client like iPhone accesses the webmail, the webmail prompts the user to download iOS profile
				which enables sync of e-mail, contacts and calendars with the device.
				Therefore, the device needs external host name of your e-mail server
				(as the device cannot use "localhost" name used by the webmail to access your local e-mail server).
			</div>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left">
			<span>SMTP host name</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="50" name="text_SMTPHostName"
				id="text_SMTPHostName" value="<?php $this->Data->PrintInputValue('text_SMTPHostName') ?>" />
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
				Specifies the external URL to the local SMTP server (for iOS profile).
			</div>
		</td>
	</tr>