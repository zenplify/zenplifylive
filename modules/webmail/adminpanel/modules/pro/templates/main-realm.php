<input name="intRealmId" type="hidden" value="<?php $this->Data->PrintInputValue('intRealmId'); ?>" />
<table class="wm_contacts_view">
	<tr class="<?php $this->Data->PrintInputValue('hideClassForEditRealm'); ?>">
		<td class="wm_field_title">
			Realm name *
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtLogin" type="text" id="txtLogin" class="wm_input" style="width: 350px" maxlength="100" value="<?php $this->Data->PrintInputValue('txtLogin'); ?>" />
		</td>
	</tr>
	<tr class="<?php $this->Data->PrintInputValue('hideClassForNewRealm'); ?>">
		<td class="wm_field_title">
			Realm name *
		</td>
		<td class="wm_field_value" colspan="2">
			<strong>
				<?php $this->Data->PrintValue('txtLogin'); ?>
				<?php $this->Data->PrintValue('txtChannelAdd'); ?>
			</strong>
		</td>
	</tr>
	<tr class="<?php $this->Data->PrintInputValue('hideClassForEditRealm'); ?>">
		<td class="wm_field_title">
			Channel
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtChannel" type="text" id="txtChannel" class="wm_input" style="width: 350px" maxlength="100" value="<?php $this->Data->PrintInputValue('txtChannel'); ?>" />

			<div class="wm_information_com">
				Leave blank to place the realm in default channel.
			</div>
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Admin email
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtEmail" type="text" id="txtEmail" class="wm_input" style="width: 350px" maxlength="100" value="<?php $this->Data->PrintInputValue('txtEmail'); ?>" />

			<div class="wm_information_com">
				The system will send notification e-mails to this address, such as when the realm quota is exceeded.<br />
				Due to that, it's recommended to specify non-local e-mail address.
			</div>
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Password
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtPassword" type="password" maxlength="100" id="txtPassword" class="wm_input" style="width: 350px;" value="<?php $this->Data->PrintInputValue('txtPassword'); ?>" />
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="chEnableAdminLogin" type="checkbox" id="chEnableAdminLogin" value="1" class="wm_checkbox"
				<?php $this->Data->PrintCheckedValue('chEnableAdminLogin'); ?> />
			<label for="chEnableAdminLogin">
				Enable admin panel access with the realm name and password
			</label>
			<div class="wm_information_com">
				Enables realm owner to log in this admin panel with their realm name and password.<br />
				Disable this if your customers (realm owners) must use only your own admin interface to manage domains in their realm.
			</div>
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Resource limits
		</td>
		<td class="wm_field_value" colspan="2">
			<?php $this->Data->PrintValue('txtUserLimit'); ?>
			<br />
			<?php $this->Data->PrintValue('txtDomainsLimit'); ?>
			<br />
			<br />
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Quota
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtQuota" type="text" maxlength="10" id="txtQuota" class="wm_input" style="width: 100px;" value="<?php $this->Data->PrintInputValue('txtQuota'); ?>" />
			&nbsp;
			MB
			&nbsp;
			<?php $this->Data->PrintValue('txtUsedText'); ?>

			<div class="wm_information_com">
				The quota value for the realm, and the used space.
			</div>
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Description
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="txtDescription" type="text" maxlength="255" id="txtDescription" class="wm_input" style="width: 350px;" value="<?php $this->Data->PrintInputValue('txtDescription'); ?>" />
		</td>
	</tr>
	<tr class="<?php $this->Data->PrintInputValue('hideClassForNewRealm'); ?>">
		<td class="wm_field_title">
			Domains
		</td>
		<td class="wm_field_value" colspan="2">
			<select class="wm_input" size="5" multiple="multiple" style="width: 353px">
				<?php $this->Data->PrintValue('selDomains'); ?>
			</select>
			<div class="wm_information_com">
				The list of domains belonging to this realm.
			</div>
		</td>
	</tr>
	<tr class="<?php $this->Data->PrintInputValue('hideClassForNewRealm'); ?>">
		<td class="wm_field_title">
		</td>
		<td class="wm_field_value" colspan="2">
			<input name="chRealmEnabled" type="checkbox" id="chRealmEnabled" value="1" class="wm_checkbox"
				<?php $this->Data->PrintCheckedValue('chRealmEnabled'); ?> />

			<label for="chRealmEnabled">
				Realm enabled
			</label>
			<div class="wm_information_com">
				When realm is disabled, no users in all domains belonging to the realm can access the system.
			</div>
		</td>
	</tr>
</table>
