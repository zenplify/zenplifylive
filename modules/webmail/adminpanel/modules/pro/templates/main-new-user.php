<table class="wm_contacts_view">
	<tr>
		<td class="wm_field_title">
		<?php if ($this->Data->GetValueAsBool('domainIsInternal')) { ?>
			User *
		<?php } else { ?>
			Login *
		<?php } ?>
		</td>
		<td class="wm_field_value">
			<input name="hiddenDomainId" type="hidden" id="hiddenDomainId" value="<?php $this->Data->PrintInputValue('hiddenDomainId') ?>" />
			<input name="txtNewLogin" type="text" id="txtNewLogin" class="wm_input"
				style="width: 150px" maxlength="100" value="" />
		</td>
	</tr>
	<tr>
		<td class="wm_field_title">
			Password *
		</td>
		<td class="wm_field_value">
			<input name="txtNewPassword" type="password" id="txtNewPassword" class="wm_input"
				style="width: 150px" maxlength="100" value="" />
		</td>
	</tr>
	<?php if ($this->Data->GetValueAsBool('domainIsInternal')) { ?>
	<tr>
		<td align="left" width="100">
			<nobr>Storage quota</nobr>
		</td>
		<td align="left">
			<input name="txtEditStorageQuota" type="text" id="txtEditStorageQuota" class="wm_input"
				style="width: 150px" maxlength="9" value="<?php $this->Data->PrintInputValue('txtEditStorageQuota') ?>" />
			KB
			(0 for unlimited)
		</td>
	</tr>
	<?php } ?>
</table>