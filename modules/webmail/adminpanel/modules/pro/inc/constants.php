<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

	define('PRO_MODE_LICESING', 'licensing');

	define('PRO_SWITCHER_MODE_CHANNEL', 'settings');
	define('PRO_SWITCHER_MODE_REALM', 'settings');
	define('PRO_SWITCHER_MODE_EDIT_CALENDAR', 'editcalendar');

	/* langs */

	define('PRO_MODE_LICESING_NAME', 'Licensing');
	define('PRO_SWITCHER_MODE_CHANNEL_NAME', 'Settings');
	define('PRO_SWITCHER_MODE_REALM_NAME', 'Settings');
	define('PRO_SWITCHER_MODE_EDIT_CALENDAR_NAME', 'Calendar');

	define('PRO_LANG_NOREALMS', 'No realms have yet been added');
	define('PRO_LANG_REALMS_RESULTEMPTY', 'No realms found');
	define('PRO_LANG_REALM_EXIST', 'Such realm already exists.');

	define('PRO_LANG_NOCHANNELS', 'No channels have yet been added');
	define('PRO_LANG_CHANNELS_RESULTEMPTY', 'No channels found');
	define('PRO_LANG_CHANNEL_EXIST', 'Such channel already exists.');

	define('PRO_LANG_DELETE_SUCCESSFUL', 'Deleted successfully.');
	define('PRO_LANG_DELETE_UNSUCCESSFUL', 'Failed to delete.');

	define('PRO_DEMO_LKEY', 'WM500-DEMO-DEMO-DEMO-DEMO-DEMO-DEMO-123457A');

