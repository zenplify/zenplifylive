<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

class CProPostAction extends ap_CoreModuleHelper
{
	public function RealmsCollectionDelete()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aAccountsIds = is_array($aCollection) ? $aCollection : array();

		$this->checkBolleanDeleteWithMessage(
			(0 < count($aAccountsIds) && $this->oModule->DeleteRealms($aAccountsIds))
		);
	}

	public function ChannelsCollectionDelete()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aAccountsIds = is_array($aCollection) ? $aCollection : array();

		$this->checkBolleanDeleteWithMessage(
			(0 < count($aAccountsIds) && $this->oModule->DeleteChannels($aAccountsIds))
		);
	}

	public function UsersCollectionDelete()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aAccountsIds = is_array($aCollection) ? $aCollection : array();

		$this->checkBolleanDeleteWithMessage(
			(0 < count($aAccountsIds) && $this->oModule->DeleteAccounts($aAccountsIds))
		);
	}

	public function UsersCollectionEnable()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aAccountsIds = is_array($aCollection) ? $aCollection : array();
		if (0 < count($aAccountsIds))
		{
			$this->oModule->EnableAccounts($aAccountsIds, true);
		}
	}

	public function UsersCollectionDisable()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aAccountsIds = is_array($aCollection) ? $aCollection : array();
		if (0 < count($aAccountsIds))
		{
			$this->oModule->EnableAccounts($aAccountsIds, false);
		}
	}

	public function DomainsCollectionDelete()
	{
		$aCollection = CPost::Get('chCollection', array());
		$aDomainsIds = is_array($aCollection) ? $aCollection : array();

		if (0 < count($aDomainsIds))
		{
			if (!$this->oModule->DeleteDomains($aDomainsIds))
			{
				$this->LastError = $this->oModule->GetLastErrorMessage();
			}
			else
			{
				$this->checkBolleanDeleteWithMessage(true);
			}
		}
		else
		{
			$this->checkBolleanDeleteWithMessage(false);
		}
	}

	public function SystemLicensing()
	{
		$sKey = CPost::Get('txtLicenseKey', null);
		if (null !== $sKey)
		{
			$this->checkBolleanWithMessage($this->oModule->UpdateLicenseKey(CPost::Get('txtLicenseKey')));
		}
	}

	public function ServicesDav()
	{
		$bResult = false;

		$this->oSettings->SetConf('WebMail/ExternalHostNameOfDAVServer', CPost::Get('text_DAVUrl'));
		$this->oSettings->SetConf('WebMail/ExternalHostNameOfLocalImap', CPost::Get('text_IMAPHostName'));
		$this->oSettings->SetConf('WebMail/ExternalHostNameOfLocalSmtp', CPost::Get('text_SMTPHostName'));

		/* @var $oApiDavManager CApiDavManager */
		$oApiDavManager = CApi::Manager('dav');
		if ($oApiDavManager)
		{
			$bResult = $oApiDavManager->SetMobileSyncEnable(CPost::GetCheckBox('ch_EnableMobileSync'));
			$bResult &= $this->oSettings->SaveToXml();
		}

		$this->checkBolleanWithMessage((bool) $bResult);
	}
}