<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

class CProAjaxAction extends ap_CoreModuleHelper
{
	public function UsersNew_Pre()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_new');
		if (!$oAccount)
		{
			$iDomainId = (int) CPost::Get('hiddenDomainId', 0);
			if ($this->oAdminPanel->HasAccessDomain($iDomainId))
			{
				$oDomain = $this->oModule->GetDomain($iDomainId);
				if ($oDomain)
				{
					$oAccount = new CAccount($oDomain);
					$this->oAdminPanel->SetMainObject('account_new', $oAccount);
				}
			}
		}
	}

	public function UsersNew()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_new');
		if ($oAccount)
		{
			$this->initNewAccountByPost($oAccount);
		}
	}

	public function UsersNew_Post()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_new');
		if ($oAccount)
		{
			$this->oAdminPanel->DeleteMainObject('account_new');
			if ($this->oModule->CreateAccount($oAccount))
			{
				$this->checkBolleanWithMessage(true);
				$this->Ref = '?edit&tab=users&uid='.$oAccount->IdAccount;
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
	}

	public function UsersEdit_Pre()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
		if (!$oAccount)
		{
			$iDomainId = (int) CPost::Get('hiddenDomainId', 0);
			if (CPost::Has('hiddenAccountId') && is_numeric(CPost::Get('hiddenAccountId', false))
			&& 	$this->oAdminPanel->HasAccessDomain($iDomainId))
			{
				$oAccount = $this->oModule->GetAccount((int) CPost::Get('hiddenAccountId', 0));
				$this->oAdminPanel->SetMainObject('account_edit', $oAccount);
			}
		}
	}

	public function UsersEdit()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
		if ($oAccount)
		{
			$this->initEditAccountByPost($oAccount);
		}
	}

	public function UsersEdit_Post()
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
		if ($oAccount)
		{
			$this->oAdminPanel->DeleteMainObject('account_edit');
			if ($this->oModule->UpdateAccount($oAccount))
			{
				$this->checkBolleanWithMessage(true);
				$this->Ref = '?edit&tab=users&uid='.$oAccount->IdAccount;
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
	}

	/**
	 * @param CAccount $oAccount
	 */
	protected function initEditAccountByPost(CAccount &$oAccount)
	{
		$oAccount->IsDisabled = !CPost::GetCheckBox('chEnableUser');
		if (CPost::Has('txtFullName'))
		{
			$oAccount->FriendlyName = (string) CPost::Get('txtFullName', '');
		}
	}

	/**
	 * @param CAccount $oAccount
	 */
	protected function initNewAccountByPost(CAccount &$oAccount)
	{
		if (CPost::Has('txtNewPassword'))
		{
			$oAccount->IsDefaultAccount = true;
			$oAccount->InitLoginAndEmail(CPost::Get('txtNewLogin'));
			$oAccount->IncomingMailPassword = CPost::Get('txtNewPassword');

			if ($oAccount->Domain && $oAccount->Domain->IsDefaultDomain)
			{
				$oAccount->Email = CPost::Get('txtNewEmail');

				$oAccount->IncomingMailProtocol = EnumConvert::FromPost(
				CPost::Get('selIncomingMailProtocol'), 'EMailProtocol');

				$oAccount->IncomingMailLogin = CPost::Get('txtIncomingMailLogin');
				$oAccount->IncomingMailServer = CPost::Get('txtIncomingMailHost');
				$oAccount->IncomingMailPort = (int) CPost::Get('txtIncomingMailPort');
				$oAccount->IncomingMailUseSSL = CPost::GetCheckBox('chIncomingUseSSL');

				$oAccount->OutgoingMailLogin = CPost::Get('txtOutgoingMailLogin');
				$oAccount->OutgoingMailPassword = CPost::Get('txtOutgoingMailPassword');
				$oAccount->OutgoingMailServer = CPost::Get('txtOutgoingMailHost');
				$oAccount->OutgoingMailPort = (int) CPost::Get('txtOutgoingMailPort');
				$oAccount->OutgoingMailUseSSL = CPost::GetCheckBox('chOutgoingUseSSL');

				$oAccount->OutgoingMailAuth = CPost::GetCheckBox('chOutgoingAuth')
					? ESMTPAuthType::AuthCurrentUser : ESMTPAuthType::NoAuth;
			}
		}
	}

	public function DomainsEdit()
	{
		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_edit');
		if ($oDomain)
		{
			$this->initUpdateDomainByPost($oDomain);
		}
	}

	protected function initUpdateDomainByPost(CDomain &$oDomain)
	{
		$oDomain->OverrideSettings = CPost::GetCheckBox('chOverrideSettings');

		if ($oDomain->OverrideSettings)
		{
			$oDomain->AllowCalendar = CPost::GetCheckBox('chEnableCalendar');

			if (CPost::Has('selWeekStartsOn'))
			{
				$oDomain->CalendarWeekStartsOn =
					EnumConvert::FromPost(CPost::Get('selWeekStartsOn'), 'ECalendarWeekStartOn');
			}

			$oDomain->CalendarShowWeekEnds = CPost::GetCheckBox('chShowWeekends');
			$oDomain->CalendarShowWorkDay = CPost::GetCheckBox('chShowWorkday');

			if (CPost::Has('selWorkdayStarts'))
			{
				$oDomain->CalendarWorkdayStarts = (int) CPost::Get('selWorkdayStarts');
			}

			if (CPost::Has('selWorkdayEnds'))
			{
				$oDomain->CalendarWorkdayEnds = (int) CPost::Get('selWorkdayEnds');
			}

			if (CPost::Has('radioDefaultTab'))
			{
				$oDomain->CalendarDefaultTab =
					EnumConvert::FromPost(CPost::Get('radioDefaultTab'), 'ECalendarDefaultTab');
			}

			$oDomain->AllowUsersAddNewAccounts = CPost::GetCheckBox('chAllowUsersAddNewAccounts');

			if (CPost::Has('selGlobalAddressBook'))
			{
				$oDomain->GlobalAddressBook =
					EnumConvert::FromPost(CPost::Get('selGlobalAddressBook'), 'EContactsGABVisibility');

				if (!$this->oAdminPanel->RType() && EContactsGABVisibility::RealmWide === $oDomain->GlobalAddressBook)
				{
					$oDomain->GlobalAddressBook	= EContactsGABVisibility::SystemWide;
				}
			}
		}
	}

	protected function initRealmByPost(CRealm &$oRealm)
	{
		$oRealm->Login = CPost::Get('txtLogin', $oRealm->Login);
		$oRealm->Email = CPost::Get('txtEmail', $oRealm->Email);

		$sChannel = CPost::Get('txtChannel', '');
		if (0 < strlen($sChannel))
		{
			$oChannelsApi = CApi::Manager('channels');
			if ($oChannelsApi)
			{
				/* @var $oChannel CChannel */
				$iIdChannel = $oChannelsApi->GetChannelIdByLogin($sChannel);
				if (0 < $iIdChannel)
				{
					$oRealm->IdChannel = $iIdChannel;
				}
				else
				{
					$this->oAdminPanel->DeleteMainObject('realm_new');
					$this->oAdminPanel->DeleteMainObject('realm_edit');
					$this->LastError = 'Channel does not exist';
				}
			}
		}

		if (CPost::Has('txtPassword') && (string) AP_DUMMYPASSWORD !== (string) CPost::Get('txtPassword'))
		{
			$oRealm->SetPassword(CPost::Get('txtPassword'));
		}

		$oRealm->QuotaInMB = (int) CPost::Get('txtQuota', 0);
		$oRealm->IsEnableAdminPanelLogin = CPost::GetCheckBox('chEnableAdminLogin');

		$bIsDisabled = !CPost::GetCheckBox('chRealmEnabled');
		if ($bIsDisabled !== $oRealm->IsDisabled)
		{
			$oRealm->IsDisabled = $bIsDisabled;
		}

		$oRealm->Description = CPost::Get('txtDescription', $oRealm->Description);
	}

	protected function initChannelByPost(CChannel &$oChannel)
	{
		$oChannel->Login = CPost::Get('txtLogin', $oChannel->Login);
		$oChannel->Description = CPost::Get('txtDescription', $oChannel->Description);
	}

	public function RealmsNew_Pre()
	{
		/* @var $oRealm CChannel */
		$oRealm =& $this->oAdminPanel->GetMainObject('realm_new');
		if (!$oRealm)
		{
			$oRealm = new CRealm();
			$this->oAdminPanel->SetMainObject('realm_new', $oRealm);
		}
	}

	public function RealmsNew()
	{
		$oRealm =& $this->oAdminPanel->GetMainObject('realm_new');
		if ($oRealm)
		{
			$this->initRealmByPost($oRealm);
		}
	}

	public function RealmsNew_Post()
	{
		$oRealm =& $this->oAdminPanel->GetMainObject('realm_new');
		if ($oRealm)
		{
			if ($this->oModule->CreateRealm($oRealm))
			{
				$this->checkBolleanWithMessage(true);
				$this->Ref = '?root';
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
	}

	public function ChannelsNew()
	{
		$oChannel = new CChannel();
		$this->initChannelByPost($oChannel);

		if ($this->oModule->CreateChannel($oChannel))
		{
			$this->checkBolleanWithMessage(true);
			$this->Ref = '?root';
		}
		else
		{
			if (0 < $this->oModule->GetLastErrorCode())
			{
				$this->LastError = $this->oModule->GetLastErrorMessage();
			}
			else
			{
				$this->checkBolleanWithMessage(false);
			}
		}
	}

	public function RealmsEdit()
	{
		$iRealmId = (int) CPost::Get('intRealmId', -1);

		$oRealm = $this->oModule->GetRealmById($iRealmId);
		if ($oRealm)
		{
			$this->initRealmByPost($oRealm);

			if ($this->oModule->UpdateRealm($oRealm))
			{
				$this->Ref = '?edit&tab='.AP_TAB_REALMS.'&uid='.$iRealmId;
				$this->checkBolleanWithMessage(true);
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
		else
		{
			$this->checkBolleanWithMessage(false);
		}
	}

	public function ChannelsEdit()
	{
		$iChannelId = (int) CPost::Get('intChannelId', -1);

		$oChannel = $this->oModule->GetChannelById($iRealmId);
		if ($oChannel)
		{
			$this->initChannelByPost($oChannel);

			if ($this->oModule->UpdateChannel($oChannel))
			{
				$this->Ref = '?edit&tab='.AP_TAB_CHANNELS.'&uid='.$iChannelId;
				$this->checkBolleanWithMessage(true);
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
		else
		{
			$this->checkBolleanWithMessage(false);
		}
	}

	public function DomainsNew_Pre()
	{
		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_new');
		if (!$oDomain)
		{
			$oDomain = new CDomain();
			$this->oAdminPanel->SetMainObject('domain_new', $oDomain);
		}
	}

	public function DomainsNew()
	{
		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_new');
		if ($oDomain)
		{
			$this->initNewDomainByPost($oDomain);
		}
	}

	public function DomainsNew_Post()
	{
		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_new');
		if ($oDomain)
		{
			$this->oAdminPanel->DeleteMainObject('domain_new');

			if ($this->oModule->CreateDomain($oDomain))
			{
				$this->checkBolleanWithMessage(true);
				$this->Ref = ($oDomain->OverrideSettings)
					? '?edit&tab=domains&uid='.$oDomain->IdDomain : '?root';
			}
			else
			{
				if (0 < $this->oModule->GetLastErrorCode())
				{
					$this->LastError = $this->oModule->GetLastErrorMessage();
				}
				else
				{
					$this->checkBolleanWithMessage(false);
				}
			}
		}
	}

	protected function initNewDomainByPost(CDomain &$oDomain)
	{
		$sDomainName = CPost::Get('txtDomainName', '');

		$oDomain->IsDefaultDomain = false;
		$oDomain->OverrideSettings = CPost::GetCheckBox('chOverrideSettings');

		$oDomain->Name = $sDomainName;
		$oDomain->Url = '';

		if (0 < $this->oAdminPanel->RealmId())
		{
			$oDomain->IdRealm = $this->oAdminPanel->RealmId();
		}
		else
		{
			$sRealm = CPost::Get('txtRealmName', '');
			if (0 < strlen($sRealm))
			{
				$iIdRealm = $this->oModule->GetRealmIdByName($sRealm);
				if (0 === $iIdRealm)
				{
					$this->oAdminPanel->DeleteMainObject('domain_new');
					$this->LastError = 'Realm does not exist';
				}
				else
				{
					$oDomain->IdRealm = $iIdRealm;
				}
			}
		}
	}

}
