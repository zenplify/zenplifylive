<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

class CProPopulateData extends ap_CoreModuleHelper
{
	public function UsersMainNew(ap_Table_Screen &$oScreen)
	{
		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_filter');
		if ($oDomain)
		{
			$oScreen->Data->SetValue('hiddenDomainId', $oDomain->IdDomain);

			if ($oDomain->IsDefaultDomain)
			{
				$oScreen->Data->SetValue('optIncomingProtocolIMAP', EMailProtocol::IMAP4 === $oDomain->IncomingMailProtocol);
				$oScreen->Data->SetValue('optIncomingProtocolPOP3', EMailProtocol::POP3 === $oDomain->IncomingMailProtocol);

				$oScreen->Data->SetValue('txtIncomingMailHost', $oDomain->IncomingMailServer);
				$oScreen->Data->SetValue('txtIncomingMailPort', $oDomain->IncomingMailPort);
				$oScreen->Data->SetValue('chIncomingUseSSL', $oDomain->IncomingMailUseSSL);

				$oScreen->Data->SetValue('txtOutgoingMailHost', $oDomain->OutgoingMailServer);
				$oScreen->Data->SetValue('txtOutgoingMailPort', $oDomain->OutgoingMailPort);
				$oScreen->Data->SetValue('chOutgoingUseSSL', $oDomain->OutgoingMailUseSSL);

				$oScreen->Data->SetValue('chOutgoingAuth',
					ESMTPAuthType::NoAuth !== $oDomain->OutgoingMailAuth);
			}

			$oScreen->Data->SetValue('txtEditStorageQuota', $oDomain->UserQuota);
		}
	}

	public function UsersMainEdit(ap_Table_Screen &$oScreen)
	{
		/* @var $oAccount CAccount */
		$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
		if ($oAccount)
		{
			$oScreen->Data->SetValue('hiddenAccountId', $oAccount->IdAccount);
			$oScreen->Data->SetValue('hiddenUserId', $oAccount->IdUser);
			$oScreen->Data->SetValue('hiddenDomainId', $oAccount->IdDomain);
			$oScreen->Data->SetValue('chEnableUser', !$oAccount->IsDisabled);
			$oScreen->Data->SetValue('txtEditLogin', $oAccount->IncomingMailLogin);
			$oScreen->Data->SetValue('txtEditPassword', AP_DUMMYPASSWORD);

			if ($oAccount->IsInternal)
			{
				$oAccount->InitStorageUsedSpace();

				$sUsedDesc = api_Utils::GetFriendlySize($oAccount->StorageUsedSpace * 1024);
				if (0 < $oAccount->StorageUsedSpace && 0 < $oAccount->StorageQuota)
				{
					$iUsed = floor(($oAccount->StorageUsedSpace / $oAccount->StorageQuota) * 100);
					$sUsedDesc .= ' ('.$iUsed.'% used)';
				}

				$oScreen->Data->SetValue('txtEditStorageQuota', $oAccount->StorageQuota);
				$oScreen->Data->SetValue('txtUsedSpaceDesc', $sUsedDesc);
			}

			$oScreen->Data->SetValue('txtFullName', $oAccount->FriendlyName);
//			if ($this->oAdminPanel->IsSuperAdminAuthType())
//			{
//				$oScreen->Data->SetValue('hrefLoginToAccount', AP_INDEX_FILE.'?blank&type=login&id='.$oAccount->IdAccount);
//			}
//			else
//			{
				$oScreen->Data->SetValue('classLoginToAccount', 'wm_hide');
//			}

		}

	}

	public function DomainsMainEdit(ap_Table_Screen &$oScreen)
	{
		$iContactsGABVisibility = EContactsGABVisibility::Off;

		/* @var $oDomain CDomain */
		$oDomain =& $this->oAdminPanel->GetMainObject('domain_edit');
		if ($oDomain)
		{
			$oScreen->Data->SetValue('chEnableCalendar', $oDomain->AllowCalendar);
			$oScreen->Data->SetValue('chShowWeekends', $oDomain->CalendarShowWeekEnds);
			$oScreen->Data->SetValue('chShowWorkday', $oDomain->CalendarShowWorkDay);

			$oScreen->Data->SetValue('optWeekStartsOnSaturday',
				ECalendarWeekStartOn::Saturday === $oDomain->CalendarWeekStartsOn);
			$oScreen->Data->SetValue('optWeekStartsOnSunday',
				ECalendarWeekStartOn::Sunday === $oDomain->CalendarWeekStartsOn);
			$oScreen->Data->SetValue('optWeekStartsOnMonday',
				ECalendarWeekStartOn::Monday === $oDomain->CalendarWeekStartsOn);

			$oScreen->Data->SetValue('radioDefaultTabDay',
				ECalendarDefaultTab::Day === $oDomain->CalendarDefaultTab);
			$oScreen->Data->SetValue('radioDefaultTabWeek',
				ECalendarDefaultTab::Week === $oDomain->CalendarDefaultTab);
			$oScreen->Data->SetValue('radioDefaultTabMonth',
				ECalendarDefaultTab::Month === $oDomain->CalendarDefaultTab);


			$iWorkdayStarts = $oDomain->CalendarWorkdayStarts;
			$iWorkdayEnds = $oDomain->CalendarWorkdayEnds;

			$sWorkdayStartsOptions = '';
			$aWorkdayStartsList = range(0, 23);
			foreach ($aWorkdayStartsList as $iWorkdayStartsCount)
			{
				$sWorkdayStartsView = (9 < $iWorkdayStartsCount)
				? $iWorkdayStartsCount.':00' : '0'.$iWorkdayStartsCount.':00';
				$sSelected = ($iWorkdayStartsCount === $iWorkdayStarts) ? ' selected="selected"' : '';
				$sWorkdayStartsOptions .= '<option value="'.$iWorkdayStartsCount
				.'"'.$sSelected.'>'.$sWorkdayStartsView.'</option>';
			}
			$oScreen->Data->SetValue('selWorkdayStartsOptions', $sWorkdayStartsOptions);

			$sWorkdayEndsOptions = '';
			$aWorkdayEndsList = range(0, 23);
			foreach ($aWorkdayEndsList as $iWorkdayEndsCount)
			{
				$sWorkdayEndsView = (9 < $iWorkdayEndsCount)
				? $iWorkdayEndsCount.':00' : '0'.$iWorkdayEndsCount.':00';
				$sSelected = ($iWorkdayEndsCount === $iWorkdayEnds) ? ' selected="selected"' : '';
				$sWorkdayEndsOptions .= '<option value="'.$iWorkdayEndsCount
				.'"'.$sSelected.'>'.$sWorkdayEndsView.'</option>';
			}
			$oScreen->Data->SetValue('selWorkdayEndsOptions', $sWorkdayEndsOptions);

			$oScreen->Data->SetValue('bRType', $this->oAdminPanel->RType());

			$oScreen->Data->SetValue('optGlobalAddressBookOff', true);

			$iContactsGABVisibility = $oDomain->GlobalAddressBook;
			$oScreen->Data->SetValue('optGlobalAddressBookOff',
				EContactsGABVisibility::Off === $iContactsGABVisibility);
			$oScreen->Data->SetValue('optGlobalAddressBookDomain',
				EContactsGABVisibility::DomainWide === $iContactsGABVisibility);
			$oScreen->Data->SetValue('optGlobalAddressBookRealm',
					EContactsGABVisibility::RealmWide === $iContactsGABVisibility);
			$oScreen->Data->SetValue('optGlobalAddressBookSystem',
				EContactsGABVisibility::SystemWide === $iContactsGABVisibility);

			$oScreen->Data->SetValue('chAllowUsersAddNewAccounts', $oDomain->AllowUsersAddNewAccounts);
		}

	}

	public function SystemLicensing(ap_Standard_Screen &$oScreen)
	{
		$oScreen->Data->SetValue('txtLicenseKey',
			$this->oAdminPanel->IsOnlyReadAuthType() ? PRO_DEMO_LKEY : $this->oModule->GetLicenseKey());

		$oScreen->Data->SetValue('txtCurrentNumberOfUsers', $this->oModule->GetCurrentNumberOfUsers());
		$oScreen->Data->SetValue('txtLicenseType', $this->oModule->GetUserNumberLimit());
		$oScreen->Data->SetValue('classHideTrialText', $this->oModule->IsTrial() ? '' : 'wm_hide');
	}

	public function RealmsMainNew(ap_Table_Screen &$oScreen)
	{
		$oScreen->Data->SetValue('chRealmEnabled', true);
		$oScreen->Data->SetValue('hideClassForNewRealm', 'wm_hide');
		$oScreen->Data->SetValue('hideClassForEditRealm', '');
		$oScreen->Data->SetValue('txtQuota', 0);
	}

	public function ChannelsMainNew(ap_Table_Screen &$oScreen)
	{
		$oScreen->Data->SetValue('hideClassForNewChannel', 'wm_hide');
		$oScreen->Data->SetValue('hideClassForEditChannel', '');

		/* @var $oChannel CChannel */
		$oChannel =& $this->oAdminPanel->GetMainObject('channel_edit');
		if ($oChannel)
		{
			$oScreen->Data->SetValue('intChannelId', $oChannel->IdChannel);
			$oScreen->Data->SetValue('txtLogin', $oChannel->Login);
			$oScreen->Data->SetValue('txtPassword', $oChannel->Password);
			$oScreen->Data->SetValue('txtDescription', $oChannel->Description);
		}
	}

	public function RealmsMainEdit(ap_Table_Screen &$oScreen)
	{
		$oScreen->Data->SetValue('hideClassForNewRealm', '');
		$oScreen->Data->SetValue('hideClassForEditRealm', 'wm_hide');

		/* @var $oRealm CRealm */
		$oRealm =& $this->oAdminPanel->GetMainObject('realm_edit');
		if ($oRealm)
		{
			$oScreen->Data->SetValue('intRealmId', $oRealm->IdRealm);
			$oScreen->Data->SetValue('txtLogin', $oRealm->Login);
			$oScreen->Data->SetValue('txtEmail', $oRealm->Email);
			$oScreen->Data->SetValue('txtDescription', $oRealm->Description);
			$oScreen->Data->SetValue('txtPassword',
				0 === strlen($oRealm->PasswordHash) ? '' : AP_DUMMYPASSWORD);

			$oScreen->Data->SetValue('chRealmEnabled', !$oRealm->IsDisabled);
			$oScreen->Data->SetValue('chEnableAdminLogin', $oRealm->IsEnableAdminPanelLogin);

			if (0 < $oRealm->IdChannel)
			{
				$oChannelsApi = CApi::Manager('channels');
				if ($oChannelsApi)
				{
					$oChannel = $oChannelsApi->GetChannelById($oRealm->IdChannel);
					if ($oChannel)
					{
						$oScreen->Data->SetValue('txtChannelAdd', '('.$oChannel->Login.')');
					}
				}
			}

			$sUserLimit = 'users: ';
			$sUserLimit .= $oRealm->GetUserCount().' used / ';
			$sUserLimit .= 0 === $oRealm->UserCountLimit ? 'unlim' : $oRealm->UserCountLimit;
			$sUserLimit .= ' max';

			$sDomainLimit = 'domains: ';
			$sDomainLimit .= $oRealm->GetDomainCount().' used / ';
			$sDomainLimit .= 0 === $oRealm->DomainCountLimit ? 'unlim' : $oRealm->DomainCountLimit;
			$sDomainLimit .= ' max';

			$oScreen->Data->SetValue('txtUserLimit', $sUserLimit);
			$oScreen->Data->SetValue('txtDomainsLimit', $sDomainLimit);
			$oScreen->Data->SetValue('txtQuota', $oRealm->QuotaInMB);

			$iUsed = 0;
			if (0 < $oRealm->QuotaInMB)
			{
				$iUsed = floor(($oRealm->AllocatedSpaceInMB / $oRealm->QuotaInMB) * 100);
				$oScreen->Data->SetValue('txtUsedText', '('.$iUsed.'% allocated)');
			}

			$aDomainsArray = $this->oModule->GetRealmDomains($oRealm->IdRealm);

			$sDomainOptions = '';
			if (is_array($aDomainsArray) && count($aDomainsArray) > 0)
			{
				foreach ($aDomainsArray as $iDomainId => $sDomainName)
				{
					$sDomainOptions .= '<option value="'.$iDomainId.'">'.$sDomainName.'</option>';
				}
			}

			$oScreen->Data->SetValue('selDomains', $sDomainOptions);
		}
	}

	public function ChannelsMainEdit(ap_Table_Screen &$oScreen)
	{
		$oScreen->Data->SetValue('hideClassForNewChannel', '');
		$oScreen->Data->SetValue('hideClassForEditChannel', 'wm_hide');

		/* @var $oChannel CChannel */
		$oChannel =& $this->oAdminPanel->GetMainObject('channel_edit');
		if ($oChannel)
		{
			$oScreen->Data->SetValue('intChannelId', $oChannel->IdChannel);
			$oScreen->Data->SetValue('txtLogin', $oChannel->Login);
			$oScreen->Data->SetValue('txtPassword', $oChannel->Password);
			$oScreen->Data->SetValue('txtDescription', $oChannel->Description);
		}
	}

	public function ServicesDav(ap_Standard_Screen &$oScreen)
	{
		/* @var $oApiDavManager CApiDavManager */
		$oApiDavManager = CApi::Manager('dav');
		if ($oApiDavManager)
		{
			$oScreen->Data->SetValue('ch_EnableMobileSync',
				(bool) $oApiDavManager->IsMobileSyncEnabled());

			$oScreen->Data->SetValue('text_DAVUrl', $this->oSettings->GetConf('WebMail/ExternalHostNameOfDAVServer'));
			$oScreen->Data->SetValue('text_IMAPHostName', $this->oSettings->GetConf('WebMail/ExternalHostNameOfLocalImap'));
			$oScreen->Data->SetValue('text_SMTPHostName', $this->oSettings->GetConf('WebMail/ExternalHostNameOfLocalSmtp'));
		}
	}

}
