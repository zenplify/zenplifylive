<?php

//$bDisabled = true;
$iSortIndex = 90;
$sCurrentModule = 'CBundleModule';
class CBundleModule extends ap_Module
{
	/**
	 * @var CApiUsersManager
	 */
	protected $oUsersApi;

	/**
	 * @var CApiDomainsManager
	 */
	protected $oDomainsApi;

	/**
	 * @var CApiMailsuiteManager
	 */
	public $oMailsuiteApi;

	/**
	 * @param CAdminPanel $oAdminPanel
	 * @param string $sPath
	 * @return CBundleModule
	 */
	public function __construct(CAdminPanel &$oAdminPanel, $sPath)
	{
		parent::__construct($oAdminPanel, $sPath);

		$this->oUsersApi = CApi::Manager('users');
		$this->oDomainsApi = CApi::Manager('domains');
		$this->oMailsuiteApi = CApi::Manager('mailsuite');

		$this->aTabs[] = AP_TAB_SERVICES;
		$this->aTabs[] = AP_TAB_DOMAINS;
		$this->aTabs[] = AP_TAB_USERS;
		$this->aTabs[] = AP_TAB_SYSTEM;

		$this->aQueryActions[] = 'new';
		$this->aQueryActions[] = 'edit';
		$this->aQueryActions[] = 'list';

		$this->oPopulateData = new CBundlePopulateData($this);
		$this->oStandardPostAction = new CBundlePostAction($this);
		$this->oStandardPopAction = new CBundlePopAction($this);
		$this->oTableAjaxAction = new CBundleAjaxAction($this);
	}

	/**
	 * @param CAdminPanel $oAdminPanel
	 */
	public function InitAdminPanel(CAdminPanel &$oAdminPanel)
	{
		$oAdminPanel->XType = true;
	}

	/**
	 * @param CMailingList &$oMailingList
	 * @return bool
	 */
	public function CreateMailingList(CMailingList &$oMailingList)
	{
		$bResult = false;
		if ($this->oMailsuiteApi)
		{
			$bResult = $this->oMailsuiteApi->CreateMailingList($oMailingList);
			if (!$bResult)
			{
				$this->lastErrorCode = $this->oMailsuiteApi->GetLastErrorCode();
				$this->lastErrorMessage = $this->oMailsuiteApi->GetLastErrorMessage();
			}
		}

		return $bResult;
	}

	/**
	 * @param CMailingList &$oMailingList
	 * @return bool
	 */
	public function DeleteMailingList(CMailingList &$oMailingList)
	{
		$bResult = false;
		if ($this->oMailsuiteApi)
		{
			$bResult = $this->oMailsuiteApi->DeleteMailingList($oMailingList);
			if (!$bResult)
			{
				$this->lastErrorCode = $this->oMailsuiteApi->GetLastErrorCode();
				$this->lastErrorMessage = $this->oMailsuiteApi->GetLastErrorMessage();
			}
		}

		return $bResult;
	}

	/**
	 * @param CMailingList &$oMailingList
	 * @return bool
	 */
	public function UpdateMailingList(CMailingList &$oMailingList)
	{
		$bResult = false;
		if ($this->oMailsuiteApi)
		{
			$bResult = $this->oMailsuiteApi->UpdateMailingList($oMailingList);
			if (!$bResult)
			{
				$this->lastErrorCode = $this->oMailsuiteApi->GetLastErrorCode();
				$this->lastErrorMessage = $this->oMailsuiteApi->GetLastErrorMessage();
			}
		}

		return $bResult;
	}

	/**
	 * @param CMailAliases &$oMailAliases
	 * @return bool
	 */
	public function UpdateMailAliases(CMailAliases &$oMailAliases)
	{
		$bResult = false;
		if ($this->oMailsuiteApi)
		{
			$bResult = $this->oMailsuiteApi->UpdateMailAliases($oMailAliases);
			if (!$bResult)
			{
				$this->lastErrorCode = $this->oMailsuiteApi->GetLastErrorCode();
				$this->lastErrorMessage = $this->oMailsuiteApi->GetLastErrorMessage();
			}
		}

		return $bResult;
	}

	/**
	 * @param CMailForwards &$oMailForwards
	 * @return bool
	 */
	public function UpdateMailForwards(CMailForwards &$oMailForwards)
	{
		$bResult = false;
		if ($this->oMailsuiteApi)
		{
			$bResult = $this->oMailsuiteApi->UpdateMailForwards($oMailForwards);
			if (!$bResult)
			{
				$this->lastErrorCode = $this->oMailsuiteApi->GetLastErrorCode();
				$this->lastErrorMessage = $this->oMailsuiteApi->GetLastErrorMessage();
			}
		}

		return $bResult;
	}

	/**
	 * @param int $iMailingListId
	 * @return CMailingList
	 */
	public function GetMailingList($iMailingListId)
	{
		if ($this->oMailsuiteApi && is_numeric($iMailingListId) && 0 < $iMailingListId)
		{
			return $this->oMailsuiteApi->GetMailingListById($iMailingListId);
		}
		return null;
	}

	/**
	 * @param CAccount $oAccount
	 * @return CMailAliases
	 */
	public function GetMailAliases($oAccount)
	{
		if ($oAccount && $this->oMailsuiteApi)
		{
			$oMailAliases = new CMailAliases($oAccount);
			$this->oMailsuiteApi->InitMailAliases($oMailAliases);
			return $oMailAliases;
		}
		return null;
	}

	/**
	 * @param CAccount $oAccount
	 * @return CMailForwards
	 */
	public function GetMailForwards($oAccount)
	{
		if ($oAccount && $this->oMailsuiteApi)
		{
			$oMailForwards = new CMailForwards($oAccount);
			$this->oMailsuiteApi->InitMailForwards($oMailForwards);
			return $oMailForwards;
		}
		return null;
	}

	/**
	 * @param int $iAccountId
	 * @return CAccount
	 */
	public function GetAccount($iAccountId)
	{
		if (is_numeric($iAccountId) && 0 < $iAccountId)
		{
			return $this->oUsersApi->GetAccountById($iAccountId);
		}
		return null;
	}

	/**
	 * @param int $iDomainId
	 * @return CDomain
	 */
	public function GetDomain($iDomainId)
	{
		if (0 < $iDomainId)
		{
			return $this->oDomainsApi->GetDomainById($iDomainId);
		}
		return null;
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initStandardMenuByTab($sTab, ap_Screen &$oScreen)
	{
		/*
		switch ($sTab)
		{
			case AP_TAB_SERVICES:

				$oScreen->AddMenuItem(BU_MODE_SMTP, BU_MODE_SMTP_NAME, $this->sPath.'/templates/smtp.php');
				$oScreen->AddMenuItem(BU_MODE_POP3IMAP, BU_MODE_POP3IMAP_NAME, $this->sPath.'/templates/pop3-imap.php');
				$oScreen->AddMenuItem(BU_MODE_LOGGING, BU_MODE_LOGGING_NAME, $this->sPath.'/templates/logging.php');
				$oScreen->AddMenuItem(BU_MODE_SPAM_VIRUS, BU_MODE_SPAM_VIRUS_NAME, $this->sPath.'/templates/spam-virus.php');

				$oScreen->AddMenuItem(BU_MODE_LDAP, BU_MODE_LDAP_NAME, $this->sPath.'/templates/ldap.php',
					array(
	'ttLdapHost' => '("LDAP_URI=ldap://host:port" in Courier) Host name or IP address of the LDAP server.',
	'ttLdapPort' => '("LDAP_URI=ldap://host:port" in Courier) Port the LDAP server listens to. If 386 (the default LDAP port), it\'s not appended to the URI.',
	'ttLdapProtocolVersion' => 'Protocol version of the LDAP server, most LDAP servers today will support version 3',
	'ttLdapBaseDN' => '(LDAP_BASEDN in Courier) The base DN from which the authentication should start from. The base DN can start from anywhere in the tree, and it should cover all the users that this module will authenticate for.',
	'ttLdapBindDN' => '(LDAP_BINDDN in Courier) This is the id to bind to the directory server as an administrator to read the ldap data. If the ACI is set properly allowing individual users to authenticate, this may not be necessary.',
	'ttLdapBindPw' => '(LDAP_BINDPW in Courier) The password to the id above.',
	'ttLdapFilter' => '(LDAP_FILTER in Courier) This optional filter will be ANDed with the query for the field defined above in LDAP_MAIL. Filter / mail',
	'ttLdapDomain' => 'Specifies which domain will be used for LDAP authentication. Only a domain hosted by this server can be used (no external domains here).',
	'ttLdapFullName' => '(LDAP_FULLNAME in Courier) The name (not the value!) of the LDAP field that the query will use to find the full name of the user.',
	'ttLdapMail' => '(LDAP_MAIL in Courier) The LDAP field name (not the value!) that the query will use to find the email address of the user.',
	'ttLdapClearPw' => '(LDAP_MAIL in Courier) The LDAP field name (not the value!) that the query will use to find the email address of the user.',
	'ttLdapCryptPw' => '(LDAP_CRYPTPW in Courier) The name (not the value!) of the LDAP field which stores the encrypted password.',
	'ttLdapHomeDir' => '(LDAP_HOMEDIR in Courier) The name (not the value!) of the LDAP field which stores the home directory of the user.'
					));

				$oScreen->SetDefaultMode(BU_MODE_SMTP, true);
				break;
		}
		*/
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableTopMenu($sTab, ap_Screen &$oScreen)
	{
		switch ($sTab)
		{
			case AP_TAB_USERS:
				$this->JsAddFile('users.js');

				$oScreen->AddTopMenuButton('New Mailing List', 'new_contact.gif', 'IdUsersNewMailingListButton',
						null, 'IdUsersNewUserButton');

				if (in_array((string) $oScreen->GetFilterIndex(), array('', '0')))
				{
					$oScreen->DeleteTopMenuButton('IdUsersNewMailingListButton');
					$oScreen->DeleteTopMenuButton('IdUsersNewUserButton');
					$oScreen->DeleteTopMenuButton('IdUsersEnableUserButton');
					$oScreen->DeleteTopMenuButton('IdUsersDisableUserButton');
					$oScreen->DeleteTopMenuButton('IdUsersDeleteButton');
				}
				break;
		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableMainSwitchers($sTab, ap_Screen &$oScreen)
	{
		$sMainAction = $this->getQueryAction();
		if (AP_TAB_DOMAINS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'edit':
					$oDomain =& /* @var $oDomain CDomain */ $this->oAdminPanel->GetMainObject('domain_edit');
					if ($oDomain && !$oDomain->IsDefaultDomain)
					{
						$oScreen->Main->AddSwitcher(
							BU_SWITCHER_MODE_EDIT_DOMAIN_GENERAL, BU_SWITCHER_MODE_EDIT_DOMAIN_GENERAL_NAME,
							$this->sPath.'/templates/main-edit-domain-general-sign-up.php');
					}
					break;
			}
		}
		else if (AP_TAB_USERS === $sTab)
		{
			switch ($sMainAction)
			{
				case 'list':
					$oScreen->Main->AddSwitcher(
						BU_SWITCHER_MODE_NEW_MAIL_LIST, BU_SWITCHER_MODE_NEW_MAIL_LIST_NAME,
						$this->sPath.'/templates/main-new-list.php');
					break;

				case 'edit':
					$oAccount =& $this->oAdminPanel->GetMainObject('account_edit');
					$iUid = isset($_GET['uid']) ? (int) $_GET['uid'] : null;

					if ($oAccount)
					{
						$this->oAdminPanel->SetMainObject('account_edit', $oAccount);

						$oScreen->Main->AddSwitcher(
							BU_SWITCHER_MODE_EDIT_USER_GENERAL, BU_SWITCHER_MODE_EDIT_USER_GENERAL_NAME,
								$this->sPath.'/templates/main-edit-user-general.php');

						if ($oAccount->Domain->IsInternal)
						{
							$oMailAliases =& $this->oAdminPanel->GetMainObject('aliases_edit');
							if (!$oMailAliases)
							{
								$oMailAliases = $this->GetMailAliases($oAccount);
								if ($oMailAliases)
								{
									$this->oAdminPanel->SetMainObject('aliases_edit', $oMailAliases);
								}
							}
							$oMailForwards =& $this->oAdminPanel->GetMainObject('forwards_edit');
							if (!$oMailForwards)
							{
								$oMailForwards = $this->GetMailForwards($oAccount);
								if ($oMailForwards)
								{
									$this->oAdminPanel->SetMainObject('forwards_edit', $oMailForwards);
								}
							}

							$oScreen->Main->AddSwitcher(
								BU_SWITCHER_MODE_EDIT_USER_ALIASES, BU_SWITCHER_MODE_EDIT_USER_ALIASES_NAME,
									$this->sPath.'/templates/main-edit-user-aliases.php');

//							$oScreen->Main->AddSwitcher(
//								BU_SWITCHER_MODE_EDIT_USER_FORWARDS, BU_SWITCHER_MODE_EDIT_USER_FORWARDS_NAME,
//									$this->sPath.'/templates/main-edit-user-forwards.php');
						}
					}

					if (!$oAccount)
					{
						$oMailingList =& $this->oAdminPanel->GetMainObject('mailinglist_edit');

						if (!$oMailingList && null !== $iUid && 0 < $iUid)
						{
							$oMailingList = $this->GetMailingList($iUid);
							if ($oMailingList)
							{
								$this->oAdminPanel->SetMainObject('mailinglist_edit', $oMailingList);

								$oScreen->Main->AddSwitcher(
									BU_SWITCHER_MODE_EDIT_LIST_GENERAL, BU_SWITCHER_MODE_EDIT_LIST_GENERAL_NAME,
									$this->sPath.'/templates/main-edit-list-general.php');
							}
						}
					}
					break;
				}
		}
	}

	/**
	 * @param string $sTab
	 * @param ap_Screen $oScreen
	 */
	protected function initTableMainSwitchersPost($sTab, ap_Screen &$oScreen)
	{
	}

	/**
	 * @return void
	 */
	protected function initInclude()
	{
		include $this->sPath.'/inc/constants.php';
		include $this->sPath.'/inc/populate.php';
		include $this->sPath.'/inc/post.php';
		include $this->sPath.'/inc/pop.php';
		include $this->sPath.'/inc/ajax.php';
	}
}
