<tr>
	<td width="100"></td>
	<td width="40"></td>
	<td></td>
</tr>
<tr>
	<td colspan="3" style="padding: 0px;">
		<div class="wm_safety_info">
			Anti-Virus & Spam
		</div>
	</td>
</tr>
<tr><td colspan="3"><br /></td></tr>
<tr>
	<td colspan="3" class="wm_settings_list_select">
		<b>Anti-Virus protection</b>
	</td>
</tr>
<tr><td colspan="3"><br /></td></tr>
<tr>
	<td align="right">
		<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableAntiVirus" id="ch_EnableAntiVirus" <?php $this->Data->PrintCheckedValue('ch_EnableAntiVirus') ?>/>
	</td>
	<td colspan="2">
		<label for="ch_EnableAntiVirus">Enable ClamAV</label>
	</td>
</tr>
<tr><td colspan="3"><br /></td></tr>
<tr>
	<td colspan="3" class="wm_settings_list_select">
		<b>Spam	 protection</b>
	</td>
</tr>
<tr><td colspan="3"><br /></td></tr>
<tr>
	<td align="right">
		<input type="checkbox" class="wm_checkbox" value="1"
		   name="ch_EnableSpamAssassin" id="ch_EnableSpamAssassin" <?php $this->Data->PrintCheckedValue('ch_EnableSpamAssassin') ?>/>
	</td>
	<td colspan="2">
		<label for="ch_EnableSpamAssassin">Enable Spam Assassin</label>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="checkbox" class="wm_checkbox" value="1"
			name="ch_UseExisting" id="ch_UseExisting" <?php $this->Data->PrintCheckedValue('ch_UseExisting') ?>/>
	</td>
	<td colspan="2">
		<label for="ch_UseExisting" id="ch_UseExisting_label">Use External Spam Assassin</label>
	</td>
</tr>
<tr>
	<td></td>
	<td>&nbsp;<span id="input_UseExistingHost_label">Host</span>:
	</td>
	<td>
		<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_UseExistingHost') ?>" name="input_UseExistingHost" id="input_UseExistingHost" />
	</td>
</tr>
<tr>
	<td></td>
	<td>&nbsp;<span id="input_UseExistingPort_label">Port</span>:
	</td>
	<td>
		<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_UseExistingPort') ?>" name="input_UseExistingPort" id="input_UseExistingPort" />
	</td>
</tr>
