	<tr>
		<td width="200"></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
				POP3 /IMAP
			</div>
		</td>
	</tr>
	<tr><td colspan="2"><br /></td></tr>
	<tr>
		<td colspan="2" class="wm_settings_list_select">
			<b>POP3 /IMAP</b>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnablePOP3" id="ch_EnablePOP3" <?php $this->Data->PrintCheckedValue('ch_EnablePOP3') ?>/>
			<label for="ch_EnablePOP3">Enable POP3</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Port
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_Pop3Port" id="text_Pop3Port" value="<?php $this->Data->PrintInputValue('text_Pop3Port') ?>" />
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableIMAP" id="ch_EnableIMAP" <?php $this->Data->PrintCheckedValue('ch_EnableIMAP') ?>/>
			<label for="ch_EnableIMAP">Enable IMAP</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Port
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_ImapPort" id="text_ImapPort" value="<?php $this->Data->PrintInputValue('text_ImapPort') ?>" />
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableSSLPOP3IMAP" id="ch_EnableSSLPOP3IMAP" <?php $this->Data->PrintCheckedValue('ch_EnableSSLPOP3IMAP') ?>/>
			<label for="ch_EnableSSLPOP3IMAP">Enable SSL POP3 / IMAP</label>
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<span id="text_SSLSertificatePath_label">SSL sertificate path</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="25" name="text_SSLSertificatePath" id="text_SSLSertificatePath" value="<?php $this->Data->PrintInputValue('text_SSLSertificatePath') ?>" />
		</td>
	</tr>
	<tr>
		<td align="left">
			<span id="text_SSLKeyPath_label">SSL key path</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="25" name="text_SSLKeyPath" id="text_SSLKeyPath" value="<?php $this->Data->PrintInputValue('text_SSLKeyPath') ?>" />
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>
	
	<tr>
		<td align="left">
			<span>Default domain</span>
		</td>
		<td align="left">
			<select class="wm_input" name="text_Pop3ImapDefaultDomain" id="text_Pop3ImapDefaultDomain"></select>
		</td>
	</tr>

	<tr>
		<td align="left">
			<span>
				Maximum POP3/IMAP process
			</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumPOP3IMAPprocess" id="text_MaximumPOP3IMAPprocess" value="<?php $this->Data->PrintInputValue('text_MaximumPOP3IMAPprocess') ?>" />
		</td>
	</tr>

	<tr>
		<td align="left">
			<span>
				Maximum connection per IP
			</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumConnectionPerIP" id="text_MaximumConnectionPerIP" value="<?php $this->Data->PrintInputValue('text_MaximumConnectionPerIP') ?>" />
		</td>
	</tr>
