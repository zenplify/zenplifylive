	<tr>
		<td width="200"></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
				SMTP
			</div>
		</td>
	</tr>
	<tr><td colspan="2"><br /></td></tr>
	<tr>
		<td colspan="2" class="wm_settings_list_select">
			<b>SMTP</b>
		</td>
	</tr>
	<tr><td colspan="2"><br /></td></tr>
	<tr>
		<td align="left">
			<span>SMTP port</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_SMTPport" id="text_SMTPport" <?php $this->Data->PrintCheckedValue('text_SMTPport') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<span>SSL port</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_SSLport" id="text_SSLport" <?php $this->Data->PrintCheckedValue('text_SSLport') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableSMTPauth" id="ch_EnableSMTPauth" <?php $this->Data->PrintCheckedValue('ch_EnableSMTPauth') ?>/>
			<label for="ch_EnableSMTPauth">Enable SMTP auth</label>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_VerifyReverseDNS" id="ch_VerifyReverseDNS" <?php $this->Data->PrintCheckedValue('ch_VerifyReverseDNS') ?>/>
			<label for="ch_VerifyReverseDNS">Verify reverse DNS</label>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left">
			<span>Maximum message size</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumMessageSize" id="text_MaximumMessageSize" <?php $this->Data->PrintCheckedValue('text_MaximumMessageSize') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<span>Maximum concurrent connections</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumConcurrentConnections" id="text_MaximumConcurrentConnections" <?php $this->Data->PrintCheckedValue('text_MaximumConcurrentConnections') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<span>Maximum connections per IP</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumConnectionsPerIP" id="text_MaximumConnectionsPerIP" <?php $this->Data->PrintCheckedValue('text_MaximumConnectionsPerIP') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<span>Bounce message size</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_BounceMessageSize" id="text_BounceMessageSize" <?php $this->Data->PrintCheckedValue('text_BounceMessageSize') ?>/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<span>Maximum messages per connection</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumMessagesPerConnection" id="text_MaximumMessagesPerConnection" <?php $this->Data->PrintCheckedValue('text_MaximumMessagesPerConnection') ?>/>
		</td>
	</tr>

	<tr>
		<td align="left">
			<span>Maximum recipients per message</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="5" name="text_MaximumRecipientsPerMessage" id="text_MaximumRecipientsPerMessage" <?php $this->Data->PrintCheckedValue('text_MaximumRecipientsPerMessage') ?>/>
		</td>
	</tr>

	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_LoadReserve" id="ch_LoadReserve" <?php $this->Data->PrintCheckedValue('ch_LoadReserve') ?>/>
			<label for="ch_LoadReserve">Load reserve</label>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_QueueLoad" id="ch_QueueLoad" <?php $this->Data->PrintCheckedValue('ch_QueueLoad') ?>/>
			<label for="ch_QueueLoad">Queue load</label>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_QueueDeliveryLoad" id="ch_QueueDeliveryLoad" <?php $this->Data->PrintCheckedValue('ch_QueueDeliveryLoad') ?>/>
			<label for="ch_QueueDeliveryLoad">Queue delivery load</label>
		</td>
	</tr>

	<tr><td colspan="2"><br /></td></tr>

	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableDKIMinbound" id="ch_EnableDKIMinbound" <?php $this->Data->PrintCheckedValue('ch_EnableDKIMinbound') ?>/>
			<label for="ch_EnableDKIMinbound">Enable DKIM inbound</label>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<input type="checkbox" class="wm_checkbox" value="1" name="ch_EnableDKIMoutbound" id="ch_EnableDKIMoutbound" <?php $this->Data->PrintCheckedValue('ch_EnableDKIMoutbound') ?>/>
			<label for="ch_EnableDKIMoutbound">Enable DKIM outbound</label>
		</td>
	</tr>

	<tr>
		<td align="left">
			<span>DKIM domain</span>
		</td>
		<td align="left">
			<input type="text" class="wm_input" size="20" name="text_DKIMdomain" id="text_DKIMdomain" value="<?php $this->Data->PrintInputValue('text_DKIMdomain') ?>" />
		</td>
	</tr>

	<tr>
		<td align="left">
			<span>DKIM signature</span>
		</td>
		<td align="left">
			<textarea class="wm_input" cols="19" rows="5" name="text_DKIMsignature" id="text_DKIMsignature"><?php
			$this->Data->PrintValue('text_DKIMsignature');
			?></textarea>
		</td>
	</tr>