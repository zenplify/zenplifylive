	<tr>
		<td width="100"></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 0px;">
			<div class="wm_safety_info">
Here you can configure MailSuite to use LDAP instead of the internal SQL database to authenticate users and perform
auto-provisioning mail accounts for them.
<br /><br />
If LDAP is enabled for certain domain, MailSuite will query the LDAP server
for the specified username/password. If the LDAP server confirms this username/password denotes a valid user, MailSuite will create the
mail account if it does not exist or log in if the account is not created.
			</div>
		</td>
	</tr>
	<tr>
		<td></td><td><br /></td>
	</tr>
	<tr>
		<td colspan="2" class="wm_settings_list_select">
			<b>LDAP Options</b>
		</td>
	</tr>
	<tr><td></td><td><br /></td></tr>

	<tr class="<?php $this->Data->PrintInputValue('classNoDomains') ?>">
		<td align="left" colspan="2">
			Prior to configuring which domain to authenticate via LDAP, you should create at least one domain first.
		</td>
	</tr>

	<tr class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span class="LabelOffset">Connection type</span>:
		</td>
		<td>
			<select id="select_Ldap" name="select_Ldap">
				<option value="disable" <?php $this->Data->PrintSelectedValue('select_Ldap_disable') ?> >SQL database (no LDAP)</option>
				<option value="open_ldap" <?php $this->Data->PrintSelectedValue('select_Ldap_open_ldap') ?> >OpenLDAP</option>
				<option value="active_directory" <?php $this->Data->PrintSelectedValue('select_Ldap_active_directory') ?> >Active Directory</option>
				<option value="other_ldap" <?php $this->Data->PrintSelectedValue('select_Ldap_other_ldap') ?> >Other LDAP</option>
			</select>
		</td>
	</tr>
	<tr id="tr_LdapServer" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapHost">Host</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapServer') ?>"
				name="input_LdapServer" id="input_LdapServer" />
		</td>
	</tr>

	<tr id="tr_LdapPort" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapPort">Port</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapPort') ?>"
				name="input_LdapPort" id="input_LdapPort" />
		</td>
	</tr>

	<tr id="tr_LdapProtocol" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapProtocolVersion">Protocol version</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapProtocol') ?>"
				name="input_LdapProtocol" id="input_LdapProtocol" size="10" />
		</td>
	</tr>

	<tr id="tr_LdapBaseDn" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapBaseDN">Base DN</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapBaseDn') ?>"
				name="input_LdapBaseDn" id="input_LdapBaseDn" size="50" />
		</td>
	</tr>

	<tr id="tr_LdapBindDn" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapBindDN">Bind DN</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapBindDn') ?>"
				name="input_LdapBindDn" id="input_LdapBindDn" size="50" />
		</td>
	</tr>

	<tr id="tr_LdapBindPw" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapBindPw">Bind password</span>:
		</td>
		<td>
			<input type="password" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapBindPw') ?>"
				name="input_LdapBindPw" id="input_LdapBindPw" />
		</td>
	</tr>

	<tr id="tr_LdapFilter" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapFilter">Filter</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapFilter') ?>"
				name="input_LdapFilter" id="input_LdapFilter" />
		</td>
	</tr>

	<tr id="tr_LdapDomain" class="wm_hide <?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapDomain">Domain</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapDomain') ?>"
				name="input_LdapDomain" id="input_LdapDomain" />
		</td>
	</tr>

	<tr id="tr_LdapDomains" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapDomain">Domain</span>:
		</td>
		<td>
			<?php $this->Data->PrintValue('select_LdapDomains') ?>
		</td>
	</tr>

	<tr id="tr_LdapFullName" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapFullName">Full name</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapFullName') ?>"
				name="input_LdapFullName" id="input_LdapFullName" />
		</td>
	</tr>

	<tr id="tr_LdapMail" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapMail">Mail</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapMail') ?>"
				name="input_LdapMail" id="input_LdapMail" />
		</td>
	</tr>

	<tr id="tr_LdapClearPw" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapClearPw">Clear password</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapClearPw') ?>"
				name="input_LdapClearPw" id="input_LdapClearPw" />
		</td>
	</tr>

	<tr id="tr_LdapCryptPw" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span id="ttLdapCryptPw">Crypt password</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapCryptPw') ?>"
				name="input_LdapCryptPw" id="input_LdapCryptPw" />
		</td>
	</tr>

	<tr id="tr_LdapHomeDir" class="<?php $this->Data->PrintInputValue('classDomainsExist') ?>">
		<td align="right">
			<span class="LabelOffset" id="ttLdapHomeDir" >Home dir</span>:
		</td>
		<td>
			<input type="text" class="wm_input" value="<?php $this->Data->PrintInputValue('input_LdapHomeDir') ?>"
				name="input_LdapHomeDir" id="input_LdapHomeDir" />
		</td>
	</tr>
