<table class="wm_contacts_view">
	<tr>
		<td align="left" width="100">
			<nobr>Password *</nobr>
		</td>
		<td align="left">
			<input name="txtEditPassword" type="password" id="txtEditPassword" class="wm_input"
				style="width: 150px" maxlength="100" value="<?php $this->Data->PrintInputValue('txtEditPassword') ?>" />
		</td>
	</tr>
	<tr>
		<td align="left" width="100">
			<nobr>Storage quota</nobr>
		</td>
		<td align="left">
			<input name="txtEditStorageQuota" type="text" id="txtEditStorageQuota" class="wm_input"
				style="width: 150px" maxlength="9" value="<?php $this->Data->PrintInputValue('txtEditStorageQuota') ?>" />
			KB
			(0 for unlimited)
		</td>
	</tr>
	<tr>
		<td align="left" width="100">
			<nobr>Used space</nobr>
		</td>
		<td align="left">
			<?php $this->Data->PrintValue('txtUsedSpaceDesc') ?>
		</td>
	</tr>
</table>