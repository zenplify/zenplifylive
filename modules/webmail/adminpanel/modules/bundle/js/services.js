function InitDKIMOutbound()
{
	SetDisabledArray('ch_EnableDKIMoutbound', ['text_DKIMdomain', 'text_DKIMsignature']);
}

function InitEnableSSLforPOP3IMAP()
{
	SetDisabledArray('ch_EnableSSLPOP3IMAP', ['text_SSLSertificatePath', 'text_SSLKeyPath']);
}

function InitMobileCheck()
{
	var chEnableSpam = document.getElementById('ch_EnableSpamAssassin');
	var chUseExist = document.getElementById('ch_UseExisting');
	if (chEnableSpam && chEnableSpam && !chEnableSpam.checked) {
		chUseExist.checked = false;
	}
	SetDisabledArray('ch_EnableSpamAssassin', ['ch_UseExisting', 'input_UseExistingHost', 'input_UseExistingPort']);
	SetDisabledArray('ch_UseExisting', ['input_UseExistingHost', 'input_UseExistingPort']);
}

function InitLdapSelect(setDefault) {
	var i, c, select, value, tr, trs = [
		'tr_LdapUri', 'tr_LdapServer', 'tr_LdapPort', 'tr_LdapProtocol', 'tr_LdapBaseDn',
		'tr_LdapBindDn', 'tr_LdapBindPw', 'tr_LdapFilter', 'tr_LdapDomains',
		'tr_LdapFullName', 'tr_LdapMail', 'tr_LdapClearPw', 'tr_LdapCryptPw', 'tr_LdapHomeDir'
	];
	var trs_ldap = [
		'tr_LdapServer', 'tr_LdapBaseDn', 'tr_LdapBindDn', 'tr_LdapBindPw', 'tr_LdapDomains'
	];

	for (i = 0, c = trs.length; i < c; i++) {
		tr = document.getElementById(trs[i]);
		if (tr) {
			tr.className = 'wm_hide';
		}
	}

	select = document.getElementById('select_Ldap');
	if (select) {
		value = select.value;
		if ('open_ldap' === value || 'active_directory' === value) {
			for (i = 0, c = trs_ldap.length; i < c; i++) {
				tr = document.getElementById(trs_ldap[i]);
				if (tr) {
					tr.className = '';
				}
			}

			if (setDefault) {
				if ('open_ldap' === value) {
					document.getElementById('input_LdapServer').value = '127.0.0.1';
					document.getElementById('input_LdapBaseDn').value = 'dc=example,dc=com';
					document.getElementById('input_LdapBindDn').value = 'cn=root,cn=Users,dc=example,dc=com';
					document.getElementById('input_LdapBindPw').value = '';
				} else {
					document.getElementById('input_LdapServer').value = '127.0.0.1';
					document.getElementById('input_LdapBaseDn').value = 'cn=Users,dc=example,dc=com';
					document.getElementById('input_LdapBindDn').value = 'cn=Administrator,cn=Users,dc=example,dc=com';
					document.getElementById('input_LdapBindPw').value = '';
				}
			}
		} else if ('other_ldap' === value) {
			for (i = 0, c = trs.length; i < c; i++) {
				tr = document.getElementById(trs[i]);
				if (tr) {
					tr.className = '';
				}
			}
		}
	}
}

$(function(){
	$('#ch_EnableDKIMoutbound').change(function(){
		InitDKIMOutbound();
	});
	$('#ch_EnableSSLPOP3IMAP').change(function(){
		InitEnableSSLforPOP3IMAP();
	});
	$('#ch_EnableSpamAssassin').click(function(){
		InitMobileCheck();
	});
	$('#ch_UseExisting').click(function(){
		InitMobileCheck();
	});
	$('#select_Ldap').change(function(){
		InitLdapSelect(true);
	});

	InitDKIMOutbound();
	InitEnableSSLforPOP3IMAP();
	InitMobileCheck();
	InitLdapSelect(false);
});
