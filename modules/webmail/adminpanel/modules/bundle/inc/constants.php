<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 * 
 */

	define('BU_MODE_SMTP', 'smtp');
	define('BU_MODE_POP3IMAP', 'pop3imap');
	define('BU_MODE_LOGGING', 'logging');
	define('BU_MODE_SPAM_VIRUS', 'spamvirus');
	define('BU_MODE_LDAP', 'ldap');

	define('BU_SWITCHER_MODE_EDIT_DOMAIN_GENERAL', 'editgeneral');
	define('BU_SWITCHER_MODE_EDIT_USER_GENERAL', 'editgeneral');
	define('BU_SWITCHER_MODE_EDIT_USER_ALIASES', 'editaliases');
	define('BU_SWITCHER_MODE_EDIT_USER_FORWARDS', 'editforwards');
	define('BU_SWITCHER_MODE_EDIT_LIST_GENERAL', 'editlist');
	define('BU_SWITCHER_MODE_NEW_MAIL_LIST', 'newmaillist');

	/* langs */

	define('BU_MODE_SMTP_NAME', 'SMTP');
	define('BU_MODE_POP3IMAP_NAME', 'POP3 / IMAP');
	define('BU_MODE_LOGGING_NAME', 'Logging');
	define('BU_MODE_SPAM_VIRUS_NAME', 'Spam & Anti-Virus');
	define('BU_MODE_LDAP_NAME', 'LDAP');

	define('BU_SWITCHER_MODE_EDIT_DOMAIN_GENERAL_NAME', 'General');
	define('BU_SWITCHER_MODE_EDIT_USER_GENERAL_NAME', 'General');
	define('BU_SWITCHER_MODE_EDIT_USER_ALIASES_NAME', 'Aliases');
	define('BU_SWITCHER_MODE_EDIT_USER_FORWARDS_NAME', 'Forwards');
	define('BU_SWITCHER_MODE_EDIT_LIST_GENERAL_NAME', 'General');
	define('BU_SWITCHER_MODE_NEW_MAIL_LIST_NAME', 'New Mailing List');