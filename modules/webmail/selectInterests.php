<?php 
session_start();
error_reporting(0);
include_once("../../classes/init.php");
require_once('../../classes/messages.php');
$database=new database();
$messages=new messages();
$interestArray = array();


if(isset($_SESSION['selectedInterest']) && $_SESSION['selectedInterest']!='')
{
	$interestArray =array_diff($_POST['interests'],$_SESSION['selectedInterest']);
}
else
{
	$interestArray = $_POST['interests'];
}
foreach($interestArray as $rowGroup)
{
	switch ($rowGroup) {
		case 'Host':
			$interestId = 50;
			break;
		case 'Client':
			$interestId = 51;
			break;
		case 'Independent Consultant':
			$interestId = 52;
			break;
	}
	echo '<li class="listClass" id="'.$interestId.'" style=" width:250px;">';
	echo '<input type="hidden" name="interestId[]" id="interestId" value="'.$interestId.'"/>';
	echo'<label>'.$rowGroup.'</label><img src="../../images/delete.png" onclick="delInterest('.$interestId.')" class="delit" id="delit'.$interestId.'" style="float:right;"></li>';
}
?>