<?php 
session_start();
error_reporting(1);
include_once("../../classes/init.php");
require_once('../../classes/messages.php');
require_once('../../classes/contact.php');
$database=new database();
$messages=new messages();
$contact=new contact();
$userId=$_SESSION['userId'];
$groups=implode(',',$_POST['groupId']);

$groupData=$contact->GetGroupContacts($userId,$groups);


if(!empty($groups))
{
	$i=0;
foreach ($groupData as $rowGroup)
{
		$status=$messages->getContactRequestStatus($database,$rowGroup->contactId);
		
		if($status=='Subscribed')
		{
			$email=$messages->showContactEmailAddress($database,$rowGroup->contactId);
			
			$emails[$i]=$email;
			$i++;
		}
}
if(!empty($emails))
{
	$emailIds=implode(',',$emails);	
}
else
{
	$emailIds='';
}
echo $emailIds;	
$_SESSION['toemails']=$emailIds;		
}
else
{
	echo '';
}
?>