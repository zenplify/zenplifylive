<?php 
session_start();
$_SESSION['selectedGroup']='';
error_reporting(0);
include_once("../../classes/init.php");
require_once('../../classes/messages.php');
require_once('../../classes/contact.php');
$database=new database();
$messages=new messages();
$contact=new contact();
$userId=$_SESSION['userId'];
$groups=implode(',',$_POST['groupId']);
$_SESSION['selectedGroup']=$_POST['groupId'];
$groupData=$contact->GetGroupContacts($userId,$groups);

$unprocess=0;
$total=sizeof($groupData);
$pending=0;
$sub=0;
$unsub=0;
$bounce=0;
$unsent=0;

if(!empty($groups))
{
foreach ($groupData as $rowGroup)
{
						
		$status=$messages->getContactRequestStatus($database,$rowGroup->contactId);
		if($status=='Unprocessed')
		{
			$unprocess++;
			
		}
		if($status=='Pending')
		{
			$pending++;
			
		}
		if($status=='Subscribed')
		{
			$sub++;
			
		}
		if($status=='Unsubscribed')
		{
			$unsub++;
			
		}
		if($status=='Bounced')
		{
			$bounce++;
			
		}
		if($status=='')
		{
			$unsent++;
			
		}
		$notsent=$unsent+$unprocess+$unsub+$bounce;
}
				echo '<table width="100%" border="0">
			<tbody>
				<tr>
					<td valign="top" align="left" width="40%">
						<span class="stepMarker" style="color: #555; margin: 0px; font-weight:bold; font-size:14px;">'.$sub.'</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="posEmailPlural">s</span> Will Be Sent</span>
					</td>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px;font-weight:bold; font-size:14px;">'.$notsent.'</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="negEmailPlural">s</span> Won\'t Be Sent</span>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left" class="sectionList">
					</td>
					<td valign="top" align="left" class="sectionList">
						<table cellspacing="3" border="0" id="showNumbersPerOptStatus" width="70%">
							<tbody><tr>
								<td align="right" class="label">
									<label>Unprocessed</label>
								</td>
								<td class="label">'.$unprocess.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Pending Approval</label>
								</td>
								<td class="label">'.$pending.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Unsubscribed</label>
								</td>
								<td class="label">'.$unsub.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Bounced</label>
								</td>
								<td class="label">'.$bounce.'</td>
							</tr>
						</tbody></table>
						<input type="button" onclick="goToPermissionPage()" class="permissionManager" value="">
					</td>
				</tr>
			</tbody>
		</table>';
}
else
{
	echo '<table width="100%" border="0">
			<tbody>
				<tr>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px; font-weight:bold; font-size:14px;">0</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="posEmailPlural">s</span> Will Be Sent</span>
					</td>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px;font-weight:bold; font-size:14px;">0</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="negEmailPlural">s</span> Won\'t Be Sent</span>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left" class="sectionList">
					</td>
					<td valign="top" align="left" class="sectionList">
						<table cellspacing="3" border="0" id="showNumbersPerOptStatus">
							<tbody><tr>
								<td align="right" class="label">
									<label>Unprocessed</label>
								</td>
								<td class="label">'.$unprocess.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Pending Approval</label>
								</td>
								<td class="label">'.$pending.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Unsubscribed</label>
								</td>
								<td class="label">'.$unsub.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Bounced</label>
								</td>
								<td class="label">'.$bounce.'</td>
							</tr>
						</tbody></table>
						<input type="button" onclick="goToPermissionPage()" class="permissionManager" value="">
					</td>
				</tr>
			</tbody>
		</table>';
}
?>