<?php
// Example of logging into WebMail account using email and password for incorporating into another web application
error_reporting(0);
include_once("../../classes/init.php");
include_once("../../classes/messages.php");
include_once("encryption_class.php");
$encryption=new encryption_class();
$database= new database();
 $messgaes=new messages();
// determining main directory
defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'\\'));
 
// utilizing WebMail Pro API
//include_once WM_ROOTPATH.'libraries\afterlogic\api.php';
include_once 'libraries/afterlogic/api.php';
if (class_exists('CApi') && CApi::IsValid())
{
	
	// Getting required API class
  $oApiDomainsManager = CApi::Manager('domains');
  
  // Creating domain object
  $oDomain = new CDomain($_POST['domain']);
 if($_POST['domain']!='' && $_POST['incoming']!='' && $_POST['outgoing'])
  {
  // Additional modification of domain details
	  $oDomain->IncomingMailProtocol = EMailProtocol::IMAP4;
	  $oDomain->IncomingMailServer = $_POST['incoming'];
	  $oDomain->OutgoingMailServer = $_POST['outgoing'];
	 
	  if ($oApiDomainsManager->CreateDomain($oDomain))
	  {
		echo 'Domain '.$oDomain->Name.' is created successfully.';
	  }
	  else
	  {
		echo $oApiDomainsManager->GetLastErrorMessage();
	  }
  }
  else
  {
	  
		  $oApiDomainsManager = CApi::Manager('domains');
	 
	  // Creating domain object
		  $oDomain = new CDomain('gmail.com');
	 
  // Additional modification of domain details
  		  $oDomain->IncomingMailProtocol = EMailProtocol::IMAP4;
		  $oDomain->IncomingMailServer = 'pop.gmail.com';
		  $oDomain->OutgoingMailServer = 'smtp.gmail.com';
		 
		  if ($oApiDomainsManager->CreateDomain($oDomain))
		  {
			echo 'Domain '.$oDomain->Name.' is created successfully.';
		  }
		  else
		  {
			echo $oApiDomainsManager->GetLastErrorMessage();
		  }
	}
  // data for logging into account
  $sEmail = $_POST['email'];
  $sPassword = $_POST['password'];
 
  // Getting required API class
  $oApiWebMailManager = CApi::Manager('webmail');
 
  // attempting to obtain object for account we're trying to log into
  $oAccount = $oApiWebMailManager->LoginToAccount($sEmail, $sPassword);
  if ($oAccount)
  {
    // populating session data from the account
    $oAccount->FillSession();
 
    // redirecting to WebMail
    //$oApiWebMailManager->JumpToWebMail('../webmail/webmail.php?check=1');
	$enc_pass=$encryption->encrypt('abc*def',$sPassword,16);
	$errors = $encryption->errors;
	$update=$messgaes->saveEncPassword($database,$sEmail,$enc_pass);
	header('Location: messages.php?update='.$update.'');
  }
  else
  {
    // login error
	$error=$oApiWebMailManager->GetLastErrorMessage();
	header('Location: messages.php?error='.$error.'');
    //echo $oApiWebMailManager->GetLastErrorMessage();
  }
}
else
{
  echo 'WebMail API not allowed';
}