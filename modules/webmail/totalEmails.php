<?php 
session_start();
$_SESSION['selectedGroup']='';
error_reporting(0);
include_once("../../classes/init.php");
require_once('../../classes/messages.php');
require_once('../../classes/contact.php');
$database=new database();
$messages=new messages();
$contact=new contact();

$userId=$_SESSION['userId'];
$groups=implode(',',$_POST['groupId']);
$interests=implode(',',$_POST['interestId']);

$_SESSION['selectedGroup']=$_POST['groupId'];

$interests = str_replace('50','Host',$interests);
$interests = str_replace('51','Client',$interests);
$interests = str_replace('52','Independent Consultant',$interests);

if(strstr($interests,'Independent Consultant'))
{
	$interests = $interests.',leader,consultant';	
}
$_SESSION['selectedInterest']=explode(',',$interests);


$groupData = $contact->GetGroupContacts($userId,$groups);
$interestData = $contact->GetlevelContacts($userId,$interests);

$unprocess=0;
$total=sizeof($groupData);
$pending=0;
$sub=0;
$unsub=0;
$bounce=0;
$unsent=0;
$i=0;
$j=0;
$emails = array();
$nonemails = array();

if((!empty($groups)) || (!empty($interests)))
{
	if(!empty($groups)){
		foreach ($groupData as $rowGroup)
		{
			$status=$messages->getContactRequestStatus($database,$rowGroup->contactId);
			//$email=$messages->showContactEmailAddress($database,$rowGroup->contactId);
			$email=$rowGroup->email;
			if($status=='Unprocessed')
			{
				$j++;
				$nonemails[$j] = $email;
				$unprocess++;
			}
			if($status=='Pending')
			{
				$j++;
				$nonemails[$j] = $email;
				$pending++;
			}
			if($status=='Subscribed')
			{
				$i++;
				$emails[$i] = $email;
				$sub++;
			}
			if($status=='Unsubscribed')
			{
				$j++;
				$nonemails[$j] = $email;
				$unsub++;
			}
			if($status=='Bounced')
			{
				$j++;
				$nonemails[$j] = $email;
				$bounce++;
			}
			if($status=='')
			{
				$j++;
				$nonemails[$j] = $email;
				$unsent++;
			}
			$notsent=$unsent+$unprocess+$unsub+$bounce;}
	}

	if(!empty($interests)){
		foreach ($interestData as $rowInterest)
		{
			$status=$messages->getContactRequestStatus($database,$rowInterest->contactId);
			//$email=$messages->showContactEmailAddress($database,$rowInterest->contactId);
			$email=$rowInterest->email;
			if($status=='Unprocessed')
			{
				if(!(in_array($email,$nonemails))){
					$unprocess++;
					$j++;
					$nonemails[$j] = $email;
				}
			}
			if($status=='Pending')
			{
				if(!(in_array($email,$nonemails))){
					$pending++;
					$j++;
					$nonemails[$j] = $email;
				}
			}
			if($status=='Subscribed')
			{
				if(!(in_array($email,$emails))){
					$i++;
					$emails[$i]=$email;
					$sub++;
				}
			}
			if($status=='Unsubscribed')
			{
				if(!(in_array($email,$nonemails))){
					$unsub++;
					$j++;
					$nonemails[$j] = $email;
				}
			}
			if($status=='Bounced')
			{
				if(!(in_array($email,$nonemails))){
					$bounce++;
					$j++;
					$nonemails[$j] = $email;
				}				
			}
			if($status=='')
			{
				if(!(in_array($email,$nonemails))){
					$unsent++;
					$j++;
					$nonemails[$j] = $email;
				}
			}
			$notsent=$unsent+$unprocess+$unsub+$bounce;}
	}
	echo '<table width="100%" border="0">
			<tbody>
				<tr>
					<td valign="top" align="left" width="40%">
						<span class="stepMarker" style="color: #555; margin: 0px; font-weight:bold; font-size:14px;">'.$sub.'</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="posEmailPlural">s</span> Will Be Sent</span>
					</td>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px;font-weight:bold; font-size:14px;">'.$notsent.'</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="negEmailPlural">s</span> Won\'t Be Sent</span>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left" class="sectionList">
					</td>
					<td valign="top" align="left" class="sectionList">
						<table cellspacing="3" border="0" id="showNumbersPerOptStatus" width="70%">
							<tbody><tr>
								<td align="right" class="label">
									<label>Unprocessed</label>
								</td>
								<td class="label">'.$unprocess.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Pending Approval</label>
								</td>
								<td class="label">'.$pending.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Unsubscribed</label>
								</td>
								<td class="label">'.$unsub.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Bounced</label>
								</td>
								<td class="label">'.$bounce.'</td>
							</tr>
						</tbody></table>
						<input type="button" onclick="goToPermissionPage()" class="permissionManager" value="">
					</td>
				</tr>
			</tbody>
		</table>';
}
else
{
	echo '<table width="100%" border="0">
			<tbody>
				<tr>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px; font-weight:bold; font-size:14px;">0</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="posEmailPlural">s</span> Will Be Sent</span>
					</td>
					<td valign="top" align="left">
						<span class="stepMarker" style="color: #555; margin: 0px;font-weight:bold; font-size:14px;">0</span>
						<span class="stepDesc" style="font-weight:bold;color: #555; font-size:14px;">Email<span id="negEmailPlural">s</span> Won\'t Be Sent</span>
					</td>
				</tr>
				<tr>
					<td valign="top" align="left" class="sectionList">
					</td>
					<td valign="top" align="left" class="sectionList">
						<table cellspacing="3" border="0" id="showNumbersPerOptStatus">
							<tbody><tr>
								<td align="right" class="label">
									<label>Unprocessed</label>
								</td>
								<td class="label">'.$unprocess.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Pending Approval</label>
								</td>
								<td class="label">'.$pending.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Unsubscribed</label>
								</td>
								<td class="label">'.$unsub.'</td>
							</tr>
							<tr>
								<td align="right" class="label">
									<label>Bounced</label>
								</td>
								<td class="label">'.$bounce.'</td>
							</tr>
						</tbody></table>
						<input type="button" onclick="goToPermissionPage()" class="permissionManager" value="">
					</td>
				</tr>
			</tbody>
		</table>';
}
?>