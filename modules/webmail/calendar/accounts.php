<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 * 
 */

	class AccountDiv
	{
		/**
		 * @var array
		 */
		var $accounts;
		
		/**
		 * @var int
		 */
		var $accountId;
		
		/**
		 * @var string
		 */
		var $skin;
		
		/**
		 * @param CAccount $oAccount
		 * @return AccountDiv
		 */
		function AccountDiv($oAccount)
		{
			$this->accountId = $oAccount->IdAccount;
			$this->skin = $oAccount->User->DefaultSkin;
			
			$aAccounts = AppGetAccounts($oAccount);
			$this->accounts = array();
			if (count($aAccounts)> 0)
			{
				foreach ($aAccounts as $oAccount)
				{
					$this->accounts[$oAccount->IdAccount] = $oAccount->Email;
				}
			}
		}
		
		/**
		 * @return int
		 */
		function Count()
		{
			return count($this->accounts);
		}
		
		/**
		 * @return string
		 */
		function doTitle()
		{
			$single = ($this->Count() < 2);
			$class = $single ? 'wm_accountslist_selection_none' : 'wm_accountslist_selection wm_control';
	
			$temp = ($single) ? '' :
'onmouseover="this.className=\'wm_accountslist_selection_over wm_control\'"
onmousedown="this.className=\'wm_accountslist_selection_press wm_control\'"
onmouseup="this.className=\'wm_accountslist_selection_over wm_control\'"
onmouseout="this.className=\'wm_accountslist_selection wm_control\'"';
			
			return '
				<span class="wm_accountslist_email" id="popup_replace_1" style="z-index:14;">
					<a href="#" onclick="parent.HideCalendar(\'account\'); return false;">'.$this->accounts[$this->accountId].'</a>
				</span>
				<div class="'.$class.'" id="popup_control_1" style="z-index:14;"
'.$temp.'
				></div>';
			
		}
		
		/**
		 * @return string
		 */
		function ToHideDiv()
		{
			$out = '';
			if ($this->Count() > 1)
			{
				$out .= '<div class="wm_hide" id="popup_menu_1" style="z-index:13;">';
				foreach ($this->accounts as $acctId => $email)
				{
					if ($this->accountId != $acctId)
					{
						$out .= '<div><div class="wm_account_item" onclick="parent.HideCalendar(\'account\', '.$acctId.'); return false;"
								onmouseover="this.className=\'wm_account_item_over\';" onmouseout="this.className=\'wm_account_item\';">'.$email.'</div></div>';
					}
				}
				$out.= '</div>';
			}
			return $out;
		}
	}