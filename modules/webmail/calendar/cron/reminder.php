<?php
defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'/../../'));

include_once WM_ROOTPATH.'libraries/afterlogic/api.php';
include_once WM_ROOTPATH.'application/include.php';
require_once(WM_ROOTPATH.'common/class_smtp.php');

// Include Language constants
$oSettings =& CApi::GetSettings();
AppIncludeLanguage($oSettings->GetConf('Common/DefaultLanguage'));

class CReminder
{
	/**
	 * @var CApiUsersManager
	 */
	protected $apiUsersManager;

	/**
	 * @var CApiCalendarManager
	 */
	protected $apiCalendarManager;

	/**
	 * @var array
	 */
	protected $aAccounts;

	/**
	 * @var array
	 */
	protected $aCalendars;

	protected $curRunFilePath;
	
	public function __construct()
	{
		$this->apiUsersManager = CApi::Manager('users');
		$this->apiCalendarManager = CApi::Manager('calendar');
		$this->aAccounts = array();
		$this->aCalendars = array();

		$this->curRunFilePath = CApi::DataPath().'/reminder-run';
	}

	/**
	 * @param string $sLogin
	 * @return CAccount
	 */
	protected function &getAccount($sLogin)
	{
		$mResult = null;

		if (!isset($this->aAccounts[$sLogin]))
		{
			$this->aAccounts[$sLogin] = $this->apiUsersManager->GetAccountOnLogin($sLogin);
		}

		$mResult =& $this->aAccounts[$sLogin];

		if (30 < count($this->aAccounts[$sLogin]))
		{
			$this->aAccounts = array_slice($this->aAccounts, -30);
		}

		return $mResult;
	}

	/**
	 * @param CAccount $oAccount
	 * @param string $sUri
	 * @return CalendarInfo
	 */
	protected function &getCalendar($oAccount, $sUri)
	{
		$mResult = null;

		if (!isset($this->aCalendars[$sUri]))
		{
			$this->aCalendars[$sUri] = $this->apiCalendarManager->GetCalendar($oAccount, $sUri);
		}
		if (isset($this->aCalendars[$sUri]))
		{
			$mResult =& $this->aCalendars[$sUri];
		}

		return $mResult;
	}

	protected function CreateBodyHtml($accountEmail, $eventName, $dateStr, $calendarName, $eventText,
			$calendarColor)
	{
		return sprintf('
	<div style="padding: 10px; font-size: 12px; text-align: center;">
		<div style="border: 4px solid %s; padding: 15px; width: 370px;">
			<h2 style="margin: 5px; font-size: 18px; line-height: 1.4;">%s</h2>
			<span>%s%s</span><br/>
			<span>%s: %s</span><br/><br/>
			<span>%s</span><br/>
		</div>
		<p style="color:#667766; width: 400px; font-size: 10px;">%s</p>
	</div>',
			$calendarColor,
			$eventName,
			ucfirst(ReminderEventBegin),
			$dateStr,
			Calendar,
			$calendarName,
			$eventText,
			sprintf(str_replace(array('%EMAIL%', '%CALENDAR_NAME%'),
								array('<a href="mailto:%1$s">%1$s</a>', '%2$s'),
								ReminderEmailExplanation),
					$accountEmail,
					$calendarName));

	}

	protected function CreateBodyText($accountEmail, $eventName, $dateStr, $calendarName, $eventText)
	{
		return sprintf("%s\r\n\r\n%s%s\r\n\r\n%s: %s %s\r\n\r\n%s",
						$eventName,
						ucfirst(ReminderEventBegin),
						$dateStr,
						Calendar,
						$calendarName,
						$eventText,
						sprintf(str_replace(
									array('%EMAIL%', '%CALENDAR_NAME%'),
									array('mailto:%s', '%s'),
									ReminderEmailExplanation),
								$accountEmail, $calendarName)
		);
	}

	protected function CreateMessage(&$account, $to, $subject, $fiendlyName, $html = null, $text = null)
	{
		$message = new WebMailMessage();
		$GLOBALS[MailDefaultCharset] = CPAGE_UTF8;
		$GLOBALS[MailInputCharset] = CPAGE_UTF8;
		$GLOBALS[MailOutputCharset] = APP_DEFAULT_OUTPUT_CHARSET;

		$message->Headers->SetHeaderByName(MIMEConst_MimeVersion, '1.0');
		$message->Headers->SetHeaderByName(MIMEConst_XMailer, CApi::GetConf('webmail.xmailer-value', 'PHP'));

		$ip = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']
				: ((!empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null));

		if (null !== $ip)
		{
			$message->Headers->SetHeaderByName(MIMEConst_XOriginatingIp, $ip);
		}

		$serverAddr = 'cantgetservername';
		$message->Headers->SetHeaderByName(MIMEConst_MessageID,
			'<'.substr(mt_rand(30000, 32000) * mt_rand(30000, 32000), 0, 7).'.'.md5(time()).'@'. $serverAddr .'>'
		);

		$fromCollection = new EmailAddressCollection();
		$fromCollection->Add($account->Email, $fiendlyName);
		$message->SetFrom($fromCollection);

		$message->SetToAsString(ConvertUtils::WMBackHtmlSpecialChars($to));
		$message->SetSubject(ConvertUtils::WMBackHtmlSpecialChars($subject));
		$message->SetDate(new CDateTime(time()));

		if ($html != NULL)
		{
			$message->TextBodies->HtmlTextBodyPart =
				ConvertUtils::AddHtmlTagToHtmlBody(
					str_replace("\n", CRLF,
					str_replace("\r", '', ConvertUtils::WMBackHtmlNewCode($html)))
				);
		}
		if ($text != NULL)
		{
			$message->TextBodies->PlainTextBodyPart =
				str_replace("\n", CRLF,
				str_replace("\r", '', ConvertUtils::WMBackHtmlNewCode($text))
			);
		}
		return $message;
	}

	protected function SendMessage($oAccount, $sSubject, $sFiendlyName, $sEventName, $sDate, $sCalendarName, $sEventText, $sCalendarColor)
	{
		$sHtmlBody = $this->CreateBodyHtml($oAccount->Email, $sEventName, $sDate, $sCalendarName, $sEventText, $sCalendarColor);
		$sTextBody = $this->CreateBodyText($oAccount->Email, $sEventName, $sDate, $sCalendarName, $sEventText);
		
		$message = $this->CreateMessage($oAccount, $oAccount->Email, $sSubject, $sFiendlyName, $sHtmlBody, $sTextBody);
		$message->OriginalMailMessage = $message->ToMailString(true);

		return CSmtp::SendMail($oAccount, $message, null, null);
	}

	protected function GetReminder($oEventStartDT, $oStartDT, $oEndDT, $sEventId, $aReminders)
	{
		$iStartEventTS = $oEventStartDT->getTimestamp();
		$iStartTS = $oStartDT->getTimestamp();
		$iEndTS = $oEndDT->getTimestamp();

		$aRemindersTime = array();
		foreach ($aReminders as $iReminder)
		{
			$iReminderOffset = CCalendarHelper::GetReminderOffset($iReminder['offset']);
			$aRemindersTime[] = $iStartEventTS - $iReminderOffset;
		}
		sort($aRemindersTime);
		foreach ($aRemindersTime as $iReminder)
		{
			if ($iReminder >= $iStartTS && $iReminder < $iEndTS)
			{
				return $iReminder;
			}
		}
		return false;
	}


	public function Execute($sCheckInterval)
	{
		CApi::Log('---------- Start cron script', ELogLevel::Full, 'cron-');

		$oNowDT = new DateTime('now', new DateTimeZone('UTC'));
		$iNowTS = $oNowDT->getTimestamp();
		
		$oStartDT = clone $oNowDT;
		$oStartDT->sub(new DateInterval('PT5M'));

		if (file_exists($this->curRunFilePath))
		{
			$handle = fopen($this->curRunFilePath, 'r');
			$sCurRunFileTS = fread($handle, 10);
			if (!empty($sCurRunFileTS) && is_numeric($sCurRunFileTS))
			{
				$oStartDT = new DateTime();
				$oStartDT->setTimestamp($sCurRunFileTS);
				$oStartDT->setTimezone(new DateTimeZone('UTC'));
			}
		}

		$iStartTS = $oStartDT->getTimestamp();

		if ($iNowTS >= $iStartTS)
		{
			CApi::Log('Start time: '.$oStartDT->format('r'), ELogLevel::Full, 'cron-');
			CApi::Log('End time: '.$oNowDT->format('r'), ELogLevel::Full, 'cron-');

			$aReminders = $this->apiCalendarManager->GetReminders($iStartTS, $iNowTS);
			$aEvents = array();
			if ($aReminders && is_array($aReminders) && count($aReminders) > 0)
			{
				$aCacheEvents = array();
				foreach($aReminders as $aItem)
				{
					$oAccount = $this->getAccount($aItem['user']);
					$sCalendarUri = $aItem['calendaruri'];
					$sEventId = $aItem['eventid'];
					$iStartTime = $aItem['starttime'];
					
					if (!isset($aCacheEvents[$sEventId]) && isset($oAccount))
					{
						$aCacheEvents[$sEventId]['data'] = $this->apiCalendarManager->GetEvent($oAccount, $sCalendarUri, $sEventId);

						$dt = new DateTime();
						$dt->setTimestamp($iStartTime);
						$dt->setTimezone(new DateTimeZone($oAccount->GetDefaultStrTimeZone()));

						$sDateFormat = 'm/d/Y';
						$sTimeFormat = 'h:i A';
						if ($oAccount->User->DefaultDateFormat == EDateFormat::DDMMYYYY)
						{
							$sDateFormat = 'd/m/Y';
						}
						if ($oAccount->User->DefaultTimeFormat == ETimeFormat::F24)
						{
							$sTimeFormat = 'H:i';
						}

						$aCacheEvents[$sEventId]['time'] = $dt->format($sDateFormat . ' ' . $sTimeFormat);
					}
					if (isset($aCacheEvents[$sEventId]))
					{
						$aEvents[$aItem['user']][$sCalendarUri][$sEventId] = $aCacheEvents[$sEventId];
					}
				}
			}

			foreach ($aEvents as $sEmail => $aUserCalendars)
			{
				foreach ($aUserCalendars as $sCalendarUri => $aUserEvents)
				{
					
					foreach ($aUserEvents as $aUserEvent)
					{
						$aData = $aUserEvent['data']['data'];
						$aEvents = $aUserEvent['data']['events'];
						$aReminders = $aUserEvent['data']['reminders'];

						if (isset($aEvents))
						{
							foreach ($aEvents as $aEvent)
							{
								$oAccount = $this->getAccount($sEmail);
								$oCalendar = $this->getCalendar($oAccount, $sCalendarUri);
								if ($oCalendar)
								{
									$aCalendarUsers = $this->apiCalendarManager->GetCalendarUsers($oAccount, $oCalendar);
									$sEventId = $aEvent['event_id'];
									$sDate = $aUserEvent['time'];

									$vCal = $aData[$sEventId];

									$sEventName = $aEvent['event_name'];
									$sEventText = $aEvent['event_text'];
									$sCalendarName = $oCalendar->DisplayName;
									$sCalendarColor = $oCalendar->Color;
									$sSubject = Event . ' ' . $sEventName . ' ' .  ReminderEventBegin . $sDate;
									$sFiendlyName = '';

									$bIsMessageSent = $this->SendMessage($oAccount, $sSubject, $sFiendlyName, $sEventName, $sDate, $sCalendarName, $sEventText, $sCalendarColor);

									if ($bIsMessageSent)
									{
										$this->apiCalendarManager->UpdateReminder($oAccount, $sCalendarUri, $sEventId, $vCal->serialize());
										CApi::Log("Send reminder for event: '" . $sEventName . "' started on '" . $sDate . "' to '" . $oAccount->Email ."'", ELogLevel::Full, 'cron-');
									}
									else
									{
										CApi::Log('Send reminder for event: FAILED!', ELogLevel::Full, 'cron-');
									}
									
									if (count($aCalendarUsers) > 0)
									{
										foreach ($aCalendarUsers as $aCalendarUser)
										{
											$oCalendarAccount =  $this->getAccount($aCalendarUser['email']);
											if ($oCalendarAccount)
											{
												$this->SendMessage($oCalendarAccount, $sSubject, $sFiendlyName, $sEventName, $sDate, $sCalendarName, $sEventText, $sCalendarColor);
											}
										}
									}
								}
								else
								{
									CApi::Log('Calendar '.$sCalendarUri.' not found!', ELogLevel::Full, 'cron-');
								}
							}
						}
					}
				}
			}

			file_put_contents($this->curRunFilePath, $iNowTS);
		}
		CApi::Log('---------- End cron script', ELogLevel::Full, 'cron-');
	}
}

$timer = microtime(true);

$oReminder = new CReminder();
$oReminder->Execute('P2D');

CApi::Log('Cron execution time: ' . (microtime(true) - $timer) . " sec.", ELogLevel::Full, 'cron-');
