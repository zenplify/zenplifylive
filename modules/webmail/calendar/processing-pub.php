<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'/../'));
require_once WM_ROOTPATH.'application/include.php';

require_once WM_ROOTPATH.'common/inc_constants.php';
require_once WM_ROOTPATH.'common/class_convertutils.php';
require_once WM_ROOTPATH.'calendar/lib.php';

@header('Content-Type: application/json; charset=utf-8');
@ob_start('writeJsonResponse');

/**
 * @param string $response
 * @return string
 */
function writeJsonResponse($response)
{
	CApi::Log("<<< calendar JSON:\r\n".$response);
	return $response;
}

$oInput = new api_Http();

/* @var $oApiUsersManager CApiUsersManager */
$oApiUsersManager = CApi::Manager('users');

$iAccountId = $oApiUsersManager->GetDefaultAccountId(CSession::Get(APP_SESSION_USER_ID));

/* @var $oAccount CAccount */
$oAccount = $oApiUsersManager->GetAccountById($iAccountId);

/* @var $oCalUser CCalUser */
$oCalUser = $oAccount ? $oApiUsersManager->GetOrCreateCalUserByUserId($oAccount->IdUser) : null;

$oSettings =& CApi::GetSettings();

$sLang = $oSettings->GetConf('Common/DefaultLanguage');

$sDefaultTimeFormat = (int) $oSettings->GetConf('Common/DefaultTimeFormat');
$sDefaultDateFormat = 1; // TODO Magic
$sShowWeekEnds = (int) $oSettings->GetConf('Calendar/ShowWeekEnds');
$sWorkdayStarts = (int) $oSettings->GetConf('Calendar/WorkdayStarts');
$sWorkdayEnds = (int) $oSettings->GetConf('Calendar/WorkdayEnds');
$sShowWorkDay = (int) $oSettings->GetConf('Calendar/ShowWorkDay');
$sWeekStartsOn = (int) $oSettings->GetConf('Calendar/WeekStartsOn');
$sDefaultTab = (int) $oSettings->GetConf('Calendar/DefaultTab');

$sStartDate = $oInput->GetRequest('from');
$sEndDate = $oInput->GetRequest('till');

if ($oAccount && $oCalUser)
{
	$sLang = $oAccount->User->DefaultLanguage;

	$sDefaultTimeFormat = $oAccount->User->DefaultTimeFormat;
	$sDefaultDateFormat = 1; // TODO Magic
	$sShowWeekEnds =  (int) $oCalUser->ShowWeekEnds;
	$sShowWorkDay = (int) $oCalUser->ShowWorkDay;
	$sWorkdayStarts = $oCalUser->WorkDayStarts;
	$sWorkdayEnds = $oCalUser->WorkDayEnds;
	$sWeekStartsOn = $oCalUser->WeekStartsOn;
	$sDefaultTab = $oCalUser->DefaultTab;
}


AppIncludeLanguage($sLang);


CApi::Log('>>> calendar GET:');
CApi::LogObject($_GET);

$sTimeZone = 'UTC';
$sCalendarId = null;

if (CSession::Has(CALENDAR_ID) && CSession::Has(ACCESS_LEVEL))
{
	$sCalendarId = CSession::Get(CALENDAR_ID, null);
	$sTimeZone = $oAccount ? $oAccount->GetDefaultStrTimeZone() : 'UTC';
}
else
{
	CApi::Log('calendar error: '.PROC_SESSION_ERROR, ELogLevel::Error);
	exit(getErrorJson(0, PROC_SESSION_ERROR));
}

$sAction = CGet::Has('action') ? CGet::Get('action', '') : (CPost::Has('action') ? CPost::Get('action', '') : '');

$oApiCalendarManager = CApi::Manager('calendar');
switch ($sAction)
{
	case 'get_data':
		if (isset($sCalendarId))
		{
			$oCalendar = $oApiCalendarManager->GetPublicCalendar($sCalendarId);

			$aResponse = array(
				'calendar' => array(),
				'events' => array(),
				'exclusions' => array(),
				'reminders' => array(),
				'appointments' => array()
			);

			if ($oCalendar && $oCalendar->Shared)
			{
				$aResponse['calendar'] = array(
					'active' => '1',
					'color' => CCalendarColors::GetColorNumber($oCalendar->Color),
					'description' => $oCalendar->Description,
					'calendar_id' => $oCalendar->Id,
					'name' => $oCalendar->DisplayName,
					'ical_hash' => null,
					'publication_hash' => $oCalendar->PubHash,
					'publication_level' => $oCalendar->PubLevel,
					'shares' => array(),
					'sharing_active' => '1',
					'sharing_id' => '1',
					'sharing_level' => $oCalendar->SharingLevel,
					'user_id' => '1'
				);
				$oLimits = new CMonthLimits($sWeekStartsOn, $sTimeZone);
				if (null === $sStartDate)
				{
					$sStartDate = $oLimits->GetFrom();
				}
				if (null === $sEndDate)
				{
					$sEndDate = $oLimits->GetTill();
				}
				if ($sEndDate >= $sStartDate)
				{
					$aEvents = $oApiCalendarManager->GetPublicEvents($sCalendarId, $sStartDate, $sEndDate);

					if ($aEvents)
					{
						$aResponse['events'] = array_merge(
								$aEvents['events'], $aResponse['events']);
						$aResponse['exclusions'] = array_merge(
								$aEvents['exclusions'], $aResponse['exclusions']);
						$aResponse['reminders'] = array_merge(
								$aEvents['reminders'], $aResponse['reminders']);
						$aResponse['appointments'] = array_merge(
								$aEvents['appointments'], $aResponse['appointments']);
					}
                }

				echo json_encode($aResponse);
			}
			else
			{
				CApi::Log('calendar error: Unable to load calendar', ELogLevel::Error);
				echo getErrorJson(0, 'Unable to load calendar');
			}
		}
		else
		{
			CApi::Log('calendar error: Unable to load calendar', ELogLevel::Error);
			echo getErrorJson(0, 'Unable to load calendar');
		}

		break;

	default:
		CApi::Log('calendar error: NULL JSON', ELogLevel::Error);
		echo getErrorJson(0, ErrorGeneral);
		break;
}