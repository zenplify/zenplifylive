<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 *
 */

defined('WM_ROOTPATH') || define('WM_ROOTPATH', (dirname(__FILE__).'/../'));

require_once WM_ROOTPATH.'application/include.php';
require_once WM_ROOTPATH.'common/inc_constants.php';
require_once WM_ROOTPATH.'common/class_convertutils.php';
require_once WM_ROOTPATH.'calendar/lib.php';

@header('Content-Type: application/json; charset=utf-8');
@ob_start('writeJsonResponse');

/**
 * @param string $response
 * @return string
 */
function writeJsonResponse($response)
{
	CApi::Log("<<< calendar JSON:\r\n".$response);
	return $response;
}

$iErrorCode = 0;

$oInput = new api_Http();

/* @var $oSettings api_Settings */
$oSettings =& CApi::GetSettings();

/* @var $oApiUsersManager CApiUsersManager */
$oApiUsersManager = CApi::Manager('users');

/* @var $oApiCalendarManager CApiCalendarManager */
$oApiCalendarManager = CApi::Manager('calendar');

/* @var $oApiDavManager CApiDavManager */
$oApiDavManager = CApi::Manager('dav');

$iUserId = CSession::Get(APP_SESSION_USER_ID);
$iAccountId = $oApiUsersManager->GetDefaultAccountId($iUserId);

/* @var $oAccount CAccount */
$oAccount = $oApiUsersManager->GetAccountById($iAccountId);

if ($oAccount)
{
	AppIncludeLanguage($oAccount->User->DefaultLanguage);
}
else
{
	AppIncludeLanguage($oSettings->GetConf('Common/DefaultLanguage'));

	CApi::Log('CALENDAR ERROR: '.PROC_SESSION_ERROR, ELogLevel::Error);
	exit(getErrorJson(null === $iUserId ? 100 : 200, PROC_SESSION_ERROR));
}

// validate token
if (!AppValidateCsrfToken((string) $oInput->GetRequest('token', '')))
{
	CApi::Log('CALENDAR ERROR: Token invalid', ELogLevel::Error);
	exit(getErrorJson(0, 'Token invalid'));
}

$sCalendarId = $oInput->GetRequest('calendar_id');
$sRecurrenceId = $oInput->GetRequest('id_recurrence');
$sRepeatId = $oInput->GetRequest('id_repeat');
$sEventId = $oInput->GetRequest('event_id');
$sNewCalendarId = $oInput->GetRequest('new_calendar_id');
$sAction = $oInput->GetRequest('action', '');
$sEmail = $oInput->GetRequest('email');
$sCalendarActive = $oInput->GetRequest('active');
$bEventChanged = (bool) $oInput->GetRequest('eventChanged', true);
$iCalendarColorId = $oInput->GetRequest('color_id');
$sCalendarName = $oInput->GetRequest('name');
$sCalendarContent = $oInput->GetRequest('content');

$sShowWeekEnds = $oInput->GetRequest('showWeekends');
$sWorkDayStarts = $oInput->GetRequest('workdayStarts');
$sWorkDayEnds = $oInput->GetRequest('WorkdayEnds');
$sWeekStartsOn = $oInput->GetRequest('weekstartson');
$sShowWorkDay = $oInput->GetRequest('showWorkday');
$sDefaultTab = $oInput->GetRequest('tab');

$iPublicationLevel = $oInput->GetRequest('publication_level');
$iPublicationHash = $oInput->GetRequest('publication_hash');
$aShareIds = $oInput->GetRequest('share_ids');
$aShareUsers = $oInput->GetRequest('share_emails');
$aShareLevels = $oInput->GetRequest('share_levels');
$aShareDelete = $oInput->GetRequest('share_delete');
$iStatus = (int) $oInput->GetRequest('status', 0);

$sStartDate = $oInput->GetRequest('from');
$sStartTime = $oInput->GetRequest('time_from');

$sEndDate = $oInput->GetRequest('till');
$sEndTime = $oInput->GetRequest('time_till');

$oEvent = GetEventFromRequest($oAccount, $oInput, $iErrorCode);
$oExclusion = GetExclusionFromRequest($oInput);

$iCalendarAccess = null;
$iNewCalendarAccess = null;

/* @var $oCalUser CCalUser */
$oCalUser = $oApiUsersManager->GetOrCreateCalUserByUserId($oAccount->IdUser);

if (null !== $sCalendarId)
{
	$iCalendarAccess = $oApiCalendarManager->GetCalendarAccess($oAccount, $sCalendarId);
	if (false === $iCalendarAccess)
	{
		CApi::Log('CALENDAR ERROR: An attempt of unauthorized access to calendar of another user detected. (1-'.$iUserId.'-'.$sCalendarId.')', ELogLevel::Warning);
		exit(getErrorJson(0, 'An attempt of unauthorized access to calendar of another user detected.'));
	}
}

if (null !== $sNewCalendarId)
{
	$iNewCalendarAccess = $oApiCalendarManager->GetCalendarAccess($oAccount, $sNewCalendarId);
	if (false === $iNewCalendarAccess)
	{
		CApi::Log('CALENDAR ERROR: An attempt of unauthorized access to calendar of another user detected. (2-'.$iUserId.'-'.$sNewCalendarId.')', ELogLevel::Warning);
		exit(getErrorJson(0, 'An attempt of unauthorized access to calendar of another user detected.'));
	}
}

$aResponse = array();
switch ($sAction)
{
	case 'get_settings':
		if ($oCalUser)
		{
			echo json_encode(
					array(
						'user_id' => (int) $oAccount->IdUser,
						'timeformat' => $oAccount->User->DefaultTimeFormat,
						'dateformat' =>  $oAccount->User->DefaultDateFormat == EDateFormat::MMDDYYYY ? 1 : 2, // TODO Magic
						'showweekends' => (int) $oCalUser->ShowWeekEnds,
						'showworkday' => (int) $oCalUser->ShowWorkDay,
						'workdaystarts' => $oCalUser->WorkDayStarts,
						'workdayends' => $oCalUser->WorkDayEnds,
						'weekstartson' => $oCalUser->WeekStartsOn,
						'defaulttab' => $oCalUser->DefaultTab,
						'email' => $oAccount->Email,
						'server_url' => $oApiDavManager ? $oApiDavManager->GetServerUrl($oAccount) : '',
						'principalurl' => $oApiDavManager ? $oApiDavManager->GetPrincipalUrl($oAccount) : '',
						'allowreminders' => 1
					)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Can\'t Get User Information.', ELogLevel::Error);
			echo getErrorJson(0, ErrorGeneral);
		}
		break;

	case 'update_settings':
		if(CCalendarHelper::validate($sShowWeekEnds, 0, 1) &&
		   CCalendarHelper::validate($sWorkDayStarts, 0, 23) &&
		   CCalendarHelper::validate($sWorkDayEnds, 0, 23) &&
		   CCalendarHelper::validate($sShowWorkDay, 0, 1) &&
		   CCalendarHelper::validate($sDefaultTab, 1, 3))
		{
			$oCalUser->ShowWeekEnds = $sShowWeekEnds;
			$oCalUser->WorkDayStarts = $sWorkDayStarts;
			$oCalUser->WorkDayEnds = $sWorkDayEnds;
			$oCalUser->WeekStartsOn = $sWeekStartsOn;
			$oCalUser->ShowWorkDay = $sShowWorkDay;
			$oCalUser->DefaultTab = $sDefaultTab;

			if (!$oApiUsersManager->UpdateCalUser($oCalUser))
			{
				$iErrorCode = 2;
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			echo json_encode(
				array(
					'showweekends' => $sShowWeekEnds,
					'workdaystarts' => $sWorkDayStarts,
					'workdayends' => $sWorkDayEnds,
					'showworkday' => $sShowWorkDay,
					'weekstartson' => $sWeekStartsOn,
					'defaulttab' => $sDefaultTab
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Update Settings Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorGeneral);
		}
		break;

		case 'get_data':

			// Calendars
			$aResponse['calendars'] = $oApiCalendarManager->GetCalendars($oAccount);

			// Events
			$aResponse['events'] = array();
			$aResponse['exclusions'] = array();
			$aResponse['reminders'] = array();
			$aResponse['appointments'] = array();

			$calendars = array_merge(
					$aResponse['calendars']['user'],
					$aResponse['calendars']['shared']
			);

			$oLimits = new CMonthLimits($oCalUser->WeekStartsOn, $oAccount->GetDefaultStrTimeZone());
			if (null === $sStartDate)
			{
				$sStartDate = $oLimits->GetFrom();
			}
			if (null === $sEndDate)
			{
				$sEndDate = $oLimits->GetTill();
			}

			if ($sEndDate >= $sStartDate)
			{
				// Get events
				if (isset($calendars))
				{
					foreach($calendars as $aCalendar)
					{
						$aEvents = $oApiCalendarManager->GetEvents($oAccount,
								$aCalendar['calendar_id'], $sStartDate, $sEndDate);

						if ($aEvents)
						{
							$aResponse['events'] = array_merge(
									$aEvents['events'], $aResponse['events']);
							$aResponse['exclusions'] = array_merge(
									$aEvents['exclusions'], $aResponse['exclusions']);
							$aResponse['reminders'] = array_merge(
									$aEvents['reminders'], $aResponse['reminders']);
							$aResponse['appointments'] = array_merge(
									$aEvents['appointments'], $aResponse['appointments']);
						}
					}
				}
			}
			echo json_encode($aResponse);
	break;

	case 'get_pubhash':
		$sHash = $oApiCalendarManager->GetPublicCalendarHash($oAccount, $sCalendarId);
		echo json_encode(
			array(
				'hash' => $sHash
			)
		);
		break;

	case 'update_calendar':
		if (null !== $iCalendarColorId && null !== $sCalendarId)
		{
			if (empty($sCalendarId))
			{
				$sCalendarId = $oApiCalendarManager->CreateCalendar($oAccount, $sCalendarName,
						$sCalendarContent, 0, CCalendarColors::GetColorValue($iCalendarColorId));
				if (false === $sCalendarId)
				{
					$iErrorCode = 2;
				}
			}
			else
			{
				if (null === $sCalendarName)
				{
					$oApiCalendarManager->UpdateCalendarColor($oAccount, $sCalendarId,
							CCalendarColors::GetColorValue($iCalendarColorId));
				}
				else
				{
					$oApiCalendarManager->UpdateCalendar($oAccount, $sCalendarId, $sCalendarName,
							$sCalendarContent, 0, CCalendarColors::GetColorValue($iCalendarColorId));
				}
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if  ($iErrorCode === 0)
		{
			echo json_encode(
				array(
				  'user_id' => $oAccount->IdUser,
				  'calendar_id' => $sCalendarId,
				  'name' => $sCalendarName,
				  'description' => $sCalendarContent,
				  'color' => $iCalendarColorId,
				  'active' => 1
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Update Calendar Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorUpdateCalendar);
		}
		break;

	case 'delete_calendar':
		if (null !== $sCalendarId && null !== $iCalendarAccess)
		{
			if (ECalendarPermission::Full == $iCalendarAccess)
			{
				$oApiCalendarManager->DeleteCalendar($oAccount, $sCalendarId);
			}
			else
			{
				$iErrorCode = 2;
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			echo json_encode(
				array(
					'calendar_id' => $sCalendarId
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Delete Calendar Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorDeleteCalendar);
		}
		break;

	case 'check_share':
		if (null !== $sEmail && null !== $sCalendarId)
		{
			$sEmail = urldecode(trim($sEmail));

			if (class_exists('CApi') && CApi::IsValid())
			{
				/* CAccount $oAcct*/
				$oAcct = $oApiUsersManager->GetAccountOnLogin($sEmail);
				if (!$oAcct)
				{
					// if no user on the dav-server
					$aResponse['shares_errors'][] = array(
						'email_to_user' => $sEmail,
						'error_code' => '1'
					);
				}
				else if ($oAcct->Email === $oAccount->Email)
				{
					// if calendar owner
					$aResponse['shares_errors'][] = array(
						'email_to_user' => $sEmail,
						'error_code' => '2'
					);
				}
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			echo json_encode($aResponse);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Check Sharing Error', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorGetSharing);
		}
		break;

	case 'unsubscribe_calendar':
		if (null !== $sCalendarId)
		{
			$oApiCalendarManager->UnsubscibeCalendar($oAccount, $sCalendarId);
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			echo json_encode(
				array(
					'calendar_id' => $sCalendarId
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Delete Calendar Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorDeleteCalendar);
		}
		break;

	case 'update_share':
		if (null !== $sCalendarId && $iCalendarAccess == ECalendarPermission::Full)
		{
			if (null !== $iPublicationLevel)
			{
				$iPublicationLevel = (int) $iPublicationLevel;
				if ((ECalendarPermission::Write == $iPublicationLevel ||
						ECalendarPermission::Read == $iPublicationLevel) &&
							null !== $iPublicationHash)
				{
					$oApiCalendarManager->PublicCalendar($oAccount, $sCalendarId);
				}
				else if ($iPublicationLevel === 0)
				{
					$oApiCalendarManager->UnPublicCalendar($oAccount, $sCalendarId);
				}
			}
			if (null !== $aShareUsers && null !== $aShareLevels && null !== $aShareIds &&
				count($aShareUsers) == count($aShareLevels) && count($aShareUsers) == count($aShareIds))
			{
				for ($i=0; $i<count($aShareIds); $i++)
				{
					$oApiCalendarManager->UpdateCalendarShare($oAccount, $sCalendarId, $aShareUsers[$i], $aShareLevels[$i] == '1' ? ECalendarPermission::Write : ECalendarPermission::Read);
				}
			}

			if (null !== $aShareDelete)
			{
				for ($i=0; $i<count($aShareDelete); $i++)
				{
					$oApiCalendarManager->DeleteCalendarShare($oAccount, $sCalendarId, $aShareDelete[$i]);
				}
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			$cal = $oApiCalendarManager->GetCalendar($oAccount, $sCalendarId);
			if ($cal)
			{
				$aResponse = array(
					'calendar_id' => $sCalendarId,
					'publication_level' => $cal->PubLevel,
					'publication_hash' => $cal->PubHash,
					'shares' => $cal->Shares
				);
			}

			echo json_encode($aResponse);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Update Sharing and Publications Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorUpdateSharing);
		}
	break;

	case 'update_event':
		if ($iErrorCode === 9)
		{
			CApi::Log('CALENDAR ERROR: Update Event Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, 'A duration of the event must be shorter than its repetition interval.');
		}
		else
		{
			if (null !== $sEventId && null !== $sCalendarId &&
					(ECalendarPermission::Full == $iCalendarAccess ||
						ECalendarPermission::Write == $iCalendarAccess))
			{
				if (empty($sEventId)) // Create New Event
				{
					$sNewEventId = $oApiCalendarManager->CreateEvent($oAccount, $oEvent);
					if ($sNewEventId)
					{
						$sEventId = $sNewEventId;
					}
					else
					{
						$iErrorCode = 1;
					}
				}
				else
				{
					$oApiCalendarManager->UpdateEvent($oAccount, $oEvent);
					if (isset($sNewCalendarId) &&
							(ECalendarPermission::Full == $iNewCalendarAccess ||
								ECalendarPermission::Write == $iNewCalendarAccess))
					{
						if (!$oApiCalendarManager->MoveEvent($oAccount, $sCalendarId, $sNewCalendarId, $sEventId))
						{
							$iErrorCode = 1;
						}
						else
						{
							$sCalendarId = $sNewCalendarId;
						}
					}
				}
			}
			else if ($iErrorCode === 0)
			{
				$iErrorCode = 5;
			}

			if ($iErrorCode === 0)
			{
				$event = $oApiCalendarManager->GetEvent($oAccount, $sCalendarId, $sEventId);
				if (isset($event['events']) && is_array($event['events']))
				{
					foreach ($event['events'] as $ev)
					{
						$aResponse['event'] = $ev;
						break;
					}
				}

				$aResponse['exclusion'] = isset($event['exclusions']) ? $event['exclusions'] : null;
				$aResponse['reminders'] = isset($event['reminders']) ? $event['reminders'] : null;
				$aResponse['appointments'] = isset($event['appointments']) ? $event['appointments'] : null;
				echo json_encode($aResponse);
			}
			else
			{
				CApi::Log('CALENDAR ERROR: Update Event Error ('.$iErrorCode.')', ELogLevel::Error);
				echo getErrorJson($iErrorCode, ErrorUpdateEvent);
			}
		}
		break;

	case 'delete_event':
		if (null !== $sEventId && null !== $sCalendarId && null !== $iCalendarAccess)
		{
			if ($iCalendarAccess == ECalendarPermission::Full ||
					$iCalendarAccess == ECalendarPermission::Write)
			{
				if (!$oApiCalendarManager->DeleteEvent($oAccount, $sCalendarId, $sEventId))
				{
					$iErrorCode = 3;
				}
			}
			else
			{
				$iErrorCode = 2;
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			echo json_encode(
				array(
					'event_id' => $sEventId
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Delete Event Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorDeleteEvent);
		}
		break;

	case 'update_exclusion':
		if ((null !== $sEventId && null !== $sRecurrenceId || null !== $sRepeatId) && null !== $sCalendarId &&
				(ECalendarPermission::Full == $iCalendarAccess ||
					ECalendarPermission::Write == $iCalendarAccess))
		{
			if (!$oApiCalendarManager->UpdateExclusion($oAccount, $oExclusion, $oEvent))
			{
				$iErrorCode = 2;
			}
		}
		else
		{
			$iErrorCode = 1;
		}
		if ($iErrorCode === 0)
		{
			$aResponse = $oApiCalendarManager->GetExclusion($oAccount, $sCalendarId, $sEventId, $sRecurrenceId);
			echo json_encode($aResponse);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Update Event Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorUpdateEvent);
		}
		break;

	case 'delete_exclusion':
		if (null !== $sEventId && null !== $sRecurrenceId && null !== $sRepeatId && null !== $sCalendarId &&
				($iCalendarAccess == ECalendarPermission::Full ||
					$iCalendarAccess == ECalendarPermission::Write))
		{
			if (!$oApiCalendarManager->DeleteExclusion($oAccount, $sCalendarId, $sEventId, $sRecurrenceId))
			{
				$iErrorCode = 2; //unable delete event with current event_id and calendar_id
			}
		}
		else
		{
			$iErrorCode = 1;
		}

		if ($iErrorCode === 0)
		{
			$event = $oApiCalendarManager->GetEvent($oAccount, $sCalendarId, $sEventId);
			if (isset($event) && $event && count($event) > 0)
			{
				$aResponse['event'] = $event['events'][$sEventId];
				$aResponse['exclusion'] = $event['exclusions'];
				echo json_encode($aResponse);
			}
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Delete Event Exclusion Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorDeleteEvent);
		}
		break;

	case 'update_appointment':
		if (null !== $sEventId && null != $sCalendarId)
		{
			$sAction = '';
			switch ($iStatus)
			{
				case 1:
					$sAction = 'ACCEPTED';
					break;
				case 2:
					$sAction = 'DECLINED';
					break;
				case 3:
					$sAction = 'TENTATIVE';
					break;
			}

			if (!$oApiCalendarManager->UpdateAppointment($oAccount, $sCalendarId, $sEventId, $sAction))
			{
				$iErrorCode = 2; //unable update respond
			}
		}
		else
		{
			$iErrorCode = 1;
		}
		if ($iErrorCode === 0)
		{
			echo json_encode(
				array(
					'appointment' => array(
						'accessType' => 0,
						'appointmentId' => $oAccount->Email,
						'email' => $oAccount->Email,
						'eventId' => $sEventId,
						'userId' => $oAccount->IdAccount,
						'status' => $iStatus
					)
				)
			);
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Appointment Update Respon Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorAppointmentChangeRespond);
		}
		break;

	case 'update_calendarvisible':
		if (null !== $sCalendarId && null !== $sCalendarActive &&
				!$oApiCalendarManager->UpdateCalendarVisible($sCalendarId, $sCalendarActive))
		{
			$iErrorCode = 1;
		}
		if ($iErrorCode === 0)
		{
			echo json_encode(array());
		}
		else
		{
			CApi::Log('CALENDAR ERROR: Active Status Update Respon Error ('.$iErrorCode.')', ELogLevel::Error);
			echo getErrorJson($iErrorCode, ErrorUpdateCalendar);
		}
		break;

	default:
		CApi::Log('CALENDAR ERROR: NULL JSON', ELogLevel::Error);
		echo getErrorJson(0, ErrorGeneral);
		break;
}