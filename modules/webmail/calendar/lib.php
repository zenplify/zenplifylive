<?php

/*
 * Copyright (C) 2002-2012 AfterLogic Corp. (www.afterlogic.com)
 * Distributed under the terms of the license described in COPYING
 * 
 */

/**
 * @param int $iCode
 * @param string $sDescription
 * @return string
 */
function getErrorJson($iCode, $sDescription)
{
	return json_encode(
			array(
				'error' => 'true', 
				'code' => $iCode, 
				'description' => $sDescription
			)
	);
}

/**
 * @param int $iWeekStartsOn
 * @param int $sTimeZone
 * @return array
 */

class CMonthLimits
{
    protected $from;
	protected $till;
	
	public function GetFrom()
	{
		return $this->from;
	}
	
	public function GetTill()
	{
		return $this->till;
	}

	public function __construct($iWeekStartsOn, $sTimeZone) 
	{
		$oTimeZone = new DateTimeZone($sTimeZone);
		$oTodayDateTime = new DateTime('now', $oTimeZone);

		$sYear = $oTodayDateTime->format('Y');
		$sMonth = $oTodayDateTime->format('m');
		$iDaysNum = (int)$oTodayDateTime->format('t');

		$aAddDays = array();
		$aAddDays[$iWeekStartsOn] = 0;
		for ($j = $iWeekStartsOn+1; $j<7; $j++) 
		{
			$aAddDays[$j] = $aAddDays[$j-1] + 1;
		}
		for ($k = 0; $k<$iWeekStartsOn; $k++) 
		{
			$aAddDays[$k] = $aAddDays[$j-1] + 1*($k+1);
		}

		$oFirstDateTime = new DateTime($sYear.'-'.$sMonth.'-01 12:00:00', $oTimeZone);
		$iFirstDay = (int) $oFirstDateTime->format('w');

		$oLastDateTime = clone $oFirstDateTime;
		$oLastDateTime->add(new DateInterval('P1M'));
		$iLastDay = (int) $oLastDateTime->format('w');

		$iWeekNum = ceil(($iDaysNum + $aAddDays[$iFirstDay])/7);

		$oStartInterval = new DateInterval('P0D');
		if ($iWeekNum == 6) 
		{
			$oStartInterval = new DateInterval('P'.$aAddDays[$iFirstDay].'D');
		} 
		else if ($iWeekNum == 5) 
		{
			$oStartInterval = new DateInterval('P'.(7 + $aAddDays[$iFirstDay]).'D');
		} 
		else //4
		{
			$oStartInterval = new DateInterval('P'.(14 + $aAddDays[$iFirstDay]).'D');
		}
		$oStartInterval->invert = 1;
		$oEndInterval = new DateInterval('P'.(13 - $aAddDays[$iLastDay]).'D');

		$oFirstDateTime->add($oStartInterval);
		$oLastDateTime->add($oEndInterval);

		$this->from = $oFirstDateTime->format("Ymd");
		$this->till = $oLastDateTime->format("Ymd");
	}	
}

/**
* @return CEvent
*/
function GetEventFromRequest($oAccount, $oInput, &$iErrorCode)
{
	$oEvent = new CEvent($oAccount);
	$oEvent->IdEvent = $oInput->GetRequest('event_id');
	$oEvent->IdCalendar = $oInput->GetRequest('calendar_id');	
	$oEvent->Name = $oInput->GetRequest('name');
	$oEvent->Description = $oInput->GetRequest('text');
	$oEvent->Location = $oInput->GetRequest('location');

	$oEvent->StartDate = $oInput->GetRequest('from');
	$oEvent->StartTime = $oInput->GetRequest('time_from');

	$oEvent->EndDate = $oInput->GetRequest('till');
	$oEvent->EndTime = $oInput->GetRequest('time_till');

	$oEvent->IsRepeat = (bool) $oInput->GetRequest('allow_repeat', false);

	if ($oEvent->IsRepeat)
	{
		$oEvent->RRule = new CRRule($oAccount, $oEvent);
		$oEvent->RRule->Period = $oInput->GetRequest('repeat_period');
		if (null !== $oEvent->RRule->Period)
		{
			$iPeriod = (int) $oEvent->RRule->Period;
			$mWeekNumber = $oInput->GetRequest('week_number');
			if (($iPeriod === 2 || $iPeriod === 3) && null !== $mWeekNumber)
			{
				$mWeekNumber = (int) $mWeekNumber;
				$mWeekNumber = ($mWeekNumber < 0 || $mWeekNumber > 4) ? 0 : $mWeekNumber;
				$oEvent->RRule->WeekNum = $mWeekNumber;
			}
			$oEvent->RRule->Times = $oInput->GetRequest('repeat_times');
			$oEvent->RRule->Until = $oInput->GetRequest('repeat_until');
			$oEvent->RRule->Order = $oInput->GetRequest('repeat_order');

			$iEventDuration = (int) (($oEvent->EndDate - $oEvent->StartDate) + 1);
			$iRepeatDuration = 0;

			switch($iPeriod)
			{
				case 0:
					$iRepeatDuration = (int) $oEvent->RRule->Order;
					break;
				case 1:
					$iRepeatDuration = (int) $oEvent->RRule->Order * 7;
					break;
				case 2:
					$iRepeatDuration = (int) $oEvent->RRule->Order * 30;
					break;
				case 3:
					$iRepeatDuration = (int) $oEvent->RRule->Order * 365;
					break;
			}
			if ($iEventDuration > $iRepeatDuration)
			{
				$iErrorCode = 9;
				return null;
			}

			$mRepeatEnd = $oInput->GetRequest('repeat_end');
			if (null !== $mRepeatEnd)
			{
				$iRepeatEnd = (int) $mRepeatEnd;
				if ($iRepeatEnd < 0 || $iRepeatEnd > 3)
				{
					$iRepeatEnd = 0;
				}
				$oEvent->RRule->End = $iRepeatEnd;
			}

			$oEvent->RRule->Sun = (bool) $oInput->GetRequest('sun', false);
			$oEvent->RRule->Mon = (bool) $oInput->GetRequest('mon', false);
			$oEvent->RRule->Tue = (bool) $oInput->GetRequest('tue', false);
			$oEvent->RRule->Wed = (bool) $oInput->GetRequest('wed', false);
			$oEvent->RRule->Thu = (bool) $oInput->GetRequest('thu', false);
			$oEvent->RRule->Fri = (bool) $oInput->GetRequest('fri', false);
			$oEvent->RRule->Sat = (bool) $oInput->GetRequest('sat', false);

			$oEvent->RRule->Until = $oInput->GetRequest('repeat_until');
		}
	}

	$oEvent->ReminderOffset = $oInput->GetRequest('reminder_offset');
	$oEvent->AllDay = $oInput->GetRequest('allday');
	$oEvent->AppointmentsToDelete = $oInput->GetRequest('appointments_delete');
	$oEvent->AppointmentsToSave = $oInput->GetRequest('appointments_save');

	return $oEvent;
}

/**
* @return CExclusion
*/
function GetExclusionFromRequest(api_Http $oInput)
{
	$oExclusion = new CExclusion();

	$oExclusion->IdRecurrence = $oInput->GetRequest('id_recurrence');
	$oExclusion->IdRepeat = $oInput->GetRequest('id_repeat');
	$oExclusion->StartTime = $oInput->GetRequest('event_time_from');
	$oExclusion->Deleted = (bool) $oInput->GetRequest('is_deleted', false);

	return $oExclusion;
}
