/**
 * InitCalendars
 * CreateCalendarElement
 * RenderShowCalendar
 * CalendarOver
 * CalendarOut
 * CalendarVis
 * SaveCalendarVis
 * HideCalendars
 * ShowHidecalendars
 * CheckedHide
 * DeleteCalendarElement
 */
function CCalendars() {
	this.my_calendars		= $id('my_calendars');
	this.shared_calendars	= $id('shared_calendars');
	this.sharedCalendarsHeader	= $id('sharedCalendarsHeader');

	this.InitCalendars();
}
CCalendars.prototype = {
	InitCalendars : function() {
		var calendar,i;
		if (mycache.calendars != undefined){
			for (i in mycache.calendars) {
				calendar = mycache.calendars[i];
				this.CreateCalendarElement(calendar);
			}
		}
	},
	CreateCalendarElement : function(calendar_obj) {
		if (!calendar_obj || calendar_obj == undefined) return;
		var parent;
		if (calendar_obj.sharing_level == null) {
			parent = this.my_calendars;
		} else {
			parent = this.shared_calendars;
		}
		this.RenderShowCalendar(parent, calendar_obj);
	},
	RenderShowCalendar : function(parent, calendar) {
		var idc, color, index, obj = this, active;
		var container, checkBox, contSubject, div1, div2, div3, div31, cl;
		var divText, aText, txt, publicDiv, div32, div4, div5, publicatedLink;

		idc = calendar['calendar_id'];
		color = calendar['color'];
		index = 10;

		container = document.createElement('div');
		container.id = 'calendar_'+idc;
		container.style.zIndex = index;

		var formCheckBox = document.createElement('form');
		checkBox = document.createElement('input');
		checkBox.id = 'checkbox_'+idc;
		checkBox.type = 'checkbox';

		if (calendar.sharing_level == null)
		{
			checkBox.checked = calendar.active == 1 ? true : false;
		}
		else
		{
			checkBox.checked = calendar.sharing_active == 1 ? true : false;
		}
		active = (checkBox.checked ? 'visible' : 'hidden');
		checkBox.onclick = function() {
			if(this.checked)
			{
				obj.SaveCalendarVis(calendar, 1, this);
				obj.CalendarVis(idc, 'visible');
				RenderMonth();
				obj.CheckedHide();
			}
			else
			{
				obj.SaveCalendarVis(calendar, 0, this);
				obj.CalendarVis(idc, 'hidden');
				RenderMonth();
			}
			Grid.RecalcScrollArrows();

		}

		formCheckBox.appendChild(checkBox);

		RenderMonth();

		contSubject = document.createElement('div');
		contSubject.className = 'event';
		div1 = document.createElement('div');
		div1.className = 'a';
		div2 = document.createElement('div');
		div2.className = 'b';
		div3 = document.createElement('div');
		div3.className = 'event_middle';

		div31 = document.createElement('div');
		div31.className = 'calendar_text';
		div31.id = 'calendar_'+idc+'_dtext';
		div31.onmousedown = function() {
			this.className = 'calendar_text'
		}
		div31.onmouseover = function() {
			this.className = 'calendar_text_hover'
		}
		div31.onclick= function() {
			if (mycache.calendars!=undefined){
				for (var i in mycache.calendars)
				{
					cl = mycache.calendars[i];
					var c = $id('calendar_'+cl.calendar_id);
					c.style.zIndex = index;

				}
			}

			var QW=QOpen;
			if (QOpen>0) {
				QuickMenu.RemoveQuickMenu();
			}
			if (QW!=idc) {
				QuickMenu.CreateQuickMenu(calendar.calendar_id, index+1);
				container.style.zIndex = index+2;
			}
		}

		divText = document.createElement('div');
		divText.className = 'text';

		var calendarName = calendar.name;
		if (parent.id == 'shared_calendars')			
		{
			calendarName = calendarName + ' - ' + calendar.owner;
		}

		aText = document.createElement('a');
		aText.id = 'calendar_'+idc+'_text';
		aText.title = calendarName;
		aText.href = 'javascript:void(0);';
		txt = document.createTextNode(calendarName);
		aText.appendChild(txt);

		divText.appendChild(aText);
		div31.appendChild(divText);

		publicDiv = document.createElement('div');
		publicDiv.id = 'calendar_'+idc+'public';
		publicDiv.className =  (calendar.publication_hash != null) ? 'ml_publiclink' : 'hide';
		publicatedLink = document.createElement('img');
		if (calendar.sharing_level == VIEW_BUSY_TIME) {
			color = 15;
		}
		container.className = 'eventcontainer_'+color;
		publicatedLink.src = './calendar/skins/calendar/published'+color+'.png';
		Grid.RenderContainerForEvent(idc, color);

		publicatedLink.alt = Lang.CalendarPublishedTitle;
		publicatedLink.title = Lang.CalendarPublishedTitle;
		publicDiv.appendChild(publicatedLink);
		div31.appendChild(publicDiv);

		div32 = document.createElement('div');
		div32.className = 'vis_check';
		div32.id = 'vis_check_'+idc;

		div32.onmouseover = this.CalendarOver(aText, div32)
		div32.onmouseout = this.CalendarOut(aText, div32)
		div3.onmouseover = this.CalendarOver(aText, div32);
		div3.onmouseout = this.CalendarOut(aText, div32);

		div31.appendChild(div32);
		div3.appendChild(div31);
		div4 = document.createElement('div');
		div4.className = 'b';
		div5 = document.createElement('div');
		div5.className = 'a';
		contSubject.appendChild(div1);
		contSubject.appendChild(div2);
		contSubject.appendChild(div3);
		contSubject.appendChild(div4);
		contSubject.appendChild(div5);

		container.appendChild(contSubject);
		container.appendChild(formCheckBox);
		parent.appendChild(container);
		if ((parent.id == 'shared_calendars') && parent.childNodes && (parent.childNodes.length > 0)) {
			this.sharedCalendarsHeader.style.display = 'inline';
		}
		this.CalendarVis(idc, active);
	},
	CalendarOver : function(txtObj, arrowObj) {
		return function() {
			txtObj.style.textDecoration = 'underline';
			arrowObj.style.background = 'url(./calendar/skins/calendar/arrow_bottom.gif) no-repeat center';
		}
	},
	CalendarOut : function(txtObj, arrowObj) {
		return function() {
			txtObj.style.textDecoration = 'none';
			arrowObj.style.background = 'url(./calendar/skins/calendar/arrow_bottom_light.gif) no-repeat center';
		}
	},
	CalendarVis : function(id, output) {
		var i, el;
		for (i=0; i<6; i++) {
			el=$('#container_'+id+'_'+i);
			if (el) {
				if (output!='visible')
				{
					el.addClass('hide');
				}
				else
				{
					el.removeClass('hide');
				}
			}
		}
		
		var calendar = mycache.calendars[id];
		calendar.active = (output == 'visible') ? 1 : 0;
	},
	SaveCalendarVis_CallBack : function(calendar, aData)
	{
		if (!ServErr(aData,Lang.ErrorUpdateCalendar)) {
			mycache.calendars[calendar.calendar_id] = calendar;
		}
	},
	SaveCalendarVis : function(calendar, active, chkObj)
	{
		chkObj.enabled = false;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		requestParams = 'action=update_calendarvisible' +
						'&calendar_id=' + calendar.calendar_id +
						'&active=' + active;
		if (req) {
			var self = this;
			sendARequest(req, url, requestParams, function(aData) {
				self.SaveCalendarVis_CallBack(calendar, aData);
			});
		}
		chkObj.enabled = true;
	},
	HideCalendars : function(id)
	{
		QuickMenu.RemoveQuickMenu();
		if(showHide[id]==1){
			showHide[id]=0;
			$id('checkbox_'+id).checked = true;
			this.CalendarVis(id, 'visible');
			this.ShowHidecalendars(id, false);
		} else {
			showHide[id]=1;
			this.ShowHidecalendars(id, true);
		}
	},
	ShowHidecalendars: function(id, bool)
	{
		var i, vis, calendar, id_c;
		if(bool)
			vis = 'visible';
		else
			vis = 'hidden';
		for (i in mycache.calendars)
		{
			calendar = mycache.calendars[i];
			id_c = calendar.calendar_id;
			if(id_c != id && id_c != undefined){
				$id('checkbox_'+id_c).checked = bool;
				this.CalendarVis(id_c, vis);
				showHide[id_c]=1;
			}
		}
	},
	CheckedHide : function()
	{
		var i, id_c;
		for (i in mycache.calendars)
		{
			id_c = mycache.calendars[i]['calendar_id'];
			showHide[id_c]=1;
		}
	},

	/**
	 * Deletes calendar and all its events from cache and visual
	 * @param calendar_obj
	 */
	DeleteCalendarElement : function(calendar_obj) {
		if (!calendar_obj) return;
		var i, calendar_id, event, calendar_container, container_parent, parent, delete_dates = [];
		calendar_id = calendar_obj.calendar_id;

		//delete calendar from calendars cache
		delete mycache.calendars[calendar_id];

		//delete color number from colors cache
		if (calendar_obj.sharing_level == null) {
			parent = this.my_calendars;
			delete mycache.colors[calendar_obj.color];
		} else {
			parent = this.shared_calendars;
		}

		//delete event from events cache
		for (i in mycache.ids) {
			event = mycache.ids[i];
			if (event.calendar_id == calendar_id) {
				delete_dates = DeleteEventFromCacheAndGrid(event.event_id);
			}
		}
		RenderScreens(delete_dates)

		/* deletes calendar eventcontainer and all its events */
		//5 - number of containers (1,2 for day, 3,4 for week 5 for month)
		for (i=1; i<=5; i++) {
			calendar_container = $id('container_' + calendar_id +'_'+ i);
			if (calendar_container != undefined) {
				container_parent = calendar_container.parentNode;
				container_parent.removeChild(calendar_container);
			}
		}

		var calendar_control = $id('calendar_'+calendar_id);
		if (calendar_control != undefined && parent != undefined) {
			parent.removeChild(calendar_control);
		}
	}
}