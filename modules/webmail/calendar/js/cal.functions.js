/*
 * Functions:
 *  ShowError
 *  ShowReport
 *  ShowInfo
 *  HideInfo
 *  String.prototype.parseJSON
 * Classes:
 *  CResize
 */

var weekdayz = [];
var showLimits = {
	day:null,
	weekFrom:null,
	weekTill:null,
	monthFrom:null,
	monthTill:null
}
var mycache = {
	calendars: [],
	colors: [],
	ids: [],
	days: [],
	repeats: [],
	d: [],
	w: [],
	m: [],
	exclusions: [],
	reminders: []
}

var timeFormat1 = [
	{Id:0,		Value:"12:00 AM"},
	{Id:0.5,	Value:"12:30 AM"},
	{Id:1,		Value:"1:00 AM"},
	{Id:1.5,	Value:"1:30 AM"},
	{Id:2,		Value:"2:00 AM"},
	{Id:2.5,	Value:"2:30 AM"},
	{Id:3,		Value:"3:00 AM"},
	{Id:3.5,	Value:"3:30 AM"},
	{Id:4,		Value:"4:00 AM"},
	{Id:4.5,	Value:"4:30 AM"},
	{Id:5,		Value:"5:00 AM"},
	{Id:5.5,	Value:"5:30 AM"},
	{Id:6,		Value:"6:00 AM"},
	{Id:6.5,	Value:"6:30 AM"},
	{Id:7,		Value:"7:00 AM"},
	{Id:7.5,	Value:"7:30 AM"},
	{Id:8,		Value:"8:00 AM"},
	{Id:8.5,	Value:"8:30 AM"},
	{Id:9,		Value:"9:00 AM"},
	{Id:9.5,	Value:"9:30 AM"},
	{Id:10,		Value:"10:00 AM"},
	{Id:10.5,	Value:"10:30 AM"},
	{Id:11,		Value:"11:00 AM"},
	{Id:11.5,	Value:"11:30 AM"},
	{Id:12,		Value:"12:00 PM"},
	{Id:12.5,	Value:"12:30 PM"},
	{Id:13,		Value:"1:00 PM"},
	{Id:13.5,	Value:"1:30 PM"},
	{Id:14,		Value:"2:00 PM"},
	{Id:14.5,	Value:"2:30 PM"},
	{Id:15,		Value:"3:00 PM"},
	{Id:15.5,	Value:"3:30 PM"},
	{Id:16,		Value:"4:00 PM"},
	{Id:16.5,	Value:"4:30 PM"},
	{Id:17,		Value:"5:00 PM"},
	{Id:17.5,	Value:"5:30 PM"},
	{Id:18,		Value:"6:00 PM"},
	{Id:18.5,	Value:"6:30 PM"},
	{Id:19,		Value:"7:00 PM"},
	{Id:19.5,	Value:"7:30 PM"},
	{Id:20,		Value:"8:00 PM"},
	{Id:20.5,	Value:"8:30 PM"},
	{Id:21,		Value:"9:00 PM"},
	{Id:21.5,	Value:"9:30 PM"},
	{Id:22,		Value:"10:00 PM"},
	{Id:22.5,	Value:"10:30 PM"},
	{Id:23,		Value:"11:00 PM"},
	{Id:23.5,	Value:"11:30 PM"}
];

var timeFormat2 = [
	{Id:0,		Value:"00:00"},
	{Id:0.5,	Value:"00:30"},
	{Id:1,		Value:"01:00"},
	{Id:1.5,	Value:"01:30"},
	{Id:2,		Value:"02:00"},
	{Id:2.5,	Value:"02:30"},
	{Id:3,		Value:"03:00"},
	{Id:3.5,	Value:"03:30"},
	{Id:4,		Value:"04:00"},
	{Id:4.5,	Value:"04:30"},
	{Id:5,		Value:"05:00"},
	{Id:5.5,	Value:"05:30"},
	{Id:6,		Value:"06:00"},
	{Id:6.5,	Value:"06:30"},
	{Id:7,		Value:"07:00"},
	{Id:7.5,	Value:"07:30"},
	{Id:8,		Value:"08:00"},
	{Id:8.5,	Value:"08:30"},
	{Id:9,		Value:"09:00"},
	{Id:9.5,	Value:"09:30"},
	{Id:10,		Value:"10:00"},
	{Id:10.5,	Value:"10:30"},
	{Id:11,		Value:"11:00"},
	{Id:11.5,	Value:"11:30"},
	{Id:12,		Value:"12:00"},
	{Id:12.5,	Value:"12:30"},
	{Id:13,		Value:"13:00"},
	{Id:13.5,	Value:"13:30"},
	{Id:14,		Value:"14:00"},
	{Id:14.5,	Value:"14:30"},
	{Id:15,		Value:"15:00"},
	{Id:15.5,	Value:"15:30"},
	{Id:16,		Value:"16:00"},
	{Id:16.5,	Value:"16:30"},
	{Id:17,		Value:"17:00"},
	{Id:17.5,	Value:"17:30"},
	{Id:18,		Value:"18:00"},
	{Id:18.5,	Value:"18:30"},
	{Id:19,		Value:"19:00"},
	{Id:19.5,	Value:"19:30"},
	{Id:20,		Value:"20:00"},
	{Id:20.5,	Value:"20:30"},
	{Id:21,		Value:"21:00"},
	{Id:21.5,	Value:"21:30"},
	{Id:22,		Value:"22:00"},
	{Id:22.5,	Value:"22:30"},
	{Id:23,		Value:"23:00"},
	{Id:23.5,	Value:"23:30"}
];
var DAY = 0, WEEK = 1, MONTH = 2, YEAR = 3, CALENDAR_MAIN = 0, CALENDAR_PUBLISHED = 1;
var CELL_HEIGHT = 16.6667, CELL_WIDTH = 14.2857, LINE_HEIGHT = 27;
var FULL_CONTROL = 0, EDIT_EVENTS = 1, VIEW_EVENTS_DETAILS = 2, VIEW_BUSY_TIME = 3;
var mydate, mydateNew, setcache = [], weekendDays, weekdaysOffset;
var weekDaysNamesCalendar = [], weekDaysNamesFull = [], monthLimits = [], weekLimits = [], cachedMonths = [], all_events_dates = [];

function DeleteEventInWebMail(sUid)
{
	if(WebMail)
	{
	    WebMail.deleteCalendarEvent(sUid);
    }
	else if (parent) {
		parent.WebMail.deleteCalendarEvent(sUid);
	}
}

function TentativeEventInWebMail(sUid)
{
	if(WebMail)
	{
	    WebMail.registerTentativeCalendarEvent(sUid);
    }
	else if (parent) {
		parent.WebMail.registerTentativeCalendarEvent(sUid);
	}
}

function ShowError(errorDesc)
{
	if(WebMail)
	{
	    WebMail.showError(errorDesc);
    }
	else if (parent) {
		parent.WebMail.showError(errorDesc);
	}
}

function ShowReport(report)
{
	if(WebMail)
	{
	    WebMail.showReport(report);
    }
	else if (parent && parent.WebMail) {
		parent.WebMail.showReport(report);
	}
}

function ShowInfo(Info)
{
	if(WebMail)
	{
	    WebMail.showInfo(Info);
    }
    else if (parent && parent.WebMail) {
		parent.WebMail.showInfo(Info);
	}
}

function HideInfo()
{
	if(WebMail)
	{
	    WebMail.hideInfo();
    }
    else if (parent && parent.WebMail) {
		parent.WebMail.hideInfo();
	}
}

function setMaskHeight(object) {
	if (MSIEDetect() && object != undefined) {
		var iBodyHeight = document.body.offsetHeight;
		if (object.id == "edit_window") {
			object.style.height = iBodyHeight + 'px';
		} else {
			if (calendarType == CALENDAR_MAIN) object.style.height = iBodyHeight + 'px';
		}
	}
}

function cmScroll_resize() {
	var mlist = $id("manager_list");
	var my_calendars = $id('my_calendars');
	var shared_calendars = $id('shared_calendars');

	var cn, i;
	var scrolled=(mlist.offsetHeight<mlist.scrollHeight);
	var wid1=scrolled?140:155;wid1+="px";
	var wid2=scrolled?100:115;wid2+="px";

	for (i in my_calendars.childNodes) {
		cn = my_calendars.childNodes[i];
		if ((cn != undefined) && (cn.nodeName=="DIV")&&(cn.className!="calendar_header1")) {
			cn.firstChild.style.width=wid1;
			cn.firstChild.childNodes[2].firstChild.firstChild.style.width=wid2;
		}
	}
	for (i in shared_calendars.childNodes) {
		cn = shared_calendars.childNodes[i];
		if ((cn != undefined) && (cn.nodeName=="DIV") && (cn.className!="calendar_header1")) {
			cn.firstChild.style.width=wid1;
			cn.firstChild.childNodes[2].firstChild.firstChild.style.width=wid2;
		}
	}
}

function GetHeight() {
	var height = 768;
	if (self.innerHeight)
		height = self.innerHeight;
	else if (document.documentElement && document.documentElement.clientHeight)
		height = document.documentElement.clientHeight;
	else if (document.body.clientHeight)
		height = document.body.clientHeight;
	return height;
}
function GetWidth() {
	var width = 1024;
	if (document.documentElement && document.documentElement.clientWidth)
		width = document.documentElement.clientWidth;
	else if (document.body.clientWidth)
		width = document.body.clientWidth;
	else if (self.innerWidth)
		width = self.innerWidth;
	return width;
}
function GetBounds(object) {
	var left = object.offsetLeft;
	var top = object.offsetTop;
	for (var parent = object.offsetParent; parent; parent = parent.offsetParent)
	{
		left += parent.offsetLeft;
		top += parent.offsetTop;
	}
	return {Left: left, Top: top, Width: object.offsetWidth, Height: object.offsetHeight}
}

// Check Browser Functions
function FireFoxDetect() {
	return navigator.userAgent.indexOf("Gecko") >= 0;
}
function MSIEDetect() {
	return navigator.userAgent.indexOf("MSIE") >= 0;
}
function OperaDetect() {
	return window.opera;
}
function SafariDetect() {
	var isSafari, r, Name, Version;
	isSafari = (window.devicePixelRatio)?true:false;
	r = navigator.userAgent.split(" ").reverse();
	Name = r[0].slice(0, r[0].indexOf("/"));
	Version = r[1].slice(r[1].indexOf("/")+1);
	return Obj={isSafari:isSafari, Name:Name, Version:Version}
}
function getMSIEVersion() {
	var version, subversion;
	var appVer = navigator.appVersion.toLowerCase();
	var agt=navigator.userAgent.toLowerCase();
	var iePos  = appVer.indexOf('msie');

	if (iePos !=-1) {
		if (agt.indexOf("msie 5.5") !=-1) version = 5.5;
		else {
			subversion = parseFloat(appVer.substring(iePos+5,appVer.indexOf(';',iePos)));
			version = parseInt(subversion);
		}
		return version;
	}
	else return false;
}
function $id(element) {
	if (arguments.length > 1) {
		for (var i = 0, elements = [], length = arguments.length; i < length; i++)
			elements.push($id(arguments[i]));
		return elements;
	}
	if (typeof element == "string") element = document.getElementById(element);
	return element;
}
function CreateChild(oParentNode,sTag) {
	var oNode = document.createElement(sTag);
	oParentNode.appendChild(oNode);
	return oNode;
}
function CreateChildWithAttrs(parent, tagName, arAttrs) {
	var i, attrsLen = arAttrs.length, t, key, val, node;
	if (MSIEDetect()) {
		var strAttrs = '';
		for (i=attrsLen-1; i>=0; i--) {
			t = arAttrs[i];
			key = t[0];
			val = t[1];
			strAttrs += ' ' + key + '="'+ val + '"';
		}
		tagName = '<' + tagName + strAttrs + '>';
		node = document.createElement(tagName);
	} else {
		node = document.createElement(tagName);
		for (i=attrsLen-1; i>=0; i--) {
			t = arAttrs[i];
			key = t[0];
			val = t[1];
			node.setAttribute(key, val);
		}
	}
	parent.appendChild(node);
	return node;
}
function PasteTextNode(object, text) {
	var txt = document.createTextNode(text);
	if (object.firstChild == undefined) {
		object.appendChild(txt);
	} else {
		object.replaceChild(txt, object.firstChild);
	}
}
function CleanNode(object) {
	while (object.firstChild) object.removeChild(object.firstChild);
}
function DeleteNode(element) {
	var object;
	var type = typeof(element);
	if (type != "undefined" && type != null) {
		if (type == "string") object = $id(element);
		if (type == "object") object = element;
		if (object != undefined) {
			var parent = object.parentNode;
			if (parent != undefined || parent != null) parent.removeChild(object);
		}
	}
}
function StopEvents(event)
{
	event = event || window.event;
	if (event.stopPropagation) {
		event.stopPropagation();
	} else {
		event.cancelBubble = true;
	}
	if (event.preventDefault) {
		event.preventDefault();
	} else {
		event.returnValue = false;
	}
}
function Trim(str) {
	str = str || '';
	return str.replace(/^\s+/, '').replace(/\s+$/, '');
}
function HtmlEncode(source) {
	source = source || '';
	return source.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;');
}
function GetDayShortName(day)
{
	var weekDay = new Array(7);
	weekDay[1] = Lang.DayToolMonday;
	weekDay[2] = Lang.DayToolTuesday;
	weekDay[3] = Lang.DayToolWednesday;
	weekDay[4] = Lang.DayToolThursday;
	weekDay[5] = Lang.DayToolFriday;
	weekDay[6] = Lang.DayToolSaturday;
	weekDay[0] = Lang.DayToolSunday;

	return weekDay[day];
}
function GetMonthName(month)
{
	var ar = new Array(12);
	ar[0] = Lang.FullMonthJanuary;
	ar[1] = Lang.FullMonthFebruary;
	ar[2] = Lang.FullMonthMarch;
	ar[3] = Lang.FullMonthApril;
	ar[4] = Lang.FullMonthMay;
	ar[5] = Lang.FullMonthJune;
	ar[6] = Lang.FullMonthJuly;
	ar[7] = Lang.FullMonthAugust;
	ar[8] = Lang.FullMonthSeptember;
	ar[9] = Lang.FullMonthOctober;
	ar[10] = Lang.FullMonthNovember;
	ar[11] = Lang.FullMonthDecember;

	return ar[month];
}
function GetMonthNameByNumber(monthnumber) {
	monthnumber = parseInt(monthnumber, 10);
	switch (monthnumber) {
		case 1:
			monthname = Lang.ShortMonthJanuary;break;
		case 2:
			monthname = Lang.ShortMonthFebruary;break;
		case 3:
			monthname = Lang.ShortMonthMarch;break;
		case 4:
			monthname = Lang.ShortMonthApril;break;
		case 5:
			monthname = Lang.ShortMonthMay;break;
		case 6:
			monthname = Lang.ShortMonthJune;break;
		case 7:
			monthname = Lang.ShortMonthJuly;break;
		case 8:
			monthname = Lang.ShortMonthAugust;break;
		case 9:
			monthname = Lang.ShortMonthSeptember;break;
		case 10:
			monthname = Lang.ShortMonthOctober;break;
		case 11:
			monthname = Lang.ShortMonthNovember;break;
		case 12:
			monthname = Lang.ShortMonthDecember;break;
		default:
			monthname = null;break;
	}
	return monthname;
}
function GetMonthNumberByName(monthname) {
	switch (monthname) {
		case Lang.ShortMonthJanuary:
			monthnumber = "01";break;
		case Lang.ShortMonthFebruary:
			monthnumber = "02";break;
		case Lang.ShortMonthMarch:
			monthnumber = "03";break;
		case Lang.ShortMonthApril:
			monthnumber = "04";break;
		case Lang.ShortMonthMay:
			monthnumber = "05";break;
		case Lang.ShortMonthJune:
			monthnumber = "06";break;
		case Lang.ShortMonthJuly:
			monthnumber = "07";break;
		case Lang.ShortMonthAugust:
			monthnumber = "08";break;
		case Lang.ShortMonthSeptember:
			monthnumber = "09";break;
		case Lang.ShortMonthOctober:
			monthnumber = "10";break;
		case Lang.ShortMonthNovember:
			monthnumber = "11";break;
		case Lang.ShortMonthDecember:
			monthnumber = "12";break;
		default:
			monthnumber = null;break;
	}
	return monthnumber;
}
function ConvertFromStrToDate(str) {
	var pattern, arr, monthnumber;
	if(setcache.dateformat == 1)
	{
		pattern = /^([01]?[0-9])\/([0-3]?[0-9])\/([12][0-9]{3})$/;
		arr = pattern.exec(str);
		if (arr != null) {
			return new Date(arr[3], (Number(arr[1])-1), arr[2]);
		} else {
			return null;
		}
	}
	else if (setcache.dateformat == 2)
	{
		pattern = /^([0-3]?[0-9])\/([01]?[0-9])\/([12][0-9]{3})$/;
		arr = pattern.exec(str);
		if (arr != null) {
			return new Date(arr[3], (Number(arr[2])-1), arr[1]);
		} else {
			return null;
		}
	}
	else if (setcache.dateformat == 3)
	{
		pattern = /^([12][0-9]{3})-([01]?[0-9])-([0-3]?[0-9])$/;
		arr = pattern.exec(str);
		if (arr != null) {
			return new Date(arr[1], (Number(arr[2])-1), arr[3]);
		} else {
			return null;
		}
	}
	else if (setcache.dateformat == 4)
	{
		var month, dayYear;
		pattern = /^([0-3]?[0-9])\s*,\s*([12][0-9]{3})$/;
		month = Trim(str.substr(0, str.indexOf(" ")));
		dayYear = Trim(str.substr(str.indexOf(" "), str.length));
		monthnumber = GetMonthNumberByName(month);
		if (monthnumber==null) return null;
		arr = pattern.exec(dayYear);
		if (arr != null) {
			return new Date(arr[2], (Number(monthnumber)-1), arr[1]);
		} else {
			return null;
		}
	}
	else //setcache.dateformat == 5
	{
		arr = str.split(" ");
		var arr1 = [], res;
		for (i=0; i<arr.length; i++) {
			if (Trim(arr[i]).length != 0) arr1.push(Trim(arr[i]));
		}

		if (arr1.length == 3) {
			monthnumber = GetMonthNumberByName(arr1[1]);
			pattern = /^([12][0-9]{3})$/;
			res = pattern.exec(arr1[2]);
			if (!(Number(arr1[0])<1 || Number(arr1[0])>31) &&
				(monthnumber != null) &&
				(res != null)) {
					return (new Date(res[0], (Number(monthnumber)-1), arr1[0]));
			}
		}
		return null;
	}
}
function ConvertFromDateToStr(date) {
	var str;
	if(setcache.dateformat == 1)
	{
		str = Fnum((date.getMonth()+1),2)+"/"+Fnum(date.getDate(),2)+"/"+date.getFullYear();
	}
	else if (setcache.dateformat == 2)
	{
		str = Fnum(date.getDate(),2)+"/"+Fnum((date.getMonth()+1),2)+"/"+date.getFullYear();
	}
	else if (setcache.dateformat == 3)
	{
		str = date.getFullYear()+"-"+Fnum((date.getMonth()+1),2)+"-"+Fnum(date.getDate(),2);
	}
	else if (setcache.dateformat == 4)
	{
		str = GetMonthNameByNumber(Fnum((date.getMonth()+1),2))+" "+date.getDate()+", "+date.getFullYear();
	}
	else //setcache['dateformat'] == 5
	{
		str = date.getDate()+" "+GetMonthNameByNumber(Fnum((date.getMonth()+1),2))+" "+date.getFullYear();
	}
	return str;
}
function ReplaceNonDigits()
{
	var regExp = /[^0-9]/gi;
	this.value = this.value.replace(regExp, '');
}
function ConvertObjectVariablesToTheirTypes(obj) {
	var converted_obj = new Object(), arr, elem, type;
	var pattern = /^([12][0-9]{3})-([01]?[0-9])-([0-3]?[0-9])\s*([012][0-9]):([03]0):(00)$/;
	for (elem in obj) {
		type = typeof(obj[elem]);
		if (type === "string") {
			if ((!isNaN(Number(obj[elem])) && obj[elem] !== "") || obj[elem] === "0") {
				converted_obj[elem] = parseFloat(obj[elem]);//number
			} else {
				if (obj[elem] === "true" || obj[elem] === "false") {
					converted_obj[elem] = Boolean(obj[elem]);//bool
				} else {
					arr = pattern.exec(obj[elem]);
					if (arr != null) {
						converted_obj[elem] = new Date(arr[1], (arr[2]-1), arr[3], arr[4], arr[5], arr[6]);//data
					} else {
						converted_obj[elem] = obj[elem]; //string
					}
				}
			}
		} else if (type === "object") {
			if (obj[elem] == undefined) {
				converted_obj[elem] = null;
			} else {
				var obj_len = obj[elem].length;
				if (obj_len != undefined && obj_len == 0) {//array
					converted_obj[elem] = [];
				} else {
				converted_obj[elem] = ConvertObjectVariablesToTheirTypes(obj[elem]);
				}
			}
				} else {
					converted_obj[elem] = obj[elem];
				}
	}
	return converted_obj;
}
/**
 * @param arrayObject (array)
 * return first array key or null
 */
function CheckAssociativeArrayIsEmpty(arrayObject) {
	if (null != arrayObject) {
		var i;
		for (i in arrayObject) {
			return i;
		}
	}
	return null;
}
function PreventIEFlickering() {
	/*Use Object Detection to detect IE6*/
	var  m = document.uniqueID /*IE*/
	&& document.compatMode  /*>=IE6*/
	&& !window.XMLHttpRequest /*<=IE6*/
	&& document.execCommand;
	try{
		if(!!m){
			m("BackgroundImageCache", false, true) /* = IE6 only */
		}
	}
	catch(oh){}
}
function Resizer() {
	if (Grid != undefined) {
		Grid.SetMainDivHeight();
		Grid.RecalcScrollArrows();
	}
	if (calendarType == CALENDAR_MAIN && QOpen!=0) QuickMenu.MoveQuickMenu();
	RenderMonth();
}
function GetXMLHTTPRequest()
{
	var transport = null;
	if(window.XMLHttpRequest) {
		transport = new XMLHttpRequest();
	} else {
		if(window.ActiveXObject) {
			try
			{
				transport = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (err)
			{
				try
				{
					transport = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (err2)
				{
				}
			}
		} else {
		}
	}
	return transport;
}
function sendRequest(req, url, params)
{
	var response = '';
	try
	{
		params += '&token=' + GetCsrfToken();

		req.open('POST', url, false);
		req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		req.send(params);
		response = req.responseText;
	}
	catch (e) { }
	return response.parseJSON();
}


function sendARequest(req, url, params, method)
{
	var response = '';
	try
	{
		params += '&token=' + GetCsrfToken();

		req.open('POST', url);
		req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if(req.status == 200) {
					response = req.responseText;
					if (method)
					{
						method.apply(null, [response.parseJSON()]);
					}
				}
			}
		};
		req.send(params);
	}
	catch (e) { }
}

function ServErr(jsondata, strError) {
	if (jsondata === false) {
		HideInfo();
		ShowError(strError);
		return true;
	}
	if (typeof jsondata === 'object' && jsondata !== null && (jsondata['error'] === true || jsondata['error'] === "true")) {
		if (jsondata['code'] === 100 || jsondata['code'] === 200) {
			var oWebMail = WebMail ? WebMail : (parent ? parent.WebMail : null);
			if (oWebMail && typeof oWebMail.LogOut === 'function') {
				oWebMail.LogOut(jsondata['code'] / 100);
				return true;
			}
		}
		HideInfo();
		ShowError(jsondata['description']);
		return true;
	}
	return false;
}
function EventOn(ev,func,flag) {
	if (FireFoxDetect())
	{document.addEventListener(ev,func,flag);}
	else
	{document.attachEvent(("on"+ev),func);}
}
function EventOff(ev,func,flag) {
	if (FireFoxDetect())
	{document.removeEventListener(ev,func,flag);}
	else
	{document.detachEvent(("on"+ev),func);}
}
function AddHandler(object, event, handler)
{
	if( object && event && handler )
	{
		if (typeof object.addEventListener != 'undefined')
		{
			if (event == 'mousewheel')
			{
				event = 'DOMMouseScroll';
			}
			object.addEventListener(event, handler, false);
		}
		else if (typeof object.attachEvent != 'undefined')
		{
			object.attachEvent('on' + event, handler);
		}
		else
		{
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}
function DefineDefaultSettings() {
	weekendDays = [];weekdaysOffset = [];
	if (null !== CheckAssociativeArrayIsEmpty(setcache)) {
		switch (setcache.defaulttab)
		{
			case 1:
				view = DAY;
			break;
			case 2:
				view = WEEK;
			break;
			case 3:
				view = MONTH;
			break;
			default:
				view = WEEK;
			break;
		}
		if (setcache.weekstartson == 0) {
			weekDaysNamesCalendar = [
				Lang.CalendarTableDaySunday,
				Lang.CalendarTableDayMonday,
				Lang.CalendarTableDayTuesday,
				Lang.CalendarTableDayWednesday,
				Lang.CalendarTableDayThursday,
				Lang.CalendarTableDayFriday,
				Lang.CalendarTableDaySaturday
			];
			weekDaysNamesFull = [
				{Day: 0, Value: Lang.FullDaySunday},
				{Day: 1, Value: Lang.FullDayMonday},
				{Day: 2, Value: Lang.FullDayTuesday},
				{Day: 3, Value: Lang.FullDayWednesday},
				{Day: 4, Value: Lang.FullDayThursday},
				{Day: 5, Value: Lang.FullDayFriday},
				{Day: 6, Value: Lang.FullDaySaturday}
			];
		} else if (setcache.weekstartson == 1) {
			weekDaysNamesCalendar = [
				Lang.CalendarTableDayMonday,
				Lang.CalendarTableDayTuesday,
				Lang.CalendarTableDayWednesday,
				Lang.CalendarTableDayThursday,
				Lang.CalendarTableDayFriday,
				Lang.CalendarTableDaySaturday,
				Lang.CalendarTableDaySunday
			];
			weekDaysNamesFull = [
				{Day: 1, Value: Lang.FullDayMonday},
				{Day: 2, Value: Lang.FullDayTuesday},
				{Day: 3, Value: Lang.FullDayWednesday},
				{Day: 4, Value: Lang.FullDayThursday},
				{Day: 5, Value: Lang.FullDayFriday},
				{Day: 6, Value: Lang.FullDaySaturday},
				{Day: 0, Value: Lang.FullDaySunday}
			];
		} else if (setcache.weekstartson == 6) {
			weekDaysNamesCalendar = [
				Lang.CalendarTableDaySaturday,
				Lang.CalendarTableDaySunday,
				Lang.CalendarTableDayMonday,
				Lang.CalendarTableDayTuesday,
				Lang.CalendarTableDayWednesday,
				Lang.CalendarTableDayThursday,
				Lang.CalendarTableDayFriday
			];
			weekDaysNamesFull = [
				{Day: 6, Value: Lang.FullDaySaturday},
				{Day: 0, Value: Lang.FullDaySunday},
				{Day: 1, Value: Lang.FullDayMonday},
				{Day: 2, Value: Lang.FullDayTuesday},
				{Day: 3, Value: Lang.FullDayWednesday},
				{Day: 4, Value: Lang.FullDayThursday},
				{Day: 5, Value: Lang.FullDayFriday}
			];
		}
		if (setcache.showweekends == 1) {
			weekendDays = new Array(0,6);//sat, sun
		}

		weekdaysOffset[setcache.weekstartson] = 0;
		for (var j = setcache.weekstartson+1; j<7; j++) {
			weekdaysOffset[j] = weekdaysOffset[j-1] + 1;
		}
		for (var k = 0; k<setcache.weekstartson; k++) {
			weekdaysOffset[k] = weekdaysOffset[j-1] + 1*(k+1);//k+1;
		}
	}
}

/**
 * @param e == window.event object
 * @param evobj - dom element object
 */
function invoke(e, evobj) {

	indrag = false;
	Grid.SetResizeProperty(false);
	$id('edit_form').style.display = 'none';

	obj = evobj.parentNode;/*global*/
	if (obj == undefined) return;
	var event = Isplit(obj.id);

	var event_data = mycache.ids[event.id], event_allday;
	if (event_data == undefined) return;

	event_allday = event_data.event_allday;


	mybox = Grid.GetGridParams(event_allday);
	inipos = coordss(e, obj, event_allday, mybox,0);
	cew = (mybox.width/((view == DAY)?1:7));
	stx = inipos.x;
	sty = inipos.y;

	invoked_event = {
		calendar_id:mycache.ids[event.id]['calendar_id'],
		event_allday:mycache.ids[event.id]['event_allday'],
		event_name:mycache.ids[event.id]['event_name'],
		id:event.id_original,
		obj:obj
	}
	if (mycache.ids[event.id]['event_repeats'] == 1 && mycache.repeats[event.id_original] != undefined)
	{
		var exclusion = mycache.exclusions[event.id_original];
		if (exclusion != undefined)
		{
			invoked_event.event_timefrom = exclusion['event_timefrom'];
			invoked_event.event_timetill = exclusion['event_timetill'];
			invoked_event.calendar_id = exclusion['calendar_id'];
			invoked_event.event_allday = exclusion['event_allday'];
			invoked_event.event_name = exclusion['event_name'];
		}
		else
		{
			invoked_event.event_timefrom = mycache.repeats[event.id_original]['event_timefrom'];
			invoked_event.event_timetill = mycache.repeats[event.id_original]['event_timetill'];
		}
	}
	else
	{
		invoked_event.event_timefrom = mycache.ids[event.id]['event_timefrom'];
		invoked_event.event_timetill = mycache.ids[event.id]['event_timetill'];
	}

	var tf = invoked_event.event_timefrom;

	if ((view == MONTH) || (event_allday == 1)) //month
	{
		mox = obj.offsetLeft;
	}
	else if (view == DAY)
	{
		mox = 0;
	}
	else if (view == WEEK)
	{
		var wwd = tf.getDay();
		if (setcache.weekstartson == 0)
		{
			mox = (mybox.width/7)*(wwd);
		}
		else //setcache.weekstartson == 1
		{
			if (wwd == 0) wwd = 7;
			mox = (mybox.width/7)*(wwd-1);
		}
	}

	moy = obj.offsetTop;
	if (invoked_event['event_allday'] == 1)//evlink
	{
		var cellno = Math.floor((stx-mox)/cew);
		mox += cellno*cew;
	}
	leftpos = mox+mybox.left;
	toppos = moy+mybox.top;
	if ((view < MONTH) && (event_allday != 1)) //viewMode == DAY or WEEK
	{
		var scrollbox = "area_2_"+ ((view == DAY)?"day":"week");
		var ofs = $id(scrollbox).scrollTop;
		toppos -= ofs;
	}

	var id_calendar = invoked_event["calendar_id"];//evlink
	EventOn("mouseup",gotcha,true);
	var
		eventTextData = mycache.ids[event.id],
		bAppointment = eventTextData != undefined && eventTextData.appointment
	;
	if (!bAppointment && typeof(mycache.calendars[id_calendar]) != "undefined" && calendarType == CALENDAR_MAIN)
	{
		var calendar_sharing_level = mycache.calendars[id_calendar]['sharing_level'];
		if (calendar_sharing_level != VIEW_BUSY_TIME && calendar_sharing_level !=VIEW_EVENTS_DETAILS)
		{
			Grid.CreateShadowElement();
			EventOn("mousemove",emovin,true);
		}
	}
}
function docscroll() {
	if (window.pageYOffset != null) {
		return window.pageYOffset;
	}
	if (document.body.scrollTop != null) {
		return document.body.scrollTop;
	}
	return 0;
}
function coordss(e, eventObj, ad,box,modeFinal,finlink,xflag,xhigh) {
	if (finlink==undefined) {finlink=document.body;}
	if (xflag==undefined) {xflag=0;}
	if (xhigh==undefined) {xhigh=0;}
	var xc = 0, xr = 0;

	e = e || window.event;

	var mmx = e.clientX - box.left;
	var mmy = e.clientY - box.top;
	if (view < MONTH) {//view == DAY or WEEK
		var scrollbox="area_2_"+ ((view == DAY)?"day":"week");
		mmy += $id(scrollbox).scrollTop;
		mmy += docscroll();
	}
	if (modeFinal == 1) {
		var pageX = e.clientX;
		var pageY = e.clientY;
		if (FireFoxDetect()) {
			pageX = e.pageX;
			pageY = e.pageY;
		}
		if (OperaDetect()) {
			pageX += document.documentElement.scrollLeft - document.documentElement.clientLeft;
			pageY += document.documentElement.scrollTop - document.documentElement.clientTop;
		}
		var devx = pageX - drager.offsetLeft;
		var devy = pageY - drager.offsetTop;
		mmx -= devx;mmy -= devy;
	}
	if (view == MONTH) {
		var hgh=$id('grid_2_month').offsetHeight;
		if (xflag==0)
			var corow=flor(mmy,hgh,6,modeFinal);
		else
			var corow=flor(mmy+Math.round(xhigh/2),hgh,6,false);
		xr=corow*(hgh/6);
		var wdh=$id('grid_2_month').offsetWidth;
		var colum=flor(mmx,wdh,7,modeFinal);
		xc=colum*(wdh/7);
		var coex=colum+7*corow;
		var cotime='xx:xx';
		var codate=move8(showLimits.monthFrom,coex);
	} else {
		var si=(view == WEEK ? 'w' : 'd');
		codate='unknown';
		if (ad==1) {
			cotime='xx:xx';
		} else {
			var hgh=$id('grid_2'+si).offsetHeight;
			var corow=flor(mmy,hgh,48,modeFinal);
			var xr=corow*(hgh/48);
			var hrs=Fnum(Math.floor(corow/2),2);
			var mins=((corow%2)?"30":"00");
			var cotime=hrs+":"+mins;
		}
		if (view == DAY) {
			codate=to8(mydate);
		} else {
			var wdh=$id('grid_2'+si).offsetWidth;
			var colum=flor(mmx,wdh,7,modeFinal);
			var xc=colum*(wdh/7);
			var codate=weekdayz[colum];
			if (eventObj.id=='current_day_1' || eventObj.id=='current_day_2') codate=to8(window.nowDate);
		}
	}
	return{x:mmx, y:mmy, t:cotime, d:codate, id:eventObj.id, xcol:xc, xrow:xr}
}
function flor(coord,full,parts,round) {
	var single=full/parts;
	var sector;
	if (!round) {
		sector=Math.floor(coord/single);
	} else {
		sector=Math.round(coord/single);
	}
	if (sector<0) sector=0;
	if (sector>=parts) sector=parts-1;
	return (sector);
}
function EventMouseOverHandler() {SetEventStyle(this.id, 'event_selected');}
function EventMouseOutHandler() {SetEventStyle(this.id, 'event');}
function Isplit(str) {
	str = String(str);
	var spl = str.split("_");
	return {type:spl[0],
		id_original:spl[1]+"_"+ParseToNumber(spl[2])+"_"+ParseToNumber(spl[3]),
		id:spl[1],
		repeat:ParseToNumber(spl[2]),
		date:ParseToNumber(spl[3]),
		box:ParseToNumber(spl[4]),
		mode:ParseToNumber(spl[5]),
		allday:ParseToNumber(spl[6]),
		id_object:(spl[1] + '_' + spl[2] + '_' + spl[3] + '_' + spl[4] + '_' + spl[5] + '_' + spl[6])
	}
}
function SplitEventId(event_id) {
	event_id = String(event_id);
	var parts = event_id.split('_');
	return {id:parts[0], repeat:ParseToNumber(parts[1]), date:ParseToNumber(parts[2]), original:event_id}
}
function ParseToNumber(str) {
	str = parseFloat(str);
	return (isNaN(str)? 0 : str);
}
function Fnum(num, digits) {
	num = String(num);
	return (('00000000'.substr(0,digits-num.length))+num);
}
/*
* param date as string yyyy-mm-dd H:i:s
* return date object
*/
function dt2date(str) {
	str = str ? str.toString() : '';
//	return (new Date(str.substring(0,4),str.substring(5,7)-1,str.substring(8,10),str.substring(11,13),str.substring(14,16),str.substring(17,19)));
	return new Date(
		window.parseInt(str.substring(0, 4), 10),
		window.parseInt(str.substring(5, 7), 10) - 1,
		window.parseInt(str.substring(8, 10), 10),
		window.parseInt(str.substring(11, 13), 10),
		window.parseInt(str.substring(14, 16), 10),
		window.parseInt(str.substring(17, 19), 10)
	);
}
/*
* param dt - date object
* return date  string yyyy-mm-dd H:i:s
*/
function date2dt(dt) {
	return (Fnum(dt.getFullYear(),4)+'-'+Fnum(1+dt.getMonth(),2)+'-'+Fnum(dt.getDate(),2)+' '+Fnum(dt.getHours(),2)+':'+Fnum(dt.getMinutes(),2)+':'+Fnum(dt.getSeconds(),2));
}
/*
* num - date string in format yyyymmdd
* return date object
*/
function from8(num) {
	var sof = String(num);
	return (new Date(sof.substring(0,4),sof.substring(4,6)-1,sof.substring(6,8),0,0,0,0));
}
/*
* dt - date object
* return date string in format yyyymmdd
*/
function to8(dt) {
	return(Number(Fnum(dt.getFullYear(),4)+Fnum(1+dt.getMonth(),2)+Fnum(dt.getDate(),2)));
}
/*
* dt1,dt2 - date string object in format yyyymmdd
* return (int)  number of days
*/
function range8(dt1,dt2) {
	var d1=from8(dt1), d2=from8(dt2);
	return Math.round((d2.getTime()-d1.getTime())/86400000);
}
/*
* dat -yyymmdd date, ofs - (int) number of days
* return yyyymmdd date
*/
function move8(dat,ofs) {
	var date00 = from8(dat), date01 = date00;
	date01.setDate(ofs + date00.getDate());
	return to8(date01);
}

/*
* dat -yyymmdd date, ofs - (int) number of months
* return yyyymmdd date
*/
function moveMonth8(dat,ofs) {
	var date00 = from8(dat), date01 = date00;
	date01.setMonth(ofs + date00.getMonth());
	return to8(date01);
}

/*
* dat -yyymmdd date, ofs - (int) number of years
* return yyyymmdd date
*/
function moveYear8(dat,ofs) {
	var date00 = from8(dat), date01 = date00;
	date01.setYear(ofs + date00.getFullYear());
	return to8(date01);
}

/*
* dat -yyymmdd date, ofs - (int) number of years
* return yyyymmdd date
*/
function moveToPeriod8(dat, ofs, period) {
	if (period == undefined) period = 0;
	period = Number(period);
	var result;
	switch (period){
		case DAY:
			result = move8(dat, ofs);
		break;
		case WEEK:
			result = move8(dat, 7*ofs);
		break;
		case MONTH:
			result = moveMonth8(dat, ofs);
		break;
		case YEAR:
			result = moveYear8(dat, ofs);
		break;
	}
	return result;
}

function gtime(date,hz) {
	if (hz == undefined) hz=0;
	var dt = new Date(date);
	var chour = dt.getHours();
	var cmin = dt.getMinutes();
	if ((chour == 0) && (cmin == 0) && (hz == 24)) {
		chour = 24;
		dt.setTime(dt.getTime()-86400000);
	}
	var cyear = dt.getFullYear();
	var cmonth = 1+dt.getMonth();
	var cday = dt.getDate();
	var cdate = Fnum(cday,2)+"/"+Fnum(cmonth,2)+"/"+String(cyear);
	var climit = String(cyear)+Fnum(cmonth,2)+Fnum(cday,2);
	var ctime = Fnum(chour,2)+':'+Fnum(cmin,2);

	var cfh = chour+(cmin/60);
	var cdatetime = cdate+' '+ctime;
	var cweek = dt.getDay();
	if (cweek==0) cweek=7;
	var nmonth = [, Lang.FullMonthJanuary, Lang.FullMonthFebruary, Lang.FullMonthMarch, Lang.FullMonthApril,
	 Lang.FullMonthMay, Lang.FullMonthJune, Lang.FullMonthJuly, Lang.FullMonthAugust, Lang.FullMonthSeptember,
	 Lang.FullMonthOctober, Lang.FullMonthNovember, Lang.FullMonthDecember][cmonth];
 	var smonth = [, Lang.ShortMonthJanuary, Lang.ShortMonthFebruary, Lang.ShortMonthMarch, Lang.ShortMonthApril,
	 Lang.ShortMonthMay, Lang.ShortMonthJune, Lang.ShortMonthJuly, Lang.ShortMonthAugust, Lang.ShortMonthSeptember,
	 Lang.ShortMonthOctober, Lang.ShortMonthNovember, Lang.ShortMonthDecember][cmonth];

	var utime = ctime;
	if (setcache.timeformat==1) utime = ((chour == 0)? '12' : ((chour > 12)? chour -12 : chour)) + ((cmin == 0) ? ' ' : ':'+ Fnum(cmin,2) + ' ') + ((chour <12) ? "AM" : "PM");
	var udate=cdate;
	if (setcache.dateformat==1) udate=Fnum(cday,2)+"/"+Fnum(cmonth,2)+"/"+String(cyear);
	else if (setcache.dateformat==3) udate=String(cyear)+'-'+Fnum(cmonth,2)+"-"+Fnum(cday,2);
	else if (setcache.dateformat==4) udate=smonth+' '+Fnum(cday,2)+', '+String(cyear);
	else {//setcache.dateformat==5)
		udate=Fnum(cday,2)+' '+smonth+' '+String(cyear);
	}

	var nweek = [, Lang.FullDayMonday, Lang.FullDayTuesday, Lang.FullDayWednesday, Lang.FullDayThursday,
	 Lang.FullDayFriday, Lang.FullDaySaturday, Lang.FullDaySunday][cweek];
	var inmonth = GetDaysInMonth(cmonth,cyear);
	var dobj = dt;
	var v8=to8(dt);
	return {day: cday,
			month: cmonth,
			year: cyear,
			date: cdate,/**/
			limit: climit,/**/
			hour: chour, /**/
			min: cmin, /**/
			time: ctime,
			fh: cfh,
			datetime: cdatetime, /**/
			week: cweek,
			nweek: nweek, /**/
			nmonth: nmonth, /**/
			smonth: smonth, /**/
			udate: udate,
			utime: utime,
			inmonth: inmonth,
			dobj: dobj,
			to8: v8
		}
}
function GetDaysInMonth(month,year)
{
	month = (month < 1) ? 0 : ((month > 12) ? 11 : month - 1);
	var arDaysInMonth_Usual = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var arDaysInMonth_Leap 	= [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	if ( (year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0 )
		return arDaysInMonth_Leap[month];
	else
		return arDaysInMonth_Usual[month];
}
function DateBrowse(jd) {
	if (view == MONTH) {
		var mydate1, month1, mydate2, month2;
		mydate1 = mydate;
		month1 = mydate1.getMonth();
		mydate2 = new Date(mydate);
		mydate2.setMonth(mydate2.getMonth() + jd);
		month2 = mydate2.getMonth();

		if ((jd > 0 && (month2 - month1) > 1) ||
			(jd < 0 && ((month1 - month2) < 1 && !(month1 == 0 && month2 == 11)))
		){
			mydate2.setDate(0);
		}
		mydate = mydate2;
	} else {
		var step = (view == DAY) ? 1 : 7;
		mydate.setDate(mydate.getDate() + step*jd);
	}
	CacheLoad(mydate);
}
function Switch2date(dt) {
	mydate = new Date(dt.substr(0,4), (Number(dt.substr(4,2))-1), dt.substr(6,2));
	CacheLoad(mydate);
}
function LoadScreenForDate(mydateNew) {
	CalendarsContainersClear();
	FillEvents();
	Grid.RecalcScrollArrows();
	RenderMonth();
	Grid.FillGrid(mydateNew);
	calendarInManager.Fill(mydateNew.getDate(), mydateNew.getMonth()+1, mydateNew.getFullYear());
	calendarInManager.MarkDays();
	HideInfo();
}
function mcros(h,z) {
	var A1 = h.from, A2 = h.till, B1 = z.from, B2 = z.till, ad1 = h.allday, ad2 = z.allday;

	if (ad1+ad2==2) return ( ((A1>=B1)&&(A1<=B2)) || ((A2>=B1)&&(A2<=B2)) || ((B1>=A1)&&(B1<=A2)) || ((B2>=A1)&&(B2<=A2)) ); //return ( ((A1>=B1)&&(A2<=B2)) || ((B1>=A1)&&(B2<=A2)) || ((B1>A1)&&(B1<A2)) || ((B2>A1)&&(B2<A2)) || ((A1>B1)&&(A1<B2)) || ((A2>B1)&&(A2<B2)) );
	if (ad1+ad2==0) return (A1==B1);
	if (ad1==1) return ((B1>=A1)&&(B1<=A2));
	if (ad2==1) return ((A1>=B1)&&(A1<=B2));
}
function HideMoreEventsList() {
	if (shownfetch == 2) {
		shownfetch=1;
		return;
	}
	if (shownfetch==0) return;
	var mel=$id('mfet');
	if (mel!=null) document.body.removeChild(mel);
	shownfetch=0;
}
/*
* get parameters of date
* param date - dateobj
* param hz bool
* params array of parameters
* return obj
*/
function GetDateParams(dt, hz, params) {
	if (params == undefined) return {}
	if (hz == undefined) hz = false;
	var date = new Date(dt), chour = date.getHours(), cmin = date.getMinutes(), obj = {}
	if ((chour == 0) && (cmin == 0) && hz) {
		chour = 24;
		date.setTime(date.getTime()-86400000);
	}

	for (var i=0; i<params.length; i++) {
		switch (params[i]) {
		case 'WEEK':
			obj.week = date.getDay();
		break;
		case 'FH':
			obj.fh = chour+(cmin/60);
		break;
		case 'TO8':
			obj.to8 = to8(date);
		break;
		case 'TIME':
			var result;
			if (setcache.timeformat == 1) {
				var h, timeRange;
				if (chour == 0 || chour == 24) {
					h = "12";
					timeRange = "AM";
				} else if (chour == 12) {
					h = "12";
					timeRange = "PM";
				} else if (chour < 12) {
					h = chour;
					timeRange = "AM";
				} else {
					h = chour-12;
					timeRange = "PM";
				}
				result = h.toString() + ((cmin == 0) ? "" : (":" + Fnum(cmin,2))) + " " + timeRange;
			} else {
				result = Fnum(chour, 2) + ":" + Fnum(cmin, 2);
			}
			obj.time = result;
		break;
		case 'DATE':
			obj.date = date;
		break;
		}
	}
	return obj;
}
function AddToArray(addToArr, addFromArr) {
	var i, j, exist;
	if (addToArr.common.length == 0) {
		addToArr.common = addFromArr.common;
	} else {
		for (i in addFromArr.common) {
			exist = false;
			for (j=0; j<addToArr.common.length; j++) {
				if (addToArr.common[j] == addFromArr.common[i]) {
					exist = true;
					break;
				}
			}
			if (!exist) addToArr.common.push(addFromArr.common[i]);
		}
	}
	if (addToArr.allday.length == 0) {
		addToArr.allday = addFromArr.allday;
	} else {
		for (i in addFromArr.allday) {
			exist = false;
			for (j=0; j<addToArr.allday.length; j++) {
				if (addToArr.allday[j] == addFromArr.allday[i]) {
					exist = true;
					break;
				}
			}
			if (!exist) addToArr.allday.push(addFromArr.allday[i]);
		}
	}
	return addToArr;
}

function CacheLoad_CallBack(result) {
	var i, j, k, events, allow_add = true;

	if (!ServErr(result, Lang.ErrorLoadEvents)) 
	{
		events = [];
		
		for (i in result) 
		{
			if (i == 'events') 
			{
				events = result[i];
			} 
			else if (i == 'exclusions') 
			{
				for (j  in result[i]) 
				{
					allow_add = false;
					for (k in mycache.calendars) 
					{
						if (result[i][j]['calendar_id'] == k) 
						{
							allow_add = true;
							break;
						}
					}
					if (!allow_add) 
					{
						result[i][j]['is_deleted'] = 1;
					}
					if (mycache.exclusions[j] == undefined) 
					{
						mycache.exclusions[j] = result[i][j];
						mycache.exclusions[j]['allday_flag'] = (result[i][j]["event_allday"] == 1)? 1:0;
					}
				}
			}
		}
		AddEventsToCache(events);
	}
	LoadScreenForDate(mydate);
	HideInfo();
}

function CacheLoad(date) {
	var i, todayYear, todayMonth, monthStart, monthEnd, allow_add = true;

	mydate = new Date(date);

	ShowInfo(Lang.InfoLoading);
	GetLimits(mydate);
	todayYear = date.getFullYear();
	todayMonth = date.getMonth();
	monthStart = to8(monthLimits[todayYear][todayMonth].bigStart);
	monthEnd = to8(monthLimits[todayYear][todayMonth].bigEnd);

	for (i=0; i<cachedMonths.length; i++) {
		if (todayMonth == cachedMonths[i][0] && todayYear == cachedMonths[i][1]) {
			allow_add = false;
			break;
		}
	}
	if (allow_add) {
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		requestParams = 'action=get_data' +
						'&from=' + monthStart +
						'&till=' + monthEnd +
						'&nocache=' + Math.random();
		if (req) {
			sendARequest(req, url, requestParams, CacheLoad_CallBack);
			cachedMonths.push(new Array(todayMonth, todayYear));
		}
	}
	else
	{
		HideInfo();
		LoadScreenForDate(mydate);
	}
}
function AddEventToCache(event)
{
	var datas = {common:[], allday:[]}
	if (event == undefined) return datas;

	var j, obj_timefrom,  obj_timetill, datas_arr, id = event.event_id, ev_obj = {}

	if (typeof(event.event_timefrom) == 'string')
	{
		obj_timefrom = dt2date(event.event_timefrom); /*need to use dt2date */
		obj_timetill = dt2date(event.event_timetill);
	}
	else
	{
		obj_timefrom = event.event_timefrom;
		obj_timetill = event.event_timetill;
	}

	var from_year = obj_timefrom.getFullYear();
	var till_year = obj_timetill.getFullYear();
	if (from_year != till_year)
	{
		var year = from_year, date;
		while (year <= till_year)
		{
			if (monthLimits[year] == undefined)
			{
				date = new Date(year, 0, 1);
				GetLimits(date);
			}
			year++;
		}
	}

	event.event_timefrom = obj_timefrom;
	event.event_timetill = obj_timetill;
	if (event.repeat_until != undefined) {
		if (event.repeat_until == "")
		{
			event.repeat_until = null;
		}
		if (event.repeat_until != null && typeof(event.repeat_until) == 'string')
		{
			event.repeat_until = dt2date(event.repeat_until);
		}
	}

	var params_arr = new Array('FH', 'TO8');
	var from = GetDateParams(obj_timefrom, false, params_arr);
	var till = GetDateParams(obj_timetill, false, params_arr);

	var event_length = till.fh-from.fh;
	event.allday_flag = (event.event_allday == 1) ? 1 : 0;
	if (event_length >= 24 || from.to8 < till.to8 || event.allday_flag == 1)
	{
		event.event_allday = 1;
	}
	mycache.ids[id] = event;

	if (event.event_repeats == 1 && typeof(event.repeat_end) !== "undefined" && event.repeat_end !== "")
	{
		var i, k, arr, event_timefrom, event_timetill, timefrom8, repeat_end;
		var repeat_order, repeat_period, repeat_num, week_number, calcEndDate;
		var end_date, weekDays, weekDayChecked, ev_num = 0, cnt = 0, week_cnt = 0;
		var id_original, allow;

		repeat_end = ParseToNumber(event.repeat_end);
		repeat_order = ParseToNumber(event.repeat_order);
		repeat_period = ParseToNumber(event.repeat_period);
		repeat_num = ParseToNumber(event.repeat_num);
		week_number = ParseToNumber(event.week_number);
		if (repeat_end == 0)
		{
			calcEndDate = new Array(mydate.getMonth(), mydate.getFullYear());
			for (i=0; i<cachedMonths.length; i++)
			{
				if (calcEndDate[0] <= cachedMonths[i][0] && calcEndDate[1] <= cachedMonths[i][1])
				{
					calcEndDate = cachedMonths[i];
				}
			}
			end_date = to8(monthLimits[calcEndDate[1]][calcEndDate[0]].bigEnd);
		}
		else if (repeat_end == 1)
		{
			arr = {
				date: new Date(event.event_timefrom),
				period: repeat_period,
				repeat_times: repeat_num,
				repeat_order: repeat_order,
				weekdays: new Array(event.sun, event.mon, event.tue, event.wed, event.thu, event.fri, event.sat),
				week_number: week_number
			}
			end_date = to8(CalculateUntilValue(arr));
		}
		else
		{
			end_date = to8(event.repeat_until);
		}

		if (event.week_number == "")
		{
			event.week_number = null;
		}
		weekDays = new Array(event.sun, event.mon, event.tue, event.wed, event.thu, event.fri, event.sat);
		weekDayChecked = [];
		var weekDateStartF = [], weekDateStartT = [], day = obj_timefrom.getDay();
		for (i=0, j=0; i<weekDays.length; i++)
		{
			if (weekDays[i] == 1)
			{
				weekDayChecked[j] = i;
				weekDateStartF[j] = new Date(obj_timefrom.getFullYear(), obj_timefrom.getMonth(), obj_timefrom.getDate()-(day-i), obj_timefrom.getHours(), obj_timefrom.getMinutes());
				weekDateStartT[j] = new Date(obj_timetill.getFullYear(), obj_timetill.getMonth(), obj_timetill.getDate()-(day-i), obj_timetill.getHours(), obj_timetill.getMinutes());
				j++;
			}
		}
		timefrom8 = to8(obj_timefrom);
		i = 0;

		if (weekDayChecked[0] < day)
		{
			for (k=0; k<weekDayChecked.length; k++)
			{
				if (day == weekDayChecked[k])
				{
					cnt = k;
					break;
				}
			}
		}

		event_timefrom = obj_timefrom;
		event_timetill = obj_timetill;
		var g = 0;
		while (timefrom8 <= end_date)
		{
			if (g == 0)
			{
				g++;
				continue;
			}
			
			id_original = id + '_' + ev_num + '_' + timefrom8;
			ev_obj = {
				event_id: id_original,
				event_timefrom: event_timefrom,
				event_timetill: event_timetill,
				event_allday: event.event_allday
			}
			if (mycache.repeats[id_original] == undefined)
			{
				mycache.repeats[id_original] = ev_obj;
				datas_arr = AddEventToDaysAndPosCache(ev_obj);
				datas = AddToArray(datas, datas_arr);
			}
			ev_num++;
			event_timefrom = new Date(obj_timefrom);
			event_timetill = new Date(obj_timetill);
			if (repeat_period == DAY) //day
			{
				i++;
				event_timefrom.setDate(obj_timefrom.getDate() + repeat_order*i);
				event_timetill.setDate(obj_timetill.getDate() + repeat_order*i);
			}
			else if (repeat_period == WEEK) //week
			{
				if (weekDayChecked[cnt+1] != undefined)
				{
					cnt++;
				}
				else
				{
					cnt = 0;
					week_cnt++;
				}
				if (weekDateStartF.length > 0 && weekDateStartT.length > 0)
				{
					event_timefrom.setDate(weekDateStartF[cnt].getDate() + 7*repeat_order*week_cnt);
					event_timetill.setDate(weekDateStartT[cnt].getDate() + 7*repeat_order*week_cnt);
				}
				else
				{
					event_timefrom.setDate(obj_timefrom.getDate() + 7*repeat_order*week_cnt);
					event_timetill.setDate(obj_timetill.getDate() + 7*repeat_order*week_cnt);
				}
			}
			else if (repeat_period == MONTH || repeat_period == YEAR) //month, year
			{
				if (event['week_number'] == null)
				{
					allow = true;
					while (allow)
					{
						i++;
						if (repeat_period == MONTH)
						{
							event_timefrom.setMonth(obj_timefrom.getMonth() + i*repeat_order);
							event_timetill.setMonth(obj_timetill.getMonth() + i*repeat_order);
						}
						else
						{
							event_timefrom.setFullYear(obj_timefrom.getFullYear() + i*repeat_order);
							event_timetill.setFullYear(obj_timetill.getFullYear() + i*repeat_order);
						}
						if (event_timefrom.getDate() == obj_timefrom.getDate())
						{
							allow = false;
						}
						else
						{
							event_timefrom = new Date(obj_timefrom);
							event_timetill = new Date(obj_timetill);
						}
					}
				}
				else
				{
					i++;
					var start_date_f, start_date_t;
					if (repeat_period == MONTH)
					{
						start_date_f = new Date(obj_timefrom.getFullYear(), (obj_timefrom.getMonth() + i*repeat_order), 1, obj_timefrom.getHours(), obj_timefrom.getMinutes());
						start_date_t = new Date(obj_timetill.getFullYear(), (obj_timetill.getMonth() + i*repeat_order), 1, obj_timetill.getHours(), obj_timetill.getMinutes());
					}
					else
					{
						start_date_f = new Date((obj_timefrom.getFullYear()+i*repeat_order), (obj_timefrom.getMonth() + 1), 1, obj_timefrom.getHours(), obj_timefrom.getMinutes());
						start_date_t = new Date((obj_timetill.getFullYear()+i*repeat_order), (obj_timetill.getMonth() + 1), 1, obj_timetill.getHours(), obj_timetill.getMinutes());
					}
					event_timefrom = GetDayInMonth(start_date_f, event.week_number, weekDayChecked[0]);
					event_timetill = GetDayInMonth(start_date_t, event.week_number, weekDayChecked[0]);
				}
			}
			timefrom8 = to8(event_timefrom);
		}
	}
	else
	{
		var this_day, allow_add, recurrence_date;
		recurrence_date = to8(obj_timefrom);
		this_day = mycache.days[to8(obj_timefrom)];
		allow_add = false;
		if (this_day == undefined)
		{
			allow_add = true;
		}
		else
		{
			allow_add = true;
			for (j in this_day)
			{
				if (this_day[j] == id + '_0_' + recurrence_date)
				{
					allow_add = false;
					break;
				}
			}
		}
		if (allow_add)
		{
			ev_obj = {
				event_id: id + '_0_' + recurrence_date,
				event_timefrom: obj_timefrom,
				event_timetill: obj_timetill,
				event_allday: event.event_allday
			}
			datas_arr = AddEventToDaysAndPosCache(ev_obj);
			datas = AddToArray(datas, datas_arr);
		}
	}
	return datas;
}
function AddEventsToCache(events)
{
	for (var i in events) 
	{
		AddEventToCache(events[i]);
	}
}
function AddOneRepeatEventToCache(event)
{
	var datas = {common: [], allday: []}
	if (event == undefined) return datas;

	var event_id, exclusion, event_timefrom, event_timetill, event_allday, obj_timetill, hours;
	event_id = event.event_id + '_' + event.id_repeat + '_' + event.id_recurrence;
	exclusion = mycache.exclusions[event_id];
	if (exclusion != undefined)
	{
		if (exclusion.is_deleted==1)
		{
			allow_add = false;
		}
		exclusion.event_timefrom = dt2date(exclusion.event_timefrom);
		exclusion.event_timetill = dt2date(exclusion.event_timetill);
		event_timefrom = to8(exclusion.event_timefrom);
		event_timetill = to8(exclusion.event_timetill);
		event_allday = exclusion.event_allday;
		obj_timetill = exclusion.event_timetill;
	}
	else if (mycache.repeats[event_id] != undefined)
	{
		event_timefrom = to8(mycache.repeats[event_id].event_timefrom);
		event_timetill = to8(mycache.repeats[event_id].event_timetill);
		event_allday = mycache.repeats[event_id].event_allday;
		obj_timetill = mycache.repeats[event_id].event_timetill;
	}
	else
	{
		return datas;
	}

	 CalculateEventGabaritsAndPositions(event_id);
	if (event_timefrom <= showLimits.day && event_timetill>=showLimits.day)
	{
		AddEventToGrid(event_id, 0, showLimits.day);
	}
	if (event_timefrom <= showLimits.weekTill && event_timetill >= showLimits.weekFrom)
	{
		AddEventToGrid(event_id, 1, null);
	}
	if (event_timefrom <= showLimits.monthTill && event_timetill >= showLimits.monthFrom)
	{
		AddEventToGrid(event_id, 2, null);
	}
	hours = GetDateParams(obj_timetill, false, new Array('FH'));
	if (hours.fh == 0) event_timetill = move8(event_timetill,-1);
	do {
		if (mycache.days[event_timefrom] == undefined)
		{
			mycache.days[event_timefrom] = [];
		}
		mycache.days[event_timefrom].push(event_id);
		if (event_allday == 1)
		{
			datas.allday.push(event_timefrom);
		} else {
			datas.common.push(event_timefrom);
		}
		if (all_events_dates[event_timefrom] == undefined)
		{
			all_events_dates[event_timefrom] = true;
		}
		event_timefrom = move8(event_timefrom,1);
	}
	while (event_timefrom <= event_timetill);
	return datas;
}
/*
* param datetime date as date object
* param int week_number 0- first week, 3 -4th week, 4 - last week
* param int week_day - number of day in week su=0, mo=1,..., sa=6
*/
function GetDayInMonth(date, week_number, week_day)
{
	week_number = ParseToNumber(week_number);
	week_day = ParseToNumber(week_day);
	week_day = (week_day == 0)? 7:week_day;
	var offset = 0, this_month, this_month_day;
	if (week_number == 4)
	{
		this_month = new Date(date.getFullYear(), date.getMonth()+1, 0);
		this_month_day = this_month.getDay();
		this_month_day = (this_month_day == 0) ? 7:this_month_day;
		if (this_month_day < week_day)
		{
			offset = - 7;
		}
		else
		{
			offset = 0;
		}
	}
	else
	{
		this_month = new Date(date.getFullYear(), date.getMonth(), 1);
		this_month_day = this_month.getDay();
		this_month_day = (this_month_day == 0) ? 7:this_month_day;
		if (this_month_day <= week_day)
		{
			offset = 7*week_number;
		}
		else
		{
			offset = 7*(week_number + 1);
		}
	}
	return new Date(this_month.getFullYear(), this_month.getMonth(), (this_month.getDate() + week_day - this_month_day + offset), date.getHours(), date.getMinutes());
}
function AddEventToDaysAndPosCache(event)
{
	var exclusion, event_timefrom, event_timetill, allow_add = true, datas = {common: [], allday: []}
	exclusion = mycache.exclusions[event.event_id];
	if (exclusion != undefined)
	{
		if (exclusion.is_deleted==1) {allow_add = false;}
		if (typeof(exclusion.event_timefrom) != 'object') exclusion.event_timefrom = dt2date(exclusion.event_timefrom);
		if (typeof(exclusion.event_timetill) != 'object') exclusion.event_timetill = dt2date(exclusion.event_timetill);
		event_timefrom = to8(exclusion.event_timefrom);
		event_timetill = to8(exclusion.event_timetill);
	}
	else
	{
		event_timefrom = to8(event.event_timefrom);
		event_timetill = to8(event.event_timetill);
	}

	if (allow_add)
	{
		CalculateEventGabaritsAndPositions(event.event_id);

		do {
			if (mycache.days[event_timefrom] == undefined) {
				mycache.days[event_timefrom] = [];
			}
			mycache.days[event_timefrom].push(event.event_id);
			if (event.event_allday == 1)
			{
				datas.allday.push(event_timefrom);
			}
			else
			{
				datas.common.push(event_timefrom);
			}
			if (all_events_dates[event_timefrom] == undefined)
			{
				all_events_dates[event_timefrom] = true;
			}
			event_timefrom = move8(event_timefrom,1);
		}
		while (event_timefrom < event_timetill);
	}
	return datas;
}
function BuildEvents() {
	var i, j, view = [], day, startDay, startDay8, endDay, alreadyBuilded, event_id, id_elems, allowBuild, day_from_cache;
	day = from8(showLimits.day);
	view[0] = new Array(day, day);
	view[1] = new Array(from8(showLimits.weekFrom), from8(showLimits.weekTill));
	view[2] = new Array(from8(showLimits.monthFrom), from8(showLimits.monthTill));

	for (var mode=0; mode<view.length; mode++) {
		startDay = view[mode][0];
		endDay = view[mode][1].getTime();
		alreadyBuilded = [];
		while (startDay.getTime() <= endDay) {
			startDay8 = to8(startDay);
			day_from_cache = mycache.days[startDay8];
			if (day_from_cache != undefined) {
				for (i in day_from_cache) {
					event_id = day_from_cache[i];
					id_elems = SplitEventId(event_id);
					allowBuild = true;
					if (typeof(mycache.ids[id_elems.id]) != "undefined" &&
						typeof(mycache.ids[id_elems.id].event_allday) != "undefined" &&
						mycache.ids[id_elems.id].event_allday == 1) {
						for (j=0; j<alreadyBuilded.length; j++) {
							if (alreadyBuilded[j] == event_id) {
								allowBuild = false;
							}
						}
					}
					if (allowBuild) {
						alreadyBuilded.push(event_id);
						AddEventToGrid(event_id, mode, startDay8);
					}
				}
			}
			startDay.setDate(startDay.getDate() + 1);
		}
	}
}
function BuildEvent(dates, event_id) {
	var i, j, id, wcnt, splitted, event_elem, date;
	if (dates.common.length > 0) {

		for (i in dates.common) {
			date = dates.common[i];
			event_elem = mycache.days[date];

			for (j in event_elem){
				id = event_elem[j];
				splitted = id.split("_");
				if (splitted[0] == event_id)
				{
					if (date == showLimits.day) {
						AddEventToGrid(id, 0, date);
					}
					if ((date >= showLimits.weekFrom) && (date <= showLimits.weekTill)) {
						AddEventToGrid(id, 1, date);
					}
					if ((date >= showLimits.monthFrom) && (date <= showLimits.monthTill)) {
						AddEventToGrid(id, 2, date);
					}
				}
			}
		}
	}
	if (dates.allday.length > 0) {
		var modeArr = [], mode, allowBuild = true, alreadyBuilded;
		modeArr[0] = {
			from: showLimits.day,
			till: showLimits.day
		}
		modeArr[1] = {
			from: showLimits.weekFrom,
			till: showLimits.weekTill
		}
		modeArr[2] = {
			from: showLimits.monthFrom,
			till: showLimits.monthTill
		}
		for (mode = 0; mode < modeArr.length; mode++) {
			alreadyBuilded = [];
			for (i in dates.allday) {
				date = dates.allday[i];
				if ((date >= modeArr[mode].from) && (date <= modeArr[mode].till)) {
					event_elem = mycache.days[date];
					for (j in event_elem) {
						allowBuild = true;
						id = event_elem[j];

						for (wcnt = 0; wcnt < alreadyBuilded.length; wcnt++) {
							if (alreadyBuilded[wcnt] == id) {
								allowBuild = false;
							}
						}
						if (allowBuild) {
							alreadyBuilded.push(id)
							splitted = id.split("_");
							if (splitted[0] == event_id){
								AddEventToGrid(id, mode, date);
							}
						}
					}
				}
			}
		}
	}
}

function SetEventStyle(idStr, classStr) {
	var obj = Isplit(idStr), id_main = $id(idStr), i = obj.box, id, id_str;
	if (i > 0) {
		id = id_main;
		while (id != undefined) {
			id.className = classStr;
			i--;
			id_str = obj.type+ '_' + obj.id_original + '_' + i + '_' + obj.mode + '_' + obj.allday;
			id = $id(id_str);
		}
	}
	i = 0;
	id = id_main;
	while (id != undefined) {
		id.className = classStr;
		i++;
		id_str = obj.type+ '_' + obj.id_original + '_' + i + '_' + obj.mode + '_' + obj.allday;
		id = $id(id_str);
	}
}
function copyar(arr) {
	var tar = [], arit, i;
	for (i in arr) {
		arit = arr[i];
		if (typeof(arit) == "object") arit = copyar(arr[i]);
		tar[i] = arit;
	}
	return tar;
}
function cloneObj(obj){
	var result = {}, tobj = {};
	for(var x in obj)
	{
		if((typeof tobj[x] == "undefined") || (tobj[x] != obj[x]))
		{
			result[x] = obj[x];
		}
	}
	return result;
}
/* deletes all events in calendar eventcontainer */
function CalendarsContainersClear() {
	var i, j, container;
	for (i in mycache.calendars) {
		for (j=1; j<=5; j++) {
			container = $id('container_'+mycache.calendars[i].calendar_id+"_"+j);
			if (container != undefined) CleanNode(container);
		}
	}
}
function GetEventWeek(date) {
	var i, weekArr, week_elem1, week_elem2, date8;
	if (weekLimits[date.getFullYear()] == undefined) {
		var d = new Date(date.getFullYear(), 0, 1);
		GetLimits(d);
	}
	weekArr = weekLimits[date.getFullYear()];
	date8 = to8(date);
	for (i=0; i<weekArr.length; i++) {
		week_elem1 = weekArr[i].date8;
		week_elem2 = weekArr[i+1].date8;
		if (date8 >= week_elem1 && date8 < week_elem2) {
			return i;
		}
	}
	return null;
}
function GetIntervalDays(date_from, date_till) {
	var till, from;
	till = new Date(date_till.getFullYear(), date_till.getMonth(), date_till.getDate());
	from = new Date(date_from.getFullYear(), date_from.getMonth(), date_from.getDate());
	return (Math.round((till.getTime() - from.getTime())/86400000)+1);
}
/*
* param event object
* return positions object
* basedom - parent dom of this event
* arrindex - ?
* divWidth - width of event block in %
* divHeight - height of event block in ex
* divLeft - left offset of event block
* divTop - top offset of event block
* divOff - ?
*/
function CalculateEventGabaritsAndPositions(event_id) {
	var real_event, timefrom, timetill, event_data, allday_section = false, params_arr, parent, days1, days2;
	var from, till, till_ms, width, left, arrindex, arrindex_day, left_day, width_day;
	var top, height, week_start, year_start, day_postfix, week_postfix;

	real_event = SplitEventId(event_id);
	if (mycache.exclusions[event_id] != undefined) {
		event_data = mycache.exclusions[event_id];
		timefrom = event_data.event_timefrom;
		timetill = event_data.event_timetill;
	}
	else {
		event_data = mycache.ids[real_event.id];
		if (real_event.repeat != 0
			|| (real_event.date != 0
				&& real_event.date != to8(event_data.event_timefrom))) {
			var repeat_data = mycache.repeats[event_id];
			timefrom = repeat_data.event_timefrom;
			timetill = repeat_data.event_timetill;
		}
		else {
			timefrom = event_data.event_timefrom;
			timetill = event_data.event_timetill;
		}
	}

	params_arr = new Array('FH', 'WEEK', 'TO8');
	from = GetDateParams(timefrom, false, params_arr);
	till = GetDateParams(timetill, false, params_arr);

	if (event_data.event_allday == 1) allday_section = true;

	from_ms = timefrom.getTime();
	till_ms = timetill.getTime();

	year_start = timefrom.getFullYear();
	width = [];width[year_start] = [];
	left = [];left[year_start] = [];
	arrindex = [];arrindex[year_start] = [];
	arrindex_day = [];arrindex_day[year_start] = [];
	left_day = [];left_day[year_start] = [];
	width_day = [];width_day[year_start] = [];
	year_end = timetill.getFullYear();
	week_end = GetEventWeek(timetill);

	week_start = GetEventWeek(timefrom);
	if (allday_section) {
		var week_end = 0, year_end = 0, year, startLim, endLim, l1, tf, l2, left1, days, week, arrind;
		top = 0;
		height = "3.9ex";
		week_postfix = "_3";
		day_postfix = "_1";
		var timetill_ = new Date(timetill.getFullYear(), timetill.getMonth(), timetill.getDate());
		if (timefrom.getDate() != timetill.getDate() && event_data.allday_flag == 1)
		{
//			timetill.setDate(timetill.getDate()-1); // TODO sash
		}
		year_end = timetill.getFullYear();
		days = GetIntervalDays(timefrom, timetill);
		week_end = GetEventWeek(timetill);
		left1 = (100/7) * weekdaysOffset[from.week];

		if (year_start > year_end) return;
		if (year_start == year_end) {
			year = year_start;

			if (week_start <= week_end) {
				l1 = weekLimits[year_start][week_start+1].date
				startLim = new Date(l1.getFullYear(), l1.getMonth(), l1.getDate());
				tf = new Date(timefrom.getFullYear(), timefrom.getMonth(), timefrom.getDate());
				if (week_start == week_end) {
					days1 = days;
					arrind = 0;
				}
				else {
					days1 = Math.round(startLim.getTime() - tf.getTime())/86400000;
					arrind = 2;
				}

				width[year_start][week_start] = (100/7) * days1;
				width_day[year_start][week_start] = 100;
				arrindex[year_start][week_start] = arrind;//week, month
				left_day[year_start][week_start] = 0;
				left[year_start][week_start] = left1;

				if (week_start != week_end) {
					l2 = weekLimits[year_end][week_end].date;
					endLim = new Date(l2.getFullYear(), l2.getMonth(), l2.getDate());
					days2 = GetIntervalDays(endLim, timetill);

					var days_mid = days - days1 - days2;
					if (days_mid >= 7) {
						week = week_start+1;
						var end = week + (days_mid/7);
						while (week < end) {
							width[year][week] = 100;
							width_day[year][week] = 100;
							arrindex[year][week] = 3;//week, month
							left_day[year][week] = 0;
							left[year][week] = 0;
							week++;
						}
					}
					width[year][week_end] = (100/7) * days2;
					width_day[year][week_end] = 100;
					arrindex[year][week_end] = 1;//week, month
					left_day[year][week_end] = 0;
					left[year][week_end] = 0;
				}
			}


		}
		if (year_start < year_end) {
			year = year_start;

			l1 = weekLimits[year_start][week_start+1].date
			startLim = new Date(l1.getFullYear(), l1.getMonth(), l1.getDate());
			tf = new Date(timefrom.getFullYear(), timefrom.getMonth(), timefrom.getDate());
			days1 = Math.round(startLim.getTime() - tf.getTime())/86400000;

			width[year_start][week_start] = (100/7) * days1;
			width_day[year_start][week_start] = 100;
			arrindex[year_start][week_start] = 2;//week, month
			left_day[year_start][week_start] = 0;
			left[year_start][week_start] = left1;

			week = week_start + 1;
			while (weekLimits[year_start][week] != undefined) {
				width[year][week] = 100;
				width_day[year][week] = 100;
				arrindex[year][week] = 3;//week, month
				left_day[year][week] = 0;
				left[year][week] = 0;
				week++;
			}
			year = year+1;
			while (year < year_end) {
				week = 0;
				width[year] = [];
				width_day[year] = [];
				arrindex[year] = [];
				left_day[year] = [];
				left[year] = [];
				while (weekLimits[year][week] != undefined) {
					width[year][week] = 100;
					width_day[year][week] = 100;
					arrindex[year][week] = 3;//week, month
					left_day[year][week] = 0;
					left[year][week] = 0;
					week++;
				}
				year++;
			}

			week = 0;
			width[year_end] = [];
			width_day[year_end] = [];
			arrindex[year_end] = [];
			left_day[year_end] = [];
			left[year_end] = [];
			while (week < week_end) {
				if (weekLimits[year_end][week].date.getFullYear() == timetill.getFullYear()) {
					width[year_end][week] = 100;
				}
				width_day[year_end][week] = 100;
				arrindex[year_end][week] = 3;//week, month
				left_day[year_end][week] = 0;
				left[year_end][week] = 0;
				week++;
			}

			l2 = weekLimits[year_end][week_end].date;
			endLim = new Date(l2.getFullYear(), l2.getMonth(), l2.getDate());
			days2 = GetIntervalDays(endLim, timetill);
			if (l2.getFullYear() != timetill.getFullYear()) {
				days2 = days2-7;
			}

			width[year][week_end] = (100/7) * days2;
			width_day[year][week_end] = 100;
			arrindex[year][week_end] = 1;//week, month
			left_day[year][week_end] = 0;
			left[year][week_end] = 0;
		}


		arrindex_day[year_end] = [];
		arrindex_day[year_end][to8(timefrom)] = 2;
		var current_date = new Date(timefrom);
		current_date.setDate(current_date.getDate() + 1);
		while (current_date.getTime() < till_ms) {
			arrindex_day[year_end][to8(current_date)] = 3;
			current_date.setDate(current_date.getDate() + 1);
		}
		arrindex_day[year_end][to8(timetill)] = 1;
	}
	else {
		if (year_start > year_end) return;
		top = (9*from.fh)+"ex";
		height = ((9*(till.fh-from.fh))-0.6) +"ex";
		week_postfix = "_4";
		day_postfix = "_2";
		width[year_start][week_start] = 100/7;
		width_day[year_start][week_start] = 100;
		left[year_start][week_start] = (100/7) * weekdaysOffset[from.week];
		left_day[year_start][week_start] = 0;
		arrindex[year_start][week_start] = 0;
		arrindex_day[year_start][to8(timefrom)] = 0;
	}
	parent = "container_" + event_data.calendar_id;

	mycache.d[event_id] = {
		_parent: parent + day_postfix,
		_arrindex:arrindex_day,
		_width: width_day,
		_height: height,
		_left: left_day,
		_top: top,
		_off: 0
	}
	mycache.w[event_id] = {
		_parent: parent + week_postfix,
		_arrindex:arrindex,
		_width: width,
		_height: height,
		_left: left,
		_top: top,
		_off: 0
	}
	mycache.m[event_id] = {
		_parent: parent + "_5",
		_arrindex:arrindex,
		_width: width,
		_height: "3.9ex",
		_left: left,
		_top: 0,
		_off: 0
	}
}
function amcros(h,z) {
	var A1 = h.st, A2 = h.en, B1 = z.st, B2 = z.en, rval;
	rval = ( ((A1>=B1)&&(A1<=B2)) || ((A2>=B1)&&(A2<=B2)) || ((B1>=A1)&&(B1<=A2)) || ((B2>=A1)&&(B2<=A2)) );
	return rval;
}
function GetNumberOfColor(color_id) {
	color_id = Number(color_id);
	var calendar_color;
	switch (color_id) {
		case (1):
			calendar_color = "#ef9554";
			break;
		case (2):
			calendar_color = "#f58787";
			break;
		case (3):
			calendar_color = "#6fd0ce";
			break;
		case (4):
			calendar_color = "#90bbe0";
			break;
		case (5):
			calendar_color = "#baa2f3";
			break;
		case (6):
			calendar_color = "#f68bcd";
			break;
		case (7):
			calendar_color = "#d987da";
			break;
		case (8):
			calendar_color = "#4affb8";
			break;
		case (9):
			calendar_color = "#9f9fff";
			break;
		case (10):
			calendar_color = "#5cc9c9";
			break;
		case (11):
			calendar_color = "#76cb76";
			break;
		case (12):
			calendar_color = "#aec9c9";
			break;
		case (15):
			calendar_color = "#797979";
			break;
		default:
			calendar_color = "#ef9554";
			break;
	}
	return calendar_color;
}
function tmove(arr, i, mcol) {
	var j, arr_len = arr.length;
	var mallow = true, mdata = [], mcoun = 0, colide = true, gi = arr[i],
		col = gi.col, wid = gi.wid, gj, cj, wj, ca;
	if ((col+wid) > mcol)
		mallow = false;
	else {
		colide = false;
		for (j=0; j<arr_len; j++) {
			gj = arr[j];
			if ((i!=j) && (gj.col == (col+wid)) && (tcross(gi,gj))) colide = true;
		}
		for (j=0; j<arr_len; j++) {
			gj = arr[j];
			cj = gj.col;
			wj = gj.wid;
			if (i==j || cj<=col || !tcross(gi,gj)) continue;
			if ((cj+wj)>mcol) {
				mallow=false;
				break;
			}
			ca = tmove(arr, j, mcol);
			if  (!ca.allow) {
				mallow = false;
				break;
			}
			mdata[mcoun]=j;
			mcoun++;
		}
	}
	return {colide: colide, allow: mallow, data: mdata, count: mcoun}
}
function tcross(objA,objB) {
	var A1obj, A2obj, B1obj, B2obj;
	A1obj = GetDateParams(objA.data.event_timefrom, false, new Array('FH'));
	A2obj = GetDateParams(objA.data.event_timetill, true, new Array('FH'));
	B1obj = GetDateParams(objB.data.event_timefrom, false, new Array('FH'));
	B2obj = GetDateParams(objB.data.event_timetill, true, new Array('FH'));

	var A1 = A1obj.fh, A2 = A2obj.fh, B1 = B1obj.fh, B2 = B2obj.fh;
	return ( ((A1>=B1)&&(A2<=B2)) || ((B1>=A1)&&(B2<=A2)) || ((B1>A1)&&(B1<A2)) || ((B2>A1)&&(B2<A2)) || ((A1>B1)&&(A1<B2)) || ((A2>B1)&&(A2<B2)) );
}
function tsort(arr) {
	var len = arr.length, buf, i, j;
	for (i=0; i< (len-1); i++) {
		for (j=(i+1); j<len; j++) {
			if (arr[i].len < arr[j].len) {
				buf = arr[j];
				arr[j] = arr[i];
				arr[i] = buf;
			}
		}
	}
	return arr;
}
function sortArray(arr) {
	var len = arr.length, buf, i, j;
	for (i=0; i<(len-1); i++) {
		for (j=i+1; j<len; j++) {
			if (arr[j] < arr[i]) {
				buf = arr[i];
				arr[i] = arr[j];
				arr[j] = buf;
			}
		}
	}
	return arr;
}
/* cut end */
function atcalc(obj) {
	var timefrom = obj.event_timefrom, timetill = obj.event_timetill;
/*	end = to8(timetill);
	if (timetill.getHours() == 0 && timetill.getMinutes() == 0) {
		end = move8(end, -1);
	}
	return {
		len:(1 + Math.round((timetill.getTime() - timefrom.getTime())/86400000)),
		start: to8(timefrom),
		end:end
	}*/
	return {
		len:(1 + Math.round((timetill.getTime() - timefrom.getTime())/86400000)),
		start: to8(timefrom),
		end: to8(timetill)
	}
}
/*
	monthLimits[year][month] = {
		smallStart
		smallEnd
		bigStart
		bigEnd
	}
*/
function GetLimits(date) {
	var year, current_month, current_day, year_add, firstDate, firstDay, lastDate, lastDay, days_num, startDate;
	var i, w, month, start_week, week_num = [], loadYears = [], week_date;

	year = date.getFullYear();
	current_month = date.getMonth();
	current_day = date.getDay();

	loadYears[0] = year;
	if (current_month == 0) {
		loadYears.push(year-1);
	}
	if (current_month == 11) {
		loadYears.push(year+1);
	}

	for (i=0; i<loadYears.length; i++) {
		year_add = loadYears[i];
		if (monthLimits[year_add] == undefined) {
			monthLimits[year_add] = [];
			weekLimits[year_add] = [];
			for (month=0; month<12; month++){
				firstDate = new Date(year_add, month, 1, 12 , 0, 0);
				firstDay = firstDate.getDay();
				lastDate = new Date(year_add, month+1, 0, 12 , 0, 0);
				lastDay = lastDate.getDay();

				days_num = GetDaysInMonth(month+1, year_add);
				week_num[month] = Math.ceil((days_num + weekdaysOffset[firstDay])/7);

				monthLimits[year_add][month] = {}
				if (week_num[month] == 6) {
					startDate = new Date(firstDate.getTime() - weekdaysOffset[firstDay]*86400000);
					monthLimits[year_add][month].smallStart = startDate; //from top
					monthLimits[year_add][month].bigStart = startDate; // from top
				}
				else if (week_num[month] == 5) {
					monthLimits[year_add][month].smallStart =  new Date(firstDate.getTime() - weekdaysOffset[firstDay]*86400000);  //1 off top
					monthLimits[year_add][month].bigStart =  new Date(firstDate.getTime() - (7 + weekdaysOffset[firstDay])*86400000); // 1 off top
				}
				else {//4
					monthLimits[year_add][month].smallStart = new Date(firstDate.getTime() - weekdaysOffset[firstDay]*86400000); //2 off top
					monthLimits[year_add][month].bigStart = new Date(firstDate.getTime() - (14 + weekdaysOffset[firstDay])*86400000);; // 1 off top
				}
				monthLimits[year_add][month].smallEnd = new Date(lastDate.getTime() + (6 - weekdaysOffset[lastDay])*86400000);
				monthLimits[year_add][month].bigEnd = new Date(lastDate.getTime() + (6 - weekdaysOffset[lastDay] + 7)*86400000);
			}

			start_week = new Date(monthLimits[year_add][0].smallStart);
			if (weekLimits[year_add][0] == undefined) {
				week_date = new Date(start_week);
				weekLimits[year_add][0] = {
					date: week_date,
					date8: to8(week_date)

				}
				start_week.setDate(start_week.getDate() + 7);
			}
			w=1;
			while (start_week <= monthLimits[year_add][11].smallEnd){
				if (weekLimits[year_add][w-1].date.getTime() != start_week.getTime()) {
					week_date = new Date(start_week);

					weekLimits[year_add][w++] = {
						date: week_date,
						date8: to8(week_date)
					}
					start_week.setDate(start_week.getDate() + 7);
				}
			}
			weekLimits[year_add][w] = {
				date: start_week,
				date8: to8(start_week)
			}
		}
	}

	if (true || date == mydate) {
		showLimits = {
			day : to8(mydate),
			weekFrom : to8(new Date(year, current_month, (mydate.getDate() - weekdaysOffset[current_day]))),
			weekTill : to8(new Date(year, current_month, (mydate.getDate() + 6 - weekdaysOffset[current_day]))),
			monthFrom : to8(monthLimits[year][current_month].bigStart),
			monthTill : to8(monthLimits[year][current_month].smallEnd)
		}
	}
}

function FillEvents() {
	if (mycache.days == null) return;
	BuildEvents();
	var week_num = GetEventWeek(from8(showLimits.weekFrom));
	RenderDay('BOTH', week_num);
	RenderWeek('BOTH', null, week_num);
	Grid.SetWorkAreaOffset();
	$id("allspan").onclick = function() {HideMoreEventsList();}
}

function RenderScreens(render_dates) {
	if (render_dates == undefined) return;

	var i, j, section, allowDay = false, allowWeek = false;
	var allday_section_d = false, allday_section_w = false;
	var common_section_d = false, common_section_w = false;
	var week_num =  GetEventWeek(from8(showLimits.weekFrom));

	for (i in render_dates.allday) {
		if (render_dates.allday[i] == showLimits.day) {
			allowDay = true;
			allday_section_d = true;
		}
		if ((render_dates.allday[i] < showLimits.weekTill) && (render_dates.allday[i] > showLimits.weekFrom)) {
			allowWeek = true;
			allday_section_w = true;
		}
		if (allday_section_d && allday_section_w) break;
	}
	for (j in render_dates.common) {
		if (render_dates.common[j] == showLimits.day) {
			allowDay = true;
			common_section_d = true;
		}
		if ((render_dates.common[j] >= showLimits.weekFrom) && (render_dates.common[j] <= showLimits.weekTill)) {
			allowWeek = true;
			common_section_w = true;
		}
		if (common_section_d && common_section_w) break;
	}

	if (common_section_d && allday_section_d) {
		section = 'BOTH';
	}
	else if (allday_section_d) {
		section = 'ALLDAY';
	}
	else {
		section = 'COMMON';
	}
	if (allowDay) RenderDay(section, week_num);
	if (common_section_w && allday_section_w) {
		section = 'BOTH';
	}
	else if (allday_section_w) {
		section = 'ALLDAY';
	}
	else {
		section = 'COMMON';
	}
	if (allowWeek) {
		if (section == 'COMMON' || section == 'BOTH') {
			RenderWeek(section, render_dates.common, week_num);
		} else {
			RenderWeek(section, null, week_num);
		}
	}

	Grid.RecalcScrollArrows();
	RenderMonth();
	if (calendarType == CALENDAR_MAIN)
	{
//		calendarInManager.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
		calendarInManager.MarkDays();
	}
}

function RenderDay(section, week_num) {
	var arr = CreateAllEventsArray(section, showLimits.day);
	ReRenderCommonSection(arr.common, 0, week_num);
	ReRenderAlldaySection(arr.allday, 0);
}

/*
* @param render_allday_section - string
* ALLDAY - allow render allday section,
* COMMON - render common section only
* BOTH - render both sections
* @param render_common_days - optional array of dates in format yyyy-mm-dd
*/
function RenderWeek(section, common_days_arr, week_num) {
	var i, j, k, week_days = [], alldays = [], arr, allow_add;

	if (section != 'COMMON')
	{
		var week_cur = showLimits.weekFrom, week_till = showLimits.weekTill;
		while (week_cur <= week_till)
		{
			week_days.push(week_cur);
			week_cur = move8(week_cur,1);
		}
	}
	else
	{
		week_days = common_days_arr;
	}

	for (i=0; i<week_days.length; i++)
	{
		arr = CreateAllEventsArray(section, week_days[i]);
		if (section == 'BOTH' && common_days_arr != undefined)
		{
			for (j=0; j<common_days_arr.length; j++)
			{
				if (common_days_arr[j] == week_days[i])
				{
					ReRenderCommonSection(arr.common, 1, week_num);
					break;
				}
			}
		}
		else
		{
			ReRenderCommonSection(arr.common, 1, week_num);
		}
		if (section != 'COMMON')
		{
			if (alldays.length == 0)
			{
				alldays = arr.allday;
			}
			else
			{
				for (j in arr.allday)
				{
					allow_add = false;
					if (alldays.length == 0)
					{
						alldays = arr.allday;
						break;
					}
					else
					{
						for (k in alldays)
						{
							if (arr.allday[j].event_id == alldays[k].event_id)
							{
								allow_add = true;
								break;
							}
						}
						if (!allow_add)
						{
							alldays.push(arr.allday[j]);
						}
					}
				}
			}
		}
	}
	if (section != 'COMMON') ReRenderAlldaySection(alldays, 1);
}

function RenderMonth() {
	if (view == MONTH) {
		var events_parts = [], i = 0, j, hcol = 0;
		var month_container, month_container_height, cell_height;
		var event_container, iddata, event_id, periods_num, evdata;
		var event_timefrom, allday, daysNumber, evmove, from, till;
		var calendar_id, calname, calendar_container, calendar_childs, child;
		var active_calendar, event_params;
		var month_start = from8(showLimits.monthFrom);

		month_container = $id("area_2_month");
		month_container_height = ParseToNumber(month_container.offsetHeight);
		cell_height = Math.floor(month_container_height/6-20);//6-number of rows 20-height of cell header


		for (calendar_id in mycache.calendars)
		{
			var calendar = mycache.calendars[calendar_id];
			if (calendar.sharing_level == null)
			{
				active_calendar = calendar.active == 1 ? true : false;
			}
			else
			{
				active_calendar= calendar.sharing_active == 1 ? true : false;
			}

			if (active_calendar == false) continue;

			calname = 'container_'+calendar_id+'_5';
			calendar_container = $id(calname);
			if (calendar_container != undefined)
			{
				calendar_childs = calendar_container.childNodes.length; //calendar_childs - events elements
				for (child = 0; child < calendar_childs; child++)
				{
					event_container = calendar_container.childNodes[child];
					iddata = Isplit(event_container.id);
					event_id = iddata.id_original;
					periods_num = iddata.box; //define if this event is long for few week (this event will be cut for few pieces with ids event_id_peace0_..., event_id_peace1_...)
					evdata = mycache.ids[iddata.id];
					if (evdata != undefined)
					{
						if (evdata.event_repeats == 1 && mycache.repeats[event_id] != undefined)
						{
							if (mycache.exclusions[event_id] != undefined)
							{
								event_timefrom = mycache.exclusions[event_id].event_timefrom;
							}
							else
							{
								event_timefrom = mycache.repeats[event_id].event_timefrom;
							}
						}
						else
						{
							event_timefrom = evdata.event_timefrom;
						}

						if (event_timefrom.getTime() < month_start.getTime())
						{
							event_timefrom = new Date(month_start);
						}
						event_params = GetDateParams(event_timefrom, false, new Array("FH", "WEEK", "TO8"))

						if (periods_num != 0) //multiline event
						{
							if (setcache.weekstartson == 0) //first day - sunday
							{
								evmove = periods_num * 7 - event_params.week;
							}
							else
							{
								evmove = periods_num * 7 - (((event_params.week == 0)?7:event_params.week) - 1);
							}
							from = move8(event_params.to8, evmove);
						}
						else
						{
							from = event_params.to8;
						}
						allday = ParseToNumber(evdata.event_allday);
						if (allday==1) {
							daysNumber=Math.round(ParseToNumber(event_container.style.width)/(100/7)); //number of days in this allday piece of event
						}
						else {
							daysNumber = 1;
						}
						till = move8(from, daysNumber-1);
						events_parts[i] = {
							event_container: event_container,
							event_id: event_id,
							from: from,
							allday: allday,
							daysNumber: daysNumber,
							till: till,
							fh: event_params.fh,
							calendar_id : calendar_id
						}
						i++;
					}
				}
			}
		}

		//sort arrays
		var events_parts_len, part_i, part_j, date, more_element, passed;
		var number_of_days, offsetTop, display_link, daymore;
		events_parts_len = events_parts.length;
		for (i=0; i<(events_parts_len-1); i++) {
			for (j=(i+1); j<events_parts_len; j++) {
				part_i = events_parts[i];
				part_j = events_parts[j];
				if (part_i.daysNumber < part_j.daysNumber) {
					events_parts[j] = part_i;
					events_parts[i] = part_j;
				}
				else if ((!part_i.allday) && (!part_j.allday)) {
					if ((part_i.from == part_j.from) && (part_i.fh > part_j.fh)) {
						events_parts[j] = part_i;
						events_parts[i] = part_j;
					}
				}
			}
		}
//delete all more elements
		for (date in more_dates) {
			more_element = $id('more_'+date);
			if (more_element != undefined && (more_element.parentNode == month_container)) {
				month_container.removeChild(more_element);
			}
		}

		more_dates = [];
		var column_values = [], mcr;
		for (i=0; i<events_parts.length; i++) {
			part_i = events_parts[i];
			hcol=1;
			passed = false;
			while (!passed) {
				column_values = [];
				for (j=0; j<i; j++) {
					part_j = events_parts[j];
					mcr = mcros(part_i, part_j);
					if (mcr) column_values.push(part_j.col);
				}
				passed=true;
			}
			hcol = 1;
			column_values = sortArray(column_values);
			for (var k=0; k<column_values.length; k++) {
				if (column_values[k] == hcol) {
					hcol++;
				}
				else {
					break;
				}
			}

			part_i.col=hcol;
			number_of_days = range8(showLimits.monthFrom, part_i.from);
			offsetTop = Math.floor(number_of_days/7); //cell row  there event must be
			offsetTop = Math.round((month_container_height/6)*offsetTop); //offset from top in pixels, depends on window height
			offsetTop=20+LINE_HEIGHT*(hcol-1)+offsetTop; //position with offset in cell
			part_i.event_container.style.top = offsetTop+"px";

			display_link = ((LINE_HEIGHT*hcol) > (cell_height-15));
			part_i.event_container.style.display = display_link ? "none" : "inline";

			if (display_link) {
				for (j=0; j<events_parts[i].daysNumber; j++) {
					daymore = move8(events_parts[i].from, j);
					if (more_dates[daymore] == undefined) {
						more_dates[daymore] = [];
					}
					more_dates[daymore].push(events_parts[i].event_id);
				}
			}
		}

		var links_number, vrange, visrow, viscol, vistxt, more_text;
		for (date in more_dates) {
			links_number = more_dates[date].length;
			vrange = range8(showLimits.monthFrom, date);
			visrow = Math.floor(vrange/7);
			viscol = vrange%7;
			vistxt = Lang.MonthMoreLink.replace('%d', links_number);
			more_element = document.createElement('div');
			more_element.className = "more_link";
			more_text = document.createTextNode(vistxt);
			more_element.appendChild(more_text);
			more_element.style.top = ((visrow+1)*(month_container_height/6)-16)+"px";
			more_element.style.left = (3+(100/7)*viscol)+"%";
			more_element.id = 'more_'+date;
			more_element.onclick = ShowMoreEventsList;
			month_container.appendChild(more_element);
		}
	}
	HideMoreEventsList();
}

function CreateAllEventsArray(section, this_day) {
	var cacheDays, cacheIds, common_section = [], allday_section = [], current_day;

	cacheDays = mycache.days;
	cacheIds = mycache.ids;
	this_day = Number(this_day);

	if ((current_day = cacheDays[this_day]) != undefined)
	{
		var j, commondaysNum = 0, alldaysNum = 0, allday_exist = [];
		var event_allday = 0, eventData = {}, event_id, real_event, atc;
		var len, hours_from, hours_till;

		for (j in current_day)
		{
			event_id = current_day[j];
			real_event = SplitEventId(event_id);

			if (cacheIds[real_event.id] == undefined)
			{
				delete mycache.days[this_day][j];
				delete cacheDays[this_day][j];
				continue;
			}
			if (cacheIds[real_event.id].event_repeats == 1
				&& mycache.repeats[event_id] != undefined)
			{
				if (mycache.exclusions[event_id] != undefined)
				{
					eventData = mycache.exclusions[event_id];
					event_allday = eventData.event_allday;
				}
				else
				{
					eventData = mycache.repeats[event_id];
					event_allday = cacheIds[real_event.id].event_allday;
				}
			}
			else
			{
				eventData = cacheIds[real_event.id];
				event_allday = eventData.event_allday;
			}

			if (event_allday == '1')
			{
				if (section != 'COMMON' && allday_exist[event_id] == undefined)
				{
					atc = atcalc(eventData);
					allday_section[alldaysNum] = {len:atc.len, st:atc.start, en:atc.end, col:1, event_id:event_id}
					allday_exist[event_id] = true;
					alldaysNum++;
				}
			}
			else
			{
				if (section != 'ALLDAY')
				{
					hours_from = GetDateParams(eventData.event_timefrom, false, new Array('FH'));
					hours_till = GetDateParams(eventData.event_timetill, false, new Array('FH'));
					len = hours_till.fh - hours_from.fh;
					common_section[commondaysNum] = {len:len, wid:1, data:eventData, event_id:event_id}
					commondaysNum++;
				}
			}
		}
	}
	return {common:common_section, allday:allday_section}
}

function ReRenderCommonSection(arr, mode, week_num) {
	var i, j, daysInPeriod, eventPosCache, len, mcol = 1,
		col, gone, movd, jk, event_id_i, width, ofs, left, event_id,
		_width, _left;

	daysInPeriod = (mode == 0) ? 1 : 7;
	eventPosCache = (mode == 0) ? mycache.d : mycache.w;

	_width = 0;
	_left = 0;

	len = arr.length;
	if (len > 0) {
		arr = tsort(arr);
		for (i=0; i<len; i++) {
			col=1;
			for (j=0; j<i; j++) {
				if (tcross(arr[j], arr[i]) && (arr[j].col >= col)) {
					col = 1 + arr[j].col;
					if (col>mcol) mcol = col;
				}
			}
			arr[i].col = col;
		}

		do {
			gone = false;
			for (i=0; i<len; i++) {
				movd = tmove(arr, i, mcol);
				if (movd.allow || !movd.colide) {
					gone = true;
					arr[i].wid = arr[i].wid + 1;
					for (j=0; j<movd.count; j++) {
						jk = movd.data[j];
						arr[jk].col = arr[jk].col + 1;
					}
				}
			}
		} while (gone);

		var year = mydate.getFullYear();
		for (i=0; i<len; i++) {
			event_id_i = arr[i].event_id;
			if (typeof(eventPosCache[event_id_i]) != "undefined") {
				if (typeof(eventPosCache[event_id_i]._width[year]) != "undefined"
					|| typeof(eventPosCache[event_id_i]._left[year]) != "undefined") {
					_width = eventPosCache[event_id_i]._width[year][week_num];
					_left = eventPosCache[event_id_i]._left[year][week_num];
				}
				else if (typeof(eventPosCache[event_id_i]._width[year+1]) != "undefined"
						|| typeof(eventPosCache[event_id_i]._left[year+1]) != "undefined") {
					_width = eventPosCache[event_id_i]._width[year+1][0];
					_left = eventPosCache[event_id_i]._left[year+1][0];
				}
				else if (typeof(eventPosCache[event_id_i]._width[year-1]) != "undefined"
					|| typeof(eventPosCache[event_id_i]._left[year-1]) != "undefined") {
					_width = eventPosCache[event_id_i]._width[year-1][week_num];
					_left = eventPosCache[event_id_i]._left[year-1][week_num];
				}
				if (_width != undefined && _left != undefined) {
					width = arr[i].wid * (_width/mcol);
					ofs = (arr[i].col - 1) * (100/daysInPeriod/mcol);
					left = ofs + _left;
					event_id = $id('event_'+event_id_i+'_0_'+mode+'_0');
					if (event_id != undefined) {
						event_id.style.width = width + '%';
						event_id.style.left = left + '%';
					}
				}
			}
		}
	} //end if len>0

}

function ReRenderAlldaySection(arr, mode) {
	var i, j, len = arr.length, prev_param, hcol, passed, next_param, event_id,
		allday_rows = 1, column_values = [], mcr;
	// number of allday events in allday section located one under another

	if (len > 0) {
		arr = tsort(arr);
		for (i=0; i<len; i++) {
			prev_param = arr[i];
			hcol = 1;
			passed = false;
			while (!passed) {
				column_values = [];
				for (j=0; j<i; j++) {
					next_param = arr[j];
					mcr = amcros(prev_param, next_param);
					if (mcr) column_values.push(next_param.col);
				}
				passed = true;
			}
			hcol = 1;
			column_values = sortArray(column_values);
			for (var k=0; k<column_values.length; k++) {
				if (column_values[k] == hcol) {
					hcol++;
				} else {
					break;
				}
			}
			arr[i].col = hcol;
			if (hcol > allday_rows) allday_rows = hcol;
			event_id = $id('event_'+arr[i].event_id+'_0_'+mode+'_1');
			if (event_id != undefined) {
				event_id.style.top = (LINE_HEIGHT*(hcol-1))+"px";//offsettop
			}
		}
	}
	Grid.RecalcDayWeekHeight(allday_rows, mode);
}

/*
 * Based on json.js (2007-07-03)
 * Modified by AfterLogic Corporation
 */
String.prototype.parseJSON = function (filter) {
	var j;

	function walk(k, v) {
		var i;
		if (v && typeof v === 'object') {
			for (i in v) {
				if (v.hasOwnProperty(i)) {
					v[i] = walk(i, v[i]);
				}
			}
		}
		return filter(k, v);
	}

	if (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]+$/.test(this.
		replace(/\\./g, '@').
		replace(/"[^"\\\n\r]*"/g, ''))) {
			j = eval('(' + this + ')');
			if (typeof filter === 'function') {
				j = walk('', j);
			}
			return j;
	}

	return false;
}

/*to Calendars class*/
/*
* return false - can't create event
* true - allow to create event
*/
function CheckAllowCreateEvent() {
	var allow = false, sharing_level;
	if (CheckAssociativeArrayIsEmpty(mycache.calendars) === null) {
		allow = false;
	} else {
		for (var calendar_id in mycache.calendars) {
			sharing_level = mycache.calendars[calendar_id].sharing_level;
			if (sharing_level == null || sharing_level == FULL_CONTROL || sharing_level == EDIT_EVENTS){
				allow = true;
				break;
			}
		}
	}
	return allow;
}
/*
 * return object {
 *  from - date object
 *  till - date object
 */
function GetCurrentTime() {
	var dateFrom = new Date(), dateTill;
	var minutesFrom = dateFrom.getMinutes();
	if (minutesFrom > 30) {
		dateFrom.setHours(dateFrom.getHours()+1, 0);
		dateTill = new Date(dateFrom);
		dateTill.setMinutes(30);
	} else {
		dateFrom.setMinutes(30);
		dateTill = new Date(dateFrom);
		dateTill.setHours(dateTill.getHours()+1, 0);
	}
	return {from:dateFrom, till:dateTill}
}

function GetDigitalTime(dateObj) {//time for ajax urls
	var hours = dateObj.getHours();
	var minutes = dateObj.getMinutes();
	return (Fnum(hours, 2) + ":" + Fnum(minutes, 2));
}

/*
 * SetResizeObject
 * MoveResize
 * EndResize
 */
function CResize() {
	this.element = null;
	this.DOWN               = true;
	this.UP                 = false;
	this.areaMouseDownLine  = 1;
	this.areaMouseUpLine    = 1;
	this.resize_type        = this.DOWN;
	this.allowResize        = false;
	this.parent             = null;
	this.eventDateUp		= null;
	this.eventDateDown		= null;
}

CResize.prototype = {
	SetResizeObject: function(domElement, type) {
		if (type == undefined) type = this.DOWN;
		this.resize_type = type;
		var eventParent = domElement.parentNode;
		if (eventParent == undefined || eventParent.id == undefined) return;
		var elemIdParams = Isplit(eventParent.id);
		if (elemIdParams.id == undefined || mycache.ids[elemIdParams.id] == undefined) return;
		var eventData, id_calendar;
		if (mycache.exclusions[elemIdParams.id_original] != undefined) {
			eventData = mycache.exclusions[elemIdParams.id_original];
			id_calendar = eventData.calendar_id;
		} else {
			eventData = mycache.ids[elemIdParams.id];
			id_calendar = eventData.calendar_id;
			if (eventData.event_repeats == 1) {
				eventData = mycache.repeats[elemIdParams.id_original]
			}
		}
		if (this.resize_type == this.UP) {
			this.areaMouseDownLine = Grid.GetGridLineByTime(eventData.event_timetill, true);
		} else {
			this.areaMouseDownLine = Grid.GetGridLineByTime(eventData.event_timefrom);
		}
		this.eventDateUp = new Date(eventData.event_timefrom);

		this.element = {
			parent: $id("event_" + elemIdParams.id_object),
			middle: $id("mid_" + elemIdParams.id_object),
			time: $id("time_" + elemIdParams.id_object),
			id_calendar: id_calendar,
			id_event_params : elemIdParams
		}
		if (elemIdParams.allday == 0) {
			Grid.SetResizeProperty(true);
			this.allowResize = true;
		}
	},
	MoveResize: function(e, parentDOM) {
		if (this.allowResize === false || parentDOM == undefined) return;
		this.parent = parentDOM;
		this.parent.style.cursor = "n-resize";
		this.element.parent.className = "event_selected";
		var height = 0, mouseUpTime, mouseDownTime, strTimeDown, strTimeUp, time = "";

		this.areaMouseUpLine = Grid.GetGridLine(e);

		mouseDownTime = Grid.GetLineTime(this.areaMouseDownLine);
		this.eventDateDown = new Date(this.eventDateUp);
		this.eventDateDown.setHours(mouseDownTime.hours, mouseDownTime.minutes);
		strTimeDown = GetDateParams(this.eventDateDown, true, new Array("TIME"));

		if (this.resize_type == this.UP) {
			height = this.areaMouseDownLine - this.areaMouseUpLine + 1;
			if (height >= 1) this.element.parent.style.top = LINE_HEIGHT * (this.areaMouseUpLine - 1) + "px";
			mouseUpTime = Grid.GetLineTime(this.areaMouseUpLine - 1);
			this.eventDateUp.setHours(mouseUpTime.hours, mouseUpTime.minutes);
			strTimeUp = GetDateParams(this.eventDateUp, false, new Array("TIME"));
			time = strTimeUp.time + " - " + strTimeDown.time;
		} else {
			height = this.areaMouseUpLine - this.areaMouseDownLine;
			mouseUpTime = Grid.GetLineTime(this.areaMouseUpLine);
			this.eventDateDown.setHours(mouseUpTime.hours, mouseUpTime.minutes);
			strTimeUp = GetDateParams(this.eventDateDown, false, new Array("TIME"));
			if (strTimeUp.time == "00:00") strTimeUp.time = "24:00";
			time = strTimeDown.time + " - " + strTimeUp.time;
		}

		if (height < 1) {
			height = 1;
		}
		height = height * LINE_HEIGHT;

		this.element.middle.style.height = (height - 4) + "px";
		this.element.parent.title = time;
		PasteTextNode(this.element.time, time);
	},
	EndResize: function(e) {
		if (this.parent == undefined) return;
		this.allowResize = false;
		this.areaMouseUpLine = Grid.GetGridLine(e);
		Grid.SetResizeProperty(false);
		this.parent.style.cursor = "default";
		this.element.parent.className = "event"

		ShowInfo(Lang.InfoSaving);
		var id_event = this.element.id_event_params.id;
		var id_original_event = this.element.id_event_params.id_original;
		var data = {
			id_data: SplitEventId(id_original_event),
			id_calendar: this.element.id_calendar,
			date_from: this.eventDateUp,
			date_till: this.eventDateDown
		}
		if (mycache.exclusions[id_original_event]) {
			ChooseForm.Show('RESIZE', data, 1);
		} else if (mycache.ids[id_event].event_repeats == 0) {
			ChooseForm.Show('RESIZE', data, 0);
		} else {
			ChooseForm.Show('RESIZE', data);
		}
		HideInfo();
	}
}

function FindPosY(obj)
{
	var currtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			currtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
	{
		currtop += obj.y;
	}
	return currtop;
}

function CalculateUntilValue(data) {
	var i, date_from, until_date, times, order, period;

	date_from = data.date;
	until_date = new Date(date_from);
	times = Number(data.repeat_times);
	order = Number(data.repeat_order);
	times = (times == 0) ? 1: times;
	period = times + (times-1)*(order-1) - 1;

	if (data.period == 0) {//day
		until_date.setDate(date_from.getDate() + period);
	}
	else if (data.period == 1) {//week
		var j, cbk, day, weekDayChecked, cnt, offset, n;
		day = date_from.getDay();
		weekDayChecked = [];
		for (i = 0, j=0, cbk = data.weekdays; i<cbk.length; i++) {
			if (cbk[i] == 1) {
				weekDayChecked[j] = i;
				j++;
			}
		}
		cnt = 0;
		for (i=0; i<weekDayChecked.length; i++) {
			if (day == weekDayChecked[i]) {
				cnt = i;
				break;
			}
		}
		offset = 0;
		if (weekDayChecked.length > 1) {
			n = 1;
			do {
				if (weekDayChecked[cnt+1] != undefined) {
					offset +=  weekDayChecked[cnt+1] - weekDayChecked[cnt];
					cnt++;
				}
				else {
					offset +=  7 - weekDayChecked[cnt] + weekDayChecked[0];
					cnt = 0;
				}
				n++;
			} while (n<times);
		}
		else {
			offset = 7*(times - 1);
		}
		until_date.setDate(date_from.getDate() + offset);
	} else if (data.period == 2 || data.period == 3) {//month, year
		if (data.week_number != null) {
			if (data.period == 2) {
				date_from.setMonth(date_from.getMonth() + period);
			}
			else {
				date_from.setFullYear(date_from.getFullYear() + period);
			}
			for (i=0; i<data.weekdays.length; i++) {
				if (data.weekdays[i] == 1) break;
			}
			until_date = GetDayInMonth(date_from, data.week_number, i);
		}
		else {
			var allow_month, allow_year, date, num;
			if (data.period == 2) {
				allow_month = 1;
				allow_year = 0;
			}
			else {
				allow_month = 0;
				allow_year = 1;
			}
			date = date_from.getDate();
			if (date == 29 || date == 30 || date == 31) {
				i = 0;
				num = 0;
				order++;
				while(num < times) {
					until_date = new Date((date_from.getFullYear() + order*i*allow_year), (date_from.getMonth() + order*i*allow_month), date_from.getDate());
					if (until_date.getDate() == date) {
						num++;
					}
					i++;
				}
			}
			else {
				until_date = new Date(date_from.getFullYear(), (date_from.getMonth() + period), date_from.getDate());
			}
		}
	}
	return until_date;
}

function getMainURL() {
	var main_url = String(document.location);
	main_url = main_url.substring(0,main_url.lastIndexOf("/"));
	return main_url;
}

function ObjectPropertiesCount(oObj)
{
	var iCount = 0;
	for (sPropertyName in oObj)
	{
		if (oObj.hasOwnProperty(sPropertyName))
		{
			iCount++;
		}
	}
	return iCount;
}
