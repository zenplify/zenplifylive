/**
 * initialize
 * setAppointmentContVisible
 * loadAppointments
 * closeAppontmentCont
 * addAppointment
 * deleteAppointment
 * setAppointmentContVisible
 * changeDisplay
 * changeArrow
 * loadAppointment
 * createAppointmentLine
 * removeAppointmentLine
 * createOwnerLine
 * changeAppointmentsCounts
 * setAppointmentsBlocksVisible
 * parseNewAppointments
 * getAppointmentsData
 * loadRespond
 * changeRespond
 * disableRespondCont
 */
function CAppointment()
{
	this.appointments				= [];
	this.appointmentsSave			= [];
	this.appointmentsDel			= [];
	this.statuses					= {awaiting: 0, yes: 1, no: 2, maybe: 3};
	this.counts						= {awaiting: 0, yes: 0, no: 0, maybe: 0};
	this.accessTypes				= {no: 0, full: 1};
	this.eventId					= 0;
	this.active						= false;
	this.access						= 0;
	this.appointmentsAccessType		= 0;

	// Get all needed elements
	this.eventRightContainer		= $id('eventRightContainer');
	this.expandAppointmentLink		= $id('expandAppointmentLink');
	this.expandAppointmentLinkCont	= $id('expandAppointmentLinkCont');
	this.closeAppointmentLink		= $id('closeAppointmentLink');
	this.appointmentCont			= $id('appointmentCont');
	this.eventLeftContainer			= $id('eventLeftContainer');
	this.eventEdit					= $id('eventEdit');
	this.eventContainer				= $id('eventcontainer');
	this.eventButtons				= $id('eventButtons');

	this.yesOpener					= $id('yesLinesOpener');
	this.noOpener					= $id('noLinesOpener');
	this.awaitingOpener				= $id('awaitingLinesOpener');
	this.maybeOpener				= $id('maybeLinesOpener');

	this.yesCount					= $id('yesCount');
	this.noCount					= $id('noCount');
	this.awaitingCount				= $id('awaitingCount');
	this.maybeCount					= $id('maybeCount');

	this.yesArrow					= $id('yesArrow');
	this.noArrow					= $id('noArrow');
	this.awaitingArrow				= $id('awaitingArrow');
	this.maybeArrow					= $id('maybeArrow');

	this.yesLines					= $id('linesYes');
	this.awaitingLines				= $id('linesAwaiting');
	this.noLines					= $id('linesNo');
	this.maybeLines					= $id('linesMaybe');

	this.newGuests					= $id('newGuests');
	this.appointmentRights			= $id('appointmentGuestsRights');

	this.appointmentRespondCont		= $id('appointmentRespondCont');
	this.appointmentRespond			= $id('appointmentRespond');
	this.appointmentChangeRespond1	= $id('appointmentChangeRespond1');
	this.appointmentChangeRespond2	= $id('appointmentChangeRespond2');
	this.appointmentChangeRespond3	= $id('appointmentChangeRespond3');

	this.appointmentAddGuestsCont	= $id("appointmentAddGuestsCont");
	this.appointmentGuestsRightsCont = $id("appointmentGuestsRightsCont");

	this.repeatCont					= $id('repeatCont');
	this.event_window				= $id('edit_window');
	this.event_form					= $id('edit_form');


	var obj = this;

	if (allowAppointments)
	{
		this.expandAppointmentLink.style.display = '';
	}
	else
	{
		this.expandAppointmentLink.style.display = 'none';
	}

	obj.expandAppointmentLink.onclick = function()
	{
//		obj.loadAppointments();
		obj.setAppointmentContVisible(true);
		obj.active = true;
	};
	obj.closeAppointmentLink.onclick = function()
	{
		obj.counts = {awaiting: 0, yes: 0, no: 0, maybe: 0};
		obj.closeAppontmentCont();
		obj.setAppointmentContVisible(false);
	};
	obj.yesOpener.onclick = function()
	{
		obj.changeArrow(obj.yesArrow);
		obj.changeDisplay(obj.yesLines);
	};
	obj.awaitingOpener.onclick = function()
	{
		obj.changeArrow(obj.awaitingArrow);
		obj.changeDisplay(obj.awaitingLines);
	};
	obj.noOpener.onclick = function()
	{
		obj.changeArrow(obj.noArrow);
		obj.changeDisplay(obj.noLines);
	};
}
CAppointment.prototype = {
	initialize : function(eventId)
	{
		this.access = 0;
		this.eventId = eventId;
		this.counts = {awaiting: 0, yes: 0, no: 0, maybe: 0};
		this.appointments			= [];
		this.appointmentsDel		= [];
		this.appointmentsSave		= [];
		this.appointmentsAccessType	= 0;
		this.access					= 0;
		CleanNode(this.yesLines);
		CleanNode(this.noLines);
		CleanNode(this.awaitingLines);
		CleanNode(this.maybeLines);
		CleanNode(this.appointmentRespond);
		CleanNode(this.appointmentChangeRespond1);
		CleanNode(this.appointmentChangeRespond2);
		CleanNode(this.appointmentChangeRespond3);
		this.newGuests.value = '';
		this.appointmentRights.checked = false;
		this.appointmentAddGuestsCont.style.display = "block";
		this.appointmentGuestsRightsCont.style.display = "none";
		this.appointmentRespondCont.style.display = 'none';
		this.loadAppointments.call(this);
	},
	loadAppointments : function()
	{
		var appointments, event, i;
		appointments = mycache.appointments[this.eventId];

		if (typeof(appointments) != 'undefined'
				&& typeof(this.eventId) != 'undefined'
				&& this.eventId != 0)
		{
			CleanNode(this.yesLines);
			CleanNode(this.noLines);
			CleanNode(this.awaitingLines);
			CleanNode(this.maybeLines);
			event = mycache.ids[this.eventId];
			this.appointmentsAccessType = parseInt(event.appointment_access);
			this.appointmentGuestsRightsCont.style.display = /*"block"*/"none";
			this.createOwnerLine.call(this, event.event_owner);
			if (!event.appointment)
			{
				this.appointmentGuestsRightsCont.style.display = /*"block"*/"none";
				this.appointmentRights.checked = (this.appointmentsAccessType == 1 ? true : false);
				this.appointmentRespondCont.style.display = 'block';
				PasteTextNode(this.appointmentRespond, Lang.AppointmentOrganizer);
			}
			else if (this.appointmentsAccessType != 1)
			{
				this.appointmentGuestsRightsCont.style.display = "none";
				this.appointmentAddGuestsCont.style.display = "none";
			}
			for (i in appointments)
			{
				this.loadAppointment.call(this, appointments[i], event);
			}
			this.setAppointmentContVisible(true);
			this.active = true;
		}
		else
		{
			CleanNode(this.yesLines);
			this.appointmentGuestsRightsCont.style.display = /*"block"*/"none";
			this.createOwnerLine.call(this, setcache.email);
			this.appointmentRespondCont.style.display = 'block';
			PasteTextNode(this.appointmentRespond, Lang.AppointmentOrganizer);
			this.setAppointmentContVisible(false);
			this.active = false;
		}
		this.changeAppointmentsCounts();
		this.setAppointmentsBlocksVisible();
	},
	closeAppontmentCont : function ()
	{
		for (var i in this.appointments)
		{
			this.appointmentsDel.push({
				id:this.appointments[i].id, email:this.appointments[i].email
			});
		}
		this.appointments		= [];
		this.appointmentsSave	= [];
		this.appointmentsAccessType = 0;
	},
	addAppointment : function(appointment)
	{
		if (typeof(appointment) != 'undefined')
		{
			this.appointments.push({
				id:appointment.appointmentId,
				eventId:appointment.eventId,
				email:appointment.email,
				status:parseInt(appointment.status),
				ownerEmail:appointment.ownerEmail
			});
		}
	},
	deleteAppointment : function (appointmentId)
	{
		var i = 0;
		if (typeof(appointmentId) != 'undefined')
		{
			for (i in this.appointments) {
				if(this.appointments[i].id == appointmentId)
				{
					// Append appointment line to parrent container
					switch (ParseToNumber(this.appointments[i].status))
					{
						case this.statuses.yes:
							this.counts.yes--;
							break;
						case this.statuses.no:
							this.counts.no--;
							break;
						case this.statuses.maybe:
							this.counts.maybe--;
							break;
						default:
							this.counts.awaiting--;
							break;
					}
					this.appointmentsDel.push({
						id:appointmentId, email:this.appointments[i].email
					});
					this.appointments.splice(i,1);
					break;
				}
			}
			this.changeAppointmentsCounts();
			this.setAppointmentsBlocksVisible();
		}
	},
	setAppointmentContVisible : function (mode)
	{
		if (mode)
		{
			this.eventRightContainer.style.display = 'block';
			EventForm.ChangeEventWindowSize(760);
			this.expandAppointmentLinkCont.style.display = 'none';
			this.eventButtons.style.marginTop = '-50px';
		}
		else
		{
			this.eventRightContainer.style.display = 'none';
			this.eventButtons.style.marginTop = '0px';
			this.expandAppointmentLinkCont.style.display = '';
			EventForm.ChangeEventWindowSize(435);
		}
		EventForm.RecalcEventWindowPosition();
	},
	changeDisplay : function(obj, mode)
	{
		var visible;
		if (typeof(mode) == 'undefined')
		{
			visible = (obj.style.display == 'none' ? 'block' : 'none');
		}
		else
		{
			visible = (mode ? 'block' : 'none');
		}
		obj.style.display = visible;
		EventForm.RecalcEventWindowPosition();
	},
	changeArrow : function (obj, mode)
	{
		var state;
		if (typeof(mode) == 'undefined')
		{
			state = (obj.className == 'lines_open_mode' ? 'close' : 'open');
		}
		else
		{
			state = (mode ? 'open' : 'close');
		}

		obj.className = 'lines_' + state + '_mode';
	},
	loadAppointment : function(appointment, event)
	{
		var calendar = mycache.calendars[event.calendar_id];
		var userId	=  calendar.user_id;
		var sharingLevel = calendar.sharing_level;
		if (appointment.userId == userId)
		{
			this.loadRespond.call(this, appointment);
		}
		this.addAppointment.call(this, appointment);
		this.createAppointmentLine.call(this, appointment, event.appointment, sharingLevel);
	},
	createAppointmentLine : function(appointment, isAppointment, sharingLevel)
	{
		var appointmentLine, guestName, appointmentDeleter;
		var obj = this;
		// Appointment Line
		appointmentLine = document.createElement('div');
		appointmentLine.className = 'guest_line';
		appointmentLine.id = 'appointment_' + appointment.appointmentId;
		// Guest Span
		guestName  = document.createElement('span');
		guestName.className = 'guest_name';
		// Guest Link
		PasteTextNode(guestName, appointment.email);
		// Deleter
		if ((!isAppointment || parseInt(this.appointmentsAccessType) == this.accessTypes.full) && (sharingLevel == EDIT_EVENTS || sharingLevel == FULL_CONTROL))
		{
			appointmentDeleter  = document.createElement('a');
			appointmentDeleter.className = 'link1 deleter';
			appointmentDeleter.href = 'javascript:void(0)';
			PasteTextNode(appointmentDeleter, Lang.Remove);
			// Add delete handler
			AddHandler(
				appointmentDeleter,
				"click",
				function()
				{
					obj.removeAppointmentLine(appointment.appointmentId);
					obj.deleteAppointment(appointment.appointmentId);
				}
			);
			// Append all elements
			appointmentLine.appendChild(appointmentDeleter);
		}

		appointmentLine.appendChild(guestName);

		// Append appointment line to parrent container
		switch (ParseToNumber(appointment.status))
		{
			case this.statuses.yes:
				this.yesLines.appendChild(appointmentLine);
				this.counts.yes++;
				break;
			case this.statuses.no:
				this.noLines.appendChild(appointmentLine);
				this.counts.no++;
				break;
			case this.statuses.maybe:
				this.maybeLines.appendChild(appointmentLine);
				this.counts.maybe++;
				break;
			default:
				this.awaitingLines.appendChild(appointmentLine);
				this.counts.awaiting++;
				break;
		}
	},
	removeAppointmentLine : function (appointmentId)
	{
		var appointmentLine, parent;
		appointmentLine	= $id('appointment_' + appointmentId);
		parent = appointmentLine.parentNode;
		if (typeof(appointmentLine) != 'undefined' && appointmentLine != null)
		{
			CleanNode(appointmentLine);
			appointmentLine.style.display = 'none';
			parent.removeChild(appointmentLine);
		}
	},
	createOwnerLine : function(event_owner)
	{
		var appointmentLine, ownerName, ownerNameLink, ownerCaption;
		// Appointment Line
		appointmentLine = document.createElement('div');
		appointmentLine.className = 'guest_line';
		// Guest Span
		ownerName  = document.createElement('span');
		ownerName.className = 'guest_name';
		PasteTextNode(ownerName, event_owner);
		// Caption
		ownerCaption  = document.createElement('span');
		ownerCaption.className = 'deleter';
		PasteTextNode(ownerCaption, Lang.AppointmentOrganizer);
		// Append all elements
		appointmentLine.appendChild(ownerCaption);
		appointmentLine.appendChild(ownerName);


		this.yesLines.appendChild(appointmentLine);
		this.counts.yes++;
	},
	changeAppointmentsCounts : function ()
	{
		var yesCount, noCount, awaitingCount, maybeCount;

		yesCount = (this.counts.yes > 0 ? '(' + this.counts.yes + ')' : '');
		noCount	= (this.counts.no > 0 ? '(' + this.counts.no + ')' : '');
		awaitingCount = (this.counts.awaiting > 0 ? '(' + this.counts.awaiting + ')' : '');
		maybeCount = (this.counts.maybe > 0 ? '(' + this.counts.maybe + ')' : '');

		PasteTextNode(this.yesCount, yesCount);
		PasteTextNode(this.noCount, noCount);
		PasteTextNode(this.awaitingCount, awaitingCount);
		PasteTextNode(this.maybeCount, maybeCount);
	},
	setAppointmentsBlocksVisible : function (noContVisible)
	{
		noContVisible = noContVisible || false;
		this.changeDisplay(this.yesOpener,
							(this.counts.yes > 0 ? true : false ));
		this.changeDisplay(this.yesLines,
							(this.counts.yes > 0 ? true : false ));
		this.changeArrow(this.yesArrow,
							(this.counts.yes > 0 ? true : false ));

		this.changeDisplay(this.maybeOpener,
							(this.counts.maybe > 0 ? true : false ));
		this.changeDisplay(this.maybeLines,
							(this.counts.maybe > 0 ? true : false ));
		this.changeArrow(this.maybeArrow,
							(this.counts.maybe > 0 ? true : false ));

		this.changeDisplay(this.noOpener,
							(this.counts.no > 0 ? true : false ));
		this.changeDisplay(this.noLines,
						(this.counts.no > 0 && noContVisible ? true : false ));
		this.changeArrow(this.noArrow,
						(this.counts.no > 0 && noContVisible ? true : false ));

		this.changeDisplay(this.awaitingOpener,
							(this.counts.awaiting > 0 ? true : false ));
		this.changeDisplay(this.awaitingLines,
							(this.counts.awaiting > 0 ? true : false ));
		this.changeArrow(this.awaitingArrow,
							(this.counts.awaiting > 0 ? true : false ));
	},
	parseNewAppointments : function ()
	{
		var isInvalid = false, guestsArr, guestsString = this.newGuests.value;
		var invalidEmails = [];
		this.appointmentsAccessType = (this.appointmentRights.checked ? 1 : 0);
		if (guestsString != '')
		{
			guestsString = guestsString.replace(/[\r]/, '');
			guestsArr = guestsString.split(/[,;\n]/);
			for (var i in guestsArr)
			{
				guestsArr[i] = guestsArr[i].replace(' ', '');
				if (guestsArr[i] != '' && !Validator.isCorrectEmail(guestsArr[i]))
				{
					invalidEmails.push(guestsArr[i]);
					isInvalid = true;
				}
			}
			if (isInvalid)
			{
				var srt = '';
				srt = Lang.AppointmentInvalidGuestEmail + "\n\n";
				for (var j in invalidEmails)
				{
					srt += invalidEmails[j] + "\n";
				}
				alert(srt);
				return false;
			}
			for (var i in guestsArr)
			{
				this.appointmentsSave.push({
					email:guestsArr[i]
				});
			}
		}
		return true;
	},
	getAppointmentsData : function()
	{
		return {
			save : this.appointmentsSave,
			del : this.appointmentsDel,
			access: this.appointmentsAccessType
		}
	},
	loadRespond : function(appointment)
	{
		var currentRespond, change1 = '', change2 = '', change3 = '';
		var change1Link, change2Link, change3Link, obj = this;
		var respond1, respond2, respond3;
		CleanNode(this.appointmentRespond);
		CleanNode(this.appointmentChangeRespond1);
		CleanNode(this.appointmentChangeRespond2);
		CleanNode(this.appointmentChangeRespond3);

		if (appointment)
		{
			switch (parseInt(appointment.status))
			{
				case this.statuses.yes:
					currentRespond = Lang.AppointmentRespondYes;
					change1 = Lang.AppointmentRespondMaybe;
					change2 = Lang.AppointmentRespondNo;
					respond1 = this.statuses.maybe;
					respond2 = this.statuses.no;
					break;
				case this.statuses.no:
					currentRespond = Lang.AppointmentRespondNo;
					change1 = Lang.AppointmentRespondYes;
					change2 = Lang.AppointmentRespondMaybe;
					respond1 = this.statuses.yes;
					respond2 = this.statuses.maybe;
					break;
				case this.statuses.maybe:
					currentRespond = Lang.AppointmentRespondMaybe;
					change1 = Lang.AppointmentRespondYes;
					change2 = Lang.AppointmentRespondNo;
					respond1 = this.statuses.yes;
					respond2 = this.statuses.no;
					break;
				default:
					currentRespond = Lang.AppointmentHaventRespond;
					change1 = Lang.AppointmentRespondYes;
					change2 = Lang.AppointmentRespondMaybe;
					change3 = Lang.AppointmentRespondNo;
					respond1 = this.statuses.yes;
					respond2 = this.statuses.maybe;
					respond3 = this.statuses.no;
					break;
			}
			this.appointmentRespondCont.style.display = 'block';
			this.appointmentRespond.className = '';
			PasteTextNode(this.appointmentRespond, currentRespond);

			change1Link  = document.createElement('a');
			change1Link.href="javascript:void(0)";
			change1Link.className = 'link1';
			PasteTextNode(change1Link, change1);
			AddHandler(
				change1Link,
				"click",
				function()
				{
					obj.changeRespond(appointment, respond1);
				}
				);
			this.appointmentChangeRespond1.appendChild(change1Link);

			change2Link  = document.createElement('a');
			change2Link.href="javascript:void(0)";
			change2Link.className = 'link1';
			PasteTextNode(change2Link, change2);
			AddHandler(
				change2Link,
				"click",
				function()
				{
					obj.changeRespond(appointment, respond2);
				}
				);
			this.appointmentChangeRespond2.appendChild(change2Link);

			if (change3 != '')
			{
				change3Link  = document.createElement('a');
				change3Link.href="javascript:void(0)";
				change3Link.className = 'link1';
				PasteTextNode(change3Link, change3);
				AddHandler(
					change3Link,
					"click",
					function() {
						obj.changeRespond.call(obj, appointment, respond3);
					}
					);
				this.appointmentChangeRespond3.appendChild(change3Link);
			}
		}
	},
	changeRespond : function(appointment, respond)
	{
		ShowInfo(Lang.InfoSaving);
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams, result;
		var eventId = appointment.eventId;
		var calendarId = mycache.ids[eventId].calendar_id;
		var appointmentId = appointment.appointmentId;
		this.disableRespondCont(appointment);
		switch (parseInt(appointment.status))
		{
			case this.statuses.yes:
				this.counts.yes--;
				break;
			case this.statuses.no:
				this.counts.no--;
				break;
			case this.statuses.maybe:
				this.counts.maybe--;
				break;
			default:
				this.counts.awaiting--;
				break;
		}

		requestParams = 'action=update_appointment' +
						'&calendar_id=' + calendarId +
						'&event_id=' + eventId +
						'&status=' + respond;
		if (req) {
			result = sendRequest(req, url, requestParams);
			ServErr(result,Lang.ErrorAppointmentChangeRespond);
		}

		if (respond == 2)
		{
			this.counts = {awaiting: 0, yes: 0, no: 0, maybe: 0};
			this.closeAppontmentCont();
			this.setAppointmentContVisible(false);

			this.repeatCont.style.display = "none";
			this.event_window.style.display ="none";
			calendarTableStart.Hide();
			calendarTableEnd.Hide();
			timeSelectorTill.Hide();
			timeSelectorFrom.Hide();
			this.event_form.style.display = 'none';
			Selection.DeleteSelection();

			DeleteEventInWebMail(eventId);
			DeleteEventFromCacheAndGrid(eventId);
			if (typeof(mycache.reminders[eventId]) != 'undefined') {
				delete mycache.reminders[eventId];
			}
			if (typeof(mycache.appointments[eventId]) != 'undefined') {
				delete mycache.appointments[eventId];
			}

		}
		else
		{
			this.removeAppointmentLine(appointment.appointmentId);
			this.loadRespond(result.appointment);
			this.createAppointmentLine(result.appointment, true, 0);
			this.changeAppointmentsCounts();
			this.setAppointmentsBlocksVisible(true);
			mycache.appointments[eventId][appointmentId] = result.appointment;
			if (respond == 3) {
				TentativeEventInWebMail(eventId);
			}
		}

		HideInfo();
	},
	disableRespondCont : function(appointment)
	{
		var currentRespond, change1 = '', change2 = '', change3 = '';
		switch (parseInt(appointment.status)) {
			case this.statuses.yes:
				currentRespond = Lang.AppointmentRespondYes;
				change1 = Lang.AppointmentRespondMaybe;
				change2 = Lang.AppointmentRespondNo;
				break;
			case this.statuses.no:
				currentRespond = Lang.AppointmentRespondNo;
				change1 = Lang.AppointmentRespondYes;
				change2 = Lang.AppointmentRespondMaybe;
				break;
			case this.statuses.maybe:
				currentRespond = Lang.AppointmentRespondMaybe;
				change1 = Lang.AppointmentRespondYes;
				change2 = Lang.AppointmentRespondNo;
				break;
			default:
				currentRespond = Lang.AppointmentHaventRespond;
				change1 = Lang.AppointmentRespondYes;
				change2 = Lang.AppointmentRespondMaybe;
				change3 = Lang.AppointmentRespondNo;
				break;
		}
		this.appointmentRespondCont.style.display = 'block';
		this.appointmentRespond.className = 'disable';
		PasteTextNode(this.appointmentRespond, currentRespond);
		PasteTextNode(this.appointmentChangeRespond1, change1);
		PasteTextNode(this.appointmentChangeRespond2, change2);
		PasteTextNode(this.appointmentChangeRespond3, change3);
	}
}