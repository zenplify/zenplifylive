/**
 * Show
 * RecalcShadowHeight
 * InlineEdit
 * SetEventName
 * SetNameToElements
 * CancelRepeatChooseForm
 * CalculateEventDates
 * CalculateUntilValue
 * 
 * SaveEvent
 * 
 * SaveDrag
 * SaveDragAll
 * SaveDragOne
 * 
 * SaveResize
 * SaveResizeAll
 * SaveResizeOne
 * 
 * SaveEdit
 * SaveEditAll
 * SaveEditOne
 * 
 * DeleteEvent
 * DeleteEventAll
 * DeleteEventOne
 */
function CChooseForm()
{
	this.repeat_choose_form	= $id('repeat_choose_form');
	this.repeat_choose		= $id('repeat_choose');
	this.repeat_choose_shadow = $id('repeat_chose_shadow');
	this.savebut_one		= $id('savebut_one');
	this.savebut_all		= $id('savebut_all');
	this.cancelbut_repeat	= $id('cancelbut_repeat1');
	this.choosecontainer    = $id('choosecontainer');

	this.repeatCont			= $id('repeatCont');
	this.eventForm			= $id('edit_form');
	this.eventWindow		= $id('edit_window');
	
	this.divConfirmEditRepeatEvent = $id('divConfirmEditRepeatEvent');

	this.action				= null;
	this.data				= null;
	this.chooseParams		= {all : 0, one : 1};
}
CChooseForm.prototype = {
	Show: function(action, data, save_type)
	{
		this.action = action;
		this.data = data;
		if (save_type == undefined)
		{
			var obj = this;
			this.savebut_one.onclick = function()
			{
				obj.SaveEvent(obj.chooseParams.one);
			};
			this.savebut_all.onclick = function()
			{
				obj.SaveEvent(obj.chooseParams.all);
			};
			this.cancelbut_repeat.onclick = function()
			{
				obj.CancelRepeatChooseForm(obj.action, obj.data);
			};
			if (mycache.calendars[data.id_calendar] != undefined)
			{
				var color_number = mycache.calendars[data.id_calendar]['color'];
				$id('choosecontainer').className = 'eventcontainer_'+color_number;
			}
			if (data.daily_period != '0' && action == 'DRAG')
			{
				this.divConfirmEditRepeatEvent.innerText = Lang.ConfirmEditRepeatEventNotDaily;
				this.savebut_all.style.display = 'none';
			}
			else
			{
				this.divConfirmEditRepeatEvent.innerText = Lang.ConfirmEditRepeatEvent;
				this.savebut_all.style.display = '';
			}
			
			this.repeat_choose.style.display = 'block';
			if (SafariDetect().isSafari)
			{
				this.repeat_choose_form.style.top = window.innerHeight / 2 + 'px';
			}
			setMaskHeight(this.repeat_choose);
			this.RecalcShadowHeight();
		}
		else
		{
			this.SaveEvent(save_type);
		}
	},
	RecalcShadowHeight: function()
	{
		var height = "auto";
		if ((this.choosecontainer.clientHeight - 4) > 0)
		{
			height = (this.choosecontainer.clientHeight - 4) + "px";
		}
		this.repeat_choose_shadow.style.height = height;
	},
	InlineEdit: function(param) {
		ShowInfo(Lang.InfoSaving);
		var id_event, id_repeat, id_recurrence, id_calendar, event_data;
		var repeat_data, exclusion, name;

		id_event = this.data.id_data.id;
		id_repeat = this.data.id_data.repeat;
		id_recurrence = this.data.id_data.date;
		id_calendar = this.data.id_calendar; //id_calendar of main event
		event_data = mycache.ids[id_event];
		repeat_data = mycache.repeats[this.data.id_data.id_original];
		exclusion = mycache.exclusions[this.data.id_data.id_original];
		name = this.data.name;

		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var new_name, result;
		var till_time, from_time;

		if (param == this.chooseParams.all) 
		{ //inline all
			requestParams = 'action=update_event' +
							'&event_id=' + id_event +
							'&calendar_id=' + id_calendar +
							'&name=' + name +
							'&allow_repeat=' + event_data.event_repeats +
							'&nocache=' + Math.random();

			if (req) 
			{
				result = sendRequest(req, url, requestParams);
				if (!ServErr(result,Lang.ErrorUpdateEvent)) 
				{
					new_name = result.event.event_name;
					this.SetEventName(this.data.id_data, new_name, true);
					mycache.ids[id_event].event_name = new_name;
				}
			}
		}
		else if (param == this.chooseParams.one) 
		{ // inline only the one
			var event_time_from = Fnum(event_data.event_timefrom.getHours(), 2) +
									':'+Fnum(event_data.event_timefrom.getMinutes(),2);
			requestParams = 'action=update_exclusion' +
							'&event_id=' + id_event +
							'&id_repeat=' + id_repeat +
							'&id_recurrence=' + id_recurrence +
							'&event_time_from=' + event_time_from +
							'&name=' + name +
							'&nocache=' + Math.random();
			if (exclusion != undefined) 
			{
				requestParams += '&calendar_id=' + exclusion.calendar_id +
								'&is_deleted=0';
			} 
			else 
			{
				till_time = Fnum(repeat_data.event_timetill.getHours(), 2) +
					':'+Fnum(repeat_data.event_timetill.getMinutes(), 2);
				from_time = Fnum(repeat_data.event_timefrom.getHours(), 2) +
					':'+Fnum(repeat_data.event_timefrom.getMinutes(), 2);

				requestParams += '&calendar_id=' + id_calendar +
								'&text=' + event_data.event_text +
								'&from=' + to8(repeat_data.event_timefrom) +
								'&till=' + to8(repeat_data.event_timetill) +
								'&time_from=' + from_time +
								'&time_till=' + till_time +
								'&allday=' + event_data.allday_flag +
								'&is_deleted=0';
			}
			if (req) 
			{
				result = sendRequest(req, url, requestParams);
				if (!ServErr(result,Lang.ErrorUpdateEvent)) 
				{
					this.SetEventName(this.data.id_data, result.event_name);
					var id_ex = result.event_id + '_' + result.id_repeat +
							'_' + result.id_recurrence;
					result.event_timefrom = dt2date(result.event_timefrom);
					event_timefrom = result.event_timefrom;
					result.event_timetill = dt2date(result.event_timetill);
					event_timetill = result.event_timetill;
					mycache.exclusions[id_ex] = result;
				}
			}
		}

		this.repeat_choose.style.display='none';
		HideInfo();
	},
	SetEventName: function (obj, name, allRepeats) {
		var viewCache = new Array(mycache.d, mycache.w, mycache.m);
		var mode, elem, common;
		for (mode=0; mode<viewCache.length; mode++) 
		{
			if (allRepeats != null && allRepeats) 
			{
				for (elem in viewCache[mode]) 
				{
					if (elem.split("_")[0] == obj.id && mycache.exclusions[elem] == undefined) 
					{
						common = {
							id:elem,
							box:obj.box,
							mode:mode,
							allday:obj.allday
						};
						this.SetNameToElements(common, mode, name);
					}
				}
			} 
			else 
			{//only one event
				common = {
					id:obj.id_original,
					box:obj.box,
					mode:mode,
					allday:obj.allday
				};
				this.SetNameToElements(common, mode, name);
			}
		}
	},
	SetNameToElements: function(obj, mode, name)
	{
		var common, parent, textDiv;

		common = obj.id + '_' + obj.box + '_' + obj.mode + '_' + obj.allday;
		parent = $id("event_" + common);
		textDiv= $id("textdiv_" + common);

		if (parent != undefined)
		{
			var i, id, pos = 0, parentTitle, id_str;
			parentTitle = ((pos = parent.title.indexOf("]")) < 0) ? name : parent.title.substr(0,pos+2)+name;
			if (mode == MONTH)
			{
				i = obj.box;
				if (i > 0)
				{
					id = parent;
					while (id != undefined)
					{
						id.title = parentTitle;
						if (textDiv != undefined)
						{
							CleanNode(textDiv);
							textDiv.appendChild(document.createTextNode(name));
						}
						i--;
						id_str = obj.id + "_" + i + "_" + obj.mode + "_" + obj.allday;
						id = $id("event_" + id_str);
						textDiv = $id("textdiv_" + id_str);
					}
				}
				i = 0;
				id = parent;
				while (id != undefined)
				{
					id.title = parentTitle;
					if (textDiv != undefined)
					{
						CleanNode(textDiv);
						textDiv.appendChild(document.createTextNode(name));
					}
					i++;
					id_str = obj.id + "_" + i + "_" + obj.mode + "_" + obj.allday;
					id = $id("event_" + id_str);
					textDiv = $id("textdiv_" + id_str);
				}
			}
			else
			{
				parent.title = parentTitle;
				if (textDiv != undefined)
				{
					CleanNode(textDiv);
					textDiv.appendChild(document.createTextNode(name));
				}
			}

		}
	},
	CancelRepeatChooseForm: function(action, data)
	{
		if (action == 'RESIZE' || action == 'DRAG')
		{
			var id_event = data.id_data.id;
			var id_repeat = data.id_data.repeat;
			var id_recurrence = data.id_data.date;
			var event = mycache.ids[id_event];
			event.id_repeat = id_repeat;
			event.id_recurrence = id_recurrence;
			var dell_dates = DeleteRepeatEventPart(id_event, id_repeat, id_recurrence);
			var add_dates = AddOneRepeatEventToCache(event);
			var dates = AddToArray(dell_dates, add_dates);
			RenderScreens(dates); //???
		}

		this.repeat_choose.style.display = 'none';
		setMaskHeight(this.repeat_choose);
	},
	CalculateEventDates: function(data) {
		var i, offset_day,  date_from, date_till, from_day;
		offset_day = 0;
		date_from = data.date_from;
		date_till = data.date_till;
		from_day = date_from.getDay();
		if (data.period == WEEK) 
		{ //week
			for (i = from_day; i<data.weekdays.length; i++)
			{
				if (data.weekdays[i] == 1) 
				{
					offset_day = i - from_day;
					break;
				}
			}
			if (offset_day == null) 
			{
				for (i = 0; i<from_day; i++)
				{
					if (data.weekdays[i] == 1) 
					{
						offset_day = 7 - from_day + i;
						break;
					}
				}
			}
			offset_day = (offset_day == null) ? 0: offset_day;
		}
		date_from.setDate(date_from.getDate() + offset_day);
		date_till.setDate(date_till.getDate() + offset_day);

		if ((data.period == MONTH || data.period == YEAR) && data.week_number != null) 
		{ //month, year
			for (i=0; i<data.weekdays.length; i++) 
			{
				if (data.weekdays[i] == 1) break;
			}
			var from1 = GetDayInMonth(date_from, data.week_number, i);
			if (from1.getTime()<date_from.getTime()) 
			{
				if (data.period == MONTH) 
				{
					date_from.setMonth(date_from.getMonth() + 1);
					date_till.setMonth(date_till.getMonth() + 1);
				} 
				else 
				{
					date_from.setFullYear(date_from.getFullYear() + 1);
					date_till.setFullYear(date_till.getFullYear() + 1);
				}
				date_from = GetDayInMonth(date_from, data.week_number, i);
				date_till = GetDayInMonth(date_till, data.week_number, i);
			} 
			else 
			{
				date_from = from1;
				date_till = GetDayInMonth(date_till, data.week_number, i);
			}
		}
		return {
			date_from:date_from,
			date_till:date_till
		}
	},
	CalculateUntilValue: function(data)
	{
		var i, date_from, until_date, times, order, period;

		date_from = data.date;
		until_date = new Date(date_from);
		times = Number(data.repeat_times);
		order = Number(data.repeat_order);
		times = (times == 0) ? 1: times;
		period = times + (times-1)*(order-1) - 1;

		if (data.period == 0) 
		{//day
			until_date.setDate(date_from.getDate() + period);
		} 
		else if (data.period == 1) 
		{//week
			var j, cbk, day, weekDayChecked, cnt, offset, n;
			day = date_from.getDay();
			weekDayChecked = [];
			for (i = 0, j=0, cbk = data.weekdays; i<cbk.length; i++) 
			{
				if (cbk[i] == 1) 
				{
					weekDayChecked[j] = i;
					j++;
				}
			}
			cnt = 0;
			for (i=0; i<weekDayChecked.length; i++)
			{
				if (day == weekDayChecked[i]) 
				{
					cnt = i;
					break;
				}
			}
			offset = 0;
			if (weekDayChecked.length > 1) 
			{
				n = 1;
				do {
					if (weekDayChecked[cnt+1] != undefined) 
					{
						offset +=  weekDayChecked[cnt+1] - weekDayChecked[cnt];
						cnt++;
					} 
					else 
					{
						offset +=  7 - weekDayChecked[cnt] + weekDayChecked[0];
						cnt = 0;
					}
					n++;
				} while (n<times);
			} 
			else 
			{
				offset = 7*(times - 1);
			}
			until_date.setDate(date_from.getDate() + offset);
		} 
		else if (data.period == 2 || data.period == 3) 
		{//month, year
			if (data.week_number != null) 
			{
				if (data.period == 2) 
				{
					date_from.setMonth(date_from.getMonth() + period);
				} else 
				{
					date_from.setFullYear(date_from.getFullYear() + period);
				}
				for (i=0; i<data.weekdays.length; i++) 
				{
					if (data.weekdays[i] == 1) break;
				}
				until_date = GetDayInMonth(date_from, data.week_number, i);
			} else 
			{
				var allow_month, allow_year, date, num;
				if (data.period == 2) 
				{
					allow_month = 1;
					allow_year = 0;
				} 
				else 
				{
					allow_month = 0;
					allow_year = 1;
				}
				date = date_from.getDate();
				if (date == 29 || date == 30 || date == 31) 
				{
					i = 0;
					num = 0;
					order++;
					while(num < times) {
						until_date = new Date((date_from.getFullYear() + order*i*allow_year),
												(date_from.getMonth() + order*i*allow_month),
												date_from.getDate());
						if (until_date.getDate() == date) {
							num++;
						}
						i++;
					}
				} 
				else 
				{
					until_date = new Date(date_from.getFullYear(), (date_from.getMonth() + period), date_from.getDate());
				}
			}
		}
		return until_date;
	},
	SaveEvent: function(param)
	{
		switch (this.action)
		{
			case 'RESIZE':
				this.SaveResize(param);
				break;
			case 'DRAG':
				this.SaveDrag(param);
				break;
			case 'EDIT':
				this.SaveEdit(param);
				break;
			case 'DELETE':
				this.DeleteEvent(param);
				break;
			case 'INLINE':
				this.InlineEdit(param);
				break;
			default:
				break;
		}
	},
	SaveEdit: function(param) {
		ShowInfo(Lang.InfoSaving);
		this.repeat_choose.style.display = "none";
		var dates;
		switch (param) 
		{
			case this.chooseParams.all:
				dates = this.SaveEditAll();
				break;
			case this.chooseParams.one:
				dates = this.SaveEditOne();
				break;
			default:
				break;
		}
		if (dates != null) RenderScreens(dates);
		Grid.SetWorkAreaOffset();
		HideInfo();
	},
	SaveEditAll : function () {
		var idCalendar, idEvent, eventData, repeatData, formData;
		var dates, dateFrom, dateTill;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], result;
		var remindersSave, reminderDel, appointmentsSave, appointmentsDel, appointmentsAccess;
		var arr, arr1, eventDates, dellDates, addDates, i;

		formData = this.data;

		idEvent = formData.id_data.id;
		idCalendar = formData.id_calendar;
		eventData = mycache.ids[idEvent];
		repeatData = mycache.repeats[formData.id_data.original];

		dateFrom = formData.date_from;
		dateTill = formData.date_till;

		if (idEvent != 0 && repeatData != undefined)
		{
			var cacheFrom, cacheTill, offset;
			cacheFrom = repeatData.event_timefrom;
			cacheTill = repeatData.event_timetill;
			if (to8(dateFrom) == to8(cacheFrom) && to8(dateTill) == to8(cacheTill)) 
			{
				dateFrom = eventData.event_timefrom;
				dateTill = eventData.event_timetill;
			} 
			else 
			{
				offset = Math.ceil((dateFrom.getTime() - cacheFrom.getTime())/86400000);
				dateFrom = new Date(eventData.event_timefrom);
				dateFrom.setDate(dateFrom.getDate() + offset);
				offset = Math.ceil((dateTill.getTime() - cacheTill.getTime())/86400000);
				dateTill = new Date(eventData.event_timetill);
				dateTill.setDate(dateTill.getDate() + offset);
			}
		}
		arr1 = {
			date_from : dateFrom,
			date_till : dateTill,
			period : formData.repeat_period,
			weekdays : formData.weekdays,
			week_number : formData.week_number
		};
		eventDates = this.CalculateEventDates(arr1);

		requestParams = 'action=update_event' +
						'&event_id=' + idEvent +
						'&name=' + formData.event_name +
						'&text=' + formData.event_text +
						'&location=' + formData.event_location +
						'&from=' + to8(eventDates.date_from) +
						'&till=' + to8(eventDates.date_till) +
						'&time_from=' + formData.time_from +
						'&time_till=' + formData.time_till +
						'&allday=' + formData.allday +
						'&eventChanged=' + formData.eventChanged +
						'&nocache=' + Math.random();

		remindersSave = formData.reminders_save;
		if (remindersSave != undefined && remindersSave.length > 0) 
		{
			for (i=0; i<remindersSave.length; i++) 
			{
				requestParams += '&reminder_id[' + i + ']='+remindersSave[i].Id +
								'&reminder_offset[' + i + ']=' + remindersSave[i].Time;
			}
		}
		reminderDel = formData.reminders_del;
		if (reminderDel != undefined && reminderDel.length > 0) 
		{
			for (i=0; i<reminderDel.length; i++) 
			{
				requestParams += '&reminder_delete[' + i + ']='+reminderDel[i];
			}
		}
		appointmentsSave = formData.appointments_save;
		if (typeof(appointmentsSave) != 'undefined' && appointmentsSave.length > 0) 
		{
			for (i in appointmentsSave) 
			{
				requestParams += '&appointments_save[' + i + ']='+appointmentsSave[i].email;
			}
		}
		appointmentsDel = formData.appointments_del;
		if (typeof(appointmentsDel) != 'undefined' && appointmentsDel.length > 0) 
		{
			for (i in appointmentsDel) 
			{
				requestParams += '&appointments_delete[' + i + ']='+appointmentsDel[i].email;
			}
		}
		appointmentsAccess = formData.appointments_access;
		if (typeof(appointmentsAccess) != 'undefined') 
		{
			requestParams += '&appointments_access=' + appointmentsAccess;
		}

		if (idEvent == 0) 
		{
			requestParams += '&calendar_id=' + idCalendar;
		} 
		else 
		{
			var oldCalendarId = eventData.calendar_id;
			if (oldCalendarId != idCalendar) 
			{
				requestParams += '&new_calendar_id=' + idCalendar +
					'&calendar_id=' + oldCalendarId;
			} 
			else 
			{
				requestParams += '&calendar_id=' + idCalendar;
			}
		}

		if ((formData.repeat_until != null
				&& (to8(eventDates.date_from)>=to8(formData.repeat_until)))
			|| formData.allow_repeat == 0) 
		{
			requestParams += '&allow_repeat=0';
		} 
		else 
		{
			if (formData.allow_repeat != 0) 
			{
				requestParams += '&repeat_period=' + formData.repeat_period +
								'&repeat_end=' + formData.repeat_end +
								'&repeat_order=' + formData.repeat_order +
								'&sun=' + formData.weekdays[0] +
								'&mon=' + formData.weekdays[1] +
								'&tue=' + formData.weekdays[2] +
								'&wed=' + formData.weekdays[3] +
								'&thu=' + formData.weekdays[4] +
								'&fri=' + formData.weekdays[5] +
								'&sat=' + formData.weekdays[6] +
								'&allow_repeat=1';
				if (formData.repeat_end == 1) 
				{
					arr = {
						date : new Date(eventDates.date_from),
						period : formData.repeat_period,
						repeat_times : formData.repeat_times,
						repeat_order : formData.repeat_order,
						weekdays : formData.weekdays,
						week_number : formData.week_number
					}
					var untilDate = this.CalculateUntilValue(arr);

					requestParams += '&repeat_until=' + to8(untilDate) +
									'&repeat_times=' + formData.repeat_times;
				}
				if (formData.repeat_end == 2)
				{
					requestParams += '&repeat_until=' + to8(formData.repeat_until) +
									'&repeat_times=0';
				}
				if (formData.repeat_period == 2 || formData.repeat_period == 3) 
				{//month
					if (formData.week_number != null) 
					{
						requestParams += '&week_number=' + formData.week_number;
					}
				}
			} 
			else 
			{
				requestParams += '&allow_repeat=0';
			}
		}

		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			if (!ServErr(result, Lang.ErrorUpdateEvent)) 
			{
				event = result.event;
				HideInfo();
				if (typeof(result.appointments) != 'undefined'
					&& (typeof(result.appointments.length) == 'undefined' || result.appointments.length > 0)) 
				{
					ShowReport(Lang.ReportEventSaved + Lang.ReportAppointmentSaved + '.');
				} 
				else 
				{
					ShowReport(Lang.ReportEventSaved + '.');
				}
				if (idEvent != 0) 
				{
					dellDates = DeleteEventFromCacheAndGrid(idEvent);
				}
				for (var ex in result.exclusion) 
				{
					var exclusion = result.exclusion[ex];
//					var idEx = exclusion.event_id + '_' + exclusion.id_repeat;
//					mycache.exclusions[idEx] = exclusion;
					mycache.exclusions[ex] = exclusion;
				}
				idEvent = event.event_id;
				addDates = AddEventToCache(event);
				if (dellDates == undefined) 
				{
					dates = addDates;
				} 
				else 
				{
					dates = AddToArray(dellDates, addDates);
				}

				var reminders = result.reminders[idEvent];
				if (typeof(reminders) == 'undefined') 
				{
					delete mycache.reminders[idEvent];
				} 
				else if (typeof(reminderDel) != 'undefined' && reminderDel.length > 0) 
				{
					for (i in reminderDel) 
					{
						delete mycache.reminders[idEvent][reminderDel[i]];
					}
				} else {
					for (var n in reminders) 
					{
						if (typeof(mycache.reminders[idEvent]) == 'undefined') 
						{
							mycache.reminders[idEvent] = [];
						}
						mycache.reminders[idEvent][reminders[n].id_reminder] = reminders[n];
					}
				}

				var appointments = result.appointments[idEvent];
				if (typeof(appointments) == 'undefined')
				{
					delete mycache.appointments[idEvent];
				} 
				else
				{
					mycache.appointments[idEvent] = appointments;
				}
				EventForm.ClearEventForm();
				BuildEvent(dates, event.event_id);
			} 
			else 
			{
				if (formData.allow_repeat != 0) 
				{
					this.repeatCont.style.display = "block";
				}
				this.eventForm.style.display = "block";
				this.eventWindow.style.display = "block";
			}
		}
		return dates;
	},
	SaveEditOne : function () {
		var idCalendar, idEvent, idRepeat, idRecurrenceDate;
		var eventData, formData, eventTimeFrom;
		var dates, dateFromFormatted, dateTillFormatted;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], dellDates, addDates, exclusions, result, i;

		formData = this.data;

		idEvent = formData.id_data.id;
		idRepeat = formData.id_data.repeat;
		idRecurrenceDate = formData.id_data.date;
		idCalendar = formData.id_calendar;
		eventData = mycache.ids[idEvent];

		dateFromFormatted = to8(formData.date_from);
		dateTillFormatted = to8(formData.date_till);

		eventTimeFrom =  Fnum(eventData.event_timefrom.getHours(), 2) + ':' + Fnum(eventData.event_timefrom.getMinutes(),2);
		requestParams = 'action=update_exclusion' +
						'&event_id=' + idEvent +
						'&calendar_id=' + idCalendar +
						'&id_repeat='+ idRepeat +
						'&id_recurrence='+ idRecurrenceDate +
						'&event_time_from=' + eventTimeFrom +
						'&name=' + formData.event_name +
						'&text=' + formData.event_text +
						'&location=' + formData.event_location +
						'&from=' + dateFromFormatted +
						'&till=' + dateTillFormatted +
						'&time_from=' + formData.time_from +
						'&time_till=' + formData.time_till+
						'&allday=' + formData.allday +
						'&is_deleted=0' +
						'&nocache' + Math.random();
		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			if (!ServErr(result, Lang.ErrorUpdateEvent)) 
			{
				dellDates = DeleteRepeatEventPart(idEvent, idRepeat, idRecurrenceDate);
				event = result;
				var idEx = event.event_id + '_' + event.id_repeat + '_' + event.id_recurrence;
				mycache.exclusions[idEx] = event;
				addDates = AddOneRepeatEventToCache(event);
				EventForm.ClearEventForm();
			} 
			else 
			{
				exclusions = cloneObj(mycache.exclusions);
				dellDates = DeleteEventFromCacheAndGrid(idEvent);
				event = eventData;
				mycache.exclusions = exclusions;
				addDates = AddEventToCache(event);
				BuildEvent(addDates, event.event_id);
				if (formData.allow_repeat != 0) 
				{
					this.repeatCont.style.display = "block";
				}
				this.eventForm.style.display = "block";
				this.eventWindow.style.display = "block";
			}
			dates = AddToArray(dellDates, addDates);
		}
		return dates;
	},
	SaveDrag: function (param) {
		ShowInfo(Lang.InfoSaving);
		this.repeat_choose.style.display = 'none';
		switch (param) 
		{
			case this.chooseParams.all:
				this.SaveDragAll();
				break;
			case this.chooseParams.one:
				this.SaveDragOne();
				break;
			default:
				break;
		}
	},
	SaveDragAll : function () {
		var idCalendar, idEvent;
		var dates = null, fromDate, tillDate, tillTime, untilDate;
		var eventData, repeatData, arr, arr1;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], dellDates, addDates, ex, exclusion, exclusions, idEx, result;
		var d1, d2, days, eventDates, allowRepeat;

		fromDate = this.data.from_date;
		idEvent = this.data.id_data.id;
		idCalendar = this.data.id_calendar;
		eventData = mycache.ids[idEvent];
		repeatData = mycache.repeats[this.data.id_data.id_original];

		if (repeatData != undefined) 
		{
			d1 = new Date(this.data.prev_from_date.getFullYear(), this.data.prev_from_date.getMonth(), this.data.prev_from_date.getDate());
			d2 = new Date(repeatData.event_timefrom.getFullYear(), repeatData.event_timefrom.getMonth(), repeatData.event_timefrom.getDate());
			days = Math.round((d2.getTime() - d1.getTime())/86400000);
			fromDate.setTime(fromDate.getTime() - days*86400000);
		}
		tillDate = new Date(fromDate.getTime() + this.data.event_len);
		arr1 = {
			date_from : fromDate,
			date_till : tillDate,
			days : days,
			period : eventData.repeat_period,
			weekdays : new Array(eventData.sun, eventData.mon, eventData.tue, eventData.wed, eventData.thu, eventData.fri, eventData.sat),
			week_number : eventData.week_number
		};
		eventDates = this.CalculateEventDates(arr1); //correct event_dates if event repeats

		tillTime = Fnum(tillDate.getHours(),2) + ":" + Fnum(tillDate.getMinutes(),2);
		allowRepeat = (eventData.event_repeats == 1) ? 1 : 0;

		arr = {
			date : new Date(eventDates.date_from),
			period : eventData.repeat_period,
			repeat_times : ParseToNumber(eventData.repeat_num),
			repeat_order : ParseToNumber(eventData.repeat_order),
			weekdays : new Array(eventData.sun, eventData.mon, eventData.tue, eventData.wed, eventData.thu, eventData.fri, eventData.sat),
			week_number : eventData.week_number
		};

		requestParams = 'action=update_event' +
				'&event_id=' + idEvent +
				'&calendar_id=' + idCalendar +
				'&from=' + to8(eventDates.date_from) +
				'&time_from=' + this.data.from_time +
				'&till=' + to8(eventDates.date_till) +
				'&time_till=' + tillTime +
				'&allday=' + eventData.allday_flag +
				'&nocache=' + Math.random();
		if (allowRepeat == 1) 
		{
			if (eventData.repeat_end == 1) 
			{
				untilDate = this.CalculateUntilValue(arr);
				requestParams += '&repeat_until=' + to8(untilDate);
			}
			if (eventData.repeat_end == 2) 
			{
				if (to8(eventDates.date_from) >= to8(eventData.repeat_until)) {
					allowRepeat = 0;
				} 
				else 
				{
					requestParams += '&repeat_until=' + to8(eventData.repeat_until);
				}
			}
		}
		requestParams += '&allow_repeat=' + allowRepeat;

		if (req) {
			result = sendRequest(req, url, requestParams);
			exclusions = cloneObj(mycache.exclusions);
			dellDates = DeleteEventFromCacheAndGrid(idEvent);
			if (!ServErr(result, Lang.ErrorUpdateEvent)) {
				event = result.event;
				exclusion = result.exclusion;
				for (ex in result.exclusion)
				{
					exclusion = result.exclusion[ex];
					idEx = exclusion.event_id + '_' + exclusion.id_repeat + '_' + exclusion.id_recurrence;
					mycache.exclusions[idEx] = exclusion;
				}
			} 
			else 
			{
				event = eventData;
				mycache.exclusions = exclusions;
			}
			addDates = AddEventToCache(event);
			BuildEvent(addDates, event.event_id);
			dates = AddToArray(dellDates, addDates);
		}
		if (dates != null) RenderScreens(dates);
		Grid.DeleteShadowElement();
		HideInfo();
		ShowReport(Lang.ReportEventSaved + '.');
	},
	SaveDragOne : function () {
		var idCalendar, idEvent, idRepeat, idRecurrenceDate;
		var dates, fromDate, tillDate, tillTime;
		var eventData, repeatData, eventTimeFrom;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], dellDates, addDates, exclusion, idEx, result;

		fromDate = this.data.from_date;
		idEvent = this.data.id_data.id;
		idRepeat = this.data.id_data.repeat;
		idRecurrenceDate = this.data.id_data.date;
		idCalendar = this.data.id_calendar;
		eventData = mycache.ids[idEvent];
		repeatData = mycache.repeats[this.data.id_data.id_original];
		event = mycache.ids[idEvent];
		eventTimeFrom =  Fnum(repeatData['event_timefrom'].getHours(), 2)+':'+Fnum(repeatData['event_timefrom'].getMinutes(),2);
		exclusion = mycache.exclusions[idEvent + '_' + idRepeat + '_' + idRecurrenceDate];

		requestParams = 'action=update_exclusion&event_id=' + idEvent +
				'&id_repeat='+ idRepeat +
				'&id_recurrence='+ idRecurrenceDate +
				'&event_time_from=' + eventTimeFrom +
				'&is_deleted=0' +
				'&from=' + to8(fromDate) +
				'&time_from=' + this.data.from_time +
				'&nocache=' + Math.random();

		if (exclusion != undefined) 
		{
			var eventLen = exclusion.event_timetill.getTime() - exclusion.event_timefrom.getTime(); //in milliseconds
			tillDate = new Date(fromDate.getTime() + eventLen);
			tillTime = Fnum(tillDate.getHours(),2) + ':' + Fnum(tillDate.getMinutes(),2);
			requestParams += '&calendar_id=' + exclusion.calendar_id +
				'&till=' + to8(tillDate) +
				'&time_till=' + tillTime;
		} 
		else 
		{
			tillDate = new Date(fromDate.getTime() + this.data.event_len);
			tillTime = Fnum(tillDate.getHours(),2) + ':' + Fnum(tillDate.getMinutes(),2);
			requestParams += '&name=' + eventData.event_name +
				'&calendar_id=' + idCalendar +
				'&text=' + eventData.event_text +
				'&allday=' + eventData.allday_flag +
				'&till=' + to8(tillDate) +
				'&time_till=' + tillTime;
		}

		if (req)
		{
			result = sendRequest(req, url, requestParams);
			dellDates = DeleteRepeatEventPart(idEvent, idRepeat, idRecurrenceDate);
			if (ServErr(result, Lang.ErrorUpdateEvent))
			{
				event = eventData;
			} 
			else 
			{
				event = result;
				idEx = event.event_id + '_' + event.id_repeat + '_' + event.id_recurrence;
				mycache.exclusions[idEx] = event;
			}
			addDates = AddOneRepeatEventToCache(event);
			dates = AddToArray(dellDates, addDates);
		}
		if (dates != null) RenderScreens(dates);
		Grid.DeleteShadowElement();
		HideInfo();
		ShowReport(Lang.ReportEventSaved + '.');
	},
	SaveResize: function (param) {
		ShowInfo(Lang.InfoSaving);
		this.repeat_choose.style.display = "none";
		var dates;
		switch (param) 
		{
			case this.chooseParams.all:
				dates = this.SaveResizeAll();
				break;
			case this.chooseParams.one:
				dates = this.SaveResizeOne();
				break;
			default:
				break;
		}
		if (dates != null) RenderScreens(dates);
		Grid.SetWorkAreaOffset();
		HideInfo();
		ShowReport(Lang.ReportEventSaved + '.');

	},
	SaveResizeAll : function () {
		var idCalendar, idEvent, eventData;
		var dates, dateFrom, dateTill, dateFromFormatted, dateTillFormatted, timeFrom, timeTill;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], exclusion, exclusions, idEx, result, allowRepeat;
		dates = null;
		idEvent = this.data.id_data.id;
		idCalendar = this.data.id_calendar;
		eventData = mycache.ids[idEvent];
		dateFrom = this.data.date_from;
		dateTill = this.data.date_till;
		
		dateFromFormatted = to8(dateFrom);
		dateTillFormatted = to8(dateTill);
		timeFrom = GetDigitalTime(dateFrom);
		timeTill = GetDigitalTime(dateTill);

		if (dateFrom == null || dateTill == null) 
		{
			alert(Lang.WarningIncorrectDate);
			return;
		} 
		else 
		{
			if (dateFrom.getTime() > dateTill.getTime())
			{
				alert(Lang.WarningStartEndDate);
				return;
			}
		}	

		allowRepeat = (eventData.event_repeats == 1)?1:0;

		requestParams = 'action=update_event' +
						'&calendar_id=' + idCalendar +
						'&event_id=' + idEvent +
						'&from=' + dateFromFormatted +
						'&till=' + dateTillFormatted +
						'&time_from=' + timeFrom +
						'&time_till=' + timeTill +
						'&allday=' + eventData.allday_flag +
						'&allow_repeat=' + allowRepeat +
						'&nocache='+ Math.random();

		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			exclusions = cloneObj(mycache.exclusions);
			DeleteEventFromCacheAndGrid(idEvent);
			if (!ServErr(result, Lang.ErrorUpdateEvent)) 
			{
				event = result.event;
				for (var ex in result.exclusion) 
				{
					exclusion = result.exclusion[ex];
					idEx = exclusion.event_id + '_' + exclusion.id_repeat;
					mycache.exclusions[idEx] = exclusion;
				}
			} 
			else 
			{
				event = eventData;
				mycache.exclusions = exclusions;
			}
			dates = AddEventToCache(event);
			BuildEvent(dates, event.event_id);
		}
		return dates;
	},
	SaveResizeOne : function () {
		var idCalendar, idEvent, idRepeat, idRecurrenceDate;
		var eventData, repeatData, formData;
		var dates, dateFromFormatted, dateTillFormatted;
		var eventTimeFrom, timeFrom, timeTill;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		var event = [], dellDates, addDates, exclusion, exclusions, idEx, result;

		formData = this.data;

		idEvent = formData.id_data.id;
		idRepeat = formData.id_data.repeat;
		idRecurrenceDate = formData.id_data.date;
		idCalendar = formData.id_calendar;
		eventData = mycache.ids[idEvent];
		repeatData = mycache.repeats[formData.id_data.original];
		dateFromFormatted = to8(formData.date_from);
		dateTillFormatted = to8(formData.date_till);
		timeFrom = GetDigitalTime(formData.date_from);
		timeTill = GetDigitalTime(formData.date_till);

		eventTimeFrom =  Fnum(repeatData.event_timefrom.getHours(), 2)+':'+Fnum(repeatData.event_timefrom.getMinutes(),2);
		exclusion = mycache.exclusions[idEvent + '_' + idRepeat + '_' + idRecurrenceDate];

		requestParams = 'action=update_exclusion' +
						'&event_id=' + idEvent +
						'&calendar_id=' + idCalendar +
						'&id_repeat=' + idRepeat +
						'&id_recurrence=' + idRecurrenceDate +
						'&event_time_from=' + eventTimeFrom +
						'&is_deleted=0' +
						'&from=' + dateFromFormatted +
						'&till=' + dateTillFormatted +
						'&time_from=' + timeFrom +
						'&time_till=' + timeTill +
						'&nocache=' + Math.random();
		if (exclusion == undefined) 
		{
			requestParams += '&name=' + eventData.event_name +
							'&text=' + eventData.event_text +
							'&allday=' + eventData.allday_flag;
		}
		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			if (ServErr(result, Lang.ErrorUpdateEvent)) 
			{
				exclusions = cloneObj(mycache.exclusions);
				dellDates = DeleteEventFromCacheAndGrid(idEvent);
				event = eventData;
				mycache.exclusions = exclusions;
				addDates = AddEventToCache(event);
				BuildEvent(addDates, event.event_id);
			} 
			else 
			{
				dellDates = DeleteRepeatEventPart(idEvent, idRepeat, idRecurrenceDate);
				event = result;
				idEx = event.event_id + '_' + event.id_repeat + '_' + event.id_recurrence;
				mycache.exclusions[idEx] = event;
				addDates = AddOneRepeatEventToCache(event);
			}
			dates = AddToArray(dellDates, addDates);
		}
		return dates;
	},
	DeleteEvent: function(param) {
		ShowInfo(Lang.InfoSaving);
		this.repeat_choose.style.display = "none";
		var dates;
		switch (param) 
		{
			case this.chooseParams.all:
				dates = this.DeleteEventAll();
				break;
			case this.chooseParams.one:
				dates = this.DeleteEventOne();
				break;
			default:
				break;
		}
		if (dates != null) RenderScreens(dates);
		Grid.SetWorkAreaOffset();
		EventForm.ClearEventForm();
		HideInfo();
	},
	DeleteEventAll : function () {
		var idCalendar, idEvent;
		var dates, result;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;

		idEvent = this.data.id_data.id;
		idCalendar = this.data.id_calendar;

		requestParams = 'action=delete_event' +
						'&event_id=' + idEvent +
						'&calendar_id=' + idCalendar +
						'&nocache=' + Math.random();
					
		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			if (!ServErr(result, Lang.ErrorDeleteEvent)) 
			{
				DeleteEventInWebMail(idEvent);
				dates = DeleteEventFromCacheAndGrid(idEvent);
				if (typeof(mycache.reminders[idEvent]) != 'undefined') 
				{
					delete mycache.reminders[idEvent];
				}
				if (typeof(mycache.appointments[idEvent]) != 'undefined') 
				{
					delete mycache.appointments[idEvent];
				}
			}
		}
		return dates;
	},
	DeleteEventOne : function () {
		var idCalendar, idEvent, idRepeat, idRecurrenceDate, eventData;
		var dates, eventTimeFrom, timeFrom, timeTill, result;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;

		idEvent = this.data.id_data.id;
		idRepeat = this.data.id_data.repeat;
		idRecurrenceDate = this.data.id_data.date;
		idCalendar = this.data.id_calendar;
		eventData = mycache.ids[idEvent];

		eventTimeFrom =  Fnum(eventData.event_timefrom.getHours(), 2)+':'+Fnum(eventData.event_timefrom.getMinutes(),2);
		timeFrom = Fnum(eventData.event_timefrom.getHours(), 2)+':'+Fnum(eventData.event_timefrom.getMinutes(), 2);
		timeTill = Fnum(eventData.event_timetill.getHours(), 2)+':'+Fnum(eventData.event_timetill.getMinutes(), 2);

		requestParams = 'action=update_exclusion' +
						'&event_id=' + idEvent +
						'&calendar_id=' + idCalendar +
						'&id_repeat=' + idRepeat +
						'&id_recurrence=' + idRecurrenceDate +
						'&event_time_from=' + eventTimeFrom +
						'&name=' + eventData.event_name +
						'&text=' + eventData.event_text +
						'&location=' + eventData.event_location +
						'&from=' + to8(eventData.event_timefrom) +
						'&till=' + to8(eventData.event_timetill) +
						'&time_from=' + timeFrom +
						'&time_till=' + timeTill +
						'&allday=' + eventData.allday_flag +
						'&is_deleted=1' +
						'&nocache=' + Math.random();
		if (req) 
		{
			result = sendRequest(req, url, requestParams);
			if (!ServErr(result, Lang.ErrorUpdateEvent)) 
			{
				dates = DeleteRepeatEventPart(idEvent, idRepeat, idRecurrenceDate);
			}
		}
		return dates;
	}
}