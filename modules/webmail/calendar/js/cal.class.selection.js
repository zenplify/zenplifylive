/**
 *
 * CreateSelection
 * DeleteSelection
 * EditSelection
 * StartSelection
 * MoveSelection
 * EndSelection
 * GetSelectionDates
 * CreateMonthSelection
 */

function CSelection()
{
    this.areaMouseDownColumn    = 1;
    this.areaMouseUpColumn      = 1;
    this.areaMouseDownLine      = 1;
    this.areaMouseUpLine        = 1;
    this.start                  = {Column: 0, Line: 0};
    this.end                    = {Column: 0, Line: 0};

    this.gridSelection          = null;
    this.section                = false;//common section = false; allday section = true
    this.parent                 = null;
	this.allowSelection			= false;

    this.ALLDAY                 = true;
    this.COMMON                 = false;
}

CSelection.prototype = {
	/*
	 * parent - DOM element
	 */
	CreateSelection : function(parent)
	{
		if (this.gridSelection === null)
		{
			var div = document.createElement("div");
			div.className = "select_area";
			div.style.zIndex = 14;
			this.gridSelection = div;
			parent.appendChild(div);
		}
	},
	DeleteSelection : function()
	{
		DeleteNode(this.gridSelection);
		this.gridSelection = null;
	},
	EditSelection : function()
	{
		var left = 0, width = 100, height = "100%", top = 0;
		if (view == MONTH)
		{
			left = CELL_WIDTH * (this.areaMouseDownColumn - 1);
			width = CELL_WIDTH;
			top = CELL_HEIGHT * (this.areaMouseDownLine - 1) + "%";
			height = CELL_HEIGHT + "%";
		}
		else
		{
			if (this.section === this.ALLDAY)
			{
				if (view == WEEK)
				{
					if (this.areaMouseUpColumn > this.areaMouseDownColumn)
					{
						this.start.Column = this.areaMouseDownColumn;
						this.end.Column = this.areaMouseUpColumn;
					}
					else
					{
						this.start.Column = this.areaMouseUpColumn;
						this.end.Column = this.areaMouseDownColumn;
					}
					left = CELL_WIDTH * (this.start.Column - 1);
					width = CELL_WIDTH * (this.end.Column - this.start.Column + 1);
				}
			}
			else //section == common
			{
				if (view == WEEK)
				{
					left = CELL_WIDTH * (this.areaMouseDownColumn - 1);
					width = CELL_WIDTH;
				}
				if (this.areaMouseUpLine > this.areaMouseDownLine)
				{
					this.start.Line = this.areaMouseDownLine;
					this.end.Line = this.areaMouseUpLine;
				}
				else
				{
					this.start.Line = this.areaMouseUpLine;
					this.end.Line = this.areaMouseDownLine;
				}
				this.start.Column = this.areaMouseDownColumn;
				this.end.Column = this.areaMouseDownColumn;
				height = LINE_HEIGHT * (this.end.Line - this.start.Line + 1) + "px";
				top = LINE_HEIGHT * (this.start.Line - 1) + "px";
			}
		}
		this.gridSelection.style.left = left + "%";
		this.gridSelection.style.top = top;
		this.gridSelection.style.width = width + "%";
		this.gridSelection.style.height = height;
	},
	/*
	 * parent - DOM object
	 * section - bool value (true - allday, false - common section)
	 */
	StartSelection : function(e, parent, section)
	{
		if(typeof editDiv_pres != 'undefined' && editDiv_pres) return;
		Grid.CalcLineHeight(this);
		this.allowSelection = CheckAllowCreateEvent();
		if (this.allowSelection === false) return;
		this.section = Boolean(section);
		this.parent = parent;
		this.areaMouseDownColumn = Grid.GetGridColumn(e);
		if (this.section === this.COMMON)
		{
			this.areaMouseDownLine = Grid.GetGridLine(e);
		}
		this.CreateSelection(parent);
	},
	MoveSelection : function(e)
	{
		if (this.gridSelection === null || this.allowSelection === false) return;
		if (this.section === this.ALLDAY)
		{
			this.areaMouseUpColumn = Grid.GetGridColumn(e);
		}
		else
		{
			this.areaMouseUpLine = Grid.GetGridLine(e);
		}

		this.EditSelection();
	},
	EndSelection : function(e)
	{
		if (this.gridSelection === null || this.allowSelection === false) return;
		this.areaMouseUpLine = Grid.GetGridLine(e);
		this.areaMouseUpColumn = Grid.GetGridColumn(e);
		this.EditSelection(e);
		var date = this.GetSelectionDates();
		if (this.section === this.ALLDAY)
		{
			date.till.setHours(23); // to prevent wrong till date
		}
		else
		{
			var from = Grid.GetLineTime(this.start.Line-1);
			var end = Grid.GetLineTime(this.end.Line);
			date.from.setHours(from.hours, from.minutes);
			date.till.setHours(end.hours, end.minutes);
		}

		var editEventFormData = {
			subject 	: "",
			description	: "",
			location	: "",
			event_id	: 0, // if 0 - new event
			calendar_id : 0,
			dateFrom	: date.from,
			dateTill	: date.till,
			excluded	: false,
			allday	  	: ((this.section === this.ALLDAY)?1:0)
		}
		EventForm.ShowEventForm(editEventFormData);
	},
	GetSelectionDates : function()
	{
		var dateFrom, dateTill;
		if (view == WEEK)
		{
			var weekStart = from8(showLimits.weekFrom);
			dateFrom = new Date(weekStart);
			dateTill = new Date(weekStart);
			dateFrom.setDate(dateFrom.getDate() + this.start.Column - 1);
			dateTill.setDate(dateTill.getDate() + this.end.Column - 1);
		}
		else
		{
			dateFrom = new Date(mydate);
			dateTill = new Date(mydate);
		}
		return {from: dateFrom, till: dateTill}
	},
	CreateMonthSelection : function(e, parent)
	{
		if(typeof editDiv_pres != 'undefined' && editDiv_pres) return;
		this.allowSelection = CheckAllowCreateEvent();
		if (this.allowSelection === false) return;

		var firstDayInMonthView, date, from, end, dates = {}
		this.parent = parent;

		this.areaMouseDownLine = Grid.GetGridLine(e);
		this.areaMouseDownColumn = Grid.GetGridColumn(e);
		this.CreateSelection(parent);
		this.EditSelection();

		firstDayInMonthView = monthLimits[mydate.getFullYear()][mydate.getMonth()].bigStart;
		date = new Date(firstDayInMonthView);
		date.setDate(date.getDate() + (this.areaMouseDownLine - 1)*7 + this.areaMouseDownColumn - 1);
		dates.from = date, dates.till = new Date(date);
		if (to8(date) == to8(mydate))
		{
			var time = GetCurrentTime();
			dates.from.setHours(time.from.getHours(),time.from.getMinutes());
			dates.till.setHours(time.till.getHours(),time.till.getMinutes());
		}
		else
		{
			from = Grid.GetLineTime(setcache.workdaystarts*2);
			end = Grid.GetLineTime(setcache.workdaystarts*2 + 1);
			dates.from.setHours(from.hours, from.minutes);
			dates.till.setHours(end.hours, end.minutes);
		}

		var editEventFormData = {
			subject 	: "",
			description	: "",
			location	: "",
			event_id	: 0, // if 0 - new event
			calendar_id : 0,
			dateFrom	: dates.from,
			dateTill	: dates.till,
			excluded	: false,
			allday	  	: 0
		}
		EventForm.ShowEventForm(editEventFormData);
	}
}