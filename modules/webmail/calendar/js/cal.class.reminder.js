/*
 * showForm
 * setNewReminder
 * filterReminderOptions
 * addReminder
 * createDropdown
 * selectOptionHandler
 * deleteReminder
 * setDisplayForm
 * closeForm
 * remindersToArray
 * getRemindersData
 */
function CReminder() {
	//Time in seconds
	this.remindOptions = [
        {Time: '-PT5M' , Value: 5 + " " + Lang.ReminderMinutes},
        {Time: '-PT10M', Value: 10 + " " + Lang.ReminderMinutes},
        {Time: '-PT15M', Value: 15 + " " + Lang.ReminderMinutes},
        {Time: '-PT30M', Value: 30 + " " + Lang.ReminderMinutes},
        {Time: '-PT60M', Value: 1 + " " + Lang.ReminderHour},
        {Time: '-PT120M', Value: 2 + " " + Lang.ReminderHours},
        {Time: '-PT180M', Value: 3 + " " + Lang.ReminderHours},
        {Time: '-PT240M', Value: 4 + " " + Lang.ReminderHours},
        {Time: '-PT300M', Value: 5 + " " + Lang.ReminderHours},
        {Time: '-PT360M', Value: 6 + " " + Lang.ReminderHours},
        {Time: '-PT420M', Value: 7 + " " + Lang.ReminderHours},
        {Time: '-PT480M', Value: 8 + " " + Lang.ReminderHours},
        {Time: '-PT540M', Value: 9 + " " + Lang.ReminderHours},
        {Time: '-PT600M', Value: 10 + " " + Lang.ReminderHours},
        {Time: '-PT660M', Value: 11 + " " + Lang.ReminderHours},
        {Time: '-PT720M', Value: 12 + " " + Lang.ReminderHours},
        {Time: '-PT1080M', Value: 18 + " " + Lang.ReminderHours},
        {Time: '-PT1440M', Value: 1 + " " + Lang.ReminderDay},
        {Time: '-PT2880M', Value: 2 + " " + Lang.ReminderDays},
        {Time: '-PT4320M', Value: 3 + " " + Lang.ReminderDays},
        {Time: '-PT5760M', Value: 4 + " " + Lang.ReminderDays},
        {Time: '-PT10080M', Value: 1 + " " + Lang.ReminderWeek},
        {Time: '-PT20160M', Value: 2 + " " + Lang.ReminderWeeks}
    ];
	// reminder
	this.expandRemindLinkCont   = $id("expandRemindLinkCont");
	this.expandRemindLink       = $id("expandRemindLink");
	this.remindCont             = $id("remindCont");
	this.moreRemindLinkCont     = $id("moreRemindLinkCont");
	this.moreRemindLink         = $id("moreRemindLink");
	this.remindParent           = $id("remindParent");
	this.remindersNum			= 5;
	this.defaultTime			= '-PT1440M';
	this.allow_remind			= false;

	this.reminders				= [];
	this.remindersDel			= [];
	this.selects				= [];

	var obj						= this;

	obj.expandRemindLink.onclick = function() {
		obj.showForm();
		EventForm.RecalcEventWindowPosition();
	};
	obj.moreRemindLink.onclick = function() {
		obj.addReminder();
		EventForm.RecalcEventWindowPosition();
	}
}
CReminder.prototype = {
	showForm : function() {
		this.expandRemindLinkCont.style.display = "none";
		this.remindCont.style.display = "block";
		this.moreRemindLinkCont.style.display = "block";
		this.addReminder(0, this.defaultTime);
		EventForm.RecalcShadowSize();
	},
	/* use in showEventForm */
	setNewReminder : function(eventId) {
		this.reminders = [];
		this.remindersDel = [];
		this.selects = [];

		CleanNode(this.remindParent);
		var reminders = mycache.reminders[eventId], i;
		if (typeof(reminders) != 'undefined' && eventId != 0) {
			for (i in reminders) {
				this.addReminder(reminders[i].id_reminder, reminders[i].offset);
			}
			this.expandRemindLinkCont.style.display = "none";
			this.remindCont.style.display = "block";
			this.setDisplayForm();
		} else {
			this.expandRemindLinkCont.style.display = "block";
			this.remindCont.style.display = "none";
			this.moreRemindLinkCont.style.display = "none";
		}

	},
	filterReminderOptions : function(except_time, include_time) {
		var i, j, options = [], option_time, add = true;
		var reminders_len = this.reminders.length, current_reminder = 0;
		if (except_time == undefined) except_time = 0;
		if (include_time == undefined) include_time = 0;
		for (i=0; i<this.remindOptions.length; i++) {
			add = true;
			option_time = this.remindOptions[i].Time;
			if (current_reminder != reminders_len) {
				for (j in this.reminders) {
					if (option_time == this.reminders[j].Time
						&& except_time != this.reminders[j].Time
						&& include_time != this.reminders[j].Time) {
						add = false;
						current_reminder++;
						break;
					}
				}
			}
			if (add) {
				options.push(this.remindOptions[i]);
			}
		}
		return options;
	},
	/**
     * Add new (DIV Element) reminder(if idReminder undefined or equal 0)
     * or exist reminder for user
     *
     * @param {int} idReminder - reminder id for create
     * @param {int} time - reminder time in seconds
     * @param {boolean} isEventLoading
     * @param {boolean} recreate
     * @param {int} remindersCount
     *
     * @return {void}
     */
	addReminder : function(idReminder, time, recreate, remindersCount) {
		var options = this.filterReminderOptions();
		var i, allow_add = true;
		var temp_reminders = [];
		recreate = recreate || false;

		idReminder = idReminder || 0;

		if (time != undefined) {
			for (i=0; i<this.reminders.length; i++) {
				if (this.reminders[i].Time == time) {
					allow_add = false;
				}
			}
		} else {
			time = options[0].Time;
		}
		if (allow_add === true) {
			this.allow_remind = true;
			this.reminders.push({
				Id:idReminder,
				Time:time
			});
			remindersCount = remindersCount || this.reminders.length;

			this.createReminderLine(time, options, remindersCount);
			// Check reminders number if more than two recreate all reminders
			if (this.reminders.length > 2 && !recreate) {
				temp_reminders = copyar(this.reminders);
				this.reminders = [];
				CleanNode(this.remindParent);
				for (i=0; i<temp_reminders.length; i++) {
					this.addReminder(temp_reminders[i].Id, temp_reminders[i].Time, true, temp_reminders.length);
				}
			}

		}
	},
	createReminderLine : function (time, options, reminders_count) {
		var i, j, reminder, deleter, dropdown_link, current_reminder;
		var base_string = '', temp_string1, temp_string2;
		// Create reminder element
		reminder = document.createElement("div");
		reminder.className = "remind_line";
		// Set z-index for new reminder
		reminder.style.zindex = this.remindersNum*22 - this.reminders.length*3 - 3;
		// Create link for dropdown
		dropdown_link = document.createElement("a");
		dropdown_link.href = "javascript:void(0)";
		dropdown_link.className = "link1 dropdown_link";
		for (i=0; i<options.length; i++) {
			if (options[i].Time == time) {
				dropdown_link.appendChild(document.createTextNode(options[i].Value));
			}
		}
		for (j=0; j<this.reminders.length; j++) {
			if (this.reminders[j].Time == time) {
				current_reminder = j;
				break;
			}
		}
		if (reminders_count == 1 || current_reminder == 0) {
			base_string = Lang.AddReminderBefore;
		} else if (reminders_count > 2
					&& current_reminder < reminders_count - 1) {
			base_string = Lang.AddReminderAnd;
		} else {
			base_string = Lang.AddReminderAlso;
		}
		// Insert string into reminder line
		temp_string1 = base_string.substring(0, base_string.search("%")-1);
		temp_string2 = base_string.substring(base_string.search("%")+2);

		reminder.appendChild(document.createTextNode(temp_string1 + " "));
		reminder.appendChild(dropdown_link);
		reminder.appendChild(document.createTextNode(" " + temp_string2));

		deleter = document.createElement("a");
		deleter.href = "javascript:void(0)";
		deleter.appendChild(document.createTextNode(Lang.Remove));
		deleter.className = "link1 deleter";
		deleter.style.display = "none";
		var obj = this;
		var id = this.reminders[current_reminder].Id;
		AddHandler(
			deleter,
			"click",
			function() {
				obj.deleteReminder(id, time);
			}
			);
		// Insert delete reminder link into reminder line
		reminder.appendChild(deleter);
		AddHandler(
			reminder,
			"mouseover",
			function() {
				deleter.style.display = "inline";
			}
			);
		AddHandler(
			reminder,
			"mouseout",
			function() {
				deleter.style.display = "none";
			}
			);
		AddHandler(
			dropdown_link,
			"click",
			function(e) {
				obj.createDropdown(reminder, time, dropdown_link, e, deleter);
				deleter.style.display = "none";
			}
			);
		//  End Deleter
		this.remindParent.appendChild(reminder);
		this.setDisplayForm();
	},
	/**
     * Create new dropdown list (DIV Element) and append to reminder DIV Element
     *
     * @param {object} reminder - [DIV Element] working reminder
     * @param {int} time - reminder time in seconds
     * @param {object} dropdown_link - [LINK Element] link for show dropdown list
     * @param {object} mouseEv - [Mouse Event Element] mouse event
	 * @param {object} deleter - [LINK Element] link to delete
     *
     * @return {void}
     */
	createDropdown : function (reminder, time, dropdown_link, mouseEv, deleter) {
		var options = this.filterReminderOptions(0, time);
		var i, j, one_option, dropdown, modal_div, link, dropdownOffsetTop = 0, dropdownOffsetLeft = 0, dropdownTop =0;
		var dropdown_row_height = -14; // Dropdown row height in CSS
		var border = 2; // Dropdown border in CSS
		var offset = 6; // Default dropdown offset for other browsers
		var target = (mouseEv.target) ? mouseEv.target : mouseEv.srcElement;
		dropdownOffsetLeft = target.offsetLeft - offset - border;
		// Creation blocking div element
		modal_div = document.createElement("div");
		modal_div.className = "modal_div";
		modal_div.style.zindex = reminder.style.zindex + 1; // Set z-index on 1 it is more than at an last reminder row
		// Creation dropdown div element
		dropdown = document.createElement("div");
		dropdown.className = "dropdown";
		dropdown.id = "reminder_dropdown";
		dropdown.style.zindex = reminder.style.zindex + 2; // Set z-index on 2 it is more than at an last reminder row
		for (i=0; i<options.length; i++) {
			one_option = document.createElement("div");
			link = document.createElement("a");
			link.href = "javascript:void(0)";
			if (options[i].Time == time) {
				link.className = "active"; // Set active option in dropdown list
				link.appendChild(document.createTextNode(options[i].Value));
				// Set dropdown offset
				dropdownOffsetTop = (i+1) * dropdown_row_height - border*2;
				for (j=0; j<this.reminders.length; j++) {
					if (this.reminders[j].Time == time) {
						dropdownTop = j*17;
					}
				}
			}
			else {
				var obj = this;
				link.className = "link1";
				link.id = i;
				link.appendChild(document.createTextNode(options[i].Value));
				AddHandler(
					link,
					"click",
					function (e) {
						var element = (e.target) ? e.target : e.srcElement;
						obj.selectOptionHandler(reminder, options[element.id], dropdown_link, time, deleter);
					}
					);
			}
			AddHandler(
					link,
					"click",
					function() {
						deleter.style.display = "none";
						reminder.removeChild(dropdown);
						reminder.removeChild(modal_div);
					}
					);
			one_option.appendChild(link);
			dropdown.appendChild(one_option);
		}
		// Hardcode for IE7
		if (getMSIEVersion() == 7 || getMSIEVersion() == 6) {
			dropdown.style.top = dropdownTop + 15 + "px";
			dropdown.style.left = "-71px";
		}
		dropdown.style.marginTop = dropdownOffsetTop + "px";
		dropdown.style.marginLeft = dropdownOffsetLeft + "px";

		AddHandler(
			modal_div,
			"click",
			function() {
				reminder.removeChild(dropdown);
				reminder.removeChild(modal_div);
				deleter.style.display = "none";
			}
			);
		reminder.appendChild(modal_div);
		reminder.appendChild(dropdown);
	},
	/**
     * Select reminder option in reminder options dropdown list
     *
     * @param {object} reminder - [DIV Element] working reminder
     * @param {object} option - [DIV Element] selected reminder option
     * @param {object} dropdown_link - [LINK Element] link for show dropdown list
     * @param {int} time - reminder time in seconds
	 * @param {object} deleter - [LINK Element] link to delete
     *
     * @return {void}
     */
	selectOptionHandler : function (reminder, option, dropdown_link, time, deleter)
	{
		var obj = this;
		var i, new_link;
		// Create new dropdown link
		new_link = document.createElement("a");
		new_link.href = "javascript:void(0)";
		new_link.className = "link1 dropdown_link";
		new_link.appendChild(document.createTextNode(option.Value));
		AddHandler(
			new_link,
			"click",
			function(e)
			{
				obj.createDropdown(reminder, option.Time, new_link, e, deleter);
			}
		);
		for (i in this.reminders)
		{
			if (this.reminders[i].Time == time)
			{
				this.reminders[i].Time =  option.Time;
				break;
			}
		};
		reminder.insertBefore(new_link, dropdown_link);
		reminder.removeChild(dropdown_link);
	},
	/**
     * Delete selected remider DIV Element,
     * insert reminder id in remidersDel array
     * and delete reminder fron reminders array
     *
     * @param {int} idReminder - reminder id for delete, will be inserted in remiderDel array
     * @param {int} time - reminder time in seconds
     * @return {void}
     */
	deleteReminder : function(idReminder, time)
	{
		var temp_reminders;
		if (idReminder != 0)
		{
			this.remindersDel.push(idReminder);
		}
		for (var i=0; i<this.reminders.length; i++)
		{
			if(this.reminders[i].Id == idReminder && this.reminders[i].Time == time)
			{
				this.reminders.splice(i,1);
				break;
			}
		}
		CleanNode(this.remindParent);
		if (this.reminders.length > 0)
		{
			temp_reminders = copyar(this.reminders);
			this.reminders = [];
			for (i=0; i<temp_reminders.length; i++)
			{
				this.addReminder(temp_reminders[i].Id, temp_reminders[i].Time, true, temp_reminders.length);
			}
		}
		else
		{
			this.expandRemindLinkCont.style.display = "block";
			this.remindCont.style.display = "none";
			this.moreRemindLinkCont.style.display = "none";
		}
		this.setDisplayForm();
	},
	setDisplayForm : function()
	{
		var show = (this.reminders.length < this.remindersNum && this.reminders.length != 0) ? "block" : "none";
		if (this.reminders.length == 0) this.closeForm();
		EventForm.RecalcEventWindowPosition();
		this.moreRemindLinkCont.style.display = show;
	},
	closeForm: function()
	{
		this.expandRemindLinkCont.style.display = "block";
		this.remindCont.style.display = "none";
		this.moreRemindLinkCont.style.display = "none";
		this.allow_remind = false;
		this.remindersToArray();
		EventForm.RecalcShadowSize();
	},
	remindersToArray : function()
	{
		if (this.allow_remind === false)
		{
			var allow_add = true;
			for (var i=0; i<this.reminders.length; i++)
			{
				for (var j=0; j<this.remindersDel.length; j++)
				{
					if (this.remindersDel[j] == this.reminders[i].Id)
					{
						allow_add = false;
					}
				}
				if (allow_add === true)
				{
					this.remindersDel.push(this.reminders[i].Id);
				}
			}
			this.reminders = [];
			this.selects = [];
			CleanNode(this.remindParent);
		}
	},
	getRemindersData : function()
	{
		this.remindersToArray();
		return {
			save : this.reminders,
			del : this.remindersDel
		}
	}
}