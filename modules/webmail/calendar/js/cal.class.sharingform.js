/**
 * initialize
 * RecalcShadowHeight
 * ExpandPublicationLevel
 * ExpandICalendar
 * ShowForm
 * AddShare
 * RecalcShareHeight
 * CancelShare
 * SaveShare
 */
function CSharingForm() {
	this.share_window				= $id('share_window');
	this.sharecontainer             = $id('sharecontainer');
	this.share_shadow               = $id('share_shadow');
	this.share_middle               = $id('share_middle');
	this.publicateCalendar			= $id('publicateCalendar'); //bool
	this.publicationLevelContainer	= $id('publicationLevelContainer');
	this.publicationLevel			= $id('publicationLevel');
	this.publicationUrl				= $id('publicationUrl');
	this.principalUrl				= $id('principalUrl');
	this.calendarUrl				= $id('calendarUrl');
	this.icsUrl						= $id('icsUrl');
	this.icsDownload				= $id('icsDownload');
	this.sharedEmail				= $id('sharedEmail');
	this.publicationHash			= $id('publicationHash');
	this.sharedPermission			= $id('sharedPermission');
	this.addShare					= $id('addShare');
	this.shareParent				= $id('shareParent');
	this.share_save					= $id('share_save');
	this.share_cancel				= $id('share_cancel');

	this.iCalAllow					= $id('iCalAllow');
	this.iCalContainer				= $id('iCalContainer');
	this.iCalUrl					= $id('iCalUrl');
	this.iCalHash					= $id('iCalHash');

	this.shareform_id				= $id('shareform_id');
	this.shares = [];
	this.shares_del = [];

	this.initialize();
}
CSharingForm.prototype = {
	initialize: function()
	{
		var obj = this;
		this.publicateCalendar.onclick = function()
		{
			obj.ExpandPublicationLevel();
			obj.RecalcShadowHeight();
		}
		this.publicationUrl.onclick = function()
		{
			this.select();
		}
		this.principalUrl.onclick = function()
		{
			this.select();
		}
		this.calendarUrl.onclick = function()
		{
			this.select();
		}
		this.icsUrl.onclick = function()
		{
			this.select();
		}
		this.sharedEmail.onfocus = function()
		{
			if(this.value == Lang.SharedTitleEmail) {
				this.value = '';
				this.style.color = '#696969';
			}
		}
		this.sharedEmail.onblur = function()
		{
			if(this.value == '') {
				this.style.color = '#aaa';
				this.value = Lang.SharedTitleEmail;
			}
		}
		this.addShare.onclick = function()
		{
			obj.AddShare(0, obj.sharedEmail.value, obj.sharedPermission.value, 'add');
		}
		this.share_save.onclick = function()
		{
			obj.SaveShare();
		}
		this.share_cancel.onclick = function()
		{
			obj.CancelShare();
		}
		this.iCalAllow.onclick = function()
		{
			obj.ExpandICalendar();
			obj.RecalcShadowHeight();
		}
		this.iCalUrl.onclick = function()
		{
			this.select();
		}
	},
	RecalcShadowHeight: function() {
		var height = "auto";
		if ((this.sharecontainer.clientHeight - 4) > 0) {
			height = (this.sharecontainer.clientHeight - 4) + "px";
		}
		this.share_shadow.style.height = height;
	},
	ExpandPublicationLevel: function() {
		if (this.publicateCalendar.checked) 
		{
			if (typeof(this.shareform_id.value) != "undefined") {
				var calendar_data = mycache.calendars[this.shareform_id.value];
				
				if (calendar_data.publication_level != null && calendar_data.publication_hash != null) {
					this.publicationHash.value = calendar_data.publication_hash;
					this.publicationUrl.value = getMainURL()+publication_url+"?cal="+calendar_data.publication_hash;
					this.publicationLevel.options[calendar_data.publication_level - 1].selected = true;
				} else {
					this.share_save.disabled = true;
					var hash_arr = [], result;
					var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
					requestParams = 'action=get_pubhash&calendar_id='+calendar_data.calendar_id;
					if (req) {
						result = sendRequest(req, url, requestParams);
						if (!ServErr(result,Lang.ErrorGetPublicationHash)) {
							hash_arr = result;
						}
					}

					this.publicationHash.value = hash_arr.hash;
					this.publicationUrl.value = getMainURL()+publication_url+"?cal="+hash_arr.hash;
					this.publicationLevel.options[0].selected = true;

					this.share_save.disabled = false;
				}
				

/*				this.publicationHash.value = calendar_data.calendar_id;
				this.publicationUrl.value = getMainURL()+publication_url+"?cal="+decodeURIComponent(calendar_data.publication_hash);
				this.publicationLevel.options[0].selected = true;
*/				
				this.publicationLevelContainer.style.display = "block";
			}
		} 
		else 
		{
			this.publicationLevelContainer.style.display = "none";
		}
	},
	ExpandICalendar: function() {
		if (this.iCalAllow.checked) {
			if (this.shareform_id.value != undefined) {
				var calendar_data = mycache.calendars[this.shareform_id.value];
				if (calendar_data.ical_hash != null) {
					this.iCalHash.value = calendar_data.ical_hash;
					this.iCalUrl.value = getMainURL()+publication_url+"?ical="+calendar_data.ical_hash;
				} else {
					this.share_save.disabled = true;
					var req = GetXMLHTTPRequest(), url = processing_url, requestParams, result;
					requestParams = 'action=get_ical';
					if (req) {
						result = sendRequest(req, url, requestParams);
						ServErr(result,Lang.ErrorGetPublicationHash);
					}
					this.iCalHash.value = result.hash;
					this.iCalUrl.value = getMainURL()+publication_url+"?ical="+result.hash;
					this.share_save.disabled = false;
				}
				this.iCalContainer.style.display = "block";
			}
		} else {
			this.iCalContainer.style.display = "none";
		}
	},
	ShowForm: function(id_calendar) {
		this.shares.length = 0;
		this.shares_del.length = 0;
		var calendar_data = mycache.calendars[id_calendar];

		this.shareform_id.value = id_calendar;
		QuickMenu.RemoveQuickMenu();
		var color_number = calendar_data.color;
		this.sharecontainer.className = "eventcontainer_"+color_number;

		if (calendar_data.publication_level != null) {
			this.publicateCalendar.checked = true;
			this.publicationLevel.options[calendar_data.publication_level - 1].selected = true;
			this.publicationUrl.value = getMainURL()+publication_url+"?cal="+decodeURIComponent(calendar_data.publication_hash);
			this.publicationLevelContainer.style.display = "block";
			this.publicationHash.value = calendar_data.publication_hash;
		} else {
			this.publicateCalendar.checked = false;
			this.publicationUrl.value = "";
			this.publicationLevelContainer.style.display = "none";
			this.publicationHash.value = "";
		}
		
		this.principalUrl.value = setcache.server_url + calendar_data.principal_id;
		this.calendarUrl.value = setcache.server_url + calendar_data.url;
		this.icsUrl.value = setcache.server_url + calendar_data.url + '?export';
		this.icsDownload.href = calendar_data.ics_download;

		if (calendar_data.ical_hash != null) {
			this.iCalAllow.checked = true;
			this.iCalContainer.style.display = "block";
			this.iCalUrl.value = getMainURL()+publication_url+"?ical="+calendar_data.ical_hash;
			this.iCalHash.value = calendar_data.ical_hash;
		} else {
			this.iCalAllow.checked = false;
			this.iCalContainer.style.display = "none";
			this.iCalUrl.value = "";
			this.iCalHash.value = "";
		}
		CleanNode(this.shareParent);
		if (typeof(calendar_data.shares) != "undefined" && calendar_data.shares.length > 0) {
			for (var i in calendar_data.shares) {
				this.AddShare(calendar_data.shares[i]['id_share'], 
							  calendar_data.shares[i]['email_to_user'], 
							  calendar_data.shares[i]['access_level'], 'show');
			}
		}

		this.sharedEmail.value = Lang.SharedTitleEmail;
		this.sharedEmail.style.color = "#aaa";
		setMaskHeight(this.share_window);
		this.share_window.style.display = "block";
		if(SafariDetect().isSafari) {
			$id("share_form").style.top = window.innerHeight / 2 + "px";
		}
		this.RecalcShareHeight();
		this.RecalcShadowHeight();
	},
	AddShare: function(id_share, email, permission, action) {
		action = Trim(action);
		if (action != "add" && action != "show")
		{
			return;
		}

		var emailValue = Trim(this.sharedEmail.value);
		var i;

		if (action == "add")
		{
			if (emailValue == "" || emailValue == Lang.SharedTitleEmail)
			{
				alert(Lang.WarningEmailFieldFilling);
				this.sharedEmail.focus();
				return;
			}
			if (!Validator.isCorrectEmail(emailValue))
			{
				alert(Lang.WarningCorrectEmail);
				this.sharedEmail.value = "";
				this.sharedEmail.focus();
				return;
			}
		}

		var permissionsValues = [
//			{id:0, name:Lang.SharePermission1},
	        {id:1, name:Lang.SharePermission2},
	        {id:2, name:Lang.SharePermission3}//,
//			{id:3,name:Lang.SharePermission4}
        ];

		var error_code = 0;
		if (action == "add") {
			var errorText = "", result, share_arr;
			var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
			requestParams = 'action=check_share' +
							'&email=' + encodeURIComponent(emailValue) +
							'&calendar_id=' + this.shareform_id.value;
			if (req) {
				result = sendRequest(req, url, requestParams);
				if (!ServErr(result,Lang.ErrorGetShare)) {
					share_arr = result;
				}

				if (typeof(share_arr.shares_errors) != "undefined") {
					if (share_arr.shares_errors.length>0) {
						for (i=0; i<share_arr.shares_errors.length; i++) {
							error_code = share_arr.shares_errors[i]['error_code'];
							var replace_str = "";
							if (error_code == 1) { //user doesnt exist
								replace_str = Lang.ErrorUpdateSharing1;
							}
							if (error_code == 2) { //user is me or user is multiuser or calendar belong to user, who shared calendar to us
								replace_str = Lang.ErrorUpdateSharing2;
							}
							if (error_code == 3) { //already shared to this user
								replace_str = Lang.ErrorUpdateSharing3;
							}
							errorText += replace_str.replace("%s", emailValue)+"\n";
						}
						if (errorText != "") {
							alert(errorText);
						}
					}
				}
			}
		}

		if (error_code == 0) {
			var share, col1, txtemail, col2, select, option, obj = this, col3, delbtn, cleardiv;
			share = document.createElement("div");
			share.className = "share";

			col1 = document.createElement("div");
			col1.className = "col1";
			txtemail = document.createTextNode(email);
			col1.appendChild(txtemail);
			share.appendChild(col1);

			col2 = document.createElement("div");
			col2.className = "col2";
			select = document.createElement("select");

			for (i=0; i<permissionsValues.length; i++) 
			{
				option = document.createElement("option");
				option.value = permissionsValues[i].id;
				option.innerHTML = permissionsValues[i].name;
				if (permissionsValues[i].id == permission)
				{
					option.selected = true;
				}
				select.appendChild(option);
			}

			select.onchange = function () {
				obj.shares.push({
					id:delbtn.id,
					user:email,
					level:select.value
				});
			};
			select.className = "input";
			col2.appendChild(select);
			share.appendChild(col2);

			col3 = document.createElement("div");
			col3.className = "col3";

			delbtn = document.createElement("img");
			delbtn.src = "./calendar/skins/calendar/delete.png";
			delbtn.id = id_share;
			delbtn.className = "imgbtn";
			delbtn.alt = Lang.TitleDelete;
			delbtn.title = Lang.TitleDelete;

			delbtn.onclick = function() {
				if (confirm(Lang.ConfirmAreYouSure)) {
					share.style.display = "none";
					if (this.id != 0) {
						obj.shares_del.push(this.id);
					}
					obj.RecalcShadowHeight();
				}
			}

			col3.appendChild(delbtn);
			share.appendChild(col3);

			cleardiv = document.createElement("div");
			cleardiv.className = "clear";
			share.appendChild(cleardiv);

			this.shareParent.appendChild(share);
			if (id_share == 0) this.shares.push({
				id:id_share,
				user:email,
				level:permission
			});
			this.sharedEmail.value = Lang.SharedTitleEmail;
			this.sharedEmail.style.color = "#aaa";
			this.RecalcShareHeight();
			this.RecalcShadowHeight();
		} else {
			this.sharedEmail.select();
		}
	},
	RecalcShareHeight: function() {
		this.share_middle.style.height = "auto";
		if (this.share_middle.clientHeight > 250) {
			this.share_middle.style.height = "250px";
			this.share_middle.style.overflowX = "auto";
			this.share_middle.style.overflowY = "scroll";
		} else {
			this.share_middle.style.overflowX = "hidden";
			this.share_middle.style.overflowY = "hidden";
		}
	},
	CancelShare: function() {
		this.share_window.style.display = "none";
	},
	SaveShare: function() {
		var i, publicationLevel_value = null, calendar_id;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		calendar_id = this.shareform_id.value;
		requestParams = 'action=update_share' +
						'&calendar_id=' + calendar_id +
						'&nocache=' + Math.random();

		if (this.publicateCalendar.checked) {
			publicationLevel_value = this.publicationLevel.value;
			requestParams += '&publication_hash=' + this.publicationHash.value;
		} else {
			publicationLevel_value = 0;
		}

		if (publicationLevel_value != null) 
			requestParams += '&publication_level=' + publicationLevel_value;
		if (this.shares.length>0) {
			for (i=0; i<this.shares.length; i++) {
				requestParams += '&share_ids[' + i + ']=' + this.shares[i].id +
								'&share_emails[' + i + ']=' + this.shares[i].user + 
								'&share_levels[' + i + ']=' + this.shares[i].level;
			}
		}
		if (this.shares_del.length > 0) {
			for (i=0; i<this.shares_del.length; i++) {
				requestParams += '&share_delete[' + i + ']=' + this.shares_del[i];
			}
		}

		if (this.iCalAllow.checked) {
			requestParams += '&ical_hash=' + this.iCalHash.value;
		} else {
			requestParams += '&ical_hash=0';
		}
		if (req) {
			var calendar_arr = sendRequest(req, url, requestParams);
			if (!ServErr(calendar_arr,Lang.ErrorUpdateSharing)) {
				var errorText, error_code, this_calendar, replace_str;
				calendar_id = calendar_arr.calendar_id;
				this_calendar = mycache.calendars[calendar_id];

				if (calendar_id != undefined && this_calendar != undefined) {
					this_calendar.publication_hash = (typeof(calendar_arr.publication_hash) != 'undefined')?calendar_arr.publication_hash: null;
					if (typeof(calendar_arr.publication_level) != 'undefined' && calendar_arr.publication_level != null) {
						this_calendar.publication_level =  calendar_arr.publication_level;
						$id('calendar_'+calendar_id+'public').className = 'ml_publiclink';
					} else {
						this_calendar.publication_level = null;
						$id('calendar_'+calendar_id+'public').className = 'hide';
					}
					if (calendar_arr.ical_hash == undefined || calendar_arr.ical_hash == 0) {
						this_calendar.ical_hash = null;
					} else {
						this_calendar.ical_hash = calendar_arr.ical_hash;
					}

					if (typeof(calendar_arr.shares != 'undefined')) {
						this_calendar.shares = calendar_arr.shares;
					}
					if (typeof(calendar_arr.shares_errors) != 'undefined') {
						if (calendar_arr.shares_errors.length>0) {
							errorText = '';
							error_code = 0;
							for (i=0; i<calendar_arr.shares_errors.length; i++) {
								error_code = calendar_arr.shares_errors[i]['error_code'];
								replace_str = '';
								if (error_code == 1) {
									replace_str = Lang.ErrorUpdateSharing1;
								}
								if (error_code == 2) {
									replace_str = Lang.ErrorUpdateSharing2;
								}
								if (error_code == 3) {
									replace_str = Lang.ErrorUpdateSharing3;
								}
								errorText += replace_str.replace('%s', calendar_arr.shares_errors[i]['email_to_user'])+'\n';
							}
							if (errorText != '') {
								alert(errorText);
							}
						}
					}
				}
			}
		}
		this.share_window.style.display = 'none';
		HideInfo();
	}
}