var drager = null;
var more_dates = [];
var shownfetch=0;
var QOpen=0;
var showHide = [];
var indrag=false;

function DocumentOnClickHandler(evt)
{
	var tgt = window.event ? window.event.srcElement : evt.target;

	var monthsList = $id('monthsList');
	var edit_form = $id('edit_form');
	var exist, i;
	if (monthsList.style.display != 'none'
		&& !(tgt.id == 'monthsList' || tgt.id == 'middleTdSelector'
				|| tgt.id == 'MonthSelector' || tgt.id == 'imgSelector'))
	{
		var middleTdSelector = $id('middleTdSelector');
		monthsList.style.display = 'none';
		middleTdSelector.className = 'middleTdSelector';
	}
	if (edit_form.style.display != 'none')
	{
		var select_box_list = $id('edit_select_box_list');
		if (tgt.id != 'EventTimeFrom') timeSelectorFrom.Hide();
		if (tgt.id != 'EventTimeTill') timeSelectorTill.Hide();
		if (tgt.id != 'calen_sal' && tgt.id != 'calendar_arrow' 
			&& select_box_list.style.display != 'none')
		{
			select_box_list.style.display = 'none';
		}
		if (tgt.id != 'repeatUntil')
		{
			var untilElements = $id('calRepeatUntil').getElementsByTagName(tgt.tagName);
			exist = false;
			if (typeof(untilElements) != 'undefined')
			{
				for (i=0;i < untilElements.length;i++)
				{
					if (untilElements[i] == tgt)
					{
						exist = true;
						break;
					}
				}
			}
			if (untilElements.length == 0 || !exist)
			{
				calendarRepeatUntil.Hide();
			}
		}
		var advanced_list1 = $id('advanced_list1');
		var advanced_list2 = $id('advanced_list2');
		if (tgt.id != 'advanced_repeat_text21' 
			&& tgt.id != 'advanced_repeat_text22'
			&& tgt.id != 'advanced_repeat_text23'
			&& tgt.id != 'repeatTimes' && tgt.id != 'repeatUntil'
			&& tgt.id != 'advanced_repeat_arrow2'
			&& advanced_list2.style.display != 'none')
		{
			advanced_list2.style.display = 'none';
		}
		if (tgt.id != 'repeatOnDay' && tgt.id != 'repeatWeekDays' 
			&& tgt.id != 'yearDate' && tgt.id != 'advanced_repeat_arrow1'
			&& advanced_list1.style.display != 'none')
		{
			advanced_list1.style.display = 'none';
		}
		if (tgt.id != 'EventDateFrom')
		{
			var stElements = $id('st').getElementsByTagName(tgt.tagName);
			exist = false;
			if (typeof(stElements) != 'undefined')
			{
				for (i=0;i < stElements.length;i++)
				{
					if (stElements[i] == tgt)
					{
						exist = true;
						break;
					}
				}
			}
			if (stElements.length == 0 || !exist)
			{
				calendarTableStart.Hide();
			}
		}

		if (tgt.id != 'EventDateTill')
		{
			var enElements = $id('en').getElementsByTagName(tgt.tagName);
			exist = false;
			if (typeof(enElements) != 'undefined')
			{
				for (i=0;i < enElements.length;i++)
				{
					if (enElements[i] == tgt)
					{
						exist = true;
						break;
					}
				}
			}
			if (enElements.length == 0 || !exist)
			{
				calendarTableEnd.Hide();
			}
		}
	}
}

function delarr(arr,element,keep)
{
	element = Number(element);
	var ar2 = [], j=0, i;
	for (i in arr)
	{
		if (Number(i) != element)
		{
			if (keep)
			{
				ar2[i]=arr[i];
			} 
			else
			{
				ar2[j]=arr[i];
				j++;
			}
		}
	}
	return(ar2);
}

/**
 * @param e = window.event object
 */
function dragdisp(e)
{
	e = e || window.event;
	e.cancelBubble=true;

	drager=document.createElement('div');
	drager.id = 'drager';
	if (invoked_event.event_allday ==1) drager['id'] = 'drager allday';
	drager.style.display='none';
	drager.style.position='absolute';

	drager.style.borderWidth='3px';
	drager.style.borderColor='#CCCCCC';
	var calendar_data = mycache.calendars[invoked_event['calendar_id']];//evlink

	if (invoked_event.event_allday == 1)
	{
		drager.className='eventcontainer_'+calendar_data['color'] + ' dad';
	}
	else
	{
		drager.className='eventcontainer_'+calendar_data['color'];
	}

	var div1 = document.createElement('div');
	div1.className = 'event';
	div1.style.width = cew + 'px';

	var div2 = document.createElement('div');
	div2.className = 'a';

	var div3 = document.createElement('div');
	div3.className = 'b';

	div1.appendChild(div2);
	div1.appendChild(div3);

	var div4 = document.createElement('div');
	div4.className = 'event_middle';
	div4.style.height=(invoked_event.obj.offsetHeight-4)+'px';

	var div5 = document.createElement('div');
	div5.className = 'event_text';

	var div6 = document.createElement('div');
	div6.className = 'time';
	var titl = '', tf;
	if (setcache.timeformat == 1)
	{
		var tfDateObj = invoked_event['event_timefrom'];//evlink
		var ttDateObj = invoked_event['event_timetill'];//evlink
		var chour_f = tfDateObj.getHours();
		var cmin_f = tfDateObj.getMinutes();
		var chour_t = ttDateObj.getHours();
		var cmin_t = ttDateObj.getMinutes();
		tf = ((chour_f  == 0)? '12' : ((chour_f > 12)? chour_f -12 : chour_f)) + ((cmin_f == 0) ? ' ' : ':'+ cmin_f + ' ') + ((chour_f <12) ? 'AM' : 'PM');
		tt = ((chour_t  == 0)? '12' : ((chour_t > 12)? chour_t -12 : chour_t)) + ((cmin_t == 0) ? ' ' : ':'+ cmin_t + ' ') + ((chour_t <12) ? 'AM' : 'PM');
		titl= tf +' - '+ tt;
	} 
	else //setcache.timeformat == 2
	{ 
		tf=gtime(invoked_event['event_timefrom']);
		tt=gtime(invoked_event['event_timetill'],24);
		titl=tf.time+' - '+tt.time;
	}
	if (invoked_event.event_allday == 1) titl=' ';

	var tex1 = document.createTextNode(titl);
	if (invoked_event.event_allday == 1) div6.style.marginTop='3px';
	div6.appendChild(tex1);

	var div7 = document.createElement('div');
	div7.className = 'text';
	var tex2 = document.createTextNode(invoked_event['event_name']);//evlink
	div7.appendChild(tex2);
	div5.appendChild(div6);
	div5.appendChild(div7);
	div4.appendChild(div5);
	div1.appendChild(div4);
	var div8 = document.createElement('div');
	div8.className = 'b';
	div1.appendChild(div8);
	var div9 = document.createElement('div');
	div9.className = 'a';
	div1.appendChild(div9);
	drager.appendChild(div1);
	document.body.appendChild(drager);

	drager.style.left=leftpos + 'px';
	drager.style.top=toppos + 'px';

	drager.style.zIndex=65535;
	drager.style.display='inline';
	dbeg=coordss(e, obj, invoked_event.event_allday, mybox,1,this);
	dhigh=dbeg;


/*********dragshow*********/
	dragit = document.createElement('div');
	dragit.style.position = 'absolute';
	dragit.style.left = 1 + dbeg.xcol + 'px';
	dragit.style.top = 1 + dbeg.xrow + 'px';
	dragit.className = 'dragit_area';

	var hig = 0;
	if (view < MONTH) //DAY or WEEK
	{ 
		if (invoked_event.event_allday != 1) hig=invoked_event.obj.offsetHeight-1;
		else hig=mybox.height-1;
	} 
	else 
	{
		hig=Math.floor(mybox.height/6)-1;
	}
	dragwide(mybox);
	dragit.style.height = hig+'px';
	dragit.style.zIndex = 65500;
	dragit.style.display = 'block';
	dragit.id = 'dragit';
	mybox.dom.appendChild(dragit);
/*********dragshow*********/

	invoked_event.obj.style.display='none';

	document.onselectstart = function() {
		return false;
	}
	document.onselect = function() {
		return false;
	}
}

function dragwide(mybox) 
{
	var wid = Math.floor(ParseToNumber(drager.firstChild.style.width));
	var wmax = Math.floor(mybox.width - ParseToNumber(dragit.style.left));
	if (wid>wmax) wid = wmax;
	dragit.style.width = wid+'px';
}

function emovin(e)
{
	if(typeof editDiv_pres != 'undefined' && editDiv_pres )
	{
		inline(invoked_event.obj);
	} 
	else
	{
		zb=coordss(e, invoked_event.obj, invoked_event.event_allday,mybox,0);
		dsX=mybox.left+mox+zb.x-stx;
		dsY=mybox.top+moy+zb.y-sty;

		if ((!indrag)&&((Math.abs(inipos.x-zb.x)>5)||(Math.abs(inipos.y-zb.y)>5)))
		{
			dragdisp(e);
			indrag=true;
		}

		if (indrag)
		{
			if ((view < MONTH)&&(invoked_event.event_allday!=1))
			{
				var scrollbox='area_2_'+ ((view == DAY)?'day':'week');
				var ofs = $id(scrollbox).scrollTop;
				dsY-=ofs;
			}
			drager.style.left=dsX+'px';
			drager.style.top=dsY+'px';
			var realhigh = invoked_event.obj.offsetHeight;
			dcurr=coordss(e, invoked_event.obj, invoked_event.event_allday, mybox,1,this,1, realhigh);

			if ((dcurr.xrow+realhigh)<=mybox.height)
			{
				if ((dcurr.d!=dhigh.d)||(dcurr.t!=dhigh.t)) 
				{
					/*******dragmove******/
					dragit.style.left=dcurr.xcol+1+'px';
					dragit.style.top=dcurr.xrow+1+'px';
					dragwide(mybox);
					/*******dragmove******/

					dhigh=dcurr;
				}
			}
		}
	}
}

function gotcha(e) 
{
	var cal_id = Isplit(invoked_event.obj.id);
	var calendar = mycache.ids[cal_id.id]['calendar_id'];
	var sharing_level = mycache.calendars[calendar]['sharing_level'];
	if (sharing_level != VIEW_EVENTS_DETAILS && sharing_level !=VIEW_BUSY_TIME)
	{
		EventOff('mousemove',emovin,true);
	}
	EventOff('mouseup',gotcha,true);
	e = e || window.event;
	e.cancelBubble = true;
	if (!indrag) 
	{
		invoked_event.obj.style.display='block';
		zb=coordss(e, invoked_event.obj, invoked_event.event_allday, mybox,0);/*!!!*/
		if ((Math.abs(inipos.x-zb.x)<3)&&(Math.abs(inipos.y-zb.y)<3)) 
		{
			if(typeof editDiv_pres != 'undefined' && editDiv_pres )
			{
				if (sharing_level != VIEW_EVENTS_DETAILS && sharing_level !=VIEW_BUSY_TIME)
				{
					inline(invoked_event.obj);
				}
			}
			else
			{
				OpenEventToEdit(e, invoked_event.obj);
			}
		}
		Grid.DeleteShadowElement();
	} 
	else //drag event
	{
		if (sharing_level != VIEW_EVENTS_DETAILS && sharing_level !=VIEW_BUSY_TIME) 
		{
			document.body.removeChild(drager);

			/*****dragkill****/
			dragit.style.display='none';
			mybox.dom.removeChild(dragit);
			/*****dragkill****/

			document.onselectstart = function() {}
			document.onselect = function() {}
			var id_obj = SplitEventId(invoked_event.id);
			var event_data = mycache.ids[id_obj.id];

			var id_calendar = event_data['calendar_id'];
			var prevFromDate = event_data['event_timefrom'];
			var prevTillDate = event_data['event_timetill'];
			var eventLen = prevTillDate.getTime() - prevFromDate.getTime() ; //in milliseconds
			var date = dhigh.d.toString();
			var time_from = dhigh.t;
			if (time_from == 'xx:xx') 
			{
				time_from = Fnum(prevFromDate.getHours(),2)+':'+Fnum(prevFromDate.getMinutes(),2);
			}

			var fromDate = new Date(date.substr(0,4), (date.substr(4,2)-1), date.substr(6,2), (time_from).substr(0,2), (time_from).substr(3,2));
			var this_event = Isplit(dhigh.id);
			
			var DailyPeriod = 0;
			
			if (event_data['event_repeats'] == 1 && event_data['repeat_period'] != undefined)
			{
				DailyPeriod = event_data['repeat_period'];
			}
			
			var data = {
				id_data: this_event,
				id_calendar: id_calendar,
				from_date: fromDate,
				prev_from_date : prevFromDate,
				from_time: escape(time_from),
				event_len: eventLen,
				daily_period: DailyPeriod 
			}
			if (mycache.exclusions[this_event.id_original]) 
			{
				ChooseForm.Show('DRAG', data, 1);
			} 
			else if (mycache.ids[this_event.id]['event_repeats'] == 0) 
			{
				ChooseForm.Show('DRAG', data, 0);
			} 
			else
			{
				ChooseForm.Show('DRAG', data);
			}
			indrag=0;
		}
	}
}

/**
 * @param id = { id (used in mycache.ids),  repeat (repeat_part), original (id+repeat)}
 */
function DeleteEventFromCacheAndGrid(id)
{
	var edata = mycache.ids[id], cudate, firstday, firstdayinrepeatweek;
	var dates;
	var datas = {common: [], allday: []};

	if (edata != undefined)
	{
		edata.weekdays = new Array(edata.sun, edata.mon, edata.tue, edata.wed, edata.thu, edata.fri, edata.sat);
		var oltill, id_elem, hours, mixid = '', i=0, days_arr = [], nextweek=0;
		if (edata.event_repeats == 1) // repeats
		{
			firstday = to8(edata.event_timefrom);
			i=0;
			nextweek=0;
			id_elem = id + '_' + i + '_' + firstday;
			cudate = firstday;
			firstdayinrepeatweek = move8(firstday, - (from8(firstday).getDay()));
			var isWeekdays = false, daynumber = 0;
			for (var n in edata.weekdays)
			{
				if (edata.weekdays[n] == 1)
				{
					isWeekdays = true;
					if (daynumber > 0)
					{
						firstdayinrepeatweek = move8(firstdayinrepeatweek, daynumber);
					}
					break;
				}
				daynumber++;
			}
			while (mycache.repeats[id_elem] != undefined)
			{ //delete from repeats cache
				if (mycache.exclusions[id_elem] != undefined)
				{
					hours = GetDateParams(mycache.exclusions[id_elem]['event_timetill'], false, new Array('FH'));
					oltill = to8(mycache.exclusions[id_elem]['event_timetill']);
					if (hours.fh == 0) oltill = move8(oltill,-1);
					dates = {
						from:to8(mycache.exclusions[id_elem]['event_timefrom']),
						till:oltill,
						allday:mycache.exclusions[id_elem]['event_allday']
					}
					delete mycache.exclusions[id_elem];
				}
				else
				{
					hours = GetDateParams(mycache.repeats[id_elem]['event_timetill'], false, new Array('FH'));
					oltill = to8(mycache.repeats[id_elem]['event_timetill'])
					if (hours.fh == 0) oltill = move8(oltill,-1);
					dates = {
						from:to8(mycache.repeats[id_elem]['event_timefrom']),
						till:oltill,
						allday:edata['event_allday']
					}
				}
				days_arr.push(dates);
				delete mycache.repeats[id_elem];
				i++;
				
				var repeat_order = Number(edata.repeat_order);
				var next_repeat;
				if (edata.weekdays.length > 0 && isWeekdays)
				{
					while (from8(cudate).getDay() <= 6) // Check all week
					{
						if (next_repeat == undefined || !next_repeat)
						{
							cudate = move8(cudate, 1);
							next_repeat = false;
						}
						if (edata.weekdays[from8(cudate).getDay()] == 1)
						{
							id_elem = id+'_'+i+'_'+cudate;
							break;
						}
						if (from8(cudate).getDay() == 6){
							next_repeat = true;
							nextweek++;
						}
						if (next_repeat)
						{
							if (edata.repeat_period == DAY)
								cudate = move8(firstdayinrepeatweek, i*repeat_order);
							else if (edata.repeat_period == WEEK)
								cudate = move8(firstdayinrepeatweek, 7*nextweek*repeat_order);
							else if (edata.repeat_period == MONTH)
								cudate = moveMonth8(firstdayinrepeatweek, i*repeat_order);
							else if (edata.repeat_period == YEAR)
								cudate = moveYear8(firstdayinrepeatweek, i*repeat_order);
							
							next_repeat = false;
							id_elem = id+'_'+i+'_'+cudate;
							break;
						}
					}
				}
				else
				{
					if (edata.repeat_period == DAY)
						firstday = move8(firstday, repeat_order);
					else if (edata.repeat_period == WEEK)
						firstday = move8(firstday, 7*repeat_order);
					else if (edata.repeat_period == MONTH)
						firstday = moveMonth8(firstday, repeat_order);
					else if (edata.repeat_period == YEAR)
						firstday = moveYear8(firstday, repeat_order);
					id_elem = id+'_'+i+'_'+firstday;
				}
			}
		} 
		else // do not repeats
		{
			hours = GetDateParams(edata.event_timetill, false, new Array('FH'));
			oltill = to8(edata.event_timetill);
			if (hours.fh == 0) oltill = move8(oltill,-1);
			days_arr[0] = {
				from: to8(edata['event_timefrom']),
				till:oltill,
				allday:edata['event_allday']
			}
		}

		var d, j, cnt, cuallday, tmpIdElem;
		for (d=0; d<days_arr.length; d++) {
			cudate = days_arr[d].from;
			cuallday = days_arr[d].allday;
			do {
				for (i in mycache.days[cudate]) {
					tmpIdElem = mycache.days[cudate][i];
					if (tmpIdElem.substr(0, id.length) == id)
					{
						id_elem = tmpIdElem;
						mycache.days[cudate].splice(i, 1);
						if (edata.event_allday == 1) {
							datas.allday.push(cudate);
						} else {
							datas.common.push(cudate);
						}
					}
				}
				if (mycache.days[cudate]!=undefined && mycache.days[cudate].length == 0) delete all_events_dates[cudate];

				if (mycache.d[id_elem] != undefined) {
					mixid = 'event_'+id_elem+'_0_0_'+cuallday;
					DeleteNode(mixid);
					delete mycache.d[id_elem];
				}
				if (mycache.w[id_elem] != undefined) {
					mixid = 'event_'+id_elem+'_0_1_'+cuallday;
					DeleteNode(mixid);
					delete mycache.w[id_elem];
				}
				if (mycache.m[id_elem] != undefined) {
					 cnt = 0;
					 var monthFirstLevel = mycache.m[id_elem]._width;
					 if (monthFirstLevel != undefined) {
						  for (j in monthFirstLevel) {//years
								for (k in monthFirstLevel[j]) {//weeks
									 mixid = 'event_'+id_elem+'_'+cnt+'_2_'+cuallday;
									 DeleteNode(mixid);
									 cnt++;
								}
						}
					}
					delete mycache.m[id_elem];
				}
				cudate = move8(cudate,1);
			} while (cudate <= days_arr[d].till);
		}
		delete mycache.ids[id];
	}
	return datas;
}
function DeleteRepeatEventPart(id_event, id_repeat, id_recurrence) {
	var i=0, d=0, cudate, oltill, hours, dates, days_arr = [], event_allday;
	var id_elem = id_event+'_'+id_repeat+'_'+id_recurrence, mixid = '', cnt, j;
	var datas = {
		common: [],
		allday: []
	}

	if (mycache.exclusions[id_elem] != undefined) {
		hours = GetDateParams(mycache.exclusions[id_elem]['event_timetill'], false, new Array('FH'));
		oltill = to8(mycache.exclusions[id_elem]['event_timetill']);
		if (hours.fh == 0) oltill = move8(oltill,-1);
		dates = {
			from:to8(mycache.exclusions[id_elem]['event_timefrom']),
			till:oltill
		}
		days_arr.push(dates);
		event_allday = mycache.exclusions[id_elem]['event_allday'];
	} else if (mycache.repeats[id_elem] != undefined) {
		hours = GetDateParams(mycache.repeats[id_elem]['event_timetill'], false, new Array('FH'));
		oltill = to8(mycache.repeats[id_elem]['event_timetill']);
		if (hours.fh == 0) oltill = move8(oltill,-1);
		dates = {
			from:to8(mycache.repeats[id_elem]['event_timefrom']),
			till:oltill
		}
		days_arr.push(dates);
		event_allday = mycache.repeats[id_elem]['event_allday'];
	} else {
		return datas;
	}

	for (d=0; d<days_arr.length; d++) {
		cudate = days_arr[d].from;
		do {
			for (i in mycache.days[cudate]) {
				if (mycache.days[cudate][i] == id_elem) {
					mycache.days[cudate].splice(i, 1);
					if (event_allday == 1) {
						datas.allday.push(cudate);
					} else {
						datas.common.push(cudate);
					}
					break;
				}
			}
			if (mycache.days[cudate].length == 0) delete all_events_dates[cudate];
			cudate = move8(cudate,1);
		} while (cudate <= days_arr[d].till);
	}

	if (mycache.d[id_elem] != undefined) {
		mixid = 'event_'+id_elem+'_0_0_'+event_allday;
		DeleteNode(mixid);
		delete mycache.d[id_elem];
	}
	if (mycache.w[id_elem] != undefined) {
		mixid = 'event_'+id_elem+'_0_1_'+event_allday;
		DeleteNode(mixid);
		delete mycache.w[id_elem];
	}
	if (mycache.m[id_elem] != undefined) {
		cnt = 0;
		var monthFirstLevel = mycache.m[id_elem]._width;
		if (monthFirstLevel != undefined) {
			for (j in monthFirstLevel) {//years
				for (k in monthFirstLevel[j]) {//weeks
					mixid = 'event_'+id_elem+'_'+cnt+'_2_'+event_allday;
					DeleteNode(mixid);
					cnt++;
				}
			}
		}
		delete mycache.m[id_elem];
	}
	delete mycache.exclusions[id_elem];
	return datas;
}

function OpenEventToEdit(e, div) {
	if (e != undefined || e != null) {
		e = e || window.event;
		e.cancelBubble = true;
	}
	var tfDateObj, ttDateObj, excluded = false, eventData;

	var iddata = Isplit(div.id);

	if (mycache.exclusions[iddata.id_original] != undefined) {
		eventData = mycache.exclusions[iddata.id_original];
		tfDateObj = eventData.event_timefrom;
		ttDateObj = eventData.event_timetill;
		excluded = true;
	} else {
		eventData = mycache.ids[iddata.id];
		var repeat_data = mycache.repeats[iddata.id_original];
		if (eventData.event_repeats == 1 && repeat_data) {
			tfDateObj = repeat_data.event_timefrom;
			ttDateObj = repeat_data.event_timetill;
		} else {
			tfDateObj = eventData.event_timefrom;
			ttDateObj = eventData.event_timetill;
		}
	}

	var editevent_form_data = {
		subject 	: eventData.event_name,
		dateFrom	: tfDateObj,
		dateTill	: ttDateObj,
		description	: eventData.event_text,
		location	: eventData.event_location,
		event_id	: iddata.id_original,
		calendar_id : eventData.calendar_id,
		excluded	: excluded,
		allday		: eventData.allday_flag
	}
	EventForm.ShowEventForm(editevent_form_data);
}

function ShowMoreEventsList() {
	if ($id('mfet')) {
		HideMoreEventsList();
		return;
	}
	HideMoreEventsList();
	var date_full_id, id_data, date_id, mdata, month_area, month_area_height;
	var showbox, showwid, showhei, more_block, showmax, showtop, showleft;
	var showover, stx = '', mtitl, event_name, event_id, real_event;
	var event_data, calendar_data, event_link, mtf, mtt, mtr, close_btn;

	date_full_id = String(this.id);
	id_data = date_full_id.split('_');
	date_id = Number(id_data[1]);
	mdata = more_dates[date_id]; //global array  = ['date'] = array(id1, id2, ... , idn)
	month_area = $id('area_2_month');
	month_area_height = ParseToNumber(month_area.offsetHeight);
	showbox = Grid.GetGridParams(1);
	showwid = 1.5*Math.floor(ParseToNumber(month_area.offsetWidth)/7); //cell_width *1.5
	showhei = mdata.length*13+1;

	more_block = document.createElement('div');
	showmax = 3 * Math.floor(month_area_height/6);
	if (showhei>showmax) {
		showhei = showmax;
		more_block.style.overflowY = 'scroll';
	}
	showtop = showbox.top + ParseToNumber(this.offsetTop) + 2;
	showleft = showbox.left + ParseToNumber(this.offsetLeft) + ParseToNumber(this.offsetWidth) + 2;
	showover = showtop + showhei - (month_area_height + showbox.top);
	if (showover > 0) showtop -= showover;

	more_block.style.top = showtop + 'px';
	more_block.style.left = showleft + 'px';
	more_block.style.width = showwid + 'px';
	more_block.style.height = showhei + 'px';
	more_block.id = 'mfet';
	more_block.className = 'event_select_box';

	for (var i in mdata) {
		event_name = '';
		event_id = mdata[i];
		real_event = SplitEventId(event_id);
		if (mycache.exclusions[event_id] != undefined) {
			event_data = mycache.exclusions[event_id];
		}
		else {
			event_data = mycache.ids[real_event.id];
		}
		event_name = event_data.event_name;
		stx += "\n"+event_name;
		calendar_data = mycache.calendars[event_data.calendar_id];

		event_link = document.createElement('span');
		event_link.className = 'event_element_select_box';
		if (calendar_data.sharing_level == VIEW_BUSY_TIME) 
		{
			event_link.style.color = GetNumberOfColor(15);
		}
		else 
		{
			event_link.style.color = GetNumberOfColor(calendar_data.color);
			event_link.onmouseover = function()
			{
				this.style.textDecoration='underline';
			}
			event_link.onmouseout = function()
			{
				this.style.textDecoration='none';
			}
			event_link.onmousedown = function(ev)
			{
				drag2start(ev,this);
				return false;
			}
		}

		event_link.unselectable = 'on';
		event_link.id = 'event_' + event_id + '_0_3_'+event_data.event_allday;

		mtf = gtime(event_data.event_timefrom);
		mtt = gtime(event_data.event_timetill,24);
		
		if (event_data.event_allday == 0) {
			mtitl = mtf.utime+' - '+mtt.utime+', '+mtf.udate;
		}
		else {
			mtr = range8(mtf.to8,mtt.to8)+1;
			if (mtr==1) {
				mtitl=mtf.udate;
			}
			else {
				mtitl=mtf.udate+' - '+mtt.udate+' ('+mtr+' days)';
			}
		}
		event_link.title = mtitl;
		event_link.appendChild(document.createTextNode(event_name));
		event_link.style.MozUserSelect='none';
		event_link.style.KhtmlUserSelect='event_link.style.';
		event_link.style.userSelect='none';
		more_block.appendChild(event_link);
		more_block.appendChild(document.createElement('br'));
	}
	close_btn = document.createElement('img');
	close_btn.src = './calendar/skins/calendar/close-popup.png';
	close_btn.className = 'close_more_list';
	more_block.onclick = HideMoreEventsList;
	more_block.appendChild(close_btn);

	document.body.appendChild(more_block);
	shownfetch = 2;
}

function drag2start(e, obj) {
	indrag = true;
	evobj = obj;
	var idd = evobj.id;
	var modez=Isplit(idd);
	vMid = modez.id_original; /*global*/
	if (mycache.exclusions[vMid] != undefined) {
		evlink = mycache.exclusions[vMid];
	}
	else {
		evlink = mycache.ids[modez.id]; /*global*/
	}
	var mybox = Grid.GetGridParams(0);
	stc = coordss(e, evobj,0,mybox,0); /*global*/ //view was setted as MONTH
	stx = stc.x; /* is global? */
	sty = stc.y;
	dragon=0;
	var calendar_data = mycache.calendars[evlink.calendar_id];
	if (calendar_data != undefined) {
		EventOn('mouseup',drag2done,true);
		if (calendar_data.sharing_level != VIEW_EVENTS_DETAILS) {
			EventOn('mousemove',drag2move,true);
		}
	}
	document.onselectstart = function() {
		return false;
	}
	document.onselect = function() {
		return false;
	}
}

function drag2move(e) {
	mybox = Grid.GetGridParams(0);
	mypos = coordss(e, evobj, 0,mybox,0);/*global*/ //view was setted as month
	var drag_w = Math.floor(mybox.width/7);
	var zone_w = mybox.width-drag_w;
	var zone_h = mybox.height-LINE_HEIGHT;
	myx=mypos.x;/* is global? */
	if (myx<0) myx=0;
	if (myx>zone_w) myx=zone_w;
	myy=mypos.y; /* is global? */
	if (myy<0) myy=0;
	if (myy>zone_h) myy=zone_h;
	if ((dragon==0)&&((Math.abs(myx-stx)>5)||(Math.abs(myy-sty)>5))) {
		HideMoreEventsList();
		drag2show();
		dragon=1;
	} else if (dragon==1) {
		drag2show();
	}
}

function drag2done() {
	EventOff('mouseup',drag2done,true);
	EventOff('mousemove',drag2move,true);
	document.onselectstart = function() {}
	document.onselect = function() {}
	if (dragon==0) {
		HideMoreEventsList();
		OpenEventToEdit(null, evobj);
	} else {//dreg event from more list
		ShowInfo(Lang.InfoSaving);
		document.body.removeChild(drager);
		mybox.dom.removeChild(dragit);

		var enc = mypos;/*is global???*/

		var id_obj = SplitEventId(vMid);
		var event_data = mycache.ids[id_obj.id];
		if (event_data == undefined) return;
		var prevTillDate = event_data.event_timetill;
		var prevFromDate = event_data.event_timefrom;
		var time_from = Fnum(prevFromDate.getHours(),2)+':'+Fnum(prevFromDate.getMinutes(),2);
		var eventLen = prevTillDate.getTime() - prevFromDate.getTime() ; //in milliseconds
		var date = enc.d.toString();
		var fromDate = new Date(date.substr(0,4), (date.substr(4,2)-1), date.substr(6,2), (time_from).substr(0,2), (time_from).substr(3,2));
		var from_time = Fnum(prevFromDate.getHours(),2)+':'+Fnum(prevFromDate.getMinutes(),2);

		var this_event = Isplit(enc.id);

		var data = {
			id_data: this_event,
			id_calendar: event_data.calendar_id,
			from_date: fromDate,
			prev_from_date : prevFromDate,
			from_time: from_time,
			event_len: eventLen
		}
		if (mycache.exclusions[this_event.id_original]) {
			ChooseForm.Show('DRAG', data, 1);
		} else if (mycache.ids[this_event.id]['event_repeats'] == 0) {
			ChooseForm.Show('DRAG', data, 0);
		} else {
			ChooseForm.Show('DRAG', data);
		}
	}
	indrag=false;
}

/**
 * evlink global
 * mybox global
 */
function drag2show() {
	if (dragon==0) {
		drager = document.createElement('div');
		drager.id = 'drager';
		drager.style.display = 'none';
		drager.style.position='absolute';
		drager.style.borderWidth='3px';
		drager.style.borderColor='#CCCCCC';
		drager.className='eventcontainer_'+mycache.calendars[evlink.calendar_id].color;

		var div1 = document.createElement('div');
		div1.className = 'event';
		div1.style.width = (mybox.width/7)+'px';
		var div2 = document.createElement('div');
		div2.className='a';
		var div3 = document.createElement('div');
		div3.className='b';
		div1.appendChild(div2);
		div1.appendChild(div3);

		var div4 = document.createElement('div');
		div4.className='event_middle';
		div4.style.height='3.8ex';
		var div5 = document.createElement('div');
		div5.className='event_text';
		var div6 = document.createElement('div');
		div6.className='time';

		var tf = gtime(evlink.event_timefrom);
		var tt = gtime(evlink.event_timetill,24);
		if (evlink.event_allday == 0) {
			titl = tf.utime+' - '+tt.utime+', '+tf.udate;
		} else {
			tr = range8(tf.to8,tt.to8)+1;
			if (tr == 1) titl = tf.udate; else titl = tf.udate+' - '+tt.udate+' ('+tr+' days)';
		}

		var tex1 = document.createTextNode(titl);
		div6.appendChild(tex1);

		var div7 = document.createElement('div');
		div7.className = 'text';
		var tex2 = document.createTextNode(evlink.event_name);
		div7.appendChild(tex2);
		div5.appendChild(div6);
		div5.appendChild(div7);
		div4.appendChild(div5);
		div1.appendChild(div4);
		var div8 = document.createElement('div');
		div8.className='c';
		div1.appendChild(div8);
		var div9 = document.createElement('div');
		div9.className='a';
		div1.appendChild(div9);
		drager.appendChild(div1);

		document.body.appendChild(drager);
		drager.style.zIndex=65535;

		dragit = document.createElement('div');
		dragit.style.position='absolute';
		dragit.className='dragit_area';
		dragit.style.height=(Math.floor(mybox.height/6)-1)+'px';
		dragit.style.width=(Math.floor(mybox.width/7)-1)+'px';
		dragit.style.zIndex=65500;
		dragit.style.display='none';
		dragit.id ='dragit';
		mybox.dom.appendChild(dragit);
	}
	leftpos=myx+mybox.left;
	toppos=myy+mybox.top;

	drager.style.left=leftpos+'px';
	drager.style.top=toppos+'px';

	dragit.style.left=1+mypos.xcol+'px';
	dragit.style.top=1+mypos.xrow+'px';

	if (dragon==0) {
		drager.style.display='inline';
		dragit.style.display='block';
	}
}

/**
 * return array
 */
function CacheLoadCalendars() {
	return calendar_cache;
}


/** inline reducting */

function TextareaKeyDownHandler(ev, obj, count)
{
	ev = ev ? ev : window.event;
	var key = -1;
	if (window.event)
	{
	  key = window.event.keyCode;
	}
	else if (ev)
	{
	  key = ev.which;
	}
	if (key == 13) 
	{
		if (Trim(obj.innerHTML) != '')
		{
			var text = Trim(obj.innerHTML);
			text = text.replace('<br>', " ");
			var this_event = Isplit(obj.id);
			var data = {
				id_data: this_event,
				id_calendar: mycache.ids[this_event.id].calendar_id,
				name : encodeURIComponent(text.substr(0,50))
			}
			if (mycache.exclusions[this_event.id_original]) 
			{
				ChooseForm.Show('INLINE', data, 1);
			}
			else if (mycache.ids[this_event.id].event_repeats == 0) 
			{
				ChooseForm.Show('INLINE', data, 0);
			}
			else 
			{
				ChooseForm.Show('INLINE', data);
			}
			StopEvents(ev);
			obj.blur();
		} 
		else 
		{
			obj.innerHTML='';
		}
	}
	if (key != 8 //delete
		&& key != 46 //backspace
		&& key != 16 //shift
		&& key != 17 //ctrl
		&& key != 18 //alt
		&& key != 35 //end
		&& key != 36 //home
		&& key != 37 //to the right
		&& key != 38 //up
		&& key != 39 //to the left
		&& key != 40) //down
	{ 
		if (!ev.ctrlKey && !ev.shiftKey)
		{
			text = obj.innerHTML;
			if (text.length >= count && !OperaDetect())
			{
				obj.innerHTML = text.substr(0, count);
				StopEvents(ev);
				return false;
			}
		}
	}
	else if (key == 35 && OperaDetect()) //key "End" in Opera
	{
		StopEvents(ev);
		return false;
	}
	return true;
}

function TextareaBlurHandler(textDiv)
{
	var parent = Isplit(textDiv.id);
	var event_name = (mycache.exclusions[parent.id_original] != undefined) ? mycache.exclusions[parent.id_original]['event_name'] : mycache.ids[parent.id]['event_name'];
	if (event_name != textDiv.innerHTML)
	{
		CleanNode(textDiv);
		textDiv.appendChild(document.createTextNode(event_name));
	}
	textDiv.scrollTop = 0;
	editDiv_pres = false;
}

function inline(obj)
{
	var parent = Isplit(obj.id);
	var textar = $id('textdiv_' + parent.id_object);
	if (textar != undefined) {
		textar.focus();
	}
}
/*end inline reducting*/

/**
 * @elem eventPosData - mycache.d or mycache.m or mycache.w
 * @elem eventTextData- mycahe.ids or next data  format
 *	{evnt_id, calendar_id, event_name, event_text, event_timefrom, event_timetill, event_allday .. }
 * @param event_id 'id_repeat'
 * @param viewMode -> 0-day, 1-week, 2-month
 * @param date - the day when event builds
 *	eventPosData.arrindex takes 2 types of values
 *   - for day view value is data yyyymmdd
 *   - for week and month views value is week number in weekLimits array
*/
function AddEventToGrid(event_id, viewMode, date)
{
	var i, j, cnt, eventPosData, evData, timefrom, timetill, real_event, eventTextData, calendar_container;
    var arrows = ['','_left','_right','_both'], common_id = '', startIndex=0;
	var endIndex=0, event_name, tf, tt, calendar_obj, evitem;
	var calendar_sharing_level = null, ari, allday, arsuf, div1, div2, div3;
	var div4, div5, div6, div7, div8, div9, divtit, titl, tex1, start_year;
	var end_year, tmp_start, tmp_end, build_lims = [];

	if (viewMode == DAY)
	{
		evData = mycache.d[event_id];
		tmp_start = from8(showLimits.day);
		startIndex = GetEventWeek(tmp_start);
		endIndex = startIndex;
		start_year = tmp_start.getFullYear();
		end_year = start_year;
	}
	else if (viewMode == WEEK)
	{
		evData = mycache.w[event_id];
		tmp_start = from8(showLimits.weekFrom);
		tmp_end = from8(showLimits.weekTill);
		startIndex = GetEventWeek(tmp_start);
		endIndex = GetEventWeek(tmp_end);
		start_year = tmp_start.getFullYear();
		end_year = tmp_end.getFullYear();
	}
	else
	{
		evData = mycache.m[event_id];
		tmp_start = from8(showLimits.monthFrom);
		tmp_end = from8(showLimits.monthTill);
		startIndex = GetEventWeek(tmp_start);
		endIndex = GetEventWeek(tmp_end);
		start_year = tmp_start.getFullYear();
		end_year = tmp_end.getFullYear();
	}

	eventPosData = copyar(evData);
	if (eventPosData == undefined || eventPosData._width == undefined) return;
	if (mycache.exclusions[event_id] != undefined)
	{
		eventTextData = mycache.exclusions[event_id];
		timefrom = eventTextData.event_timefrom;
		timetill = eventTextData.event_timetill;
	}
	else
	{
		real_event = SplitEventId(event_id);
		eventTextData = mycache.ids[real_event.id];
		if (eventTextData == undefined) return;
		if (real_event.repeat != 0 || (real_event.date != 0 
			&& real_event.date != to8(eventTextData.event_timefrom)))
		{
			var repeatData = mycache.repeats[event_id];
			timefrom = repeatData.event_timefrom;
			timetill = repeatData.event_timetill;
		}
		else
		{
			timefrom = eventTextData.event_timefrom;
			timetill = eventTextData.event_timetill;
		}
	}

	allday = eventTextData.event_allday;
	calendar_container = $id(eventPosData._parent);
	calendar_obj = mycache.calendars[eventTextData.calendar_id];
	if (calendar_obj != undefined)
	{
		calendar_sharing_level = calendar_obj.sharing_level;
	}
	event_name = eventTextData.event_name;
	divtit = event_name;
	titl = '';
	if (allday == 0)
	{
		tf = GetDateParams(timefrom, false, new Array('TIME'));
		tt = GetDateParams(timetill, true, new Array('TIME'));
		titl = tf.time + ' - ' + tt.time;
		divtit = '['+titl+'] '+divtit;
	}

	evitem = eventPosData;
	j = 0;

	if (start_year == end_year)
	{
		for (cnt in evitem._width[start_year])
		{
			if (cnt >= startIndex && cnt <= endIndex)
			{
				build_lims[start_year] = {start: startIndex, end: endIndex + 1}
				break;
			}
		}
	}
	if (start_year < end_year)
	{
		for (cnt in evitem._width[start_year]) {//this part of code allows build event from real current part, not from 0 always
			if (cnt >= startIndex && cnt <= (weekLimits[start_year].length-2))
			{
				build_lims[start_year] = {start: startIndex, end: weekLimits[start_year].length-1}
				break;
			}
		}

		build_lims[end_year] = {start: 0, end: endIndex + 1}
	}
	if (start_year > end_year)
	{
		return;
	}
	for (var y in build_lims)
	{
		var index = Number(build_lims[y].start);
		var end = Number(build_lims[y].end);
		while (index < end)
		{
			if (evitem._width[y] != undefined && evitem._width[y][index] != undefined)
			{
				common_id = event_id+'_'+j+'_'+viewMode+'_'+eventTextData.event_allday;
				var mixid = 'event_'+common_id;

				ari = 0;
				if (viewMode == DAY)
				{
					for (i in evitem._arrindex[y])
					{
						if (i == date)
						{
							ari = evitem._arrindex[y][i];
							break;
						}
					}
				}
				else
				{
					ari = evitem._arrindex[y][index];
				}
				editDiv_pres = false;
				arsuf = arrows[ari];
				if (document.getElementById(mixid)) {
					break;
				}
				div1 = document.createElement('div');
				div1.id = mixid;
				div1.className = 'event';
				div1.style.marginTop = evitem._off+'px';
				div1.style.width = evitem._width[y][index] + '%';
				div1.style.left = evitem._left[y][index] + '%';
				div1.style.top = evitem._top;

				div2 = document.createElement('div');
				div3 = document.createElement('div');

				div4 = document.createElement('div'); /* global */
				div4.className = 'event_middle';
				div4.style.height = evitem._height;
				div4.id = 'mid_' + common_id;

				div5 = document.createElement('div');
				div5.className = 'event_text'+arsuf;

				div6 = document.createElement('div');
				div6.id = 'time_'+common_id;

				div7 = document.createElement('div');
				div7.className = 'text';
				if (allday == 1)
				{
					div6.style.display = 'none';
					div7.style.top = '1px';
				}
				var resizer_class = 'd';
				if (calendar_sharing_level == VIEW_EVENTS_DETAILS 
					|| calendar_sharing_level == VIEW_BUSY_TIME)
				{
					resizer_class = '';
				}
				if (ari == 0)
				{
					if (viewMode == MONTH || allday == 1) {
						div2.className = 'a' + arsuf + ' a';
						div3.className = 'b' + arsuf + ' b';
					}
					else {
						div2.className = 'a ' + resizer_class;
						div3.className = 'b ' + resizer_class;
					}
					tex1 = document.createTextNode(titl);
					div6.className = 'time';
					div6.appendChild(tex1);
				}
				else
				{
					div2.className = 'a' + arsuf + ' a';
					div3.className = 'b' + arsuf + ' b';
					div6.className = 'time time' + arsuf;
				}

				var textDiv = document.createElement('div');
				textDiv.id = 'textdiv_'+common_id;
				textDiv.className = 'word_wrap';
				textDiv.style.overflow = 'hidden';

				if (OperaDetect() || SafariDetect().isSafari)
				{
					textDiv.style.overflowY = 'hidden';
				}
				textDiv.style.marginTop = '2px';
				var h = ParseToNumber(evitem._height) - 1.9;
				textDiv.style.maxHeight = ((h<0) ? 0 : h) + 'ex';

				if (calendar_sharing_level != VIEW_EVENTS_DETAILS 
					&& calendar_sharing_level != VIEW_BUSY_TIME)
				{
					textDiv.style.cursor = 'text';
					textDiv.contentEditable = 'true';
					textDiv.onblur = function()
					{
						TextareaBlurHandler(this);
					}
					textDiv.onkeydown = function (e)
					{
						TextareaKeyDownHandler(e, this, 50);
					}
					textDiv.onmousedown = function()
					{
						editDiv_pres = true;
					}
					textDiv.onmouseup = function()
					{
						editDiv_pres = false;
					}
				}
				else if (calendar_sharing_level == VIEW_BUSY_TIME)
				{
					textDiv.style.cursor = 'default';
					textDiv.contentEditable = 'false';
				}
				else if (calendar_sharing_level == VIEW_EVENTS_DETAILS)
				{
					textDiv.style.cursor = 'pointer';
					textDiv.contentEditable = 'false';
				}
				
				real_event = SplitEventId(event_id);
				var
					eventTextData = mycache.ids[real_event.id],
					bAppointment = eventTextData != undefined && eventTextData.appointment
				;
				if (bAppointment)
				{
					textDiv.style.cursor = 'pointer';
					textDiv.contentEditable = 'false';
				}
				
				div8 = document.createElement('div');
				div8.id = 'c_'+common_id;

				div9 = document.createElement('div');
				div9.id = 'a_' + common_id;

				if (viewMode == MONTH || allday == 1 || bAppointment)
				{
					div8.className = 'b' + arsuf + ' b';
					div9.className = 'a' + arsuf + ' a';
				}
				else
				{
					div8.className = 'b ' + resizer_class;
					div9.className = 'a ' + resizer_class;
				}
				if (calendar_sharing_level != VIEW_BUSY_TIME)
				{
					div1.onmouseout = EventMouseOutHandler;
					div1.onmouseover = EventMouseOverHandler;
					div1.style.cursor = 'pointer';
					div1.title = divtit;
					div4.onmousedown = function(e) {
						if(typeof editDiv_pres == 'undefined' ||  !editDiv_pres )
						{
							invoke(e, this);
							StopEvents(e);
						}
					}
					div4.onmouseup=function() {
					}
					if (viewMode == MONTH || allday == 1 || bAppointment) {
						div2.onmousedown = div3.onmousedown = div8.onmousedown = div9.onmousedown = function(e) {
							if(typeof editDiv_pres == 'undefined' ||  !editDiv_pres )
							{
								invoke(e, this);
								StopEvents(e);
							}
						}
					}
					var tex2=document.createTextNode(event_name);
					textDiv.appendChild(tex2);
					if (!bAppointment && viewMode < MONTH && calendar_sharing_level != VIEW_EVENTS_DETAILS) {//viewMode < MONTH => DAY or WEEK
						div2.onmousedown = function (e) {
							Resize.SetResizeObject(this, false);
							StopEvents(e);
						}
						div3.onmousedown = function (e) {
							Resize.SetResizeObject(this, false);
							StopEvents(e);
						}
						div8.onmousedown = function (e) {
							Resize.SetResizeObject(this);
							StopEvents(e);
						}
						div9.onmousedown = function (e) {
							Resize.SetResizeObject(this);
							StopEvents(e);
						}
					}
				} else {
					div1.style.cursor = 'default';
					div1.title = titl;
				}

				div1.appendChild(div2);
				div1.appendChild(div3);
				div7.appendChild(textDiv);
				div5.appendChild(div6);
				div5.appendChild(div7);
				div4.appendChild(div5);
				div1.appendChild(div4);
				div1.appendChild(div8);
				div1.appendChild(div9);
				calendar_container.appendChild(div1);
				var scrollWidth = calcScroll(textDiv);
				textDiv.style.marginRight = '-' + scrollWidth + 'px';
				j++;
			}
			index++;
		}
	}
}

function RefreshData_CallBack(data) {
	var
		old_calendars = [], //hash = associative array contains only numbers of calendars
		delete_calendars = [], //hash
		exist_calendars = [], //hash
		new_calendars = [], //hash
		hash = [], //hash
		i, calendar_id, calendar_obj, calendar_val,
	
		calendars_cache = [],
		calendars = data.calendars,
		events_result = data.events,
		reminders_result = data.reminders,
		exclusions_result = data.exclusions,
		appointments_result = data.appointments
	;

	if (!ServErr(data, Lang.ErrorLoadCalendar)) 
	{
		cachedMonths = [];

		if (calendars != undefined)
		{
			if (calendars.shared) {
				for (i in  calendars.shared) 
				{
					calendar_val = calendars.shared[i];
					calendar_id = calendar_val.calendar_id;
					calendars_cache[calendar_id] = calendar_val;
				}
			}
			if (calendars.user) {
				for (i in  calendars.user) 
				{
					calendar_val = calendars.user[i];
					calendar_id = calendar_val.calendar_id;
					calendars_cache[calendar_id] = calendar_val;
				}
			}
			//compare old and new sharings cache. create delete-calendars array
			for (calendar_id in mycache.calendars) 
			{
				calendar_obj = mycache.calendars[calendar_id];
				old_calendars[calendar_obj.calendar_id] = true;
			}
		}
		for (calendar_id in calendars_cache) 
		{
			hash[calendar_id] = true;
		}
		for (calendar_id in old_calendars) 
		{
			if (hash[calendar_id]) exist_calendars[calendar_id] = true;
			else delete_calendars[calendar_id] = true;
		}
		for (calendar_id in calendars_cache) 
		{
			if (!exist_calendars[calendar_id]) new_calendars[calendar_id] = true;
		}
		if (ObjectPropertiesCount(delete_calendars) == 0 && ObjectPropertiesCount(exist_calendars) == 0 && 
			ObjectPropertiesCount(new_calendars) == 0) 
		{
			ShowReport(Lang.NoNewSharedCalendars);
			return;
		}
		//delete all events that belong to calendar from delete_calendars array
		if (ObjectPropertiesCount(delete_calendars) != 0) 
		{
			for (calendar_id in delete_calendars) 
			{
				calendar_obj = mycache.calendars[calendar_id];
				if (calendar_obj != undefined) Calendars.DeleteCalendarElement(calendar_obj);
			}
		}

		//create new calendar
		if (ObjectPropertiesCount(new_calendars) != 0) 
		{
			for (calendar_id in new_calendars) 
			{
				mycache.calendars[calendar_id] = calendars_cache[calendar_id];
				Calendars.CreateCalendarElement(calendars_cache[calendar_id]);
			}
		}

		//update calendar
		var mycache_tmp, sharedcache_tmp, calendarName, publicDiv;
		for (calendar_id in exist_calendars) 
		{
			mycache_tmp = mycache.calendars[calendar_id];
			sharedcache_tmp = calendars_cache[calendar_id];

			if (mycache_tmp != sharedcache_tmp) 
			{
				mycache.calendars[calendar_id]=calendars_cache[calendar_id];
				if (mycache_tmp.name != sharedcache_tmp.name) 
				{
					calendarName = $id('calendar_'+calendar_id+'_text');
					calendarName.title = sharedcache_tmp.name;
					calendarName.innerHTML = sharedcache_tmp.name;
				}
				if (mycache_tmp.publication_hash != sharedcache_tmp.publication_hash) 
				{
					publicDiv = $id('calendar_'+calendar_id+'public');
					if (sharedcache_tmp.publication_hash != null) 
					{
						publicDiv.className = 'ml_publiclink';
					} 
					else 
					{
						publicDiv.className = 'hide';
					}
				}
				if ((mycache_tmp.color != sharedcache_tmp.color) ||
					(mycache_tmp.sharing_level != sharedcache_tmp.sharing_level))
				{
					if (sharedcache_tmp.sharing_level == VIEW_BUSY_TIME) 
					{
						ChangeColorContainer(calendar_id, 15);
					} 
					else 
					{
						ChangeColorContainer(calendar_id, sharedcache_tmp.color);
					}
				}
			}
		}

		//show link 'Check Shared Calendar or Refresh Calendar Header'
		var shared_calendars = $id('shared_calendars');
		if ((shared_calendars.childNodes) && (shared_calendars.childNodes.length > 0)) 
		{
			$id('sharedCalendarsHeader').style.display = 'inline';
		} 
		else 
		{
			$id('sharedCalendarsHeader').style.display = 'none';
		}

		var j, k, allow_add = false, 
			render_dates = {
				common:[], 
				allday:[]
			}, 
			events = [], excl = [];
		events = events_result;
		if (typeof(exclusions_result) == 'object') 
		{
			for (j  in exclusions_result) 
			{
				allow_add = false;
				for (k in mycache.calendars) 
				{
					if (exclusions_result[j]['calendar_id'] == k) 
					{
						allow_add = true;
						break;
					}
				}
				if (!allow_add) 
				{
					exclusions_result[j]['is_deleted'] = 1;
				}
				exclusions_result[j]['event_timefrom'] = dt2date(exclusions_result[j]['event_timefrom']);
				exclusions_result[j]['event_timetill'] = dt2date(exclusions_result[j]['event_timetill']);
				mycache.exclusions[j] = exclusions_result[j];
				excl[j] = exclusions_result[j];
			}
		}
		mycache.reminders = reminders_result;
		mycache.appointments = appointments_result;

		var ex, event, event_id, del_dates = [], add_dates = [];
		for (i in events) 
		{
			event = events[i];
			event.event_timefrom = dt2date(event.event_timefrom);
			event.event_timetill = dt2date(event.event_timetill);
			if (event.repeat_until != undefined) event.repeat_until = dt2date(event.repeat_until);
			event_id = event.event_id;
			calendar_id = event.calendar_id;
			if ((mycache.calendars != undefined) && (mycache.calendars[calendar_id] != undefined))
			{
				del_dates = DeleteEventFromCacheAndGrid(event_id);
				render_dates = AddEventToCache(event);
				BuildEvent(render_dates, event_id);
				RenderScreens(render_dates);
				
				if (mycache.ids[event_id] == undefined) 
				{ //add event
					render_dates = AddEventToCache(event);
					BuildEvent(render_dates, event_id);
				} 
				else 
				{
//					if (mycache.ids[event_id] != event) 
//					{ //update event
						del_dates = DeleteEventFromCacheAndGrid(event_id);
						for(ex in excl) 
						{
							mycache.exclusions[ex] = excl[ex];
						}
						add_dates = AddEventToCache(event);
						BuildEvent(add_dates, event_id);
						render_dates = AddToArray(del_dates, add_dates);
//					}
				}
				
			}
		}
		
		for (i in mycache.ids) 
		{
			event = mycache.ids[i];
			event_id = event.event_id;
			calendar_id = event.calendar_id;
			if (/*exist_calendars[calendar_id] && */events_result[event_id] == undefined) 
			{
				del_dates = DeleteEventFromCacheAndGrid(event_id);
				render_dates = AddToArray(render_dates, del_dates);
			}
		}
		
		cmScroll_resize();
		RenderScreens(render_dates);
		RenderMonth();
		Grid.FillGrid(mydate);
		Grid.RecalcScrollArrows();
		calendarInManager.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
		EventForm.RedrawCalendarSelect();	
		HideInfo();	
	}
}

function RefreshData() {
	ShowInfo(Lang.InfoLoading);

	if (monthLimits.length > 0)
	{
		var todayYear = mydate.getFullYear();
		var todayMonth = mydate.getMonth();
		var monthStart = to8(monthLimits[todayYear][todayMonth].bigStart);
		var monthEnd = to8(monthLimits[todayYear][todayMonth].bigEnd);
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		requestParams = 'action=get_data' +
						'&from=' + monthStart +
						'&till=' + monthEnd +
						'&nocache=' + Math.random();

		if (req) {
			sendARequest(req, url, requestParams, RefreshData_CallBack);
		}
	}
}

function ChangeColorContainer(id, numb)
{
	$('#calendar_'+id).removeClass().addClass('eventcontainer_'+numb);
	$('#calendar_'+id+'public img:first').attr('src','./calendar/skins/calendar/published'+numb+'.png');
	var i;
	for(i=1; i<6; i++){
		var el = $('#container_'+id+'_'+i);
		
		if (el){
			el.removeClass().addClass('eventcontainer_'+numb);
			if (!$('#checkbox_'+id).prop("checked")){
				el.addClass('hide');
			}
		}
	}
	var row = mycache.calendars[id];
	var array = delarr(mycache.colors, row.color);
	mycache.colors = [];
	for(i=0; i<array.length; i++){
		mycache.colors[array[i]] = array[i];
	}
	mycache.colors[numb] = numb;
	row.color = numb;
}

function LoadSettingsFromServer_CallBack(settings)
{
	mydate = new Date();

	if (!ServErr(settings, Lang.ErrorGeneral))
	{
		$('#toolbar').css('display', 'block');
		$('#main_block').css('display', 'block');

		//settings
		setcache = ConvertObjectVariablesToTheirTypes(settings);
		DefineDefaultSettings();
		
		GetLimits(mydate);
		calendarInManager = new CCalendarMini();
		Selection = new CSelection();
		Resize = new CResize();
		Grid = new CGrid(mydate);
		Grid.LoadView();
		
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		requestParams = 'action=get_data&nocache=' + Math.random();
		if (req) 
		{
			sendARequest(req, url, requestParams, LoadDataFromServer_CallBack);
		}
	}
}

function LoadDataFromServer_CallBack(data)
{
	var i, j, k;
	var color = [], calendar_cache = [], allcalendars, calendars_part, calendar_val;
	var todayYear, todayMonth, events = [], event, allow_add;

	if (!ServErr(data,Lang.ErrorGeneral))
	{
		//calendars
		allcalendars = data.calendars;
		for (i in  allcalendars)
		{
			calendars_part = allcalendars[i];
			for (j in  calendars_part)
			{
				calendar_val = calendars_part[j];
				color[calendar_val.color] = calendar_val.color;
				calendar_cache[calendar_val.calendar_id] = calendar_val;
			}
		}
		mycache.calendars = calendar_cache;
		mycache.colors = color;
		//events
		if (data.events != undefined)
		{
			if (data.events != undefined) events = data.events;
			var exclusionsData = data.exclusions, excl,  exclusions = [];
			for (excl in exclusionsData) {
				var excl_id = 0, oneExclusion = exclusionsData[excl];
				var eventData = events[oneExclusion.event_id];
				var cudate = to8(dt2date(eventData.event_timefrom));
				if (Number(oneExclusion.id_repeat) == 0 
					&& oneExclusion.id_recurrence != cudate)
				{
					for (i=0; i<eventData.repeat_num; i++)
					{
						if (cudate == oneExclusion.id_recurrence)
						{
							oneExclusion.id_repeat = i;
							break;
						}
						cudate = moveToPeriod8(cudate, 1, eventData.repeat_period);
					}
				}
				else if (Number(oneExclusion.id_recurrence) == 0)
				{
					if (Number(oneExclusion.id_repeat) != 0)
					{
						cudate = moveToPeriod8(cudate, oneExclusion.id_repeat, eventData.repeat_period);
					}
					oneExclusion.id_recurrence = cudate;
				}
				excl_id = oneExclusion.event_id + '_' + oneExclusion.id_repeat + '_' + oneExclusion.id_recurrence;
				exclusions[excl_id] = oneExclusion;
			}

			if (exclusions != undefined)
			{
				for (j in exclusions)
				{
					allow_add = false;
					for (k in mycache.calendars)
					{
						if (exclusions[j]['calendar_id'] == k)
						{
							allow_add = true;
							break;
						}
					}
					if (!allow_add) {
						exclusions[j]['is_deleted'] = 1;
					}
					if (mycache.exclusions[j] == undefined) mycache.exclusions[j] = ConvertObjectVariablesToTheirTypes(exclusions[j]);
				}
			}
		}
		//reminders
		if (setcache.allowreminders != undefined && setcache.allowreminders == 1 && data.reminders != undefined)
		{
			mycache.reminders = data.reminders;
		}

		if (data.appointments != undefined)
		{
			mycache.appointments = data.appointments;
		}

		todayYear = mydate.getFullYear();
		todayMonth = mydate.getMonth();
		cachedMonths.push(new Array(todayMonth, todayYear));

		AddEventsToCache(events);
	
		Calendars = new CCalendars();//render_calendar();
		window.nowDate = new Date();
		FillEvents();
		Grid.LoadView();
		calendarInManager.MarkDays();
		document.onclick = DocumentOnClickHandler;

		calendarTableStart = new CCalendarTableEvent("st");
		calendarTableEnd = new CCalendarTableEvent("en");
		timeSelectorTill = new CTimeSelector("TILL");
		timeSelectorFrom = new CTimeSelector("FROM");
		SharingForm = new CSharingForm();
		EventForm = new CEventForm();
		CalendarForm = new CCalendarForm();
		QuickMenu = new CQuickMenu();
		ChooseForm = new CChooseForm();
	}
	HideInfo();
	isLoaded = true;	
	
	setInterval(RefreshData, 300000);
}

function LoadDataFromServer()
{
	ShowInfo(Lang.InfoLoading);
	
	var req = GetXMLHTTPRequest(), url = processing_url, requestParams;

	requestParams = 'action=get_settings&nocache=' + Math.random();
	if (req) 
	{
		sendARequest(req, url, requestParams, LoadSettingsFromServer_CallBack);
	}
}

function calcScroll (obj)
{
	var div1 = document.createElement('div');
	obj.appendChild(div1);
	var objWidth = obj.offsetWidth;
	var divWidth = div1.offsetWidth;
	obj.removeChild(div1);
	return objWidth - divWidth;
}

