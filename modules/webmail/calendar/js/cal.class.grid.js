/**
 * Initialize
 * ShowTab
 * SetResizeProperty
 * GetGridColumn
 * GetGridLine
 * GetGridLineByTime
 * GetLineTime
 * GetGridParams
 * RenderContainerForEvent
 * DisplayGrid
 * DailyWeeklyScrollHandler
 * WindowScrollHandler
 * FillGrid
 * FillDayGrid
 * FillWeekGrid
 * CreateMonthGrid
 * FillMonthGrid
 * CreateDayWeekGrid
 * LoadView
 * SetWorkAreaOffset
 * RecalcDayWeekHeight
 * SetMainDivHeight
 * ShowScrollArrows
 * Traverse
 * GetEventsPos
 * RecalcScrollArrows
 * ScrollArrowDown
 * ScrollArrowUp
 * CreateShadowElement
 * DeleteShadowElement
 * CalcLineHeight
 */
function CGrid(current_date_obj) {
	this.mainbody				= $id("mainbody");
	this.day_header				= $id('day_header');

//grid
	this.workAreaDay			= $id('work_area_day');
	this.workAreaWeek			= $id('work_area_week');
	this.workAreaMonth			= $id('work_area_month');

	this.week_allday_area		= $id('area_1_week');
	this.week_common_area		= $id('area_2_week');
	this.day_allday_area		= $id('area_1_day');
	this.day_common_area		= $id('area_2_day');
	this.month_area				= $id('area_2_month');
	this.rheaders_week		  	= $id("rheaders_2");

	this.week_allday_grid		= $id('grid_1w');
	this.week_common_grid		= $id('grid_2w');

	this.day_common_grid		= $id('grid_2d');
	this.day_allday_grid		= $id('grid_1d');
	this.month_grid				= $id('grid_2_month');
	this.month_cell_container	= $id('month_cell_container');

//grid elements
	this.week_inner				= new Array(7);
	this.span_week				= new Array(7);
	this.weekends_elem			= [];
	this.monthRow				= new Array(6);

//toolbar
	this.day_browse_title		= $id("time_title_1");
	this.week_browse_title		= $id("time_title_2");
	this.month_browse_title		= $id("time_title_3");
	this.dayTab					= $id('tab_1');
	this.weekTab				= $id('tab_2');
	this.monthTab				= $id('tab_3');
	this.todayBtn			   	= $id('toolbar_today');
	this.backBtn				= $id('toolbar_back');

//week today markers
	this.current_allday			= $id('current_day_1');
	this.current_common			= $id('current_day_2');

//headers
	this.day_headers_cont		= $id('day_headers_day');
	this.week_headers_cont		= $id("day_headers_week");
	this.month_headers_cont		= $id('day_headers_month');

//other screen elements
	this.logo					= $id('logo');
	this.accountslist			= $id('accountslist');
	this.toolbar				= $id('toolbar');
	this.upperIndent			= $id('upper_indent');
	this.lowerIndent			= $id('lower_indent');
	this.calendarManager		= $id('right');
	this.manager_list			= $id("manager_list");
	this.calhead1				= $id("calhead1");
	this.calhead2				= $id("calhead2");
	this.mini_calendar_box		= $id("mini_calendar_box");
	this.checkSharedCalendarsHeader = $id('checkSharedCalendarsHeader');


//scroll arrow layer
	this.arrow_day_up			= $id('arrow_up_day');
	this.arrow_day_down			= $id('arrow_down_day');
	this.arrow_day				= $id('arrow_layer_day');
	this.arrow_week				= [];
	for (var i=0; i<7; i++) {
		this.arrow_week[i] = $id('arrow_layer_week_'+i);
	}

//events position markers
	this.eventsChanged			= true;
	this.masEvents				= [];
	this.maxEventPos			= 0;
	this.minEventPos			= 0;

	this.areaResize = false;
	this.area					= null;
	this.WorkAreaOffsetTop		= 0;
	this.windowScrollTop		= 0;
	this.iScrollTop				= 0;

	this.Initialize(current_date_obj);
}

CGrid.prototype = {
	Initialize: function(current_date_obj) {
		var obj = this;
		this.week_common_area.onmousewheel	= function(e) {
			obj.DailyWeeklyScrollHandler(this, e);
		}
		this.week_common_area.onscroll		= function(e) {
			obj.DailyWeeklyScrollHandler(this, e);
		}
		window.onscroll						= function(e) {
			obj.WindowScrollHandler(e);
		}
		window.onmousewheel					= function(e) {
			obj.WindowScrollHandler(e);
		}
		this.day_common_area.onscroll		= function(e) {
			obj.DailyWeeklyScrollHandler(this, e);
		}
		this.day_common_area.onmousewheel	= function(e) {
			obj.DailyWeeklyScrollHandler(this, e);
		}

		this.DisplayGrid();
		this.CreateDayWeekGrid();
		this.CreateMonthGrid();
		this.FillGrid(current_date_obj);

		this.dayTab.onclick = function() {
			obj.ShowTab(DAY);
		}
		this.weekTab.onclick = function() {
			obj.ShowTab(WEEK);
		}
		this.monthTab.onclick = function() {
			obj.ShowTab(MONTH);
		}
		this.todayBtn.onclick = function() {
			mydate = new Date();
			var dt = String(to8(mydate));
			view = DAY;
			Switch2date(dt);
			obj.LoadView(DAY);
		}
		this.todayBtn.onmouseover = function() {
			this.className = "wm_toolbar_item_over";
		}
		this.todayBtn.onmouseout = function() {
			this.className = "wm_toolbar_item";
		}
		if (calendarType == CALENDAR_MAIN) {
			this.backBtn.onclick = function() {
				HideSettings();
				RenderMonth();
			}
			this.backBtn.onmouseover = function() {
				this.className = "wm_toolbar_item_over";
			}
			this.backBtn.onmouseout = function() {
				this.className = "wm_toolbar_item";
			}
			this.day_allday_grid.onmousedown = function(e) {
				Selection.StartSelection(e, this, true);
				if (typeof(editDiv_pres) != 'undefined' && !editDiv_pres)
				{
					StopEvents(e);
				}
			}
			this.week_allday_grid.onmousedown = function(e) {
				Selection.StartSelection(e, this, true);
				if (typeof(editDiv_pres) != 'undefined' && !editDiv_pres)
				{
					StopEvents(e);
				}
			}
			this.day_allday_grid.onmousemove = function(e) {
				Selection.MoveSelection(e);
			}
			this.week_allday_grid.onmousemove = function(e) {
				Selection.MoveSelection(e);
			}
			this.day_allday_grid.onmouseup = function(e) {
				Selection.EndSelection(e);
			}
			this.week_allday_grid.onmouseup = function(e) {
				Selection.EndSelection(e);
			}
			this.day_common_grid.onmousedown = function(e) {
				Selection.StartSelection(e, this, false);
				if (typeof(editDiv_pres) != 'undefined' && !editDiv_pres)
				{
					StopEvents(e);
				}
			}
			this.week_common_grid.onmousedown = function(e) {
				Selection.StartSelection(e, this, false);
				if (typeof(editDiv_pres) != 'undefined' && !editDiv_pres)
				{
					StopEvents(e);
				}
				
			}
			this.day_common_grid.onmousemove = function(e) {
				if (obj.areaResize) {
					Resize.MoveResize(e, this);
				} else {
					Selection.MoveSelection(e);
				}
			}
			this.week_common_grid.onmousemove = function(e) {
				if (obj.areaResize) {
					Resize.MoveResize(e, this);
				} else {
					Selection.MoveSelection(e);
				}
			}
			this.day_common_grid.onmouseup = function(e) {
				if (obj.areaResize) {
					Resize.EndResize(e);
				} else {
					Selection.EndSelection(e);
				}
			}
			this.week_common_grid.onmouseup = function(e) {
				if (obj.areaResize) {
					Resize.EndResize(e);
				} else {
					Selection.EndSelection(e);
				}
			}
			this.month_grid.onmousedown = function(e) {
				Selection.CreateMonthSelection(e, this);
				if (typeof(editDiv_pres) != 'undefined' && !editDiv_pres)
				{
					StopEvents(e);
				}
			}
		}
	},
	ShowTab : function(view) {
		this.LoadView(view);
		calendarInManager.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
		calendarInManager.MarkDays();
	},
	SetResizeProperty : function(value) {
		this.areaResize = value;
	},
	GetGridColumn : function(e) {
		var i;
		var column = 1;

		if (view == DAY) {
			column = 1;
		} else {
			var area, x, cell_width;
			e = e || window.event;
			x = e.clientX;
			if (view == WEEK){
				area = this.week_allday_grid;
				x -= this.rheaders_week.clientWidth;
			} else {//view = MONTH
				area = this.month_grid;
			}

			cell_width = area.clientWidth/7;
			for (i=1; i<=7; i++) {
				if (x < (cell_width*i)) {
					column = i;
					break;
				}
			}
		}
		return column;
	},
	GetGridLine : function(e) {
		e = e || window.event;
		var lineHeight = (view == MONTH) ? (this.month_grid.clientHeight/6) : LINE_HEIGHT;
		var workAreaCursorY = e.clientY - this.WorkAreaOffsetTop + this.iScrollTop + this.windowScrollTop - 3;
		var lineNumber = Math.floor(workAreaCursorY/lineHeight);
		return (lineNumber + 1);
	},
	GetGridLineByTime : function(date, till_flag) {
		if (till_flag == undefined) till_flag = false;
		var line;
		var hours = date.getHours();
		var minutes = date.getMinutes();
		if (till_flag == true && hours == 0 && minutes == 0) {
			line = 48;
		} else {
			line = hours*2;
			if (minutes <= 30 && minutes > 0) line += 1;
			if (minutes > 30) line += 2;
		}
		return line;
	},
	/*
	 * (int) line = 0-48
	 */
	GetLineTime : function(line) {
		line = line/2;
		var hours = line, minutes = 0, fhours = Math.floor(line);
		if (fhours < hours) {
			minutes = 30;
		}
		return {hours: fhours, minutes: minutes}
	},
	GetGridParams : function(allday) {
		var left = 0, top = 0, parent;

		top += (this.logo != null)? this.logo.offsetHeight : 0;
		top += (this.accountslist != null)? this.accountslist.offsetHeight: 0;
		top += (this.toolbar != null)? this.toolbar.offsetHeight: 0;
		top += (this.upperIndent != null)? this.upperIndent.offsetHeight: 0;
		if (view == DAY) {
			top += (this.day_headers_cont!=null)? this.day_headers_cont.offsetHeight: 0;
		} else if (view == WEEK) {
			top += (this.week_headers_cont!=null)? this.week_headers_cont.offsetHeight: 0;
		} else { //view == MONTH
			top += (this.month_headers_cont!=null)? this.month_headers_cont.offsetHeight: 0;
		}

		if (view != MONTH) {
			left+=41;
			if (allday == 0) {
				var vnod;
				if (view == WEEK) {
					vnod = this.week_allday_area;
					parent = this.week_common_grid;
				} else {
					vnod = this.day_allday_area;
					parent = this.day_common_grid;
				}
				top += ParseToNumber(vnod.offsetHeight) + 2;
			} else {
				parent = (view == WEEK) ? this.week_allday_grid: this.day_allday_grid;
			}
		} else {
			parent = this.month_grid;
		}

		return {
			top:ParseToNumber(top),
			left:ParseToNumber(left),
			width:parent.offsetWidth,
			height:parent.offsetHeight,
			dom:parent
		}
	},
	RenderContainerForEvent : function(id, color)
	{
		var div1, div2, div3, div4, div5;
		div1 = document.createElement('div');
		div1.className = 'eventcontainer_'+color;
		div1.id = 'container_'+id+'_1';
		this.day_allday_grid.appendChild(div1);

		div2 = document.createElement('div');
		div2.className = 'eventcontainer_'+color;
		div2.id = 'container_'+id+'_2';
		this.day_common_grid.appendChild(div2);

		div3 = document.createElement('div');
		div3.className = 'eventcontainer_'+color;
		div3.id = 'container_'+id+'_3';
		this.week_allday_grid.appendChild(div3);

		div4 = document.createElement('div');
		div4.className = 'eventcontainer_'+color;
		div4.id = 'container_'+id+'_4';
		this.week_common_grid.appendChild(div4);

		div5 = document.createElement('div');
		div5.className = 'eventcontainer_'+color;
		div5.id = 'container_'+id+'_5';
		this.month_grid.appendChild(div5);
	},
	DisplayGrid : function() {
		this.calendarManager.style.visibility = "visible";
		switch (view)
		{
			case DAY:
				this.workAreaDay.style.display = "block";
			break;
			case WEEK:
				this.workAreaWeek.style.display = "block";
			break;
			case MONTH:
				this.workAreaMonth.style.display = "block";
			break;
			default:
				this.workAreaWeek.style.display = "block";
			break;
		}
	},
	DailyWeeklyScrollHandler: function(object, e) {
		e = e || window.event;
		if (OperaDetect()) {
			var ev, version_browser;
			ev = (e.wheelDelta ) ? e.wheelDelta  : 0;
			this.iScrollTop = object.scrollTop + ev ;

			/* Mousewheel UP */
			if (object.scrollTop < 120) {
				this.iScrollTop = 0;
			}
			/* Mousewheel DOWN*/
			if (ev > 0 && object.scrollTop == '0') {
				this.iScrollTop = ev;
			}
			version_browser = window.navigator.appVersion;
			if (version_browser.substr(0, version_browser.lastIndexOf(" (")) == '9.00') {
				object.scrollTop = this.iScrollTop;
			}
		}
		else {
			this.iScrollTop = object.scrollTop;
		}
		this.ShowScrollArrows();
	},
	WindowScrollHandler : function(e) {
		e = e || window.event;
		if (OperaDetect()) {
			this.windowScrollTop = window.pageYOffset;
		}
		else {
			this.windowScrollTop = window.scrollY;
		}
		this.ShowScrollArrows();
	},
	FillGrid : function (dateObj) {
		this.FillDayGrid(dateObj);
		this.FillWeekGrid();
		this.FillMonthGrid(dateObj);
	},
	FillDayGrid : function (dateObj) {
		var i, day, year, month, date, dayName = "", text, strong_day;
		day = dateObj.getDay();
		year = dateObj.getFullYear();
		month = dateObj.getMonth();
		date = dateObj.getDate();

		for (i=0; i<weekDaysNamesFull.length; i++) {
			if (weekDaysNamesFull[i].Day == day) {
				dayName = weekDaysNamesFull[i].Value;
				break;
			}
		}
		PasteTextNode(this.day_header, dayName);
		CleanNode(this.day_browse_title);
		text = document.createTextNode(date + " " + GetMonthName(month) + " " + year);
		strong_day = document.createElement('strong');
		strong_day.appendChild(text);
		this.day_browse_title.appendChild(strong_day);
	},
	FillWeekGrid : function () {
		var i, j, d1, d2, tit1, strong_week, this_date, this_date_8format, this_date_day, the_day, left = 0;
		var day, date, month, m, topsection_height, viewDate, weekend;

		CleanNode(this.week_browse_title);
		d1 = from8(showLimits.weekFrom);
		d2 = from8(showLimits.weekTill);

		if (d1.getMonth() == d2.getMonth() && d1.getYear() == d2.getYear()){
			tit1 = d1.getDate() + " - " + d2.getDate() + " " + GetMonthName(d1.getMonth()) + " " + d1.getFullYear();
		} else {
			tit1 = d1.getDate() + " " + GetMonthName(d1.getMonth()) + " " + d1.getFullYear() + " - " + d2.getDate() + " " + GetMonthName(d2.getMonth()) + " " + d2.getFullYear();
		}
		strong_week = document.createElement('strong');
		strong_week.appendChild(document.createTextNode(tit1));
		this.week_browse_title.appendChild(strong_week);

//******
		this_date = new Date();
		this_date_8format = to8(this_date);
		this_date_day = this_date.getDay();

		the_day = new Date(d1);
		for (i=0; i < this.span_week.length; i++){
			weekdayz[i] = to8(the_day);
			day = the_day.getDay();
			date = the_day.getDate();
			month = the_day.getMonth();

			this.week_inner[i].className = (this_date_8format == weekdayz[i]) ? "day_headers_inner_today" : "day_headers_inner";
			m = (month < 9) ? ("0"+ (month + 1)) : (month + 1);

			if (typeof(weekendDays) != 'undefined')
			{
				for (j=0; j<weekendDays.length; j++) {
					if (weekendDays[j] == day) {
						weekend = this.weekends_elem[j];
						weekend.allday.style.left = left+"%";
						weekend.common.style.left = left+"%";
						topsection_height = weekend.allday.parentNode.style.height;
						topsection_height = (topsection_height == undefined || !isNaN(topsection_height)) ? "100%" : topsection_height;
						weekend.allday.style.hight = topsection_height;
					}
				}
			}

			if(setcache.dateformat == 1) {
				viewDate = m + "/" + date;
			}
			else if (setcache.dateformat == 2) {
				viewDate = date + "/" +m;
			}
			else if (setcache.dateformat == 3) {
				viewDate = m + "-" + date;
			}
			else if (setcache.dateformat == 4) {
				viewDate = GetMonthNameByNumber(m) + " " + date;
			}
			else //setcache.dateformat == 5
			{
				viewDate = date + " " + GetMonthNameByNumber(m);
			}

			PasteTextNode(this.span_week[i], (GetDayShortName(day) + " " + viewDate));

			if (this_date_day == day) {
				if (this_date_8format == weekdayz[i]) {
					this.current_allday.className = "current_day";
					this.current_common.className = "current_day";
				}
				else {
					this.current_allday.className = "current_day_nocolor";
					this.current_common.className = "current_day_nocolor";
				}
				this.current_allday.style.left = left+"%";
				this.current_common.style.left = left+"%";
			}

			left += CELL_WIDTH;
			the_day.setDate(the_day.getDate() + 1);
		}
	},
	CreateMonthGrid : function() {
		//create month cells
		var i, d, pos_left = 0, top = 0, header, cell, span_arr, txt;

		for (i=0; i<this.monthRow.length; i++) {
			this.monthRow[i] = [];
			for (d=0; d<7; d++) {
				this.monthRow[i][d] = {}
				header = document.createElement("div");
				this.monthRow[i][d].header = header;
				cell = document.createElement("div");
				this.monthRow[i][d].cell = cell
				cell.appendChild(header);
				cell.style.top = top+"%";
				cell.style.left = pos_left + "%";
				this.month_cell_container.appendChild(cell);
				pos_left += CELL_WIDTH;
			}
			top += CELL_HEIGHT;
			pos_left = 0;
		}

		//create month headers
		span_arr = this.month_headers_cont.getElementsByTagName("span");
		for (i=0; i<weekDaysNamesFull.length; i++) {
			txt = document.createTextNode(weekDaysNamesFull[i].Value);
			span_arr[i].appendChild(txt);
		}
	},
	FillMonthGrid : function(dateObj) {
		var i, j, w, todayDateFull, this_year, this_month, current_day, cell_class = "", year, month, date, day, text1, strong_month, row, today;

		todayDateFull = new Date();

		this_year = dateObj.getFullYear();
		this_month = dateObj.getMonth();
		current_day = new Date(monthLimits[this_year][this_month].bigStart);

		for (i=0; i<this.monthRow.length; i++) {
			for (j=0; j<7; j++) {
				year = current_day.getFullYear();
				month = current_day.getMonth();
				date = current_day.getDate();
				day = current_day.getDay();

				row = this.monthRow[i][j];
				row.header.className = (month == this_month && year == this_year) ? "header" : "header header_inactiv";
				PasteTextNode(row.header, date);

				row.cell.id = "cel_"+ to8(current_day);
				cell_class = "cell";
				for (w=0; w<weekendDays.length; w++) {
					if (weekendDays[w] == day && setcache.showweekends == 1) {
						cell_class = "cell_weekend"
						break;
					}
				}

				row.cell.className = cell_class;
				current_day.setDate(current_day.getDate() + 1);
			}
		}
		today = $id("cel_" + to8(todayDateFull));
		if (today != undefined) today.className += " today";

		//fill browse title
		CleanNode(this.month_browse_title);

		text1 = document.createTextNode(GetMonthName(this_month) + " " + this_year);
		strong_month = document.createElement('strong');
		strong_month.appendChild(text1);
		this.month_browse_title.appendChild(strong_month);
	},
	CreateDayWeekGrid : function()
	{
		//create rheaders
		var n, j, t, i, w, rheaders, parents, timeFormat, top, rdiv1, rdiv11, rdiv2, left = 0;
		var div1, div12, div2, div22, div3, allday, common, rounded_div;

		rheaders = new Array($id("rheaders_1"), $id("rheaders_2"));
		parents = new Array(this.day_common_grid, this.week_common_grid);
		timeFormat = (setcache.timeformat == 1) ? timeFormat1 : timeFormat2;
		for(n = 0; n < rheaders.length; n++){
			top = 0;
			for (j=0,t=0; j<(timeFormat.length-1); j=j+2, t++) {
				rdiv1 = document.createElement("div");
				top = t*9;
				if (setcache.showworkday == 1 && (setcache.workdaystarts > t || t >= setcache.workdayends)) {
					rdiv11 = document.createElement("div");
					rdiv11.className = "notworkday";
					rdiv11.style.top = top +"ex";
					parents[n].appendChild(rdiv11);
					rdiv1.className = "rhead_notworkday";
				} else {
					rdiv1.className = "rhead";
				}
				rdiv2 = document.createElement("div");
				rdiv2.appendChild(document.createTextNode(timeFormat[j].Value));
				if (j == 0) {
					rdiv1.style.borderTop = "1px solid #fff";
				}
				rdiv1.style.top = top +"ex";
				rdiv1.appendChild(rdiv2);
				rheaders[n].appendChild(rdiv1);
			}
		}

		//create week headers elements
		div1 = document.createElement("div");
		div1.className = "day_headers_outer";
		div1.style.width = "40px";
		div1.style.left = "-40px";

		div12 = document.createElement("div");
		div12.className = "day_headers_inner";
		div12.style.height = "24px";
		div1.appendChild(div12);
		this.week_headers_cont.appendChild(div1);

		div2 = document.createElement("div");
		div2.className = "day_headers_outer";
		div2.style.width = "16px";
		div2.style.left = "100%";
		div22 = document.createElement("div");
		div22.className = "day_headers_inner";
		div12.style.height = "24px";
		div2.appendChild(div22);
		this.week_headers_cont.appendChild(div2);

		for (i=0; i<7; i++){
			div3 = document.createElement("div");
			div3.className = "day_headers_outer";
			div3.style.left = left+"%";
			this.week_inner[i] = document.createElement("div");
			rounded_div = document.createElement("div");
			this.week_inner[i].appendChild(rounded_div);
			this.span_week[i] = document.createElement("span");
			this.week_inner[i].appendChild(this.span_week[i]);
			div3.appendChild(this.week_inner[i]);
			this.week_headers_cont.appendChild(div3);
			left += CELL_WIDTH;
		}
		if (setcache.showweekends == 1) {
			if (typeof(weekendDays) != 'undefined')
			{
				for (w=0; w < weekendDays.length; w++) {
					this.weekends_elem[w] = {}
					this.weekends_elem[w].allday = document.createElement("div");
					allday = this.weekends_elem[w].allday;
					this.weekends_elem[w].common = document.createElement("div");
					common = this.weekends_elem[w].common
					allday.className = "weekend_day";
					this.week_allday_grid.appendChild(allday);
					common.className = "weekend_day";
					this.week_common_grid.appendChild(common);
				}
			}
		}
	},
	LoadView : function(viewpar) {
		if (viewpar == undefined) {
			view = (view == undefined) ? WEEK : view;
		}
		else {
			view = viewpar;
		}

		var obj;
		switch (view)
		{
			case DAY:
				if (calendarType == CALENDAR_MAIN) {
					if (parent && parent.WebMail) parent.WebMail.setTitle(Lang.TitleDay);
					else document.title = calendarTitle + ' - ' + Lang.Calendar + ' - ' + Lang.TitleDay;
				}
				this.area = this.day_common_grid;

				this.dayTab.className = "time_tabs_outer_activ";
				this.weekTab.className = "time_tabs_outer";
				this.monthTab.className = "time_tabs_outer";

				this.workAreaDay.style.display = "block";
				this.workAreaWeek.style.display = "none";
				this.workAreaMonth.style.display = "none";

				this.day_browse_title.style.display = "";
				this.week_browse_title.style.display = "none";
				this.month_browse_title.style.display = "none";

				obj = this.day_common_area;
			break;
			case WEEK:
				if (calendarType == CALENDAR_MAIN) {
					if (parent && parent.WebMail) parent.WebMail.setTitle(Lang.TitleWeek);
					else document.title = calendarTitle + ' - ' + Lang.Calendar + ' - ' + Lang.TitleWeek;
				}
				this.area = this.week_common_grid;

				this.dayTab.className = "time_tabs_outer";
				this.weekTab.className = "time_tabs_outer_activ";
				this.monthTab.className = "time_tabs_outer";

				this.workAreaDay.style.display = "none";
				this.workAreaWeek.style.display = "block";
				this.workAreaMonth.style.display = "none";

				this.day_browse_title.style.display = "none";
				this.week_browse_title.style.display = "";
				this.month_browse_title.style.display = "none";

				obj = this.week_common_area;
			break;
			case MONTH:
				if (calendarType == CALENDAR_MAIN) {
					if (parent && parent.WebMail) parent.WebMail.setTitle(Lang.TitleMonth);
					else document.title = calendarTitle + ' - ' + Lang.Calendar + ' - ' + Lang.TitleMonth;
				}
				this.area = this.month_grid;

				this.dayTab.className = "time_tabs_outer";
				this.weekTab.className = "time_tabs_outer";
				this.monthTab.className = "time_tabs_outer_activ";

				this.workAreaDay.style.display = "none";
				this.workAreaWeek.style.display = "none";
				this.workAreaMonth.style.display = "block";

				this.day_browse_title.style.display = "none";
				this.week_browse_title.style.display = "none";
				this.month_browse_title.style.display = "";

				obj = null;
			break;
		}
		this.SetMainDivHeight();
		if (obj != null) {
			var scroll_top, events, val;
			events = this.Traverse();
			val = this.GetEventsPos(events);
			if (val.minEv > (setcache.workdaystarts*54) || val.minEv == -1){
				scroll_top = setcache.workdaystarts*54;
			}else{
				scroll_top = val.minEv;
			}
			obj.scrollTop = scroll_top;
			this.iScrollTop = scroll_top + this.windowScrollTop;
 		}
		else {
			this.iScrollTop = 0;
		}

		if (calendarType == CALENDAR_MAIN) {
			this.SetWorkAreaOffset();
		}

		this.RecalcScrollArrows();
		RenderMonth();
		//calendarInManager.RefreshCalendarSelector(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
	},
	SetWorkAreaOffset : function() {
		if (this.area != null) {
			this.WorkAreaOffsetTop = FindPosY(this.area);
		}
	},
	RecalcDayWeekHeight : function(allday_rows, view) {
		var i, mainbody_height, half_mainbody, real_allday_height, new_allday_height, common_area_height,
			allday_container, allday_area, common_area, dayheaders, minhigh = 53;// 2*27=54 ???

		mainbody_height = this.mainbody.offsetHeight - (MSIEDetect() ? 0 : 1);
		half_mainbody = (mainbody_height > 0) ? Math.floor(mainbody_height/2) - 10 : 0;
		real_allday_height = 8 + allday_rows*LINE_HEIGHT; //8-number of free pixels in allday section independently of allday events number 27-standart height of allday event

		if (real_allday_height < minhigh) real_allday_height = minhigh;
		new_allday_height = Math.min(real_allday_height, half_mainbody);
		if (view == DAY) {
			allday_container = this.day_allday_grid;
			allday_area = this.day_allday_area;
			common_area = this.day_common_area;
			dayheaders = this.day_headers_cont.offsetHeight;
		} else {
			allday_container = this.week_allday_grid;
			allday_area = this.week_allday_area;
			common_area = this.week_common_area;
			dayheaders = this.week_headers_cont.offsetHeight;
		}
		allday_container.style.height = real_allday_height+"px";
		this.current_allday.style.height = real_allday_height+"px";
		if (mainbody_height > 0) {
			allday_area.style.height = new_allday_height + "px";
			common_area_height = mainbody_height - dayheaders - 3 - new_allday_height;
			if (common_area_height > 0) {
				common_area.style.height = common_area_height + "px";
			}
		}
		for (i=0; i<this.weekends_elem.length; i++) {
			this.weekends_elem[i].allday.style.height = real_allday_height+"px"
		}
	},
	SetMainDivHeight : function() {
		var dayBody1, main_div, dayHeaders, externalHeight = 0, dhOffsetHeight, db1offsetHeight;

		switch (view) {
			case 0:
				dayBody1	= this.day_allday_area;
				main_div	= this.day_common_area;
				dayHeaders	= this.day_headers_cont;
			break;
			case 1:
				dayBody1	= this.week_allday_area;
				main_div	= this.week_common_area;
				dayHeaders	= this.week_headers_cont;
			break;
			case 2:
				main_div	= this.month_area;
				dayHeaders	= this.month_headers_cont;
			break;
			default:
				dayBody1	= this.week_allday_area;
				main_div	= this.week_common_area;
				dayHeaders	= this.week_headers_cont;
		}

		externalHeight += (this.logo != null)?(this.logo.offsetHeight): 0;
		externalHeight += (this.accountslist != null)?(this.accountslist.offsetHeight): 0;
		externalHeight += (this.toolbar != null)?(this.toolbar.offsetHeight): 0;
		externalHeight += (this.upperIndent != null)?(this.upperIndent.offsetHeight): 0;
		dhOffsetHeight = (dayHeaders != null)?(dayHeaders.offsetHeight):0;
		externalHeight += dhOffsetHeight;

		if(view == MONTH) {
			db1offsetHeight = 0;
		}
		else {
			db1offsetHeight = (dayBody1 != null)?(dayBody1.offsetHeight):0;
			externalHeight += db1offsetHeight;
		}

		externalHeight += (this.lowerIndent != null)?(this.lowerIndent.offsetHeight):0;

		if (main_div != null) {
			var aHtmls, copyMargin = 0, tdBorder = 1, height, cmHeight = 360, wto, mincal, mold, check_link_height, calhead2, mnew;

			aHtmls = document.getElementsByTagName("HTML");
			Html = aHtmls[0];
			externalHeight += copyMargin + tdBorder;
			height = GetHeight() - externalHeight;
			if (height < 200) height = 200;
			main_div.style.height = height + 'px';

			if ((height + dhOffsetHeight + db1offsetHeight + tdBorder) < cmHeight) {
				height = cmHeight - dhOffsetHeight - db1offsetHeight;//main div height - div with time or weeks(in month)
				main_div.style.height = height+"px";

				if(!FireFoxDetect()) {
					Html.style.overflow = 'auto';
				}
				else {
					document.body.scroll = "yes";
					document.body.style.overflow = "auto";
				}
			}
			else {
				if(!FireFoxDetect()) {
					Html.style.overflow = 'hidden';
				}
				else {
					document.body.scroll = "no";
					document.body.style.overflow = "hidden";
				}
			}

			wto = height+db1offsetHeight+dhOffsetHeight; // area_2 + area_1 + days_headers
			mincal = this.mini_calendar_box.offsetHeight;// mincal=189;
			mold = parseInt(this.manager_list.style.height);
			if (calendarType == CALENDAR_MAIN) {
				check_link_height = this.checkSharedCalendarsHeader.offsetHeight;
				calhead2 = this.calhead2.offsetHeight;
			}
			else {
				check_link_height = 0;
				calhead2 = 0;
			}
			mnew = (wto - this.calhead1.offsetHeight - calhead2 - mincal - 3 - check_link_height); //if (mnew<65) mnew=65; mnew+="px";

			if (mold!=mnew) {
				this.manager_list.style.height=mnew+"px";
				this.calendarManager.style.height=(wto-5)+"px";
			}
			if (calendarType == CALENDAR_MAIN) cmScroll_resize();
		}
	},
	ShowScrollArrows : function() {
		var i, arrow_layer = [], area, events, val, top, bottom;
		if (view == DAY)
		{
			area = this.day_common_area;
			events = this.Traverse();
			val = this.GetEventsPos(events);

			this.maxEventPos = val.maxEv;
			this.minEventPos = val.minEv;
			this.eventsChanged = false;

			this.arrow_day_up.onclick = this.ScrollArrowUp(this, this.minEventPos);
			this.arrow_day_down.onclick = this.ScrollArrowDown(this, this.maxEventPos);
			top = area.scrollTop;
			bottom = top + area.offsetHeight;
			this.arrow_day_down.style.display = bottom < this.maxEventPos ? 'block' : 'none';
			this.arrow_day_up.style.display = (top > this.minEventPos && this.minEventPos!=(-1)) ? 'block' : 'none';
			arrow_layer[0] = this.arrow_day;
		}
		if(view == WEEK)
		{
			var e, left = 0, double_left = CELL_WIDTH, resCnt, arrowUp, arrowDown;

			area = this.week_common_area;
			if (this.eventsChanged)
			{
				events = this.Traverse();
				var even_left = [];

				for (i = 0; i<events.length; i++) {
					even_left[i] = parseFloat(events[i].style.left);
				}
				for(e = 0; e < 7; e++)
				{
					resCnt = 0;
					this.masEvents[e] = [];
					for (i = 0; i<even_left.length; i++) {
						if (even_left[i] >= left && even_left[i] < double_left) this.masEvents[e][resCnt++] = events[i];
					}
					left += CELL_WIDTH;
					double_left += CELL_WIDTH;
				}
				this.eventsChanged = false;
			}

			for (e = 0; e < 7; e++)
			{
				val = this.GetEventsPos(this.masEvents[e]);
				this.maxEventPos = val.maxEv;
				this.minEventPos = val.minEv;
				top = area.scrollTop;
				bottom = top + area.offsetHeight;
				var weekChilds = this.arrow_week[e].childNodes;
				if (MSIEDetect() && getMSIEVersion() < 9) {
					arrowUp = weekChilds[0];
					arrowDown = weekChilds[1];
				}
				else {
					arrowUp = weekChilds[1];
					arrowDown = weekChilds[3];
				}
				arrowUp.onclick = this.ScrollArrowUp(this, this.minEventPos);
				arrowDown.onclick = this.ScrollArrowDown(this, this.maxEventPos);

				arrowDown.style.display = bottom < this.maxEventPos ? 'block' : 'none';
				if (top > this.minEventPos && this.minEventPos != (-1))
					arrowUp.style.display = 'block';
				else
					arrowUp.style.display = 'none';
			}
			arrow_layer = this.arrow_week;
		}
		if (arrow_layer.length != 0) {
			var resCntArrow;
			for (i = 0; resCntArrow = arrow_layer[i]; i++){
				resCntArrow.style.height = area.clientHeight + 'px';
			}
		}
	},
	Traverse : function() {
		var res = [], resCnt = 0, container, par, calendar_id, it;
		par = (view == DAY) ? 2 : 4;

		for (calendar_id in mycache.calendars) {
			container = $id('container_'+calendar_id+'_'+par);
			if (container != undefined) {
				for (it = container.firstChild; it; it = it.nextSibling) {
					res[resCnt++] = it;
				}
			}
		}
		return res;
	},
	GetEventsPos : function(events) {
		var i, minEventPos2 = -1, maxEventPos2 = 0, event, top, bottom;
		for (i = 0; event = events[i]; i++) {
			var parrent = event.parentNode;
			if (parrent && parrent.className.indexOf('hide') == -1)
			{
				top = event.offsetTop;
				bottom = top + event.offsetHeight;
				if (maxEventPos2 < bottom) {
					maxEventPos2 = bottom;
				}
				if (minEventPos2 > top || minEventPos2 == -1) {
					minEventPos2 = top;
				}
			}
		}
		return {minEv:minEventPos2, maxEv:maxEventPos2};
	},
	RecalcScrollArrows : function() {
		this.eventsChanged = true;
		this.ShowScrollArrows();
	},
	ScrollArrowDown : function(obj, maxEvPos) {
		return function() {
			var arrow = (view == DAY)? obj.day_common_area : obj.week_common_area;
			arrow.scrollTop = maxEvPos + 24 - arrow.offsetHeight;
		}
	},
	ScrollArrowUp : function(obj, minEvPos) {
		return function() {
			var arrow = (view == DAY)? obj.day_common_area : obj.week_common_area;
			arrow.scrollTop = minEvPos - 24;
		}
	},
	CreateShadowElement : function() {
		DeleteNode("standEventShadow");
		var event = invoked_event.obj;
		var calendar = mycache.calendars[invoked_event.calendar_id];
		var elem = document.createElement("div");
		elem.id = "standEventShadow";

		var div1, div2, div3, div4, div5, div8, div9;
		div1 = document.createElement("div");
		div1.className = "event";

		div2 = document.createElement("div");
		div2.className = "a";

		div3 = document.createElement("div");
		div3.className = "b";

		div1.appendChild(div2);
		div1.appendChild(div3);

		div4 = document.createElement("div");
		div4.className = "event_middle";

		div5 = document.createElement("div");
		div5.className = "event_text";

		div4.appendChild(div5);
		div1.appendChild(div4);

		div8 = document.createElement("div");
		div8.className = "b";
		div1.appendChild(div8);

		div9 = document.createElement("div");
		div9.className = "a";
		div1.appendChild(div9);

		elem.appendChild(div1);

		elem.className = "stand_event_shadow eventcontainer_"+calendar.color;
		elem.style.top = event.style.top;
		elem.style.left = event.style.left;
		div1.style.width = event.style.width;
		div4.style.height = (event.clientHeight - 4) + "px";
		var parent;
		if (view == MONTH) {
			parent = this.month_grid;
		}
		else if (view == WEEK) {
			parent = (invoked_event.event_allday == 1) ? this.week_allday_grid : this.week_common_grid;
		}
		else {
			parent = (invoked_event.event_allday == 1) ? this.day_allday_grid : this.day_common_grid;
		}
		parent.appendChild(elem);
	},
	DeleteShadowElement : function() {
		DeleteNode("standEventShadow");
	},
	CalcLineHeight : function (obj)
	{
		var row1, row2;
		if (obj.section !== obj.ALLDAY)
		{
			if (view == WEEK)
			{
				row1 = $id('wr2');
				row2 = $id('wr4');
			}
			else
			{
				row1 = $id('dr2');
				row2 = $id('dr4');
			}
			LINE_HEIGHT = (row2.offsetTop - row1.offsetTop)/2;
		}

	}
}