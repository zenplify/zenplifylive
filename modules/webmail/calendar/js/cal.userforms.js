/**
 * CTimeSelector
 * CCalendarTableEvent
 * CSharingForm
 * CCalendarForm
 * CQuickMenu
 */

function CTimeSelector (type)
{
	this.startOptionNumber  = 0;
	this.pattern            = null;
	this.timeOptions        = null;
	this.time_elem          = [];
	if (type == "FROM")
	{
		this.type		= "FROM";
		this.container	= $id("EventTimeFrom_dropdown");
		this.input		= $id("EventTimeFrom");
	} 
    else
	{
		this.type		= "TILL";
		this.container	= $id("EventTimeTill_dropdown");
		this.input		= $id("EventTimeTill");
	}
	this.inputTimeFrom	= $id("EventTimeFrom");
	this.inputTimeTill	= $id("EventTimeTill");
	this.inputDateFrom	= $id("EventDateFrom");
	this.inputDateTill	= $id("EventDateTill");
	this.selectBoxList	= $id("edit_select_box_list");

	this.Create();
}
CTimeSelector.prototype = {
	Create : function()
	{
		this.container.style.position = "absolute";
		this.container.style.zIndex = "10";
		this.container.className = "wm_hide";
		if (setcache.timeformat == 1)
		{
			this.pattern = /^1?[0-9](:[03]0)?(\s+(AM|PM))$/;
			this.patternCommonTime = /^(1?[0-9])(:[0-5][0-9])?(\s+)(AM|PM)$/;
			this.timeOptions = timeFormat1;
			this.container.style.textAlign = "left";
			this.container.style.paddingLeft = "4px";
		}
		else
		{
			this.pattern = /^([0-2])?[0-9](:[03]0)$/;
			this.patternCommonTime = /^([0-2])?([0-9])(:[0-5][0-9])$/;
			this.timeOptions = timeFormat2;
		}
		var i, div, obj = this;
		for (i in this.timeOptions)
		{
			div = document.createElement("div");
			div.className = "ts_text";
			PasteTextNode(div, this.timeOptions[i].Value);
			div.onmouseover = div.className = "ts_text_hover";
			div.onmouseout = div.className = "ts_text";
			div.onclick = this.CreateTimeFunc.call(this, i);
			this.time_elem[i] = {
				Elem: div,
				Id:i
			};
			this.container.appendChild(div);
		}

		if (this.type == "TILL")
		{
			this.inputTimeTill.onfocus = function(event)
			{
				if (calendarTableStart != undefined) calendarTableStart.Hide();
				if (calendarTableEnd != undefined) calendarTableEnd.Hide();
				obj.Hide();
				obj.Show(this, event);
			}
		}
		if (this.type == "FROM") {
			this.inputTimeFrom.onfocus = function(evt)
			{
				evt = (evt)?evt:window.event;
				var tgt = window.event ? window.event.srcElement : evt.target;
				if (tgt.id != "calen_sal" && tgt.id != "calendar_arrow" 
					&& obj.selectBoxList.style.display != "none") {
					obj.selectBoxList.style.display = "none";
				}
				if (calendarTableStart != undefined) calendarTableStart.Hide();
				if (calendarTableEnd != undefined) calendarTableEnd.Hide();
				obj.Hide();
				obj.Show(this, evt);
			}
		}
	},
	Fill: function(dateTime)
	{
		var date = this.GetFormattedDateAndTime(dateTime);

		this.input.value = date.time;

		for (var i in this.timeOptions)
		{
			var div = this.time_elem[i].Elem;
			if (date.time == this.timeOptions[i].Value)
			{
				div.style.fontWeight = "bold";
				this.startOptionNumber = i;
			}
			else
			{
				div.style.fontWeight = "normal";
			}
		}
		
		if (this.type == "TILL") {
			this.inputDateTill.value = date.date;
		}
		if (this.type == "FROM") {
			this.inputDateFrom.value = date.date;
		}
	},
	/**
     * datetime - date object
     * return onject = {date:formatted fate, time: formatted time }
     */
	GetFormattedDateAndTime: function(datetime)
	{
		var time, date, hours, minutes;
		hours = datetime.getHours();
		minutes = datetime.getMinutes();

		if (setcache.timeformat == 1) {
			time = (hours == 0 ? '12' : (hours > 12 ? hours - 12 : hours) ) + ':' + (minutes < 10 ? '0' : '') + minutes + (hours > 11 ? ' PM' : ' AM');
		} else {
			time = (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
		}
		
		date = ConvertFromDateToStr(datetime);

		return {
			date:date,
			time:time
		};
	},
	CreateTimeFunc : function(i)
	{
		var obj = this;
		return function ()
		{
			obj.Hide();
			var index = obj.time_elem[i].Id;
			obj.onpicktime(index);
		}
	},
	Show: function (input, event)
	{
		var
			oInpOffset = $(input).offset(),
			iInpHeight = $(input).height(),
			fInpBottom = oInpOffset.top + iInpHeight,
			iWinHeight = $(window).height(),
			iContHeight = 0,
			iContMaxHeight = 0
		;
		
		StopEvents(event);
		this.container.className = "time_selector";
		this.container.style.height = 'auto';
		
		iContHeight = this.container.offsetHeight;
		iContMaxHeight = Math.round(iWinHeight - fInpBottom - 20);
		
		if (iContHeight > iContMaxHeight) {
			 this.container.style.height = iContMaxHeight + 'px';
		}
		
		this.container.scrollTop = this.startOptionNumber * 16; //16-height of 1 option;
	},
	Hide: function ()
	{
		this.container.className = "wm_hide";
	},
	ScrollValue : function(obj)
	{
		var i, v = 0; //vertical offset
		var thisTime = Trim(obj.value), len;

		len = (this.type == "FROM")?(this.timeOptions.length-1):this.timeOptions.length;
		for (i=this.startOptionNumber; i<len; i++)
		{
			if (thisTime == this.timeOptions[i].Value) break; 
			v++;
		}
		return (v*16);
	},
	onpicktime : function(Id)
	{
		var dateFrom, dateTill, dateFromValue, dateTillValue, fromTimeId, fromTimeValue, 
			tillTimeId, tillTimeValue, addDay = 0, oFromTime, oTillTime;
		
		dateFrom = ConvertFromStrToDate(Trim(this.inputDateFrom.value));
		dateTill = ConvertFromStrToDate(Trim(this.inputDateTill.value));

		dateFromValue = dateFrom.getTime();
		dateTillValue = dateTill.getTime();

		if (this.type == "TILL")
		{
			tillTimeId = parseInt(Id);
			tillTimeValue = this.fixTimeFormat(this.timeOptions[tillTimeId].Value);
			fromTimeValue = this.fixTimeFormat(Trim(this.inputTimeFrom.value));

			if (this.patternCommonTime.test(fromTimeValue)) {
				oFromTime = new Date('January 1, 1980 ' + fromTimeValue);
				oTillTime = new Date('January 1, 1980 ' + tillTimeValue);
				
				if (oFromTime >= oTillTime && dateTillValue == dateFromValue) {
					fromTimeId = (tillTimeId != 0) ? tillTimeId - 1 : 47;
					addDay = (tillTimeId != 0) ? 0 : -1;
					fromTimeValue = this.fixTimeFormat(this.timeOptions[fromTimeId].Value);
				}
			} else {
				fromTimeId = (tillTimeId != 0) ? tillTimeId - 1 : 47;
				addDay = (tillTimeId != 0) ? 0 : -1;
				fromTimeValue = this.fixTimeFormat(this.timeOptions[fromTimeId].Value);
			}
		}
		else if (this.type == "FROM")
		{
			fromTimeId = parseInt(Id);
			fromTimeValue = this.fixTimeFormat(this.timeOptions[fromTimeId].Value);
			tillTimeValue = this.fixTimeFormat(Trim(this.inputTimeTill.value));
			
			// fromTimeValue = this.fixTimeFormat(tillTimeValue);
			
			if (this.patternCommonTime.test(tillTimeValue)) {
				oFromTime = new Date('January 1, 1980 ' + fromTimeValue);
				oTillTime = new Date('January 1, 1980 ' + tillTimeValue);
				
				if (oFromTime >= oTillTime && dateTillValue == dateFromValue) {
					tillTimeId = (fromTimeId != 47) ? fromTimeId + 1 : 0;
					addDay = (fromTimeId != 47) ? 0 : 1;
					tillTimeValue = this.fixTimeFormat(this.timeOptions[tillTimeId].Value);
				}
			} else {
				tillTimeId = (fromTimeId != 47) ? fromTimeId + 1 : 0;
				addDay = (fromTimeId != 47) ? 0 : 1;
				tillTimeValue = this.fixTimeFormat(this.timeOptions[tillTimeId].Value);
			}
		}

		fromTimeValue = new Date('January 1, 1980 ' + fromTimeValue);
		// fromTimeValue = fromTimeValue.split(':');
		dateFrom.setHours(fromTimeValue.getHours(), fromTimeValue.getMinutes());

		tillTimeValue = new Date('January 1, 1980 ' + tillTimeValue);
		// tillTimeValue = tillTimeValue.split(':');
		dateTill.setHours(tillTimeValue.getHours(), tillTimeValue.getMinutes());
		if (addDay > 0) {
			dateTill.setDate(dateTill.getDate() + addDay);
		}
		if (addDay < 0){
			dateFrom.setDate(dateFrom.getDate() + addDay);
		}
		
		timeSelectorFrom.Fill(dateFrom);
		timeSelectorTill.Fill(dateTill);
	},
	
	fixTimeFormat : function(sTime) {
		sTime = '' + sTime;
		var result = this.patternCommonTime.exec(sTime);
		if (result) {
			if (!result[2]) {
				result[2] = ':00';
			}
			
			sTime = ''
			for (var i=1, c=result.length; i < c; i++) {
				sTime += result[i];
			}
			
			return sTime;
		}
		
		return false
	}
}

/**
 * Create
 * Show
 * Hide
 * RefreshCalendarSelector
 * CreateChangeMonthFunc
 * EventCreatePickerFunc
 */
function CCalendarTableEvent (parent_id)
{
	this.m_names = [
		Lang.FullMonthJanuary,
		Lang.FullMonthFebruary,
		Lang.FullMonthMarch,
		Lang.FullMonthApril,
		Lang.FullMonthMay,
		Lang.FullMonthJune,
		Lang.FullMonthJuly,
		Lang.FullMonthAugust,
		Lang.FullMonthSeptember,
		Lang.FullMonthOctober,
		Lang.FullMonthNovember,
		Lang.FullMonthDecember
	];
	this.weeksnum = 7;

	this.HtmlContainer		= $id(parent_id);
	this.parent_id			= parent_id;
	this.prevMonthSwitcher	= $id(parent_id + "_prevMonthSwitcher");
	this.nextMonthSwitcher	= $id(parent_id + "_nextMonthSwitcher");
	this.calendarBlock		= $id(parent_id + "_calendarBlock");
	this.inp				= $id(parent_id + "_currentMonth");
	this.currentMonth		= $id(parent_id + "_monthName");
	this.till				= $id("EventDateTill");
	this.from				= $id("EventDateFrom");
	this.input_timeFrom		= $id("EventTimeFrom");
	this.input_timeTill		= $id("EventTimeTill");

	this.calendarRows		= [];
	var obj					= this;

	this.Create();
	this.Hide();
	if (this.parent_id == "st") {
		this.onpickdate = function(date)
		{
			obj.from.value = ConvertFromDateToStr(date);
			obj.till.value = obj.from.value;
		}
	}
	if ((this.parent_id == "en")) {
		this.onpickdate = function(date)
		{
			var date_from = ConvertFromStrToDate(obj.from.value);
			if (date <= date_from) {
				obj.till.value = obj.from.value;
			} else {
				obj.till.value = ConvertFromDateToStr(date);
			}
		}
	}
}
CCalendarTableEvent.prototype = {
	Create: function ()
	{
		var i, w, trArr, tdArr, title, a;
		trArr = this.calendarBlock.getElementsByTagName("tr");
		tdArr = trArr[0].getElementsByTagName("td");
		for (i=0; i<tdArr.length; i++) {
			title = document.createTextNode(weekDaysNamesCalendar[i]);
			tdArr[i].appendChild(title);
		}

		for (w=1; w<trArr.length; w++) {
			this.calendarRows[w-1] = [];
			tdArr = trArr[w].getElementsByTagName("td");
			for(i = 0; i<7; i++){
				a = document.createElement("a");
				a.href = "javascript:void(1);";
				tdArr[i].appendChild(a);
				this.calendarRows[w-1][i] = {
					td: tdArr[i],
					a: a
				};
			}
		}
	},
	Show: function ()
	{
		this.HtmlContainer.style.position = "absolute";
		this.HtmlContainer.style.zIndex = "10";
		this.HtmlContainer.className = "box";
	},
	Hide: function ()
	{
		this.HtmlContainer.className = "wm_hide";
	},
	Fill: function(day,month,year) //fill calendar with new data
	{
		var todayDate, today_month, today_year, today_date, PM_prev_month, PM_prev_year, NM_next_month, NM_next_year;
		var w, d, tr, a, i, this_date_year, this_date_month, this_date_day, this_date_date, id, link_class, this_date, obj = this;

		day = Number(day);
		month = Number(month);
		year = Number(year);

		todayDate = new Date();
		today_month = todayDate.getMonth();
		today_year = todayDate.getFullYear();
		today_date = todayDate.getDate();

		// checking input data
		if (month == null || isNaN(month) || day == null || isNaN(day)
			|| year == null || isNaN(year)) {
			month = today_month+1;
			day = today_date;
			year = today_year;
		}
		// For Previous Month
		PM_prev_month = month-1;
		PM_prev_year = year;
		if (PM_prev_month == 0) {
			PM_prev_month = 12;
			PM_prev_year = year -1;
		}
		// For Next Month
		NM_next_month = month +1;
		NM_next_year = year;
		if (NM_next_month == 13) {
			NM_next_month = 1;
			NM_next_year = year + 1;
		}
		this.prevMonthSwitcher.onclick = this.CreateChangeMonthFunc(this, PM_prev_month, PM_prev_year);
		this.nextMonthSwitcher.onclick = this.CreateChangeMonthFunc(this, NM_next_month, NM_next_year);
		PasteTextNode(this.currentMonth, (this.m_names[month - 1] + ' ' + year));
		this.inp.value = month+'_'+year;

		month = month - 1;
		this_date = new Date(monthLimits[year][month].bigStart);

		for (w = 0; w<this.weeksnum; w++) {
			tr = this.calendarRows[w];//tr = {td: td, a: a};
			for (d = 0; d < tr.length; d++) {
				this_date_year = this_date.getFullYear();
				this_date_month = this_date.getMonth();
				this_date_date = this_date.getDate();
				this_date_day = this_date.getDay();

				tr[d].td.className = "basic";
				a = tr[d].a;
				id = this.parent_id + "_" + this_date_year + Fnum((this_date_month+1),2) + Fnum(this_date_date,2);
				a.id = id;

				if (this_date_month != month) {
					link_class = "CalLinkInactive";
					for (i=0; i<weekendDays.length; i++) {
						if (weekendDays[i] == this_date_day && setcache.showweekends == 1) {
							link_class = "CalLinkInactiveWeekend";
							break;
						}
					}
				}
				else {
					link_class = "CalLink";
				}
				a.className = link_class;
				a.onclick = function() {
					var id = this.id;
					obj.EventCreatePickerFunc(obj, id.slice((id.indexOf("_")+1),id.length));
				}

				PasteTextNode(a, this_date_date);
				this_date.setDate(this_date_date + 1);
			}
		}

		today_id = this.parent_id + "_" + to8(todayDate);
		current_day = $id(today_id);
		if (current_day != undefined) current_day.parentNode.className = "today";
	},
	CreateChangeMonthFunc: function(obj, PM_prev_month, PM_prev_year) {
		return function() {
			CacheLoad(new Date(PM_prev_year, (PM_prev_month-1), 1));
			obj.Fill(1, PM_prev_month, PM_prev_year);
		}
	},
	EventCreatePickerFunc: function(obj, dt){
		var date = new Date(dt.substr(0,4), (Number(dt.substr(4,2))-1), dt.substr(6,2));
		obj.onpickdate(date);
		obj.Hide();
	}
}

/**
 * initialize
 * RecalcShadowHeight
 * SelectColorForNewCalendar
 * CalendarFormCreate
 * CalendarFormCorrect
 * CalendarFormDelete
 * CalendarFormCancel
 * SaveFormOnEnter
 * CalendarFormSave
 * GetColor
 */
function CCalendarForm() {
	this.calendar_window			= $id('manager_window');
	this.calendar_form				= $id('manager_form');
	this.calendar_shadow            = $id("manager_shadow");
	this.window_header				= $id('ef_fulldate_calendar');
	this.id_calendar				= $id('clndform_id');
	this.calendar_subject			= $id('CalendarSubject');
	this.description				= $id('CalendarDescription');
	this.confirm_window				= $id('confirm_window');
	this.calendar_color_number		= $id('calendarColorNumber');
	this.calendarcontainer          = $id('calendarcontainer');

	this.savebut	= $id('savebut_calendar');
	this.cancelbut	= $id("calncelbut_calendar");
	this.delbut		= $id('delbut_calendar');

	this.initialize();
}
CCalendarForm.prototype = {
	initialize: function() {
		var obj = this;
		this.calendar_subject.onkeypress = function(e) {
			obj.SaveFormOnEnter(e);
		};
		this.savebut.onclick = function() {
			obj.CalendarFormSave();
		};
		this.cancelbut.onclick = function() {
			obj.CalendarFormCancel();
		};
		this.delbut.onclick = function() {
			obj.CalendarFormDelete();
		};
	},
	RecalcShadowHeight: function() {
		var height = "auto";
		if ((this.calendarcontainer.clientHeight - 4) > 0) {
			height = (this.calendarcontainer.clientHeight - 4) + "px";
		}
		this.calendar_shadow.style.height = height;
	},
	SelectColorForNewCalendar: function(numb, div)
	{
		div.style.borderColor = '#000';
		var i, color_elem;
		for(i = 1; i<13; i++)
		{
			color_elem = $id('color_'+i);
			if(color_elem.id != div.id)
				color_elem.style.borderColor = '#fff';
		}
		this.calendar_color_number.value = numb;
		this.calendarcontainer.className = 'eventcontainer_'+ numb;
	},
	CalendarFormCreate: function() {
		setMaskHeight(this.calendar_window);
		this.calendar_window.style.display = 'block';
		if(SafariDetect().isSafari) {
			this.calendar_form.style.top = window.innerHeight / 2 + 'px';
		}
		this.window_header.innerHTML = Lang.CalendarHeaderNew;
		this.delbut.style.display="none";
		this.id_calendar.value = 0;

		this.calendar_subject.value = '';
		this.calendar_subject.focus();

		var color = this.GetColor();
		this.SelectColorForNewCalendar(color, $id('color_'+color));
		this.description.value = '';
		this.RecalcShadowHeight();
	},
	CalendarFormCorrect: function(obj) {
		var idc, calendar_obj, color_number;

		QuickMenu.RemoveQuickMenu();
		setMaskHeight(this.calendar_window);

		idc = obj.calendar_id;
		calendar_obj = mycache.calendars[idc];

		if (calendar_obj.is_default == '1')
		{
			this.delbut.style.display = "none";
		}
		else
		{
			this.delbut.style.display = "";
		}
		this.id_calendar.value = idc;
		this.calendar_window.style.display='block';
		if(SafariDetect().isSafari) {
			this.calendar_form.style.top = window.innerHeight / 2 + 'px';
		}
		this.window_header.innerHTML = Lang.CalendarHeaderEdit;
		color_number = calendar_obj.color;
		this.SelectColorForNewCalendar(color_number, $id('color_'+color_number));
		this.calendar_subject.value = calendar_obj.name;
		this.calendar_subject.focus();
		description = calendar_obj.description;
		if (description == null || description == undefined) description = "";
		this.description.value = description;
		this.RecalcShadowHeight();
	},
	CalendarFormUnsubscribe_CallBack: function(aData) {
		if (!ServErr(aData,Lang.ErrorDeleteCalendar)) {
			if (aData.calendar_id != 'undefined') {
				var calendar_obj = mycache.calendars[aData.calendar_id], i, 
					calendar, shared_exsists = false;
				if (calendar_obj != undefined) {
					Calendars.DeleteCalendarElement(calendar_obj);
					
					if (mycache.calendars != undefined){
						for (i in mycache.calendars) {
							calendar = mycache.calendars[i];
							if (calendar.sharing_level != null)
							{
								shared_exsists = true;
							}
						}
					}
					if (!shared_exsists)
					{
						$id('sharedCalendarsHeader').style.display = 'none';
					}
					
					cmScroll_resize();
					calendarInManager.MarkDays();
//					calendarInManager.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
					RenderMonth();
					EventForm.RedrawCalendarSelect();
				}
			}
		}
		HideInfo();
	},
	CalendarFormUnsubscribe: function(id_calendar) {
		if (id_calendar == undefined) {
			id_calendar = this.id_calendar.value;
		}
		if (id_calendar == undefined || id_calendar == "") return;
		
		var calendar_data = mycache.calendars[id_calendar];
		if (calendar_data == undefined) {
			this.calendar_window.style.display = 'none';
			return;
		}
		this.confirm_window.style.display='block';
		if (confirm(Lang.ConfirmUnsubscribeCalendar + " "+calendar_data.name+"?")) {
			ShowInfo(Lang.InfoUnsubscribing);
			this.confirm_window.style.display='none';

			var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
			requestParams = 'action=unsubscribe_calendar' +
							'&calendar_id=' + id_calendar +
							'&nocache=' + Math.random();
			if (req) {
				var self = this;
				sendARequest(req, url, requestParams, function(aData) {
					self.CalendarFormUnsubscribe_CallBack(aData);
				});
			}
		} else {
			this.confirm_window.style.display='none';
			return;
		}
		this.calendar_window.style.display = 'none';
	},
	CalendarFormDelete_CallBack: function(aData) {
		if (!ServErr(aData,Lang.ErrorDeleteCalendar)) {
			if (aData.calendar_id != 'undefined') {
				var calendar_obj = mycache.calendars[aData.calendar_id];
				if (calendar_obj != undefined) {
					Calendars.DeleteCalendarElement(calendar_obj);
					cmScroll_resize();
					calendarInManager.MarkDays();
//					calendarInManager.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
					RenderMonth();
					EventForm.RedrawCalendarSelect();
				}
			}
		}
		HideInfo();
	},
	CalendarFormDelete: function(id_calendar) {
		if (id_calendar == undefined) {
			id_calendar = this.id_calendar.value;
		}
		if (id_calendar == undefined || id_calendar == "") return;

		var calendar_data = mycache.calendars[id_calendar];
		if (calendar_data == undefined) {
			this.calendar_window.style.display = 'none';
			return;
		}
		this.confirm_window.style.display='block';
		if (confirm(Lang.ConfirmDeleteCalendar + " "+calendar_data.name+"?")) {
			ShowInfo(Lang.InfoDeleting);
			this.confirm_window.style.display='none';

			var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
			requestParams = 'action=delete_calendar' +
							'&calendar_id=' + id_calendar +
							'&nocache=' + Math.random();
			if (req) {
				var self = this;
				sendARequest(req, url, requestParams, function(aData) {
					self.CalendarFormDelete_CallBack(aData);
				});
			}
		} else {
			this.confirm_window.style.display='none';
			return;
		}
		this.calendar_window.style.display = 'none';
	},
	CalendarFormCancel: function() {
		this.calendar_window.style.display='none';
	},
	SaveFormOnEnter: function(ev) {
		ev = (ev) ? ev : window.event;
		if(ev.keyCode == 13) this.CalendarFormSave();
	},
	CalendarFormSave_CallBack: function(calendar)
	{
		var j, calendar_id, calendar_color, calendar_cahe_data;
		if (!ServErr(calendar,Lang.ErrorLoadCalendar))
		{
			calendar_id = calendar.calendar_id;
			calendar_color = calendar.color;
			if (calendar_id != undefined) {
				calendar_cahe_data = mycache.calendars[calendar_id];
				if (calendar_cahe_data != undefined)
				{
					for (j in  calendar)
					{
						calendar_cahe_data[j]=calendar[j];
					}
				} 
				else
				{
					mycache.calendars[calendar_id] = calendar;
					mycache.calendars[calendar_id].publication_hash = null;
					mycache.calendars[calendar_id].publication_level = null;
					mycache.calendars[calendar_id].sharing_level = null;
					mycache.calendars[calendar_id].shares = [];
				}
			}
			if (this.id_calendar.value == 0)
			{
				Calendars.RenderShowCalendar($id('my_calendars'), calendar); // only in main calendar
				cmScroll_resize();
			} 
			else
			{
				var CalText = $id('calendar_'+calendar_id+'_text');
				CalText.innerHTML = HtmlEncode(this.calendar_subject.value);
				CalText.title = this.calendar_subject.value;
				ChangeColorContainer(calendar_id, calendar_color);
			}
			EventForm.RedrawCalendarSelect();
			ShowReport(Lang.ReportCalendarSaved);
		}
		HideInfo();
	},
	CalendarFormSave: function()
	{
		ShowInfo(Lang.InfoSaving);

		var id_cal, subject, color, description;
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;

		this.calendar_subject.value = Trim(this.calendar_subject.value);
		this.description.value = Trim(this.description.value);
		id_cal = this.id_calendar.value;
		subject = encodeURIComponent(this.calendar_subject.value.substr(0,50));
		description = encodeURIComponent(this.description.value.substr(0,255));

		if (subject == '')
		{
			alert(Lang.WarningCalendarNameBlank);
			this.calendar_subject.focus();
			return;
		}

		if (this.calendar_color_number.value != '0')
		{
			color = this.calendar_color_number.value;
		} 
		else
		{
			if (id_cal != 0)
			{
				var row = mycache.calendars[id_cal];
				color = row.color;
			} 
			else
			{
				color = this.GetColor();
			}
		}

		mycache.colors[color] = color;
		requestParams = 'action=update_calendar'+
					'&calendar_id=' + id_cal +
					'&name=' + subject +
					'&content=' + description +
					'&color_id=' + color +
					'&nocache=' + Math.random();

		if (req) {
			var self = this;
			sendARequest(req, url, requestParams, function(aData) {
				self.CalendarFormSave_CallBack(aData);
			});
		}

		this.calendar_window.style.display = 'none';
	},
	GetColor: function()
	{
		var c, res = [], cnt = 1, calendar = mycache.colors;

		for(c = 1; c < 13; c++)
		{
			if(calendar != undefined && calendar[c] != c) res[cnt++] = c;
		}
		if(res[1] == undefined)
		{
			mycache.colors = new Array;
			res[1] = 1;
		}
		return res[1];
	}
}

/**
 * CreateQuickMenu
 * CreateColorCell
 * ColorChange
 * MoveQuickMenu
 * RemoveQuickMenu
 * FramesetQuickMenu
 * ClickOutside
 */
function CQuickMenu() {
	this.quick_menu				= $id('quick_menu');
	this.manager_list			= $id("manager_list");
	this.allspan				= $id("allspan");
}
CQuickMenu.prototype = {
	CreateQuickMenu: function(calendar_id, index)
	{
		var obj = this, calendar_obj, color, idc, sharing_level;
		calendar_obj = mycache.calendars[calendar_id];
		if (!calendar_obj) return;
		color = calendar_obj.color;
		idc = calendar_obj.calendar_id;
		sharing_level = calendar_obj.sharing_level;

		CleanNode(this.quick_menu);

		/*-------create_shadow-------*/
		var divShadow, divShadow_a_top, divShadow_b_top, divShadow_middle, divShadow_b_bottom, divShadow_a_bottom;
		divShadow = document.createElement('div');
		divShadow.className = "event_black";
		divShadow.id = "quick_menu_shadow";
		divShadow_a_top = document.createElement('div');
		divShadow_a_top.className = "a";
		divShadow.appendChild(divShadow_a_top);
		divShadow_b_top = document.createElement('div');
		divShadow_b_top.className = "b";
		divShadow.appendChild(divShadow_b_top);
		divShadow_middle = document.createElement('div');
		divShadow_middle.className = "event_middle";
		divShadow.appendChild(divShadow_middle);
		divShadow_b_bottom = document.createElement('div');
		divShadow_b_bottom.className = "b";
		divShadow.appendChild(divShadow_b_bottom);
		divShadow_a_bottom = document.createElement('div');
		divShadow_a_bottom.className = "a";
		divShadow.appendChild(divShadow_a_bottom);
		this.quick_menu.appendChild(divShadow);
		/*-------//create_shadow-------*/

		/*-------------create_quick_menu-------------*/
		var i, divEdit, divMain, div1, div2, div3, div31, span31, a31, txt, span41, a41, title, span51, a51, span61, a61, span71, a71, div, div9, div10;
		divEdit = document.createElement('div');
		divEdit.id = "quick_edit";
		divEdit.style.zIndex = index+1;
		divShadow.style.zIndex = index;
		this.quick_menu.style.zIndex = index;
		if (sharing_level == VIEW_BUSY_TIME) {
			divEdit.className = "eventcontainer_15";
		} else {
			divEdit.className = "eventcontainer_"+color;
		}

		divMain = document.createElement('div');
		divMain.className = "event";

		div1 = document.createElement('div');
		div1.className = 'a';
		divMain.appendChild(div1);
		div2 = document.createElement('div');
		div2.className = 'b';
		divMain.appendChild(div2);

		div3 = document.createElement('div');
		div3.className = 'event_middle';
		div31 = document.createElement('div');
		div31.className = "calendar_text";

		if (sharing_level == FULL_CONTROL || sharing_level == null || sharing_level == EDIT_EVENTS) {
			span31 = document.createElement('span');
			span31.className = 'text';
			a31 = document.createElement('a');
			a31.href="javascript:void(0)";
			a31.onclick = function()
			{
				obj.RemoveQuickMenu();
				$id('calendarColorNumber').value = calendar_obj.color;
				EventForm.CreateEventForm(idc);
			};
			txt = document.createTextNode(Lang.EventCreate);
			a31.appendChild(txt);
			span31.onmouseover = function() {
				this.className = "text hover";
			};
			span31.onmouseout = function() {
				this.className = "text";
			};
			span31.appendChild(a31);
			div31.appendChild(span31);
		}

		span41 = document.createElement('span');
		span41.className = 'text';
		span41.onmouseover = function() {
			this.className = "text hover";
		};
		span41.onmouseout = function() {
			this.className = "text";
		};
		a41 = document.createElement('a');
		a41.href="javascript:void(0)";
		a41.id = 'showHide';
		a41.onclick = function(){
			Calendars.HideCalendars(idc);
		};
		if (typeof(showHide[idc]) == "undefined") {
			showHide[idc]=1;
		}
		title = showHide[idc]? Lang.CalendarHideOther : Lang.CalendarShowOther;
		txt = document.createTextNode(title);
		a41.appendChild(txt);
		span41.appendChild(a41);
		div31.appendChild(span41);

		if (sharing_level == FULL_CONTROL || sharing_level == null) {
			span51 = document.createElement("span");
			span51.className = 'text';
			span51.onmouseover = function() {
				this.className = "text hover";
			};
			span51.onmouseout = function() {
				this.className = "text";
			};
			a51 = document.createElement("a");
			a51.href="javascript:void(0)";
			a51.onclick=function() {
				CalendarForm.CalendarFormCorrect(calendar_obj)
			};
			txt = document.createTextNode(Lang.CalendarActionEdit);
			a51.appendChild(txt);
			span51.appendChild(a51);
			div31.appendChild(span51);

			span61 = document.createElement("span");
			span61.className = "text";
			span61.onmouseover = function() {
				this.className = "text hover";
			};
			span61.onmouseout = function() {
				this.className = "text";
			};
			a61 = document.createElement("a");
			a61.href="javascript:void(0)";
			a61.onclick = function() {
				SharingForm.ShowForm(idc);
			};
			
			if (sharedCalendars == true)
			{
				txt = document.createTextNode(Lang.ShareActionEdit);
				a61.appendChild(txt);
				span61.appendChild(a61);
				div31.appendChild(span61);
			}
			
			if (calendar_obj.is_default != '1')
			{
				span71 = document.createElement("span");
				span71.className = "text";
				span71.onmouseover = function() {
					this.className = "text hover";
				};
				span71.onmouseout = function() {
					this.className = "text";
				};
				a71 = document.createElement("a");
				a71.href="javascript:void(0)";
				a71.onclick = function() {
					obj.RemoveQuickMenu();
					CalendarForm.CalendarFormDelete(idc);
				};
				txt = document.createTextNode(Lang.CalendarRemove);
				a71.appendChild(txt);
				span71.appendChild(a71);
				div31.appendChild(span71);
			}
			div = document.createElement("div");
			div.className = "evt_mid_color";

			for (i=1; i<13; i++) {
				var checked = 0;
				if (i==color) checked=1;
				this.CreateColorCell(idc, i, div, checked);
			}
			div31.appendChild(div);
		}
		else
		{
			span71 = document.createElement("span");
			span71.className = "text";
			span71.onmouseover = function() {
				this.className = "text hover";
			};
			span71.onmouseout = function() {
				this.className = "text";
			};
			a71 = document.createElement("a");
			a71.href="javascript:void(0)";
			a71.onclick = function() {
//				obj.RemoveQuickMenu();
				CalendarForm.CalendarFormUnsubscribe(idc);
			};
			txt = document.createTextNode(Lang.CalendarUnsubscribe);
			a71.appendChild(txt);
			span71.appendChild(a71);
			div31.appendChild(span71);
		}

		div3.appendChild(div31);
		divMain.appendChild(div3);

		div9 = document.createElement("div");
		div9.className = "b";
		divMain.appendChild(div9);
		div10 = document.createElement("div");
		div10.className = "a";
		divMain.appendChild(div10);
		divEdit.appendChild(divMain);
		this.quick_menu.appendChild(divEdit);
		/*-------------//create_quick_menu-------------*/

		this.quick_menu.style.display = "block";
		this.quick_menu.style.height = (div3.offsetHeight + 4) + "px";

		var qDT=1, calendarManagerTitleHeight, mlScrollTop, mlHeight, ci, calendarHeight, quick_menu_height, scrolledCalendarOffset, heightOfDownOpenedMenu, scrolled, wid;

		calendarManagerTitleHeight = this.manager_list.offsetTop;
		mlScrollTop = this.manager_list.scrollTop;
		mlHeight = this.manager_list.offsetHeight;

		ci = $id("calendar_"+idc);
		calendarHeight = ci.offsetHeight;

		quick_menu_height = this.quick_menu.offsetHeight;
		scrolledCalendarOffset = ci.offsetTop - mlScrollTop;
		if (scrolledCalendarOffset<0) scrolledCalendarOffset = 0;

		heightOfDownOpenedMenu = scrolledCalendarOffset + calendarHeight + quick_menu_height;

		if (heightOfDownOpenedMenu > mlHeight) { //open up
			qDT=1;
			this.quick_menu.style.top = (calendarManagerTitleHeight + scrolledCalendarOffset - quick_menu_height + (FireFoxDetect()?2:1)) +"px";
			divShadow_middle.style.height = (div3.offsetHeight - 2) + "px";
		} else {//open down
			qDT=2;
			divShadow_middle.style.height = div3.offsetHeight + "px";
			this.quick_menu.style.top = (calendarManagerTitleHeight + scrolledCalendarOffset + calendarHeight - (FireFoxDetect()?0:1)) +"px";
		}

		scrolled=(mlHeight<this.manager_list.scrollHeight);
		wid=scrolled?111:125;
		divMain.style.width = wid + "px";
		divShadow.style.width = wid + "px";
		this.quick_menu.style.width = (wid + 2) + "px";

		this.FramesetQuickMenu(qDT);
		QOpen=idc;

		if (MSIEDetect()) {
			this.manager_list.onscroll = function() {
				QuickMenu.MoveQuickMenu();
			};
			this.allspan.onmouseup = function(e) {
				QuickMenu.ClickOutside(e);
			};
		}
		else {
			this.manager_list.addEventListener('scroll', function() {
				QuickMenu.MoveQuickMenu()
			}, false);
			this.allspan.addEventListener('mouseup', function(e) {
				QuickMenu.ClickOutside(e)
			}, false);
		}
	},
	CreateColorCell: function(idc, colorNumber, div, checked)
	{
		var bgColor = GetNumberOfColor(colorNumber);
		var divColor = document.createElement('div');
		divColor.style.backgroundColor = bgColor;
		if (checked == 1) divColor.className = "color_pick checked";
		else divColor.className = "color_pick";
		var obj = this;
		divColor.onclick = function()
		{
			obj.ColorChange(idc, colorNumber);
			obj.RemoveQuickMenu();
		};
		divColor.onmouseover= function()
		{
			this.className='color_pick_hover';
			if (checked == 1) this.className += " checked";
		};
		divColor.onmouseout=function()
		{
			this.className='color_pick';
			if (checked == 1) this.className += " checked";
		};
		div.appendChild(divColor);
	},
	ColorChange_CallBack: function(id, old_color, result) {
		if (ServErr(result,Lang.ErrorUpdateCalendar)) {
			ChangeColorContainer(id, old_color);
			return;
		}
		HideInfo();
	},
	ColorChange: function(id, numb) {
		ShowInfo(Lang.InfoSaving);
		var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
		requestParams = 'action=update_calendar' + 
						'&calendar_id=' + id + 
						'&color_id=' + numb + 
						'&nocache=' + Math.random();
		var old_color = mycache.calendars[id].color;
					
		if (req) {
			var self = this;
			sendARequest(req, url, requestParams, function(aData) {
				self.ColorChange_CallBack(id, old_color, aData);
			});
		}
		ChangeColorContainer(id, numb);
		this.RemoveQuickMenu();
		EventForm.RedrawCalendarSelect();
	},
	MoveQuickMenu : function() {
		var calendarManagerTitleHeight = this.manager_list.offsetTop;
		var mlScrollTop = this.manager_list.scrollTop;
		var mlHeight = this.manager_list.offsetHeight;
		if (QOpen==0) return;
		var qDT = 1;
		var ci = $id('calendar_' + QOpen);

		var calendarOffsetTop = ci.offsetTop;
		var calendarHeight = ci.offsetHeight;

		var quick_menu_height = this.quick_menu.offsetHeight;
		var scrolledCalendarOffset = calendarOffsetTop - mlScrollTop;
		if (scrolledCalendarOffset<0) scrolledCalendarOffset = 0;

		var heightOfDownOpenedMenu = scrolledCalendarOffset + calendarHeight + quick_menu_height;
		var qm_top = 0;
		if (heightOfDownOpenedMenu > mlHeight) {//open up
			qDT = 1;
			qm_top = (calendarManagerTitleHeight + scrolledCalendarOffset - quick_menu_height + (FireFoxDetect()?2:1)) +"px";
		}
		else {//open down
			qDT = 2;
			qm_top = (calendarManagerTitleHeight + calendarOffsetTop - mlScrollTop + calendarHeight - (FireFoxDetect()?0:1)) +"px";
		}

		if (((calendarOffsetTop - mlScrollTop + calendarHeight) < 0) || ((calendarOffsetTop - mlScrollTop) > mlHeight)) {
			this.RemoveQuickMenu();
		// 1) drop down, menu top is above manager_list top
		// 2) any drop, marker bottom is above list top
		// 3) drop down, marker below list top
		}
		else {
			this.quick_menu.style.top = qm_top;
			this.FramesetQuickMenu(qDT);
		}
	},
	RemoveQuickMenu: function() {
		this.quick_menu.style.display = 'none';
		QOpen=0;
		if (MSIEDetect()) {
			this.allspan.detachEvent('onmouseup', QuickMenu.ClickOutside);
			this.manager_list.detachEvent('onscroll', QuickMenu.MoveQuickMenu);
		}
		else {
			this.allspan.removeEventListener('mouseup', function(e)
			{
				QuickMenu.ClickOutside(e)
			}, false);
			this.manager_list.removeEventListener('scroll', function()
			{
				QuickMenu.MoveQuickMenu()
			}, false);
		}
	},
	FramesetQuickMenu: function(menumode) {
		var div = $id('quick_edit').getElementsByTagName('div')[0];
		var quick_menu_shadow = $id('quick_menu_shadow');
		var childLen = div.childNodes.length;
		if (menumode==2) { //open down
			div.childNodes[0].className='a qmenu1';
			div.childNodes[1].className='b qmenu1';
			div.childNodes[childLen-2].className='b';
			div.childNodes[childLen-1].className='a';
			quick_menu_shadow.childNodes[0].className='a qmenu1';
			quick_menu_shadow.childNodes[1].className='b qmenu1';
			quick_menu_shadow.childNodes[3].className='b';
			quick_menu_shadow.childNodes[4].className='a';
		}
		else { //open up
			div.childNodes[0].className='a';
			div.childNodes[1].className='b';
			div.childNodes[childLen-2].className='b qmenu1';
			div.childNodes[childLen-1].className='a qmenu1';
			quick_menu_shadow.childNodes[0].className='a';
			quick_menu_shadow.childNodes[1].className='b';
			quick_menu_shadow.childNodes[3].className='b qmenu1';
			quick_menu_shadow.childNodes[4].className='a qmenu1';
		}
	},
	ClickOutside: function(evt) {
		if (QOpen==0) return;
		var even = window.event ? window.event.srcElement : evt.target;
		if (even.id) {
			if ((even.id==('calendar_' + QOpen + '_text'))
				|| (even.id=='calendar_' + QOpen + '_dtext')
				|| (even.id==('vis_check_' + QOpen))) {
				return;
			}
		}
		this.RemoveQuickMenu();
	}
}