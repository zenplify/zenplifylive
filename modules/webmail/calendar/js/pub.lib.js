var drager = null;
var more_dates = [];
var shownfetch=0;
var indrag=false;

function DocumentOnClickHandler(evt) {
	var tgt = window.event ? window.event.srcElement : evt.target;
	var monthsList = $id("monthsList");
	if (!monthsList.hidden && !(tgt.id == "monthsList" || tgt.id == "middleTdSelector" || tgt.id == "MonthSelector" || tgt.id == "imgSelector")){
		var middleTdSelector = $id("middleTdSelector");
		monthsList.style.display = 'none';
		monthsList.hidden = true;
		middleTdSelector.className = 'middleTdSelector';
	}
}

function gotcha(ev) {
	EventOff("mouseup",gotcha,true);
	ev.cancelBubble=true;

	if (!indrag) {
		invoked_event.obj.style.display="block";
		zb=coordss(ev,invoked_event.obj, invoked_event.event_allday, mybox,0);/*!!!*/
		if ((Math.abs(inipos.x-zb.x)<3)&&(Math.abs(inipos.y-zb.y)<3)) {
			OpenEventToEdit(ev,invoked_event.obj);
		}
	}
}

function OpenEventToEdit(ev,div) {
	if (ev != null) ev.cancelBubble = true;
	var tfDateObj, ttDateObj, excluded = false, iddata, evlink, tf, tt;

	iddata = Isplit(div.id);

	if (mycache.exclusions[iddata.id_original] != undefined) {
		evlink = mycache.exclusions[iddata.id_original];
		tfDateObj = evlink.event_timefrom;
		ttDateObj = evlink.event_timetill;
		excluded = true;
	}
	else {
		evlink = mycache.ids[iddata.id];
		var repeat_data = mycache.repeats[iddata.id_original];
		if (evlink.event_repeats == 1 && repeat_data) {
			tfDateObj = repeat_data["event_timefrom"];
			ttDateObj = repeat_data["event_timetill"];
		}
		else {
			tfDateObj = evlink["event_timefrom"];
			ttDateObj = evlink["event_timetill"];
		}
	}

	tf = GetDateParams(tfDateObj, false, new Array('TIME'));
	tt = GetDateParams(ttDateObj, true, new Array('TIME', 'DATE'));

	var editevent_form_data = {
		subject 	: evlink["event_name"],
		timeFrom	: tf.time,
		timeTill	: tt.time,
		fullFromDate: tfDateObj,
		fullTillDate: tt.date,
		description	: evlink["event_text"],
		event_id	: iddata.id_original,
		calendar_id : evlink['calendar_id'],
		excluded	: excluded,
		allday	  : 0
	}
	EventForm.ShowEventForm(editevent_form_data);
}

function ShowMoreEventsList() {
	if ($id('mfet')) {
		HideMoreEventsList();
		return;
	}
	HideMoreEventsList();
	var date_full_id, id_data, date_id, mdata, month_area, month_area_height;
	var showbox, showwid, showhei, more_block, showmax, showtop, showleft;
	var showover, stx = '', mtitl, event_name, event_id, real_event;
	var event_data, calendar_data, event_link, mtf, mtt, mtr, close_btn;

	date_full_id = String(this.id);
	id_data = date_full_id.split('_');
	date_id = Number(id_data[1]);
	mdata = more_dates[date_id]; //global array  = ['date'] = array(id1, id2, ... , idn)
	month_area = $id('area_2_month');
	month_area_height = ParseToNumber(month_area.offsetHeight);
	showbox = Grid.GetGridParams(1);
	showwid = 1.5*Math.floor(ParseToNumber(month_area.offsetWidth)/7);
	showhei = mdata.length*13+1;

	more_block = document.createElement('div');
	showmax = 3 * Math.floor(month_area_height/6);
	if (showhei>showmax) {
		showhei = showmax;
		more_block.style.overflowY = 'scroll';
	}
	showtop = showbox.top + ParseToNumber(this.offsetTop) + 2;
	showleft = showbox.left + ParseToNumber(this.offsetLeft) + ParseToNumber(this.offsetWidth) + 2;
	showover = showtop + showhei - (month_area_height + showbox.top);
	if (showover > 0) showtop -= showover;

	more_block.style.top = showtop + 'px';
	more_block.style.left = showleft + 'px';
	more_block.style.width = showwid + 'px';
	more_block.style.height = showhei + 'px';
	more_block.id = 'mfet';
	more_block.className = 'event_select_box';

	for (var i in mdata) {
		event_name = '';
		event_id = mdata[i];
		real_event = SplitEventId(event_id);
		if (mycache.exclusions[event_id] != undefined) {
			event_data = mycache.exclusions[event_id];
		}
		else {
			event_data = mycache.ids[real_event.id];
		}
		event_name = event_data['event_name'];
		stx += "\n"+event_name;
		calendar_data = mycache.calendars[event_data['calendar_id']];

		event_link = document.createElement('span');
		event_link.className = 'event_element_select_box';

		if (calendar_data.publication_level == 2) {
			event_link.style.color = GetNumberOfColor(15);
		}
		else {
			event_link.style.color = GetNumberOfColor(calendar_data['color']);
			event_link.onmouseover = function()
			{
				this.style.textDecoration='underline';
			}
			event_link.onmouseout = function()
			{
				this.style.textDecoration='none';
			}
			event_link.onmousedown = function(ev)
			{
				drag2start(ev,this);
				return false;
			}
		}
		
		event_link.unselectable = 'on';
		event_link.id = 'event_' + event_id + '_0_3_'+event_data.event_allday;

		mtf = gtime(event_data["event_timefrom"]);
		mtt = gtime(event_data["event_timetill"],24);

		if (event_data['event_allday'] == 0) {
			mtitl = mtf.utime+' - '+mtt.utime+', '+mtf.udate;
		}
		else {
			mtr = range8(mtf.to8,mtt.to8)+1; 
			if (mtr==1) {
				mtitl=mtf.udate;
			}
			else {
				mtitl=mtf.udate+' - '+mtt.udate+' ('+mtr+' days)';
			}
		}
		event_link.title = mtitl;
		event_link.appendChild(document.createTextNode(event_name));
		event_link.style.MozUserSelect='none';
		event_link.style.KhtmlUserSelect='event_link.style.';
		event_link.style.userSelect='none';
		more_block.appendChild(event_link);
		more_block.appendChild(document.createElement('br'));
	}
	close_btn = document.createElement('img');
	close_btn.src = './calendar/skins/calendar/close-popup.png';
	close_btn.className = 'close_more_list';
	more_block.onclick = HideMoreEventsList;
	more_block.appendChild(close_btn);

	document.body.appendChild(more_block);
	shownfetch = 2;
}

function drag2start(ev, obj) {
	indrag = true;
	evobj = obj;
	var idd = evobj.id;
	var modez = Isplit(idd);
	vMid = modez.id_original; /*global*/ 
	if (mycache.exclusions[vMid] != undefined) {
		evlink = mycache.exclusions[vMid];
	}
	else {
		evlink = mycache.ids[modez.id]; /*global*/
	}
	var mybox = Grid.GetGridParams(0);
	stc = coordss(ev, evobj, 0, mybox, 0); /*global*///mode was setted as MONTH
	stx = stc.x; /* is global? */
	sty = stc.y;
	dragon=0;
	var calendar_data = mycache.calendars[evlink['calendar_id']];
	if (calendar_data != undefined) {
		if (calendar_data.publication_level == 1) {
			EventOn("mouseup", drag2done, true);
		}
	}
	document.onselectstart = function()
	{
		return false;
	}
	document.onselect = function()
	{
		return false;
	}
}

function drag2done() {
	EventOff("mouseup", drag2done, true);
	document.onselectstart = function() {}
	document.onselect = function() {}
	if (dragon == 0) {
		HideMoreEventsList();
		OpenEventToEdit(null,evobj);
	}
	indrag = false;
}

function AddEventToGrid(event_id, viewMode, date) {
	var i, j, cnt, eventPosData, evData, timefrom, timetill, real_event, eventTextData, calendar_container;
	var arrows = ['','_left','_right','_both'], common_id = '', startIndex=0;
	var endIndex=0, event_name, tf, tt, calendar_obj, evitem;
	var calendar_public_level = null, ari, allday, arsuf, div1, div2, div3;
	var div4, div5, div6, div7, div8, div9, divtit, titl, tex1, start_year;
	var end_year, tmp_start, tmp_end, build_lims = [];

	if (viewMode == 0) {
		evData = mycache.d[event_id];
		tmp_start = from8(showLimits.day);
		startIndex = GetEventWeek(tmp_start);
		endIndex = startIndex;
		start_year = tmp_start.getFullYear();
		end_year = start_year;
	}
	else if (viewMode == 1) {
		evData = mycache.w[event_id];
		tmp_start = from8(showLimits.weekFrom);
		tmp_end = from8(showLimits.weekTill);
		startIndex = GetEventWeek(tmp_start);
		endIndex = GetEventWeek(tmp_end);
		start_year = tmp_start.getFullYear();
		end_year = tmp_end.getFullYear();
	}
	else {
		evData = mycache.m[event_id];
		tmp_start = from8(showLimits.monthFrom);
		tmp_end = from8(showLimits.monthTill);
		startIndex = GetEventWeek(tmp_start);
		endIndex = GetEventWeek(tmp_end);
		start_year = tmp_start.getFullYear();
		end_year = tmp_end.getFullYear();
	}

	eventPosData = copyar(evData);
	if (eventPosData == undefined || eventPosData._width == undefined) return;
	if (mycache.exclusions[event_id] != undefined) {
		eventTextData = mycache.exclusions[event_id];
		timefrom = eventTextData.event_timefrom;
		timetill = eventTextData.event_timetill;
	}
	else {
		real_event = SplitEventId(event_id);
		eventTextData = mycache.ids[real_event.id];
		if (eventTextData == undefined) return;
 	if (real_event.repeat != 0 || (real_event.date != 0 && real_event.date != to8(eventTextData.event_timefrom))) {
			var repeatData = mycache.repeats[event_id];
			timefrom = repeatData.event_timefrom;
			timetill = repeatData.event_timetill;
		}
		else {
			timefrom = eventTextData.event_timefrom;
			timetill = eventTextData.event_timetill;
		}
	}

	allday = eventTextData.event_allday;
	calendar_container = $id(eventPosData._parent);
	calendar_obj = mycache.calendars[eventTextData['calendar_id']];
	if (calendar_obj != undefined) {
		calendar_public_level = calendar_obj.publication_level;
	}
	event_name = eventTextData['event_name'];
	divtit = event_name;
	titl = '';
	if (allday == 0) {
		tf = GetDateParams(timefrom, false, new Array('TIME'));
		tt = GetDateParams(timetill, true, new Array('TIME'));
		titl = tf.time + ' - ' + tt.time;
		divtit = '['+titl+'] '+divtit;
	}

	evitem = eventPosData;
	j = 0;

	if (start_year == end_year) {
		for (cnt in evitem._width[start_year]) {
			if (cnt >= startIndex && cnt <= endIndex) {
				build_lims[start_year] = {start: startIndex, end: endIndex + 1}
				break;
			}
		}
	}
	if (start_year < end_year) {
		for (cnt in evitem._width[start_year]) {//this part of code allows build event from real current part, not from 0 always
			if (cnt >= startIndex && cnt <= (weekLimits[start_year].length-2)) {
				build_lims[start_year] = {start: startIndex, end: weekLimits[start_year].length-1}
				break;
			}
		}

		build_lims[end_year] = {start: 0, end: endIndex + 1}
	}
	if (start_year > end_year){
		return;
	}
	for (var y in build_lims) {
		var index = Number(build_lims[y].start);
		var end = Number(build_lims[y].end);
		while (index < end) {
			if (evitem._width[y] != undefined && evitem._width[y][index] != undefined){
				common_id = event_id + '_' + j + '_' + viewMode + '_' + eventTextData['event_allday'];
				mixid = 'event_' + common_id;

				ari = 0;
				if (viewMode == DAY) {
					for (i in evitem._arrindex[y]) {
						if (i == date) {
							ari = evitem._arrindex[y][i];
							break;
						}
					}
				}
				else {
					ari = evitem._arrindex[y][index];
				}

				arsuf = arrows[ari];
				div1 = document.createElement('div');
				div1.id = mixid;
				div1.className = 'event';
				div1.style.marginTop = evitem._off + 'px';
				div1.style.width = evitem._width[y][index] + '%';
				div1.style.left = evitem._left[y][index] + '%';
				div1.style.top = evitem._top;

				div2 = document.createElement('div');
				div3 = document.createElement('div');

				div4 = document.createElement('div'); /* global */
				div4.className = 'event_middle';
				div4.style.height = evitem._height;
				div4.id = 'mid_' + common_id;

				div5 = document.createElement('div');
				div5.className = 'event_text' + arsuf;

				div6 = document.createElement('div');
				div6.id = 'time_' + common_id;

				div7 = document.createElement('div');
				div7.className = 'text';
				if (allday == 1) {
					div6.style.display = 'none';
					div7.style.top = '1px';
				}

				if (ari == 0) {
					div2.className = 'a';
					div3.className = 'b';
					tex1 = document.createTextNode(titl);
					div6.className = 'time';
					div6.appendChild(tex1);
				}
				else {
					div2.className = 'a' + arsuf + ' a';
					div3.className = 'b' + arsuf + ' b';
					div6.className = 'time time' + arsuf;
				}

				div8 = document.createElement('div');
				div8.id = 'c_'+common_id;

				div9 = document.createElement('div');
				div9.id = 'a_' + common_id;

				if (viewMode == 2 || allday == 1) {
					div8.className = 'b' + arsuf + ' b';
					div9.className = 'a' + arsuf + ' a';
				}
				else {
					div8.className = 'b';
					div9.className = 'a';
				}

				if (calendar_public_level != 2) {
					div1.onmouseout = EventMouseOutHandler;
					div1.onmouseover = EventMouseOverHandler;
					div1.style.cursor = 'pointer';
					div1.title = divtit;
					div4.onmousedown = function(ev)
					{
						invoke(ev, this);
						StopEvents(ev);
						return false;
					}
					var tex3=document.createTextNode(event_name);
					div7.appendChild(tex3);
				}
				else {
					div1.style.cursor = 'default';
					div1.title = titl;
				}

				div1.appendChild(div2);
				div1.appendChild(div3);


				div5.appendChild(div6);
				div5.appendChild(div7);
				div4.appendChild(div5);
				div1.appendChild(div4);
				div1.appendChild(div8);
				div1.appendChild(div9);

				calendar_container.appendChild(div1);
				j++;
			}
			index++;
		}
	}
}

function LoadDataFromServer() {

	var data = [], i, j, k;
	var color_arr = [], color, calendar_cache = [], calendar, calendar_id;
	var todayYear, todayMonth, events = [], event, allow_add;
	var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
	
	requestParams = 'action=get_data&nocache=' + Math.random();
	if (req) {
		data = sendRequest(req, url, requestParams);
		if (!ServErr(data,Lang.ErrorGeneral)) {
			
//calendar
			calendar = data['calendar'];
			calendar_id = calendar["calendar_id"];
			color = calendar["color"];
			color_arr[color] = color;
			calendar_cache[calendar_id] = calendar;

			mycache.calendars = calendar_cache;
			mycache.colors = color_arr;
			
			
//events
			if (data['events'] != undefined) {
				events = data['events'];
				var excl = data['exclusions'];
				if (excl != undefined  && ObjectPropertiesCount(excl) > 0) {
					for (j in excl) {
						allow_add = false;
						for (k in mycache.calendars) {
							if (excl[j]['calendar_id'] == k) {
								allow_add = true;
								break;
							}
						}
						if (!allow_add) {
							excl[j]['is_deleted'] = 1;
						}
						if (mycache.exclusions[j] == undefined) mycache.exclusions[j] = ConvertObjectVariablesToTheirTypes(excl[j]);
					}
				}
			}
			todayYear = mydate.getFullYear();
			todayMonth = mydate.getMonth();
			cachedMonths.push(new Array(todayMonth, todayYear));

			AddEventsToCache(events);
		}
	}

}

function RenderCalendar() {
	var calendar, color;
	for (i in mycache.calendars) {
		calendar = mycache.calendars[i];
		if (calendar.publication_level == 2) {
			color = 15;
		} else {
			color = calendar.color;
		}
		Grid.RenderContainerForEvent(calendar.calendar_id, color);
		break;
	}
}