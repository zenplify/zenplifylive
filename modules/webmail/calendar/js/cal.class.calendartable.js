/*
 * Create
 * Fill
 * MarkDays
 * CreateChangeMonthFunc
*/

function CCalendarMini ()
{
	this.m_names = [
		Lang.FullMonthJanuary,
		Lang.FullMonthFebruary,
		Lang.FullMonthMarch,
		Lang.FullMonthApril,
		Lang.FullMonthMay,
		Lang.FullMonthJune,
		Lang.FullMonthJuly,
		Lang.FullMonthAugust,
		Lang.FullMonthSeptember,
		Lang.FullMonthOctober,
		Lang.FullMonthNovember,
		Lang.FullMonthDecember
	];

	this.middleTdSelector	= null;
	this.monthsList			= null;
	this.selectedMonth		= null;
	this.prevMonthSwitcher	= null;
	this.nextMonthSwitcher	= null;
	this.months				= [];	

	// ---------------------------------

	this.weeksnum = 7;

	this.calendarInManagerBlock = null;
	this.weekendIds	= [];
	
	this.Create();
	this.Fill(mydate.getDate(), mydate.getMonth()+1, mydate.getFullYear());
}

CCalendarMini.prototype = {
	CreateSwitcher: function ()
	{
		var obj = this, a, i;
		
		this.selectedMonth = $('<span></span>')
			.attr(
				{
					'id': 'MonthSelector'
				}
			);
		
		this.middleTdSelector = $('<div></div>')
			.attr(
				{
					'id': 'middleTdSelector',
					'class': 'middleTdSelector'
				}
			)
			.append(this.selectedMonth)
			.append($('<img></img>')
				.attr(
					{
						'id': 'imgSelector',
						'alt': '',
						'src': './calendar/skins/calendar/minicalendar_arrow_bottom.gif'
					}
				)
			);
		
		this.monthsList = $('<div></div>')
			.attr(
				{
					'id': 'monthsList',
					'class': 'event edit_gray monthsList'
				}
			);
				
		this.prevMonthSwitcher = $('<img></img>')
			.attr(
				{
					'class': 'calendar_switcher',
					'id': 'prevMonthSwitcher',
					'alt': Lang.AltPrevMonth,
					'title': Lang.AltPrevMonth,
					'src': './calendar/skins/calendar/minicalendar_arrow_left_activ.gif'
				}
			);
		
		this.nextMonthSwitcher = $('<img></img>')
			.attr(
				{
					'class': 'calendar_switcher',
					'id': 'nextMonthSwitcher',
					'alt': Lang.AltNextMonth,
					'title': Lang.AltNextMonth,
					'src': './calendar/skins/calendar/minicalendar_arrow_right_activ.gif'
				}
			);
		
		$('#mini_calendar_box')
			.append($('<table></table>')
				.addClass('Calendar_Title')
				.append($('<tr></tr>')
					.append($('<td></td>')
						.addClass('arrow_left')
						.append(this.prevMonthSwitcher)
					)
					.append($('<td></td>')
						.addClass('middle')
						.append($('<div></div>')
							.addClass('middleTdSelector_')
							.append(this.middleTdSelector)
						)
						.append(this.monthsList)
					)
					.append($('<td></td>')
						.addClass('arrow_right')
						.append(this.nextMonthSwitcher)
					)
				)
					
			);
		
		this.monthsList.hide();
		this.middleTdSelector.mouseover(function() {
			obj.middleTdSelector.removeClass().addClass('middleTdSelector_over');
		})
		this.middleTdSelector.mouseout(function() {
			if (obj.monthsList.css('display') == 'none') {
				obj.middleTdSelector.removeClass().addClass('middleTdSelector');
			}
		})
		this.middleTdSelector.click(function() {
			if (obj.monthsList.css('display') == 'none') 
			{
				obj.monthsList.show();
			}
			else
			{
				obj.monthsList.hide();
			}
		})

		this.monthsList.empty();
		for (i=0; i<13; i++) {
			a = $('<a></a>').attr({'href' : 'javascript:void(0);'});
			this.months[i] = a;
			this.monthsList
				.append($('<div></div>')
					.removeClass()
					.addClass('event_middle')
					.append($('<div></div>')
						.removeClass()
						.addClass('calendar_text')
						.append($('<span></span>')
							.removeClass()
							.addClass('text')
							.append(a)			
						)
					)
				);
		}
		
		this.monthsList
			.append($('<div></div>')
				.addClass('b')
				.attr({'height': '1px !important'})
			)
			.append($('<div></div>')
				.addClass('a')
			);		
	},
	Create: function ()
	{
		var i, j, trHeader, trContent;
		
		this.CreateSwitcher();
		this.calendarInManagerBlock = $('<table></table>')
			.attr(
				{
					'id': 'calendarInManagerBlock',
					'class': 'calendar_block',
					'cellpadding': '0',
					'cellspacing': '0'
				}
			);
		
		trHeader = $('<tr></tr>');
		for (i=0; i<7; i++){
			trHeader.append($('<td></td>')
				.addClass('title')
				.append(weekDaysNamesCalendar[i])
			);
		}
		this.calendarInManagerBlock.empty().append(trHeader);
		
		for (j=1; j<=7; j++) 
		{
			trContent = $('<tr></tr>');
			
			for(i=0; i<7; i++)
			{
				trContent
					.append($('<td></td>')
					.addClass('basic')
						.append($('<a></a>')
							.attr({'href': 'javascript:void(1);'})
						)
					);
			}
			this.calendarInManagerBlock.append(trContent);
		}
		$('#mini_calendar_box').append(this.calendarInManagerBlock);
	},
	FillSwitcher: function(month, year) //fill calendar with new data
	{
				var obj=this, i, j, k, a, switcher_month, switcher_year, m_num, 
			action_month, action_year;
		
		// For Previous Month
		switcher_month = month - 1;
		switcher_year = year;
		if (switcher_month == 0) 
		{
			switcher_month = 12;
			switcher_year = year - 1;
		}
		
		this.prevMonthSwitcher.unbind('click');
		this.prevMonthSwitcher.bind('click', 
			obj.CreateChangeMonthFunc(obj, switcher_month, switcher_year)
		);
			
		// For Next Month
		switcher_month = month + 1;
		switcher_year = year;
		if (switcher_month == 13) 
		{
			switcher_month = 1;
			switcher_year = year + 1;
		}
		
		this.nextMonthSwitcher.unbind('click');
		this.nextMonthSwitcher.bind('click', 
			obj.CreateChangeMonthFunc(obj, switcher_month, switcher_year)
		);
		
		this.selectedMonth.text(this.m_names[month - 1] + ' ' + year + ' ');
		
		k=0;
		for (i=-6, j=0; i<=6; i++, j++) 
		{
			a = this.months[j];
			m_num = month - 1 + i;
			
			if (m_num < 0) 
			{
				action_month = m_num + 13;
				action_year = year - 1;
			} 
			else if (m_num >= 0 && m_num < 12) 
			{
				action_month = m_num + 1;
				action_year = year;

				if (action_month == month) 
				{
					a.css('fontWeight', 'bold');
				}
			} 
			else 
			{
				action_month = k + 1;
				action_year = year + 1;
				k++;
			}

			a.unbind('click');
			a.bind('click', 
				obj.CreateChangeMonthFunc(obj, action_month, action_year)
			);
			a.text(this.m_names[action_month - 1] + ' ' + action_year);
		}	
	},
	Fill: function(day, month, year) //fill calendar with new data
	{
		var obj = this, i, todayDate, today_month, today_year, today_date;

		day = Number(day);
		month = Number(month);
		year = Number(year);

		todayDate = new Date();
		today_month = todayDate.getMonth();
		today_year = todayDate.getFullYear();
		today_date = todayDate.getDate();

		// checking input data
		if (month == null || isNaN(month) || day == null || isNaN(day) || year == null || isNaN(year)) {
			month = today_month+1;
			day = today_date;
			year = today_year;
		}
		
		this.FillSwitcher(month, year);

		month = month -1;
		this_date = new Date(monthLimits[year][month].bigStart);

		var link_class = "", this_date_year, this_date_month, this_date_date, this_date_day, id, 
			push_weekend, tdClass = "", currentYear, currentMonth, monthStart, monthEnd, limitsDay, 
			weekLimitsFrom, weekLimitsTill, today_id, weekendDay;
		
		
		currentYear = mydate.getFullYear();
		currentMonth = mydate.getMonth();
		monthStart = to8(monthLimits[currentYear][currentMonth].smallStart);
		monthEnd = to8(monthLimits[currentYear][currentMonth].smallEnd);
		limitsDay = showLimits.day;
		weekLimitsFrom = showLimits.weekFrom;
		weekLimitsTill = showLimits.weekTill;
		
		this.calendarInManagerBlock
			.find($('tr td a'))
			.each(function(){
				
				this_date_year = this_date.getFullYear();
				this_date_month = this_date.getMonth();
				this_date_date = this_date.getDate();
				this_date_day = this_date.getDay();
				
				id = this_date_year + Fnum((this_date_month+1), 2) + Fnum(this_date_date, 2);
				$(this).attr({'id': id});
				push_weekend = false;
				for (i=0; i<weekendDays.length; i++) 
				{
					if (weekendDays[i] == this_date_day && setcache.showweekends == 1) 
					{
						obj.weekendIds[String(id)] = true;
						push_weekend = true;
						break;
					}
				}
				if (this_date_month != month) 
				{
					link_class = (push_weekend) ? 'CalLinkInactiveWeekend' : 'CalLinkInactive';
				}
				else 
				{
					link_class = 'CalLink';
				}
				$(this).addClass(link_class);
				$(this).click(function() {
					Switch2date(id);
				});
				$(this).text(this_date_date);
				this_date.setDate(this_date_date + 1);
				
				event_id = $(this).prop('id');
				if (view == DAY && event_id == limitsDay) {
					tdClass = 'select';
				}
				else if (view == WEEK && (event_id >= weekLimitsFrom && event_id <= weekLimitsTill)) 
				{
					tdClass = 'select';
				}
				else if (view == MONTH && (event_id >= monthStart && event_id <= monthEnd)) 
				{
					tdClass = 'select';
				}
				else if (setcache.showweekends == 1) {
					weekendDay = obj.weekendIds[event_id];
					if (weekendDay != undefined && weekendDay == true)
					{
						tdClass = 'weekend';
					}
					else
					{
						tdClass = 'basic';
					}
				}
				else 
				{
					tdClass = 'basic';
				}
				$(this).parent('td').removeClass().addClass(tdClass);				
			})
		today_id = to8(todayDate);
		$('#'+String(today_id)).parent().removeClass().addClass('today');
	},
	MarkDays : function() {
		var event_id, checked = false;

		this.calendarInManagerBlock
			.find($('tr td a'))
			.each(function(){

				event_id = $(this).prop('id');
				checked = (all_events_dates[event_id] != undefined);
				 $(this).css('fontWeight', checked ? 'bold' : 'normal');
			});
	},
	CreateChangeMonthFunc : function(obj, month, year) {
		return function() {
			CacheLoad(new Date(year, (month-1), 1));
			obj.FillSwitcher(month, year);
		}
	}	
}//CCalendarMini.prototype