/*
* CTimeSelector
*/
/*
 * Initialize
 * CancelEventForm
 * ClearEventForm
 * ShowEventForm
 */
function CEventForm() {
	this.event_window			= $id('edit_window');
	this.event_form				= $id('edit_form');
	this.evform_id				= $id('evform_id');
	this.calendar_id			= $id('id_calendar');
	this.eventcontainer			= $id('eventcontainer');
	this.window_header			= $id('ef_fulldate');
	this.EventSubject			= $id('EventSubject');
	this.edit_select_box		= $id('edit_select_box');
	this.color_calendar_now		= $id('color_calendar_now');
	this.calen_sal				= $id('calen_sal');

	this.EventTimeFrom			= $id('EventTimeFrom');
	this.EventDateFrom			= $id('EventDateFrom');
	this.EventTimeTill			= $id('EventTimeTill');
	this.EventDateTill			= $id('EventDateTill');
	this.EventDescription		= $id('EventDescription');
	this.cancelbut				= $id('cancelbut');
	this.shadow_middle			= $id('edit_event_shadow');

	this.eventEdit				= $id("eventEdit");

	this.Initialize();
}

CEventForm.prototype = {
	Initialize: function() {
		var obj = this;
		this.cancelbut.onclick = function() {
			obj.CancelEventForm();
		};
	},
	CancelEventForm : function() {
		this.event_window.style.display = "none";
		this.ClearEventForm();
	},
	ClearEventForm : function() {
		this.event_form.style.display = "none";
	},
	ShowEventForm : function (event_data) {
		var calendar, color, color_number, calendar_name, calendar_id;

		if (mycache.calendars != undefined){
			for (i in mycache.calendars)
			{
                if (mycache.calendars[i].calendar_id == event_data.calendar_id) {
                    calendar = mycache.calendars[i];
                }
			}
		}

		this.cancelbut.style.display = "inline";
		this.window_header.innerHTML = Lang.EventHeaderView;
		this.calen_sal.parentNode.style.cursor = "default";

		this.EventSubject.value		= event_data.subject;
		this.EventTimeFrom.value	= event_data.timeFrom;
		this.EventTimeTill.value	= event_data.timeTill;
		this.EventDateFrom.value	= ConvertFromDateToStr(event_data.fullFromDate);
		this.EventDateTill.value	= ConvertFromDateToStr(event_data.fullTillDate);
		this.EventDescription.value	= event_data.description;
		this.evform_id.value		= event_data.event_id;

		color_number	= calendar.color;
		color			= GetNumberOfColor(calendar.color);
		calendar_name	= HtmlEncode(calendar.name);
		calendar_id		= calendar.calendar_id;

		this.eventcontainer.className = 'eventcontainer_'+color_number;
		this.color_calendar_now.style.backgroundColor = color;
		this.calendar_id.value = calendar_id;
		CleanNode(this.calen_sal);
		var calendarNameTxt = document.createTextNode(calendar_name);
		this.calen_sal.appendChild(calendarNameTxt);
		this.calen_sal.parentNode.title = calendar_name;

		setMaskHeight(this.event_window);

		this.event_form.style.display = 'block';
		this.event_window.style.display = 'block';
		this.RecalcEventWindowPosition();
	},
	/**
	 * Recalculate size of shadow for edit event window.
	 *
	 * @return void
	 */
	RecalcShadowSize: function() {
		var height = "auto";
		var width = "auto";
		if ((this.eventcontainer.clientHeight - 4) > 0) {
			height = (this.eventcontainer.clientHeight - 4) + "px";
		}
		if ((this.eventcontainer.clientWidth - 2) > 0) {
			width = (this.eventcontainer.clientWidth - 2) + "px";
		}
		this.shadow_middle.style.height = height;
		this.shadow_middle.style.width = width;
	},
	/**
	 * Recalculate position of edit event window and change shadow size.
	 *
	 * @param (int) yOffset - top offset for edit event window
	 * @param (int) xOffset - left offset for edit event window
	 *
	 * @return void
	 */
	RecalcEventWindowPosition : function(xOffset, yOffset) {
		var width, height;
		if (typeof(xOffset) == "undefined") xOffset = 0;
		if (typeof(yOffset) == "undefined") yOffset = 0;
		width = (typeof(window.innerWidth) == "undefined" ? document.documentElement.clientWidth : window.innerWidth );
		height = (typeof(window.innerHeight) == "undefined" ? document.documentElement.clientHeight : window.innerHeight );
		if(SafariDetect().isSafari) {
			this.event_form.style.top = (window.innerHeight / 2 + xOffset) + 'px';
		}
		else {
			this.event_form.style.top = (height/2 - this.event_form.offsetHeight/2 + xOffset) + "px";
		}
		this.event_form.style.left = (width/2 - this.eventEdit.offsetWidth/2 + yOffset) + "px";
		this.RecalcShadowSize();
	}
};
