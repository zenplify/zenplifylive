/**
 * initialize
 * RecalcShadowHeight
 * FillRepeatForm
 * CalcSelectBoxes
 * CreateResultText
 * CreateEventForm
 * CancelEventForm
 * ClearEventForm
 * ShowHideCalendarsList
 * CheckTimeStr
 * GetTimeObjectParts
 * SaveFormOnEnter
 * PrepareToSubmit
 * GetRepeatEventData
 * UndoOneRepeat
 * PrepareToDelete
 * CreateCalendarSelect
 * RedrawCalendarSelect
 * RemoveCalendarSelect
 * MarkCalendarSelectOption
 * ShowEventForm
 * ExpandAllday
 */
function CEventForm() {
	this.event_window				= $id('edit_window');
	this.event_form					= $id('edit_form');
	this.evform_id					= $id('evform_id');
	this.id_calendar				= $id('id_calendar');
	this.eventcontainer				= $id('eventcontainer');
	this.toolbar_new_event			= $id('toolbar_new_event');
	this.window_header				= $id('ef_fulldate');
	this.EventSubject				= $id('EventSubject');
	this.EventLocation				= $id('EventLocation');
	this.edit_select_box			= $id('edit_select_box');
	this.color_calendar_now			= $id('color_calendar_now');
	this.calen_sal					= $id('calen_sal');
	this.calendar_arrow				= $id('calendar_arrow');
	this.edit_select_box_list       = $id('edit_select_box_list');
	this.calendar_options           = [];
	this.start_month                = $id("st_currentMonth");
	this.end_month                  = $id("en_currentMonth");
	this.EventTimeFrom				= $id('EventTimeFrom');
	this.EventDateFrom				= $id('EventDateFrom');
	this.EventTimeTill				= $id('EventTimeTill');
	this.EventDateTill				= $id('EventDateTill');
	this.EventDescription			= $id('EventDescription');
	this.repeatExclusionWarning		= $id('repeatExclusionWarning');
	this.remindCont					= $id('remindCont');
	this.shadow_middle              = $id('edit_event_shadow');
	this.shadow_parent              = $id('event_black');

	//repeat_data
	this.expandRepeatLinkCont       = $id('expandRepeatLinkCont');
	this.expandRepeatLink			= $id('expandRepeatLink');
	this.closeRepeatForm			= $id('closeRepeatForm');
	this.repeatCont					= $id('repeatCont');
	this.repeatCont.style.display   = "none";
	this.allow_repeat				= false;

	this.repeatOpt					= $id('repeatOpt');
	this.repeatOrder                = $id('repeatOrder');
	this.repeatOrderCont            = $id('repeatOrderCont');
	this.repeatIntervalText         = $id('repeatIntervalText');
	this.repeatResult               = $id('repeatResult');


	this.advancedRepeatCont1		= $id('advancedRepeatCont1');
	this.advanced_repeat1			= $id('advanced_repeat1');
	this.advanced_repeat_arrow1		= $id('advanced_repeat_arrow1');
	this.advanced_list1             = $id('advanced_list1');
	this.repeatMonthOpt0			= $id('repeatMonthOpt0');
	this.repeatMonthOpt1			= $id('repeatMonthOpt1');
	this.repeatOnDay				= $id('repeatOnDay');
	this.repeatWeekDays				= $id('repeatWeekDays');
	this.yearDate					= $id('yearDate');
	this.yearDate1					= $id('yearDate1');
	this.weekOrderSelect			= $id('weekOrderSelect');
	this.weekOrderSelect1			= $id('weekOrderSelect1');
	this.weekdaysRepeatSel			= $id('weekdaysRepeatSel');
	this.weekdaysRepeatSel1			= $id('weekdaysRepeatSel1');
	this.repeatMonthYearType        = 0;


	this.advanced_repeat2			= $id('advanced_repeat2');
	this.advanced_list2				= $id('advanced_list2');
	this.advanced_repeat_arrow2		= $id('advanced_repeat_arrow2');
	this.advanced_repeat_text21		= $id('advanced_repeat_text21');
	this.advanced_repeat_text22		= $id('advanced_repeat_text22');
	this.advanced_repeat_text23		= $id('advanced_repeat_text23');
	this.repeatTimes				= $id('repeatTimes');
	this.repeatUntil				= $id('repeatUntil');
	this.repeatExpiresCont			= $id('repeatExpiresCont');
	this.repeatExpire0				= $id('repeatExpire0');
	this.repeatExpire1				= $id('repeatExpire1');
	this.repeatExpire2				= $id('repeatExpire2');
	this.repeatEnd					= 0;

	this.allday_container			= $id('allday_container');
	this.alldayCbk					= $id('alldayCbk');

	this.weekdays = [];
	for (var i=0; i<7; i++) {
		this.weekdays[i] = $id('weekDay['+i+']');
	}
	this.weekdaysRepeatChbk			= $id('weekdaysRepeatChbk');

	this.repeat_choose				= $id('repeat_choose');
	this.form_data					= null;
	this.undoOneRepeat				= $id('undoOneRepeat');
	this.confirm_window				= $id('confirm_window');

	this.closebut					= $id('closebut');
	this.savebut					= $id('button_save');
	this.cancelbut					= $id('cancelbut');
	this.delbut						= $id('delbut');

	this.eventEdit					= $id("eventEdit");
	this.eventContainer				= $id("eventcontainer");

	this.appointmentGuestsRightsCont	= $id("appointmentGuestsRightsCont");

	if (setcache.allowreminders) Reminder = new CReminder();
	Appointment = new CAppointment();

	this.initialize();
}
CEventForm.prototype = {
	initialize: function() {
		calendarRepeatUntil = new CCalendarTableEvent('calRepeatUntil');
		this.CreateCalendarSelect();
		var obj = this;

		this.toolbar_new_event.onclick = function() {
			if (null !== CheckAssociativeArrayIsEmpty(mycache.calendars)) {
				$id('calendarColorNumber').value = 0;
				obj.CreateEventForm();
			}
		};
		this.toolbar_new_event.onmouseover = function() {
			if(null !== CheckAssociativeArrayIsEmpty(mycache.calendars)) this.className = "wm_toolbar_item_over";
		};
		this.toolbar_new_event.onmouseout = function() {
			if(null !== CheckAssociativeArrayIsEmpty(mycache.calendars)) this.className = "wm_toolbar_item";
		};
		this.EventSubject.onkeypress = function(e) {
			obj.SaveFormOnEnter(e);
		};
		this.EventDescription.onfocus = function() {
			calendarTableStart.Hide();
			calendarTableEnd.Hide();
		};
		this.EventDateTill.onfocus = function() {
			calendarTableStart.Hide();
			calendarTableEnd.Show();
			if (timeSelectorTill != null) {
				timeSelectorTill.Hide();
			};
			if (timeSelectorFrom != null) {
				timeSelectorFrom.Hide();
			}
		};
		this.EventDateFrom.onfocus = function() {
			calendarTableStart.Show();
			calendarTableEnd.Hide();
			if (timeSelectorTill != null) {
				timeSelectorTill.Hide();
			};
			if (timeSelectorFrom != null) {
				timeSelectorFrom.Hide();
			}
		};
		this.alldayCbk.onclick = function() {
			obj.ExpandAllday(this.checked);
		};
		this.closebut.onclick = function() {
			obj.CancelEventForm();
		};
		this.savebut.onclick = function() {
			obj.PrepareToSubmit();
		};
		this.cancelbut.onclick = function() {
			obj.CancelEventForm();
		};
		this.delbut.onclick = function() {
			obj.PrepareToDelete();
		};

		this.expandRepeatLink.onclick = function() {
			obj.expandRepeatLinkCont.style.display = "none";
			obj.repeatCont.style.display = "block";
			obj.CalcSelectBoxes();
			obj.allow_repeat = true;
			obj.RecalcShadowHeight();
		};
		this.closeRepeatForm.onclick = function() {
			obj.expandRepeatLinkCont.style.display = "block";
			obj.repeatCont.style.display = "none";
			obj.allow_repeat = false;
			obj.event_form.style.top = (obj.event_window.clientHeight/2 - obj.event_form.offsetHeight/2) + "px";
			obj.RecalcShadowHeight();
		};
		this.repeatOpt.onchange = function() {
			obj.FillRepeatForm(this.value);
			obj.CreateResultText();
			obj.RecalcShadowHeight();
		};
		this.advanced_repeat1.onclick = function() {
			if (obj.advanced_list1.style.display == "none") {
				obj.advanced_list1.style.display = "block";
			} else {
				obj.advanced_list1.style.display = "none";
			}
		};
		this.advanced_repeat_arrow1.onclick = function() {
			if (obj.advanced_list1.style.display == "none") {
				obj.advanced_list1.style.display = "block";
			} else {
				obj.advanced_list1.style.display = "none";
			}
		};
		this.advanced_repeat2.onclick = function() {
			if (obj.advanced_list2.style.display === "none") {
				obj.advanced_list2.style.display = "block";
			} else {
				obj.advanced_list2.style.display = "none";
			}
		};
		this.advanced_repeat_arrow2.onclick = function() {
			if (obj.advanced_list2.style.display == "none") {
				obj.advanced_list2.style.display = "block";
			} else {
				obj.advanced_list2.style.display = "none";
			}
		};
		this.repeatMonthOpt0.onmouseover = function() {
			this.className = "select_box_opt_active";
		};
		this.repeatMonthOpt0.onmouseout = function() {
			this.className = "select_box_opt";
		};
		this.repeatMonthOpt0.onclick = function() {
			obj.repeatOnDay.style.display = "block";
			obj.repeatWeekDays.style.display = "none";
			obj.repeatMonthYearType = 0;
			obj.CreateResultText();
		};
		this.repeatMonthOpt1.onmouseover = function() {
			this.className = "select_box_opt_active";
		};
		this.repeatMonthOpt1.onmouseout = function() {
			this.className = "select_box_opt";
		};
		this.repeatMonthOpt1.onclick = function() {
			obj.repeatOnDay.style.display = "none";
			obj.repeatWeekDays.style.display = "block";
			obj.repeatMonthYearType = 1;
			obj.CreateResultText();
		};
		this.repeatExpire0.onmouseover = function() {
			this.className = "select_box_opt_active";
		};
		this.repeatExpire0.onmouseout = function() {
			this.className = "select_box_opt";
		};
		this.repeatExpire0.onclick = function() {
			obj.advanced_repeat_text21.style.display = "block";
			obj.advanced_repeat_text22.style.display = "none";
			obj.advanced_repeat_text23.style.display = "none";
			obj.repeatEnd = 0;
			obj.CreateResultText();
		};
		this.repeatExpire1.onmouseover = function(e) {
			this.className = "select_box_opt_active";
			StopEvents(e);
		};
		this.repeatExpire1.onmouseout = function(e) {
			this.className = "select_box_opt";
			StopEvents(e);
		};
		this.repeatExpire1.onclick = function() {
			obj.advanced_repeat_text21.style.display = "none";
			obj.advanced_repeat_text22.style.display = "block";
			obj.advanced_repeat_text23.style.display = "none";
			obj.repeatEnd = 1;
			obj.CreateResultText();
		};
		this.repeatExpire2.onmouseover = function() {
			this.className = "select_box_opt_active";
		};
		this.repeatExpire2.onmouseout = function() {
			this.className = "select_box_opt";
		};
		this.repeatExpire2.onclick = function() {
			obj.advanced_repeat_text21.style.display = "none";
			obj.advanced_repeat_text22.style.display = "none";
			obj.advanced_repeat_text23.style.display = "block";
			obj.repeatEnd = 2;
			obj.CreateResultText();
		};
		this.repeatOrder.onchange = function() {
			obj.CreateResultText();
		};
		this.repeatTimes.onblur = function() {
			obj.CreateResultText();
		};
		this.repeatUntil.onfocus = function() {
			var field_value = ConvertFromStrToDate(this.value);
			if (field_value != null) {
				calendarRepeatUntil.Fill(field_value.getDate(), (field_value.getMonth()+1), field_value.getFullYear());
			} else {
				calendarRepeatUntil.Fill();
			};
			calendarRepeatUntil.Show();
		};
		calendarRepeatUntil.onpickdate = function(date) {
			obj.repeatUntil.value = ConvertFromDateToStr(date);
			obj.CreateResultText();
		};
		this.undoOneRepeat.onclick = function() {
			obj.event_window.style.display = "none";
			obj.UndoOneRepeat();
		};
		for (var i=0, cbk = this.weekdays; i<cbk.length; i++) {
			cbk[i].onclick = function() {
				obj.CreateResultText();
			}
		}
		this.repeatTimes.onkeyup = ReplaceNonDigits;
		this.repeatTimes.onclick = ReplaceNonDigits;
	},
	/**
	 * Recalculate height of shadow for edit event window.
	 * (it is out-of-date function, use function RecalcShadowSize)
	 *
	 * @return void
	 */
	RecalcShadowHeight: function() {
		var height = "auto";
		if ((this.eventcontainer.clientHeight - 4) > 0) {
			height = (this.eventcontainer.clientHeight - 4) + "px";
		}
		this.shadow_middle.style.height = height;
	},
	/**
	 * Recalculate size of shadow for edit event window.
	 *
	 * @return void
	 */
	RecalcShadowSize: function() {
		var height = "auto";
		var width = "auto";
		if ((this.eventcontainer.clientHeight - 4) > 0) {
			height = (this.eventcontainer.clientHeight - 4) + "px";
		}
		if ((this.eventcontainer.clientWidth - 2) > 0) {
			width = (this.eventcontainer.clientWidth) + "px";
		}
		this.shadow_middle.style.height = height;
		this.shadow_parent.style.width = width;
	},
	/**
	 * Recalculate position of edit event window and change shadow size.
	 *
	 * @param {int} xOffset - left offset for edit event window
	 * @param {int} yOffset - top offset for edit event window
	 *
	 * @return void
	 */
	RecalcEventWindowPosition : function(xOffset, yOffset) {
		var width, height;
		if (typeof(xOffset) == "undefined") xOffset = 0;
		if (typeof(yOffset) == "undefined") yOffset = 0;
		width = (typeof(window.innerWidth) == "undefined" ? document.documentElement.clientWidth : window.innerWidth );
		height = (typeof(window.innerHeight) == "undefined" ? document.documentElement.clientHeight : window.innerHeight );
		this.event_form.style.top = (height/2 - this.event_form.offsetHeight/2 + xOffset) + "px";
		this.event_form.style.left = (width/2 - this.eventEdit.offsetWidth/2 + yOffset) + "px";
		this.RecalcShadowSize();
	},
	/**
	 * Change size of edit event window and change shadow size.
	 *
	 * @param {int} width - new width for edit event window
	 * @param {int} height - new height for edit event window
	 *
	 * @return void
	 */
	ChangeEventWindowSize : function(width, height) {
		if (typeof(width) != "undefined") {
			this.eventEdit.style.width = width + "px";
			this.eventContainer.style.width = this.eventEdit.style.width;
		}
		if (typeof(height) != "undefined") {
			this.eventEdit.style.height = height + "px";
			this.eventContainer.style.height = this.eventEdit.style.height;
		}
		this.RecalcShadowSize();
	},
	FillRepeatForm: function(optionValue) {
		var period = 0, date_from, event_id_data, event_id, repeat_data, weekDays;
		date_from = ConvertFromStrToDate(this.EventDateFrom.value);
		calendarRepeatUntil.Hide();

		event_id_data = SplitEventId(this.evform_id.value);
		event_id = event_id_data.id;
		if (event_id == undefined) return;

		repeat_data = mycache.ids[event_id];
		if ((repeat_data != undefined && repeat_data.event_repeats == 1
			&& repeat_data.repeat_end != undefined && repeat_data.repeat_end != "")
			|| optionValue != undefined) {
			this.expandRepeatLinkCont.style.display = "none";
			this.repeatCont.style.display = (mycache.exclusions[event_id_data.original] == undefined) ? "block" : "none";
			this.allow_repeat = true;
		} else {
			this.expandRepeatLinkCont.style.display = "block";
			this.repeatCont.style.display = "none";
			this.allow_repeat = false;
		}
		
		if (repeat_data != undefined && repeat_data.appointment)
		{
			this.expandRepeatLinkCont.style.display = "none";
			this.calen_sal.onclick = function() {};
		}

		if (optionValue != undefined || event_id == 0 || repeat_data == undefined
			|| repeat_data.event_repeats == 0 || repeat_data.repeat_end == undefined) {
			period = (optionValue != undefined) ? optionValue : 0;
			repeat_data = {
				repeat_period: period,
				repeat_order: 1,
				repeat_num: 2,
				repeat_until: null,
				week_number: null,
				repeat_end: 0
			};
			weekDays = new Array(0, 0, 0, 0, 0, 0, 0);
			var this_day = date_from.getDay();
			weekDays[this_day] = 1;
		} else {
			repeat_data.week_number = (repeat_data.week_number != null) ? ParseToNumber(repeat_data.week_number) : null;
			weekDays = new Array(repeat_data.sun, repeat_data.mon, repeat_data.tue, repeat_data.wed, repeat_data.thu, repeat_data.fri, repeat_data.sat);
			period = ParseToNumber(repeat_data.repeat_period);
		}

		this.weekdaysRepeatChbk.style.display = "none";
		this.repeatOrderCont.style.display = "block";
		this.repeatExpiresCont.style.display = "block";
		this.advanced_repeat2.style.display = "block";
		if (repeat_data.repeat_order != undefined && repeat_data.repeat_order != "") {
			this.repeatOrder.options[repeat_data.repeat_order-1].selected = true;
		}

		if (period == DAY || period == WEEK) { //day, week
			this.advancedRepeatCont1.style.display = "none";
			if (period == DAY) {//day
				this.repeatIntervalText.innerHTML = Lang.RepeatDescDay;
			} else if (period == WEEK) {//week
				this.repeatIntervalText.innerHTML = Lang.RepeatDescWeek;
				this.weekdaysRepeatChbk.style.display = "block";
				for (var i=0, cbk=this.weekdays; i<cbk.length; i++) {
					cbk[i].checked = (weekDays[i] == 1) ? true : false;
				}
			}
		}

		if (period == MONTH || period == YEAR) {//month, year
			var weekDayNumber, yearDate = " ", monthDate;
			this.advancedRepeatCont1.style.display = "block";
			if (period == MONTH) {//month
				this.repeatIntervalText.innerHTML = Lang.RepeatDescMonth;
				monthDate = date_from.getDate() + " " + Lang.RepeatDescDay;
			} else if (period == YEAR) {//year
				this.repeatIntervalText.innerHTML = Lang.RepeatDescYear;
				monthDate = date_from.getDate() + " " +GetMonthName(date_from.getMonth());
				yearDate = GetMonthName(date_from.getMonth());
			}
			for (weekDayNumber = 0; weekDayNumber < weekDays.length; weekDayNumber++) {
				if (weekDays[weekDayNumber] == 1) {
					break;
				}
			}
			if (weekDayNumber == 7) weekDayNumber = 0;

			if (repeat_data.week_number == null || repeat_data.week_number == "") {
				var week_num = 0, offset;
				this.repeatOnDay.innerHTML = Lang.Every+ "&nbsp;" + monthDate;
				this.repeatOnDay.style.display = "block";
				this.repeatWeekDays.style.display = "none";
				this.repeatMonthYearType = 0;
				var monthFirstDay = new Date(date_from.getFullYear(), date_from.getMonth(), 1);
				if (monthFirstDay.getDay() <= date_from.getDay()) {
					offset = date_from.getDay() - monthFirstDay.getDay();
				} else {
					offset = 7 - (monthFirstDay.getDay() - date_from.getDay());
				}
				monthFirstDay.setDate(monthFirstDay.getDate() + offset);

				while (monthFirstDay<date_from) {
					monthFirstDay.setDate(monthFirstDay.getDate() + 7);
					week_num++;
				}
				this.weekOrderSelect.options[week_num].selected = true;
				this.weekOrderSelect1.options[week_num].selected = true;
			} else {
				this.weekOrderSelect.options[repeat_data.week_number].selected = true;
				this.weekOrderSelect1.options[repeat_data.week_number].selected = true;
				this.repeatOnDay.style.display = "none";
				this.repeatWeekDays.style.display = "block";
				this.repeatMonthYearType = 1;
			}
			this.weekdaysRepeatSel.options[weekDayNumber].selected = true;
			this.weekdaysRepeatSel1.options[weekDayNumber].selected = true;
			this.repeatMonthOpt0.innerHTML = Lang.Every+ "&nbsp;" + monthDate;
			this.yearDate.innerHTML = yearDate;
			this.yearDate1.innerHTML = yearDate;
			this.yearDate1.style.display = "block";
		}

		if (repeat_data.repeat_end == 1 || this.repeatEnd == 1) {
			this.repeatUntil.value = "";
			if (repeat_data.repeat_end == 1
				&& typeof(repeat_data.repeat_num) != "undefined") {
				this.repeatTimes.value = repeat_data.repeat_num;
			} else if (typeof(this.repeatTimes.value) == "undefined") {
				this.repeatTimes.value = 2;
			}
			this.advanced_repeat_text21.style.display = "none";
			this.advanced_repeat_text22.style.display = "block";
			this.advanced_repeat_text23.style.display = "none";
			this.repeatEnd = 1;
		} else if (repeat_data.repeat_end == 2 || this.repeatEnd == 2) {
			this.repeatTimes.value = 2;
			if (repeat_data.repeat_until != null) {
				this.repeatUntil.value = ConvertFromDateToStr(repeat_data.repeat_until);
			}
			this.advanced_repeat_text21.style.display = "none";
			this.advanced_repeat_text22.style.display = "none";
			this.advanced_repeat_text23.style.display = "block";
			this.repeatEnd = 2;
		} else {
			this.repeatUntil.value = "";
			this.repeatTimes.value = 2;
			this.advanced_repeat_text21.style.display = "block";
			this.advanced_repeat_text22.style.display = "none";
			this.advanced_repeat_text23.style.display = "none";
			this.advanced_repeat_text21.innerHTML = Lang.NoEndRepeatEvent;
			this.repeatEnd = 0;
		}
		this.repeatOpt.options[period].selected = true;
		this.CalcSelectBoxes();
		this.CreateResultText();
	},
	CalcSelectBoxes: function() {
		var list1, list2;
		list1 = this.advanced_list1;
		list2 = this.advanced_list2;
		list2.style.display = "block";
		this.advanced_repeat2.style.width = list2.offsetWidth + "px";
		list2.style.display = "none";
		list1.style.display = "block";
		this.advanced_repeat1.style.width = list1.offsetWidth  + "px";
		list1.style.display = "none";
		this.RecalcEventWindowPosition();
	},
	CreateResultText: function() {
		var str = "", date_from = ConvertFromStrToDate(this.EventDateFrom.value);
		var weekdays_arr = new Array(Lang.FullDaySunday, Lang.FullDayMonday, Lang.FullDayTuesday, Lang.FullDayWednesday, Lang.FullDayThursday, Lang.FullDayFriday, Lang.FullDaySaturday);

		if (this.repeatOpt.value == DAY) {
			if (this.repeatOrder.value == 0) {
				if (this.repeatEnd == 1) {
					str += Lang.RepeatEveryDayTimes;
				} else if (this.repeatEnd == 2) {
					str += Lang.RepeatEveryDayUntil;
				} else {
					str += Lang.RepeatEveryDayInfin;
				}
			} else {
				if (this.repeatEnd == 1) {
					str += Lang.RepeatDaysTimes;
				} else if (this.repeatEnd == 2) {
					str += Lang.RepeatDaysUntil;
				} else {
					str += Lang.RepeatDaysInfin;
				}
			}
		} else if (this.repeatOpt.value == WEEK) {
			var checked_days = [];
			for (var i=0, cbk=this.weekdays; i<cbk.length; i++) {
				if (cbk[i].checked)  checked_days.push(weekdays_arr[cbk[i].value]);
			}
			if (this.repeatOrder.value == 0) {
				if (this.repeatEnd == 1) {
					str += Lang.RepeatEveryWeekTimes;
				} else if (this.repeatEnd == 2) {
					str += Lang.RepeatEveryWeekUntil;
				} else {
					str += Lang.RepeatEveryWeekInfin;
				}
			} else {
				if (this.repeatEnd == 1) {
					str += Lang.RepeatWeeksTimes;
				} else if (this.repeatEnd == 2) {
					str += Lang.RepeatWeeksUntil;
				} else {
					str += Lang.RepeatWeeksInfin;
				}
			}
			str = str.replace('%DAYS%', checked_days.join(', '));
		} else if (this.repeatOpt.value == MONTH || this.repeatOpt.value == YEAR) {
			if (this.repeatMonthYearType == 0) {
				var date_str;
				if (this.repeatOpt.value == MONTH) {
					if (this.repeatOrder.value == 0) {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatEveryMonthDateTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatEveryMonthDateUntil;
						} else {
							str += Lang.RepeatEveryMonthDateInfin;
						}
					} else {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatMonthsDateTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatMonthsDateUntil;
						} else {
							str += Lang.RepeatMonthsDateInfin;
						}
					}
					date_str = date_from.getDate();
				} else {//year
					if (this.repeatOrder.value == 0) {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatEveryYearDateTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatEveryYearDateUntil;
						} else {
							str += Lang.RepeatEveryYearDateInfin;
						}
					} else {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatYearsDateTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatYearsDateUntil;
						} else {
							str += Lang.RepeatYearsDateInfin;
						}
					}
					date_str = date_from.getDate()+" "+GetMonthName(date_from.getMonth());
				}
				str = str.replace('%DATE%', date_str);
			} else {
				if (this.repeatOpt.value == MONTH) {
					if (this.repeatOrder.value == 0) {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatEveryMonthWDTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatEveryMonthWDUntil;
						} else {
							str += Lang.RepeatEveryMonthWDInfin;
						}
					} else {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatMonthsWDTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatMonthsWDUntil;
						} else {
							str += Lang.RepeatMonthsWDInfin;
						}
					}
					str = str.replace('%DAY%', weekdays_arr[this.weekdaysRepeatSel.value]);
				} else {//year
					if (this.repeatOrder.value == 0) {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatEveryYearWDTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatEveryYearWDUntil;
						} else {
							str += Lang.RepeatEveryYearWDInfin;
						}
					} else {
						if (this.repeatEnd == 1) {
							str += Lang.RepeatYearsWDTimes;
						} else if (this.repeatEnd == 2) {
							str += Lang.RepeatYearsWDUntil;
						} else {
							str += Lang.RepeatYearsWDInfin;
						}
					}
					str = str.replace('%DAY%', (weekdays_arr[this.weekdaysRepeatSel.value] + " " + GetMonthName(date_from.getMonth())));
				}

				var weekOrder = ParseToNumber(this.weekOrderSelect.value);
				var weekorder_arr = new Array(Lang.First, Lang.Second, Lang.Third, Lang.Fourth, Lang.Last);
				str = str.replace('%NUMBER%', weekorder_arr[weekOrder]);
			}
		}

		str = str.replace('%PERIOD%', (Number(this.repeatOrder.value)+1));
		if (this.repeatEnd == 1) {
			str = str.replace('%TIMES%', this.repeatTimes.value);
		} else if (this.repeatEnd == 2) {
			str = str.replace('%UNTIL%', this.repeatUntil.value);
		}
		this.repeatResult.innerHTML = str;
	},
	CreateEventForm: function(idc) {
		idc = (idc == null) ? 0 : idc;
		var date = GetCurrentTime();
		var editevent_form_data = {
			subject 	: '',
			dateFrom	: date.from,
			dateTill	: date.till,
			description	: '',
			location	: '',
			event_id	: 0, //0  if new event
			calendar_id : idc,
			excluded	: false,
			allday      : 0
		};
		this.ShowEventForm(editevent_form_data);
	},
	CancelEventForm: function() {
		this.repeatCont.style.display = "none";
		this.event_window.style.display ="none";
		this.ClearEventForm();
	},
	ClearEventForm: function() {
		calendarTableStart.Hide();
		calendarTableEnd.Hide();
		timeSelectorTill.Hide();
		timeSelectorFrom.Hide();
		this.event_form.style.display = 'none';
		Selection.DeleteSelection();
	},
	ShowHideCalendarsList: function(){
		var selList = this.edit_select_box_list;
		if(typeof(selList.opened) == "undefined") selList.opened = false;
		if (selList.opened == false || selList.style.display=='none') {
			selList.style.display = 'block';
			selList.style.visibility = 'visible';
			selList.opened = true;
			selList.style.width = "auto";

			if (FireFoxDetect()) {
				selList.style.width = (selList.clientWidth>=109)?('auto'):("113px");
			} else {
				selList.style.width = (selList.clientWidth>=109)?((selList.clientWidth > 290) ? ((selList.clientWidth + 17) + 'px') : 'auto'):("112px");
			}
		} else {
			selList.style.display = 'none';
			selList.style.visibility = 'hidden';
			selList.opened = false;
		}
	},
	CheckTimeStr : function(time)
	{
		var pattern, arr, timeObj = {};
		if (setcache.timeformat == 1){
			pattern = /^(\d{1,2}):?(\d{2})?\s+(AM|PM)$/;
		} else {
			pattern = /^(\d{1,2}):(\d{2})$/;
		}

		arr =  pattern.exec(time);
		if (arr != null) {
			timeObj.time = arr[0];
			timeObj.hours = Number(arr[1]);
			timeObj.minutes = (arr[2]!=undefined && arr[2]!="") ? Number(arr[2]) : 0;
			timeObj.timeInterval = (arr[3]!="") ? arr[3] : null;
		} else {
			return null;
		}
		return timeObj;
	},

	GetTimeObjectParts : function(timeObj, endTime) {
		var hours = 0;
		hours = timeObj.hours;
		if (setcache.timeformat == 1) { // AM-PM
			if (timeObj.timeInterval == "AM") { // AM
				if (timeObj.hours == 12) {
					hours = (endTime) ? 24 : 0;
				}
			} else  { // PM
				hours += (timeObj.hours != 12 ? 12 : 0)
			}
		}
		return {
			hours:hours,
			minutes:timeObj.minutes
		}
	},
	SaveFormOnEnter: function(ev) {
		ev = ev || window.event;
		if(ev.keyCode == 13) this.PrepareToSubmit();
	},
	PrepareToSubmit: function() {
		if (this.calen_sal.value == '') {
			alert(Lang.ErrorCalendarNotCreated);
			return;
		}

		// Check appointed guests emails
		if (Appointment.active && !Appointment.parseNewAppointments()) return;

		this.EventSubject.value		= Trim(this.EventSubject.value);
		this.EventLocation.value		= Trim(this.EventLocation.value);
		this.EventDescription.value	= Trim(this.EventDescription.value);
		if (typeof this.EventSubject === 'object' && this.EventSubject.value == '') {
			alert(Lang.WarningSubjectBlank);
			if (!this.EventSubject.disabled) {
				this.EventSubject.focus();
			}
			return;
		}

		var strTimeFrom, strTimeTill, strDateFrom, strDateTill, eventId;
		var timeFrom, timeTill, dateFrom, dateTill;
		strTimeFrom = this.EventTimeFrom.value;
		strTimeTill = this.EventTimeTill.value;
		strDateFrom = this.EventDateFrom.value;
		strDateTill = this.EventDateTill.value;

		eventId = SplitEventId(this.evform_id.value);

		timeFrom = this.CheckTimeStr(strTimeFrom);
		timeTill = this.CheckTimeStr(strTimeTill);
		if (timeFrom == null || timeTill == null) {
			alert(Lang.WarningIncorrectTime);
			return;
		} else {
			var tmpTimeFromMin = timeFrom.minutes;
			var tmpTimeTillMin = timeTill.minutes;
			if (timeFrom.timeInterval == null) { //timeformat = 2
				if (timeFrom.hours>24 || (tmpTimeFromMin > 59 &&  tmpTimeFromMin < 0)) {
					alert(Lang.WarningIncorrectFromTime);
					return;
				}
			} else  { //timeformat = 1
				if (timeFrom.hours>12 ||
					(tmpTimeFromMin >59 && tmpTimeFromMin < 0
						&& timeFrom.minutes!=null)) {
					alert(Lang.WarningIncorrectFromTime);
					return;
				}
			}
			if (timeTill.timeInterval == null) { //timeformat = 2
				if (timeTill.hours>24 || (tmpTimeTillMin > 59 &&  tmpTimeTillMin < 0)) {
					alert(Lang.WarningIncorrectTillTime);
					return;
				}
			} else  { //timeformat = 1
				if (timeTill.hours>12 ||
					(tmpTimeTillMin >59 && tmpTimeTillMin < 0 && timeTill.minutes!=null)) {
					alert(Lang.WarningIncorrectTillTime);
					return;
				}
			}
		}

		dateFrom = ConvertFromStrToDate(strDateFrom);
		dateTill = ConvertFromStrToDate(strDateTill);

		var tf = this.GetTimeObjectParts(timeFrom, false);
		var tt = this.GetTimeObjectParts(timeTill, false);

		if (dateFrom == null || dateTill == null) {
			alert(Lang.WarningIncorrectDate);
			return;
		} else {
			if (dateFrom.getTime() > dateTill.getTime()) {
				alert(Lang.WarningStartEndDate);
				return;
			} else {
				if (dateFrom.getTime() == dateTill.getTime()
					&& this.alldayCbk.checked == false) {
					if ((tf.hours*60 + tf.minutes) >= (tt.hours*60 + tt.minutes)) {
						alert(Lang.WarningStartEndTime);
						return;
					}
				}
			}
		}

		//check repeat untill
		if (this.allow_repeat && this.repeatEnd == 2) {
			if (Trim(this.repeatUntil.value) == "") {
				alert(Lang.WarningUntilDateBlank);
				return;
			}
			var repeat_until_data = ConvertFromStrToDate(Trim(this.repeatUntil.value));
			if (repeat_until_data == null) {
				alert(Lang.WarningWrongUntilDate);
				return;
			} else {
				if (repeat_until_data.getTime() < dateFrom.getTime()) {
					alert(Lang.WarningWrongUntilDate);
					return;
				}
			}
		}

		timeFrom.format = Fnum(tf.hours, 2) + ":" + Fnum(tf.minutes, 2);
		timeTill.format = Fnum(tt.hours, 2) + ":" + Fnum(tt.minutes, 2);
		var formated_time = {
			from:timeFrom,
			till:timeTill
		};

		var event_data = mycache.ids[eventId.id];
		this.form_data = this.GetRepeatEventData(formated_time);
		if (setcache.allowreminders) {
			var reminders = Reminder.getRemindersData();
			this.form_data.reminders_save = reminders.save;
			this.form_data.reminders_del = reminders.del;
		}

		if (Appointment.active) {
			var appointments = Appointment.getAppointmentsData();
			this.form_data.appointments_save = appointments.save;
			this.form_data.appointments_del = appointments.del;
			this.form_data.appointments_access = appointments.access;
		}

		this.repeatCont.style.display = "none";
		this.event_form.style.display = "none";
		this.event_window.style.display = "none";
		Selection.DeleteSelection();
		this.form_data.eventChanged = 1;
		if (mycache.exclusions[eventId.original]) {
			ChooseForm.Show('EDIT', this.form_data, 1);
		} else if (eventId.id == 0 || event_data == undefined) {
			ChooseForm.Show('EDIT', this.form_data, 0);
		} else {
			this.form_data.eventChanged = 0;
			if (event_data.calendar_id != this.form_data.id_calendar ||
				event_data.event_name != this.form_data.event_name ||
				event_data.event_text != this.form_data.event_text ||
				event_data.event_timefrom.getTime() != this.form_data.date_from.getTime() ||
				event_data.event_timetill.getTime() != this.form_data.date_till.getTime() ||
				event_data.event_allday != this.form_data.allday ||
				event_data.repeat_period != this.form_data.repeat_period ||
				event_data.repeat_order != this.form_data.repeat_order ||
				event_data.repeat_num != this.form_data.repeat_times ||
				event_data.week_number != this.form_data.week_number ||
				event_data.repeat_end != this.form_data.repeat_end ||
				(typeof(this.form_data.weekdays) != 'undefined' &&
					(event_data.sun != this.form_data.weekdays[0] ||
					event_data.mon != this.form_data.weekdays[1] ||
					event_data.tue != this.form_data.weekdays[2] ||
					event_data.wed != this.form_data.weekdays[3] ||
					event_data.thu != this.form_data.weekdays[4] ||
					event_data.fri != this.form_data.weekdays[5] ||
					event_data.sat != this.form_data.weekdays[6]))) {
				this.form_data.eventChanged = 1;
			}
			
			if (event_data.event_repeats == 0) {
				ChooseForm.Show('EDIT', this.form_data, 0);
			} else {
				ChooseForm.Show('EDIT', this.form_data);
			}
		}
	},
	GetRepeatEventData: function(time) {
		var id_event, subject, description, location, calendar_id, date_from, date_till, allday, allow_repeat, data;
		id_event = SplitEventId(this.evform_id.value);
		subject = encodeURIComponent(this.EventSubject.value.substr(0,50));
		location = encodeURIComponent(this.EventLocation.value.substr(0,50));
		description = encodeURIComponent(this.EventDescription.value.substr(0,255));
		calendar_id = this.id_calendar.value;
		date_from = ConvertFromStrToDate(this.EventDateFrom.value);
		date_till = ConvertFromStrToDate(this.EventDateTill.value);
		if (time == undefined) {
			time_from = "00:00";
			time_till = "00:30";
		} else {
			var hours1, hours2, min;
			hours1 = time.from.format.split(":");
			time.from.hours = hours1[0];
			time.from.minutes = hours1[1];
			hours2 = time.till.format.split(":");
			time.till.hours = hours2[0];
			time.till.minutes = hours2[1];
			min = (time.from.minutes != undefined) ? time.from.minutes : 0;
			date_from.setHours(ParseToNumber(time.from.hours), min);
			date_till = new Date(date_till.getFullYear(), date_till.getMonth(), date_till.getDate());
			min = (time.till.minutes != undefined) ? time.till.minutes : 0;
			date_till.setHours(time.till.hours, min);
			time_from = Fnum(date_from.getHours(),2) + ":" + Fnum(date_from.getMinutes(),2);
			time_till = Fnum(date_till.getHours(),2) + ":" + Fnum(date_till.getMinutes(),2);
		}

		allday = (this.alldayCbk.checked)? 1: 0;
		allow_repeat = (this.allow_repeat)? 1: 0;
		data = {
			id_data: id_event, /*object with id_original, id, repeat*/
			event_name: subject,
			event_text: description,
			event_location: location,
			id_calendar: calendar_id,
			date_from: date_from,
			date_till: date_till,
			time_from: time_from,
			time_till: time_till,
			allday: allday,
			allow_repeat: allow_repeat
		}
		if (this.repeatOpt.value < 0 || this.repeatOpt.value > 4) {//<DAY || >YEAR
			allow_repeat = 0;
		}

		if (allow_repeat != 0) {
			var week_number = null, repeat_period = this.repeatOpt.value, weekdays_arr = new Array(0, 0, 0, 0, 0, 0, 0);
			var i, repeat_order, repeat_times, repeat_until;
			if (repeat_period == WEEK) {//week
				for (i=0, cbk=this.weekdays; i<cbk.length; i++) {
					if (cbk[i].checked == true) weekdays_arr[i] = 1;
				}
			}
			if (repeat_period == MONTH || repeat_period == YEAR) {//month, year
				if (this.repeatMonthYearType == 1) {
					week_number = ParseToNumber(this.weekOrderSelect.value);
					if (week_number < 0 || week_number > 4) week_number = 0;
					weekdays_arr[this.weekdaysRepeatSel.value] = 1;
				}
			} else {
				week_number = null;
			}

			repeat_order = ParseToNumber(this.repeatOrder.value);
			if (repeat_order < 0) repeat_order = 1;
			else repeat_order++;

			repeat_times = ParseToNumber(this.repeatTimes.value);
			if (this.repeatEnd != 1) repeat_times = 0;
			else if (repeat_times <= 0) repeat_times = 1;

			repeat_until = (this.repeatEnd==2) ? ConvertFromStrToDate(this.repeatUntil.value) : null;

			data.repeat_end = this.repeatEnd;
			data.repeat_period = repeat_period;
			data.repeat_order = repeat_order;
			data.repeat_times = repeat_times;
			data.repeat_until = repeat_until;
			data.week_number = week_number;
			data.weekdays = weekdays_arr;
		}
		return data;
	},
	/*_________________SAVE_REPEAT_EVENT___________________*/
	UndoOneRepeat: function() {
		this.repeat_choose.style.display = 'none';
		this.confirm_window.style.display = 'block';
		if (confirm(Lang.ConfirmUndoOneRepeat)) {
			ShowInfo(Lang.InfoSaving);
			this.confirm_window.style.display = 'none';
			var id_event = SplitEventId(this.evform_id.value);
			var calendar_id = this.id_calendar.value;
			if (mycache.exclusions[id_event.original] != undefined) {
				old_allday_value = mycache.exclusions[id_event.original]['event_allday'];
			} else {
				old_allday_value = mycache.ids[id_event.id]['event_allday'];
			}

			var req = GetXMLHTTPRequest(), url = processing_url, requestParams;
			var event_data = mycache.ids[id_event.id]
			var event_time_from =  Fnum(event_data.event_timefrom.getHours(), 2) +
									':'+Fnum(event_data.event_timefrom.getMinutes(),2);
			requestParams = 'action=delete_exclusion' +
							'&event_id=' + id_event.id +
							'&calendar_id=' + calendar_id +
							'&id_repeat='+ id_event.repeat +
							'&id_recurrence='+ id_event.date +
							'&event_time_from=' + event_time_from +
							'&nocache=' + Math.random();
			if (req) {
				var repeat_event = sendRequest(req, url, requestParams);
				if (!ServErr(repeat_event,Lang.ErrorUpdateEvent)) {
					var dell_dates = DeleteRepeatEventPart(id_event.id, id_event.repeat, id_event.date);
					repeat_event.event.id_repeat = id_event.repeat;
					repeat_event.event.id_recurrence = id_event.date;
					var add_dates = AddOneRepeatEventToCache(repeat_event.event);
					var dates = AddToArray(dell_dates, add_dates);
					RenderScreens(dates);
					if (old_allday_value == 1 || repeat_event.event_allday == 1) //Grid.
						Grid.SetWorkAreaOffset();
				}
			}
			HideInfo();
		} else {
			this.confirm_window.style.display='none';
			return;
		}
		this.ClearEventForm();
	},
	/*_________________DELETE_REPEAT_EVENT___________________*/
	PrepareToDelete: function()
	{
		var id_event = SplitEventId(this.evform_id.value);
		var id_calendar = mycache.ids[id_event.id].calendar_id;

		var data = {
			id_data: id_event,
			id_calendar: id_calendar
		};
		if (mycache.exclusions[id_event.original] != undefined)
		{
			var ex = mycache.exclusions[id_event.original];
			data = {
				id_data: id_event,
				id_calendar: ex.calendar_id
			};
			ChooseForm.Show('DELETE', data, 1);
		}
		else if (mycache.repeats[id_event.original] != undefined)
		{
			ChooseForm.Show('DELETE', data);
		}
		else
		{
			var confirm_window = $id('confirm_window');
			confirm_window.style.display='block';
			if (confirm(Lang.ConfirmAreYouSure)) {
				confirm_window.style.display='none';
				ChooseForm.Show('DELETE', data, 0);
			} else {
				confirm_window.style.display='none';
				return;
			}
		}
		this.repeatCont.style.display = "none";
		this.event_form.style.display = "none";
		this.event_window.style.display = "none";
	},
	CreateCalendarSelect: function() {
		if (mycache.calendars!=undefined)
		{
			var obj = this, mas_cal = [], list = this.edit_select_box_list;
			var i, my_calendars_cont, shared_calendars_cont, cl, option,  div_color, option_txt, option_color;

			my_calendars_cont = document.createElement('div');
			my_calendars_cont.style.paddingBottom = "5px";
			list.appendChild(my_calendars_cont);
			shared_calendars_cont = document.createElement('div');
			shared_calendars_cont.style.borderTop = "1px solid #ccc";
			shared_calendars_cont.style.paddingTop = "5px";
			shared_calendars_cont.style.display = "none";
			list.appendChild(shared_calendars_cont);

			for (i in mycache.calendars)
			{
				cl = mycache.calendars[i];
				option = document.createElement('div');
				this.calendar_options[i] = option;
				option.id = 'clnd_'+ cl.calendar_id;
				option.style.position = 'relative';
				div_color = GetNumberOfColor(cl.color);

				option.onclick = function()
				{
					var new_calendar, form_calendar_id, name = obj.calen_sal, name_val, list = obj.edit_select_box_list;
					new_calendar = mycache.calendars[this.id.substr(5, this.id.length)];
					form_calendar_id = obj.id_calendar.value;
					name_val = HtmlEncode(new_calendar.name);
					name.innerHTML = name_val;
					name.parentNode.title = name_val + ' - ' + new_calendar.owner;
					obj.eventcontainer.className = 'eventcontainer_'+new_calendar.color;
					$id('clnd_' + form_calendar_id).style.fontWeight = 'normal';
					obj.id_calendar.value = new_calendar.calendar_id;
					$id('clnd_' + new_calendar.calendar_id).style.fontWeight = 'bold';
					list.style.display = 'none';
					list.style.visibility = 'hidden';
					obj.color_calendar_now.style.backgroundColor = GetNumberOfColor(new_calendar.color);
				};
				option.onmouseover = function(){
					this.style.backgroundColor = '#DDDDDD';
				};
				option.onmouseout = function(){
					this.style.backgroundColor = '#FFFFFF';
				};
				option_color = document.createElement("div");
				option_color.className = "color_pick";
				option_color.style.border = "1px solid #fff";
				option_color.style.margin = "1px 5px 0 3px";
				option_color.style.backgroundColor = div_color;
				option_txt = document.createTextNode(/*HtmlEncode(*/cl.name/*)*/);
				option.appendChild(option_color);
				option.appendChild(option_txt);
				mas_cal[i] = option;
				if (cl.sharing_level == null) {
					my_calendars_cont.appendChild(option);
				} else if (cl.sharing_level == FULL_CONTROL || cl.sharing_level == EDIT_EVENTS) {
					shared_calendars_cont.appendChild(option);
				}

			};
			if (shared_calendars_cont.childNodes.length >0) shared_calendars_cont.style.display = "block";
		}
	},
	RedrawCalendarSelect: function() {
		CleanNode(this.edit_select_box_list);
		this.CreateCalendarSelect();
	},
	RemoveCalendarSelect: function() {
		if(null !== CheckAssociativeArrayIsEmpty(mycache.calendars)) {
			var list = this.edit_select_box_list;
			list.style.display = 'none';
			list.style.visibility = 'hidden';
		}
	},
	MarkCalendarSelectOption: function() {
		var id_calendar;
		for (id_calendar in this.calendar_options) {
			if (this.id_calendar.value == id_calendar) {
				this.calendar_options[id_calendar].style.fontWeight = 'bold';
			} else {
				this.calendar_options[id_calendar].style.fontWeight = 'normal';
			};
		}
	},
	ShowEventForm: function (event_data)
	{
		timeSelectorFrom.Fill(event_data.dateFrom);
		timeSelectorTill.Fill(event_data.dateTill);
		this.repeatEnd = 0;
		if (null === CheckAssociativeArrayIsEmpty(mycache.calendars)) return;
		this.form_data = null;
		var i, calendar, color, color_number, name, calendar_id, obj = this, event_id, calendarNameTxt, tbl_start, tbl_end, sharing_level;
		var eventFormIdValue = this.evform_id.value, id_calendar = this.id_calendar.value;

		this.RemoveCalendarSelect();
		event_id = SplitEventId(event_data.event_id);

		if (setcache.allowreminders) Reminder.setNewReminder(event_id.id);
		Appointment.initialize(event_id.id);

		if (event_id.id == 0)
		{
			if (event_data.calendar_id == 0)
			{
				if (eventFormIdValue == 0 && id_calendar != 0 && mycache.calendars[id_calendar] != undefined)
				{
					calendar = mycache.calendars[id_calendar];
				}
				else
				{
					// Gets the first calendar from calendar cache and return its
					for (i in mycache.calendars)
					{
						sharing_level = mycache.calendars[i]['sharing_level'];
						if (sharing_level == null || sharing_level == FULL_CONTROL || sharing_level == EDIT_EVENTS){
							calendar = mycache.calendars[i];
							break;
						}
					}
				}
			}
			else
			{
				calendar = mycache.calendars[event_data.calendar_id];
			}
			this.window_header.innerHTML = Lang.EventHeaderNew;
			this.savebut.style.display="inline";
			this.cancelbut.style.display = "inline";
			this.delbut.style.display = "none";
			this.closebut.style.display = "none";
			this.alldayCbk.checked == true;
		}
		else
		{
			for (i in mycache.calendars)
			{
				if (mycache.calendars[i]['calendar_id'] == event_data.calendar_id)
				{
					calendar = mycache.calendars[i];
				}
			}
		}

		this.alldayCbk.disabled = "";
		if (calendar.sharing_level == VIEW_EVENTS_DETAILS)
		{
			this.savebut.style.display = "none";
			this.cancelbut.style.display = "none";
			this.delbut.style.display = "none";
			this.alldayCbk.disabled = "disadled";
			
			Appointment.appointmentAddGuestsCont.style.display="none";
			Appointment.expandAppointmentLinkCont.style.display="none";
			Reminder.expandRemindLinkCont.style.display = "none";

			this.closebut.style.display = "inline";
			this.window_header.innerHTML = Lang.EventHeaderView;
			this.EventSubject.disabled = true;
			this.EventDescription.disabled = true;
			this.EventTimeFrom.disabled = true;
			this.EventTimeTill.disabled = true;
			this.EventDateFrom.disabled = true;
			this.EventDateTill.disabled = true;
			this.calen_sal.onclick = function() {};
			this.calen_sal.parentNode.style.cursor = "default";
			this.calendar_arrow.style.display = "none";
			this.calendar_arrow.style.visibility = "hidden";
			this.calendar_arrow.onclick = function() {};
		}
		else
		{
			CleanNode(this.window_header);
			if (event_id.id == 0)
			{
				this.EventSubject.disabled = false;
				this.EventDescription.disabled = false;
				this.EventLocation.disabled = false;
				this.EventTimeFrom.disabled = false;
				this.EventDateFrom.disabled = false;
				this.EventTimeTill.disabled = false;
				this.EventDateTill.disabled = false;
				this.alldayCbk.disabled = false;
				this.savebut.style.display = "inline";
				this.cancelbut.style.display = "inline";
				this.delbut.style.display = "none";
				this.closebut.style.display = "none";
				this.window_header.appendChild(document.createTextNode(Lang.EventHeaderNew));
			}
			else if (mycache.ids[event_id.id].appointment)
			{
				this.EventSubject.disabled = true;
				this.EventDescription.disabled = true;
				this.EventLocation.disabled = true;
				this.EventTimeFrom.disabled = true;
				this.EventTimeTill.disabled = true;
				this.EventDateFrom.disabled = true;
				this.EventDateTill.disabled = true;		
				this.alldayCbk.disabled = true;
				this.expandRepeatLinkCont.style.display = "none";
				Reminder.expandRemindLinkCont.style.display = "none";
				
				if (Appointment.appointmentsAccessType != 1)
				{
					this.delbut.style.display = "inline";
					this.cancelbut.style.display = "none";
					this.savebut.style.display = "none";
					this.closebut.style.display = "inline";
					Appointment.appointmentAddGuestsCont.style.display = "none";
					Appointment.appointmentGuestsRightsCont.style.display = "none";
					this.window_header.appendChild(document.createTextNode(Lang.EventHeaderView));
				}
				else
				{
					this.delbut.style.display = "inline";
					this.cancelbut.style.display = "inline";
					this.savebut.style.display = "inline";
					this.closebut.style.display = "none";
					Appointment.appointmentAddGuestsCont.style.display = "block";
					Appointment.appointmentGuestsRightsCont.style.display = "none";
					this.window_header.appendChild(document.createTextNode(Lang.EventHeaderEdit));
				}
			}
			else
			{
				this.delbut.style.display = "inline";
				this.cancelbut.style.display = "inline";
				this.savebut.style.display = "inline";
				this.closebut.style.display = "none";
				
				this.EventSubject.disabled = false;
				this.EventDescription.disabled = false;
				this.EventLocation.disabled = false;
				this.EventTimeFrom.disabled = false;
				this.EventDateFrom.disabled = false;
				this.EventTimeTill.disabled = false;
				this.EventDateTill.disabled = false;
				this.alldayCbk.disabled = false;
				this.expandRepeatLinkCont.style.display = "block";
//				Reminder.expandRemindLinkCont.style.display = "block"; // issue #1917
				
				this.window_header.appendChild(document.createTextNode(Lang.EventHeaderEdit));
			}
			this.calen_sal.onclick = function()
			{
				obj.ShowHideCalendarsList();
			};
			this.calen_sal.parentNode.style.cursor = "pointer";
			this.calendar_arrow.style.display = "block";
			this.calendar_arrow.style.visibility = "visible";
			this.calendar_arrow.onclick = function()
			{
				obj.ShowHideCalendarsList();
			};
		}

		if (event_data.allday != undefined) this.ExpandAllday(event_data.allday);
		this.EventSubject.value		= event_data.subject;
		this.EventLocation.value	= event_data.location;
		if (event_data.description == null || event_data.description == undefined)
		{
			this.EventDescription.value = "";
		}
		else
		{
			this.EventDescription.value	= event_data.description;
		}
		this.EventTimeFrom.value    = GetDateParams(event_data.dateFrom, false, ["TIME"]).time;
		this.EventTimeTill.value    = GetDateParams(event_data.dateTill, false, ["TIME"]).time;
		this.EventDateFrom.value	= ConvertFromDateToStr(event_data.dateFrom);
		this.EventDateTill.value	= ConvertFromDateToStr(event_data.dateTill);

		this.evform_id.value		= event_data.event_id;

		color_number	= calendar.color;
		color			= GetNumberOfColor(color_number);
		name	= HtmlEncode(calendar.name);
		calendar_id		= calendar.calendar_id;

		this.eventcontainer.className = 'eventcontainer_'+color_number;
		this.color_calendar_now.style.backgroundColor = color;
		this.id_calendar.value = calendar_id;
		CleanNode(this.calen_sal);
		calendarNameTxt = document.createTextNode(name);
		this.calen_sal.appendChild(calendarNameTxt);
		this.calen_sal.parentNode.title = name + ' - ' + calendar.owner;
		this.repeatExclusionWarning.style.display = (event_data.excluded) ? "block":"none";
		
		this.remindCont.style.display = (event_data.excluded) ? "none" : "block"; // TODO
		
		setMaskHeight(this.event_window);

		this.FillRepeatForm();
		if (calendar.sharing_level == VIEW_EVENTS_DETAILS)
		{
			this.expandRepeatLinkCont.style.display = "none";
		}

		this.event_form.style.display = 'block';
		this.event_window.style.display = 'block';

		this.CalcSelectBoxes();
		this.RecalcShadowSize();

		this.MarkCalendarSelectOption();
		tbl_start = this.start_month.value.split("_");

		if ( (tbl_start[0] != (event_data.dateFrom.getMonth()+1)) ||  tbl_start[1] !=  event_data.dateFrom.getFullYear())
		{
			calendarTableStart.Fill(1, (event_data.dateFrom.getMonth()+1), event_data.dateFrom.getFullYear());
		}
		tbl_end = this.end_month.value.split("_");
		if ( (tbl_end[0] != (event_data.dateTill.getMonth()+1)) ||  tbl_end[1] !=  event_data.dateTill.getFullYear())
		{
			calendarTableEnd.Fill(1, (event_data.dateTill.getMonth()+1), event_data.dateTill.getFullYear());
		}
		
		var bSubjEnable = (typeof this.EventSubject === 'object' && !this.EventSubject.disabled)
		if (calendar.sharing_level != VIEW_EVENTS_DETAILS && bSubjEnable)
		{
			this.EventSubject.focus();
		}
	},
	ExpandAllday : function(allday)
	{
		if (allday == true)
		{
			this.alldayCbk.checked = true;
			this.EventTimeFrom.style.display = "none";
			this.EventTimeTill.style.display = "none";
		}
		else
		{
			this.alldayCbk.checked = false;
			this.EventTimeFrom.style.display = "block";
			this.EventTimeTill.style.display = "block";
		}
	}
}