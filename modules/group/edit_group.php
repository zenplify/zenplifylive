<?php
	session_start();
	include_once("../headerFooter/header.php");
	include_once("../../classes/group.php");
	include_once("../../classes/notification.php");
	require_once('../../classes/init.php');

	$groups = new group();
	$database = new database();
	$notification = new notification();
	$groupId = $_REQUEST['group_id'];
	$userId = $_SESSION["userId"];
	$leaderId = $_SESSION["leaderId"];
//	$generatedVia = $_SESSION["generatedVia"];
	//Query Section

	$group = $groups->showGroupById($groupId);
	$privacy = $group->privacy;
	$linked = $groups->isGroupLinked($groupId, $userId);
	if (empty($linked))
	{
		$groupflag = 1;
	}
	else
	{
		$groupflag = 0;
	}

	if (isset($_POST['save']))
	{
		$name = $_REQUEST['groupTitle'];
		$description = $_REQUEST['groupDescription'];
		$color = $_REQUEST['colors'];
		$oldName = $_REQUEST['oldName'];

		if ($privacy == 1)
		{
			$check = $groups->editGroup($_POST, $generatedVia, $userId, $privacy, $groupId, $leaderId);
			$notification->notificationToUser($database, $leaderId, $groupId, 2, 2, "Do you want to Edit the Group \'" . $oldName . "\'");
		}
		else
		{
			$check = $groups->editGroup($_POST, $generatedVia, $userId, $privacy, $groupId, $leaderId);
		}

		if ($check == true)
		{
			SendRedirect('groups_view.php');
		}
	}

?>
	
	  <!-- validation file included  -->
		<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
			
			//This is used to show color picker
			
			$('#groupCategory').change(function(){
			   var opt=$('#groupCategory').val();
				   if(opt==1){
					   
					   	$('#colorPicker').hide();
				   	}	
				   	else{
						$('#colorPicker').show();
					   }
			}); 
			
			//This is used for color picker
			
			$(".click").click(function(){
				var pos='#'+this.id;
				var bgcolor=$(pos).css("background-color");
	  				$("#colmain").css("background-color",bgcolor);
	 				$("#colorval").val(bgcolor);
	  
	  		});

	});
</script>
</head> 
<body>
<?php require_once("../../classes/resource.php"); ?>
<div class="container">
	<div class="top_content">
		<h1 class="gray">Edit Group</h1>

		<p id="manage">
			<a href="manage_contacts.php?group_id=<?php echo $groupId; ?>">
				<input type="button" name="manage_contacts" id="manage_contacts" class="manage_contacts" value=""/>
			</a>
		</p>
	</div>
	<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>" id="editGroupForm">
		<input type="hidden" name="oldName" id="oldName" value="<?php echo $group->name; ?>"/>
		<table cellspacing="0" cellpadding="0" width="585px;">
			<tr>
				<td class="label">Name</td>
				<td><input type="text" name="groupTitle" class="textfield required" value="<?php echo $group->name; ?>"/></td>
			</tr>
			<tr>
				<td class="label">Description</td>
				<td><textarea class="textarea" name="groupDescription"><?php echo $group->description; ?></textarea>
				</td>
			</tr>
			<tr>
				<td class="label">Linked Plan</td>
				<td>
					<?php
						if ($groupflag != 1)
						{
							echo "<p class='linked_plan' style='background-color:transparent !important'>";
							//echo "<img src='../../images/link.gif' style='cursor:pointer;' width='17' height='17' title='linked'/>";
							foreach ($linked as $lg)
							{

								echo "<a href='../plan/create_plan_detail.php?id=" . $lg->planId . "' style='text-decoration:none'> " . $lg->title . "</a> <br \> ";

							}
							echo "</p>";
						}
						else
						{
							?>
							<p class="linked_plan" style="background-color:transparent !important;">No Plans</p>
						<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="label">Color</td>
				<?php $colorCodes = array("#BBF", "#06C", "#64B461", "#509F00", "#FF3EFF", "#FF8C97", "#FF6464", "#F8F06A", "#FF9900", "#B16363", "#FF2D2D","#CCCCCC","#CC99FF");

					$colorDropDown = array('Light Blue', 'Dark Blue',
						'Light Green', 'Dark Green', 'Purple', 'Light Pink',
						'Dark Pink', 'Yellow', 'Orange', 'Brown'
					, 'Red', 'Grey','Voilet');
				?>
				<td><select name="colors" id="colors" class="SelectFieldSmall" style="margin-right:30px !important; width:225px;" onChange="displayColor()">
						<option value="">Please Choose Color</option>
						<?php
							for ($i = 0; $i < sizeof($colorCodes); $i++)
							{
								if ($colorCodes[$i] == $group->color)
									echo '<option value="' . $colorCodes[$i] . '" selected="selected" style="background-color:' . $colorCodes[$i] . '; ">' . $colorDropDown[$i] . '</option>';
								else
									echo '<option value="' . $colorCodes[$i] . '" style="background-color:' . $colorCodes[$i] . '; ">' . $colorDropDown[$i] . '</option>';

							}
						?>

					</select>

					<p id="colorspan" style="background-color:<?php echo $group->color; ?>; display:block; margin-left:11px;;"></p></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>

				<td class="label">&nbsp;</td>
				<td><input type="submit" name="save" id="submit" class="save" value=""/>
					<input type="button" name="cancel" class="cancel" value="" onClick="history.go(-1)"/>
				</td>
			</tr>
		</table>

	</form>
	<br/> <br/>
<?php include_once("../headerFooter/footer.php"); ?>