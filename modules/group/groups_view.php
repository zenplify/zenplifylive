<?php
	error_reporting(0);
	session_start();
	include_once("../headerFooter/header.php");
	include_once("../../classes/group.php");
	$groups = new group();
	$userId = $_SESSION["userId"];
	$leaderId = $_SESSION["leaderId"];

?>
<script type="text/javascript" src="../../js/dragdrop/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/dragdrop/jquery-ui-1.7.1.custom.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(function () {
			$("[id^='contentLeft'] ul ").sortable({ opacity: 0.6, cursor: 'move', update: function () {
				var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
				$.post("../../js/dragdrop/updateDB.php", order, function (theResponse) {
					$("#contentRight").html(theResponse);
				});
			}
			});
		});
		(function ($) {
			jQuery.expr[':'].Contains = function (a, i, m) {
				return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
			};
			function filterList(header, list) {
				var form = $("<form>").attr({"class": "filterform", "action": "#"}),
					input = $("<input>").attr({"class": "filterinput searchbox2", "type": "text"});
				$(form).append(input).appendTo(header);
				$(input)
					.change(function () {
						var filter = $(this).val();
						if (filter) {
							$matches = $(list).find('a:Contains(' + filter + ')').parent();
							$('li', list).not($matches).slideUp();
							$matches.slideDown();
						}
						else {
							$(list).find("li").slideDown();
						}
						return false;
					})
					.keyup(function () {
						$(this).change();
					});
			}

			$(function () {
				filterList($("#form"), $("#list"));
			});
		}(jQuery));
		// Write on keyup event of keyword input element
		$("#searchGroup").keyup(function () {
			// When value of the input is not blank
			if ($(this).val() != "") {
				// Show only matching TR, hide rest of them
				$("#group_searching li").hide();
				//alert($(this).val());
				$("#group_searching li:contains-ci('" + $(this).text() + "')").show();
			}
			else {
				// When there is no input or clean again, show everything back
				$("#group_searching li").show();
			}
		});
	});
	function deleteThis(id) {
		$.post("../../js/dragdrop/updateDB.php?id=" + id, id, function (theResponse) {
			if (theResponse == 1) {
				$('.removeFooter' + id).fadeOut(1000);
			}
		});
	}
	function editThis(id) {
		window.location = "edit_group.php?group_id=" + id;
	}
	/*
	 $.ajax({ url: '../../ajax/ajax.php',
	 data: {action: 'ChangePlanTyep',plan_id:id},
	 type: 'post',
	 success: function(output) {
	 }

	 });

	 */
	function archiveGroup(groupId, userId) {
		var thisVal = $("#archiveGroup" + groupId).html();
		$("#archiveGroup" + groupId).html('<img style="" src="../../images/loading.gif" width="18" height="18">');
		var newVal = (thisVal == 'Archive' ? 'Unarchive' : 'Archive').trim();
		var groupName = $("#groupName_" + groupId).html();
		$.ajax({
			url: "../../classes/ajax.php",
			type: "post",
			data: {action: 'archiveGroup', groupId: groupId, userId: userId, groupName: groupName},
			success: function (result) {
				$("#recordsArray_" + groupId).slideUp();
//				$("#archiveGroup" + groupId).html(newVal);
			}
		});
	}
	function publishGroup(groupId) {
		var thisVal = $("#publishGroup" + groupId).html();
		$("#publishGroup" + groupId).html('<img src="../../images/loading.gif" width="28" height="28">');
		var newVal = (thisVal == 'Publish Group' ? 'Unpublish' : 'Publish').trim();
		var groupName = $("#groupName_" + groupId).html();
		$.ajax({
			url: "../../classes/ajax.php",
			type: "post",
			data: {action: 'publishGroup', groupId: groupId, groupName: groupName},
			success: function (result) {
				$("#publishGroup" + groupId).html('');
			}
		});
	}
</script>
<!--  <div id="menu_line"></div>-->
<div class="container">
	<div class="top_content">
		<a href="new_group.php"> <input type="button" id="new_group" value=""/></a>
	</div>
	<div class="sub_container">
		<div class="col_table">
			<div class="col_table">
				<table width="100%" cellpadding="0" cellspacing="0" id="group_table">
					<thead class="header_table" style="background-color:#CCC;">
					<th class="firstsmall"></th>
					<th class="center big">Name</th>
					<th class="center exbig">Description</th>
					<th class="center smallcol">Linked</th>
					<th class="center smallcol">Actions</th>
					<th class="last"></th>
					</thead>
				</table>
			</div>
			<script type="text/javascript" src="js/jquery.pajinate.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					var siz = $("#pagesize").val();
					$('#paging_container6').pajinate({
						start_page: 0,
						items_per_page: siz
					});
				});
				function changepageSize() {
					var siz = $("#pagesize").val();
					$('#paging_container6').pajinate({
						start_page: 0,
						items_per_page: siz
					});
				}
			</script>
			<div id="wrap">
				<div class="contentLeft" id="contentLeft<?php echo $gc->groupTypeId; ?>">
					<div id="paging_container6">
						<ul id="list" class="content">
							<?php
								$count = 0;
								$showGroup = $groups->showGroups($userId); //show all groups sort by group type
								foreach ($showGroup as $g)
								{
									$linked = $groups->isGroupLinked($g->groupId, $userId);
									if (empty($linked))
									{
										$groupflag = 1;
									}
									else
									{
										$groupflag = 0;
									}
									$count++;
									?>
									<li class="removeFooter<?php echo $g->groupId; ?>" id="recordsArray_<?php echo $g->groupId; ?>" style="background-color:<?php echo $g->color; ?> !important;">
										<a id="groupName_<?php echo $g->groupId; ?>" class="gfirst guestLink" href="<?php echo($userId == $g->guser ? 'edit_group.php?group_id=' . $g->groupId : 'javascript:') ?>">
											<?php if (strlen($g->name) <= 20)
												echo $g->name;
											else
											{
												echo substr($g->name, 0, 20);
												echo '...';
											} ?>
										</a>

										<div class="gsecond"> <?php //echo(empty($g->description)?'&nbsp;':$g->description);
												if (!empty($g->description))
												{
													if (strlen($g->description) > 45)
													{
														echo substr($g->description, 0, 45);
														echo '...';
													}
													else
													{
														echo $g->description;
													}
												}
												else
													echo '&nbsp;'?></div>
										<div class="gthird">
											<?php if ($groupflag != 1)
											{
												$count = 0; ?>
												<img src="../../images/link.gif" style="cursor:pointer;" width="17" height="17" title="<?php foreach ($linked as $lg)
												{
													echo($count == 0 ? "" : ",");
													echo $lg->title;
													$count++;
												} ?>"/>
											<?php
											}
											else
											{
												echo '&nbsp;';
											} ?>
										</div>
										<div class="gfourth">
											<?php

												if ($userId == $g->guser)
													/*(($leaderId == $userId) || (($leaderId != $userId) and $g->leaderId == 0))*/
												{
													?>
													<img src="../../images/edit.png" style="cursor:pointer;" width="17" height="17" onClick="editThis('<?php echo $g->groupId; ?>')" title="Edit"/>
												<?php
												}
											?>
											<!--											<img src="../../images/delete.png" style="cursor:pointer;" width="17" height="17" onClick="deleteThis('-->
											<?php //echo $g->groupId; ?><!--')" title="Delete"/>-->
											<?php
												if ($g->guser != 0)
												{
													?>
													<span id="archiveGroup<?php echo $g->groupId; ?>" class="link" onClick="archiveGroup('<?php echo $g->groupId; ?>','<?php echo $userId; ?>')"><?php echo($g->isactive == '1' ? 'Archive' : 'Unarchive'); ?></span>
												<?php
												}
												if (($leaderId == $userId) && ($g->privacy == 0))
												{
													?>
													<span id='publishGroup<?php echo $g->groupId; ?>' class="link" onClick="publishGroup('<?php echo $g->groupId; ?>')">Publish</span></td>
												<?php
												}
											?>
										</div>
									</li>
								<?php } ?>
						</ul>
						<div style="float:right; margin-top:14px;">
							<select id="pagesize" name="pagesize" class="pagesize" onChange="changepageSize()">
								<option value="50" selected="selected">50</option>
								<option value="100">100</option>
								<option value="250">250</option>
							</select>
						</div>
						<div class="page_navigation" style="float:right; margin-top:10px;"></div>
						<!--
							<form>
								<input type="text" class="pagedisplay" value="1/1">
							</form>-->
					</div>
				</div>
			</div>
			<!------this code is for drag checking starts--------->
			<div id="contentRight" style="display:none;">
				<p>Array will be displayed here.</p>

				<p>&nbsp; </p>
			</div>
			<!------this code is for drag checking ends--------->
		</div>
		<div class="col_search">
			<p class="search_header">Find Groups</p>

			<div id="form"></div>

		</div>
	</div>
	<?php include_once("../headerFooter/footer.php"); ?>

