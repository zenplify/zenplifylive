<?php
	session_start();
	include_once("../headerFooter/header.php");
	include_once("../../classes/group.php");
	$groups = new group();
	$userId = $_SESSION["userId"];
	$leaderId = $_SESSION["leaderId"];

	//$groupCategory=$groups->showGroupsCategory($database);//group Category
	if (isset($_POST['save']))
	{
		$check = $groups->addGroups($_POST, $generatedVia, $userId, $leaderId);
		if ($check == true)
		{
			//echo '<script type="text/javascript">'.'alert("inserted");'.'</script>';
			SendRedirect('groups_view.php');
		}
	}
?>
<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		//This is used for color picker
		$(".click").click(function () {
			var pos = '#' + this.id;
			var bgcolor = $(pos).css("background-color");
			$("#colmain").css("background-color", bgcolor);
			$("#colorval").val(bgcolor);
		});
	});
</script>
<?php require_once("../../classes/resource.php"); ?>
<!--  <div id="menu_line"></div>-->
<div class="container">
	<h1 class="gray">New Group</h1>

	<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>" id="addGroupForm">
		<input type="hidden" name="userId" value="<?php echo $userId; ?>"/>
		<table cellspacing="0" cellpadding="0" width="70%">
			<tr>
				<td class="label">Name</td>
				<td width="80%"><input type="text" name="groupTitle" class="textfield required"/></td>
			</tr>

			<tr>
				<td class="label">Description</td>
				<td><textarea name="groupDescription" class="textarea"></textarea></td>
			</tr>

			<tr>
				<td class="label">Color</td>
				<td><select name="color" id="colors" class="SelectField required" style="margin-right:30px !important; " onchange="displayColor()">
						<option value="">Please Choose Color</option>
						<option value="#64B461" style="background-color:#64B461; ">Dark Green</option>
						<option value="#88FFA1" style="background-color:#88FFA1; ">Light Green</option>
						<option value="#409AFF" style="background-color:#409AFF; ">Sky Blue</option>
						<option value="#FFA430" style="background-color:#FFA430; ">Dark Orange</option>
						<option value="#FFD05B" style="background-color:#FFD05B; ">Light Orange</option>
						<option value="#FF8C97"  style="background-color:#FF8C97; ">Indigo</option>
						<option value="#CCCCCC" style="background-color:#CCCCCC; ">Grey</option>
						<option value="#CC99FF"style="background-color:#CC99FF; ">Violet</option>
						<option value="#FFC5D7"style="background-color:#FFC5D7; ">Light PInk</option>
						<option value="#FF99CC" style="background-color:#FF99CC; ">Dark Pink</option>

<!--						<option value="#64B461" style="background-color:#64B461; ">Dark Green</option>-->
<!--						<option value="#88FFA1" style="background-color:#88FFA1; ">Light Green</option>-->
<!--						<option value="#409AFF" style="background-color:#409AFF; ">Sky Blue</option>-->
<!--						<option value="#FFA430" style="background-color:#FFA430; ">Dark Orange</option>-->
<!--						<option value="#FFD05B" style="background-color:#FFD05B; ">Light Orange</option>-->
<!--						<option value="#FF8C97"  style="background-color:#FF8C97; ">Indigo</option>-->
<!--						<option value="#CCCCCC" style="background-color:#CCCCCC; ">Grey</option>-->
<!--						<option value="#CC99FF"style="background-color:#CC99FF; ">Violet</option>-->
<!--						<option value="#FFC5D7"style="background-color:#FFC5D7; ">Light PInk</option>-->
<!--						<option value="#FF99CC" style="background-color:#FF99CC; ">Dark Pink</option>-->
					</select>

					<p id="colorspan"></p></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>

				<td class="label">&nbsp;</td>
				<td class="label">
					<input type="submit" name="save" id="submit" class="save" value=""/><input type="button" name="cancel" class="cancel" value="" onclick="history.go(-1)"/></td>
			</tr>
		</table>
	</form>


	<?php
		include_once("../headerFooter/footer.php");
	?>
   