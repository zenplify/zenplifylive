
<?php
/**
 * Created for zapier api login page
 * User: suave solutions
 * Date: 5/16/16
 * Time: 4:03 PM
 */
	require_once("../../classes/register.php");
    require_once("../../classes/init.php");
session_start();
/*
 * getting parameters
 */
    $redirectUri=$_GET['redirect_uri'];
    $database=new database();
    $register=new register();
    $code=$_GET['state'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Zenplify</title>
    <link rel="stylesheet" href="../../css/style.css" type="text/css">
<?php
/*
 * submit request
 */

	if(isset($_REQUEST['submit']))
	{
       $userNam=$_REQUEST['username'];
       $password=$_REQUEST['password'];
        $admin = $register->checkUserForZapier($userNam,$password);


		if($admin->userName == "" || empty($admin->userName))
		{
			$message='Invalid User';

		}else{

            $token = openssl_random_pseudo_bytes(16);
            $token = bin2hex($token);
            $register->addZapierUserToken($admin->userId,$token);
            $_SESSION['code']=$token;
            $redirectUri=$redirectUri.'?code='.$token;
           header('Location: '.$redirectUri);
			}

	}

?>

<!-- login design -->

<div align="center" id="index_logo"><img src="../../images/logo.png"/>

	<div id="login_container">
		<div style="margin-top:10px;"></div>
		<div><span class="message"><?php echo $message; ?></span></div>
		<form name="login_user" id="login_user" action="" method="post">
			<table width="400px" style="margin-left:170px; padding:0;" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>&nbsp;<input type="hidden" name="signinType" id="signinType"/></td>
				</tr>
				<tr>
					<td><input type="text" name="username" id="username" class="textfield required"
							value="" style="padding-left:2px;"/></td>
				</tr>
				<tr>
					<td><input type="password" name="password" id="password" class="textfield required"
							value="" style="padding-left:2px;"/></td>
				</tr>
				<tr>
					<td>

					</td>
				</tr>
				<tr>
					<td height="20px"></td>
				</tr>

				<tr>
					<td>

						<span style="width:64%; margin:0 auto; display: block">
							<input type="submit" name="submit" id="submit_button_user" class="signin" value=""/>
						</span>
					<td>
				<tr height="15">
					<td></td>
				</tr>


						</table>

					</td>
				</tr>
			</table>
		</form>
	</div>

</div>
</body>
</html>
