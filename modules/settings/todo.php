<?php 
	session_start();
 	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');
	require_once('../../classes/contact.php');
 	$settings = new settings();
	$contact = new contact();
	$userDetails = $contact->GetUserDetails($userId);
	$upComingtasks = $settings->getUpcomingTasks($userId);
	$currentUpcomingTask = $userDetails->upComingTasks;
?>

<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<?php
		if(isset($_POST['btnUpdate'])){
			$settings->updateUpcomingTasks($_POST['ddlupcomingtask'],$userId);
			$currentUpcomingTask = $_POST['ddlupcomingtask'];
		}
	?>
    <div class="rightMiddleContent">
		<div id="existingField">
            <h1 class="gray">To Do Settings</h1>
            <div class="clear"></div>
            <form name="frmUpcomingTasks" id="frmUpcomingTasks" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <table cellpadding="0" cellspacing="0" id="tblUpcomingtask" width="90%" >
                    <tr>
                        <td width="30%">Show Upcoming Tasks</td>
                        <td>
                            <select name="ddlupcomingtask" id="ddlupcomingtask">
                            <?php
                            foreach($upComingtasks as $uct){
                                echo $currentUpcomingTask.'_'.$uct->id.'<br>';
								if($currentUpcomingTask == $uct->id){
                                    ?><option selected="selected" value="<?php echo $uct->id ?>"><?php echo $uct->value; ?></option><?php
                                }
                                else{
                                    ?><option value="<?php echo $uct->id ?>"><?php echo $uct->value; ?></option><?php
                                }
                            }
                            ?>
                            </select>
                    </tr>
                    <tr class="spacer"><td></td><td colspan="2"></td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input value="Update" type="submit" class="fieldbtn frmbtn" name="btnUpdate" id="btnUpdate" />
                            <input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancel" id="btnCancel" />
                        </td>
                    </tr>
                </table>
			</form>
        </div> <!--existing field-->
	</div>
    <div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
</div>
