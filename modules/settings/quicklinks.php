<?php
	session_start();
	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');
	$settings = new settings();
	$leaderQuickLinks = $settings->getLeaderQuickLinks($userId);
?>
<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#tblQuickLinks").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
		$('#btnCancelEditField').click(function (e) {
			$('#newField').hide();
			$('#editField').hide();
			$('#existingField').show();
		});
		$('.editQuickLink').click(function (e) {
			$('#newField').hide();
			$('#editField').show();
			$('#existingField').hide();
			$('#txtEditTitle').val($(this).parent().siblings(':first').text());
			$('#txtEditLink').val($(this).parent().siblings(':nth-child(2)').text());
			$('#txtEditId').val($(this).attr('id'));
		});
		$('#btnCancelNewField').click(function (e) {
			$('#existingField').show();
			$('#newField').hide();
		});
	});
	function editField(id, fieldName) {
		$('#edit' + id).hide();
		$('#save' + id).show();
		$('#txt' + fieldName).css('border', '1px solid gray').css('background-color', 'white').focus();
	}
	function saveField(id, fieldName) {
		var fieldValue = $("#txt" + fieldName).val();
		if (fieldValue == "" || fieldValue.length < 2) {
			$("#msg" + id).html('At least 2 character name is required').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
			return false;
		}
		else {
			$.ajax({
				url: "../../classes/ajax.php",
				type: "post",
				data: {action: 'saveLeaderDefaultFields', us: fieldValue, id: id},
				success: function (result) {
					$("#msg" + id).html('Field Name is updated.').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
				}
			});
		}
		$('#edit' + id).show();
		$('#save' + id).hide();
		$('#txt' + fieldName).css('border', '0px').css('background-color', '#f7f6f6');
	}
	function toggleStatus(id, legend) {
		var thisVal = $("#status" + id).html().trim();
		var newVal = (thisVal == 'Inactive' ? 'Active' : 'Inactive').trim();
		$("#status" + id).html('<img src="../../images/loading.gif" width="28" height="28">');
		$.ajax({
			url: "../../classes/ajax.php",
			type: "post",
			data: {action: 'toggleLeaderQuickLinks', id: id},
			success: function (result) {
				$("#msg").html('Field is updated').css('color', '#20BF04').show('slow').animate({top: 0}, 5000).fadeOut();
				$("#status" + id).html(newVal);
			}
		});
	}
	function addNewField() {
		$('#newField').show();
		$('#existingField').hide();
		$('#editField').hide();
	}
	function validateForm(frmId) {
		if(frmId == 'frmQuickLinks')
		{
			if($("#txtTitle").val().length == 0)
			{
				$("#txtTitleWarning").show();
				$("#txtTitle").focus();
				return false;
			}
			else{
				$("#txtTitleWarning").hide();
			}
			if($("#txtLink").val().length == 0)
			{
				$("#txtLinkWarningUrl").hide();
				$("#txtLinkWarning").show();
				$("#txtLink").focus();
				return false;
			}
			else{
//				if (!(/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/.test($("#txtLink").val()))) {
				if(!(/^(http:\/\/www\.|https:\/\/www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test($("#txtLink").val()))){
						$("#txtLinkWarningUrl").show();
					$("#txtLinkWarning").hide();
					$("#txtLink").focus();
					return false;
				}
				$("#txtLinkWarningUrl").hide();
				$("#txtLinkWarning").hide();
			}
		}
	}
</script>
<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<?php
		if (isset($_POST['btnAddNewField']))
		{
			$settings->addLeaderQuickLinks($_POST, $userId);
			$leaderQuickLinks = $settings->getLeaderQuickLinks($userId);
		}
		elseif (isset($_POST['btnEditNewField']))
		{
			$settings->updateLeaderQuickLinks($_POST, $userId);
			$leaderQuickLinks = $settings->getLeaderQuickLinks($userId);
		}
	?>
	<div class="rightMiddleContent">
		<div id="existingField">
			<h1 class="gray">Quick Links</h1>
			<span id="clickNewField" class="fieldbtn" onclick="addNewField()">New Link</span>

			<div class="clear" style="height:15px;"><span id=_"msg">&nbsp;</span></div>
			<table cellpadding="0" cellspacing="0" id="tblQuickLinks" width="100%" class="tablesorter">
				<thead>
				<tr class="header">
					<th width="24%" class="">Title <img src="../../images/bg.gif"/></th>
					<th width="30%" class="">Link <img src="../../images/bg.gif"/></th>
					<th width="15%" class="">Status <img src="../../images/bg.gif"/></th>
					<th width="20%" class="">Date Time<img src="../../images/bg.gif"/></th>
					<th width="10%" class="">Action</th>
				</tr>
				</thead>
				<tbody id="quickLinksTbody">
				<?php
					foreach ($leaderQuickLinks as $lql)
					{
						?>
						<tr id="<?php echo $lql->id; ?>">
							<td width="24%"><?php echo $lql->title; ?></td>
							<td width="30%"><a href="<?php echo $lql->link; ?>"><?php echo $lql->link; ?></a></td>
							<td width="15%"><a id="status<?php echo $lql->id; ?>" onclick="toggleStatus('<?php echo $lql->id; ?>')">
									<?php echo $lql->status == '1' ? 'Active' : 'Inactive'; ?>
								</a>
							</td>
							<td width="20%"><?php echo $lql->datetime; ?></td>
							<td width="10%"><a class="editQuickLink" id="<?php echo $lql->id; ?>">Edit</a></td>
						</tr>
					<?php
					}
				?>
				</tbody>
			</table>
			<div id="pager" class="pager">
				<form>
					<span class="total"></span>
					<img src="../../images/paging_far_left.gif" class="first"/>
					<img src="../../images/paging_left.gif" class="prev"/>
					<input type="text" class="pagedisplay"/>
					<img src="../../images/paging_right.gif" class="next"/>
					<img src="../../images/paging_far_right.gif" class="last"/>
					<select id="my_pagesize" name="pagesize" class="pagesize">
						<option value="50">50</option>
						<option value="100">100</option>
						<option value="250">250</option>
					</select>
				</form>
			</div>
		</div>
		<!--existing field-->

		<div id="newField">
			<h1 class="gray">New Field</h1>

			<form name="frmQuickLinks" id="frmQuickLinks" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return validateForm('frmQuickLinks')">
				<table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="90%">
					<tr>
						<td width="40%">Title:</td>
						<td>
							<input type="text" name="txtTitle" id="txtTitle"/>
							<span id="txtTitleWarning" class="warning">This field is required</span>
						</td>
					</tr>
					<tr class="spacer">
						<td></td>
					</tr>
					<tr>
						<td width="40%">Link</td>
						<td>
							<input type="text" name="txtLink" id="txtLink"/>
							<span id="txtLinkWarning" class="warning">This field is required</span>
							<span id="txtLinkWarningUrl" class="warning">Please enter a valid URL i.e http://www.google.com</span>
						</td>
					</tr>
					<tr class="spacer">
						<td></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input value="Add" type="submit" class="fieldbtn frmbtn" name="btnAddNewField" id="btnAddNewField"/>
							<input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelNewField" id="btnCancelNewField"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<!--new field-->

		<div id="editField">
			<h1 class="gray">Edit Field</h1>

			<form name="frmEditQuickLinks" id="frmEditQuickLinks" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="90%">
					<input type="hidden" name="txtEditId" id="txtEditId"/>
					<tr>
						<td width="40%">Title:</td>
						<td><input type="text" name="txtEditTitle" id="txtEditTitle"/>
					</tr>
					<tr class="spacer">
						<td></td>
					</tr>
					<tr>
						<td width="40%">Link</td>
						<td><input type="text" name="txtEditLink" id="txtEditLink"/>
					</tr>
					<tr class="spacer">
						<td></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input value="Update" type="submit" class="fieldbtn frmbtn" name="btnEditNewField" id="btnEditNewField"/>
							<input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelEditField" id="btnCancelEditField"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
</div>
