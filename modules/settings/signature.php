<?php
	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');


	$settings = new settings();
	$database = new database();
	$data = $settings->getSignature($userId, $database);
?>
<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<div class="rightMiddleContent">
		
		<div>
			<h1 class="gray" style="display: inline-block;">Signature Settings</h1>
           <div id="message" style="display: inline-block; width: 400px; font-size: 12px; text-align: center;"></div>
			<input type="button" class="save" id="btnSave" style="display: inline-block;float: right;margin-top: 10px;" value=""/>
			<textarea id="signature"><?php if($data != "") { echo $data->signature; }?></textarea>
			<input id="signatureId" type="hidden" value="<?php if($data != "") { echo $data->userSignatureId; }?>">
		</div>
	</div>
	<div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
	<script src="../../lib/ckeditor/ckeditor.js"></script>
	<script>
		CKEDITOR.replace( 'signature' );
		$('#btnSave').on('click', function() {
			var signature = CKEDITOR.instances['signature'].getData();
			var signatureId = $('#signatureId').val();
			if (signature == '') {
				$('#message').html('<p style="color: red;">Please provide signature.</p>').show().delay(3000).fadeOut();
			} else {
				
				$.ajax({
					type: 'POST',
					url: '../../classes/ajax.php',
					data: {action: 'saveSignature', signature: signature, signatureId: signatureId},
					success: function(response) {
						console.log(response);
						response = JSON.parse(response);
						$('#message').html('<p style="color: green;">' + response.message + '</p>').show().delay(3000).fadeOut();
					}
				});
			}
		});

	</script>
</div>
</div>