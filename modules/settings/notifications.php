<?php
	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');
	require_once('../../classes/contact.php');
	include_once("../../classes/group.php");
	include_once("../../classes/notification.php");
	$settings = new settings();
	$contact = new contact();
	$group = new group();
	$notification = new notification();

	$database = new database();

	///$userDetails = $contact->GetUserDetails($userId);
	//$upComingtasks = $settings->getUpcomingTasks($userId);
	//$currentUpcomingTask = $userDetails->upComingtasks;
?>
<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$("#tblNotification").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
	});
</script>
<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<div class="rightMiddleContent">
		<div id="existingField">
			<h1 class="gray">Notifications</h1>

			<div class="clear" style="height:15px;"><span id=_"msg">&nbsp;</span></div>
			<?php
				$getNotNowDetail = $notification->getNotNowNotifications($userId);
				if (empty($getNotNowDetail))
				{
					?>
					<div style="margin-top:30px; margin:0 auto; width: 250px;;">No Notifications are present</div><?php
				}
				else
				{
					?>
					<table cellpadding="0" cellspacing="0" id="tblNotification" width="100%" class="tablesorter">
						<thead>
						<tr class="header">
							<th width="40%" class="">Notification<img src="../../images/bg.gif"/></th>
							<th width="30%" class="">Dismiss Time<img src="../../images/bg.gif"/></th>
							<th width="25%" class="">Action<img src="../../images/bg.gif"/></th>
						</tr>
						</thead>
						<tbody id="">
						<?php
							foreach ($getNotNowDetail as $gd)
							{
								?>
								<tr id="notification_<?php echo $gd->userstatusId; ?>">
									<td width="40%" align="left"><?php echo $gd->textToShow; ?></td>
									<td width="30%" align="center"><?php echo $gd->dismissTime; ?></td>
									<td width="25%" align="center">
                                <?php
                                        if($gd->tablename='plansteps' and $gd->action=='2' or $gd->action=='3'){
                                        echo "<span class='date_span' style='float:none;'>
								<a href=\"javascript:confirmForstep($gd->userstatusId)\" id='confirm_" . $gd->userstatusId . "'>Confirm</a>  ";
                                    }
                                        else{

                                         echo "<span class='date_span' style='float:none;'><a href=\"javascript:requestConfirm($gd->userstatusId)\" id='confirm_" . $gd->userstatusId . "'>Confirm</a> </span>";
                                        } ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<form>
							<span class="total"></span>
							<img src="../../images/paging_far_left.gif" class="first"/>
							<img src="../../images/paging_left.gif" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="../../images/paging_right.gif" class="next"/>
							<img src="../../images/paging_far_right.gif" class="last"/>
							<select id="my_pagesize" name="pagesize" class="pagesize">
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="250">250</option>
							</select>
						</form>
					</div>
				<?php
				}
			?>
		</div>
	</div>
	<div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
</div>
</div>
