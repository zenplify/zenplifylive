<?php
include_once("../../classes/init.php");
include_once("../headerFooter/header.php");
require_once('../../classes/settings.php');

$settings = new settings();
$leaderSettings = $settings->getLeaderSettings($userId);
$leaderDefaultFields = $settings->getLeaderDefaultFields($userId);
$leaderCustomQuestions = $settings->getLeaderCustomQuestions($userId);
$leaderQuestionTypes = $settings->getLeaderCustomQuestionTypes();
$SignupStatus = $settings->SignupStatus($userId);

?>

<div class="container">
    <?php include_once("settingsMenu.php"); ?>
    <?php
    if (isset($_POST['btnAddQuestion'])) {
        $settings->addLeaderCustomQuestions($_POST);
        $leaderCustomQuestions = $settings->getLeaderCustomQuestions($userId);
    } elseif (isset($_POST['btnUpdateQuestion'])) {
        $settings->updateLeaderCustomQuestions($_POST);
        $leaderCustomQuestions = $settings->getLeaderCustomQuestions($userId);
    }
    ?>
    <div class="rightMiddleContent">
        <span id="txtCheckboxTemplate" class="none"><input type="text" class="displayBlock" name="txtCheckbox[]"
                                                           id="txtCheckbox[]" value=""/></span>
        <span id="txtRadioBtnTemplate" class="none"><input type="text" class="displayBlock" name="txtRadioBtn[]"
                                                           id="txtRadioBtn[]" value=""/></span>
        <?php
        if (isset($_POST['editQuestionId'])) {
            $questionDetails = $settings->getLeaderQuestionDetails($_POST['editQuestionId']);
            $radioCount = $checkboxCount = 0;
            $checkDisplay = $radioDisplay = 'none';
            if ($_POST['editTypeId'] == 3) {
                $checkboxCount = count($questionDetails);
                $checkDisplay = '';
            } elseif ($_POST['editTypeId'] == 4) {
                $radioCount = count($questionDetails);
                $radioDisplay = '';
            }

            ?>
            <div id="editFieldAfter">
                <h1 class="gray">Edit Field</h1>

                <form name="frmEditQuestion" id="frmEditQuestion" action="<?php echo $_SERVER['PHP_SELF'] ?>"
                      method="post">
                    <input type="hidden" name="txtQuestionId" id="txtQuestionId"
                           value="<?php echo $_POST['editQuestionId']; ?>"/>
                    <table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="90%">
                        <tr>
                            <td width="40%">Field's Label:</td>
                            <td><input type="text" name="txtFieldLabel" id="txtFieldLabel"
                                       value="<?php echo $_POST['editQuestionValue'] ?>"/>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td width="40%">Field type</td>
                            <td>
                                <select id="ddlFieldType" name="ddlFieldType">
                                    <?php
                                    foreach ($leaderQuestionTypes as $lqt) {
                                        if ($_POST['editTypeId'] == $lqt->typeId) {
                                            ?>
                                            <option selected="selected"
                                                    value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
                                        <?php
                                        } else {
                                            ?>
                                            <option
                                                value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="spacer checkboxCount <?php echo $checkDisplay; ?>">
                            <td></td>
                        </tr>
                        <tr class="checkboxCount <?php echo $checkDisplay; ?>">
                            <td width="40%">No of checkboxes</td>
                            <td>
                                <input type="text" name="txtCheckboxCount" id="txtCheckboxCount"
                                       value="<?php echo $checkboxCount; ?>"/>
                                <input type="hidden" name="txtCheckboxCountLast" id="txtCheckboxCountLast"
                                       value="<?php echo $checkboxCount; ?>"/>
                            </td>
                        </tr>
                        <tr class="spacer checkbox <?php echo $checkDisplay; ?>">
                            <td></td>
                        </tr>
                        <tr class="checkbox <?php echo $checkDisplay; ?>">
                            <td width="40%">Value</td>
                            <td>
                                <?php
                                if ($_POST['editTypeId'] == 3) {
                                    foreach ($questionDetails as $qd) {
                                        ?>
                                        <input class="displayBlock" type="text" name="txtCheckbox[]" id="txtCheckbox[]"
                                               value="<?php echo $qd->choiceText ?>"/>
                                    <?php
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr class="spacer radiobtnCount <?php echo $radioDisplay; ?>">
                            <td></td>
                        </tr>
                        <tr class="radiobtnCount <?php echo $radioDisplay; ?>">
                            <td width="40%">No of radio buttons</td>
                            <td>
                                <input type="text" name="txtRadioBtnCount" id="txtRadioBtnCount"
                                       value="<?php echo $radioCount; ?>"/>
                                <input type="hidden" name="txtRadioBtnCountLast" id="txtRadioBtnCountLast"
                                       value="<?php echo $radioCount; ?>"/>
                            </td>
                        </tr>
                        <tr class="spacer radio <?php echo $radioDisplay; ?>">
                            <td></td>
                        </tr>
                        <tr class="radiobtn <?php echo $radioDisplay; ?>">
                            <td width="40%">Value</td>
                            <td>
                                <?php
                                if ($_POST['editTypeId'] == 4) {
                                    foreach ($questionDetails as $qd) {
                                        ?>
                                        <input class="displayBlock" type="text" name="txtRadioBtn[]" id="txtRadioBtn[]"
                                               value="<?php echo $qd->choiceText; ?>"/>
                                    <?php
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td width="40%">Mandatory</td>
                            <td>
                                <input type="radio" name="rdMandatory"
                                       value="1" <?php if ($_POST['editIsMandatory'] == 1) echo 'checked="checked"' ?>  />Yes
                                <input type="radio" name="rdMandatory"
                                       value="0" <?php if (!($_POST['editIsMandatory'] == 1)) echo 'checked="checked"' ?>/>No
                            </td>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input value="Update" type="submit" class="fieldbtn frmbtn" name="btnUpdateQuestion"
                                       id="btnUpdateQuestion"/>
                                <input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelQuestion"
                                       id="btnCancelQuestion"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        <?php
        } else {
            ?>
            <div id="existingField">
                <h1 class="gray">Sign up Setup</h1>


                <div class="clear"></div>
                <table cellpadding="0" cellspacing="0" id="tblSettingsField" width="100%">
                    <tr>
                        <td colspan="3"> <strong>Signup URL:</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://techgeese/signup/&nbsp;&nbsp;<input type="text" style="font-size: 12px;" readonly name="signupName" id="signupName"
                                                                                                      value="<?php echo $leaderSettings->signupName; ?>"/>&nbsp;&nbsp;&nbsp;
                            <a id="editUrl" onClick="enableSignupName(this.id,'saveUrl')">Update</a>
                            <a id="saveUrl" onClick="enableSignupName(this.id,'editUrl')">Save</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp; <?php  echo ' <a   onclick="copyToClipboard(\'' . $CONF["siteURL"]  .
                                'signup/' .
                                $leaderSettings->signupName . '\')" />Copy Link</a>'; ?>
                            <input style="font-size: 12px;" type="hidden" name="signupNameCheck" id="signupNameCheck">


                        </td>


                    </tr>
                    <tr>
                        <td colspan="3" style="display: inline-block;  padding-left: 35px;"><strong>Allow Sign up: </strong>
                                &nbsp;
                                &nbsp;
                            &nbsp; <span
                                id="errormessage"
                                                                                          style="display:
                        none;"></span>




                            <?php if ($SignupStatus == '2') {
                                echo '<span id="statusmessage" style="color: #008000;font-size: 12px; font-style: italic;">Approved</span>';
                            } else if ($SignupStatus == '1') {
                                echo '<span id="statusmessage" style="color:#EB939A;font-size: 12px; font-style: italic;"> Request pending</span>';
                           ?>  &nbsp;&nbsp;&nbsp;&nbsp;<?php  } ?>

                            <span id="signupAction"><?php if ($SignupStatus == '0') { ?>
                                    <a id="allowsignUp" onClick="RequestAllowsignup(1)">Request</a> <?php } ?> <?php if ($SignupStatus == '2') { ?> &nbsp;&nbsp;&nbsp; <a id="allowsignUp" onClick="RequestAllowsignup(0)">Cancle</a> <?php } ?></span>

                        </td>

                    </tr>
                    <tr class="spacer">
                        <td colspan="3"><span class="msg" id="msg"></span></td>
                    </tr>
<tr><td colspan="3"> <h1 class="gray">Custom fields Setup</h1>
        <span id="clickNewField" class="fieldbtn" onclick="addNewField()">New Field</span></td></tr>

                    <tr class="spacer">
                        <td colspan="3"><span class="msg" id="msg"></span></td>
                    </tr>


                    <?php if (!empty($leaderDefaultFields)) { ?>
                        <tr>
                            <td colspan="3">
                                <table id="tblsortableFields" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody class="sortableFields">
                                    <?php
                                    foreach ($leaderDefaultFields as $ldf) {
                                        ?>
                                        <tr id="ldf<?php echo $ldf->id ?>">
                                            <td width="40%">
                                                <input class="hideBorders" type="text"
                                                       name="txt<?php echo $ldf->fieldMappingName; ?>"
                                                       id="txt<?php echo $ldf->fieldMappingName; ?>"
                                                       value="<?php echo $ldf->fieldCustomName; ?>"
                                                       disabled="disabled"/>
                                            </td>
                                            <td><input type="text" readonly name="" id=""/></td>
                                            <td>
                                                <a id="edit<?php echo $ldf->id; ?>"
                                                   onClick="editField('<?php echo $ldf->id; ?>',
                                                       '<?php echo $ldf->fieldMappingName; ?>')">Update</a>
                                                <a class="hidden" id="save<?php echo $ldf->id; ?>"
                                                   onClick="saveField('<?php echo $ldf->id; ?>','<?php echo $ldf->fieldMappingName; ?>')">Save</a>
                                            </td>
                                            <td><a id="status<?php echo $ldf->id; ?>"
                                                   onclick="toggleStatus('<?php echo $ldf->id; ?>','defaultField')">
                                                    <?php echo $ldf->status == '1' ? 'Active' : 'Inactive'; ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--<tr class="spacer"><td colspan="3"><span class="msg" id="msg< ?php echo $ldf->id ?>" ></span></td></tr>-->
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    <?php } ?>
                    <tr class="noCss">
                        <td colspan="3"></td>
                    </tr>
                    <tr>

                        <td colspan="3">
                            <table id="tblsortableQuestions" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody class="sortableQuestions">
                                <?php
                                foreach ($leaderCustomQuestions as $lcq) {
                                    ?>
                                    <tr id="lcq<?php echo $lcq->questionId ?>">
                                        <td width="40%">
                                            <input class="hideBorders" type="text"
                                                   name="txt<?php echo $lcq->questionId; ?>"
                                                   id="txt<?php echo $lcq->questionId; ?>"
                                                   value="<?php echo $lcq->question; ?>" disabled="disabled"/>
                                        </td>
                                        <td><input type="text" readonly name="" id=""/></td>
                                        <td>
                                            <a id="edit<?php echo $lcq->questionId; ?>"
                                               onClick="editQuestion('<?php echo $lcq->question; ?>','<?php echo $lcq->questionId; ?>','<?php echo $lcq->typeId; ?>','<?php echo $lcq->mandatory; ?>')">Edit</a>
                                            <a class="hidden" id="save<?php echo $lcq->questionId; ?>"
                                               onClick="saveField('<?php echo $lcq->questionId; ?>','<?php echo $lcq->question; ?>')">Save</a>
                                        </td>
                                        <td><a id="status<?php echo $lcq->questionId; ?>"
                                               onclick="toggleStatus('<?php echo $lcq->questionId; ?>','customQuestion')">
                                                <?php echo $lcq->status == '1' ? 'Active' : 'Inactive'; ?>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div> <!--existing field-->

            <div id="newField">
                <h1 class="gray">New Field</h1>

                <form name="frmCustomFields" id="frmCustomFields" action="<?php echo $_SERVER['PHP_SELF']; ?>"
                      method="post" onsubmit="return validateForm('frmCustomFields')">
                    <table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="90%">
                        <tr>
                            <td width="40%">Field's Label:</td>
                            <td>
                                <input type="text" name="txtFieldLabel" id="txtFieldLabel"/>
                                <span id="txtFieldLabelWarning" class="warning">This field is required</span>
                            </td>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td width="40%">Field type</td>
                            <td>
                                <select id="ddlFieldType" name="ddlFieldType">
                                    <option value="0">Select Field type</option>
                                    <?php
                                    foreach ($leaderQuestionTypes as $lqt) {
                                        ?>
                                        <option
                                            value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <span id="ddlFieldTypeWarning" class="warning">This field is required</span>
                            </td>
                        </tr>
                        <tr class="spacer checkboxCount none">
                            <td></td>
                        </tr>
                        <tr class="checkboxCount none">
                            <td width="40%">No of checkboxes</td>
                            <td>
                                <input type="text" name="txtCheckboxCount" id="txtCheckboxCount" value="0"/>
                                <input type="hidden" name="txtCheckboxCountLast" id="txtCheckboxCountLast" value="0"/>
                            </td>
                        </tr>
                        <tr class="spacer checkbox none">
                            <td></td>
                        </tr>
                        <tr class="checkbox none">
                            <td width="40%">Value</td>
                            <td></td>
                        </tr>
                        <tr class="spacer radiobtnCount none">
                            <td></td>
                        </tr>
                        <tr class="radiobtnCount none">
                            <td width="40%">No of radio buttons</td>
                            <td>
                                <input type="text" name="txtRadioBtnCount" id="txtRadioBtnCount" value="0"/>
                                <input type="hidden" name="txtRadioBtnCountLast" id="txtRadioBtnCountLast" value="0"/>
                            </td>
                        </tr>
                        <tr class="spacer radiobtn none">
                            <td></td>
                        </tr>
                        <tr class="radiobtn none">
                            <td width="40%">Value</td>
                            <td></td>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td width="40%">Mandatory</td>
                            <td>
                                <input type="radio" name="rdMandatory" value="1"/>Yes
                                <input type="radio" name="rdMandatory" value="0"/>No
                                <span name="rdMandatoryWarning" id="rdMandatoryWarning" class="warning">This field is required</span>
                            </td>
                        </tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input value="Add" type="submit" class="fieldbtn frmbtn" name="btnAddQuestion"
                                       id="btnAddQuestion"/>
                                <input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelNewField"
                                       id="btnCancelNewField"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div> <!--new field-->

            <div id="editFieldBefore">
                <h1 class="gray"></h1>

                <form name="frmEditQuestion" id="frmEditQuestion" action="<?php echo $_SERVER['PHP_SELF']; ?>"
                      method="post">
                    <input type="hidden" name="editQuestionValue" id="editQuestionValue"/>
                    <input type="hidden" name="editQuestionId" id="editQuestionId"/>
                    <input type="hidden" name="editTypeId" id="editTypeId"/>
                    <input type="hidden" name="editIsMandatory" id="editIsMandatory"/>
                    <input value="Add" type="submit" class="fieldbtn frmbtn" name="btnEditQuestion"
                           id="btnEditQuestion">
                </form>
            </div>
        <?php } ?>
    </div>
    <div class="clear"></div>
    <?php include("../headerFooter/footer.php"); ?>
    <script type="text/javascript" src="../../js/sort/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../../js/sort/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="../../js/sort/jquery.ui.mouse.js"></script>
    <script type="text/javascript" src="../../js/sort/jquery.ui.sortable.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.sortableFields').sortable({
                update: function (event, ui) {
                    var newOrder = $(this).sortable('toArray').toString();
                    $.ajax({
                        url: "../../classes/ajax.php",
                        type: "post",
                        data: {action: 'updateDefaultFieldOrder', id: newOrder},
                        success: function (result) {
                            $("#msg").html('Field Order Updated').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                            $("#usercheck").val(result);
                        }
                    });
                }
            });
            $('.sortableQuestions').sortable({
                update: function (event, ui) {
                    var newOrder = $(this).sortable('toArray').toString();
                    $.ajax({
                        url: "../../classes/ajax.php",
                        type: "post",
                        data: {action: 'updateCustomQuestionsOrder', id: newOrder},
                        success: function (result) {
                            $("#msg").html('Field Order Updated').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                            $("#usercheck").val(result);
                        }
                    });
                }
            });
            $('#signupName').blur(function () {
                if (!$('#signupName').attr('readonly')) {
                    var signupName = $(this).val();
                    if (!/^[a-zA-Z0-9]+$/.test(signupName)) {
                        $("#msg").html('Only Alphanumeric characters are allowed.').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                        $("#signupNameCheck").val(0);
                        return false;
                    }
                    else if (signupName.trim() == "support") {
                        $("#msg").html('Support cannot be set as signup name.').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                        $("#signupNameCheck").val(0);
                        return false;
                    }
                    else if (signupName.trim() == "admin") {
                        $("#msg").html('Admin cannot be set as signup name.').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                        $("#signupNameCheck").val(0);
                        return false;
                    }
                    if (signupName == "" || signupName.length < 2) {
                        $("#msg").html('');
                    }
                    else {
                        $.ajax({
                            url: "../../classes/ajax.php",
                            type: "post",
                            data: {action: 'checkLeaderSignupName', us: $(this).val()},
                            success: function (result) {
                                if (result == 0) {
                                    $("#msg").html('Signup Name is Available').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                                    $("#signupNameCheck").val(result);
                                }
                                else {
                                    $("#msg").html('This signup Name is Unavailable. Please try another one.').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                                    $("#signupNameCheck").val(result);
                                }
                            }
                        });
                    }
                }
            });
            $('#txtCheckboxCount').blur(function (e) {
                if ($(this).val() > 0) {
                    var template = $('#txtCheckboxTemplate').html();
                    var lastCount = parseInt($('#txtCheckboxCountLast').val());
                    var newValue = parseInt($(this).val());
                    var thisValue = parseInt(0);
                    var i, count = 0;
                    if (lastCount == 0) {
                        $('tr.checkbox td:nth-child(2)').html('');
                    }
                    if (newValue > lastCount) {
                        thisValue = newValue - lastCount;
                        for (i = 0; i < thisValue; i++) {
                            $('tr.checkbox td:nth-child(2)').append(template);
                        }
                    }
                    else if (newValue < lastCount) {
                        thisValue = lastCount - newValue;
                        $('tr.checkbox td input:text').each(function (index, element) {
                            count++;
                            if (count > newValue) {
                                $(this).remove();
                            }
                        });
                    }
                    $('#txtCheckboxCountLast').val(newValue);
                    $('.radiobtn').hide();
                    $('.checkbox').show();
                    $('.radio').hide();
                }
                else {
                    $('tr.checkbox td:nth-child(2)').html('<div class="displayBlock">&nbsp;</div>');
                    $('#txtCheckBoxBtnCountLast').val('0');
                }
            });
            $('#txtRadioBtnCount').blur(function (e) {
                if ($(this).val() > 0) {
                    var template = $('#txtRadioBtnTemplate').html();
                    var lastCount = parseInt($('#txtRadioBtnCountLast').val());
                    var newValue = parseInt($(this).val());
                    var thisValue = parseInt(0);
                    var i, count = 0;
                    if (lastCount == 0) {
                        $('tr.radiobtn td:nth-child(2)').html('');
                    }
                    if (newValue > lastCount) {
                        thisValue = newValue - lastCount;
                        for (i = 0; i < thisValue; i++) {
                            $('tr.radiobtn td:nth-child(2)').append(template);
                        }
                    }
                    else if (newValue < lastCount) {
                        thisValue = lastCount - newValue;
                        $('tr.radiobtn td input:text').each(function (index, element) {
                            count++;
                            if (count > newValue) {
                                $(this).remove();
                            }
                        });
                    }
                    $('#txtRadioBtnCountLast').val(newValue);
                    $('.checkbox').hide();
                    $('.radiobtn').show();
                    $('.radio').show();
                }
                else {
                    $('tr.radiobtn td:nth-child(2)').html('<div class="displayBlock">&nbsp;</div>');
                    $('#txtRadioBtnCountLast').val('0');
                }
            });
            $('#ddlFieldType').change(function (e) {
                var questionType = $('option:selected', $(this)).text();
                if (questionType == 'checkboxes') {
                    $('.checkboxCount').show();
                    $('.radiobtnCount').hide();
                    $('.radiobtn').hide();
                }
                else if (questionType == 'radiobuttons') {
                    $('.radiobtnCount').show();
                    $('.checkboxCount').hide();
                    $('.checkbox').hide();
                }
                else if (questionType == 'textbox') {
                    $('.radiobtnCount').hide();
                    $('.checkboxCount').hide();
                    $('.checkbox').hide();
                    $('.radiobtn').hide();
                }
            });
            $('#btnCancelNewField').click(function (e) {
                $('#existingField').show();
                $('#newField').hide();
            });
            $("#btnCancelQuestion").click(function (e) {
                location.href = "settings.php";
            });
        });
        function saveSignupName(idFrom, idTo) {
            var signupName = $("#signupName").val();
            if (signupName == "" || signupName.length < 2) {
                $("#msg").html('At least 2 character signup name is required').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                return false;
            }
            else {
                if ($("#signupNameCheck").val() == 0) {
                    $.ajax({
                        url: "../../classes/ajax.php",
                        type: "post",
                        data: {action: 'saveLeaderSignupName', us: signupName},
                        success: function (result) {
                            $("#msg").html('Signup Name is updated.').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                        }
                    });
                    return true;
                }
                else {
                    $("#msg").html('This signup Name is Unavailable. Please try another one.').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                    return false;
                }
            }
        }
        function enableSignupName(idFrom, idTo) {
            if ($('#signupName').attr('readonly')) {
                $('#signupName').removeAttr('readonly');
                $('#' + idFrom).hide();
                $('#' + idTo).show();
            }
            else {
                if (saveSignupName(idFrom, idTo)) {
                    $('#signupName').attr('readonly', true);
                    $('#' + idFrom).hide();
                    $('#' + idTo).show();
                }
            }
        }
        function editField(id, fieldName) {
            $('#edit' + id).hide();
            $('#save' + id).show();
            $('#txt' + fieldName).css('border', '1px solid gray');
            $('#txt' + fieldName).css('background-color', 'white');
            $('#txt' + fieldName).focus();
            $('#txt' + fieldName).attr('disabled', false);
        }
        function saveField(id, fieldName) {
            var fieldValue = $("#txt" + fieldName).val();
            if (fieldValue == "" || fieldValue.length < 2) {
                $("#msg" + id).html('At least 2 character name is required').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
                return false;
            }
            else {
                $.ajax({
                    url: "../../classes/ajax.php",
                    type: "post",
                    data: {action: 'saveLeaderDefaultFields', us: fieldValue, id: id},
                    success: function (result) {
                        $("#msg" + id).html('Field Name is updated.').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                    }
                });
            }
            $('#edit' + id).show();
            $('#save' + id).hide();
            $('#txt' + fieldName).css('border', '0px');
            $('#txt' + fieldName).css('background-color', '#f7f6f6');
            $('#txt' + fieldName).attr('disabled', true);
        }
        function toggleStatus(id, legend) {
            var thisVal = $("#status" + id).html().trim();
            var newVal = (thisVal == 'Inactive' ? 'Active' : 'Inactive').trim();
            $("#status" + id).html('<img src="../../images/loading.gif" width="28" height="28">');
            $.ajax({
                url: "../../classes/ajax.php",
                type: "post",
                data: {action: 'toggleLeaderDefaultFields', id: id, legend: legend},
                success: function (result) {
                    $("#msg").html('Field is updated.').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
                    $("#status" + id).html(newVal);
                }
            });
        }
        function RequestAllowsignup(status) {

//            if (document.getElementById('allow').checked) {
                $("#signupAction").html('<img src="../../images/loading.gif" width="18" height="18">');
               // var status = 1;
                $.ajax({
                    url: "../../classes/ajax.php",
                    type: "post",
                    data: {action: 'RequestAllowsignup', status: status},
                    success: function (result) {
                        $('#statusmessage').hide();
                        if (status==1){
                        $("#errormessage").html(' Request pending').css('color', '#EB939A').show('slow');
                        $("#errormessage").html(' Request pending').css('font-style','italic').show('slow');
                        $("#signupAction").html('').css('display','none').show('slow');
                        }else {


                            $("#signupAction").html('<a id="allowsignUp" onClick="RequestAllowsignup(1)">Request</a>').show('slow');
                        }

                    }
                });
//            }

//            else {
//                var status = 0;
//                $.ajax({
//                    url: "../../classes/ajax.php",
//                    type: "post",
//                    data: {action: 'RequestAllowsignup', status: status},
//                    success: function (result) {
//                        $('#statusmessage').hide();
//                        $("#errormessage").html('Disabled ').css('color', 'red').show('slow').delay(5000).fadeOut('slow');
//                    }
//                });
//
//            }
        }

        function addNewField() {
            $('#existingField').hide();
            $('#newField').show();
        }
        function editQuestion(questionValue, questionId, typeId, isMandatory) {
            $('#editQuestionValue').val(questionValue);
            $('#editQuestionId').val(questionId);
            $('#editTypeId').val(typeId);
            $('#editIsMandatory').val(isMandatory);
            $('#frmEditQuestion').submit();
        }
        function validateForm(frmId) {
            if (frmId == 'frmCustomFields') {
                if ($("#txtFieldLabel").val().length == 0) {
                    $("#txtFieldLabelWarning").show();
                    $("#txtFieldLabel").focus();
                    return false;
                }
                else {
                    $("#txtFieldLabelWarning").hide();
                }
                if ($("#ddlFieldType").val() == 0) {
                    $("#ddlFieldTypeWarning").show();
                    $("#ddlFieldType").focus();
                    return false;
                }
                else {
                    $("#ddlFieldTypeWarning").hide();
                }
                if (!$("input[name='rdMandatory']").is(":checked")) {
                    $("#rdMandatoryWarning").show();
                    return false;
                }
                else {
                    $("#rdMandatoryWarning").hide();
                }
            }
        }
        function copyToClipboard(text) {
			var myrul = document.getElementById('signupName').value;
			var mycompleteur = 'http://techgeese.com/signup/'+myrul;
            window.prompt("Copy to clipboard: Ctrl+C, Enter", mycompleteur);
        }
    </script>
</div>
