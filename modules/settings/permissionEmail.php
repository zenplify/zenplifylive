<?php
	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');


	$settings = new settings();
	$database = new database();
	$data = $settings->getPermissionEmail($userId, $database);
?>
<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<div class="rightMiddleContent">
		
		<div>
			<h1 class="gray" style="display: inline-block;">Permission Email</h1>
           <div id="message" style="display: inline-block; width: 400px; font-size: 12px; text-align: center;"></div>
			<input type="button" class="save" id="btnSave" style="display: inline-block;float: right;margin-top: 10px;" value=""/>
            <span style="#6D6262;font-size: 12px;">Permission Email Subject</span>
            <input type="text" class="permissionEmailSubject" style="width: 692px;height: 30px;border: 1.5px solid #ccc;margin-bottom: 5px; border-radius: 3px;font-size: 16px;" id="permissionEmailSubject"  value="<?php if($data != "") { echo $data->permissionEmailSubject; }?>"/>
            <span style="#6D6262;font-size: 12px;">Permission Email Body</span>
            <textarea id="permissionEmail"><?php if($data != "") { echo $data->permissionEmailBody; }?></textarea>
			<input id="permissionEmailId" type="hidden" value="<?php if($data != "") { echo $data->permissionEmailId; }?>">
		</div>
	</div>
	<div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
	<script src="../../lib/ckeditor/ckeditor.js"></script>
	<script>
		CKEDITOR.replace( 'permissionEmail' );
		$('#btnSave').on('click', function() {
			var permissionEmail = CKEDITOR.instances['permissionEmail'].getData();
			var permissionEmailId = $('#permissionEmailId').val();
            var permissionEmailSubject= $('#permissionEmailSubject').val();
			if (permissionEmail == '' || permissionEmailSubject =='') {
				$('#message').html('<p style="color: red;">Please provide valid data.</p>').show().delay(3000).fadeOut();
			} else {
				
				$.ajax({
					type: 'POST',
					url: '../../classes/ajax.php',
					data: {action: 'savePermissionEmail', permissionEmail: permissionEmail, permissionEmailId: permissionEmailId,permissionEmailSubject: permissionEmailSubject},
					success: function(response) {
						console.log(response);
						response = JSON.parse(response);
						$('#message').html('<p style="color: green;">' + response.message + '</p>').show().delay(3000).fadeOut();
					}
				});
			}
		});

	</script>
</div>
</div>