<?php

	include_once("../headerFooter/header.php");
	include_once("../../classes/init.php");
	require_once('../../classes/settings.php');
	require_once('../../classes/contact.php');
	include_once("../../classes/group.php");
	include_once("../../classes/notification.php");
	$settings = new settings();
	$contact = new contact();
	$group = new group();
	$notification = new notification();

	$database = new database();

	///$userDetails = $contact->GetUserDetails($userId);
	//$upComingtasks = $settings->getUpcomingTasks($userId);
	//$currentUpcomingTask = $userDetails->upComingtasks;
?>
<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$("#tblarchive").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
//		$("#tblProfile").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
//		$("#tblGroup").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
//		$("#tblPlan").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
//		$("#tblStep").tablesorter({}).tablesorterPager({container: $("#pager"), size: 50});
	});
	function unArchive(id, type, unarchiveTag) {
		$("#unarchive_" + unarchiveTag).html('<img style="" src="../../images/loading.gif" width="18" height="18">');
		$.ajax({ url: '../../classes/ajax.php',
			data    : {action: 'markunArchive', id: id, type: type},
			type    : 'post',
			success : function (output) {
				$("#unarchive_" + unarchiveTag).slideUp(function () { $(this).closest('tr').remove(); });
				console.log(output);
			}
		});
	}
	function submitFrm(e, frmId) {
		if (e.keyCode == 13) {
			if ($("#txtsearch").val()) {
				$("#" + frmId).submit();
			}
		}
	}
</script>
<div class="container">
	<?php include_once("settingsMenu.php"); ?>
	<div class="rightMiddleContent">
		<div id="existingField">
			<h1 class="gray">Archive</h1>

			<form name="frmSearch" id="frmSearch" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				<input type="text" name="txtsearch" id="txtsearch" placeholder="Search Text" value="<?php echo $_REQUEST['txtsearch']; ?>" onkeypress="submitFrm(event,'frmSearch');">
			</form>
			<div class="clear" style="height:15px;"><span id=_"msg">&nbsp;</span></div>
			<?php
				//				1:profile,2:group,3:campaign,4:question,5:step
				$archivedProfile = $notification->getArchivedData($database, $userId, 1, $_REQUEST['txtsearch']);
				$archivedGroup = $notification->getArchivedData($database, $userId, 2, $_REQUEST['txtsearch']);
				$archivedPlan = $notification->getArchivedData($database, $userId, 3, $_REQUEST['txtsearch']);
				//				$archivedQuestion = $notification->getArchivedData($userId,4);
				$archivedStep = $notification->getArchivedData($database, $userId, 5, $_REQUEST['txtsearch']);

				if ((empty($archivedProfile)) && (empty($archivedGroup)) && (empty($archivedPlan)) && (empty($archivedStep)))
				{
					?>
					<div style="margin-top:30px; margin:0 auto; width: 250px;;">No Archive data</div><?php
				}
				else
				{
					?>
					<table id="tblarchive" cellpadding="0" cellspacing="0" id="tblarchive" width="100%" class="tablesorter">
						<thead>
						<tr class="header">
							<th width="40%" class="">Type<img src="../../images/bg.gif"/></th>
							<th width="40%" class="">Title<img src="../../images/bg.gif"/></th>
							<th width="25%" class="">Action<img src="../../images/bg.gif"/></th>
						</tr>
						</thead>
						<tbody id="">
						<?php
							$count = 0;
							foreach ($archivedProfile as $ap)
							{
								$count++;
								?>
								<tr id="archiveProfile_<?php echo $ap->pageId; ?>">
									<td width="40%" align="left">Profile</td>
									<td width="40%" align="left"><?php echo $ap->newCode; ?></td>
									<td width="25%" align="center" id="unarchive_<?php echo $count; ?>">
										<?php echo "<span class='date_span' style='float:none;'>
										<a href=\"javascript:unArchive('$ap->pageId','1','" . $count . "')\" id='profile_" . $ap->pageId . "'>Restore</a> </span>"; ?>
									</td>
								</tr>
							<?php
							}
							foreach ($archivedGroup as $ag)
							{
								$count++;
								?>
								<tr id="archiveGroup_<?php echo $ag->groupId; ?>">
									<td width="40%" align="left">Group</td>
									<td width="40%" align="left"><?php echo $ag->name; ?></td>
									<td width="25%" align="center" id="unarchive_<?php echo $count; ?>">
										<?php echo "<span class='date_span' style='float:none;'>
										<a href=\"javascript:unArchive('$ag->groupId','2','" . $count . "')\" id='group_" . $ag->groupId . "'>Restore</a> </span>"; ?>
									</td>
								</tr>
							<?php
							}
							foreach ($archivedPlan as $ap)
							{
								$count++;
								?>
								<tr id="archivePlan_<?php echo $ap->planId; ?>">
									<td width="40%" align="left">Plan</td>
									<td width="40%" align="left"><?php echo $ap->title; ?></td>
									<td width="25%" align="center" id="unarchive_<?php echo $count; ?>">
										<?php echo "<span class='date_span' style='float:none;'>
										<a href=\"javascript:unArchive('$ap->planId','3','" . $count . "')\" id='plan_" . $ap->planId . "'>Restore</a> </span>"; ?>
									</td>
								</tr>
							<?php
							}
							foreach ($archivedStep as $as)
							{
								$count++;
								?>
								<tr id="archiveStep_<?php echo $as->stepId; ?>">
									<td width="40%" align="left">Step</td>
									<td width="40%" align="left"><?php echo $as->summary; ?></td>
									<td width="25%" align="center" id="unarchive_<?php echo $count; ?>">
										<?php echo "<span class='date_span' style='float:none;'>
										<a href=\"javascript:unArchive('$as->stepId','5','" . $count . "')\" id='step_" . $as->stepId . "'>Restore</a> </span>"; ?>
									</td>
								</tr>
							<?php
							}
						?>
						</tbody>
					</table>
					<div id="pager" class="pager">
						<form>
							<span class="total"></span>
							<img src="../../images/paging_far_left.gif" class="first"/>
							<img src="../../images/paging_left.gif" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="../../images/paging_right.gif" class="next"/>
							<img src="../../images/paging_far_right.gif" class="last"/>
							<select id="my_pagesize" name="pagesize" class="pagesize">
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="250">250</option>
							</select>
						</form>
					</div>
				<?php
				}

			?>
		</div>
	</div>
	<div class="clear"></div>
	<?php include("../headerFooter/footer.php"); ?>
</div>
</div>
