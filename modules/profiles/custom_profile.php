<?php

$details = $profile->getUserPageDetails($database, $_REQUEST['code'], $_REQUEST['user']);
if ($details->leaderId == 0) {
    $consultantTitleUserId = $details->userId;
} else {
    $consultantTitleUserId = $details->leaderId;
}
$consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');

$code = $details->newCode;
$url = $profile->curPageURL();
$viewId = $profile->PageView($database, $txtPageId, $url, $code, $details->userId, $_SERVER['HTTP_USER_AGENT']);
$templateQuestions = $profile->getPageQuestions($database, $txtPageId, 1, 1);
$quest = $profile->getPageQuestions($database, $txtPageId, -1, 1);
$customsQuestionStatus = $profile->getCustomQuestionStatus($database, $txtPageId);
$customMailQuestionsStatus = $profile->getcustomMailQuestionStatus($database, $txtPageId);
?>
<head>
    <base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"/>
</head>

<?php echo str_replace('<<Page Title>>', $details->pageBrowserTitle, $details->templateHTML);
$questionCount = 0;
$count = 0;
$totalCount = 0;
if ($details->columnCount == 1) {
    $class = 'grid_100';
} elseif ($details->columnCount == 2) {
    $class = 'grid_50';
} elseif ($details->columnCount == 3) {
    $class = 'grid_33';
}

$questionCount = $details->columnCount;
$font = $profile->getFontStyleNamebyId($database, $details->profileFontStyle);
$bgColor = $profile->getProfileColorById($database, $details->profilebgColor);
$fontColor = $profile->getProfileColorById($database, $details->profileFontColor);
$accentColor = $profile->getProfileColorDetail($database, $details->accentColor);
?>
<script type="text/javascript">
    function submitFrm() {
        var fewSeconds = 5;
        $('#questionform').submit(function (e) {
            console.log($(".required").val());


            $("#submitBtn").attr('disabled', true);
            setTimeout(function () {
                $("#submitBtn").attr('disabled', false);
            }, fewSeconds * 1000);

            // $("#submitBtn").attr('disabled',true);

            //

        });
    }
</script>

<!-- -->
<script type="text/javascript">
    var fewSeconds = 10;
    $('#submitButton').click(function () {
        // Ajax request
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
        }, fewSeconds * 1000);
    });
</script>
<!-- -->
<style>
    table td {
        font-family: <?php echo $font; ?>;
    <!-- background-color : --> <?php //echo $bgColor; ?> <!--;
    --> <!-- color : --> <?php //echo $fontColor; ?> <!--;
    -->
    }
</style>
<div class="container_16" id="box"
     style="font-family:<?php echo $font; ?>; background-color: <?php echo $bgColor; ?>; color:<?php echo $fontColor; ?>; ">
<div id="topbar"
     style="background-image: url('../../images/profile/<?php echo $details->profileHeader; ?>'); height: 100px;">
    &nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">&nbsp;</div>
<div class="grid_16 spacer">&nbsp;</div>
<?php if ($details->isDefaultVideoshow==1) { $classForVideo='grid_6'; ?>
<div class="grid_10" style="margin-bottom: 10px;">
    <?php if ($leaderId == 3741 || $userId == 3741) {
        $autoPlay = 0;
    } else {
        $autoPlay = 0;
    } ?>
    <?php if (!empty($details->profileEditedVideo)) {
        $profileVideo = $details->profileEditedVideo;
    } else {
        $profileVideo = $details->profileVideoLink;
    } ?>
    <iframe allowfullscreen="" frameborder="0" height="315"
            src="//www.youtube.com/embed/<?php echo $profileVideo; ?>?rel=0;autoplay=<?php echo $autoPlay; ?>;theme=light;color=white;showinfo=0"
            width="560"></iframe>
</div>

<?php } else{$classFoatLeft='float:left; width:97%;';$classForVideo='grid_12'; } ?>
<div class="<?php echo $classForVideo; ?>" style="<?php echo $classFoatLeft; ?>">
    <div class="texttitle">
        <!--		<h1>Discover Arbonne</h1>-->

        <?php // $class = '';
        if (($details->userId == '3658' || $details->leaderId == '3658') && $details->pageName == 'Guest Profile') {
            $class1 = 'custom-title';
        }
        ?>
        <h1 style="color:  <?php echo $accentColor->actualName; ?>;"><?php echo $details->pageBrowserTitle; ?></h1>
    </div>
    <div class="pagetext">
        <?php $prfileWelcomeText= str_replace("[{First Name}]","$details->firstName","$details->profileDefaultText");
        $prfileWelcomeText= str_replace("[{Last Name}]","$details->lastName","$prfileWelcomeText");?>
        <p><?php echo $prfileWelcomeText; ?></p>
    </div>
</div>
<div class="greenbg"
     style="height:1px; float:left; width:100%; background-color: <?php echo $accentColor->actualName; ?>;">&nbsp;</div>
<div class="grid_6 signature">
    <?php echo $details->firstName . ' ' . $details->lastName;
    if ($details->title != '') {
        echo ', ' . $details->title;
    }?>
    <br/>
    <?php
    echo $details->consultantId . '<br/>';


    ?>

    <a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->webAddress, "https://") != false)
        echo $details->webAddress;
    else echo 'http://' . $details->webAddress; ?>"
       target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
    <a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->facebookAddress, "https://") != false)
        echo $details->facebookAddress;
    else echo 'http://' . $details->facebookAddress; ?>"
       target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br/>
    <a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br/>
    <?php echo $details->phoneNumber; ?></div>
<div class="greenbg"
     style="height:1px; float:left; width:100%; background-color: <?php echo $accentColor->actualName; ?>;">
    &nbsp;</div>
<div class="grid_16 texttitle">
    <h1 style="color:  <?php echo $accentColor->actualName; ?>;"><?php echo $details->pageBrowserTitle; ?></h1>
</div>
<form action="<?php echo $CONF["siteURL"]; ?>modules/profiles/profileActionCustom.php" id="questionform" method="post">
<input type="hidden" value="<?php echo $details->pageId; ?>" name="pageId"/>
<input type="hidden" value="<?php echo $details->userId; ?>" name="userId"/>
<input type="hidden" value="<?php echo $details->pageName; ?>" name="pageName"/>
<input type="hidden" value="<?php echo $code; ?>" name="code"/>
<input type="hidden" value="<?php echo $url; ?>" name="pageUrl"/>
<input type="hidden" value="<?php echo $viewId; ?>" name="viewId"/>
<input type="hidden" value="<?php echo $details->pageBrowserTitle; ?>" name="pageBrowserTitle"/>

<div class="clear">&nbsp;</div>
<div>
<div class="equalize">
<div class="grid_6 questionbox" style="width: 370px;">
<div style="padding:0 10px 0 10px;">
<div class="boxtitle"
     style="background-color: <?php echo $accentColor->actualName; ?>;<?php if ($accentColor->actualName == 'black'
         || $accentColor->actualName == 'green'
         || $accentColor->actualName == 'blue'
         || $accentColor->actualName == 'yellowgreen'
         || $accentColor->actualName == 'skyblue'
         || $accentColor->actualName == '#51004d'
     ) {
         echo 'color:white;';
     }?>; margin-left: -10px; margin-right: -10px;">Contact Information:
</div>
<div class="errorMessage">&nbsp;</div>
<table border="0" cellpadding="0" width="100%">
<tbody>
<tr>
    <td align="right" width="22%">
        Name:<span class="requiredstar">*</span></td>
    <td width="39%">
        <input class="required" name="firstName" type="text"/></td>
    <td width="39%">
        <input class="required" name="lastName" type="text"/></td>
</tr>
<tr>
    <td align="right">
        Email:<span class="requiredstar">*</span></td>
    <td colspan="2">
        <input class="required email" name="email" type="text"/></td>
</tr>
<?php foreach ($customsQuestionStatus as $s) {

    if ($s->questionId == '121' && $s->status == '1') {
        ?>
        <tr>
            <td align="right">
                Phone:<span class="requiredstar">*</span></td>
            <td colspan="2">
                <input class="required" name="phone" type="text"/></td>
        </tr>
    <?php
    } else if ($s->questionId == '56' && $s->status == '1') {
        ?>
        <tr>
            <td align="right">
                Birthday: <?php  if ($s->mandatory == 1) {
                    ?><span class="requiredstar">*</span> <?php } ?></td>
            <td colspan="2">
                <select <?php  if ($s->mandatory == 1) {
                        ?>class="required" <?php } ?> id="birthday_month" name="birthday_month">
                    <option value="">Month</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
                </select>
                <select <?php  if ($s->mandatory == 1) {
                        ?>class="required" <?php } ?>  id="birthday_day" name="birthday_day">
                    <option value="">Day:</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                    <option value="05">5</option>
                    <option value="06">6</option>
                    <option value="07">7</option>
                    <option value="08">8</option>
                    <option value="09">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                </select>
                <select <?php  if ($s->mandatory == 1) {
                        ?>class="required" <?php } ?>  id="birthday_year" name="birthday_year">
                    <option value="">Year:</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>
                    <option value="2010">2010</option>
                    <option value="2009">2009</option>
                    <option value="2008">2008</option>
                    <option value="2007">2007</option>
                    <option value="2006">2006</option>
                    <option value="2005">2005</option>
                    <option value="2004">2004</option>
                    <option value="2003">2003</option>
                    <option value="2002">2002</option>
                    <option value="2001">2001</option>
                    <option value="2000">2000</option>
                    <option value="1999">1999</option>
                    <option value="1998">1998</option>
                    <option value="1997">1997</option>
                    <option value="1996">1996</option>
                    <option value="1995">1995</option>
                    <option value="1994">1994</option>
                    <option value="1993">1993</option>
                    <option value="1992">1992</option>
                    <option value="1991">1991</option>
                    <option value="1990">1990</option>
                    <option value="1989">1989</option>
                    <option value="1988">1988</option>
                    <option value="1987">1987</option>
                    <option value="1986">1986</option>
                    <option value="1985">1985</option>
                    <option value="1984">1984</option>
                    <option value="1983">1983</option>
                    <option value="1982">1982</option>
                    <option value="1981">1981</option>
                    <option value="1980">1980</option>
                    <option value="1979">1979</option>
                    <option value="1978">1978</option>
                    <option value="1977">1977</option>
                    <option value="1976">1976</option>
                    <option value="1975">1975</option>
                    <option value="1974">1974</option>
                    <option value="1973">1973</option>
                    <option value="1972">1972</option>
                    <option value="1971">1971</option>
                    <option value="1970">1970</option>
                    <option value="1969">1969</option>
                    <option value="1968">1968</option>
                    <option value="1967">1967</option>
                    <option value="1966">1966</option>
                    <option value="1965">1965</option>
                    <option value="1964">1964</option>
                    <option value="1963">1963</option>
                    <option value="1962">1962</option>
                    <option value="1961">1961</option>
                    <option value="1960">1960</option>
                    <option value="1959">1959</option>
                    <option value="1958">1958</option>
                    <option value="1957">1957</option>
                    <option value="1956">1956</option>
                    <option value="1955">1955</option>
                    <option value="1954">1954</option>
                    <option value="1953">1953</option>
                    <option value="1952">1952</option>
                    <option value="1951">1951</option>
                    <option value="1950">1950</option>
                    <option value="1949">1949</option>
                    <option value="1948">1948</option>
                    <option value="1947">1947</option>
                    <option value="1946">1946</option>
                    <option value="1945">1945</option>
                    <option value="1944">1944</option>
                    <option value="1943">1943</option>
                    <option value="1942">1942</option>
                    <option value="1941">1941</option>
                    <option value="1940">1940</option>
                    <option value="1939">1939</option>
                    <option value="1938">1938</option>
                    <option value="1937">1937</option>
                    <option value="1936">1936</option>
                    <option value="1935">1935</option>
                    <option value="1934">1934</option>
                    <option value="1933">1933</option>
                    <option value="1932">1932</option>
                    <option value="1931">1931</option>
                    <option value="1930">1930</option>
                    <option value="1929">1929</option>
                    <option value="1928">1928</option>
                    <option value="1927">1927</option>
                    <option value="1926">1926</option>

                    <option value="1925">1925</option>
                    <option value="1924">1924</option>
                    <option value="1923">1923</option>
                    <option value="1922">1922</option>
                    <option value="1921">1921</option>
                    <option value="1920">1920</option>
                    <option value="1919">1919</option>
                    <option value="1918">1918</option>
                    <option value="1917">1917</option>
                    <option value="1916">1916</option>
                    <option value="1915">1915</option>
                    <option value="1914">1914</option>
                    <option value="1913">1913</option>
                    <option value="1912">1912</option>
                    <option value="1911">1911</option>
                    <option value="1910">1910</option>
                    <option value="1909">1909</option>
                    <option value="1908">1908</option>
                    <option value="1907">1907</option>
                    <option value="1906">1906</option>
                    <option value="1905">1905</option>
                </select>
            </td>
        </tr>
    <?php
    } else if ($s->questionId == '122' && $s->status == '1') {
        ?>
        <tr>
            <td align="right">
                Referred By:<span class="requiredstar">*</span></td>
            <td colspan="2">
                <input class="required" name="referredBy" type="text"/></td>
        </tr>
    <?php
    } else if ($s->questionId == '59' && $s->status == '1') {
        ?>
        <tr>
            <td align="right">
                Best Times<br/>
                To Reach You<span class="requiredstar">*</span></td>
            <td>
                <input class="required" name="bestTimeToCall[]" type="checkbox" value="Weekdays" required/>Weekdays<br/>
                <input name="bestTimeToCall[]" type="checkbox" value="Weekday Evenings" required/>Weekday Evenings
            </td>
            <td>
                <input name="bestTimeToCall[]" type="checkbox" value="Weekend Days" required/>Weekend Days<br/>
                <input name="bestTimeToCall[]" type="checkbox" value="Weekend Evenings" required/>Weekend Evenings
            </td>
        </tr>
    <?php
    }
}
?>
</tbody>
</table>
</div>
</div>
<?php if (!empty($customMailQuestionsStatus)) {
    ?>
    <div class="grid_10 questionbox" style="width: 550px;">
        <div class="boxtitle"
             style="background-color: <?php echo $accentColor->actualName; ?>;<?php if ($accentColor->actualName == 'black'
                 || $accentColor->actualName == 'green' ||
                 $accentColor->actualName == 'blue' ||
                 $accentColor->actualName == 'yellowgreen' ||
                 $accentColor->actualName == 'skyblue' ||
                 $accentColor->actualName == '#51004d'
             ) {
                 echo 'color:white;';
             }?>;">
            Mailing Address:
        </div>
        <div style="padding:0 10px 0 10px;">
            <div class="errorMessage">
                &nbsp;</div>
            <table border="0" cellpadding="0" width="100%">
                <tbody>
                <?php foreach ($customsQuestionStatus as $s) {
                    if ($s->questionId == '123' && $s->status == '1') {
                        ?>
                        <tr>
                            <td align="right" width="20%">
                                Street Address:<span class="requiredstar">*</span></td>
                            <td width="80%">
                                <input class="required" name="street" type="text"/></td>
                        </tr>
                    <?php
                    } else if ($s->questionId == '58' && $s->status == '1') {
                        ?>
                        <tr>
                            <td align="right" width="20%">
                                City:<span class="requiredstar">*</span></td>
                            <td width="80%">
                                <input class="required" name="city" type="text"/></td>
                        </tr>
                    <?php
                    } else if ($s->questionId == '124' && $s->status == '1') {
                        ?>
                        <tr>
                            <td align="right" width="20%">
                                State/Prov:<span class="requiredstar">*</span></td>
                            <td width="80%">
                                <input class="required" name="state" type="text"/></td>
                        </tr>
                    <?php
                    } else if ($s->questionId == '125' && $s->status == '1') {
                        ?>
                        <tr>
                            <td align="right" width="20%">
                                Zip/Postal:<span class="requiredstar">*</span></td>
                            <td width="80%">
                                <input class="required" name="zipCode" type="text"/></td>
                        </tr>
                    <?php
                    } else if ($s->questionId == '57' && $s->status == '1') {
                        ?>
                        <tr>
                            <td align="right" width="20%">
                                Country:<span class="requiredstar">*</span></td>
                            <td width="80%">
                                <input class="required" name="country" type="text"/></td>
                        </tr>
                    <?php
                    }
                }?>

                </tbody>
            </table>
        </div>
    </div>
<?php } ?>
<div class="clear"></div>
<div class="questionMargin">
    <?php foreach ($quest as $q)
        if ($q->typeId == 1) {
            ?>
        <div class="<?php echo $class;
        if ($count != '0') {
            echo ' secondQuestionMargin ';
        } ?> floatLeft">
            <div class="questiontext" style="color: <?php echo $accentColor->actualName; ?>"><?php echo $q->question;
                if ($q->mandatory == 1) {
                    ?><span class="requiredstar">*</span> <?php } ?></div>
            <input type="text" class="<?php if ($q->mandatory == 1) {
                echo 'required';
            } ?>" name="<?php echo 'question' . $q->questionId; ?>">
            </div><?php
            $count++;
            if ($count == $questionCount) {
                $count = 0; ?>
                <div class="clear questionBlock">&nbsp;</div> <?php
            }
        } // text
        elseif ($q->typeId == 2) {
            ?>
        <div class="<?php echo $class;
        if ($count != '0') {
            echo ' secondQuestionMargin ';
        } ?> floatLeft">
            <div class="questiontext" style="color: <?php echo $accentColor->actualName; ?>"><?php echo $q->question;
                if ($q->mandatory == 1) {
                    ?><span class="requiredstar">*</span> <?php } ?></div>
            <textarea class="<?php if ($q->mandatory == 1) {
                echo 'required';
            } ?>" name="<?php echo 'question' . $q->questionId; ?>" rows="6"></textarea>
            </div><?php
            $count++;
            if ($count == $questionCount) {
                $count = 0; ?>
                <div class="clear questionBlock">&nbsp;</div> <?php
            }
        } //textarea
        elseif ($q->typeId == 3) {
            ?>
            <div class="<?php echo $class;
            if ($count != '0') {
                echo ' secondQuestionMargin ';
            } ?> floatLeft">
                <div class="questiontext"
                     style="color: <?php echo $accentColor->actualName; ?>"><?php echo $q->question;
                    if ($q->mandatory == 1) {
                        ?><span class="requiredstar">*</span> <?php } ?></div>
                <?php $choices = $profile->getQuestionChoices($database, $q->questionId);
                $i = 0;
                foreach ($choices as $c) {
                    if ($i % 3 == 0) {
                        echo '</tr><tr>';
                    }
                    echo '<td><input ';
                    if (($i == 0) && ($q->mandatory == 1))
                        echo 'class="required"';
                    echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '</br>';
                    $i++;
                }
                ?>
            </div>
            <?php
            $count++;
            if ($count == $questionCount) {
                $count = 0; ?>
                <div class="clear questionBlock">&nbsp;</div> <?php
            }
        } //checkbox
        elseif ($q->typeId == 4) {
            ?>
        <div class="<?php echo $class;
        if ($count != '0') {
            echo ' secondQuestionMargin ';
        } ?> floatLeft">
            <div class="questiontext" style="color: <?php echo $accentColor->actualName; ?>"><?php echo $q->question;
                if ($q->mandatory == 1) {
                    ?><span class="requiredstar">*</span> <?php } ?></div>
            <?php $choices = $profile->getQuestionChoices($database, $q->questionId);
            $i = 0;
            foreach ($choices as $c) {
                echo '<input ';
                if (($i == 0) && ($q->mandatory == 1))
                    echo 'class="required"';
                echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
                $i++;
            }
            ?>
            </div><?php
            $count++;
            if ($count == $questionCount) {
                $count = 0; ?>
                <div class="clear questionBlock">&nbsp;</div> <?php
            }
        } //radio
        // test
        elseif ($q->typeId == 5) {
            ?>
            <div class="grid_16 questionbox"
                 style="height: auto !important;min-height: auto !important; margin-left: 0px !important; padding-bottom:9px !important;">
                <div style="padding:0 10px 0 10px;">
                    <div class="errorMessage">
                        &nbsp;</div>
                    <div class="questiontext" style="color: <?php echo $accentColor->actualName; ?>">
                        <?php echo $q->question; ?><span class="requiredstar">*</span></div>

                    <table cellspacing="0" class="qchart">
                        <tbody>
                        <tr class="theader" style="background-color: <?php echo $accentColor->actualName; ?>">
                            <td width="40%">&nbsp;
                            </td>
                            <?php
                            $labels = $profile->getQuestionLabel($database, $q->questionId);
                            foreach ($labels as $ql) {
                                ?>
                                <td width="12%">
                                    <?php echo $ql->label; ?>
                                </td>
                            <?php
                            } ?>
                        </tr>


                        <?php echo '<input type="hidden" value="" name="question' . $q->questionId . '" />';
                        $choices = $profile->getQuestionChoices($database, $q->questionId);
                        $i = 0;
                        foreach ($choices as $c) {

                            if ($i % 2 != 0)
                                echo '<tr class="alt" style="background-color:' . $accentColor->lightColor . '">';
                            else
                                echo '<tr>';
                            echo '<th><label>' . $c->choiceText . '</label></th>';
                            foreach ($labels as $ql) {

                                echo '<td><input style="transform: scale(1.1) !important;" type="radio" value="' . $ql->label . '" name="answer' . $c->choiceId . '" class="required"></td>';
                            }
                            echo '</tr>';
                            //
                            $i++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="grid_16 spacer">
                &nbsp;</div>
        <?php
        }
    //test
    $totalCount++;
    ?>
</div>
<div class="clear spacer_20"></div>
<div class="grid_16">
    <div style="padding:0px;">
        <input class="button" style="background-color: <?php echo $accentColor->lightColor; ?>!important;"
               onclick="submitFrm()" id="submitBtn" type="submit" value="Submit"/>
        <!--		<input class="button" type="submit" id="submitButton" value="Submit"/>-->
    </div>

</div>
<p style="padding-left: 12px;font-size: 11px;margin-top: 0px; color: #887D7D;">Note: After clicking submit, you will see the Thank You screen. If you missed a required question, it will be outlined in red.</p>

<div class="grid_16 spacer_30">&nbsp;</div>
</form>
</div>
</body>
</html>