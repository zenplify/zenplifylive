<?php
session_start();
include_once('../../classes/init.php');
include_once('../../classes/profiles.php');
$database=new database();
$profile=new Profiles();
$details=$profile->getUserPageDetails($database,$_REQUEST['code'],$_REQUEST['user']);
$code = $details->newCode;
$url=$profile->curPageURL();
$viewId=$profile->PageView($database,$details->pageId,$url,$code,$details->userId,$_SERVER['HTTP_USER_AGENT']);
$quest=$profile->getPageQuestions($database,$details->pageId);
?><head><base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"  /></head>
<?php echo $details->templateHTML;
?><div class="grid_6">
<div class="texttitle">
<h1>
				The Arbonne Opportunity</h1>
</div>
<div class="pagetext">
<p>
				Thank you for your interest in the Arbonne Business opportunity. Grab a pen and paper so you can write your questions down as you watch the video and I&#39;ll go through them together during our follow up call.</p>
<p>
				The assessment below will help me develop a strategy together for how you will launch your business if you decide to move forward. There are no wrong answers! I look forward to speaking with you again soon!</p>
<p class="right" style="margin-right:40px;">
				Best regards,<br />
				<?php echo $details->firstName; ?></p>
</div>
</div>
<div class="grid_16 spacer">
		&nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
<div class="grid_6 signature">
		<?php echo $details->firstName.' '.$details->lastName ;
	if ($details->title!='')echo ', '.$details->title;  ?><br />
		Arbonne Consultant <?php echo $details->consultantId; ?><br />
<a href="<?php if(strpos($details->webAddress,"http://") != false || strpos($details->webAddress,"https://") != false  )echo $details->webAddress; else echo 'http://'.$details->webAddress; ?>"  target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
<a href="<?php if(strpos($details->webAddress,"http://") != false  || strpos($details->facebookAddress,"https://") != false  )echo $details->facebookAddress; else echo 'http://'. $details->facebookAddress; ?>" target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br />
<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br />
		<?php echo $details->phoneNumber; ?></div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
<div class="grid_16 texttitle">
<h1>
			Consultant Profile</h1>
</div>
<form action="<?php echo  $CONF["siteURL"];?>modules/profiles/profileAction.php" id="questionform" method="post">
<input type="hidden" value="<?php echo $details->pageId; ?>" name="pageId"  />
<input type="hidden" value="<?php echo $details->userId; ?>" name="userId"  />
<input type="hidden" value="<?php echo $details->pageName; ?>" name="pageName"  />
<input type="hidden" value="<?php echo $code; ?>" name="code"  />
<input type="hidden" value="<?php echo $url; ?>" name="pageUrl" />
<input type="hidden" value="<?php echo $viewId;?>" name="viewId" />
<div class="equalize">
<div class="grid_6 questionbox">
<div class="boxtitle">
					Contact Information:</div>
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<table border="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td align="right" width="20%">
								Name:<span class="requiredstar">*</span></td>
<td width="40%">
<input class="required" name="firstName" type="text" /></td>
<td width="40%">
<input class="required" name="lastName" type="text" /></td>
</tr>
<tr>
<td align="right" width="20%">
								Email:<span class="requiredstar">*</span></td>
<td colspan="2" width="80%">
<input class="required email" name="email" type="text" /></td>
</tr>
<tr>
<td align="right" width="20%">
								Phone:<span class="requiredstar">*</span></td>
<td colspan="2" width="80%">
<input class="required" name="phone" type="text" /></td>
</tr>
</tbody>
</table>
</div>
</div>
<?php foreach($quest as $q)
{
	if($q->order=='1')
	{ ?>
<div class="grid_10 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
 <?php  $choices=$profile->getQuestionChoices($database, $q->questionId);
				$i=0;
					foreach($choices as $c)
					{
						
						
					echo '<tr><td><input ';
					if($i==0)
					echo 'class="required"';
					echo ' name="question'.$q->questionId.'" type="radio" value="'.$c->choiceId.'" /></td><td> '.$c->choiceText.'</td><tr>';
					$i++;
					}
		?>
</tbody>
</table>
</div>
</div>

</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php }?>

<?php if($q->order=='2')
	{ ?>
    <div class="equalize">
<div class="grid_6 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
 
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<input class="required" name="<?php echo 'question'.$q->questionId;?>" type="text" /><br />
 <?php }if($q->order=='3')
	{ ?>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<textarea class="required" name="<?php echo 'question'.$q->questionId;?>" rows="4"></textarea>
</div></div>
<?php } ?>

 <?php if($q->order=='4')
	{ ?>
<div class="grid_10 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<textarea class="required" name="<?php echo 'question'.$q->questionId;?>" rows="6"></textarea></div>
</div>
</div>

<div class="grid_16 spacer">
			&nbsp;</div>
<?php } 
if($q->order=='5')
	{  ?>
<div class="grid_16 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
					&nbsp;</div>
<div class="questiontext">
					<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<table cellspacing="0" class="qchart">
<tbody>
<tr class="theader">
<td width="40%">&nbsp;
								</td>
<td width="12%">
								$50-$200/mo</td>
<td width="12%">
								$200-$1000/mo</td>
<td width="12%">
								$1000-$3500/mo</td>
<td width="12%">
								$3500-$10,000/mo</td>
<td width="12%">
								$10,000/mo+</td>
</tr>
<?php echo '<input type="hidden" value="" name="question'.$q->questionId.'" />';
$choices=$profile->getQuestionChoices($database, $q->questionId);
					$i=0;
					foreach($choices as $c)
					{
						
						if($i%2!=0)
						echo '<tr>';
						else
						echo '<tr class="alt">';
						echo '<th><label>'.$c->choiceText.'</label></th><td><input type="radio" value="$50-$200/mo" name="answer'.$c->choiceId.'" class="required"></td> <td><input type="radio" value="$200-$1000/mo" name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="$1000-$3500/mo" name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="$3500-$10,000/mo" name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="$10,000/mo+" name="answer'.$c->choiceId.'" ></td></tr>';
					
						$i++;
					}
			?>
</tbody>
</table>
</div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php } if($q->order=='6')
	{  ?>

<div class="equalize">
<div class="grid_6 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
 <?php $choices=$profile->getQuestionChoices($database, $q->questionId);
		 
		 $i=0;
		foreach($choices as $c)
		{
			
			
		echo '<input ';
		if($i==0)
		echo 'class="required"';
		echo ' name="question'.$q->questionId.'" type="radio" value="'.$c->choiceId.'" /> '.$c->choiceText.'<br />';
		$i++;
		} ?>
        </div>
</div>
<?php } if($q->order=='7')
	{  ?>
<div class="grid_10 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<textarea class="required" name="<?php echo 'question'.$q->questionId;?>" rows="6"></textarea></div>
</div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php } if($q->order=='8')
	{  ?>
<div class="equalize">
<div class="grid_6 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<textarea class="required" name="<?php echo 'question'.$q->questionId;?>" rows="7"></textarea>
</div>
</div>
<?php } if($q->order=='9')
	{  ?>
<div class="grid_10 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
						&nbsp;</div>
<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<textarea class="required" name="<?php echo 'question'.$q->questionId;?>" rows="6"></textarea></div>
</div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
 <?php } if($q->order=='10')
	{  ?>           
<div class="grid_16 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
					&nbsp;</div>
<div class="questiontext">
					<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<table cellspacing="0" class="qchart">
<tbody>
<tr class="theader">
<td width="40%">&nbsp;
								</td>
<td width="12%">
								Aargh... Scary!!!</td>
<td width="12%">
								Umm...I can try.</td>
<td width="12%">
								Sure...I can do that.</td>
<td width="12%">
								Piece of Cake!!</td>
</tr>
<?php echo '<input type="hidden" value="" name="question'.$q->questionId.'" />';
$choices=$profile->getQuestionChoices($database, $q->questionId);
					$i=0;
					foreach($choices as $c)
					{
						
						if($i%2!=0)
						echo '<tr>';
						else
						echo '<tr class="alt">';
						echo '<th><label>'.$c->choiceText.'</label></th><td><input type="radio" value="Aargh... Scary!!!" name="answer'.$c->choiceId.'" class="required"></td> <td><input type="radio" value="Umm...I can try." name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="Sure...I can do that." name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="Piece of Cake!!" name="answer'.$c->choiceId.'" ></td></tr>';
					
						$i++;
					}
			?>
            </tbody>
</table>
</div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php } if($q->order=='11')
	{  ?>
<div class="grid_16 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
					&nbsp;</div>
<div class="questiontext">
					<?php echo $q->question; ?><span class="requiredstar">*</span></div>
<table cellspacing="0" class="qchart">
<tbody>
<tr class="theader">
<td width="40%">&nbsp;
								</td>
<td width="12%">
								Aargh... Scary!!!</td>
<td width="12%">
								Umm...I can try.</td>
<td width="12%">
								Sure...I can do that.</td>
<td width="12%">
								Piece of Cake!!</td>
</tr>
<?php echo '<input type="hidden" value="" name="question'.$q->questionId.'" />';
$choices=$profile->getQuestionChoices($database, $q->questionId);
					$i=0;
					foreach($choices as $c)
					{
						
						if($i%2!=0)
						echo '<tr>';
						else
						echo '<tr class="alt">';
						echo '<th><label>'.$c->choiceText.'</label></th><td><input type="radio" value="Aargh... Scary!!!" name="answer'.$c->choiceId.'" class="required"></td> <td><input type="radio" value="Umm...I can try." name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="Sure...I can do that." name="answer'.$c->choiceId.'" ></td><td><input type="radio" value="Piece of Cake!!" name="answer'.$c->choiceId.'" ></td></tr>';
					
						$i++;
					}
			?>
</tbody>
</table>
</div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php } if($q->order=='12')
	{  ?>

<div class="grid_16 questionbox">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
					&nbsp;</div>
<div class="questiontext">
					<?php echo $q->question; ?></div>
<textarea name="<?php echo 'question'.$q->questionId;?>" rows="3"></textarea></div>
</div>
<div class="grid_16 spacer">
			&nbsp;</div>
<?php }

}?>
<div class="grid_16">
<div style="padding:0 10px 0 10px;">
<div class="errorMessage">
					&nbsp;</div>
<input class="button" type="submit" value="Submit" /></div>
</div>
<div class="grid_16 spacer_30">
			&nbsp;</div>
</form>
</div>
<p>&nbsp;
	</p>
</body></html>