<?php 
include_once("header.php");
?>
         <script>
	$(document).ready(function() {
		   
		
		$('#new_survery').click(function(){
			loadPopupSurvey();
			centerPopupSurvey()
		});
		$('.edit_link').click(function(){
			
			loadPopupEditLink();
			centerPopupEditLink()
		});
		$("#backgroundPopup").click(function(){
			disablePopupSurvey();
			disablePopupEditLink();
			
		});
		$('#cancel').click(function(){
			disablePopupSurvey();
			
		});
		$('.cancel_2').click(function(){
			disablePopupEditLink();
			
		});
		$('#next_Survey').click(function(){
			window.location='survey_template.php';
			
		});
		$('.edit_profile').click(function(){
			window.location='edit_profile.php';
			
		});
		$('.delete_profile').click(function(){
			alert('Are you sure to delete this profile');
			
		});
		
    });
  
    </script>
   
 
<!--  <div id="menu_line"></div>-->
  <div class="container">
      <div class="top_content2">
      
       <h1 class="gray">Profiles</h1>
        
      </div>
      <div id="backgroundPopup"></div>
      <div id="popupSurvey">
      <h1>Create New Profile</h1>
      <form>
      <label class="survey_label">profile Name</label>
      <input type="text" class="textfield" name="surveyName" id="surveyName" />
      <br />
      <input type="button" class="next_Survey" id="next_Survey" /> <input type="button" id="cancel" style="margin-left:10px;" />
      </form>
      </div>
      <div id="backgroundPopup"></div>
      <div id="popupEditLink">
      <h1>Edit link</h1>
      <form>
      <label class="survey_label">Link</label>
      <input type="text" class="textfield" name="profileLink" id="profileLink" />
      <br />
      <input type="button" class="update" id="save_Survey" /> <input type="button" class="cancel_2" style="margin-left:10px;" />
      </form>
      </div>
     <div class="sub_container">
      <div class="survey_box">
      <img src="images/Survey 2.JPG" class="survey_img" />
      <div class="survey_desc">
      <h3>Guest Profile:</h3>
      <br />
      <h4>Since January 07, 2013</h4>
      <br />
      <a href="javascrit:void(0)">www.myprofileone.com</a>
      <table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Manual</span></td>
					<td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
      </div>
      <div class="profile_buttons">
     <!--<input type="button" class="edit_profile" />--><input type="button" class="edit_link" /> 	
     <input type="button" class="copy_link" /><input type="button" class="delete_profile" />
     </div>
      </div>
      <div class="survey_box">
      <img src="images/Survey 1.JPG" class="survey_img" />
      <div class="survey_desc">
      <h3>Skin Care Profile:</h3>
      <br />
      <h4>Since January 07, 2013</h4>
      <table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Manual</span></td>
					<td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
      </div>
     <div class="profile_buttons">
      	<input type="button" class="delete_profile" />
     </div>
      </div>
      <div class="survey_box">
      <img src="images/Survey 3.JPG" class="survey_img" />
      <div class="survey_desc">
      <h3>Consultant Profile:</h3>
      <br />
      <h4>Since January 07, 2013</h4>
      <br />
      <a href="javascrit:void(0)">www.myprofilethree.com</a>
      <table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Manual</span></td>
					<td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
      </div>
     <div class="profile_buttons">
    <!-- <input type="button" class="edit_profile" />--><input type="button" class="edit_link" /> 
     <input type="button" class="copy_link" /><input type="button" class="delete_profile" />
     </div>
      </div>
      <div class="survey_box">
      <img src="images/Survey 4.JPG" class="survey_img" />
      <div class="survey_desc">
      <h3>Fit Profile:</h3>
      <br />
      <h4>Since January 07, 2013</h4>
      <table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Manual</span></td>
					<td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
      </div>
     <div class="profile_buttons">
     <!--<input type="button" class="edit_profile" />--><input type="button" class="delete_profile" />
     </div>
      </div>
      <!--<div class="survey_box">
      <img src="images/Survey 5.JPG" class="survey_img" />
      <div class="survey_desc">
      <h3>Guest Profile:</h3>
      <br />
      <h4>Since January 07, 2013</h4>
       <br />
      <a href="javascrit:void(0)">www.myprofilefour.com</a>
      <table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Manual</span></td>
					<td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					<td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
      </div>
     <div class="profile_buttons">
     <input type="button" class="edit_profile" /><input type="button" class="edit_link" /> 
     <input type="button" class="copy_link" /><input type="button" class="delete_profile" />
     </div>
      </div>--> 
     </div>
    <?php 
		include_once("footer.php");
	?>
   