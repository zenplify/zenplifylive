<div class="leftOptions containerSettings">
    <table cellpadding="0" cellspacing="0" border="1" id="tblProfileMenu">
        <tr <?php if($profileStep == 0) {echo "class='settingsSelected'"; } ?>><td>Choose a Template</td></tr>
        <tr <?php if($profileStep == 1) {echo "class='settingsSelected'"; } ?>><td>Edit Profile Template</td></tr>
        <tr <?php if($profileStep == 2) {echo "class='settingsSelected'"; } ?>><td>Profile Question</td></tr>
        <tr <?php if($profileStep == 3) {echo "class='settingsSelected'"; } ?>><td>Add/Edit Profile Question</td></tr>
        <tr <?php if($profileStep == 4) {echo "class='settingsSelected'"; } ?>><td>Thank you Page</td></tr>
        <tr <?php if($profileStep == 5) {echo "class='settingsSelected'"; } ?>><td>Assign Campaign or Group</td></tr>
        <tr <?php if($profileStep == 6) {echo "class='settingsSelected'"; } ?>><td>View Custom Rules</td></tr>
        <tr <?php if($profileStep == 7) {echo "class='settingsSelected'"; } ?>><td>Add Custom Rules</td></tr>
        <tr <?php if($profileStep == 8) {echo "class='settingsSelected'"; } ?>><td>Preview</td></tr>
<!--        <tr --><?php //if($profileStep == 9) {echo "class='settingsSelected'"; } ?><!--<td>Preview Thank you Page</td></tr>-->
    </table>
</div>
