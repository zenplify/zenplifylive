<?php 
error_reporting(0);
include('../../classes/init.php');
include('../../classes/profiles.php');
include('../../classes/contact.php');
include('../../classes/plan.php');
//print_r($_POST);
$database= new database();
$profiles=new Profiles();
$contact= new contact();
$plan= new plan();
$questions=$profiles->getPageQuestions($database,$_POST['pageId']);
$results=$profiles->generateContact($database,$_POST['pageId'],$_POST['userId'],$_POST['firstName'],$_POST['lastName'],$_POST['email'],$_POST['phone']);
//print_r($questions);
//die();
$contactId=$results['contactId'];
//echo $contactId;
$replyId=$results['replyId'];
$emailId=$results['emailId'];
$contact->createPermissionRequest($_POST['userId'],$contactId,$emailId);

$contactInfo='<b>First Name:</b> '.$_POST['firstName'].'<br /><b>Last Name:</b> '.$_POST['lastName'].'<br /><b>Birthday:</b> '.$_POST['birthday_month'].'/'.$_POST['birthday_day'].'/'.$_POST['birthday_year'].'<br /><b>Phone:</b> '.$_POST['phone'].'<br />'.'<b>Email:</b> '.$_POST['email'].'<br />';

if($_POST['pageName']=='guestProfile')
{
	$birthday=$_POST['birthday_month'].'/'.$_POST['birthday_day'].'/'.$_POST['birthday_year'];
	$address=$profiles->addMailingAddress($database,$replyId,$contactId,$_POST['userId'],$birthday, $_POST['referredBy'],$_POST['bestTimeToCall'],$_POST['street'],$_POST['city'],$_POST['state'],$_POST['zipCode'],$_POST['country']);
	
	$groupId=$profiles->addToGroup($database,$contactId,'Guests');
	$planIds=$profiles->EditContactPlanByGroup($database,$contactId,$groupId,$_POST['userId']);
	$bestTime=implode(", ",$_POST['bestTimeToCall']);
	$contactInfo=$contactInfo.'<b>Street:</b> '.$_POST['street'].'<br /><b>City:</b> '.$_POST['city'].'<br /><b>State/Prov:</b> '.$_POST['state'].'<br /><b>Zip/Postal:</b> '.$_POST['zipCode'].'<br /><b>Country:</b> '.$_POST['country'].'<br /><b>Best Times to Call:</b> '.$bestTime.'<br /><b>Referred by:</b> '.$_POST['referredBy'].'<br />';
	$profiles->sendPermissionEmail($database,$_POST['userId'],$contactId);
	
}
if($_POST['pageName']=='consultantProfile')
{
	
	$planId=$profiles->addToPlan($database,$contactId,'Consultant Profile Response (automatic)',$_POST['userId']);
	
}
$mailQuesions='';
foreach ($questions as $q)
{
	
	$mailQuesions=$mailQuesions.'<b>'.$q->label.':</b> ';
	$name='question'.$q->questionId;
	
	
	if($q->typeId=='1' || $q->typeId=='2' )
	{
		
		$ans=str_replace("'","\'",$_POST[$name]);
		$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$ans."', '".$contactId."')");
		$mailQuesions=$mailQuesions.$ans.'<br />';
		
	}
	else if($q->typeId=='3')
	{
		$choices=array();
		if($q->questionId=='10' || $q->questionId=='14' || $q->questionId=='42')
		{
			$interests='';
			foreach($_POST[$name] as $checkox) 
			{
				$inter=$database->executeObject("SELECT choiceText from pageformquestionchoices where choiceId ='".$checkox."'");
				
			
				$start=strpos($inter->choiceText,'<b>');
				$stop=strpos($inter->choiceText,':</b>');
				$stop=$stop-3;
				$choiceText=substr($inter->choiceText,$start+3,$stop);
				if($choiceText=='NO THANK YOU')
				$choiceText='CLIENT';
				if($interests!='')
				$interests=$interests.', '.$choiceText;
				else
				$interests=$choiceText;
				//$mailQuesions=$mailQuesions.$interests.'<br />';
			}
			
			$database->executeNonQuery("UPDATE contact SET interest='".$interests."' WHERE contactId='".$contactId."'");
			
		}
		
		$j=0;
		foreach($_POST[$name] as $checkox) 
		{
			
			$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$checkox."', '".$contactId."')");
			$choice=$database->executeScalar("SELECT choiceText from pageformquestionchoices where choiceId ='".$checkox."'");
			$choices[$j]=$choice;
			$j++;
		}
		
			$answerss=implode(', ',$choices);
			$mailQuesions=$mailQuesions.$answerss.'<br />';
	}
	else if($q->typeId=='4')
	{
		
		$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$_POST[$name]."', '".$contactId."')");
		$choice=$database->executeScalar("SELECT choiceText from pageformquestionchoices where choiceId ='".$_POST[$name]."'");
		$mailQuesions=$mailQuesions.$choice.'<br />';
	}
	else if($q->typeId=='5')
	{
		$i=0;
		$answers=$profiles->getQuestionChoices($database,$q->questionId);
		foreach($answers as $radio) 
		{
			$name='answer'.$radio->choiceId;
		
		
			$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, value, contactId) VALUES ('".$replyId."','".$q->questionId."', '".$radio->choiceId."', '".$_POST[$name]."' ,'".$contactId."')");
			if($q->questionId=='21')
			{
				
				
				$options=array('In your first year','In 1-2 years','In 2-3 years','In 3-4 years','In 5+ Years');
				
				
					
					$answersss=$options[$i].':'.$_POST[$name];
					$answerss=$answersss;
					$i++;
				
					$mailQuesions=$mailQuesions.$answerss.'<br />';
			}
			
			if($q->questionId=='27')
			{
				
				
				$options1=array("Facebook","Technology in general","Talking to people you know","Talking to people you don't know","Talking to groups of people","Talking to groups of people","Talking on the phone","Inviting someone to try a sample","Recommending products","Taking an order","Customer service","Recruiting/Hiring","Teaching/Training");
				$answersss=$options1[$i].':'.$_POST[$name];
				$answerss=$answersss;
				$i++;
			
				$mailQuesions=$mailQuesions.$answerss.'<br />';
			}
			
			if($q->questionId=='3')
			{
				
				
				$options2=array("Sleep","Energy","Appetite","Digestion","Menses/ Men's health","Mood","Muscles/ joints/ body pain","Skin","Stress Management");//,"Sex"
				$answersss=$options2[$i].':'.$_POST[$name];
				$answerss=$answersss;
				$i++;
			
				$mailQuesions=$mailQuesions.$answerss.'<br />';
			}
		}
		
		

	}
	
}
//echo $contactInfo.$mailQuesions;
$pageTitle=$profiles->getPageTitle($database,$_POST['pageId']);
$userInfo=$database->executeObject("SELECT * FROM user WHERE userId='".$_POST['userId']."'");
$headers = "From: info@zenplify.biz\r\n";
$headers .= "Content-Type: text/html";
				
$body=nl2br($contactInfo.$mailQuesions);
//echo $body;
mail($userInfo->email,$pageTitle.' - '.$_POST['firstName'].' '.$_POST['lastName'],'<html><body>'.$body.'</html></body>',$headers);
//die();
echo '<head>
   
    <META http-equiv="refresh" content="0;URL='.$CONF['siteURL']."profiles/".$_POST['code'].'/Thanks.html">
  </head> ';

//header("Location: ".$CONF['siteURL']."profiles/".$_POST['code']."/Thanks.html");
?>