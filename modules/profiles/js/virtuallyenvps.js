$(document).ready(function(){
	jQuery.validator.addMethod(
		"dateAfter1900",
		function(value, element) {
			var check = false;
			var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
			if( re.test(value)){
				var adata = value.split('/');
				var gg = parseInt(adata[1],10);
				var mm = parseInt(adata[0],10);
				var aaaa = parseInt(adata[2],10);
				var xdata = new Date(aaaa,mm-1,gg);
				if ( xdata.getFullYear() > 1900 )
					check = true;
				else
					check = false;
			} else
				check = true;
			return this.optional(element) || check;
		}, 
		"Please enter a year later than 1900"
	);
	jQuery.validator.addMethod(
		"dateNA",
		function(value, element) {
			var check = false;
			var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
			if( re.test(value)){
				var adata = value.split('/');
				var gg = parseInt(adata[1],10);
				var mm = parseInt(adata[0],10);
				var aaaa = parseInt(adata[2],10);
				var xdata = new Date(aaaa,mm-1,gg);
				if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )
					check = true;
				else
					check = false;
			} else
				check = false;
			return this.optional(element) || check;
		}, 
		"Please enter a date in the format MM/DD/YYYY"
	);
	var checkErrorStatus = function(errorDiv) {
		var errorList = errorDiv.data('errors');
		if (errorList == undefined) {
			errorList = {};
		}
		var foundError = null;
		for (key in errorList) {
			foundError = errorList[key];
		}
		if (foundError == null) {
			$(errorDiv).closest(".questionbox").removeClass("errorBox");
			$(errorDiv).empty();
		}
		else {
			$(errorDiv).closest(".questionbox").addClass("errorBox");
			$(errorDiv).empty();
			foundError.appendTo(errorDiv);
			$('.equalize').equalHeightsImproved(undefined, '.questionbox');
		}
	}
		$("#questionform").validate({
		errorPlacement: function(error, element) {
			var errorDiv = element.closest(".questionbox").find(".errorMessage");
			var errorList = $(errorDiv).data('errors');
			if (errorList == undefined) {
				errorList = {};
			}
			error.data('parent', errorDiv);
			errorList[error[0].htmlFor] = error;
			$(errorDiv).data('errors', errorList);
			checkErrorStatus(errorDiv);			
		},
		rules: {
			"birthdate": {
				required: true,
				dateNA: true,
				dateAfter1900: true
			 }
		},
		success: function(label) {
			var errorDiv = $(label).data('parent');
			if (errorDiv == null) {
				errorDiv = $(label).closest('.questionbox').find('.errorMessage');
			}
			var errorList = $(errorDiv).data('errors');
			if (errorList == undefined) {
				errorList = {};
			}
			else {
				delete errorList[label[0].htmlFor];
			}
			$(errorDiv).data('errors', errorList);
			checkErrorStatus(errorDiv);
		}
	});
	$.validator.messages.required = 'Please fill in all required fields.';
});
/*-------------------------------------------------------------------- 
 * JQuery Plugin: "EqualHeights"
 * by:	Scott Jehl, Todd Parker, Maggie Costello Wachs (http://www.filamentgroup.com)
 *
 * Copyright (c) 2008 Filament Group
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 *
 * Description: Compares the heights or widths of the top-level children of a provided element 
 		and sets their min-height to the tallest height (or width to widest width). Sets in em units 
 		by default if pxToEm() method is available.
 * Dependencies: jQuery library, pxToEm method	(article: 
		http://www.filamentgroup.com/lab/retaining_scalable_interfaces_with_pixel_to_em_conversion/)							  
 * Usage Example: $(element).equalHeights();
  		Optional: to set min-height in px, pass a true argument: $(element).equalHeights(true);
 * Version: 2.0, 08.01.2008
--------------------------------------------------------------------*/

$.fn.equalHeightsImproved = function(px, inner) {
	$(this).each(function(){
		var currentTallest = 0;
		var matchingElements = $(this).children();
		if (inner) {
			matchingElements = $(this).find(inner);
		}
		matchingElements.each(function(i){
			if ($(this).height() > currentTallest) { currentTallest = $(this).height();
			}
		});
		//if (!px || !Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
		// for ie6, set height since min-height isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { matchingElements.css({'height': currentTallest}); }
		matchingElements.css({'min-height': currentTallest}); 
	});
	return this;
};

$(window).load(function(){
		$('.equalize').equalHeightsImproved(undefined, '.questionbox');
	});
