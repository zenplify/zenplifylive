<?php
	session_start();
	include_once('../../classes/init.php');
	include_once('../../classes/profiles.php');
	$database = new database();
	$profile = new Profiles();
	$userDetails = $profile->getUserDetailsfromNewCode($database, $_REQUEST['code'], $_REQUEST['user']);
	if(($userDetails->generatedVia == 2) || ($userDetails->generatedVia == 3))
	{
		$txtPageId = $userDetails->pageId;
		include_once('custom_profile_thanks.php');
	}
	else
	{
		$pageName = $profile->getUserPageName($database, $_REQUEST['code'], $_REQUEST['user']);
		include_once("$pageName".'Thanks.php');
	}

?>