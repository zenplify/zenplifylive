<?php
session_start();
include_once('../../classes/init.php');
include_once('../../classes/profiles.php');
$database=new database();
$profile=new Profiles();
$details=$profile->getUserPageDetails($database,$_REQUEST['code'],$_REQUEST['user']);
//$quest=$profile->getPageQuestions($database,$details->pageId);
//echo $details->templateHTML;
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Thank You</title>
<base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"  />
<link rel="stylesheet" type="text/css" href="styles.css" media="all" />
</head><body><p>&nbsp;
	</p>
<link href="css/reset.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/text.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/960.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/virtuallyenvps.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Dancing+Script|Italiana" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script><script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script><script type="text/javascript" src="http://www.oprius.com/virtuallyenvpsnew/virtuallyenvps.js"></script>
<div class="container_16" id="box">
<div id="topbar" style="background-image: url('../../images/venvps_1.jpg'); height: 100px;">
		&nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
<div class="grid_6 signature">
	<?php echo $details->firstName.' '.$details->lastName ;
	if ($details->title!='')echo ', '.$details->title;  ?><br />
		Arbonne Consultant <?php echo $details->consultantId; ?><br />
<a href="<?php if(strpos($details->webAddress,"http://") !== false || strpos($details->webAddress,"https://") !== false  )echo $details->webAddress; else echo 'http://'.$details->webAddress; ?>"  target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
<a href="<?php if(strpos($details->webAddress,"http://") !== false  || strpos($details->facebookAddress,"https://") !== false  )echo $details->facebookAddress; else echo 'http://'. $details->facebookAddress; ?>" target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br />
<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br />
		<?php echo $details->phoneNumber; ?></div>
<div class="grid_16 spacer">
		&nbsp;</div>
<div style="font-size: 12px; " class="grid_16 texttitle">
<h1>
			Thank You!</h1>
</div>
<div class="grid_16 pagetext">
<?php echo $details->thankYouMessage;?>
</div>
<div class="grid_16">
<p style="font-size: 12px; ">
			Best regards,<br />
			<?php echo $details->firstName;?></p>
</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
</div>
<p>&nbsp;
	</p>
</body></html>
