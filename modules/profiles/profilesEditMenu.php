<div class="leftOptions containerSettings">
    <table cellpadding="0" cellspacing="0" border="1" id="tblProfileMenu">
        <tr style="cursor: pointer;" <?php if($profileStep == 0) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(0,'jumpStep');">
			<td>Choose a Template</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 1) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(1,'jumpStep');">
			<td>Edit Profile Template</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 2) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(2,'jumpStep');">
			<td>Profile Question</td>
		</tr>
        <tr style="" <?php if($profileStep == 3) {echo "class='settingsSelected'"; } ?> >
			<td>Add/Edit Profile Question</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 4) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(4,'jumpStep');">
			<td>Thank you Page</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 5) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(5,'jumpStep');">
			<td>Assign Campaign or Group</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 6) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(6,'jumpStep');">
			<td>View Custom Rules</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 7) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(7,'jumpStep');">
			<td>Add Custom Rules</td>
		</tr>
        <tr style="cursor: pointer;" <?php if($profileStep == 8) {echo "class='settingsSelected'"; } ?> onClick="submitFrm(8,'jumpStep');">
			<td>Preview</td>
		</tr>
<!--        <tr --><?php //if($profileStep == 9) {echo "class='settingsSelected'"; } ?><!--<td>Preview Thank you Page</td></tr>-->
    </table>
</div>
