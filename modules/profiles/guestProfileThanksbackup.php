<?php
session_start();
include_once('../../classes/init.php');
include_once('../../classes/profiles.php');
$database=new database();
$profile=new Profiles();
$details=$profile->getUserPageDetails($database,$_REQUEST['code']);
$userId=$details->userId;
$uniqueCode=$profile->getUserFitProfile($database,$userId);
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Thank You</title>
<base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"  />
<link rel="stylesheet" type="text/css" href="styles.css" media="all" />
</head><body><p>&nbsp;
	</p>
<link href="css/reset.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/text.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/960.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/virtuallyenvps.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Dancing+Script|Italiana" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script><script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script><script type="text/javascript" src="http://www.oprius.com/virtuallyenvpsnew/virtuallyenvps.js"></script>
<div class="container_16" id="box">
<div id="topbar" style="background-image: url('../../images/discover-arbonne.png'); height: 100px;">
		&nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
<div class="grid_6 signature">
		<?php echo $details->firstName.' '.$details->lastName ;
	if ($details->title!='')echo ', '.$details->title;  ?><br />
		Arbonne Consultant <?php echo $details->consultantId; ?><br />
<a href="<?php if(strpos($details->webAddress,"http://") != false || strpos($details->webAddress,"https://") != false  )echo $details->webAddress; else echo 'http://'.$details->webAddress; ?>"  target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
<a href="<?php if(strpos($details->webAddress,"http://") != false  || strpos($details->facebookAddress,"https://") != false  )echo $details->facebookAddress; else echo 'http://'. $details->facebookAddress; ?>" target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br />
<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br />
		<?php echo $details->phoneNumber; ?></div>
<div class="grid_16 spacer">
		&nbsp;</div>
<div class="grid_16 texttitle">
<h1>
			Thank you!</h1>
</div>
<div class="grid_16 pagetext">
<div style="font-size: 16px; " class="grid_16 texttitle">
<h2>
<strong style="font-size: 23px; "><span style="font-size: 23px; color: rgb(0, 0, 0); ">Next Step - Request Your Samples!
</span></strong></h2>

<span>Thank you for submitting your Guest Profile! Follow the steps below to let me know what you're most excited to try!
</span>
<br />
</div>
<div style="font-size: 16px; " class="grid_16">
<ol>
<?php 
if( strpos($details->facebookPersonalAddress,"http://") != false || strpos($details->facebookPersonalAddress,"https://") != false ) 
{
	$personal=$details->facebookPersonalAddress;
}
 else
{  
	$personal= "http://".$details->facebookPersonalAddress;
}
if( strpos($details->facebookPartyAddress,"http://") !=false || strpos($details->facebookPartyAddress,"https://") !=false) 
{
	$party= $details->facebookPartyAddress;
}
else
{  $party= "http://".$details->facebookPartyAddress;
}
?>

<li style="font-size: 16px; ">
					Find me on Facebook at <span style="color:#0000ff;"><a href="<?php echo $personal; ?>" target="_blank"><?php echo $details->facebookPersonalAddress;?></a></span> <span style="font-size: 16px; ">and click the "Add Friend" button if we're not already connected.</span></li>
<li style="font-size: 16px; ">
					Stop by our Virtual Party at <span style="color:#0000ff;"><a href="<?php echo $party;  ?>" target="_blank"><?php echo $details->facebookPartyAddress;?></a></span>&nbsp;to take a look at what's available and request your samples.&nbsp;<span style="font-size: 16px; ">If you haven't been added to the Party by a host or a consultant yet, click the "Join" button and send me a private message in place of the post below.</span></li>
<li style="font-size: 16px; ">
					Post <span style="color:#008000;">I want to feel gorgeous!</span>&nbsp;to pamper your skin, <span style="color:#008000;">I want to feel healthy!</span> to try our nutrition or <span style="color:#008000;">I WANT IT ALL!</span> if you're interested in both personal care and nutrition or weight loss.</li>
</ol>
<p style="font-size: 16px; ">
				Can't wait to get started? <span style="color:#0000ff;"><a href="<?php echo $CONF['siteURL'].'profiles/'.$uniqueCode; ?>" target="_blank">Click Here</a></span> to watch our Nutrition Workshop and find out how simple exchanges in what you eat can make a big difference in how you feel by reducing toxins and inflammation as well as balancing your hormones.</p>
<p style="font-size: 16px; ">
				Have a fantastic day!</p>
</div>
</div>
<div class="grid_16" style="font-size: 16px; ">
<p>
			<?php echo $details->firstName;?></p>
<p>
			p.s.&nbsp;<span style="font-size: 16px; ">Not on Facebook? No worries! Just send me an email at&nbsp;</span><span style="font-size: 16px; color: rgb(0, 0, 255); "><a href="mailto:<?php echo $details->email; ?>" target="_blank"><?php echo $details->email; ?></a></span><span style="font-size: 16px; ">&nbsp;to let me know how you want me to customize your pamper pack.</span></p>
</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
</div>
<p>&nbsp;
	</p>
</body></html>
