<?php
session_start();
include_once('../../classes/init.php');
include_once('../../classes/profiles.php');

$database=new database();
$profile=new Profiles();
$details=$profile->getUserPageDetails($database,$_REQUEST['code'],$_REQUEST['user']);
$userId=$details->userId;
$uniqueCode=$profile->getUserFitProfile($database,$userId);
$uniqueCode2=$profile->getUserSkinProfile($database,$userId);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Thank You </title>
<base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"  />
<link rel="stylesheet" type="text/css" href="styles.css" media="all" />
</head><body><p>&nbsp;
	</p>
<link href="css/reset.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/text.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/960.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/virtuallyenvps.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Dancing+Script|Italiana" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script><script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script><script type="text/javascript" src="http://www.oprius.com/virtuallyenvpsnew/virtuallyenvps.js"></script>
<div class="container_16" id="box">
<div id="topbar" style="background-image: url('../../images/venvps_1.jpg'); height: 100px;">
		&nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
<div class="grid_6 signature">
		<?php
		
		 echo $details->firstName.' '.$details->lastName ;
	if ($details->title!='')echo ', '.$details->title;  ?><br />
		Arbonne Consultant <?php echo $details->consultantId; ?><br />
<a href="<?php if(strpos($details->webAddress,"http://") !== false || strpos($details->webAddress,"https://") !== false  )echo $details->webAddress; else echo 'http://'.$details->webAddress; ?>"  target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
<a href="<?php if(strpos($details->facebookAddress,"http://") !== false  || strpos($details->facebookAddress,"https://") !== false  )echo $details->facebookAddress;  if(strpos($details->facebookAddress,"http://") === false && strpos($details->facebookAddress,"https://") === false  ) echo 'http://'. $details->facebookAddress; ?>" target="_blank"><?php echo $web=preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br />
<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br />
		<?php echo $details->phoneNumber; ?></div>
<div class="grid_16 pagetext" style="margin-left:15px;">
 <div style="font-size: 16px; width: 925px; " class="grid_16 texttitle">
  <div class="texttitle">
<h1>Thank You!</h1>
</div>
<h2>
<strong style="font-size: 23px; "><span style="font-size: 23px; color: rgb(0, 0, 0); ">Ready to look and feel your best?
</span></strong></h2>

<span>Completing the Guest Profile was your first step in the right direction! I’ll be in touch to review your profile with you and offer personal recommendations. I want to provide an experience that’s just right for you, so don’t be shy about telling me what will make the biggest difference in your life as it relates to achieving your optimal health, beauty and financial goals! </span>
<br />
<br />
</div>


<?php 
if( strpos($details->facebookPersonalAddress,"http://") !== false || strpos($details->facebookPersonalAddress,"https://") !== false ) 
{
	$personal=$details->facebookPersonalAddress;
}
 else
{
	$personal= "http://".$details->facebookPersonalAddress;
}

if( strpos($details->facebookAddress,"http://")!== false || strpos($details->facebookAddress,"https://") !== false) 
{
	$party= $details->facebookAddress;
}
 if(strpos($details->facebookAddress,"http://") === false  && strpos($details->facebookAddress,"https://") === false  )
{  
	$party= "http://".$details->facebookAddress;
}?>
<!--<ol>
<li style="font-size: 16px; ">
					Find me on Facebook at <span style="color:#0000ff;"><a href="<?php //echo $personal; ?>" target="_blank"><?php //echo $details->facebookPersonalAddress;?></a></span> <span style="font-size: 16px; ">and click the "Add Friend" button if we're not already connected.</span></li>
<li style="font-size: 16px; ">
					Stop by our Virtual Party at <span style="color:#0000ff;"><a href="<?php //echo $party;  ?>" target="_blank"><?php //echo $details->facebookPartyAddress;?></a></span>&nbsp;to take a look at what's available and request your samples.&nbsp;<span style="font-size: 16px; ">If you haven't been added to the Party by a host or a consultant yet, click the "Join" button and send me a private message in place of the post below.</span></li>
<li style="font-size: 16px; ">
					Post <span style="color:#008000;">I want to feel gorgeous!</span>&nbsp;to pamper your skin, <span style="color:#008000;">I want to feel healthy!</span> to try our nutrition or <span style="color:#008000;">I WANT IT ALL!</span> if you're interested in both personal care and nutrition or weight loss.</li>
</ol>-->
<div class="grid_16 pagetext" style="margin-left:15px; width: 925px;">
<h3>Next Steps:</h3>
<ol>
<li style="font-size: 16px; ">
					<!--Visit my Facebook page at <a href="<?php // echo $party; ?>" target="_blank"><?php // echo $web=preg_replace('#^https?://#', '', $details->facebookAddress);?></a><span style="font-size: 16px; "> and click the "Like" button.</span>-->
			Check your email for the "Newsletter Permission" and click the approve link.

</li>

<li style="font-size: 16px; ">
					<!--Post "I want to feel great!" "I want gorgeous skin!" or "I WANT IT ALL" to give me an idea of what interests you most!-->
Click on one of the links below or in the "Arbonne Nutrition, Skincare & Consultant Workshops" email I sent to learn more about the topics that interest you.
</li>
</ol>
</div>
<br />
<div style="font-size: 16px; " class="grid_16">
<table width="95%">
<tr>
<td><h3>Simple Nutrition </h3></td>
<td><h3>Simple Beauty</h3></td>
</tr>
<tr>
<td><p style="font-size: 16px; width:400px; "><span style="color:#0000ff;"><a href="<?php echo $CONF['siteURL'].$details->username.'/'.$uniqueCode; ?>" target="_blank">Click Here</a></span> to watch our Nutrition & Weight Loss Workshop and learn how simple changes can give you a Fresh Start and help you feel healthier and more energetic without going hungry!</p></td>
<td><p style="font-size: 16px;width:400px; "><span style="color:#0000ff;"><a href="<?php echo $CONF['siteURL'].$details->username.'/'.$uniqueCode2; ?>" target="_blank">Click Here</a></span> to watch our Personal Care & Cosmetics Workshop and learn how the latest developments in science and botanicals can help you look younger next month than you do today!</p></td>
</tr>
</table>
<br />
				Have a fantastic day!
                <br />
                <?php echo $details->firstName;?></p>
</div>
</div>
<div class="grid_16 pagetext" style="font-size: 16px; ">
<span>
			</span>
<!--<p>
			p.s.&nbsp;<span style="font-size: 16px; ">Not on Facebook? No worries! Just send me an email at&nbsp;</span><span style="font-size: 16px; color: rgb(0, 0, 255); "><a href="mailto:<?php //echo $details->email; ?>" target="_blank"><?php //echo $details->email; ?></a></span><span style="font-size: 16px; ">&nbsp;to let me know how you want me to customize your pamper pack.</span></p>
-->
</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
		&nbsp;</div>
</div>
<p>&nbsp;
	</p>
</body></html>
