<?php
	$details = $profile->getUserPageDetails($database, $_REQUEST['code'], $_REQUEST['user']);
	if ($details->leaderId == 0) { $consultantTitleUserId = $details->userId; }
	else { $consultantTitleUserId = $details->leaderId; }
	$consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
	$font = $profile->getFontStyleNamebyId($database, $details->thankyouFontStyle);
	$bgColor = $profile->getProfileColorById($database, $details->thankyoubgColor);
	$fontColor = $profile->getProfileColorById($database, $details->thankyouFontColor);
    $accentColor = $profile->getProfileColorDetail($database, $details->accentColor);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Thank You </title>
		<base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"/>
		<link rel="stylesheet" type="text/css" href="styles.css" media="all"/>
	</head>
	<body>
	<p>&nbsp;
	</p>
	<link href="css/reset.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="css/text.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="css/960.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="css/customprofiles.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Dancing+Script|Italiana" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate
	.js"></script>
	<script type="text/javascript" src="http://www.oprius.com/virtuallyenvpsnew/virtuallyenvps.js"></script>
	<!--<div class="container_16" id="box" style="font-family:--><?php //echo $font; ?><!--; background-color: --><?php //echo $bgColor; ?><!--; color:--><?php //echo $fontColor; ?><!--; ">-->
	<div class="container_16" id="box">
		<div id="topbar" style="background-image: url('../../images/profile/<?php echo $details->thankyouHeader; ?>'); height: 100px;">&nbsp;</div>
		<div class="greenbg" style="height:1px; float:left; width:100%; background: <?php echo $accentColor->actualName; ?>;">&nbsp;</div>
		<div class="grid_16 spacer">&nbsp;</div>
		<div class="grid_10">
			<?php  if($details->thankyouVideoVisibility=='1'){  ?>
			<iframe allowfullscreen="" frameborder="0" height="315" src="//www.youtube.com/embed/<?php echo $details->thankyouVideoLink; ?>?rel=0;autoplay=0;theme=light;color=white;showinfo=0" width="560"></iframe>
			<?php } ?>
		</div>
		<div class="grid_16 spacer">&nbsp;</div>
		<div class="greenbg" style="height:1px; float:left; width:100%;background: <?php echo $accentColor->actualName; ?>;">&nbsp;</div>
		<div class="grid_6 signature">
			<?php
				echo $details->firstName . ' ' . $details->lastName;
				if ($details->title != '')
					echo ', ' . $details->title;  ?><br/>
            <?php




                echo $details->consultantId.'<br/>';

         

            ?>
			 
			<a href="<?php if (strpos($details->webAddress, "http://") !== false || strpos($details->webAddress, "https://") !== false)
				echo $details->webAddress;
			else echo 'http://' . $details->webAddress; ?>" target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
		<div class="grid_10 signature" style="text-align:right;">
			<a href="<?php if (strpos($details->facebookAddress, "http://") !== false || strpos($details->facebookAddress, "https://") !== false)
				echo $details->facebookAddress;
				if (strpos($details->facebookAddress, "http://") === false && strpos($details->facebookAddress, "https://") === false)
					echo 'http://' . $details->facebookAddress; ?>" target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br/>
			<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br/>
			<?php echo $details->phoneNumber; ?></div>
		<div class="greenbg" style="height:1px; float:left; width:100%; background: <?php echo $accentColor->actualName; ?>;">&nbsp;</div>
		<div class="grid_16 pagetext" style="margin-left:15px;">
			<div style="font-size: 16px; width: 925px; " class="grid_16 texttitle">
				<!--			<div class="texttitle">-->
				<!--				<h1>Thank You!</h1>-->
				<!--			</div>-->
				<!--			<h2>-->
				<!--				<strong style="font-size: 23px; "><span style="font-size: 23px; color: rgb(0, 0, 0); ">Ready to look and feel your best?-->
				<!--</span></strong></h2>-->
				<span><?php
                    $thankyouDefaultText= str_replace("[{First Name}]","$details->firstName","$details->thankyouDefaultText");
                    $thankyouDefaultTextt= str_replace("[{Last Name}]","$details->lastName","$thankyouDefaultText");?>
                    <p><?php echo $thankyouDefaultTextt; ?></p>
                   </span>
				<br/>
				<br/>
			</div>
			<?php
				if (strpos($details->facebookPersonalAddress, "http://") !== false || strpos($details->facebookPersonalAddress, "https://") !== false) {
					$personal = $details->facebookPersonalAddress;
				}
				else {
					$personal = "http://" . $details->facebookPersonalAddress;
				}

				if (strpos($details->facebookAddress, "http://") !== false || strpos($details->facebookAddress, "https://") !== false) {
					$party = $details->facebookAddress;
				}
				if (strpos($details->facebookAddress, "http://") === false && strpos($details->facebookAddress, "https://") === false) {
					$party = "http://" . $details->facebookAddress;
				}?>
			<br/>


		</div>
		<div class="grid_16 pagetext" style="font-size: 16px; ">
			<span></span>
		</div>
		<div class="greenbg" style="height:1px; float:left; width:100%; background: <?php echo $accentColor->actualName; ?>;">&nbsp;</div>
	</div>
	<p>&nbsp;</p>
	</body>
</html>
