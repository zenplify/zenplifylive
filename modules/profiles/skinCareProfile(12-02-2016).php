<?php
    session_start();
    include_once('../../classes/init.php');
    include_once('../../classes/profiles.php');
    $database = new database();
    $profile = new Profiles();
    $details = $profile->getUserPageDetails($database, $_REQUEST['code'], $_REQUEST['user']);
    $code = $details->newCode;
    $url = $profile->curPageURL();
    $viewId = $profile->PageView($database, $details->pageId, $url, $code, $details->userId, $_SERVER['HTTP_USER_AGENT']);
    //	$quest = $profile->getPageQuestions($database, $details->pageId);
    $quest = $profile->getPageQuestions($database, $details->pageId, '-1', 'old', 1);
?>
<script type="text/javascript">
    function submitFrm() {
        var fewSeconds = 10;
        $('#questionform').submit(function (e) {
            console.log($(".required").val());


            $("#submitBtn").attr('disabled', true);
            setTimeout(function () {
                $("#submitBtn").attr('disabled', false);
            }, fewSeconds * 1000);

            // $("#submitBtn").attr('disabled',true);

            //

        });
    }
</script>
<head>
    <base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"/>
    <meta property="og:title" content="Guest Profile">
    <meta property="og:url" content="<?php echo $url; ?>">
    <meta property="og:description" content=" Thank you for your interest in Arbonne s skincare products. Please take a few minutes to watch the
            workshop video to the left, and then fill out the form below. If
            you have already filled out a guest profile, the information you enter below will simply be added to your
            file. I hope you fall in love with the products you are
            sampling - if you have any questions, dont hesitate to ask. Thanks again, and make it a beautiful
            day.">
</head>
<?php if (!empty($details->profileEditedVideo)){

    $pageHtml= str_ireplace("laNWy9ekoRk",$details->profileEditedVideo,$details->templateHTML);


}else {

    $pageHtml=$details->templateHTML;
}
    echo $pageHtml;
?><!-- Page Template End  --->
<div class="grid_6">
    <div class="texttitle">
        <h1>
            Skincare Workshop</h1>
    </div>
    <div class="pagetext">
        <p>
            Thank you for your interest in Arbonne&#39;s skincare products. Please take a few minutes to watch the
            workshop video to the left, and then fill out the form below. If
            you have already filled out a guest profile, the information you enter below will simply be added to your
            file. I hope you fall in love with the products you are
            sampling - if you have any questions, don&#39;t hesitate to ask. Thanks again, and make it a beautiful
            day.</p>

        <p class="right" style="margin-right:40px;">
            Best regards,<br/>
            <?php echo $details->firstName; ?></p>
    </div>
</div>
<div class="grid_16 spacer">
    &nbsp;</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
    &nbsp;</div>
<div class="grid_6 signature">
    <?php echo $details->firstName . ' ' . $details->lastName;
        if ($details->title != '')
            echo ', ' . $details->title;  ?><br/>
    Arbonne Consultant <?php echo $details->consultantId; ?><br/>
    <a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->webAddress, "https://") != false)
        echo $details->webAddress;
    else echo 'http://' . $details->webAddress; ?>"
       target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
    <a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->facebookAddress, "https://") != false)
        echo $details->facebookAddress;
    else echo 'http://' . $details->facebookAddress; ?>"
       target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br/>
    <a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br/>
    <?php echo $details->phoneNumber; ?></div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
    &nbsp;</div>
<div class="grid_16 texttitle">
    <h1>
        Skincare Profile</h1>
</div>
<form action="<?php echo $CONF["siteURL"]; ?>modules/profiles/profileAction.php" id="questionform" method="post">
<input type="hidden" value="<?php echo $details->pageId; ?>" name="pageId"/>
<input type="hidden" value="<?php echo $details->userId; ?>" name="userId"/>
<input type="hidden" value="<?php echo $details->pageName; ?>" name="pageName"/>
<input type="hidden" value="<?php echo $code; ?>" name="code"/>
<input type="hidden" value="<?php echo $url; ?>" name="pageUrl"/>
<input type="hidden" value="<?php echo $viewId; ?>" name="viewId"/>

<div class="grid_6 questionbox">
    <div class="boxtitle">
        Contact Information:
    </div>
    <div style="padding:0 10px 0 10px;">
        <div class="errorMessage">
            &nbsp;</div>
        <table border="0" cellpadding="0" width="100%">
            <tbody>
            <tr>
                <td align="right" width="20%">
                    Name:<span class="requiredstar">*</span></td>
                <td width="40%">
                    <input class="required" name="firstName" type="text"/></td>
                <td width="40%">
                    <input class="required" name="lastName" type="text"/></td>
            </tr>
            <tr>
                <td align="right" width="20%">
                    Email:<span class="requiredstar">*</span></td>
                <td colspan="2" width="80%">
                    <input class="required email" name="email" type="text"/></td>
            </tr>
            <tr>
                <td align="right" width="20%">
                    Phone:<span class="requiredstar">*</span></td>
                <td colspan="2" width="80%">
                    <input class="required" name="phone" type="text"/></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="grid_10">
    &nbsp;</div>
<div class="grid_16 spacer">
    &nbsp;</div>
<div class="equalize">
    <?php
        $isfirst = true;
        foreach ($quest as $q)
        {
        if ($q->order == '1')
        {
            ?>
            <div class="grid_10 questionbox">
                <div style="padding:0 10px 0 10px;">
                    <div class="errorMessage">
                        &nbsp;</div>
                    <div class="questiontext">
                        <?php echo $q->question; ?><span class="requiredstar">*</span></div>
                    <table border="0" cellpadding="0" width="100%">
                        <tbody>
                        <tr>
                            <?php $choices = $profile->getQuestionChoices($database, $q->questionId);
                                $i = 0;
                                $i = 0;
                                foreach ($choices as $c)
                                {

                                    if ($i % 3 == 0)
                                    {
                                        echo '</tr><tr>';
                                    }
                                    echo '<td><input ';
                                    if ($i == 0)
                                        echo 'class="required"';
                                    echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '</td>';
                                    $i++;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Qno1---><?php
        }

        if ($q->order == '2')
        {
            ?>
            <div class="grid_6 questionbox">
                <div style="padding:0 10px 0 10px;">
                    <div class="errorMessage">
                        &nbsp;</div>
                    <div class="questiontext">
                        <?php    echo $q->question . '<span class="requiredstar">*</span></div>';

                            $choices = $profile->getQuestionChoices($database, $q->questionId);
                            $i = 0;
                            foreach ($choices as $c)
                            {


                                echo '<input ';
                                if ($i == 0)
                                    echo 'class="required"';
                                echo 'name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
                                $i++;
                            }?></div>
                </div>
            </div>
            <div class="grid_16 spacer">
                &nbsp;</div>
        <?php
        }
        if ($q->order == '3')
        {
        if ($q->typeId == '3')
        {
    ?>
    <div class="grid_6 questionbox">
        <div style="padding:0 10px 0 10px;">
            <div class="errorMessage">
                &nbsp;</div>
            <div class="questiontext">
                <?php echo $q->question; ?><span class="requiredstar">*</span></div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <?php  $choices = $profile->getQuestionChoices($database, $q->questionId);
                    $i = 0;
                    foreach ($choices as $c)
                    {
                        if (!($c->choiceId == '73' or $c->choiceId == '74'))
                        {
                            echo '<tr><td><input ';
                            if ($i == 0)
                                echo 'class="required"';
                            echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /></td><td> ' . $c->choiceText . '</td><tr>';
                            $i++;
                        }
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
    }
    else
    {

?>
<div class="equalize">
    <div class="grid_10 questionbox" style="width: 580px;">
        <div style="padding:0 10px 0 10px;">
            <div class="errorMessage">
                &nbsp;</div>
            <div class="questiontext">
                <?php echo $q->question; ?></div>
            <textarea name="<?php echo 'question' . $q->questionId; ?>" rows="7"></textarea></div>
    </div>
    <?php
        }
        }
        if ($q->order == '4')
        {
    ?>
    <div class="grid_16 spacer">
        &nbsp;</div>
    <div class="equalize">
        <div class="grid_10 questionbox">
            <div style="padding:0 10px 0 10px;">
                <div class="errorMessage">
                    &nbsp;</div>
                <div class="questiontext">
                    <?php echo $q->question; ?><span class="requiredstar">*</span></div>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <?php  $choices = $profile->getQuestionChoices($database, $q->questionId);
                        $i = 0;
                        foreach ($choices as $c)
                        {
                            if (!($c->choiceId == '73' or $c->choiceId == '74'))
                            {
                                echo '<tr><td><input ';
                                if ($i == 0)
                                    echo 'class="required"';
                                echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /></td><td> ' . $c->choiceText . '</td><tr>';
                                $i++;
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
            }

            if ($q->order == '5')
            {
        ?>

        <div class="grid_6 questionbox">
            <div style="padding:0 10px 0 10px;">
                <div class="errorMessage">
                    &nbsp;</div>
                <div class="questiontext">
                    <?php echo $q->question; ?></div>
                <textarea name="<?php echo 'question' . $q->questionId; ?>" rows="8"></textarea></div>
        </div>
    </div>

<?php
    }
    }?>

    <div class="grid_16 spacer">
        &nbsp;</div>
    <div class="grid_16">
        <div style="padding:0 10px 0 10px;">
            <div class="errorMessage">
                &nbsp;</div>
            <!--			<input class="button" type="submit" value="Submit"/>-->
            <input class="button" onclick="submitFrm()" id="submitBtn" type="submit" value="Submit"/>
        </div>
    </div>
    <div class="grid_16 spacer_30">
        &nbsp;</div>
</form>
</div>
<p>&nbsp;
</p>
</body></html>
