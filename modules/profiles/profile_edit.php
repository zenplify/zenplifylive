<?php
include_once("../../classes/init.php");
include_once("../headerFooter/header.php");
include_once("../../classes/profiles.php");
include_once("../../classes/settings.php");
include_once("../../classes/group.php");
include_once("../../classes/plan.php");

$database = new database();
$profile = new Profiles();
$settings = new settings();
$group = new group();
$plan = new plan();
$userInfo=$profile->getUserDetails($database,$_SESSION['userId']);
$profileStep = $_POST['txtProfileStep'];
if (isset($_POST['txtProfileStepQuestion'])) {
	$profileStep = $_POST['txtProfileStepQuestion'];
}

if ((isset($_POST['txtQuestionId'])) && ($_POST['txtQuestionId'] != '-1') && ($_POST['txtProfileStep'] == '-1')) {
	$profileStep = 3;
}
$vewProfile = false;
?>

<div class="container">
	<div id="warningProfileDiv">
		<div id="warningProfile" class="warningProfile">This field is required</div>
	</div>
    <span id="txtCheckboxTemplate" class="none">
        <input type="text" class="displayBlock txtCreateProfile txtCreateProfileCheckbox" name="txtCheckbox[]"
			   id="txtCheckbox[]" value=""/>
    </span>
    <span id="txtRadioBtnTemplate" class="none">
        <input type="text" class="displayBlock txtCreateProfile txtCreateProfileRadioBtn" name="txtRadioBtn[]"
			   id="txtRadioBtn[]" value=""/>
    </span>

	<?php include_once("profilesEditMenu.php"); ?>

	<div class="rightMiddleContent">
		<div id="backgroundPopupProfile">&nbsp;</div>
		<div id="popupProfilColumnOptions">
			<table width="100%" style="font-size:12px; text-align: center; margin-top:16px;">
				<tr>
					<td>One Column</td>
					<td>Two Column</td>
					<td>Three Column</td>
				</tr>
				<tr>
					<td><img src="../../images/profilecolumn/1column.png" width="200" height="200"></td>
					<td><img src="../../images/profilecolumn/2column.png" width="200" height="200"></td>
					<td><img src="../../images/profilecolumn/3column.png" width="200" height="200"></td>
				</tr>
			</table>
		</div>
		<div id="popupHeaderImage">
			<div class="margin0auto">
				<img style="right:0px; position: absolute; cursor: pointer; margin-top:-10px;"
					 src="../../images/close-icon.png" width="18" height="18" onclick="hideHeaderImage()">
				<img src="" id="headerImage" width="500" height="auto" class="margin0auto">
			</div>
		</div>

		<!-- Update Question View -->

		<?php
		if (isset($_POST)) {
			if ((isset($_POST['txtQuestionId'])) && ($_POST['txtQuestionId'] != '-1') && ($_POST['txtProfileStep'] == '-1')) {
				?><h1 class="gray">Update Question</h1><?php
				$questionDetails = $profile->getQuestionDetails($database, $_POST['txtQuestionId']);
				$questionChoiceDetails = $profile->getQuestionChoiceDetails($database, $_POST['txtQuestionId']);
				$leaderQuestionTypes = $settings->getLeaderCustomQuestionTypes('edit');

				$pageCustomRules = $profile->getCustomQuestionRules($database, $_POST['txtQuestionId']);
			if (!empty($pageCustomRules)) {
				?>
				<input type="hidden" name="txtShowWarning" id="txtShowWarning" value="1"><?php
			}
			else {
				?>
			<input type="hidden" name="txtShowWarning" id="txtShowWarning" value="0"><?php
			}
				$radioCount = $checkboxCount = 0;
				$checkDisplay = $radioDisplay = 'none';
				if ($questionDetails->typeId == 3) {
					$checkboxCount = count($questionChoiceDetails);
					$checkDisplay = '';
				} elseif ($questionDetails->typeId == 4) {
					$radioCount = count($questionChoiceDetails);
					$radioDisplay = '';
				} elseif ($questionDetails->typeId == 5) {
					$radioCount = count($questionChoiceDetails);
					$radioDisplay = '';
				}
				?>
				<form name="frmEditQuestion" id="frmEditQuestion" action="<?php echo $_SERVER['PHP_SELF'] ?>"
					  method="post" onsubmit="return vaidateCustomChoice('frmEditQuestion')">
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="2"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>">
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="txtQuestionId" id="txtQuestionId"
						   value="<?php echo $_POST['txtQuestionId']; ?>"/>
					<table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="100%">
						<tr>
							<td width="40%">Question:</td>
							<td>
								<input type="text" class="txtCreateProfile" name="txtFieldQuestion"
									   id="txtFieldQuestion" value="<?php echo $questionDetails->question ?>"/>

								<div id="txtFieldQuestionWarning" class="warningProfile">This field is required</div>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td width="40%">Question Label:</td>
							<td>
								<input type="text" class="txtCreateProfile" name="txtFieldQuestionLabel"
									   id="txtFieldQuestionLabel" value="<?php echo $questionDetails->label ?>"/>

								<div id="txtFieldQuestionLabelWarning" class="warningProfile">This field is required
								</div>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td width="40%">Question type</td>
							<td>
								<select id="ddlQuestionType" class="txtCreateProfile" name="ddlQuestionType" style="width: 488px;height: 33px;">
									<?php
									foreach ($leaderQuestionTypes as $lqt) {
										if ($questionDetails->typeId == $lqt->typeId) {
											?>
											<option selected="selected"
													value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
											<?php
										} else {
											?>
											<option
													value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
											<?php
										}
									}
									?>
								</select>

								<div id="ddlQuestionTypeWarning" class="warningProfile">This field is required</div>
							</td>
						</tr>
						<tr class="spacer checkboxCount <?php echo $checkDisplay; ?>">
							<td></td>
						</tr>
						<tr class="checkboxCount <?php echo $checkDisplay; ?>">
							<td width="40%">No of checkboxes</td>
							<td>
								<input style="width: 70px;" type="text" class="txtCreateProfile" name="txtCheckboxCount"
									   id="txtCheckboxCount" value="<?php echo $checkboxCount; ?>"/>
								<input type="hidden" name="txtCheckboxCountLast" id="txtCheckboxCountLast"
									   value="<?php echo $checkboxCount; ?>"/>

								<div id="txtCheckboxCountWarning" class="warningProfile">This field is required and it
									cant be Zero
								</div>
							</td>
						</tr>
						<tr class="spacer checkbox <?php echo $checkDisplay; ?>">
							<td></td>
						</tr>
						<tr class="checkbox <?php echo $checkDisplay; ?>">
							<td width="40%">Value</td>
							<td>
								<?php
								if ($questionDetails->typeId == 3 ) {
									foreach ($questionChoiceDetails as $qd) {
										?>
										<input class="displayBlock txtCreateProfile txtCreateProfileCheckbox"
											   type="text" name="txtCheckbox[]" id="txtCheckbox[]"
											   value="<?php echo $qd->choiceText ?>"/>
										<?php
									}
								}
								?>
							</td>
						</tr>
						<tr class="spacer radiobtnCount <?php echo $radioDisplay; ?>">
							<td></td>
						</tr>
						<tr class="radiobtnCount <?php echo $radioDisplay; ?>">
							<td width="40%">No of radio buttons</td>
							<td>
								<input style="width: 70px;" type="text" class="txtCreateProfile" name="txtRadioBtnCount"
									   id="txtRadioBtnCount" value="<?php echo $radioCount; ?>"/>
								<input style="width: 70px;"  type="hidden" name="txtRadioBtnCountLast" id="txtRadioBtnCountLast"
									   value="<?php echo $radioCount; ?>"/>

								<div id="txtRadioBtnCountWarning" class="warningProfile">This field is required and it
									cant be Zero
								</div>
							</td>
						</tr>
						<tr class="spacer radio <?php echo $radioDisplay; ?>">
							<td></td>
						</tr>
						<tr class="radiobtn <?php echo $radioDisplay; ?>">
							<td width="40%">Value</td>
							<td>
								<?php
								if ($questionDetails->typeId == 4 || $questionDetails->typeId == 5) {
									foreach ($questionChoiceDetails as $qd) {
										?>
										<input class="displayBlock txtCreateProfile txtCreateProfileRadioBtn"
											   type="text" name="txtRadioBtn[]" id="txtRadioBtn[]"
											   value="<?php echo $qd->choiceText; ?>"/>
										<?php
									}
								}
								?>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td width="40%">Mandatory</td>
							<td>
								<input type="radio" name="rdMandatory"
									   value="1" <?php if ($questionDetails->mandatory == 1)
									echo 'checked="checked"' ?> />
								Yes
								<input type="radio" name="rdMandatory"
									   value="0" <?php if ($questionDetails->mandatory == 0)
									echo 'checked="checked"' ?>/>
								No
								<div id="rdMandatoryWarning" class="warningProfile">This field is required</div>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<input value="Update" type="button" class="fieldbtn frmbtn" name="btnUpdateQuestion"
									   id="btnUpdateQuestion" onclick="submitFrm('frmEditQuestion','showWarning');"/>
								<input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelQuestion"
									   id="btnCancelQuestion"
									   onclick="submitFrm('frmEditQuestion','backNoEditQuestion');"/>
							</td>
						</tr>
					</table>
				</form>

			<?php
			}
			else
			{
			if ((isset($_POST['txtProfileStep'])) && ($_POST['txtProfileStep'] == 0))
			{
			$profileTemplateThumbnail = $profile->getselectedtemplate($database, $_POST['txtPageId']);
			$currentTemplate = $profileTemplateThumbnail->pageTemplateId;

			$currentStepId = 0;
			if (isset($_REQUEST['currentStepId'])) {
				if ($_REQUEST['currentStepId'] > $currentStepId) {
					$currentStepId = $_REQUEST['currentStepId'];
				} else {
					if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
						$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
					}
				}
			}
			?>
				<h1 class="gray">Selected Template</h1>
				<div class="profileThumb">

					<img class="profileThumb"
						 src="../../images/template/<?php echo $profileTemplateThumbnail->thumbnail; ?>" height="185"
						 width="168">
				</div>
				<h1 class="gray">Choose Template</h1><?php
				$profileTemplates = $profile->getProfileTemplates($database, '1', $currentTemplate);
				$i = 0;
			foreach ($profileTemplates as $pt) {
				$i++;
			if ($i == 1) {
				?><div class="profileThumb"><?php } ?>
				<a id="<?php echo $pt->pageTemplateId; ?>" onclick="setTemplate(this.id)">
					<img class="<?php echo(($i == 4) ? 'noMargin' : ''); ?>"
						 src="../../images/template/<?php echo $pt->thumbnail; ?>" height="185" width="168"> </a>
				<?php
			if ($i == 4) {
				?></div><?php $i = 0;
			}
			}
				?>
			<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" class="textfield" name="txtProfileStepPrevious" id="txtProfileStepPrevious"
						   value="0"/>
					<input type="hidden" class="textfield" name="txtProfileStep" id="txtProfileStep" value="1"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>">
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId" value="">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
					<input value="Next" type="submit" class="floatRight fieldbtn frmbtn" name="btnProfileSubmit"
						   id="btnProfileSubmit" onclick="submitFrm('frmCreateProfile','back')"/>
				</form>
				<?php
			} //Choose a Template
			elseif ((isset($_POST['txtProfileStep'])) && ($_POST['txtProfileStep'] == 1)) {
				?><h1 class="gray">Edit Profile Template</h1><?php
				$txtPageId = $_POST['txtPageId'];
				$fontStyle = $profile->getFontStyle($database);
				$bgColor = $profile->getProfileColor($database, '0,1');
				$fontColor = $profile->getProfileColor($database, '0,2');
				$accentColor = $profile->getProfileColor($database, 0);
				$txtError = $_POST['txtError'];
				$currentStepId = 1;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}

				if ((isset($_POST['txtPageId'])) && ($_POST['txtPageId'] != 1) && (!empty($_POST['txtPageId']))) {
					if ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 0) && (!(empty($_POST['txtProfileTemplateId'])))) {
						$profile->updateProfileTemplate($database, $_POST);
					}
					$profileTemplate = $profile->getPageDetails($database, $_POST['txtPageId']);
				} // get details from pages table
				?>
				<label class="error"><?php echo $txtError; ?></label>
			<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST" enctype="multipart/form-data">
					<input type="hidden" class="textfield" name="txtProfileStepPrevious" id="txtProfileStepPrevious"
						   value="1"/>
					<input type="hidden" class="textfield" name="txtProfileStep" id="txtProfileStep" value="2"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>">
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">

					<table name="tblCreateProfile" id="tblCreateProfile" cellspacing="0" cellpadding="0" width="100%">

						<tr>
							<td width="30%">Page Title</td>
							<td>
								<input onchange="updateInput()" class="txtCreateProfile" type="text"
									   name="pageBrowserTitle" id="pageBrowserTitle"
									   value="<?php echo $profileTemplate->pageBrowserTitle; ?>">
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td width="30%">Profile Welcome Text</td>
							<td>
                                <textarea onchange="updateInput()" class="required" rows="10" cols="65"
										  name="txtDefaultText"
										  id="txtDefaultText"><?php echo $profileTemplate->profileDefaultText; ?></textarea>

							</td>
						</tr>
                        <tr><td>&nbsp;</td>
                            <td style="color: #888484;">Please use[{First Name}] for first name and [{Last Name}] for last name</td>
                        </tr>

						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Video Link</td>
							<td>
								<input onchange="updateInput()" class="txtCreateProfile" type="text" name="txtVideoLink"
									   id="txtVideoLink" value="<?php echo $profileTemplate->profileVideoLink; ?>">

							</td>
						</tr>
                        <tr class="spacer">
                            <td></td>
                        </tr>
                        <tr>
                            <td>Show Video</td>
                            <td><input type="checkbox" <?php if ($profileTemplate->isDefaultVideoshow == '1') {
                                    echo 'checked';
                                } ?> name="isDefaultVideoshow" id="isDefaultVideoshow" value="1"></td>
                        </tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Profile Short Name</td>
							<td>
								<input onchange="updateInput()" class="txtCreateProfile" type="text" name="txtNewCode"
									   id="txtNewCode" value="<?php echo $profileTemplate->newCode; ?>">
								<span id="msgProfile"></span><span id="loadingImg"></span>
								<input type="hidden" name="uniqueProfileShortName" id="uniqueProfileShortName" value="1">
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Text Font Style</td>
							<td>
								<select onchange="updateInput()" class="txtCreateProfile required"
										id="ddlProfileFontStyle" name="ddlProfileFontStyle" style="width: 488px;height: 33px;">
									<option value="">Please Select</option>
									<?php
									foreach ($fontStyle as $fs) {
										if ($fs->fontId == $profileTemplate->profileFontStyle) {
											?>
											<option selected="selected"
													value="<?php echo $fs->fontId; ?>"><?php echo $fs->name; ?></option><?php
										} else {
											?>
											<option
											value="<?php echo $fs->fontId; ?>"><?php echo $fs->name; ?></option><?php
										}
									}
									?>
								</select>

							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Header Image (960px X 100px) <br>
								<?php
								if (!empty($profileTemplate->profileHeader)) {
									?><a class="link"
										 onClick="showHeaderImage('<?php echo $profileTemplate->profileHeader; ?>')">
										View Existing</a><?php
								}
								?>

							</td>
							<td>
								<input onchange="updateInput()" type="file" class="txtCreateProfile"
									   name="txtHeaderImage" id="txtHeaderImage">

							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Accent Color</td>
							<td>
								<select onchange="updateInput()" class="txtCreateProfile required" id="ddlAccentColor"
										name="ddlAccentColor" style="width: 488px;height: 33px;">
									<option value="">Please Select</option>
									<?php
                foreach ($accentColor as $ac){
                    if ($ac->colorId == $profileTemplate->accentColor) {
                        ?>
                        <option selected="selected" value="<?php echo $ac->colorId; ?>"
                                style="background-color: <?php echo $ac->actualName; ?>; <?php if ($ac->actualName == 'black' || $ac->actualName == 'green' || $ac->actualName == 'blue' || $ac->actualName == 'yellowgreen' || $ac->actualName == 'skyblue' || $ac->actualName == '#51004d') {
                                    echo 'color:white;';
                                } ?>"><?php echo $ac->name; ?></option><?php
                    } else {
                        ?>
                        <option value="<?php echo $ac->colorId; ?>"
                                style="background-color: <?php echo $ac->actualName; ?>; <?php if ($ac->actualName == 'black' || $ac->actualName == 'green' || $ac->actualName == 'blue' || $ac->actualName == 'yellowgreen' || $ac->actualName == 'skyblue'|| $ac->actualName == '#51004d') {
                                    echo 'color:white;';
                                } ?>"><?php echo $ac->name; ?></option><?php
                    }
                }
									?>
								</select>

							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr valign="top">
							<td>Profile Columns <a class="link" onclick="showProfilesColumnOptions()">&#63;</a></td>
							<td>
								<select onchange="showColumn(this.value)" id="ddlColumnCount" name="ddlColumnCount"
										class="txtCreateProfile" style="width: 488px;height: 33px;">
									<optgroup label="Single">
										<option <?php if (($profileTemplate->columnCount == 1) || (empty($profileTemplate->columnCount)))
											echo 'selected="selected";' ?> value="1">One
										</option>
									</optgroup>
									<optgroup label="Multi">
										<option <?php if ($profileTemplate->columnCount == 2)
											echo 'selected="selected";' ?> value="2">Two
										</option>
										<option <?php if ($profileTemplate->columnCount == 3)
											echo 'selected="selected";' ?> value="3">Three
										</option>
									</optgroup>
								</select>

							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input value="Back" type="button" class="fieldbtn frmbtn" name="btnProfileCancel"
									   id="btnProfileCancel" onclick="submitFrm('frmCreateProfile','backTemplate')"/>
								<input value="Save And Continue" type="Submit" class="fieldbtn frmbtn btnSecond"
									   name="btnProfileSubmit" id="btnProfileSubmit"/>
							</td>
						</tr>
					</table>
				</form>
			<?php
			} //Edit Profile Template
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 2))) || ($_POST['txtProfileStepQuestion'] == 2))
			{
			$leaderQuestionTypes = $settings->getLeaderCustomQuestionTypes();
			$fileError = 0;
			$questionAdd = 0; //used to execute either customQuestionAdd code or Normal Profile Step
			$profileAdded = 0;
			$questionUpdated = 0;
			$txtPageId = $_POST['txtPageId'];

			$currentStepId = 2;
			if (isset($_REQUEST['currentStepId'])) {
				if ($_REQUEST['currentStepId'] > $currentStepId) {
					$currentStepId = $_REQUEST['currentStepId'];
				} else {
					if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
						$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
					}
				}
			}

			?>
			<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" name="txtError" id="txtError"/>
					<input type="hidden" class="textfield" name="txtProfileStepPrevious" id="txtProfileStepPrevious"
						   value="2"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="3"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $txtPageId; ?>"/>
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="txtQuestionId" id="txtQuestionId" value="-1">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
				</form>
			<?php
			if ((isset($_POST['txtPageId'])) && ($_POST['txtPageId'] != 1) && (!empty($_POST['txtPageId']))) {
			if ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 1))
			{
			if (empty($_FILES["txtHeaderImage"]["name"])) {
				$fileError = 0;
				$fileName = 'old';
			} elseif (($_FILES["txtHeaderImage"]["type"] == "image/gif")
					|| ($_FILES["txtHeaderImage"]["type"] == "image/jpeg")
					|| ($_FILES["txtHeaderImage"]["type"] == "image/jpg")
					|| ($_FILES["txtHeaderImage"]["type"] == "image/pjpeg")
					|| ($_FILES["txtHeaderImage"]["type"] == "image/x-png")
					|| ($_FILES["txtHeaderImage"]["type"] == "image/png")
			) {
				if ($_FILES["txtHeaderImage"]["error"] > 0) {
					$fileError = 1;
				} else {
					$ext = pathinfo($_FILES["txtHeaderImage"]["name"], PATHINFO_EXTENSION);
					$fileName = uniqid() . time() . '.' . $ext;
					move_uploaded_file($_FILES["txtHeaderImage"]["tmp_name"], "../../images/profile/" . $fileName);
				}
			} else {
				$fileError = 1;
			}
			if ($fileError == 1)
			{
			?>
				<script type="text/javascript">
					$("#txtError").val('Please select a valid image to upload');
					$("#txtProfileStep").val('1');
					$("#txtPageId").val('<?php echo $txtPageId; ?>');
					$("#frmCreateProfile").submit();
				</script>
			<?php
			} //goback and ask to upload a valid file
			elseif ($fileError == 0 && $_POST['uniqueProfileShortName'] == 1) {
				$profile->editPageDetails($database, $_POST, $fileName, 'profile');
			}
			}
			elseif (isset($_POST['txtProfileStepQuestion']) && ($_POST['txtProfileStepQuestion'] == 2) && ($_POST['txtProfileStepPrevious'] == 2)) {
				$profile->addPageQuestion($database, $_POST);
				$questionAdd = 1; // page came from CustomQuestionAdd form
			} elseif ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 4)) {
				$profileAdded = 1;
			} elseif ((isset($_POST['txtQuestionId'])) && ($_POST['txtQuestionId'] != '-1')) {
				$profile->updatePageQuestion($database, $_POST);
				$questionUpdated = 1;
				$profileAdded = 1;
				$fileError = 0;
			}
			}
			?>
				<div id="questionsList">
					<h1 class="gray">Profile Questions</h1>
					<span id="clickNewField" class="fieldbtn" onClick="addQuestion()">Add Question</span>
					<?php
					$profileDefaultQuestions = $profile->getPageQuestions($database, $txtPageId, 1);
					$profileCustomQuestions = $profile->getPageQuestions($database, $txtPageId, 0);
					?>
					<input type="hidden" name="defaultQuestionsCount" id="defaultQuestionsCount"
						   value="<?php echo count($profileDefaultQuestions) ?>">

					<div class="clear" style="height:15px;"><span id="_msg">&nbsp;</span></div>
					<table cellpadding="0" cellspacing="0" id="tblQuestions" width="100%" class="tablesorter">
						<thead>
						<tr>
							<th width="171px">Question</th>
							<th width="120px">Type</th>
							<th width="136px">Status</th>
							<th width="206px">Date & Time</th>
							<th width="69px">Action</th>
						</tr>
						</thead>
						<tbody class="sortableFields">
						<?php
						foreach ($profileDefaultQuestions as $pq) {
							?>
							<tr class="notSortable" id="<?php echo $pq->questionId; ?>">
								<td width="169px"><?php echo $pq->label; ?></td>
								<td width="120px"><?php echo ucfirst($pq->typeTitle); ?></td>
								<?php
								if ($pq->enableStatus == 1) {
									?>
									<td width="136px">
									<a id="status<?php echo $pq->questionId; ?>"
									   onclick="toggleStatus('<?php echo $pq->questionId; ?>','<?php echo $pq->status; ?>','toggleDefaultQuestions','<?php echo $txtPageId; ?>')"><?php echo $pq->defaultStatus == '1' ? 'Active' : 'Inactive'; ?></a>
									</td><?php
								} else {
									?>
									<td width="136px">Active</td><?php
								}
								?>
								<td width="206px">_<?php //echo date("Y-m-d H:i:s");
									?></td>
								<td width="69px">_</td>
							</tr>
							<?php
						}

						if ($profileCustomQuestions) {
							?>
							<tr class="spacer sortableSpacer">
								<td colspan="5"></td>
							</tr><?php
							foreach ($profileCustomQuestions as $pq) {
								?>
								<tr id="<?php echo $pq->questionId; ?>">
									<td width="169px"><?php echo $pq->label; ?></td>
									<td width="120px"><?php echo ucfirst($pq->typeTitle); ?></td>
									<td width="136px">
										<a id="status<?php echo $pq->questionId; ?>"
										   onclick="toggleStatus('<?php echo $pq->questionId; ?>','<?php echo $pq->status; ?>','togglePageQuestions')"><?php echo($pq->status == 1 ? 'Active' : 'Inactive'); ?></a>
									</td>
									<td width="206px"><?php echo strtok($pq->datetime, " "); ?></td>
									<td width="69px">
										<a class="editQuickLink" id="<?php echo $pq->questionId; ?>"
										   onclick="editQuestion('<?php echo $pq->questionId; ?>','frmCreateProfile')">Edit</a>
									</td>
								</tr>
								<?php
							}

						}
						?>
						</tbody>
					</table>
					<input name="btnBack" id="btnBack" type="button" class="fieldbtn frmbtn floatLeft small"
						   value="Back" onclick="submitFrm('frmCreateProfile','back')">
					<input name="btnNext" id="btnNext" type="button" class="fieldbtn frmbtn floatRight small"
						   value="Save And Continue" onclick="submitFrm('frmCreateProfile','next')">
				</div>

				<!--New question module starts here-->
				<div id="newQuestion" class="none">
					<h1 class="gray">Add Profile Question</h1>

					<form name="frmNewQuestions" id="frmNewQuestions" action="<?php echo $_SERVER['PHP_SELF']; ?>"
						  method="post" onsubmit="return vaidateCustomChoice('frmNewQuestions')">
						<input type="hidden" class="textfield" name="txtProfileStepPrevious" id="txtProfileStepPrevious"
							   value="2"/>
						<input type="hidden" name="txtProfileStepQuestion" id="txtProfileStepQuestion" value="2"/>
						<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $txtPageId; ?>">
						<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
							   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
						<table cellpadding="0" cellspacing="0" id="tblSettingsNewField" width="90%">
							<tr>
								<td width="40%">Question:</td>
								<td>
									<input type="text" name="txtFieldQuestion" id="txtFieldQuestion"
										   class="txtCreateProfile"/>

									<div id="txtFieldQuestionWarning" class="warningProfile">This field is required
									</div>
								</td>
							</tr>
							<tr class="spacer">
								<td></td>
							</tr>
							<tr>
								<td width="40%">Question Label:</td>
								<td>
									<input type="text" name="txtFieldQuestionLabel" id="txtFieldQuestionLabel"
										   class="txtCreateProfile"/>

									<div id="txtFieldQuestionLabelWarning" class="warningProfile">This field is required
									</div>
								</td>
							</tr>
							<tr class="spacer">
								<td></td>
							</tr>
							<tr>
								<td width="40%">Question type</td>
								<td>
									<select id="ddlQuestionType" name="ddlQuestionType" class="txtCreateProfile" style="width: 488px;height: 33px;">
										<option value="0">Select Field type</option>
										<?php
										foreach ($leaderQuestionTypes as $lqt) {
											?>
											<option
													value="<?php echo $lqt->typeId; ?>"><?php echo $lqt->typeTitle; ?></option>
											<?php
										}
										?>
									</select>

									<div id="ddlQuestionTypeWarning" class="warningProfile">This field is required</div>
								</td>
							</tr>
							<tr class="spacer checkboxCount none">
								<td></td>
							</tr>
							<tr class="checkboxCount none">
								<td width="40%">No of checkboxes</td>
								<td>
									<input style="width: 70px;" type="text" name="txtCheckboxCount" id="txtCheckboxCount" value="0"
										   class="txtCreateProfile"/>
									<input type="hidden" name="txtCheckboxCountLast" id="txtCheckboxCountLast"
										   value="0"/>

									<div id="txtCheckboxCountWarning" class="warningProfile">This field is required and
										it cant be Zero
									</div>
								</td>
							</tr>
							<tr class="spacer checkbox none">
								<td></td>
							</tr>
							<tr class="checkbox none">
								<td width="40%">Value</td>
								<td></td>
							</tr>
							<tr class="spacer radiobtnCount none">
								<td></td>
							</tr>
							<tr class="radiobtnCount none">
								<td width="40%">No of radio buttons</td>
								<td>
									<input style="width: 70px;"  type="text" name="txtRadioBtnCount" id="txtRadioBtnCount" value="0"
										   class="txtCreateProfile"/>
									<input type="hidden" name="txtRadioBtnCountLast" id="txtRadioBtnCountLast"
										   value="0"/>

									<div id="txtRadioBtnCountWarning" class="warningProfile">This field is required and
										it cant be Zero
									</div>
								</td>
							</tr>
							<tr class="spacer radiobtn none">
								<td></td>
							</tr>
							<tr class="radiobtn none">
								<td width="40%">Value</td>
								<td></td>
							</tr>
							<tr class="spacer">
								<td></td>
							</tr>
							<tr>
								<td width="40%">Mandatory</td>
								<td>
									<input type="radio" name="rdMandatory" value="1"/>
									Yes
									<input type="radio" name="rdMandatory" value="0"/>
									No
									<div id="rdMandatoryWarning" class="warningProfile">This field is required</div>
								</td>
							</tr>
							<tr class="spacer">
								<td></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<input value="Add" type="submit" class="fieldbtn frmbtn" name="btnAddQuestion"
										   id="btnAddQuestion"/>
									<input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnCancelNewField"
										   id="btnCancelNewQuestion" onclick="cancelNewQuestion()"/>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<!--New question module ends here-->
				<?php
			} //Profile Question & Add Profile Question
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 4)))) {
				?><h1 class="gray">Thank you Page</h1><?php

				$profileTemplate = $profile->getPageDetails($database, $_POST['txtPageId']);
				$fontStyle = $profile->getFontStyle($database);
				$bgColor = $profile->getProfileColor($database, '0,1');
				$fontColor = $profile->getProfileColor($database, '0,2');
				$txtError = $_POST['txtError'];
				$txtPageId = $_POST['txtPageId'];

				$currentStepId = 4;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}

				?>
				<label class="error"><?php echo $txtError; ?></label>

				<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST" enctype="multipart/form-data">
					<input type="hidden" class="textfield" name="txtProfileStep" id="txtProfileStep" value="5"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>">
					<input type="hidden" name="txtProfileStepPrevious" id="txtProfileStepPrevious" value="4">
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
					<table name="tblCreateProfile" id="tblCreateProfile" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="30%">Default text</td>
							<td>
                                <textarea onchange="updateInput()" class="required" rows="10" cols="65"
										  name="txtDefaultText"
										  id="txtDefaultText"><?php echo $profileTemplate->thankyouDefaultText; ?></textarea>

							</td>
						</tr>
                        <tr><td>&nbsp;</td>
                            <td style="color: #888484;">Please use[{First Name}] for first name and [{Last Name}] for last name</td>
                        </tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Video Link</td>
							<td>
								<input onchange="updateInput()" class="txtCreateProfile" type="text" name="txtVideoLink"
									   id="txtVideoLink" value='<?php echo $profileTemplate->thankyouVideoLink; ?>'>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Show Video</td>
							<td>
								<input type="checkbox"
									   name="videoVisibility" <?php if ($profileTemplate->thankyouVideoVisibility == '1') {
									echo 'checked';
								} ?> id="videoVisibility" value="1">
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Text Font Style</td>
							<td>
								<select onchange="updateInput()" class="txtCreateProfile required"
										id="ddlProfileFontStyle" name="ddlProfileFontStyle" style="width: 488px;height: 33px;">
									<option value="">Please Select</option>
									<?php
									foreach ($fontStyle as $fs) {
										if ($fs->fontId == $profileTemplate->thankyouFontStyle) {
											?>
											<option selected="selected"
													value="<?php echo $fs->fontId; ?>"><?php echo $fs->name; ?></option><?php
										} else {
											?>
											<option
											value="<?php echo $fs->fontId; ?>"><?php echo $fs->name; ?></option><?php
										}
									}
									?>
								</select>
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td>Header Image (960px X 100px) <br>
								<?php
								if (!empty($profileTemplate->thankyouHeader)) {
									?><a class="link"
										 onClick="showHeaderImage('<?php echo $profileTemplate->thankyouHeader; ?>')">
										View Existing</a><?php
								}
								?>
							</td>
							<td>
								<input onchange="updateInput()" type="file" class="txtCreateProfile"
									   name="txtHeaderImage" id="txtHeaderImage">
							</td>
						</tr>
						<tr class="spacer">
							<td></td>
						</tr>

						<tr class="spacer">
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input value="Back" type="button" class="fieldbtn frmbtn" name="btnProfileCancel"
									   id="btnProfileCancel" onclick="submitFrm('frmCreateProfile','backQuestions')"/>
								<input value="Save And Continue" type="Submit" class="fieldbtn frmbtn btnSecond"
									   name="btnProfileSubmit" id="btnProfileSubmit"/>
							</td>
						</tr>
					</table>
				</form>
				<?php
			} // Thank you Page
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 5)))) {
				?><h1 class="gray">Assign Campaign or Group to Profile</h1><?php
				$fileError = 0;
				$pageCustomRules;
				$txtPageId = $_POST['txtPageId'];
				$txtProfileTemplateId = $_POST['txtProfileTemplateId'];
				$allGroups = $group->getAllUserGroups($_SESSION['userId']);
				$allPlans = $plan->GetAllPlans($_SESSION['userId']);

				$currentStepId = 5;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}

				?>
				<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" name="txtError" id="txtError"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="6"/>
					<input type="hidden" name="txtProfileStepPrevious" id="txtProfileStepPrevious" value="5"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
					<?php
					if ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 4)) {
						if (empty($_FILES["txtHeaderImage"]["name"])) {
							$fileError = 0;
							$fileName = 'old';
						} elseif (($_FILES["txtHeaderImage"]["type"] == "image/gif")
								|| ($_FILES["txtHeaderImage"]["type"] == "image/jpeg")
								|| ($_FILES["txtHeaderImage"]["type"] == "image/jpg")
								|| ($_FILES["txtHeaderImage"]["type"] == "image/pjpeg")
								|| ($_FILES["txtHeaderImage"]["type"] == "image/x-png")
								|| ($_FILES["txtHeaderImage"]["type"] == "image/png")
						) {
							if ($_FILES["txtHeaderImage"]["error"] > 0) {
								$fileError = 1;
							} else {
								$ext = pathinfo($_FILES["txtHeaderImage"]["name"], PATHINFO_EXTENSION);
								$fileName = uniqid() . time() . '.' . $ext;
								move_uploaded_file($_FILES["txtHeaderImage"]["tmp_name"], "../../images/profile/" . $fileName);
							}
						} else {
							$fileError = 1;
						}
						if ($fileError == 1) {
							?>
							<script type="text/javascript">
								$("#txtError").val('Please select a Valid image to upload');
								$("#txtProfileStep").val('4');
								$("#frmCreateProfile").submit();
							</script>
							<?php
							die;
						} //goback and ask to upload a valid file
						elseif ($fileError == 0) {
							$profile->editPageDetails($database, $_POST, $fileName, 'thankyou');
						} //update thankyou fields in page table
						$pageCustomRules = $profile->getPageCustomRules($database, $_POST['txtPageId'], 1);
					}

					$groupIds = $profile->getCommaSeperatedIdsinCustomRules($database, $_POST['txtPageId'], 1);
					$plans = $profile->getCommaSeperatedIdsinCustomRules($database, $_POST['txtPageId'], 2);
					$planIds;
					$planSteps = array();
					$checkedCount = 0;
					foreach ($plans as $p) {
						$planSteps[] = $p->stepId;
						if (empty($plans)) {
							$planIds = $p->planId;
						} else {
							$planIds = $planIds . ',' . $p->planId;
						}
					}

					if (!empty($pageCustomRules)) {
						$groupIds = $profile->getCommaSeperatedIdsinCustomRules($database, $_POST['txtPageId'], 1);
						$plans = $profile->getCommaSeperatedIdsinCustomRules($database, $_POST['txtPageId'], 2);
						$planIds;
						$planSteps = array();
						$checkedCount = 0;
						foreach ($plans as $p) {
							$planSteps[] = $p->stepId;
							if (empty($plans)) {
								$planIds = $p->planId;
							} else {
								$planIds = $planIds . ',' . $p->planId;
							}
						}

					}

					?>
					<div id="pageGroups">
						<h2 class="gray">Assign Groups</h2>
						<table name="tblAssignGroups" id="tblAssignGroups">
							<?php
							foreach ($allGroups as $ag) {
								?>
								<tr bgcolor="<?php echo $ag->color; ?>">
									<td>
										<?php
										if (strstr($ag->groupId, $groupIds)) {
											?>
											<input onchange="updateInput()" type="checkbox" name="chkGroup[]"
												   checked="checked" id="chkGroup_<?php echo $ag->groupId; ?>"
												   value="<?php echo $ag->groupId; ?>"><?php echo $ag->name;
										} else {
											?>
											<input onchange="updateInput()" type="checkbox" name="chkGroup[]"
												   id="chkGroup_<?php echo $ag->groupId; ?>"
												   value="<?php echo $ag->groupId; ?>"><?php echo $ag->name;
										}
										?>
									</td>
								</tr>
								<?php
							}
							?>
						</table>
					</div>
					<div id="pageCampaign">
						<h2 class="gray">Assign Campaigns with Specified Steps</h2>
						<table name="tblAssignPlans" id="tblAssignPlans">
							<?php
							foreach ($allPlans as $ap)
							{
							$allPlanSteps = $plan->GetPlanStepsInfo($ap->planId);
							if (strstr($planIds, $ap->planId))
							{
							?>
							<tr id="trPlanSteps_<?php echo $ap->planId; ?>" style="background-color:#007DB1 !important">
								<td>
									<?php
									$checkedCount++;
									?>
									<input onchange="updateInput()" type="checkbox" checked="checked" name="chkPlan[]"
										   id="chkPlan_<?php echo $ap->planId; ?>"
										   onclick="showPlan('<?php echo $ap->planId; ?>')"
										   value="<?php echo $ap->planId; ?>"><?php echo $ap->title; ?>
									<select name="ddlPlanSteps_<?php echo $ap->planId; ?>"
											id="ddlPlanSteps_<?php echo $ap->planId; ?>" class="planSteps" style="width: 488px;height: 33px;">
										<?php
										}
										else
										{
										?>
										<tr id="trPlanSteps_<?php echo $ap->planId; ?>" bgcolor="">
											<td>
												<input type="checkbox" name="chkPlan[]"
													   id="chkPlan_<?php echo $ap->planId; ?>"
													   onclick="showPlan('<?php echo $ap->planId; ?>')"
													   value="<?php echo $ap->planId; ?>"><?php echo $ap->title; ?>
												<select onchange="updateInput()"
														name="ddlPlanSteps_<?php echo $ap->planId; ?>"
														id="ddlPlanSteps_<?php echo $ap->planId; ?>"
														class="planSteps none" style="width: 488px;height: 33px;">
													<?php
													}
													foreach ($allPlanSteps as $aps) {
														if (empty($planSteps)) {
															?>
															<option selected="selected"
																	value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
															<?php
														} else {

															foreach ($planSteps as $ps) {
																if ($ps == $aps->stepId) {
																	?>
																	<option selected="selected"
																			value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
																	<?php
																} else {
																	?>
																	<option
																			value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
																	<?php
																}
															}
														}
													}
													?>
												</select>
											</td>
										</tr>
										<?php
										}

										?>
						</table>
					</div>
					<div class="clear spacer"></div>
					<input value="Save And Continue" type="submit" class="fieldbtn frmbtn floatRight"
						   name="btnProfileSubmit" id="btnProfileSubmit"/>
					<input value="Back" type="button" class="fieldbtn frmbtn floatLeft" name="btnProfileCancel"
						   id="btnProfileCancel" onclick="submitFrm('frmCreateProfile','backThankyou')"/>
				</form>
				<?php
			} // Assign Campaign or Group
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 6)))) {
				$txtPageId = $_POST['txtPageId'];

				$currentStepId = 6;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}

				?><h1 class="gray">View Custom Rules for Question Responses</h1>
				<span id="clickNewField" class="fieldbtn largeQuestion none"
					  onclick="submitFrm('frmCreateProfile','nextAddCustomQuestionRule')">Add Custom Rules</span>
				<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="8"/>
					<input type="hidden" name="txtProfileStepPrevious" id="txtProfileStepPrevious" value="6"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="txtEditCustomRuleQuestion" id="txtEditCustomRuleQuestion" value="0">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
					<?php
					if ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 7)) {
						if ((isset($_POST['btnProfileSubmit'])) && ($_POST['btnProfileSubmit'] == 'Add') && ($_POST['txtEditCustomRuleQuestion'] == '0')) {
							$profile->addPageCustomQuestionRules($database, $_POST);
						} elseif ((isset($_POST['txtEditCustomRuleQuestion'])) && ($_POST['txtEditCustomRuleQuestion'] != 0) && (!empty($_POST['txtEditCustomRuleQuestion']))) {
							$profile->updatePageCustomQuestionRules($database, $_POST);
						}
					} elseif ((isset($_POST['txtProfileStepPrevious'])) && ($_POST['txtProfileStepPrevious'] == 5)) {
						$profile->updatePageCustomRules($database, $_POST);
					}
					$pageQuestionCustomRules = $profile->getPageCustomRules($database, $_POST['txtPageId'], 2);

					?>
					<div class="clear spacer"></div>
					<span class="none" id="_msg">&nbsp;</span>

					<div class="clear"></div>
					<?php
					if ($pageQuestionCustomRules) {
						?>
						<script type="text/javascript">
							$("#clickNewField").show();
						</script>
						<table name="tblPageCustomQuestionRules" id="tblPageCustomQuestionRules">
							<thead>
							<tr>
								<th>Question</th>
								<th>Question Responses</th>
								<th>Groups</th>
								<th>Campaigns</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ($pageQuestionCustomRules as $pqcr) {
								$questionDetails = $profile->getPageCustomQuestionRulesDetails($database, $pqcr->questionId, $pqcr->questionChoices)
								?>
								<tr>
									<td><?php echo $questionDetails->question; ?></td>
									<td><?php echo $questionDetails->choices; ?></td>
									<td><?php echo $pqcr->groupName; ?></td>
									<td><?php echo $pqcr->planName; ?></td>
									<td>
										<a onclick="editCustomQuestionRule('frmCreateProfile','<?php echo $pqcr->questionId; ?>', '<?php echo $pqcr->questionChoices; ?>','<?php echo $pqcr->pageCustomRulesDetailsid; ?>')">Edit</a>
										<a id="status<?php echo $pqcr->pageCustomRulesDetailsid ?>"
										   onclick="toggleStatus('<?php echo $pqcr->pageCustomRulesDetailsid; ?>','<?php echo $pqcr->status; ?>','toggleCustomRuleQuestion')"><?php echo $pqcr->status == '1' ? 'Active' : 'Inactivate'; ?>
										</a>
									</td>
								</tr>
								<?php
							}
							?>
							</tbody>
						</table>
					<?php
					}
					else
					{
					?>
						<a class="link centreText" onclick="submitFrm('frmCreateProfile','nextAddCustomQuestionRule')">Click
							here to add a Custom Rule</a><?php
					} ?>
					<div class="clear spacer"></div>
					<input value="Save And Continue" type="Submit" class="fieldbtn frmbtn floatRight"
						   name="btnProfileSubmit" id="btnProfileSubmit"/>
					<input value="Back" type="button" class="fieldbtn frmbtn floatLeft" name="btnProfileCancel"
						   id="btnProfileCancel"
						   onclick="submitFrm('frmCreateProfile','backAssignGroupsandCampaigns')"/>
				</form>

				<div class="clear spacer"></div>

				<?php
			} // View Custom Rules
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 7)))) {
				$currentStepId = 7;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}

				?><h1 class="gray">Add Custom Rules for Question Responses</h1>
				<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="6"/>
					<input type="hidden" name="txtProfileStepPrevious" id="txtProfileStepPrevious" value="7"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="txtProfileTemplateId" id="txtProfileTemplateId"
						   value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input type="hidden" name="txtEditCustomRuleQuestion" id="txtEditCustomRuleQuestion"
						   value="<?php echo $_POST['txtEditCustomRuleQuestion']; ?>">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
					<?php
					$editQuestionDetails = $_POST['txtEditCustomRuleQuestion'];
					if ($editQuestionDetails != 0) {
						$questionExplodes = explode('-', $editQuestionDetails);
						$editQuestionId = $questionExplodes[0];
						$editQuestionChoices = $questionExplodes[1];
						$pageCustomRulesDetailsId = $questionExplodes[2];
						$groupIds = $profile->getCommaSeperatedIdsinCustomQuestionRules($database, $_POST['txtPageId'], 1, $pageCustomRulesDetailsId);
						$plans = $profile->getCommaSeperatedIdsinCustomQuestionRules($database, $_POST['txtPageId'], 2, $pageCustomRulesDetailsId);
						$planIds;
						$planSteps = array();
						$checkedCount = 0;
						foreach ($plans as $p) {
							$planSteps[] = $p->stepId;
							if (empty($plans)) {
								$planIds = $p->planId;
							} else {
								$planIds = $planIds . ',' . $p->planId;
							}
						}

					}
					$txtPageId = $_POST['txtPageId'];
					$txtProfileTemplateId = $_POST['txtProfileTemplateId'];
					$allGroups = $group->getAllUserGroups($_SESSION['userId']);
					$allPlans = $plan->GetAllPlans($_SESSION['userId']);

					$pageQuestions = $profile->getPageQuestionsForCustomRules($database, $txtPageId);
					?>
					<table id="tblPageCustomQuestions" name="tblPageCustomQuestions">
						<tr>
							<td>Select Question</td>
							<td>
								<select id="ddlCustomQuestionId" name="ddlCustomQuestionId"
										onChange="showQuestionResponse(this.value)" class="txtCreateProfile" style="width: 488px;height: 33px;">
									<option value="">Plese Select</option>
									<?php
									foreach ($pageQuestions as $pq) {
										if ($pq->questionId == $editQuestionId) {
											?>
											<option selected="selected"
													value="<?php echo $pq->questionId; ?>"><?php echo $pq->question; ?></option><?php
										} else {
											?>
											<option
											value="<?php echo $pq->questionId; ?>"><?php echo $pq->question; ?></option><?php
										}
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="spacer"></td>
						</tr>
						<tr id="questionResponses">
							<td>Question Responses</td>
							<td>
								<?php
								foreach ($pageQuestions as $pq)
								{
								$choices = $profile->getQuestionChoices($database, $pq->questionId);

								if ($pq->questionId == $editQuestionId)
								{
								?><span class="" id="questionResponses_<?php echo $pq->questionId; ?>"><?php
									}
									else
									{
									?><span class="none" id="questionResponses_<?php echo $pq->questionId; ?>"><?php
										}
										foreach ($choices as $c) {
											if (strstr($editQuestionChoices, $c->choiceId)) {
												?>
												<input type="checkbox" checked="checked"
													   name="questionChoice_<?php echo $pq->questionId; ?>[]"
													   value="<?php echo $c->choiceId ?>"><?php echo $c->choiceText . '<br>';
											} else {
												?>
												<input type="checkbox"
													   name="questionChoice_<?php echo $pq->questionId; ?>[]"
													   value="<?php echo $c->choiceId ?>"><?php echo $c->choiceText . '<br>';
											}
										}
										?></span><?php
									}
									?>
							</td>
						</tr>
					</table>
					<div id="pageGroups">
						<h2 class="gray">Assign Groups</h2>
						<table name="tblAssignGroups" id="tblAssignGroups">
							<?php
							foreach ($allGroups as $ag) {
								?>
								<tr bgcolor="<?php echo $ag->color; ?>">
									<td>
										<?php
										if (strstr($groupIds, $ag->groupId)) {
											?>
											<input type="checkbox" name="chkGroup[]" checked="checked"
												   id="chkGroup_<?php echo $ag->groupId; ?>"
												   value="<?php echo $ag->groupId; ?>"><?php echo $ag->name;
										} else {
											?>
											<input type="checkbox" name="chkGroup[]"
												   id="chkGroup_<?php echo $ag->groupId; ?>"
												   value="<?php echo $ag->groupId; ?>"><?php echo $ag->name;
										}
										?>
									</td>
								</tr>
								<?php
							}
							?>
						</table>
					</div>
					<div id="pageCampaign">
						<h2 class="gray">Assign Campaigns with Specified Steps</h2>
						<table name="tblAssignPlans" id="tblAssignPlans">
							<?php
							foreach ($allPlans as $ap)
							{
							$allPlanSteps = $plan->GetPlanStepsInfo($ap->planId);
							if (strstr($planIds, $ap->planId))
							{
							?>
							<tr id="trPlanSteps_<?php echo $ap->planId; ?>" style="background-color:#007DB1 !important">
								<td>
									<?php
									$checkedCount++;
									?>
									<input type="checkbox" checked="checked" name="chkPlan[]"
										   id="chkPlan_<?php echo $ap->planId; ?>"
										   onclick="showPlan('<?php echo $ap->planId; ?>')"
										   value="<?php echo $ap->planId; ?>"><?php echo $ap->title; ?>
									<select name="ddlPlanSteps_<?php echo $ap->planId; ?>"
											id="ddlPlanSteps_<?php echo $ap->planId; ?>" class="planSteps" style="width: 488px;height: 33px;">
										<?php
										}
										else
										{
										?>
										<tr id="trPlanSteps_<?php echo $ap->planId; ?>" bgcolor="">
											<td>
												<input type="checkbox" name="chkPlan[]"
													   id="chkPlan_<?php echo $ap->planId; ?>"
													   onclick="showPlan('<?php echo $ap->planId; ?>')"
													   value="<?php echo $ap->planId; ?>"><?php echo $ap->title; ?>
												<select name="ddlPlanSteps_<?php echo $ap->planId; ?>"
														id="ddlPlanSteps_<?php echo $ap->planId; ?>"
														class="planSteps none" style="width: 488px;height: 33px;">
													<?php
													}
													foreach ($allPlanSteps as $aps) {
														if (empty($planSteps)) {
															?>
															<option selected="selected"
																	value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
															<?php
														} else {
															foreach ($planSteps as $ps) {
																if ($ps == $aps->stepId) {
																	?>
																	<option selected="selected"
																			value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
																	<?php
																} else {
																	?>
																	<option
																			value="<?php echo $aps->stepId; ?>"><?php echo $aps->summary; ?></option>
																	<?php
																}
															}
														}
													}
													?>
												</select>
											</td>
										</tr>
										<?php
										}
										?>
						</table>
					</div>
					<div class="clear spacer"></div>
					<input value="Add" type="Submit" class="fieldbtn frmbtn floatRight" name="btnProfileSubmit"
						   id="btnProfileSubmit">
					<input value="Back" type="button" class="fieldbtn frmbtn floatLeft" name="btnProfileCancel"
						   id="btnProfileCancel" onclick="submitFrm('frmCreateProfile','backCustomQuestionRules')"/>
				</form>
				<div class="clear spacer"></div>
				<?php
			} // Add Custom Rules
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 8)))) {
				$currentStepId = 8;
				if (isset($_REQUEST['currentStepId'])) {
					if ($_REQUEST['currentStepId'] > $currentStepId) {
						$currentStepId = $_REQUEST['currentStepId'];
					} else {
						if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
							$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
						}
					}
				}
				$profileName = $profile->getProfileName($database, $_POST['txtPageId']);
				?><h1 class="gray">Preview Pages</h1>

			<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
				<form name="frmPreviewProfile" id="frmPreviewProfile" action="preview_profile.php" method="POST"
					  target="_blank">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="profileName" id="profileName" value="<?php echo $profileName; ?>"/>
					<input value="Preview Profile Page" type="Submit"
						   class="fieldbtn frmbtn largeButton floatLeft marginLeft_150" name="btnProfileSubmit"
						   id="btnProfileSubmit">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
				</form>

				<form name="frmPreviewThankyou" id="frmPreviewThankyou" action="preview_thankyou.php" method="POST"
					  target="_blank">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="profileName" id="profileName" value="<?php echo $profileName; ?>"/>
					<input value="Preview Thankyou Page" type="Submit"
						   class="fieldbtn frmbtn largeButton floatLeft btnSecond" name="btnProfileSubmit"
						   id="btnProfileSubmit">
					<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
				</form>
				<div class="spacer clear"></div>

				<form name="frmCreateProfile" id="frmCreateProfile" action="<?php echo $_SERVER['PHP_SELF']; ?>"
					  method="POST">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="9"/>
					<input type="hidden" name="txtProfileStepPrevious" id="txtProfileStepPrevious" value="8"/>
					<input type="hidden" name="profileName" id="profileName" value="<?php echo $profileName; ?>"/>
					<input type="hidden" class="textPageId" name="txtPageId" id="txtPageId"
						   value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="txtProfileTemplateId" class="textPageProfileTemplateId"
						   id="txtProfileTemplateId" value="<?php echo $_POST['txtProfileTemplateId']; ?>">
					<input value="Update" type="Submit" class="fieldbtn frmbtn floatRight" name="btnProfileSubmit"
						   id="btnProfileSubmit"/>
				</form>
				<?php
				$txtPageId = $_POST['txtPageId'];
				$txtProfileTemplateId = $_POST['txtProfileTemplateId'];
				?>
				<div class="clear spacer"></div>
			<?php
			} // Preview Profile
			elseif (((isset($_POST['txtProfileStep'])) && (($_POST['txtProfileStep'] == 9))))
			{
			$currentStepId = 9;
			if (isset($_REQUEST['currentStepId'])) {
				if ($_REQUEST['currentStepId'] > $currentStepId) {
					$currentStepId = $_REQUEST['currentStepId'];
				} else {
					if ((isset($currentStepId)) && (!empty($currentStepId)) && (isset($_POST['txtPageId'])) && (!empty($_POST['txtPageId']))) {
						$profile->UpdateCurrentStepId($database, $_POST['txtPageId'], $currentStepId);
					}
				}
			}

			$profileName = $profile->getProfileName($database, $_POST['txtPageId']);
			$profile->activateProfile($database, $_POST['txtPageId']);
			?>
			<input class="txtCreateProfile" type="hidden" name="changeCheck" id="changeCheck" value="0">
			<input type="hidden" name="currentStepId" id="currentStepId" value="<?php echo $currentStepId; ?>">
				<?php $vewProfile = true; ?>
				<form name="frmPreviewProfile" id="frmPreviewProfile" action="preview_profile.php" method="POST" target="_blank">
					<input type="hidden" name="txtError" id="txtError" value="<?php echo $_POST['txtError']; ?>"/>
					<input type="hidden" name="txtPageId" id="txtPageId" value="<?php echo $_POST['txtPageId']; ?>"/>
					<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="8"/>
				</form>
				<?php
			} // enable profile and redirect to profiles_view.php
			}
		}
		?>
	</div>
	<div class="clear spacer"></div>
</div>
<div class="clear"></div>

<?php include("../headerFooter/footer.php"); ?>
<script type="text/javascript" src="../../js/nicEdit.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.sortable.js"></script>
<?php if (($profileStep == 1) || ($profileStep == 4)): ?>
	<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function (e) {
//		bkLib.onDomLoaded(function () {
//			nicEditors.allTextAreas()
//		});
		<?php if(($profileStep == 1) || ($profileStep == 4)){?>
		$("#frmCreateProfile").validate({
			ignore: '.ignore',
			rules: {
				pageBrowserTitle: {
					required: true
				},
				txtDefaultText: {
					required: true
				},
				ddlProfileFontStyle: {
					required: true
				},

				ddlBgColor: {
					required: true
				},
				ddlFontColor: {
					required: true
				},
				ddlAccentColor: {
					required: true
				}
			},
			messages: {
				pageBrowserTitle: {
					required: "Please provide Page Title"
				},
				txtDefaultText: {
					required: "Please provide default text"
				},
				ddlProfileFontStyle: {
					required: "Please select text style"
				},
				txtHeaderImage: {
					required: "Please Select header Image"
				},
				ddlBgColor: {
					required: "Please provide Background Color"
				},
				ddlFontColor: {
					required: "Please provide Font Color"
				},
				ddlAccentColor: {
					required: "Please provide Accent Color"
				}
			}
		});
		<?php } else if($profileStep == 2) { ?>
		$(".sortableFields").sortable({
			items: '> tr:not(.notSortable)',
			update: function (event, ui) {
				var orderStart = $("#defaultQuestionsCount").val();
				var newOrder = $(this).sortable('toArray').toString();
				$.ajax({
					url: "../../classes/ajax.php",
					type: "post",
					data: {action: 'updatePageQuestiontFieldOrder', order: newOrder, orderStart: orderStart},
					success: function (result) {
						$("#msg").html('Field Order Updated').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
						$("#usercheck").val(result);
					}
				});
			}
		});
		<?php } ?>
		$('#ddlQuestionType').change(function (e) {
			var questionType = $('option:selected', $(this)).text();
			if (questionType == 'checkboxes') {
				if ($("#txtCheckboxCountLast").val() > 0) {
					$('.checkbox').show();
				}
				$('.checkboxCount').show();
				$('.radiobtnCount').hide();
				$('.radiobtn').hide();
				$('.radio').hide();
			}
			else if (questionType == 'radiobuttons') {
				if ($("#txtRadioBtnCountLast").val() > 0) {
					$(".radio").show();
					$(".radiobtn").show();
				}
				$('.radiobtnCount').show();
				$('.checkboxCount').hide();
				$('.checkbox').hide();
			}
			else if (questionType == 'textbox') {
				$('.radiobtnCount').hide();
				$('.checkboxCount').hide();
				$('.checkbox').hide();
				$('.radiobtn').hide();
				$('.radio').hide();
			}
		});
		$('#txtCheckboxCount').blur(function (e) {
			if ($(this).val() > 0) {
				var template = $('#txtCheckboxTemplate').html();
				var lastCount = parseInt($('#txtCheckboxCountLast').val());
				var newValue = parseInt($(this).val());
				var thisValue = parseInt(0);
				var i, count = 0;
				if (lastCount == 0) {
					$('tr.checkbox td:nth-child(2)').html('');
				}
				if (newValue > lastCount) {
					thisValue = newValue - lastCount;
					for (i = 0; i < thisValue; i++) {
						$('tr.checkbox td:nth-child(2)').append(template);
					}
				}
				else if (newValue < lastCount) {
					thisValue = lastCount - newValue;
					$('tr.checkbox td input:text').each(function (index, element) {
						count++;
						if (count > newValue) {
							$(this).remove();
						}
					});
				}
				$('#txtCheckboxCountLast').val(newValue);
				$('.radiobtn').hide();
				$('.checkbox').show();
			}
			else {
				$('tr.checkbox td:nth-child(2)').html('<div class="displayBlock">&nbsp;</div>');
				$('#txtCheckboxCountLast').val('0');
				$('.checkbox').hide();
			}
		});
		$('#txtRadioBtnCount').blur(function (e) {
			if ($(this).val() > 0) {
				var template = $('#txtRadioBtnTemplate').html();
				var lastCount = parseInt($('#txtRadioBtnCountLast').val());
				var newValue = parseInt($(this).val());
				var thisValue = parseInt(0);
				var i, count = 0;
				if (lastCount == 0) {
					$('tr.radiobtn td:nth-child(2)').html('');
				}
				if (newValue > lastCount) {
					thisValue = newValue - lastCount;
					for (i = 0; i < thisValue; i++) {
						$('tr.radiobtn td:nth-child(2)').append(template);
					}
				}
				else if (newValue < lastCount) {
					thisValue = lastCount - newValue;
					$('tr.radiobtn td input:text').each(function (index, element) {
						count++;
						if (count > newValue) {
							$(this).remove();
						}
					});
				}
				$('#txtRadioBtnCountLast').val(newValue);
				$('.checkbox').hide();
				$('.radiobtn').show();
				$('.radio').show();
			}
			else {
				$('tr.radiobtn td:nth-child(2)').html('<div class="displayBlock">&nbsp;</div>');
				$('#txtRadioBtnCountLast').val('0');
				$('.radiobtn').hide();
				$('.radio').hide();
			}
		});
		$('#backgroundPopupProfile').click(function () {
			$(this).hide();
			$("#popupHeaderImage").hide();
			$("#popupProfilColumnOptions").hide();
		});
	});
	function cancelNewQuestion() {
		$("#tblProfileMenu tr").each(function () {
			$(this).removeClass("settingsSelected");
		});
		$("#tblProfileMenu td").filter(function () {
			return $.text([this]) == 'Profile Question';
		}).parent().addClass("settingsSelected");
		$("#questionsList").show();
		$("#newQuestion").hide();
	}
	function addQuestion() {
		$("#tblProfileMenu tr").each(function () {
			$(this).removeClass("settingsSelected");
		});
		$("#tblProfileMenu td").filter(function () {
			return $.text([this]) == 'Add/Edit Profile Question';
		}).parent().addClass("settingsSelected");
		$("#questionsList").hide();
		$("#newQuestion").show();
	}
	function getTemplateDefault() {
		$.ajax({
			url: "../../classes/ajax.php",
			type: "post",
			data: {action: 'getDefaultTemplateValues', id: $("#txtProfileTemplateId").val()},
			success: function (result) {
				console.log(result);
			}
		});
	}
	function setTemplate(templateId) {
		$("#txtProfileTemplateId").val(templateId);
		$("#frmCreateProfile").submit();
	}
	function toggleStatus(id, status, legend, pageId) {
		if (typeof(pageId) === 'undefined') {
			pageId = 0;
		}
		var thisVal = $("#status" + id).html().trim();
		var newVal = (thisVal == 'Inactive' ? 'Active' : 'Inactive').trim();
		$("#status" + id).html('<img src="../../images/loading.gif" width="28" height="28">');
		$.ajax({
			url: "../../classes/ajax.php",
			type: "post",
			data: {action: legend, id: id, pageId: pageId},
			success: function (result) {
				$("#msg").html('Field is updated').css('color', '#20BF04').show('slow').animate({top: 0}, 5000).fadeOut();
				$("#status" + id).html(newVal);
			}
		});
	}
	function submitFrm(frmId, legend) {
		if (legend == 'jumpStep') {
			$("#txtProfileStep").val(frmId);
			$("#currentStepId").val($("#currentStepId").val());
			if ($("#changeCheck").val() == 0) {
				$("#frmCreateProfile").submit();
			}
			else if (confirm('One or more fields have been updated, are you sure you want to discard these?')) {
				$("#frmCreateProfile").submit();
			}
		}
		else if (legend == 'next') {
			$("#txtProfileStep").val(4);
			$("#" + frmId).submit();
		}
		else if (legend == 'back') {
			$("#txtProfileStep").val(1);
			$("#" + frmId).submit();
		}
		else if (legend == 'backTemplate') {
			$("#txtHeaderImage").addClass("ignore");
			$("#txtProfileStep").val(0);
			$("#" + frmId).submit();
		}
		else if (legend == 'backQuestions') {
			$("#txtHeaderImage").addClass("ignore");
			$("#txtProfileStep").val(2);
			$("#" + frmId).submit();
		}
		else if (legend == 'backThankyou') {
			$("#txtProfileStep").val(4);
			$("#" + frmId).submit();
		}
		else if (legend == 'backAssignGroupsandCampaigns') {
			$("#txtProfileStep").val(5);
			$("#" + frmId).submit();
		}
		else if (legend == 'nextAddCustomQuestionRule') {
			$("#txtProfileStep").val(7);
			$("#" + frmId).submit();
		}
		else if (legend == 'backCustomQuestionRules') {
			$("#txtProfileStep").val(6);
			$("#txtEditCustomRuleQuestion").val(0);
			$("#" + frmId).submit();
		}
		else if (legend == 'backViewCustomRules') {
			$("#txtProfileStep").val(6);
			$("#" + frmId).submit();
		}
		else if (legend == 'backNoEditQuestion') {
			$("#txtQuestionId").val(-1);
			$("#" + frmId).submit();
		}
		else if (legend == 'showWarning') {
			if ($("#txtShowWarning").val() == 1) {
				if (confirm("There are few Custom Rules associated with this question, updating these options will erase them?")) {
					$("#" + frmId).submit();
				}
			}
			else {
				$("#" + frmId).submit();
			}
		}

	}
	function editCustomQuestionRule(frmId, customRuleId, choices, ruleId) {
		$("#txtProfileStep").val(7);
		$("#txtEditCustomRuleQuestion").val(customRuleId + '-' + choices + '-' + ruleId);
		$("#" + frmId).submit();
	}
	function editQuestion(questionId, frmId) {
		$("#txtQuestionId").val(questionId);
		$("#txtProfileStep").val(-1);
		$("#" + frmId).submit();
	}
	function showPlan(planId) {
		if ($("#chkPlan_" + planId).is(":checked")) {
			$("#trPlanSteps_" + planId).css("background-color", "#007db1");
			$("#ddlPlanSteps_" + planId).show();
		}
		else {
			$("#trPlanSteps_" + planId).css("background-color", "#ffffff");
			$("#ddlPlanSteps_" + planId).hide();
		}
	}
	function showQuestionResponse(questionId) {
		$("[id*='questionResponses_']").hide();
		$("#questionResponses_" + questionId).show();
		$("#questionResponses").show();
	}
	function showColumn(columnCount) {
		$(".profileColumns").hide();
		$("#" + columnCount + 'column').show();
	}
	function showProfilesColumnOptions() {
		$("#backgroundPopupProfile").show();
		$("#popupHeaderImage").hide();
		$("#popupProfilColumnOptions").show();
	}
	function showHeaderImage(imageName) {
		$("#headerImage").attr("src", "../../images/profile/" + imageName);
		$("#backgroundPopupProfile").show();
		$("#popupHeaderImage").show();
		$("#popupProfilColumnOptions").hide();
	}
	function hideHeaderImage() {
		$("#headerImage").attr("src", "");
		$("#backgroundPopupProfile").hide();
		$("#popupHeaderImage").hide();
	}
	function vaidateCustomChoice(frmId) {
		var isSubmit = true;

		if ($("#txtFieldQuestion").val().length == 0) {
			$("#txtFieldQuestionWarning").show();
			$("#txtFieldQuestion").focus();
			return false;
		}
		else {
			$("#txtFieldQuestionWarning").hide();
		}
		if ($("#txtFieldQuestionLabel").val().length == 0) {
			$("#txtFieldQuestionLabelWarning").show();
			$("#txtFieldQuestionLabel").focus();
			return false;
		}
		else {
			$("#txtFieldQuestionLabelWarning").hide();
		}
		if ($("#ddlQuestionType").val() == 0) {
			$("#ddlQuestionTypeWarning").show();
			$("#ddlQuestionType").focus();
			return false;
		}
		else {
			$("#ddlQuestionTypeWarning").hide();
		}

		if ($("#ddlQuestionType").val() == 3)//checkbox
		{
			if (($("#txtCheckboxCount").val().length == 0) || ($("#txtCheckboxCount").val() == 0)) {
				$("#txtCheckboxCountWarning").show();
				$("#txtCheckboxCount").focus();
				return false;
			}
			else {
				$("#txtCheckboxCountWarning").hide();
				$("form#" + frmId + " .txtCreateProfileCheckbox").each(function () {
					if ($(this).val().length == 0) {
						$(this).nextAll(".warningProfile").remove();
						$(this).after($("#warningProfileDiv").html());
						$(this).next(".warningProfile").show();
						$(this).focus();
						isSubmit = false;
						return false;
					}
					else {
						$(this).next(".warningProfile").remove();
					}
				});
			}
		}
		else if ($("#ddlQuestionType").val() == 4 || $("#ddlQuestionType").val() == 5)//radiobutton
		{
			if (($("#txtRadioBtnCount").val().length == 0) || ($("#txtRadioBtnCount").val() == 0)) {
				$("#txtRadioBtnCountWarning").show();
				$("#txtRadioBtnCount").focus();
				return false;
			}
			else {
				$("#txtRadioBtnCountWarning").hide();
				$("form#" + frmId + " .txtCreateProfileRadioBtn").each(function () {

					if ($(this).val().length == 0) {
						$(this).nextAll(".warningProfile").remove();
						$(this).after($("#warningProfileDiv").html());
						$(this).next(".warningProfile").show();
						isSubmit = false;
						return false;
					}
					else {
						$(this).next(".warningProfile").remove();
					}
				});
			}
		}

		if (!($("input:radio[name=rdMandatory]").is(":checked"))) {
			$("#rdMandatoryWarning").show();
			return false;
		}
		else {
			$("#rdMandatoryWarning").hide();
		}

		if (isSubmit) {
			return true;
		}
		else {
			return false;
		}
	}
	function updateInput() {
		$("#changeCheck").val('1');
	}





</script>

<!--Namoos Code-->

<script>
	$('#txtNewCode').blur(function () {
		$("#msgProfile").html('');
		$("#uniqueProfileShortName").val(0);
		$('#btnProfileSubmit').prop('disabled', true);
		var profileName = $(this).val();
		var profileId = <?php echo $_POST['txtPageId']; ?>;
		if (!/^[a-zA-Z0-9]+$/.test(profileName)) {
			$("#msgProfile").html('Only Alphanumeric characters are allowed.').css('color', 'red');
			$("#uniqueProfileShortName").val(0);
			return false;
		}
		else if (profileName.trim() == "support") {
			$("#msgProfile").html('Support cannot be set as Profile Name.').css('color', 'red');
			$("#uniqueProfileShortName").val(0);
			return false;
		}
		if (profileName == "" || profileName.length < 2) {
			$("#msgProfile").html('Profile name should be greater the 2 characters.').css('color', 'red');
			$("#uniqueProfileShortName").val(0);
		}
		else {
			$.ajax({
				url: "../../classes/ajax.php",
				type: "post",
				data: {action: 'checkProfileNameAvailability', profileName: profileName, userId: '<?php echo $userId; ?>', profileId: profileId },
				success: function (result) {
					if (result == 0) {
						$("#msgProfile").html('This Profile name is Unavailable. Please try another one.').css('color', 'red');
						$("#uniqueProfileShortName").val(0);
						$('#btnProfileSubmit').prop('disabled', true);
					}
					else {
						$("#msgProfile").html('Profile Name is Available').css('color', '#20BF04');
						$("#uniqueProfileShortName").val(1);
						$('#btnProfileSubmit').prop('disabled', false);
					}
				}
			});
		}
	});

	$('#btnProfileSubmit').on('click', function() {
		if ($('#uniqueProfileShortName').val() == 0) {
			return false;
		}
		return true;
	});
</script>

<script type="text/javascript">
	<?php if($vewProfile == true): ?>
	var changeId = <?php echo $_POST['txtPageId']; ?>;
	var profileName = '<?php echo $profileName; ?>';
	$.ajax({
		url: "../../classes/ajax.php",
		type: "post",
		data: {action: 'editProfileNotification', changeId: changeId, profileName: profileName},
		success: function (result) {
			document.location.href = "profiles_view.php";
			if (result) {
				return false;
			}
			else {
				return false;
			}
		}
	});
	<?php endif; ?>
</script>
<script src="../../lib/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'txtDefaultText' );
    </script>
<style>
    .txtCreateProfile {
        width: 97%;
        height: 21px;
        border-radius: 2px;
        border: 1px solid #93A4A5;
        box-shadow: 0.5px 0.5px #93A4A5;
        background: rgba(147, 164, 165, 0.04);
        padding: 5px;
    }
</style>
</div>
