<?php
	session_start();
	include_once("../../classes/init.php");
	include_once("../headerFooter/header.php");
	include_once("../../classes/profiles.php");
	require_once("../../classes/resource.php");

	$database = new database();
	$profile = new Profiles();
	$userProfiles = $profile->getAllProfiles($database, $_SESSION['userId']);
?>

<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sort/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.sortable.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('#txtProfileShortName').blur(function () {
		$("#msgProfile").html('');
		$("#loadingImg").css("display", "block");
		$("#profileCheck").val(0);
		var profileName = $(this).val();
		if (!/^[a-zA-Z0-9]+$/.test(profileName)) {
			$("#msgProfile").html('Only Alphanumeric characters are allowed.').css('color', 'red');
			$("#profileCheck").val(0);
			$("#loadingImg").hide();
			return false;
		}
		else if (profileName.trim() == "support") {
			$("#msgProfile").html('Support cannot be set as Profile Name.').css('color', 'red');
			$("#profileCheck").val(0);
			$("#loadingImg").hide();
			return false;
		}
		if (profileName == "" || profileName.length < 2) {
			$("#msgProfile").html('Profile name should be greater the 2 characters.').css('color', 'red');
			$("#profileCheck").val(0);
			$("#loadingImg").hide();
		}
		else {
			$.ajax({
				url: "../../classes/ajax.php",
				type: "post",
				data: {action: 'checkProfileNameAvailability', profileName: profileName, userId: '<?php echo $userId; ?>'},
				success: function (result) {
					if (result == 0) {
						$("#msgProfile").html('This Profile name is Unavailable. Please try another one.').css('color', 'red');
						$("#profileCheck").val(result);
					}
					else {
						$("#msgProfile").html('Profile Name is Available').css('color', '#20BF04');
						$("#profileCheck").val(result);
					}
					$("#loadingImg").hide();
				}
			});
		}
	});
	$('.edit_link').click(function () {
		var name = $("#userName");
	});
	$("#backgroundPopup").click(function () {
		disablePopupEditLink();
	});
	$("#backgroundPopupProfile").click(function () {
		hidePopup();
	});
	$('.cancel_2').click(function () {
		disablePopupEditLink();
	});
	$('#btnProfileCancel').click(function (e) {
		hidePopup();
	});
    $('#cancleVideoEditpopUp').click(function (e) {

        $('#backgroundPopupVideoEdit').hide();
        $('#popupVideoSurvey').hide();


    });
	$('.delete_profile').click(function () {
		alert('Are you sure to delete this profile');
	});


    $('#EditVideoSubmit').click(function () {

        var editedVideo = $('#videoEdit').val();
        var pageIdforvideoEdit= $('#pageIdforvideoEdit').val();

        var lengthOfField = $('#videoEdit').length;

if (!$('#videoEdit').val()){

    $("#videoEdit").addClass("borderred");

}else
{
    $("#videoEdit").removeClass("borderred");
        $.ajax({
            url: "../../classes/ajax.php",
            type: "post",
            data: {action: 'editVideoForVENVPsTemplate', editedVideo: editedVideo,pageId:pageIdforvideoEdit},
            success: function (result) {

                $('#backgroundPopupVideoEdit').hide();
                $('#popupVideoSurvey').hide();
                $("#msg").html('Video has been edited successfully').css('color', '#20BF04').show('slow').delay(5000)
                    .fadeOut('slow');
                $('#videoEdit').val('');
                var editedVideo = null;


                }
            });
}

    });

	$('.sortableFields ').sortable({
		update: function (event, ui) {
			var newOrder = $(this).sortable('toArray').toString();
			$.ajax({
				url: "../../classes/ajax.php",
				type: "post",
				data: {action: 'updateProfileOrder', id: newOrder},
				success: function (result) {
					$("#msg").html('Page Ordered SuccessFully').css('color', '#20BF04').show('slow').delay(5000).fadeOut('slow');
					$("#usercheck").val(result);
				}
			});
		}
	});
});

function hidePopup() {
	$('#backgroundPopupProfile').hide();
	$('#popupSurvey').hide();
}
function hidePopupEditVideo() {
    alert('sdfds');
    $('#backgroundPopupVideoEdit').hide();
    $('#popupVideoSurvey').hide();
}
function get(id) {
	var linkname = '#link' + id;
	linkcode = $(linkname).attr("href");
	var code = linkcode.split('/');
	var pagecode = code[code.length - 1];
	var username = code[code.length - 2];
	//$('#profileLink').val(pagecode);
	//document.getElementById('profileLink').value=pagecode;
	//$('#pageId').val(id);
	$.ajax({ url: 'get_edit_link.php',
		data: {pageId: id, code: pagecode, user: username},
		type: 'post',
		success: function (output) {
			if (output != '') {
				$('#popupEditLink').html(output);
				loadPopupEditLink();
				centerPopupEditLink();
			}
			else {
				alert("Something went wrong");
			}
			// disablePopupTransfer();
		}
	});
}
function deleteProfile(id) {
	var answer = confirm("Are you sure to delete this profile?")
	if (answer) {
		$.ajax({ url: 'delete_link.php',
			data: {pageId: id},
			type: 'post',
			success: function (output) {
				if (output != '') {
					location.reload();
				}
				else {
					alert("Something went wrong");
				}
				// disablePopupTransfer();
			}
		});
	}
}
function updateProfileLink() {
	var id = $('#pageId').val();
	var code = $('#profileLink').val();
	if (code == '' || code == null) {
		var flag = false;
	}
	if (flag != false) {
		if (code.indexOf("/") == -1 && code.indexOf(".") == -1 && code.indexOf("\\") == -1 && code.indexOf(" ") == -1 && code.indexOf("'") == -1) {
			$.ajax({ url: 'edit_link.php',
				data: {pageId: id, linkcode: code},
				type: 'post',
				success: function (output) {
					var message = output;
					if (message == 0 || message == '0') {
						$('#error1').html('That address is already taken');
					}
					else {
						var linkname = '#link' + id;
						$(linkname).text(message);
						$(linkname).attr("href", message);
						disablePopupEditLink();
						window.location.reload();
					}
				}
			});
		}
		else {
			$('#error1').html('you can not use "." , "/" and "\\" , apostrophes and spaces in link');
		}
	}
	else {
		$('#error1').html('Please enter some code');
		return false;
	}
}
function copyToClipboard(text) {
	window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
}
function resetCounters(id) {
	$.ajax({ url: 'reset_counters.php',
		data: {pageId: id},
		type: 'post',
		success: function (output) {
			$('#viewsField' + id).text('0');
			$('#generatedLeadsField' + id).text('0');
		}
	});
}
function codeview(url) {
	window.open("code_view.php?url=" + url);
}
function addNewProfile() {
	$('#backgroundPopupProfile').show();
	$('#popupSurvey').show();
	$("#txtProfileName").focus();
}
function editVideoForDefaultTemplate(pageId,alreadyExitVideo) {
    $('#backgroundPopupVideoEdit').show();
    $('#popupVideoSurvey').show();
    $('#pageIdforvideoEdit').val(pageId);

    var editedVideo= $('#existingEditedVideo').val();

    $('#videoEdit').val(alreadyExitVideo);


}
function checkProfile() {
	if ($("#profileCheck").val() == 0) {
		return false;
	}
	else {
		$("#frmNewProfilePopup").submit();
	}
}
function archiveProfile(profileId, pageName) {
	$("#survey_box_" + profileId + " .profile_buttons").append('<img class="loadingProfile" src="../../images/loading.gif" width="28" height="28">');
	$.ajax({
		url: "../../classes/ajax.php",
		type: "post",
		data: {action: 'archiveProfile', profileId: profileId, pageName: pageName},
		success: function (result) {
			$("#survey_box_" + profileId).fadeTo("slow", 0.00, function () { //fade
				$(this).slideUp("slow", function () { //slide up
					$(this).remove(); //then remove from the DOM
				});
			});
		}
	});
	//console.log($(this).attr('clase'));
}
function discardProfile(profileId, pageName) {
	$("#survey_box_" + profileId + " .profile_buttons").append('<img class="loadingProfile" src="../../images/loading.gif" width="28" height="28">');
	$.ajax({
		url: "../../classes/ajax.php",
		type: "post",
		data: {action: 'discardProfile', profileId: profileId, pageName: pageName},
		success: function (result) {
			$("#survey_box_" + profileId).fadeTo("slow", 0.00, function () { //fade
				$(this).slideUp("slow", function () { //slide up
					$(this).remove(); //then remove from the DOM
				});
			});
		}
	});
	//console.log($(this).attr('clase'));
}
function submitFrm(frmId, pageId, currentStepId) {
	$("#txtPageId").val(pageId);
	$("#currentStepId").val(currentStepId);
	$("#frmEditProfile #txtProfileStep").val(currentStepId);
	$("#" + frmId).submit();
}
/*my code*/
function publishProfile(profileId, newCode) {
	$("#survey_box_" + profileId + " .profile_buttons").append('<img class="loadingProfile" src="../../images/loading.gif" width="28" height="28">');
	$.ajax({
		url: "../../classes/ajax.php",
		type: "post",
		data: {action: 'publishProfile', profileId: profileId, newCode: newCode},
		success: function (result) {
			$("#publishProfile" + profileId).hide();
			$(".loadingProfile").hide();
		}
	});

}
</script>
<div class="container">
	<div class="top_content2">
		<h1 class="gray">Profiles</h1>
		<span id="clickNewField" class="fieldbtn" onclick="addNewProfile()">New Profile</span>
	</div>
	<div id="backgroundPopupProfile"></div>
	<div id="msg"></div>
	<div id="popupSurvey">
		<h1>Create New Profile</h1>
		<input type="hidden" name="profilecheck" id="profileCheck" value="0">

		<form name="frmNewProfilePopup" id="frmNewProfilePopup" action="profile_create.php" method="POST">
			<input type="hidden" class="textfield" name="txtProfileStep" id="txtProfileStep" value="0"/>
			<label class="survey_label">Name</label>
			<input type="text" class="textfield" name="txtProfileName" id="txtProfileName"/>
			<label class="survey_label">Profile Short Name</label>
			<input type="text" class="textfield" name="txtProfileShortName" id="txtProfileShortName"/>

			<div id="msgDiv">
				<span id="msgProfile">&nbsp;</span>
				<img id="loadingImg" class="none" src="../../images/loading.gif" width="20" height="20">
			</div>
			<div class="clear"></div>
			<input value="Next" type="button" onclick="return checkProfile()" class="fieldbtn frmbtn frmbtnNewProfile" name="btnProfileSubmit" id="btnProfileSubmit"/>
			<input value="Cancel" type="button" class="fieldbtn frmbtn" name="btnProfileCancel" id="btnProfileCancel"/>
		</form>
	</div>
    <!------------------------------ popup for VENVPS video edit option----------------------->

    <div id="backgroundPopupVideoEdit"></div>
    <div id="msg"></div>
    <div id="popupVideoSurvey">
        <h1>Edit Video</h1>


        <form name="frmVideoPopup" id="frmVideoPopup" action="" method="POST">

            <label class="survey_label">Video code</label>
            <input type="text" placeholder="iz0N7v652K0" class="textfield" name="videoEdit" id="videoEdit" value=""/>

            <p style="color:#535C68; font-size: 10px; padding-left: 16px; margin-top: 0;white-space: nowrap; ">e.g. https://www.youtube.com/watch?v=(code)
            </p>
            <p style="display: none;  font-size: 10px; padding-left: 16px; margin-top: 0;white-space: nowrap; " id="errormsg">e.g. https://www.youtube.com/watch?v=(code)
            </p>

            <div id="msgDiv">
                <span id="msgProfile">&nbsp;</span>
                <img id="loadingImg" class="none" src="../../images/loading.gif" width="20" height="20">
            </div>
            <div class="clear"></div>
            <input value="Update" type="button" class="fieldbtn frmbtn frmbtnNewProfile" name="EditVideoSubmit"  id="EditVideoSubmit"/>
            <input value="" type="hidden" class="fieldbtn frmbtn frmbtnNewProfile" name="pageIdforvideoEdit"
                   id="pageIdforvideoEdit"/>
            <input value="Cancel" type="button" class="fieldbtn frmbtn" name="cancleVideoEditpopUp"
                   id="cancleVideoEditpopUp"/>
        </form>
    </div>

    <!------------------------------ end popup for VENVPS video edit option----------------------->

	<div id="backgroundPopup"></div>
	<div id="popupEditLink"></div>
	<div class="sub_container">
		<table width="100%">
			<tbody class="sortableFields">
			<?php foreach ($userProfiles as $profiles) {
				?>
			<tr id="pId<?php echo $profiles->pageId; ?>">
				<td><?php
				$views = $profile->getPageViews($database, $profiles->pageId);
				$contacts = $profile->getPageGeneratedContacts($database, $profiles->pageId);
				echo '<div class="survey_box " id="survey_box_' . $profiles->pageId . '">';

				if (($profiles->generatedVia == 2) || ($profiles->generatedVia == 3)) {
					echo '<img src="../../images/template/' . $profiles->thumbnail . '" class="survey_img" />';
				}
				else {
					echo '<img src="../../images/' . $profiles->thumbnail . '" class="survey_img" />';
				}
				echo '<div class="survey_desc">
			   <h3>' . $profiles->pageBrowserTitle . ':</h3>
			   <br />
      		   <h4>Since ' . date('M d, Y', strtotime($profiles->addDate)) . '</h4>
			    <br />';
				if (($profiles->isComplete != 0)) {
					echo '<a href="' . $CONF["siteURL"] . $profiles->username . '/' . $profiles->newCode . '" target="_blank" id="link' . $profiles->pageId . '">' . $CONF["siteURL"] . $profiles->username . '/' . $profiles->newCode . '</a>';

					echo '<table style="margin-left:15px;">
				<tbody><tr>
					<td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					<td><span>Automatic</span></td>
					<td width="150" align="right" rowspan="4"><a href="javascript:resetCounters(' . $profiles->pageId . ');">Reset Counters</a></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label id="views"># of Views:</label></td>
					<td><span id="viewsField' . $profiles->pageId . '">' . $views . '</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label id="contacts">Contacts Generated:</label></td>
					<td><span id="generatedLeadsField' . $profiles->pageId . '">' . $contacts . '</span></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					<td>0</td>
				</tr>
			</tbody></table>
			</div>
      <div class="profile_buttons">
		<input type="button" class="copy_code verticalAlignBottom" name="copy_code" onclick="codeview(\'' . $CONF["siteURL"] . $profiles->username . '/' . $profiles->newCode . '\');" />';

					if ((($signedIn == 'user') && ($profiles->generatedVia == 3)) ||
						(($signedIn == 'leader') && (($profiles->generatedVia == 2) || ($profiles->generatedVia == 1)))
					) {
						echo '<input type="button" class="edit_profile verticalAlignBottom" name="edit_profile" onclick="submitFrm(\'frmEditProfile\',\'' . $profiles->pageId . '\',0);" />';

//				echo '<input type="button" class="fieldbtn frmbtn" value="Edit Profile" name="edit_profile" onclick="submitFrm(\'frmEditProfile\',\'' . $profiles->pageId . '\');" />';
					}
					echo '<input type="button" class="archives verticalAlignBottom" name="archive_profile" onclick="archiveProfile(' . $profiles->pageId . ',\'' . $profiles->newCode . '\');" />';

//			echo '<input type="button" class="fieldbtn frmbtn" value="Archive" name="archive_profile" onclick="archiveProfile(' . $profiles->pageId . ',\'' . $profiles->newCode . '\');" />';


                    if (($profiles->generatedVia == 0 || $profiles->generatedVia == 2) && ($_SESSION['leaderId'] !=$_SESSION['userId'] ||$_SESSION['leaderId']==236)){
                        echo '<input type="button"  id="editVideo' . $profiles->pageId . '"   class="editDefaultVideo
                         verticalAlignBottom" name="editVideo"
                        onclick="editVideoForDefaultTemplate(' . $profiles->pageId .
                            ',\''.$profiles->profileEditedVideo.'\');" value="" />

                            ';
                    }
                    if (($signedIn == 'leader') && ($profiles->privacy == 0)) {
						echo '<input type="button" id="publishProfile' . $profiles->pageId . '" class="publish_profile verticalAlignBottom" name="publish_profile" onclick="publishProfile(' . $profiles->pageId . ',\'' . $profiles->newCode . '\');" />';

//				echo '<input type="button" class="publish_profile frmbtn" id="publishProfile' . $profiles->pageId . '" value="Publish Profiles" name="publish_profile" onclick="publishProfile(' . $profiles->pageId . ',\'' . $profiles->newCode . '\');" />';
					}
					/*}*/


//			if (($profiles->generatedVia == 2) || ($profiles->generatedVia == 4))
//			{
//				echo '<input type="button" class="fieldbtn frmbtn" value="Edit Profile" name="edit_profile" onclick="submitFrm(\'frmEditProfile\',\'' . $profiles->pageId . '\');" />';
//				echo '<input type="button" class="fieldbtn frmbtn" value="Archive" name="archive_profile" onclick="archiveProfile(\'' . $profiles->pageId . '\',\'' . $profiles->newCode . '\');" />';
//				if ($profiles->privacy == '0')
//				{
//					echo '<input type="button" class="fieldbtn frmbtn" id="publishProfile' . $profiles->pageId . '" value="Publish Plan" name="publish_profile" onclick="publishProfile(\'' . $profiles->pageId . '\',\'' . $profiles->newCode . '\');" />';
//				}
//
//			}
					echo ' <input type="button" class="copy_link" onclick="copyToClipboard(\'' . $CONF["siteURL"] . $profiles->username . '/' . $profiles->newCode . '\')" />';
					//<input type="button" class="delete_profile"  onclick="deleteProfile('.$profiles->pageId.');"/>';
					//echo '<input type="button" class="edit_link" name="edit_link"  onclick="get('.$profiles->pageId.');" />';
					//<input type="button" class="edit_link" name="edit_link"  onclick="get('.$profiles->pageId.');" />';
					echo '  </div>
      </div>'; ?>
					</tr></td>
				<?php
				}
				else {
					echo '</br></br></br></br></br>' . '<input type="button" class="complete_profile verticalAlignBottom" name="edit_profile" onclick="submitFrm(\'frmEditProfile\',\'' . $profiles->pageId . '\',\'' . $profiles->currentStepId . '\');" />
				    <input type="button" class="discard verticalAlignBottom" name="archive_profile" onclick="discardProfile(' . $profiles->pageId . ',\'' . $profiles->newCode . '\');" />';


				}
			} ?>
			</tbody>
		</table>

		<!--<div class="survey_box">
		<img src="../../images/Survey 2.JPG" class="survey_img" />
		<div class="survey_desc">
		<h3>Guest Profile:</h3>
		<br />
		<h4>Since January 07, 2013</h4>
		<br />
		<a href="javascrit:void(0)">www.myprofileone.com</a>
		<table style="margin-left:15px;">
				  <tbody><tr>
					  <td align="right" style="padding-right:5px;"><label>Lead Approval Type:</label></td>
					  <td><span>Manual</span></td>
					  <td width="150" align="right" rowspan="4"><a onclick="confirmDialog(partial(leadcapturelistinstance.resetCounters,'0746165246d705afd5796694ccd963648d8f288f'), 'Are you sure you want to reset the counters for this widget? The data accumulated will be from this date forward.', 'Confirm Reset', '!undo', event);" href="javascript:void(0);">Reset Counters</a></td>
				  </tr>
				  <tr>
					  <td align="right" style="padding-right:5px;"><label># of Views:</label></td>
					  <td><span id="0746165246d705afd5796694ccd963648d8f288f_viewsField">0</span></td>
				  </tr>
				  <tr>
					  <td align="right" style="padding-right:5px;"><label>Leads Generated:</label></td>
					  <td><span id="0746165246d705afd5796694ccd963648d8f288f_generatedLeadsField">0</span></td>
				  </tr>
				  <tr>
					  <td align="right" style="padding-right:5px;"><label>Leads to Approve:</label></td>
					  <td>0</td>
				  </tr>
			  </tbody></table>
		</div>
		<div class="profile_buttons">
	   <!--<input type="button" class="edit_profile" />-->
		<!--<input type="button" class="edit_link" />
	 <input type="button" class="copy_link" /><input type="button" class="delete_profile" />
	 </div>
	  </div>

	 </div>--->
		<form name="frmEditProfile" id="frmEditProfile" method="post" action="profile_edit.php">
			<input type="hidden" name="txtPageId" id="txtPageId">
			<input type="hidden" name="txtProfileStep" id="txtProfileStep" value="0">
			<input type="hidden" name="currentStepId" id="currentStepId" value="">
		<form>
	</div>
	<?php include_once("../headerFooter/footer.php"); ?>
   