<?php
	include_once('../../classes/init.php');
	include_once('../../classes/profiles.php');

	$database = new database();
	$profile = new Profiles();
	$details = $profile->getUserPageDetails($database, $_REQUEST['code'], $_REQUEST['user']);
	$code = $details->newCode;
	$url = $profile->curPageURL();
	$viewId = $profile->PageView($database, $details->pageId, $url, $code, $details->userId, $_SERVER['HTTP_USER_AGENT']);
	$quest = $profile->getPageQuestions($database, $details->pageId, '-1', 'old');
?>
<head>
	<base href="<?php echo $CONF['siteURL']; ?>modules/profiles/"/>
</head>
<?php echo $details->templateHTML; ?>
<div class="grid_6">
	<div class="texttitle">
		<h1>
			Yum!!
		</h1>
	</div>
	<div class="pagetext">
		<p>
			Hooray for fall!
		</p>
		<p>
			Submit your info below and confirm your email to receive my 7 favorite nutrient packed smoothie recipes. Thanks for connecting!
		</p>

		<p class="right" style="margin-right:40px;">
			Best regards,<br/>
			<?php echo $details->firstName; ?></p>
	</div>
</div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
	&nbsp;</div>
<div class="grid_6 signature">
	<?php echo $details->firstName . ' ' . $details->lastName;
		if ($details->title != '')
			echo ', ' . $details->title;  ?><br/>
	Arbonne Consultant <?php echo $details->consultantId; ?><br/>
	<a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->webAddress, "https://") != false)
		echo $details->webAddress;
	else echo 'http://' . $details->webAddress; ?>"
	   target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->webAddress); ?></a></div>
<div class="grid_10 signature" style="text-align:right;">
	<a href="<?php if (strpos($details->webAddress, "http://") != false || strpos($details->facebookAddress, "https://") != false)
		echo $details->facebookAddress;
	else echo 'http://' . $details->facebookAddress; ?>"
	   target="_blank"><?php echo $web = preg_replace('#^https?://#', '', $details->facebookAddress); ?></a><br/>
	<a href="mailto:<?php echo $details->email; ?>"><?php echo $details->email; ?></a><br/>
	<?php echo $details->phoneNumber; ?></div>
<div class="greenbg" style="height:1px; float:left; width:100%;">
	&nbsp;</div>
<div class="grid_16 texttitle">
	<h1>
		Request your Smoothie Recipes!
	</h1>
</div>
<form action="<?php echo $CONF["siteURL"]; ?>modules/profiles/profileAction.php" id="questionform" method="post">
<input type="hidden" value="<?php echo $details->pageId; ?>" name="pageId"/>
<input type="hidden" value="<?php echo $details->userId; ?>" name="userId"/>
<input type="hidden" value="<?php echo $details->pageName; ?>" name="pageName"/>
<input type="hidden" value="<?php echo $code; ?>" name="code"/>
<input type="hidden" value="<?php echo $url; ?>" name="pageUrl"/>
<input type="hidden" value="<?php echo $viewId; ?>" name="viewId"/>

<div class="equalize">
<div class="grid_6 questionbox" style="width: 370px;">
<div class="boxtitle">
	Contact Information:
</div>
<div style="padding:0 10px 0 10px;">
	<div class="errorMessage">
		&nbsp;</div>
	<table border="0" cellpadding="0" width="100%">
		<tbody>
		<tr>
			<td align="right" width="22%">
				Name:<span class="requiredstar">*</span></td>
			<td width="39%">
				<input class="required" name="firstName" type="text"/></td>
			<td width="39%">
				<input class="required" name="lastName" type="text"/></td>
		</tr>
		<tr>
			<td align="right">
				Email:<span class="requiredstar">*</span></td>
			<td colspan="2">
				<input class="required email" name="email" type="text"/></td>
		</tr>
<!--		<tr>-->
<!--			<td align="right">-->
<!--				Phone:<span class="requiredstar">*</span></td>-->
<!--			<td colspan="2">-->
<!--				<input class="required" name="phone" type="text"/></td>-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<td align="right">-->
<!--				Birthday:<span class="requiredstar">*</span></td>-->
<!--			<td colspan="2">-->
<!--				<select class="required" id="birthday_month" name="birthday_month">-->
<!--					<option value="">Month</option>-->
<!--					<option value="01">Jan</option>-->
<!--					<option value="02">Feb</option>-->
<!--					<option value="03">Mar</option>-->
<!--					<option value="04">Apr</option>-->
<!--					<option value="05">May</option>-->
<!--					<option value="06">Jun</option>-->
<!--					<option value="07">Jul</option>-->
<!--					<option value="08">Aug</option>-->
<!--					<option value="09">Sep</option>-->
<!--					<option value="10">Oct</option>-->
<!--					<option value="11">Nov</option>-->
<!--					<option value="12">Dec</option>-->
<!--				</select>-->
<!--				<select class="required" id="birthday_day" name="birthday_day">-->
<!--					<option value="">Day:</option>-->
<!--					<option value="01">1</option>-->
<!--					<option value="02">2</option>-->
<!--					<option value="03">3</option>-->
<!--					<option value="04">4</option>-->
<!--					<option value="05">5</option>-->
<!--					<option value="06">6</option>-->
<!--					<option value="07">7</option>-->
<!--					<option value="08">8</option>-->
<!--					<option value="09">9</option>-->
<!--					<option value="10">10</option>-->
<!--					<option value="11">11</option>-->
<!--					<option value="12">12</option>-->
<!--					<option value="13">13</option>-->
<!--					<option value="14">14</option>-->
<!--					<option value="15">15</option>-->
<!--					<option value="16">16</option>-->
<!--					<option value="17">17</option>-->
<!--					<option value="18">18</option>-->
<!--					<option value="19">19</option>-->
<!--					<option value="20">20</option>-->
<!--					<option value="21">21</option>-->
<!--					<option value="22">22</option>-->
<!--					<option value="23">23</option>-->
<!--					<option value="24">24</option>-->
<!--					<option value="25">25</option>-->
<!--					<option value="26">26</option>-->
<!--					<option value="27">27</option>-->
<!--					<option value="28">28</option>-->
<!--					<option value="29">29</option>-->
<!--					<option value="30">30</option>-->
<!--					<option value="31">31</option>-->
<!--				</select>-->
<!--				<select class="required" id="birthday_year" name="birthday_year">-->
<!--					<option value="">Year:</option>-->
<!--					<option value="2013">2013</option>-->
<!--					<option value="2012">2012</option>-->
<!--					<option value="2011">2011</option>-->
<!--					<option value="2010">2010</option>-->
<!--					<option value="2009">2009</option>-->
<!--					<option value="2008">2008</option>-->
<!--					<option value="2007">2007</option>-->
<!--					<option value="2006">2006</option>-->
<!--					<option value="2005">2005</option>-->
<!--					<option value="2004">2004</option>-->
<!--					<option value="2003">2003</option>-->
<!--					<option value="2002">2002</option>-->
<!--					<option value="2001">2001</option>-->
<!--					<option value="2000">2000</option>-->
<!--					<option value="1999">1999</option>-->
<!--					<option value="1998">1998</option>-->
<!--					<option value="1997">1997</option>-->
<!--					<option value="1996">1996</option>-->
<!--					<option value="1995">1995</option>-->
<!--					<option value="1994">1994</option>-->
<!--					<option value="1993">1993</option>-->
<!--					<option value="1992">1992</option>-->
<!--					<option value="1991">1991</option>-->
<!--					<option value="1990">1990</option>-->
<!--					<option value="1989">1989</option>-->
<!--					<option value="1988">1988</option>-->
<!--					<option value="1987">1987</option>-->
<!--					<option value="1986">1986</option>-->
<!--					<option value="1985">1985</option>-->
<!--					<option value="1984">1984</option>-->
<!--					<option value="1983">1983</option>-->
<!--					<option value="1982">1982</option>-->
<!--					<option value="1981">1981</option>-->
<!--					<option value="1980">1980</option>-->
<!--					<option value="1979">1979</option>-->
<!--					<option value="1978">1978</option>-->
<!--					<option value="1977">1977</option>-->
<!--					<option value="1976">1976</option>-->
<!--					<option value="1975">1975</option>-->
<!--					<option value="1974">1974</option>-->
<!--					<option value="1973">1973</option>-->
<!--					<option value="1972">1972</option>-->
<!--					<option value="1971">1971</option>-->
<!--					<option value="1970">1970</option>-->
<!--					<option value="1969">1969</option>-->
<!--					<option value="1968">1968</option>-->
<!--					<option value="1967">1967</option>-->
<!--					<option value="1966">1966</option>-->
<!--					<option value="1965">1965</option>-->
<!--					<option value="1964">1964</option>-->
<!--					<option value="1963">1963</option>-->
<!--					<option value="1962">1962</option>-->
<!--					<option value="1961">1961</option>-->
<!--					<option value="1960">1960</option>-->
<!--					<option value="1959">1959</option>-->
<!--					<option value="1958">1958</option>-->
<!--					<option value="1957">1957</option>-->
<!--					<option value="1956">1956</option>-->
<!--					<option value="1955">1955</option>-->
<!--					<option value="1954">1954</option>-->
<!--					<option value="1953">1953</option>-->
<!--					<option value="1952">1952</option>-->
<!--					<option value="1951">1951</option>-->
<!--					<option value="1950">1950</option>-->
<!--					<option value="1949">1949</option>-->
<!--					<option value="1948">1948</option>-->
<!--					<option value="1947">1947</option>-->
<!--					<option value="1946">1946</option>-->
<!--					<option value="1945">1945</option>-->
<!--					<option value="1944">1944</option>-->
<!--					<option value="1943">1943</option>-->
<!--					<option value="1942">1942</option>-->
<!--					<option value="1941">1941</option>-->
<!--					<option value="1940">1940</option>-->
<!--					<option value="1939">1939</option>-->
<!--					<option value="1938">1938</option>-->
<!--					<option value="1937">1937</option>-->
<!--					<option value="1936">1936</option>-->
<!--					<option value="1935">1935</option>-->
<!--					<option value="1934">1934</option>-->
<!--					<option value="1933">1933</option>-->
<!--					<option value="1932">1932</option>-->
<!--					<option value="1931">1931</option>-->
<!--					<option value="1930">1930</option>-->
<!--					<option value="1929">1929</option>-->
<!--					<option value="1928">1928</option>-->
<!--					<option value="1927">1927</option>-->
<!--					<option value="1926">1926</option>-->
<!--					<option value="1925">1925</option>-->
<!--					<option value="1924">1924</option>-->
<!--					<option value="1923">1923</option>-->
<!--					<option value="1922">1922</option>-->
<!--					<option value="1921">1921</option>-->
<!--					<option value="1920">1920</option>-->
<!--					<option value="1919">1919</option>-->
<!--					<option value="1918">1918</option>-->
<!--					<option value="1917">1917</option>-->
<!--					<option value="1916">1916</option>-->
<!--					<option value="1915">1915</option>-->
<!--					<option value="1914">1914</option>-->
<!--					<option value="1913">1913</option>-->
<!--					<option value="1912">1912</option>-->
<!--					<option value="1911">1911</option>-->
<!--					<option value="1910">1910</option>-->
<!--					<option value="1909">1909</option>-->
<!--					<option value="1908">1908</option>-->
<!--					<option value="1907">1907</option>-->
<!--					<option value="1906">1906</option>-->
<!--					<option value="1905">1905</option>-->
<!--				</select>-->
<!--			</td>-->
<!--		</tr>-->

</div>
</td>
</tr>
<!--<tr>-->
<!--	<td align="right">-->
<!--		Referred By:<span class="requiredstar">*</span></td>-->
<!--	<td colspan="2">-->
<!--		<input class="required" name="referredBy" type="text"/></td>-->
<!--</tr>-->
<!--<tr>-->
<!--	<td align="right">-->
<!--		Best Times<br/>-->
<!--		To Reach You<span class="requiredstar">*</span></td>-->
<!--	<td>-->
<!--		<input class="required" name="bestTimeToCall[]" type="checkbox" value="Weekdays"/>Weekdays<br/>-->
<!--		<input name="bestTimeToCall[]" type="checkbox" value="Weekday Evenings"/>Weekday Evenings-->
<!--	</td>-->
<!--	<td>-->
<!--		<input name="bestTimeToCall[]" type="checkbox" value="Weekend Days"/>Weekend Days<br/>-->
<!--		<input name="bestTimeToCall[]" type="checkbox" value="Weekend Evenings"/>Weekend Evenings-->
<!--	</td>-->
<!--</tr>-->
</tbody>
</table>
</div>
</div>
<div class="grid_10 questionbox" style="width: 550px;">
	<div class="boxtitle">
		Mailing Address:
	</div>
	<div style="padding:0 10px 0 10px;">
		<div class="errorMessage">
			&nbsp;</div>
		<table border="0" cellpadding="0" width="100%">
			<tbody>
<!--			<tr>-->
<!--				<td align="right" width="20%">-->
<!--					Street Address:<span class="requiredstar">*</span></td>-->
<!--				<td width="80%">-->
<!--					<input class="required" name="street" type="text"/></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td align="right" width="20%">-->
<!--					City:<span class="requiredstar">*</span></td>-->
<!--				<td width="80%">-->
<!--					<input class="required" name="city" type="text"/></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td align="right" width="20%">-->
<!--					State/Prov:<span class="requiredstar">*</span></td>-->
<!--				<td width="80%">-->
<!--					<input class="required" name="state" type="text"/></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td align="right" width="20%">-->
<!--					Zip/Postal:<span class="requiredstar">*</span></td>-->
<!--				<td width="80%">-->
<!--					<input class="required" name="zipCode" type="text"/></td>-->
<!--			</tr>-->
			<tr>
				<td align="right" width="20%">
					Country:<span class="requiredstar">*</span></td>
				<td width="80%">
					<input class="required" name="country" type="text"/></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
<div class="grid_16 spacer">
	&nbsp;</div>
<div class="equalize">
	<?php
		foreach ($quest as $q)
		{
		if ($q->order == '1')
		{
			?>
			<div class="grid_11 questionbox" style="min-height:145px;">
				<div style="padding:0 10px 0 10px;">
					<div class="errorMessage">
						&nbsp;</div>
					<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
					<textarea class="required" name="<?php echo 'question' . $q->questionId; ?>" rows="6"></textarea>
				</div>
			</div>

		<?php
		}
		if ($q->order == '2')
		{
	?>
	<div class="grid_5 questionbox" style="min-height:145px;">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

				$i = 0;
				foreach ($choices as $c)
				{
					echo '<input ';
					if ($i == 0)
						echo 'class="required"';
					echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
					$i++;
				} ?></div>
	</div>
</div>
<div class="grid_16 spacer">
	&nbsp;</div>
<?php
	}
	if ($q->order == '3')
	{
?>
<div class="equalize">
	<div class="grid_11 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<textarea class="required" name="<?php echo 'question' . $q->questionId; ?>" rows="6"></textarea>
		</div>
	</div>
	<?php
		}
		if ($q->order == '4'){
	?>
	<div class="grid_5 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

				$i = 0;
				foreach ($choices as $c)
				{
					echo '<input ';
					if ($i == 0)
						echo 'class="required"';
					echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
					$i++;
				} ?>
		</div>
	</div>
</div>
	<div class="grid_16 spacer">
		&nbsp;</div>
<?php
	}
	if ($q->order == '5')
	{
		?>
		<div class="grid_16 questionbox">
			<div style="padding:0 10px 0 10px;">
				<div class="errorMessage">
					&nbsp;</div>
				<div class="questiontext">
					<?php echo $q->question; ?><span class="requiredstar">*</span></div>
				<table border="0" cellpadding="0" width="100%">
					<tbody>
					<tr>
						<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

							$i = 0;
							foreach ($choices as $c)
							{
								if ($i % 3 == 0)
								{
									echo '</tr><tr>';
								}
								echo '<td><input ';
								if ($i == 0)
									echo 'class="required"';
								echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '</td>';
								$i++;
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="grid_16 spacer">
			&nbsp;</div>
	<?php
	}
	if ($q->order == '6')
	{
		?>
		<div class="grid_16 questionbox">
			<div style="padding:0 10px 0 10px;">
				<div class="errorMessage">
					&nbsp;</div>
				<div class="questiontext">
					<?php echo $q->question; ?><span class="requiredstar">*</span></div>
				<table border="0" cellpadding="0" width="100%">
					<tbody>
					<tr>
						<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

							$i = 0;
							foreach ($choices as $c)
							{
								if ($i % 3 == 0)
								{
									echo '</tr><tr>';
								}
								echo '<td><input ';
								if ($i == 0)
									echo 'class="required"';
								echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '</td>';
								$i++;
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="grid_16 spacer">
			&nbsp;</div>
	<?php
	}
	if ($q->order == '7'){
?>
<div class="equalize">
	<div class="grid_11 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<table border="0" cellpadding="0" width="100%">
				<tbody>
				<tr>
					<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

						$i = 0;
						foreach ($choices as $c)
						{
							if ($i % 3 == 0)
							{
								echo '</tr><tr>';
							}
							echo '<td><input ';
							if ($i == 0)
								echo 'class="required"';
							echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '</td>';
							$i++;
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<?php
		}
		if ($q->order == '8'){
	?>
	<div class="grid_5	 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

				$i = 0;
				foreach ($choices as $c)
				{
					echo '<input ';
					if ($i == 0)
						echo 'class="required"';
					echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
					$i++;
				} ?>
		</div>
	</div>
</div>
	<div class="grid_16 spacer">
		&nbsp;</div>
<?php
	}
	if ($q->order == '9'){
?>
<div class="equalize">
	<div class="grid_5 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?></div>
			<textarea name="<?php echo 'question' . $q->questionId; ?>" rows="7"></textarea>
			<?php
				}
				if ($q->order == '10'){
			?>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

				$i = 0;
				foreach ($choices as $c)
				{
					echo '<input ';
					if ($i == 0)
						echo 'class="required"';
					echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
					$i++;
				} ?></div>
	</div>
	<?php
		}
		if ($q->order == '11')
		{
			?>
			<div class="grid_6 questionbox">
				<div style="padding:0 10px 0 10px;">
					<div class="errorMessage">
						&nbsp;</div>
					<div class="questiontext">
						<?php echo $q->question; ?><span class="requiredstar">*</span></div>
					<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

						$i = 0;
						foreach ($choices as $c)
						{
							echo '<input ';
							if ($i == 0)
								echo 'class="required"';
							echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
							$i++;
						}
					?></div>
			</div>
		<?php
		}
		if ($q->order == '12') {
	?>
	<div class="grid_5 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody>
				<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

					$i = 0;
					foreach ($choices as $c)
					{
						echo '<tr><td><input ';
						if ($i == 0)
							echo 'class="required"';
						echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /></td><td> ' . $c->choiceText . '</td></tr>';
						$i++;
					} ?>
				</tbody>
			</table>
			<?php
				}
				if ($q->order == '13'){
			?>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody>
				<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

					$i = 0;
					foreach ($choices as $c)
					{
						echo '<tr><td><input ';
						if ($i == 0)
							echo 'class="required"';
						echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /></td><td> ' . $c->choiceText . '</td></tr>';
						$i++;
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
	<div class="grid_16 spacer">
		&nbsp;</div>
<?php
	}
	if ($q->order == '14'){
?>
<div class="equalize">
	<div class="grid_11 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody>
				<?php  $choices = $profile->getQuestionChoices($database, $q->questionId);
					$i = 0;
					foreach ($choices as $c)
					{
						if (!($c->choiceId == '193' or $c->choiceId == '194'))
						{
							echo '<tr><td><input ';
							if ($i == 0)
								echo 'class="required"';
							echo ' name="question' . $q->questionId . '[]" type="checkbox" value="' . $c->choiceId . '" /></td><td> ' . $c->choiceText . '</td><tr>';
							$i++;
						}
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
	<?php
		}
		if ($q->order == '15'){
	?>
	<div class="grid_5 questionbox">
		<div style="padding:0 10px 0 10px;">
			<div class="errorMessage">
				&nbsp;</div>
			<div class="questiontext">
				<?php echo $q->question; ?><span class="requiredstar">*</span></div>

			<?php $choices = $profile->getQuestionChoices($database, $q->questionId);

				$i = 0;
				foreach ($choices as $c)
				{
					echo '<input ';
					if ($i == 0)
						echo 'class="required"';
					echo ' name="question' . $q->questionId . '" type="radio" value="' . $c->choiceId . '" /> ' . $c->choiceText . '<br />';
					$i++;
				} ?>
			<br/>
			<?php
				}
				if ($q->order == '16'){
			?>
			<div class="questiontext">
				<?php echo $q->question; ?></div>
			<textarea name="<?php echo 'question' . $q->questionId; ?>" rows="6"></textarea></div>
	</div>
</div>
<?php
	}
	} ?>
<div class="grid_16 spacer">
	&nbsp;</div>
<div class="grid_16">
	<div style="padding:0 10px 0 10px;">
		<div class="errorMessage">
			&nbsp;</div>
		<input class="button" type="submit" value="Submit"/></div>
</div>
<div class="grid_16 spacer_30">
	&nbsp;</div>
</form>
</div>
<p>&nbsp;
</p>
</body></html>