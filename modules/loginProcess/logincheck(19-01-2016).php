<?php

include '../../classes/init.php'; 

require_once ("../../classes/payment.php");
require_once("../../classes/registerOntraport.php");


$register = new registerOntraport();
$confirm=new payment();

$url=$_SERVER['REQUEST_URI'];
if (!isset($_SESSION['userId']))
{
	if((isset($_COOKIE['stayLogIn']) && ($_COOKIE['userId']!=NULL) )){
		
	SendRedirect("../../signup.php?url=".$url);
	}
	else
	{
			SendRedirect("../../signup.php");
	}

}else{
	
	$userId=$_SESSION['userId'];
	$ExpiryConfirmation=$confirm->trialPeriod($userId);
	$isPaypalVerified=$confirm->isPaypalVerified($userId);
		 if(strtotime("$ExpiryConfirmation->expiryDate + 1 day")<strtotime(date('Y-m-d')) && $isPaypalVerified==1){
			sendRedirect("../loginProcess/paymentplans.php?user_id=".$userId);
			}
	}
$isOntraportUserLogin=$register->isOntraportUserLogin($_SESSION['userId']);
$isOntraportUser=$register->isOntraportUser($_SESSION['userId']);
if ($isOntraportUser->isTransferredToOntraport==1 || $isOntraportUser->isOntraport == 1 ){
    $ontraport=1;
}
if ($ontraport == 1 && $isOntraportUserLogin==0){

    SendRedirect("../../signup.php");
}elseif($isOntraportUser->isProfileComplete==0 && $isOntraportUser->isOntraport==1){

    SendRedirect("../ontraport/profilecompletion.php");

}


?>
