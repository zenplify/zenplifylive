<?php 
session_start();
require_once ("../../classes/payment.php");

$confirm=new payment();

$userId=$_REQUEST['user_id'];

$userdetail=$confirm->getUserById($userId);
$transactionDetail=$confirm->getPaypalTransactionDetail($userId);

?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	 <title>Zenplify</title>
    	 <link rel="stylesheet" href="../../css/style.css" type="text/css">
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
             
        <script>
        
			$(document).ready(function(e) {
                $("#activate").click(function(e) {
				var userId=<?php echo $userId;?>;	
                $.ajax({
					type: "POST",
					 url:"../../ajax/ajax.php",
					 data:{action:'activatepayment',userId:userId},
					 success: function(data) {
						if(data == 1){
							
							window.location.href = "paymentplans.php?user_id="+userId;
						}
					}
				 });
				});
				
            });
				
				
        </script>
	
  </head>
  
  <body>
  	<div class="container">
      <div id="top_cont">
          <div id="logo"><img src="../../images/logo.png" /></div>
      </div>
 	</div>
  <div class="container">
  <div class="planPeriod">
  <h1 >Deactivated Payment</h1>
  <table class="planPeriod" width="600px">
  <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  <tr>
     <td style="text-align:center; " colspan="2">
        <?php 
            echo "Zenplify didn't receive schedule payments for your current period against <b>".$userdetail->userName."</b> . <br />Your account is deactivated till ".$transactionDetail->nextpaymentDate.". ";  
         ?>
      </td>
</tr>
  <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  <tr><td>&nbsp;</td><td >&nbsp;</td></tr>
  
  <tr><td align="right"><input type="button" name="activePayment"  value="" id="activate" style="float:right !important;"/></td><td ><input type="button" name="cancel"  value="" id="cancel" onclick="window.location.href='../../signup.php'"/></td></tr>
  </table>

  </div>
  <div class="empty"></div>
  <div class="empty"></div>
   <div class="empty"></div>
  <div class="empty"></div>
  <div class="empty"></div>
  <div class="empty"></div>
  
 
   <?php 
		include_once("../headerFooter/footer.php");
	?>
   
     
  