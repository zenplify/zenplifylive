<?php
    include '../../classes/init.php';
    require_once '../../classes/register.php';
    require_once '../../classes/email.php';
    require_once '../../classes/settings.php';

    $database = new database();
    $register = new register();
    $settings = new settings();
    $timezone = $register->getTimeZone();
    $leaderTemplates = $register->getLeaderTemplates();
    $action = $_REQUEST['action'];
    $leaderSignUp = $_REQUEST['leadersignup'];

if (isset($leaderSignUp))
{

    $isOntraportLeader = $register->checkUserAvailable($leaderSignUp);

    if (!empty($isOntraportLeader))
    {
        header('Location:https://zenplify.biz/sales/'.$leaderSignUp);
//        header('Location:http://techgeese.com/modules/loginProcess/signup.php?leadersignup='.$_REQUEST['leaderName']);
    }
}



    if (isset($leaderSignUp))
    {

        $getUserIdIfexist = $register->checkUserAvailablity($_REQUEST['leadersignup']);

        if (empty($getUserIdIfexist))
        {

            header('Location:https://zenplify.biz/invalid.php');
        }
    }

    if ($action == "failure")
    {
        $message = "This Username is Unavailable. Please try another one.";
        $message1 = " This Email Address already exists.";
    }

    if ((isset($_POST["submit"])) && ($_POST["step"] == 2))
    {
        $checkemailusername = $register->SignupCheckForUser($_POST['username'], $_POST['email']);
        if ($checkemailusername == 1)
        {
            sendRedirect("signup.php?action=failure");
        }
        else
        {
            $result = $register->RegisterUser($_POST);
            $codeanduserId = explode('/', $result);
            $code = $codeanduserId[0];
            $userId = $codeanduserId[1];
            if (!empty($code))
            {
                $to = $_POST['email']; //to address
                $subject = "Welcome to Zenplify!";
                $message = "<br />Hi " . $_POST['firstname'] . "," . "<br /><br />Welcome to Zenplify!" . "<br /><br />Your username is " . $_POST['username'] . ". " . "<br /><br />(Click <a href=\"https://zenplify.biz/signin.php\"> Here </a> if you need help with your password)" . "<br /><br />" . "Please click the link below to verify your email address and activate your account.<br /><br />" . "https://zenplify.biz/modules/loginProcess/login_check.php?code=" . $code . "<br /><br />
			If you click the link and it appears to be broken, copy and paste it into a new browser window. Make sure to copy and paste the whole URL to ensure it works properly.<br /><br />Thanks for using Zenplify!<br /><br />Sincerely,<br /><br />The Zenplify Team";
                $from = "<support@zenplify.biz>";
                $headers = "From:" . $from . "\r\n";
                $headers .= "Content-Type: text/html";
                $sent = mail($to, $subject, $message, $headers);
                if ($sent == 1)
                {
                    //$payment->userPayments(1,$userId);//userPayments(planId,userId)
                    //sendRedirect("../../index.php?signup=signup");
                }
                sendRedirect("paymentplans.php?user_id=" . $userId);
                //sendRedirect("../../signup.php");
                //exit;
                //sendredirect('main.php?code='.$pin);
            }
            //timeZoneId='.$timeZoneId.',
        }
    }

    if (isset($_REQUEST['leader']))
    {
        $leaderTemplate = $register->getUserIdbyUsername($database, $_REQUEST['leader']);
    }


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Zenplify</title>
    <link rel="stylesheet" href="<?php echo $CONF["siteURL"]; ?>css/style.css" type="text/css">
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>js/script.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>validation/lib/jquery.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>validation/jquery.validate.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#email").focus(function (e) {
                $("#messageEmail").html('');
                messageUser
            });
            $("#username").focus(function (e) {
                $("#messageUser").html('');
            });
            /*$('#web').blur(function(e) {
             var web=$('#web').val();
             var result = web.replace(/.*?:\/\//g, "");
             $('#web').val(result);
             });
             $('#facebook_page').blur(function(e) {
             var web=$('#facebook_page').val();
             var result = web.replace(/.*?:\/\//g, "");
             $('#facebook_page').val(result);
             });
             $('#facebook_personalpage').blur(function(e) {
             var web=$('#facebook_personalpage').val();
             var result = web.replace(/.*?:\/\//g, "");
             $('#facebook_personalpage').val(result);
             });
             $('#facebook_partypage').blur(function(e) {
             var web=$('#facebook_partypage').val();
             var result = web.replace(/.*?:\/\//g, "");
             $('#facebook_partypage').val(result);
             });   */
        });
        window.onload = function () {
            var myInput = document.getElementById('emailConfirm');
            if (myInput) {
                myInput.onpaste = function (e) {
                    e.preventDefault();
                }
            }
        }
        function checkIfAlredyExist() {
            var emailcheck = document.getElementById('emailcheck').value;
            var usercheck = document.getElementById('usercheck').value;
            var userSpecialcheck = document.getElementById('userSpecialcheck').value;
            if (emailcheck == 0 || usercheck == 0 || userSpecialcheck == 0) {
                if (userSpecialcheck == 0) {
                    $("#username").focus();
                }
                return false;
            }
            else {
                var leaderTemplate = $("#leader_template").val();
                if (leaderTemplate == 1)
                    return confirm("Do you want to signup as blank account!");

                return true;
            }
        }
        function cancelPage() {
            window.location.replace("https://zenplify.biz/signup.php");
        }
    </script>

    <?php require_once("../../classes/resource.php"); ?>
</head>
<body>
<div class="container">
    <div id="top_cont">
        <div id="logo"><img alt="" src="../../images/logo.png"/></div>
    </div>
</div>
<div class="container">
<div style="float:right; margin-bottom:10px; ">
    <!--<input  class="signin" type="button" value="" name="submit" onclick="window.location.href='../../signup.php'">-->
</div>
<h1 class="gray" style="padding-left:0px !important;">Registration</h1>

<form name="add_user" id="add_user" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post"
      onsubmit="return checkIfAlredyExist();">
    <table id="frmSignupUser" cellspacing="0" cellpadding="0" width="970px">
        <?php
            if ($_POST['step'] == 1)
            {
            $leaderCustomFields = $settings->getLeaderDefaultFields($_POST['leaderTemplate'], 1);
            $leaderCustomQuestions = $settings->getLeaderCustomQuestions($_POST['leaderTemplate'], '1');
            //$leaderSettings = $settings->getLeaderSettings($_POST['leaderTemplate']);
            //$upComingTask = $leaderSettings->upComingTasks;
        ?>
        <input type="hidden" name="isleader" id="isleader" value="0"/>
        <input type="hidden" name="step" id="step" value="2"/>
        <input type="hidden" name="leaderTemplate" id="leaderTemplate" value="<?php echo $_POST['leaderTemplate'] ?>"/>
        <input type="hidden" name="username" id="username" value="<?php echo $_POST['username'] ?>"/>
        <input type="hidden" name="password" id="password" value="<?php echo $_POST['password'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo $_POST['firstname'] ?>"/>
        <input type="hidden" name="lastname" id="lastname" value="<?php echo $_POST['lastname'] ?>"/>
        <input type="hidden" name="email" id="email" value="<?php echo $_POST['email'] ?>"/>
        <input type="hidden" name="timezone" id="timezone" value="<?php echo $_POST['timezone'] ?>"/>
        <input type="hidden" name="phonenumber" id="phonenumber" value="<?php echo $_POST['phonenumber'] ?>"/>
        <input type="hidden" name="emailcheck" id="emailcheck" value="1">
        <input type="hidden" name="usercheck" id="usercheck" value="1">
        <input type="hidden" name="userSpecialcheck" id="userSpecialcheck" value="1">
        <!--<input type="hidden" name="upComingTask" id="upComingTask" value="< ?php echo $upComingTask ?>" />-->
        <?php
            foreach ($leaderCustomFields as $lcf)
            {
                switch ($lcf->fieldMappingName)
                {
                    case 'consultantId':
                        ?>
                        <tr>
                            <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                            <td><input type="text" name="consultant_id" class="textfield"/></td>
                        </tr>
                        <?php
                        break;
                    case 'title':
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                        <td><input type="text" name="title" class="textfield" style="float:left;"/></td>
                        </tr><?php
                        break;
                    case 'webAddress':
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                        <td><input type="text" name="web" class="textfield url" style="float:left;" id="web"/></td>
                        </tr><?php
                        break;
                    case 'facebookAddress':
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                        <td><input type="text" name="facebook_page" class="textfield url" style="float:left;"
                                   id="facebook_page"/></td>
                        </tr><?php
                        break;
                }
            }
            foreach ($leaderCustomQuestions as $lcq)
            {
                switch ($lcq->typeId)
                {
                    case '1':
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcq->question; ?></td>
                        <td><input type="text" name="<?php echo 'question' . $lcq->questionId; ?>"
                                   id="<?php echo 'question' . $lcq->questionId; ?>"
                                   class="textfield <?php if ($lcq->mandatory == 1)
                                   {
                                       echo 'required';
                                   } ?>"/></td>
                        </tr><?php
                        break;
                    case '3':
                        $questionChoice = $settings->getQuestionChoice($lcq->questionId);
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcq->question; ?></td>
                        <td>
                            <?php
                                foreach ($questionChoice as $qc)
                                {
                                    ?>
                                    <input type="checkbox" name="<?php echo 'choice' . $lcq->questionId; ?>[]"
                                           class="customField <?php if ($lcq->mandatory == 1)
                                           {
                                               echo 'required';
                                           } ?>" value="<?php echo $qc->choiceId . '_' . $qc->choiceText; ?>"/>
                                    <span
                                        style="display:inline-block; vertical-align:top;"><?php echo $qc->choiceText; ?></span>
                                    <label for="<?php echo 'choice' . $lcq->questionId; ?>[]" generated="true"
                                           class="error"></label>
                                    <br>
                                <?php
                                }
                            ?>
                        </td>
                        </tr><?php
                        break;
                    case '4':
                        $questionChoice = $settings->getQuestionChoice($lcq->questionId);
                        ?>
                        <tr>
                        <td class="label"><?php echo $lcq->question; ?></td>
                        <td>
                            <?php
                                foreach ($questionChoice as $qc)
                                {
                                    ?>
                                    <input type="radio" name="<?php echo 'choice' . $lcq->questionId; ?>[]"
                                           class="customField <?php if ($lcq->mandatory == 1)
                                           {
                                               echo 'required';
                                           } ?>" value="<?php echo $qc->choiceId . '_' . $qc->choiceText; ?>"/>
                                    <span
                                        style="display:inline-block; vertical-align:top;"><?php echo $qc->choiceText; ?></span>
                                    <label for="<?php echo 'choice' . $lcq->questionId; ?>[]" generated="true"
                                           class="error"></label>
                                    <br>
                                <?php
                                }
                            ?>
                        </td>
                        </tr><?php
                        break;
                }
            }
        ?>
        <tr>
            <td class="label">&nbsp;</td>
            <td id="btnTag" style="display: none">
                <input type="submit" name="submit" class="create_account" value=""/>
                <input type="button" name="cancel" class="cancel" value=""/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</form>
<?php
    if ((empty($leaderCustomFields)) && (empty($leaderCustomQuestions)))
    {
        ?>
        <script type="text/javascript">
            document.getElementsByName('submit')[0].click();
        </script>
    <?php
        }
        else
        {
    ?>
        <script type="text/javascript">
            $("#btnTag").show();
        </script>
    <?php
    }
    }
    else
    {
        if ((isset($_REQUEST['leader'])) && empty($leaderTemplate))
        {
            echo "<div style='margin:0 auto; width:2550px; margin-top:100px;'>" . $_REQUEST['leader'] . " is not a valid leader id, Kindly user correct Leader Id</div>";
        }
        else
        {
            ?>
            <tr>
                <td class="task_col">User Information</td>
                <td class="task_col" style="width:80% !important;">
                    <span style="color:#000; float:right; margin-right:15px; font-weight:normal; text-decoration:none;
                    font-size:12px;">
                    <?php  if (isset($_REQUEST['leadersignup']))
                    { ?>
                        &nbsp;<a  style="color:#63F; font-weight:normal; text-decoration:none; font-size:12px;" href="https://zenplify.biz/signup.php">Sign in</a> &nbsp;|&nbsp;


                    <?php }  ?>

                        <a style="color:#63F; font-weight:normal; text-decoration:none; font-size:12px;" href="https://zenplify.biz/support" target="_blank" >Help Desk</a>

</span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <input type="hidden" name="isleader" id="isleader" value="0"/>
            <input type="hidden" name="step" id="step" value="1"/>
            <?php
            if ((isset($_REQUEST['leader'])) && (!empty($leaderTemplate)))
            {
                ?><input type="hidden" name="leaderTemplate" id="leaderTemplate"
                         value="<?php echo $leaderTemplate; ?>"><?php
            }
            else
            {
                if (isset($leaderSignUp))
                {

                    $getUserIdIfexist = $register->checkUserAvailablity($_REQUEST['leadersignup']);

                    if (!empty($getUserIdIfexist))
                    {

                        ?>
                        <tr>
                            <td class="label">Leader's Template</td>


                            <td><p class="label"><?php echo $leaderSignUp; ?></p>
                                <input type="hidden" name="leaderTemplate" id="leader_template"
                                       class="textfield required" value="<?php echo $getUserIdIfexist; ?>"/>

                            </td>
                        </tr>
                    <?php
                    }
                }
                else
                {


                    ?>
                    <tr>
                        <td class="label">Leader's Template Id</td>
                        <td>
                            <select name="leaderTemplate" onchange="checkLeader(this)" class="SelectField" id="leader_template">
                                <option value="" selected="selected">Choose Leader's Template ID</option>
                                <option value="4">Blank</option>
                                <?php
                                    foreach ($leaderTemplates as $lt)
                                    {
                                        ?>
                                        <option value="<?php echo $lt->userId ?>"> <?php if ($lt->userId == '236')
                                            {
                                                echo
                                                'VENVPs';
                                            }
                                            else
                                            {
                                                echo $lt->userName;
                                            }
                                            ?>

                                    </option>

                                <?php
                                }
                            ?>

                            </select>
                        </td>
                    </tr>
                <?php
                }

            }
            ?>

            <tr>
                <td class="label" style="width:20% !important;">User Name</td>
                <td style="width:80% !important;">
                    <input type="text" name="username" id="username" class="textfield required"
                           value="<?php echo $_POST['username'] ?>"/>
                    <label>
                        <font class="message" id="messageUser"><?php echo($message == "" ? "" : $message); ?></font>
                    </label>
                    <label id="msg" class="msg"></label>
                    <input type="hidden" name="usercheck" id="usercheck" value="1"/>
                    <input type="hidden" name="userSpecialcheck" id="userSpecialcheck" value="1"/>
                </td>
            </tr>
            <tr>
                <td class="label">Password</td>
                <td>
                    <input type="password" name="password" class="textfield required" id="password"/>
                    <!--single quote removal-->
                    <label id="passwordMsg" class="msg"></label>
                    <!--single quote removal-->
                </td>
            </tr>
            <tr>
                <td class="label">Confirm Password</td>
                <td><input type="password" name="confirm_password" class="textfield"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="task_col">Contact Information</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="label">Enter your information below exactly as you would like it to appear on
                    your email signature and guest profile pages.
                </td>
            </tr>
            <tr>
                <td class="label">First Name</td>
                <td><input type="text" name="firstname" class="textfield"/></td>
            </tr>
            <tr>
                <td class="label">Last Name</td>
                <td><input type="text" name="lastname" class="textfield"/></td>
            </tr>
            <tr>
                <td class="label">Email Address</td>
                <td>
                    <input type="text" name="email" class="textfield" id="email"/><label><font class="message"
                                                                                               id="messageEmail"><?php echo($message1 == "" ? "" : $message1); ?></font></label>
                    <!-- <span style="color:#FF3300;font-size:12px;"> -->
                    <!-- <p style="color:#009933;font-size:12px;"> -->
                    <label id="msge" class="msg"></label>
                    <input type="hidden" name="emailcheck" id="emailcheck" value="1"/>
                </td>
            </tr>
            <tr>
                <td class="label">Confirm Email Address</td>
                <td>
                    <input type="text" name="emailConfirm" class="textfield" id="emailConfirm"/>
                    <!-- <span style="color:#FF3300;font-size:12px;"> -->
                    <!-- <p style="color:#009933;font-size:12px;"> -->
                </td>
            </tr>
            <!--<tr>
                  <td class="label">Facebook Personal Profile Link</td>
                <td><input type="text" name="facebook_personalpage" class="textfield url" id="facebook_personalpage" style="float:left;" /><div style="margin-top:4px; float:left; font-size:12px;color:#555555; margin-right:135px;">www.facebook.com/personalpage</div></td>
              </tr>-->
            <!--<tr>
                  <td class="label">Facebook Party Group Link</td>
                <td><input type="text" name="facebook_partypage" class="textfield" style="float:left;" id="facebook_partypage" /><div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;">www.facebook.com/groups/partyaddress</div></td>
              </tr>-->
            <tr>
                <td class="label">Time Zone</td>
                <td>
                    <select name="timezone" class="SelectField">
                        <?php

                            foreach ($timezone as $tz)
                            {
                                echo '<option value="' . $tz->timeZoneId . '">' . $tz->zoneTitle . ' (GMT ' . $tz->GMTDiff . ':00 )</option>';
                            }?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="label">Phone Number</td>
                <td><input type="text" name="phonenumber" class="textfield" style="float:left;"/>

                    <div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;"> Format
                        your number the way you want it to show in your signature
                    </div>
                </td>
            </tr>
            <!--<tr>
                <td class="label">Which Discover Arbonne video do you want in your account?</td>
                <td>
                    <select class="SelectField" name="videoOption" style="margin-right:35px;">
                    <option value="">Select Video</option>
                    <option value="US" selected="selected">Heather Mitchell (US)</option>
                    <option value="AUS">Dinah Williams (AUS)</option></select>
                </td>
            </tr>-->
            <tr>
                <td colspan="2" class="task_col"></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="label" style="padding-left:0px !important;">
                    <input type="checkbox" name="termandconditions" class="termandconditions"
                           style="float:left; margin-left:12px;"/>
                    <label
                        style="display: inline-block;height: 12px; margin-right:30px;margin-left: 10px; float:left; line-height:17px;">
                        Please accept our
                        <a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;"
                           onclick="loadform()">Terms of Service</a> &
                        <a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;"
                           onclick="loadform1()">Privacy Policy</a>
                    </label>
                </td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    <input type="submit" name="submit" class="create_account" value=""/>
                    <input type="button" name="cancel" class="cancel" value="" onclick="cancelPage();"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </table>
            </form>
        <?php
        }
    }
?>
<?php include("../headerFooter/footer.php"); ?>
<div id="Popup"></div>
<div id="popupQuickquote">
    <h1 class="gray">Terms of Service </h1>
    <?php include('termofUse.html'); ?>
    <!--	<ol>-->
    <!--		<li> You must be 13 years or older to use this Service.</li>-->
    <!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
    <!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</li>-->
    <!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to-->
    <!--			 comply with this security obligation.-->
    <!--		</li>-->
    <!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
    <!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
    <!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not-->
    <!--			 limited to copyright laws).-->
    <!--		</li>-->
    <!--	</ol>-->
</div>
<div id="popupQuickquote1">
    <h1 class="gray">Privacy Policy</h1>
    <?php include('termofUse.html'); ?>
    <!--	<ol>-->
    <!--		<li> You must be 13 years or older to use this Service.</li>-->
    <!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
    <!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</li>-->
    <!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to-->
    <!--			 comply with this security obligation.-->
    <!--		</li>-->
    <!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
    <!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
    <!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not-->
    <!--			 limited to copyright laws).-->
    <!--		</li>-->
    <!--	</ol>-->
</div>
</div>
<script>

    function checkLeader(leaderName) {
        var leaderId = leaderName.value;


        $.ajax({
            url    : "../../classes/ajax.php",
            type   : "post",
            data   : {action: 'isOntraportLeader', leaderName: leaderId},
            success: function (result) {

                var leaderName= result.trim();
                if (result != 1){

                    window.location = 'https://zenplify.biz/sales/'+leaderName;
                }
            }
        });
    }
</script>