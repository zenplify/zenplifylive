<?php
	session_start();
	require_once("../../classes/payment.php");
	require_once("../../classes/contact.php");
	$confirm = new payment();
	$contact = new contact();
	$userType = $_REQUEST['type'];
	$userId = $_REQUEST['user_id'];
	$contactDetails = $contact->GetUserDetails($userId);
	if ($contactDetails->leaderId == 0)
		$userType = 0;
	else
		$userType = 1;
	$duplicatepayerid = $_REQUEST['duplicatepayerid'];
	$ExpiryConfirmation = $confirm->trialPeriod($userId);
	$isPaypalVerified = $confirm->isPaypalVerified($userId);
	$paymentPlans = $confirm->getpaymentPlans($userType);
?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	 <title>Zenplify</title>
    	 <link rel="stylesheet" href="../../css/style.css" type="text/css">
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
	</head>
<body>
	<div class="container">
		<div id="top_cont">
			<div id="logo"><img src="../../images/logo.png"/></div>
		</div>
	</div>
<div class="container">
	<div class="planPeriod">
		<h1>Choose Your Payment Plan</h1>
		<table class="planPeriod" width="500px">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:center; " colspan="2">  <?php
						if ($duplicatepayerid == 1)
						{
							echo "Provided Paypal account is already associated with another existing Zenplify account. Please use some other Paypal account. ";
						}
						else
						{
							//if(strtotime($ExpiryConfirmation->expiryDate)<strtotime(date('Y-m-d')) && $isPaypalVerified!=1){
							if (strtotime($ExpiryConfirmation->expiryDate) < strtotime(date('Y-m-d')) && $isPaypalVerified != 1)
							{
								echo "<b>7-days</b> Free Trial on all Paid Plans. No credit card required.";
							}
							else
							{
								echo "Your Account has been Expired. Please choose any Plan to Subscribe for Zenplify";
							}
						}
					?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<?php
					$c = 0;
					foreach ($paymentPlans as $pp)
					{
						if ($pp->product_name <> "Zenplify Subscription - Trial")
						{
							echo "<td><input alt='" . $pp->title . "' type=\"image\" src=\"../../images/" . $pp->product_image . "\" border=\"0\" name=\"submit\" onmouseover=\"this.src='../../images/" . $pp->product_hover_image . "'\" onmouseout=\"this.src='../../images/" . $pp->product_image . "'\"  onclick=\"javascript:document.location.href='paypal.php?planId=" . $pp->planId . "&user_id=" . $userId . "'\"></td>";
						}
					}
				?>
				<!--<input type="image" src="../../images/basic.png" border="0" name="submit" onmouseover="this.src='../../images/basic-t.png';" onmouseout="this.src='../../images/basic.png';"  onclick="javascript:document.location.href='paypal.php?plan=basic&user_id=<?php //echo $userId;?>'">
            
                </td>
    <td>
        <input type="image" src="../../images/classic.png" border="0" name="submit" onmouseover="this.src='../../images/classic-t.png';" onmouseout="this.src='../../images/classic.png';" onclick="javascript:document.location.href='paypal.php?plan=classic&user_id=<?php //echo $userId;?>'">
     </td>-->
			</tr>
			<!-- <tr>
			 <td><input type="button" class="basic" value="" id="basic" onclick="javascript: window.location.href='confirm.php?plantype=basic'" /></td>
			 <td><input type="button" class="classic" id="classic" value=""  onclick="javascript: window.location.href='confirm.php?plantype=classic'"/></td>
			 </tr>-->
			<tr>
				<td>&nbsp;</td>
				<td class="creditcard">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div class="empty"></div>
	<div class="empty"></div>
<?php
	include_once("../headerFooter/footer.php");
?>