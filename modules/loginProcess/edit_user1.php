<?php
session_start();
include '../../classes/init.php';
require_once '../../classes/email.php';
include_once("../headerFooter/header.php");
require_once '../../classes/register.php';

$user_Id = $_SESSION['userId'];

$register = new register();
$profileDeleted = $_REQUEST['profileDeleted'];

$timezone = $register->getTimeZone();
$userInformation = $register->loginUserAfterRegister($user_Id);

if (isset($_GET['cancel_sub']) && $_GET['cancel_sub'] == 'success') {
	$register->updateOntraportPaymentInfo($user_Id, '0');
	$mes = 'Your Future payment Subscription has been cancelled';
	$color = '#0C3';
}

$timeZoneId = $userInformation->timeZoneId;

if ($_GET["success"] == 'success') {
	$mes = 'Your Information has been updated successfully.';
	$color = '#0C3';
} //single quote removal
elseif ($_GET["success"] == 'failed') {
	$mes = 'Your old password didnt match.';
	$color = 'red';
}
//single quote removal
if ($_GET["profileDeleted"] == 1) {
	$mes = 'Your Future payment Subscription has been cancelled';
	$color = '#0C3';
}

//if (isset($_POST["submit"]) && $_POST['usercheck']==1 && $_POST['emailcheck']==1) {
if (isset($_POST["submit"])) {
	//single quote removal
	if (!(empty($_POST['current_password']))) {
		$currentPassword = $register->matchCurrentPassword($_POST);
		if (empty($currentPassword)) {
			sendRedirect("edit_user1.php?success=failed");
			exit();
		}
	}
	// Update email for "Ontraport" & wordpress account
	$contactId = $_POST['ontraportContactId'];
	if ($contactId && $_POST['email'] != $_POST['oldEmail']) {

		// Ontraport
		$email = $_POST['email'];
		$data = <<<STRING
		<contact id="$contactId">
			<Group_Tag name="Contact Information">
				<field name="Email">$email</field>
				<field name="username">$email</field>
			</Group_Tag>

			<Group_Tag name="Memberships">
				<field name="username">$email</field>
			</Group_Tag>
		</contact>
STRING;

		$data = urlencode(urlencode($data));

		$appid = "2_29936_qqQw6MlCW";
		$key = "UEsIjGnYgLoxUsM";
		$reqType= "update";
		$postargs = "appid=".$appid."&key=".$key."&return_id=1&reqType=".$reqType."&data=".$data;
		$request = "http://api.ontraport.com/cdata.php";
		$session = curl_init($request);
		curl_setopt ($session, CURLOPT_POST, true);
		curl_setopt ($session, CURLOPT_POSTFIELDS, $postargs);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($session);
		curl_close($session);


		// Wordpress
		$postargs = "oldEmail=".$_POST['oldEmail']."&email=".$email;
		$request = "http://programs.zenplify.biz/updateUserInfo.php";
		$session = curl_init($request);
		curl_setopt ($session, CURLOPT_POST, true);
		curl_setopt ($session, CURLOPT_POSTFIELDS, $postargs);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($session);
		curl_close($session);

	}


	//single quote removal
	$result = $register->UpdateUser($_POST);
	$codeanduserId = explode('/', $result);
	$code = $codeanduserId[0];
	$userId = $codeanduserId[1];
	$emailCheck = $_POST['oldemail'];
	$email = $_POST['email'];
	if (strcasecmp($emailCheck, $email) != 0 && !empty($code)) {
		$to = $_POST['email']; //to address
		$subject = "Zenplify Mail Verification";
		$message = "<br />Dear " . $_POST['firstname'] . "," . "<br />You have changed the email address associated with your Zenplify account from " . $emailCheck . "  to " . $email . "." . "<br /><br />" . "Your username is " . $_POST['username'] . ".<br /><br />You will use this to log in to your Zenplify account. Please save this email so that you don't forget it.<br /><br />Please click the link below to verify your email address:<br /><br />
		http://zenplify.biz/modules/loginProcess/login_check.php?email=change&code=" . $code . "<br /><br />By verifying your external email address you will be able to send email from Zenplify using this address..<br /><br />Thanks for using Zenplify!<br /><br />Sincerely,<br /><br />ZenplifyTeam";
		$from = "<support@zenplify.biz>";
		$headers = "From:" . $from . "\r\n";
		$headers .= "Content-Type: text/html";
		$sent = mail($to, $subject, $message, $headers);
		if ($sent == 1) {
			sendRedirect("edit_user1.php?success=success");
		}

	} else {
		sendRedirect("edit_user1.php?success=success");
		exit();
	}

}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Zenplify</title>
<link rel="stylesheet" href="../../css/style.css" type="text/css">
<script type="text/javascript" src="../../js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="../../js/script.js"></script>
<!-- validation file included  -->
<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
<script>
	//single quote removal
	$().ready(function () {
		$('#newpassword').blur(function (e) {
			var userPassword = $('#newpassword').val();
			if (userPassword.length > 5) {
				console.log(userPassword.indexOf("'"));
				if (userPassword.indexOf("'") == -1) {
					$("#cnpMessage").hide();
					$("#usercheck").val(0);
				}
				else {
					$("#cnpMessage").html('Password cannot contain Single quotes').css('color', 'red').show();
					$("#usercheck").val(1);
				}
			}
			//var result = web.replace(/.*?:\/\//g, "");
			//		/$('#web').val(result);
		});
	});
	//single quote removal
	function sumform() {

		var msg = $("#msge").text();
		if (msg == 'This Email Address already exists.') {

			$("#msge").show();
			$("#msge").html("This Email Address already exists.").css('color', 'red');
			return false;
		}

		//single quote removal
		if ($("#usercheck").val() == 0) {
			//single quote removal
			var set_curr_pass = $("#set_curr_pass").val();                  //old password
			var get_curr_pass = $("#get_curr_pass").val();                  // old match password
			var get_new_pass = $("#newpassword").val();                     // new password
			var get_new_confirm_pass = $("#confirm_newpassword").val();     // new matched passsword
			console.log('current password length is ' + set_curr_pass.length);
			console.log('new password length is ' + get_new_pass.length);
			console.log('new password confirm length is ' + get_new_confirm_pass.length);
			if ((set_curr_pass.length == 0) && (get_new_pass.length == 0) && (get_new_confirm_pass == 0)) {
//				$("#cnpMessage").html("");
//				$("#cpMessage").show();
//				$("#cpMessage").html("Please provide valid current password");
				return true;
			}
			else if ((get_new_pass.length == 0) && (get_new_confirm_pass == 0)) {
				$("#cpMessage").html("");
				$("#cnpMessage").show();
				$("#cnpMessage").html("Please provide new password");
				return false;
			}
			else if (set_curr_pass.length == 0 && set_curr_pass != get_curr_pass) {
				$("#cpMessage").html("Please provide valid current password");
				$("#cnpMessage").html("Please provide new password");
				return false;
			}
			else if (set_curr_pass.length == 0 && set_curr_pass == get_curr_pass) {
				$("#cpMessage").html("Please provide valid current password");
				return false;
			}
			else if (set_curr_pass.length == 0 && get_new_pass.length == 0) {
				$("#cpMessage").html("");
				$("#cnpMessage").show();
				$("#cnpMessage").html("Please provide new password");
				return false;
			}
			else {
				return true;
			}
		}
		//single quote removal
		else {
			$("#cnpMessage").html('Password cannot contain Single quotes').css('color', 'red').show();
			return false;
		}
		//single quote removal
	}
</script>

<?php require_once("../../classes/resource.php"); ?>
</head>
<body>
<!--  <div id="menu_line"></div>-->
<div class="container">
	<h1 class="gray" style="padding-left:0px !important; width:200px !important; ">Edit Account Details</h1>

	<div style="margin:-29px 0 30px 190px; ">
		<font style="color:<?php echo $color; ?>; font-size:12px;"><?php echo($mes == "" ? "" : $mes); ?></font></div>
	<form name="add_user" id="add_user" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" onSubmit="return sumform();">
		<!--single quote removal-->
		<input type="hidden" name="usercheck" id="usercheck" value="0">
		<!--single quote removal-->
		<input type="hidden" name="userId" value="<?php echo $user_Id; ?>"/>
		<table cellspacing="0" cellpadding="0" width="970px">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" class="task_col" style="width:20% !important;">Contact Information
				</td>
				<td style="width:80% !important;"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<!-- Ontraport -->
			<?php
			$database = new database();
			$sql = "SELECT * FROM `usersOntraport` WHERE userId='".$user_Id."'";
			$ontraportDetails = $database->executeObject($sql);

			$ontraportContactId = ($ontraportDetails) ? $ontraportDetails->contactId : '';
			?>
			<input type="hidden" name="ontraportContactId" value="<?php echo $ontraportContactId; ?>"/>
			<input type="hidden" name="oldEmail" value="<?php echo $userInformation->email; ?>"/>
			<tr>
				<td class="label" style="width:20% !important;">User Name</td>
				<td style="width:80% !important;">
					<span class="label"><?php echo $userInformation->userName; ?></span>

					<input type="hidden" name="username" id="username" class="textfield" value="<?php echo $userInformation->userName; ?>"/>
					<label id="msg" class="msg"></label>

					<input type="hidden" name="usercheck" id="usercheck" value="1"/>
				</td>
			</tr>
			<tr>
				<td class="label">First Name</td>
				<td>
					<input type="text" name="firstname" class="textfield" value="<?php echo $userInformation->firstName; ?>"/>
				</td>
			</tr>
			<tr>
			<tr>
				<td class="label">Last Name</td>
				<td>
					<input type="text" name="lastname" class="textfield" value="<?php echo $userInformation->lastName; ?>"/>
				</td>
			</tr>
			<tr>
				<td class="label">Email Address</td>
				<td>
					<input type="text" name="email" class="textfield" id="email" value="<?php echo $userInformation->email; ?>"/>
					<input type="hidden" name="oldemail" class="textfield" value="<?php echo $userInformation->email; ?>"/>
					<!-- <span style="color:#FF3300;font-size:12px;"> -->
					<!-- <p style="color:#009933;font-size:12px;"> -->
					<label id="msge" class="msg"></label>

					<input type="hidden" name="emailcheck" id="emailcheck" value="1"/>
				</td>
			</tr>
			<tr>
				<td class="label">Independent Consultant ID</td>
				<td>
					<input type="text" name="consultant_id" class="textfield" value="<?php echo $userInformation->consultantId; ?>"/>
				</td>
			</tr>
			<tr>
				<td class="label">Title</td>
				<td>
					<input type="text" name="title" class="textfield" style="float:left;" value="<?php echo $userInformation->title; ?>"/>

					<div style="margin-top:4px; font-size:12px; color:#555555;">District Manager, Area Manager, etc
					</div>
				</td>
			</tr>
			<tr>
				<td class="label">Web Address</td>
				<td>
					<input type="text" name="web" class="textfield url" style="float:left;" value="<?php echo $userInformation->webAddress; ?>"/>

					<div style="margin-top:4px; float:left; font-size:12px;color:#555555; margin-right:19px;">
						www.arbonne.com or your myarbonne website address
					</div>
				</td>
			</tr>
			<tr>
				<td class="label">Facebook Page</td>
				<td>
					<input type="text" name="facebook_page" class="textfield url" style="float:left;" value="<?php echo $userInformation->facebookAddress; ?>"/>

					<div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:97px;">
						www.facebook.com/Arbonne
					</div>
				</td>
			</tr>
			<!--<tr>
				  <td class="label">Facebook Personal Profile Link</td>
				<td><input type="text" name="facebook_personalpage" class="textfield url" style="float:left;" value="< ?php echo $userInformation->facebookPersonalAddress;?>" /><div style="margin-top:4px; float:left; font-size:12px;color:#555555; margin-right:135px;">www.facebook.com/personalpage</div></td>
			  </tr>-->			<!--<tr>
          	<td class="label">Facebook Party Group Link</td>
            <td><input type="text" name="facebook_partypage" class="textfield url" style="float:left;" value="<?php echo $userInformation->facebookPartyAddress; ?>"/><div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;">www.facebook.com/groups/partyaddress</div></td>
          </tr>-->
			<tr>
				<td class="label">Time Zone
				</td>
				<td>
					<select name="timezone" class="SelectField">
						<?php

						foreach ($timezone as $tz) {
							echo '<option value="' . $tz->timeZoneId . '"  ' . ($tz->timeZoneId == $timeZoneId ? 'selected' : '') . '>' . $tz->zoneTitle . ' (GMT ' . $tz->GMTDiff . ':00 )</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="label">Phone Number</td>
				<td>
					<input type="text" name="phonenumber" class="textfield" style="float:left;" value="<?php echo $userInformation->phoneNumber; ?>"/>

					<div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;"> Format
						your number the way you want it to show in your signature
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="task_col" style="width:20% !important;">Change Password</td>
				<td style="width:80% !important;"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="label">Current Password</td>
				<td>
					<input type="password" name="current_password" class="textfield " id="set_curr_pass"/>
					<label style="font-size:12px;color:#F00;" id="cpMessage"></label>

					<input type="hidden" name="currentPassword" class="textfield " value="<?php //echo $userInformation->password; ?>" id="get_curr_pass"/>
				</td>
			</tr>
			<tr>
				<td class="label">New Password</td>
				<td>
					<input type="password" name="newpassword" class="textfield " id="newpassword"/>
					<label style="font-size:12px;color:#F00;" id="cnpMessage"></label></td>
			</tr>
			<tr>
			<tr>
				<td class="label">Confirm Password</td>
				<td>
					<input type="password" name="confirm_newpassword" class="textfield" id="confirm_newpassword"/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2" class="task_col" style="width:20% !important;">Payment Settings</td>
				<td style="width:80% !important;"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>
					<?php if ($userInformation->isOntraportPaymentActive == 1): ?>
						<a href="javascript:;" id="cancel-sub">Cancel Future Payments</a>
					<?php elseif ($userInformation->isPaypalProfileActive == 1): ?>
					<a style="font-size:12px; text-decoration:none;" href="javascript:" onClick="cancelPayment('<?php echo $user_Id; ?>','<?php echo $userInformation->paypalProfileId; ?>');">Cancel
						Future Payments</a></td>
				<?php endif; ?>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td class="label">&nbsp;</td>
				<td>
					<input type="submit" name="submit" class="update" value=""/>
					<input type="button" name="cancel" class="cancel" value="" onClick="history.go(-1)"/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form>
	<?php
	include("../headerFooter/footer.php");
	?>
</div>
<div id="backgroundPopupProfile"></div>
<div id="modal">
	<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css"/>
	<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css"/>
	<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css"/>
	<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css"/>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css"/>
	<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c29936f23" type="text/css"/>
	<script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c29936f23"></script>
	<div class="moonray-form-p2c29936f23 ussr">
		<div class="moonray-form moonray-form-label-pos-stacked">
			<form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
				<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-html">
					<div id="mr-field-element-475724633550" class=" moonray-form-element-html">
						<p>Are you sure want to cancel your subscription?</p>
					</div>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
					<input type="submit" name="submit-button" value="Confirm" class="moonray-form-input" id="mr-field-element-108281820779" src/>
					<button type="button" id="btnClose">Cancel</button>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="afft_" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="aff_" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="sess_" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="ref_" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="own_" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="oprid" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="contact_id" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="utm_source" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="utm_medium" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="utm_term" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="utm_content" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="utm_campaign" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="referral_page" type="hidden" value=""/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="uid" type="hidden" value="p2c29936f23"/>
				</div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
					<input name="email" type="hidden" value="<?php echo $userInformation->email; ?>"/>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$('#cancel-sub').on('click', function () {
		$('#backgroundPopupProfile').show();
		$('#modal').show();
	});
	$('#btnClose').on('click', function () {
		$('#backgroundPopupProfile').hide();
		$('#modal').hide();
	});
</script>
</body>
</html>
