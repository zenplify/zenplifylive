<?php
    error_reporting(0);
    include_once("../headerFooter/header.php");
    include_once("../../classes/init.php");
   require_once("../../classes/contact.php");
    require_once("../../classes/appiontment.php");
    require_once("../../classes/task.php");
    include_once("../../classes/profiles.php");
    include_once("../../classes/settings.php");
    include_once("../../classes/notification.php"); // by me
    $database = new database();
    $profile = new Profiles();
    $contact = new contact();
    $app = new appiontment();
    $task = new task();
    $settings = new settings();
    $notification = new notification(); // by me
    $details = $profile->getUserSignature($database, $userId);
    //
    if ($details->leaderId == 0) { $consultantTitleUserId = $details->userId; }
    else { $consultantTitleUserId = $details->leaderId; }
    $consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
    if (empty($consultantTitle->fieldCustomName) || ($details->userId == '3736' || $details->leaderId == '3736'))
    {
		// Exclude Juice Plus users
		if ($details->userId == '236' || $details->leaderId == '236')  {
			$arbonLogo = '<img src="https://zenplify.biz/images/GreenLogo1x1.png">';
		} elseif ($details->userId == '3736' || $details->leaderId == '3736') {

            $arbonLogo = '<img src="https://zenplify.biz/images/GreenLogo1x1.png">';
        }
        else{
			$arbonLogo = '';
		}
    }
    //
    $signature = '<span style="font-size:12px;font-family: arial, sans-serif;">Best Regards,</span><br /><br /><span style="font-size:14px;">' . $details->firstName . '</span><br />
	<div><div style="float:left;">'.$arbonLogo.'</div><div style=" margin-left:5px;float:left;font-size:12px;">' .
$details->firstName . ' ' . $details->lastName;

    if ($details->title != '')
        $signature = $signature . ', ' . $details->title;
    if (strpos($details->webAddress, "http://") >= 0 || strpos($details->webAddress, "https://") >= 0)
        $web = $details->webAddress;
    else
        $web = "http://" . $details->webAddress;

    if (strpos($details->facebookAddress, "http://") >= 0 || strpos($details->facebookAddress, "https://") >= 0)
        $facebook = $details->facebookAddress;
    else
        $facebook = "http://" . $details->facebookAddress;

    $webNew = preg_replace('#^https?://#', '', $web);
    $fbNew = preg_replace('#^https?://#', '', $facebook);

    //for some weired reason links doesnt work under iOS devices unless http:// is removed and added back to url
    $web = str_replace('http://www.', '', $web);
    $facebook = str_replace('http://www.', '', $facebook);
    $web = 'http://' . $web;
    $facebook = 'http://' . $facebook;
    //for some weired reason links doesnt work under iOS devices unless http:// is removed and added back to url
if (!empty($consultantTitle->fieldCustomName)) {

    if ($consultantTitle->status==1){
        $consultantTitleName= $consultantTitle->fieldCustomName;
        $consultantTitleId= $details->consultantId;
    }
} elseif(empty($consultantTitle->fieldCustomName)){

    $consultantTitleId= $details->consultantId;

}

    $signature = $signature . '<br /><span style="color: #2E6A30;font-size:12px;font-family: arial, sans-serif;">'.$consultantTitleName.' ' . $details->consultantId . '</span><br /><span style="color: #2E6A30;font-size:14px;"><a style="color: #2E6A30;font-size:14px;" href="' . $web . '" target="_blank">' . $webNew . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $facebook . '" target="_blank">' . $fbNew . '</a></div></div>';

    $_SESSION['signature'] = $signature;
    $userId = $_SESSION['userId'];

    $queryValueUpcomingTasks = $contact->queryValueUpcomingTasks($userId);
    $contacts = $contact->GetUserContacts($userId);
    $appiontment = $app->GetTodaysAppointments($userId);
    $tasks = $task->getTodaysTask($userId);
    $tasksOver = $task->getOverdueTask5($userId);
    $tasksUpcoming = $task->getUpcomingtask5($userId, $queryValueUpcomingTasks);
    $tasksCount = count($tasks);
    $contactsCount = count($contacts);
    $appiontmentCount = count($appiontment);
    $totalTasks = count($tasks) + count($tasksUpcoming) + count($tasksOver);
    $totalRecords = 15 - $tasksCount;
    $taskTotal = $tasksCount + $tasksCountOver;
    $quicklink = $settings->getLeaderQuickLinks($leaderId, 1);
    $usernotification = $notification->getNotification($database, $userId); // by me

?>
<!--  <div id="menu_line"></div>-->
<div class="container">
<h1>
    <?php
        echo date('M') . '  ' . date('d') . ',     ' . date(Y);
    ?>
    <!-- Thursday, 27 December, 2012-->
</h1>

<div id="dashboard">
<div class="col_one">
    <div style="height:200px">
        <section>
            <a href="../calender/sample.php"><p class="col_header"><img src="../../images/Appointment-icon.png"/>
                    Calendar </p></a>
            <ul>
                <?php
                    if (!(empty($appiontment)))
                    {
                        $j = 0;
                        while ($j < sizeof($appiontment))
                        {
                            if ($j % 2 == 0)
                            {
                                echo "<li class=\"col_row\">";
                            }
                            else
                            {
                                echo "<li class=\"col_row_alter\">";
                            }
                            echo "<span class=\"title\"><a href=\"../appiontment/edit_appiontment.php?appId=" . $appiontment[$j]->appiontmentId . "\">";
                            if (strlen($appiontment[$j]->title) > 25)
                            {
                                echo substr($appiontment[$j]->title, 0, 25) . "...";
                            }
                            else
                            {
                                echo $appiontment[$j]->title;
                            }
                            echo "</a></span>";
                            echo "<span class=\"date_span\">" . date('h:i a', strtotime($appiontment[$j]->startDateTime)) . "</span>";
                            echo "<br />";
                            echo "<span class=\"line\"></span>";
                            echo "</li>";
                            $j++;
                        }
                    }
                    else
                        echo 'No Appointment available';
                ?>
            </ul>
            </br>
            <?php if ($appiontmentCount >= 5)
            {
                ?>
                <div align="right"><a href="../calender/sample.php" class="more">View more</a></div>
            <?php } ?>
        </section>
    </div>
    <!--<div class="empty"></div>
              <section>
                  <a href="../contact/view_all_contacts.php"><p class="col_header"><img src="../../images/HotContacts-icon.png" /> Hot Contacts </p></a>
                  <ul>
                   <?php
        /* if(!(empty($contacts)))
        {
                 $i=0;
                while($i<=4)
                    {
                        $id=$contacts[$i]->contactId;
                        if($i%2==0)
                      {
                            echo "<li class=\"col_row\">";
                      }
                      else
                      {
                          echo "<li class=\"col_row_alter\">";
                      }
                        echo "<span class=\"title\"><a href=../contact/contact_detail.php?contactId=".$id.">".$contacts[$i]->firstName." ".$contacts[$i]->lastName ."</a></span>";
                        echo "<br />";
                        echo "<span class=\"line\"></span>";
                        echo "</li>";

                        $i++;
                    }
        }
        else
        echo 'No Contact available
';*/
    ?>
                 
                  </ul>
                  </br>
                  <?php /* if($contactsCount >=5){*/ ?>
                  	<div align="right"><a href="../contact/view_all_contacts.php" class="more">View more</a></div>
          		  <?php /*}*/ ?>
          </section>
               <div class="empty"></div>-->
</div>
<div class="col_two">
<section>
    <a href="../task/tasks_view.php"><p class="col_header"><img src="../../images/To Do List-icon.png"/><span
                style="width:142px; display:inline-block;">To Do</span></p></a>
    <ul>
        <?php
            if (!empty($tasks) || !empty($tasksOver) || !empty($tasksUpcoming))
            {
                $i = 0;
                foreach ($tasks as $ts1)
                {
                    $id = $ts1->taskId;
                    $contactId = $task->getcontactId($id);
                    if (!empty($contactId))
                    {
                        if ($i % 2 == 0)
                        {
                            echo "<li class=\"col_row\">";
                        }
                        else
                        {
                            echo "<li class=\"col_row_alter\">";
                        }
                        echo "<span class=\"title\" style=\"width:162px;\"><a href=../task/edit_task.php?task_id=" . $id . ">";
                        if (strlen($ts1->title) > 20)
                            echo substr($ts1->title, 0, 20) . "...";
                        else
                            echo $ts1->title;
                        echo "</a></span>";
                        echo "<span class=\"date_span\" style=\"float: left; margin-left:5px;\">";
                        $total = sizeof($contactId);
                        $j = 1;
                        foreach ($contactId as $ci)
                        {
                            //$task_contact=$task->taskContact($ci->contactId);
                            if ($j < 2)
                            {
                                echo "<a href=../contact/contact_detail.php?contactId=" . $ci->contactId . ">" . $ci->firstName . " " . $ci->lastName . "</a>";
                                if ($j < $total)
                                    echo ',';
                                $j++;
                            }
                            else
                            {
                                echo '...';
                                break;
                            }

                        }
                        echo "</span>";
                        echo "<span class=\"date_span\">" . date("m/d/Y", strtotime($ts1->dueDateTime)) . "</span>";
                        echo "<br />";
                        echo "<span class=\"line\"></span>";
                        echo "</li>";
                        $i++;
                    }
                    else
                    {
                        $flag = false;
                    }

                }
                if ($tasksCount < 15)
                {
                    foreach ($tasksOver as $ts2)
                    {
                        $id = $ts2->taskId;
                        $contactId = $task->getcontactId($id);
                        if (!empty($contactId))
                        {
                            if ($i % 2 == 0)
                            {
                                echo "<li class=\"col_row\">";
                            }
                            else
                            {
                                echo "<li class=\"col_row_alter\">";
                            }
                            echo "<span class=\"title\" style=\"width:162px;\"><a href=../task/edit_task.php?task_id=" . $id . ">";
                            if (strlen($ts2->title) > 20)
                                echo substr($ts2->title, 0, 20) . "...";
                            else
                                echo $ts2->title;
                            echo "</a></span>";
                            echo "<span class=\"date_span\" style=\"float: left; margin-left:5px;\">";
                            $total = sizeof($contactId);
                            $j = 1;
                            foreach ($contactId as $ci)
                            {
                                if ($j < 2)
                                {
                                    echo "<a href=../contact/contact_detail.php?contactId=" . $ci->contactId . ">" . $ci->firstName . " " . $ci->lastName . "</a>";
                                    if ($j < $total)
                                        echo ',';
                                    $j++;
                                }
                                else
                                {
                                    echo '...';
                                    break;
                                }
                            }
                            echo "</span>";
                            echo "<span class=\"date_span\" >" . date("m/d/Y", strtotime($ts2->dueDateTime)) . "</span>";
                            echo "<br />";
                            echo "<span class=\"line\"></span>";
                            echo "</li>";
                            $i++;
                            if ($i > 14)
                                break;
                        }
                        else
                        {
                            $flag = false;
                        }
                    }

                }
                if ($taskTotal < 15 && $taskTotal < $totalRecords)
                {
                    foreach ($tasksUpcoming as $ts3)
                    {
                        $id = $ts3->taskId;
                        $contactId = $task->getcontactId($id);
                        if (!empty($contactId))
                        {
                            if ($i % 2 == 0)
                            {
                                echo "<li class=\"col_row\">";
                            }
                            else
                            {
                                echo "<li class=\"col_row_alter\">";
                            }
                            echo "<span class=\"title\" style=\"width:162px;\"><a href=../task/edit_task.php?task_id=" . $id . ">";
                            if (strlen($ts3->title) > 20)
                                echo substr($ts3->title, 0, 20) . "...";
                            else
                                echo $ts3->title;
                            echo "</a></span>";
                            echo "<span class=\"date_span\" style=\"float: left;  margin-left:5px;\">";
                            $total = sizeof($contactId);
                            $j = 1;
                            foreach ($contactId as $ci)
                            {
                                if ($j < 2)
                                {
                                    echo "<a href=../contact/contact_detail.php?contactId=" . $ci->contactId . ">" . $ci->firstName . " " . $ci->lastName . "</a>";
                                    if ($j < $total)
                                        echo ',';
                                    $j++;
                                }
                                else
                                {
                                    echo '...';
                                    break;
                                }
                            }
                            echo "</span>";
                            echo "<span class=\"date_span\" >" . date("m/d/Y", strtotime($ts3->dueDateTime)) . "</span>";
                            echo "<br />";
                            echo "<span class=\"line\"></span>";
                            echo "</li>";
                            $i++;
                            if ($i > 14)
                                break;
                        }
                        else
                        {
                            $flag = false;
                        }
                    }

                }
            }
            else if ((empty($tasks) && empty($tasksUpcoming) && empty($tasksOver)))
            {
                echo 'No Task available';
            }
        ?>
    </ul>
</section>
</br>
<?php if ($totalTasks > 15)
{
    ?>
    <div align="right"><a href="../task/tasks_view.php" class="more">View more</a></div>
<?php } ?>
<div class="empty"></div>
<!--<section>
   <p class="col_header"><img src="images/Communication-icon.png" /> Communication </p>
   <ul>
   <li class="col_row">
       <span class="title"><img src="images/email.png" /> Alastair Baron</span>
        <br />
       <span class="todo_text">Email Subject</span>
       <br />
       <span class="line"></span>
   </li>


    <li class="col_row_alter">
       <span class="title"><img src="images/FB.png" /> Justin Grey</span>
        <br />
       <span class="todo_text">Facebook Message Here</span>
       <br />
       <span class="line"></span>
   </li>

   <li class="col_row">
       <span class="title"><img src="images/FB.png" /> Emily Cronie</span>
        <br />
       <span class="todo_text">Facebook Message Here</span>
       <br />
       <span class="line"></span>
   </li>

   </ul>
</section>
<div class="empty"></div>-->
</div>
<div class="col_three">
    <section>
        <p class="col_header links">Quick Links</p>
        <ul><a href="http://www.zenplify.biz/support" target="_blank">Zen Help Desk</a></br>
            <a href="http://programs.zenplify.biz/zenplify-accelerator-pack/" target="_blank">Zenplify Accelerator Pack</a>
            <?php
                foreach ($quicklink as $ql)
                {
                    ?>
                    <li><a href="<?php echo $ql->link; ?>" target="_blank"
                           title="<?php echo $ql->title; ?>"><?php echo $ql->title; ?></a></li><?php
                }
            ?>
            <!--<li><a href="http://virtuallyenvps.com/" target="_blank">VENVPs Training Site</a></li>
            <li><a href="https://www.facebook.com/groups/VirtuallyENVPs" target="_blank">VENVPs Facebook Group </a></li>
            <li><a href="https://www.dropbox.com/sh/w5udjnzpxijxkn2/gCeyfShkVf" target="_blank">VENVPs Workbook</a></li>
            <li><a href="https://www.dropbox.com/sh/3hg2y9m7oo7kzgy/kcMy2fwCE7" target="_blank">LAUNCH to VP!!</a></li>
            <li><a href="https://www.dropbox.com/sh/2lwf820z19l99tu/Ngj7sSLrPW" target="_blank">Global Resources</a></li>
            <li><a href="https://www.dropbox.com/sh/wdsbwle547n3a1e/TKjTitKrQx" target="_blank">Mailing Labels & Sample Inserts</a></li>
            <li><a href="https://www.dropbox.com/sh/yfxe465tumr6rx2/HzKfaqh1Wf" target="_blank">Tracking Sheets</a></li>
            <li><a href="https://www.dropbox.com/sh/8hcvlkda6i8niin/-ImSzu-yVB" target="_blank">Sample Options</a></li>
            <li><a href="https://www.dropbox.com/sh/nfejf04qbg69v9w/_V-DdWZiqL" target="_blank">New PC Packages</a></li>
            <li><a href="https://www.dropbox.com/sh/2idlwopuexgod98/yshh5b0-09" target="_blank">New CNS Packages</a></li>
            <li><a href="https://www.dropbox.com/sh/5wyki0zr0cztpa4/JNG-7RCUhl" target="_blank">New CNS Checklist + Tracking</a></li>-->
        </ul>
    </section>
</div>

<?php
    if ($userId != $leaderId)
    {
        ?>
        <div class="col_one" style="clear: both; width:80%;">
            <div>
                <section>
                    <a href="#"><p class="col_header"><img src="../../images/Appointment-icon.png"/> Notifications </p>
                    </a>
                    <ul>
                        <?php
                            if ($usernotification)
                            {
                                $notificationcount = 0;
                                foreach ($usernotification as $un)
                                {

                                    if ($usernotification % 2 == 0)
                                    {
                                        echo "<li class=\"col_row\" id=\"notification_$un->userstatusId\">";
                                    }
                                    else
                                    {
                                        echo "<li class=\"col_row_alter\" id=\"notification_$un->userstatusId\">";
                                    }


                                    echo "<span class='title'><a href='#'>";
                                    if (strlen($un->textToShow) > 40)
                                    {
                                        //echo $un->textToShow;
                                        echo substr($un->textToShow, 0, 100) . "...";
                                    }
                                    else
                                    {
                                        echo $un->textToShow;
                                    }
                                    echo "</a></span>";
                                    if($un->tablename =='plansteps')
                                    {
                                        echo "<span class='date_span_ notificationStatus'>
								<a href=\"javascript:confirmForstep($un->userstatusId)\" id='confirm_" . $un->userstatusId . "'>Confirm</a> | ";
                                    }
                                    else
                                    {
                                        echo "<span class='date_span_ notificationStatus'>
								<a href=\"javascript:requestConfirm($un->userstatusId)\" id='confirm_" . $un->userstatusId . "'>Confirm</a> | ";
                                    }
                                    echo "<a href=\"javascript:requestNotNow($un->userstatusId)\" id='notnow_" . $un->userstatusId . "' >Not Now</a>  </span>";
                                    echo "<br />";
                                    echo "<span class=\"line\"></span>";
                                    echo "</li>";
                                    $$usernotification++;
                                }


                            }
                            else
                            {
                                echo 'No Notification Avaliable!';
                            }?>
                    </ul>
                </section>
            </div>
            <!--<div class="empty"></div>
              <section>
                  <a href="../contact/view_all_contacts.php"><p class="col_header"><img src="../../images/HotContacts-icon.png" /> Hot Contacts </p></a>
                  <ul>
                   <?php
                /* if(!(empty($contacts)))
                {
                         $i=0;
                        while($i<=4)
                            {
                                $id=$contacts[$i]->contactId;
                                if($i%2==0)
                              {
                                    echo "<li class=\"col_row\">";
                              }
                              else
                              {
                                  echo "<li class=\"col_row_alter\">";
                              }
                                echo "<span class=\"title\"><a href=../contact/contact_detail.php?contactId=".$id.">".$contacts[$i]->firstName." ".$contacts[$i]->lastName ."</a></span>";
                                echo "<br />";
                                echo "<span class=\"line\"></span>";
                                echo "</li>";

                                $i++;
                            }
                }
                else
                echo 'No Contact available
            ';*/
            ?>

                  </ul>
                  </br>
                  <?php /* if($contactsCount >=5){*/ ?>
                  	<div align="right"><a href="../contact/view_all_contacts.php" class="more">View more</a></div>
          		  <?php /*}*/ ?>
          </section>
               <div class="empty"></div>-->
        </div>
    <?php } ?>
</div>
<div class="empty"></div>
<div class="empty"></div>
<div class="empty"></div>
<div class="empty"></div>
<div class="empty"></div>
<!-- <p class="separator"></p>
  <div class="footer">-->
<?php
    include_once("../headerFooter/footer.php");
?>
   
     
  