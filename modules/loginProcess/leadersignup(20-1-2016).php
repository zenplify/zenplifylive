<?php
	include_once '../../classes/init.php';
	require_once '../../classes/register.php';
	require_once '../../classes/email.php';

	//$payment=new payment();

	$register = new register();
	$timezone = $register->getTimeZone();
	$action = $_REQUEST['action'];

	if ($action == "failure")
	{
		$message = "This Username is Unavailable. Please try another one.";
		$message1 = " This Email Address already exists.";
	}
	//if (isset($_POST["submit"]) && $_POST['usercheck']==1 && $_POST['emailcheck']==1) {
	if (isset($_POST["submit"]))
	{
		$checkemailusername = $register->SignupCheckForUser($_POST['username'], $_POST['email']);
		if ($checkemailusername == 1)
		{
			sendRedirect("leadersignup.php?action=failure");
		}
		else
		{
			$result = $register->RegisterUser($_POST);
			$codeanduserId = explode('/', $result);
			$code = $codeanduserId[0];
			$userId = $codeanduserId[1];
			$register->addLeaderDefaultFields($userId);
			if (!empty($code))
			{
				$to = $_POST['email']; //to address
				$subject = "Welcome to Zenplify!";
				$message = "<br />Hi " . $_POST['firstname'] . "," . "<br /><br />Welcome to Zenplify!" . "<br /><br />Your username is " . $_POST['username'] . ". " . "<br /><br />(Click <a href=\"https://zenplify.biz/signin.php\"> Here </a> if you need help with your password)" . "<br /><br />" . "Please click the link below to verify your email address and activate your account.<br /><br />" . "https://zenplify.biz/modules/loginProcess/login_check.php?code=" . $code . "<br /><br />
			If you click the link and it appears to be broken, copy and paste it into a new browser window. Make sure to copy and paste the whole URL to ensure it works properly.<br /><br />Thanks for using Zenplify!<br /><br />Sincerely,<br /><br />The Zenplify Team";
				$from = "<support@zenplify.biz>";
				$headers = "From:" . $from . "\r\n";
				$headers .= "Content-Type: text/html";
				$sent = mail($to, $subject, $message, $headers);
				if ($sent == 1)
				{
					//$payment->userPayments(1,$userId);//userPayments(planId,userId)
					//sendRedirect("../../index.php?signup=signup");
				}
//				sendRedirect("home.php");
				//sendRedirect("../../signup.php");
				SendRedirect("paymentplans.php?user_id=".$userId);
				//exit;
				//sendredirect('main.php?code='.$pin);
			}
			//timeZoneId='.$timeZoneId.',
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Zenplify</title>
	<link rel="stylesheet" href="../../css/style.css" type="text/css">
	<script type="text/javascript" src="../../js/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../../js/script.js"></script>
	<script type="text/javascript" src="../../validation/lib/jquery.js"></script>
	<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function (e) {
			$("#email").focus(function (e) {
				$("#messageEmail").html('');
				messageUser

			});
			$("#username").focus(function (e) {
				$("#messageUser").html('');
			});
			$('#web').blur(function (e) {
				var web = $('#web').val();
				var result = web.replace(/.*?:\/\//g, "");
				$('#web').val(result);
			});
			$('#facebook_page').blur(function (e) {
				var web = $('#facebook_page').val();
				var result = web.replace(/.*?:\/\//g, "");
				$('#facebook_page').val(result);
			});
			$('#facebook_personalpage').blur(function (e) {
				var web = $('#facebook_personalpage').val();
				var result = web.replace(/.*?:\/\//g, "");
				$('#facebook_personalpage').val(result);
			});
			/*$('#facebook_partypage').blur(function(e) {
			 var web=$('#facebook_partypage').val();
			 var result = web.replace(/.*?:\/\//g, "");
			 $('#facebook_partypage').val(result);
			 });   */
		});
		window.onload = function () {
			var myInput = document.getElementById('emailConfirm');
			myInput.onpaste = function (e) {
				e.preventDefault();
			}
		}
		function checkIfAlredyExist() {
			var emailcheck = document.getElementById('emailcheck').value;
			var usercheck = document.getElementById('usercheck').value;
			var userSpecialcheck = document.getElementById('userSpecialcheck').value;
			if (emailcheck == 0 || usercheck == 0 || userSpecialcheck == 0) {
				if (userSpecialcheck == 0) $("#username").focus();
				return false;
			}
			else {
				return true;
			}

//			var emailcheck = document.getElementById('emailcheck').value;
//			var usercheck = document.getElementById('usercheck').value;
//			alert('value of usecheck us '+usercheck);
//			if (emailcheck == 0 || usercheck == 0) {
//				alert('in check');
//				return false;
//			} else {
//				alert('in not check');
//				return true;
//			}
		}

        function cancelPage(){
            window.location.replace("https://zenplify.biz/signup.php");
        }
	</script>
	<?php require_once("../../classes/resource.php"); ?>
</head>
<body>
<div class="container">
	<div id="top_cont">
		<div id="logo"><img src="../../images/logo.png"/></div>
		<!--<div > <a href="../../signin.php" class="loginLink">Log in here</a></div>-->
	</div>
</div>

<!-- <div id="menu_line"></div>-->
<div class="container">
<div style="float:right; margin-bottom:10px; ">
	<!--<input  class="signin" type="button" value="" name="submit" onclick="window.location.href='../../signup.php'">--></div>
<h1 class="gray" style="padding-left:0px !important;">Leader Registration</h1>

<form name="add_user" id="add_user" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post"
      onsubmit="return checkIfAlredyExist();">
	<table cellspacing="0" cellpadding="0" width="970px">
		<tr>
			<td class="task_col">Leader Information</td>
			<td class="task_col" style="width:80% !important;"><a href="http://techgeese.com/support"
			                                                      target="_blank"
			                                                      style="color:#63F; float:right; margin-right:15px; font-weight:normal; text-decoration:none; font-size:12px;">Help Desk</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<input type="hidden" name="isleader" id="isleader" value="1"/>
		<tr>
			<td class="label" style="width:20% !important;">User Name</td>
			<td style="width:80% !important;">
				<input type="text" name="username" id="username" class="textfield required" value="<?php echo $_POST['username'] ?>"/>
				<label>
					<font class="message" id="messageUser"><?php echo($message == "" ? "" : $message); ?></font>
				</label>
				<label id="msg" class="msg"></label>
				<input type="hidden" name="usercheck" id="usercheck" value="1"/>
				<input type="hidden" name="userSpecialcheck" id="userSpecialcheck" value="1"/>
			</td>
		</tr>
		<tr>
			<td class="label">Password</td>
			<td><input type="password" name="password" class="textfield required" id="password"/></td>
		</tr>
		<tr>
			<td class="label">Confirm Password</td>
			<td><input type="password" name="confirm_password" class="textfield"/></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="task_col">Contact Information</td>
			<td></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="label">Enter your information below exactly as you would like it to appear on your email signature and guest profile pages.
			</td>
		</tr>
		<tr>
			<td class="label">First Name</td>
			<td><input type="text" name="firstname" class="textfield"/></td>
		</tr>
		<tr>
			<td class="label">Last Name</td>
			<td><input type="text" name="lastname" class="textfield"/></td>
		</tr>
		<tr>
			<td class="label">Email Address</td>
			<td>
				<input type="text" name="email" class="textfield" id="email"/><label><font class="message"
				                                                                           id="messageEmail"><?php echo($message1 == "" ? "" : $message1); ?></font></label>
				<!-- <span style="color:#FF3300;font-size:12px;"> --><!-- <p style="color:#009933;font-size:12px;"> -->
				<label id="msge" class="msg"></label> <input type="hidden" name="emailcheck" id="emailcheck" value="1"/>
			</td>
		</tr>
		<tr>
			<td class="label">Confirm Email Address</td>
			<td>
				<input type="text" name="emailConfirm" class="textfield" id="emailConfirm"/>
				<!-- <span style="color:#FF3300;font-size:12px;"> --><!-- <p style="color:#009933;font-size:12px;"> -->
			</td>
		</tr>
		<tr>
			<td class="label">Independent Consultant ID</td>
			<td><input type="text" name="consultant_id" class="textfield"/></td>
		</tr>
		<tr>
			<td class="label">Title</td>
			<td><input type="text" name="title" class="textfield" style="float:left;"/>

				<div style="margin-top:4px; font-size:12px; color:#555555;">District Manager, Area Manager, etc
				</div>
			</td>
		</tr>
		<tr>
			<td class="label">Web Address</td>
			<td><input type="text" name="web" class="textfield url" style="float:left;" id="web"/>

				<div style="margin-top:4px; float:left; font-size:12px;color:#555555; margin-right:19px;">
					www.example.com
				</div>
			</td>
		</tr>
		<tr>
			<td class="label">Facebook Page</td>
			<td><input type="text" name="facebook_page" class="textfield url" style="float:left;"
			           id="facebook_page"/>

				<div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:97px;">
					www.facebook.com/your-page-name
				</div>
			</td>
		</tr>
		<!--<tr>
	  <td class="label">Facebook Personal Profile Link</td>
	<td><input type="text" name="facebook_personalpage" class="textfield url" id="facebook_personalpage" style="float:left;" /><div style="margin-top:4px; float:left; font-size:12px;color:#555555; margin-right:135px;">www.facebook.com/personalpage</div></td>
  </tr>--><!--<tr>
                <td class="label">Facebook Party Group Link</td>
              <td><input type="text" name="facebook_partypage" class="textfield" style="float:left;" id="facebook_partypage" /><div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;">www.facebook.com/groups/partyaddress</div></td>
            </tr>-->
		<tr>
			<td class="label">Time Zone</td>
			<td>
				<select name="timezone" class="SelectField">
					<?php
						foreach ($timezone as $tz)
						{
							echo '<option value="' . $tz->timeZoneId . '">' . $tz->zoneTitle . ' (GMT ' . $tz->GMTDiff . ':00 )</option>';
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="label">Phone Number</td>
			<td><input type="text" name="phonenumber" class="textfield" style="float:left;"/>

				<div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;"> Format your number the way you want it to show in your signature
				</div>
			</td>
		</tr>
		<!--<tr>
	  <td class="label">Which Discover Arbonne video do you want in your account?</td>
	<td><select class="SelectField" name="videoOption" style="margin-right:35px;"><option value="">Select Video</option><option value="US" selected="selected">Heather Mitchell (US)</option>
	<option value="AUS">Dinah Williams (AUS)</option></select></td>
  </tr>-->
		<tr>
			<td colspan="2" class="task_col"></td>
			<td></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="label" style="padding-left:0px !important;">
				<input type="checkbox" name="termandconditions" class="termandconditions" style="float:left; margin-left:12px;"/>
				<label style="display: inline-block;height: 12px; margin-right:30px;margin-left: 10px; float:left; line-height:17px;">
					Please accept our
					<a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;" onclick="loadform()">Terms of Service</a> &
					<a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;" onclick="loadform1()">Privacy Policy</a>
				</label>
			</td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td><input type="submit" name="submit" class="create_account" value=""/> <input type="button" name="cancel" class="cancel" value="" onclick="cancelPage()"/></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<?php include("../headerFooter/footer.php"); ?>
<div id="Popup"></div>
<div id="popupQuickquote">
	<h1 class="gray">Terms of Service </h1>
    <?php include('termofUse.html'); ?>
<!--	<ol>-->
<!--		<li> You must be 13 years or older to use this Service.</li>-->
<!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
<!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.-->
<!--		</li>-->
<!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.-->
<!--		</li>-->
<!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
<!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
<!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).-->
<!--		</li>-->
<!--	</ol>-->
</div>
<div id="popupQuickquote1">
	<h1 class="gray">Privacy Policy</h1>
    <?php include('termofUse.html'); ?>
<!--	<ol>-->
<!--		<li> You must be 13 years or older to use this Service.</li>-->
<!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
<!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.-->
<!--		</li>-->
<!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.-->
<!--		</li>-->
<!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
<!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
<!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).-->
<!--		</li>-->
<!--	</ol>-->
</div>