<?php
include '../../classes/init.php'; 

session_start();
?> 
 
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	 <title>Zenplify</title>
    	 <link rel="stylesheet" href="../../css/style.css" type="text/css">
         <script src="../../js/jquery.min.js" type="text/javascript"></script>
	<script>
    $(document).ready(function(e) {
        $('.options').click(function(e) {
			
            var buttonVal=$(this).val();
			 $("input[type=button]").attr("disabled", "disabled");
			if(buttonVal=='Yes')
			{
				
				window.location.href='paypal.php?task=ConfirmCheckout&payerId=<?php echo $_GET['payerId']; ?>&token=<?php echo $_GET['token']; ?>&user_id=<?php echo $_GET['user_id']; ?>&planId=<?php echo $_GET['planId']; ?>';
			}
			else
			{
				window.location.href='cancel.php';
			}
			
        });
    });
    </script>
	
  </head>
  
  <body>
  <div align="center" id="index_logo"><img src="../../images/logo.png" />
 <div id="login_container" style="margin-top:20px;">
 <?php if(isset($_REQUEST['payerExists'])){?>
 <h2 style="color:#555555;">Payment Confirmation!</h2>
 <p style="color:#555555; font-size:12px;">
 This paypal account is already associated with <?php echo $_REQUEST['payerExists']; ?> Zenplify account(s). Do you want to associate it with this account as well?
 <form>
 <span><input type="button" name="yes" value="Yes" class="options" /> &nbsp; <input type="button" name="no" value="No" class="options"   /> </span>
 </form> </p>
 
 <?php }else {?>
  <span style="color:#555555; font-size:12px;">Your Paypal transaction has been cancelled . Please click  <a href="../../signup.php" style="text-decoration:none; font-weight:bold; color:#555555; "> here</a> to Login.</span>
   <?php }?>
</div>

</div>
  
