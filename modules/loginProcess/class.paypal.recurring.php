<?php

    //require_once ("../../classes/payment.php");
    class paypal_recurring
    {

        function setExpressCheckout()
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            $description = $this->description;
            $startAmount = $this->startAmount;
            // Add request-specific fields to the request string.
            $nvpStr = "&Amt=1&ReturnUrl=$returnURL&CANCELURL=$cancelURL&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID&NOSHIPPING=1&BILLINGTYPE=RecurringPayments&BILLINGAGREEMENTDESCRIPTION=$description"; //&L_BILLINGAGREEMENTDESCRIPTION0=$description
            // Execute the API operation; see the PPHttpPost function above.
            $httpParsedResponseAr = $this->fn_setExpressCheckout('SetExpressCheckout', $nvpStr);
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                // Redirect to paypal.com.
                $token = urldecode($httpParsedResponseAr["TOKEN"]);
                $payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
                if("sandbox" === $environment || "beta-sandbox" === $environment)
                {
                    $payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&token=$token";
                }
                header("Location: $payPalURL");
                exit;
            }
            else
            {
                exit('SetExpressCheckout failed: '.print_r($httpParsedResponseAr, true));
            }

        }

        function getExpressCheckout($userId)
        {
            echo "getexpress checkout";
            $p1 = new payment();
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            $planId = $this->planId;
            // Obtain the token from PayPal.
            if(!array_key_exists('token', $_REQUEST))
            {
                exit('Token is not received.');
            }
            // Set request-specific fields.
            $token = urlencode(htmlspecialchars($_REQUEST['token']));
            // Add request-specific fields to the request string.
            $nvpStr = "&TOKEN=$token";
            // Execute the API operation; see the PPHttpPost function above.
            $httpParsedResponseAr = $this->fn_getExpressCheckout('GetExpressCheckoutDetails', $nvpStr);
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                // Extract the response details.
                $payerID = $httpParsedResponseAr['PAYERID'];
                // Check for multiple $PayerIDs
                /*echo $p1->IsUserExistAgainstPayerIdWithUserId($payerID,$userId)."=======";
                if($p1->IsUserExistAgainstPayerIdWithUserId($payerID,$userId) == 1)
                {
                    echo "exist function";
                    header("Location:paymentplans.php?duplicatepayerid=1&user_id=$userId&payerId=$payerID");
                    exit();
                }*/
                $street1 = $httpParsedResponseAr["SHIPTOSTREET"];
                if(array_key_exists("SHIPTOSTREET2", $httpParsedResponseAr))
                {
                    $street2 = $httpParsedResponseAr["SHIPTOSTREET2"];
                }
                $city_name = $httpParsedResponseAr["SHIPTOCITY"];
                $state_province = $httpParsedResponseAr["SHIPTOSTATE"];
                $postal_code = $httpParsedResponseAr["SHIPTOZIP"];
                $country_code = $httpParsedResponseAr["SHIPTOCOUNTRYCODE"];
                $ifPayerExists = $p1->IsPayerIdExists($payerID, $userId);
                if($ifPayerExists>0)
                {       echo 'Zen001';
                    header("Location:cancel.php?payerExists=".$ifPayerExists."&user_id=$userId&payerId=$payerID&token=$token&planId=$planId");
                    exit;
                }
                else
                {
                    $this->atferConfirmation($payerID, $token, $userId);

                }
                //header("Location:final.php?AfterCallingDoExpressCheckOut");
                //exit('Get Express Checkout Details Completed Successfully: '.print_r($httpParsedResponseAr, true));
            }
            else
            {   echo 'Zen002';
                header("Location:cancel.php?".print_r($httpParsedResponseAr, true));
                exit;
                //exit('GetExpressCheckoutDetails failed: ' . print_r($httpParsedResponseAr, true));
            }

        }

        function atferConfirmation($payerID, $token, $userId)
        {
            $p1 = new payment();
            $p1->AssignPaymentIdToUser($payerID, $userId);
            $this->doExpressCheckout($payerID, $token, $userId);
        }

        function doExpressCheckout($payerID, $token, $userId)
        {
            echo "do";
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startAmount = $this->startAmount;
            // Add request-specific fields to the request string.
            $nvpStr = "&TOKEN=$token&PAYERID=$payerID&PAYMENTACTION=$paymentType&AMT=1&CURRENCYCODE=$currencyID&CUSTOM=1123";
            echo 'doExpressCheckout-'.$nvpStr;
            // Execute the API operation; see the PPHttpPost function above.
            $httpParsedResponseAr = $this->fn_doExpressCheckout('DoExpressCheckoutPayment', $nvpStr);
            echo 'resposnse<br>';
            print_r($httpParsedResponseAr);
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                $this->createRecurringPaymentsProfile($token, $userId);
                //header("Location:final.php?AfterCallingCreateRecurringProfile");
                //exit('Express Checkout Payment Completed Successfully: '.print_r($httpParsedResponseAr, true));
            }
            else
            {   echo 'Zen003';
                header("Location:cancel.php?".print_r($httpParsedResponseAr, true));
                exit;
                //exit('DoExpressCheckoutPayment failed: ' . print_r($httpParsedResponseAr, true));
            }
        }

        function createRecurringPaymentsProfile($token, $userId)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            $billingPeriod = $this->billingPeriod;
            $billingFreq = $this->billingFreq;
            $startAmount = $this->startAmount;
            $token = $_REQUEST['token'];
            $nvpStr = "&TOKEN=$token&AMT=$paymentAmount&CURRENCYCODE=$currencyID&PROFILESTARTDATE=$startDate";
            $nvpStr .= "&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=$billingFreq&INITAMT=$startAmount";
            $httpParsedResponseAr = $this->fn_createRecurringPaymentsProfile('CreateRecurringPaymentsProfile', $nvpStr);
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                // Add user's Paypal profile
                $p1 = new payment();
                $p1->addPaypalProfile($userId, $httpParsedResponseAr["PROFILEID"]);
                header("Location:final.php");
                //exit('CreateRecurringPaymentsProfile Completed Successfully: '.print_r($httpParsedResponseAr, true));
            }
            else
            {   echo 'Zen004';
                header("Location:cancel.php?".print_r($httpParsedResponseAr, true));
                exit;
                //exit('CreateRecurringPaymentsProfile failed: ' . print_r($httpParsedResponseAr, true));
            }

        }

        function fn_createRecurringPaymentsProfile($methodName_, $nvpStr_)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            $description = $this->description;
            $version = urlencode('51.0');
            if("sandbox" === $environment || "beta-sandbox" === $environment)
            {
                $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
            }
            // setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // turning off the server and peer verification(TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // NVPRequest for submitting to server
            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_&DESC=$description";
            // setting the nvpreq as POST FIELD to curl
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
            // getting response from server
            $httpResponse = curl_exec($ch);
            if(!$httpResponse)
            {      echo 'Zen005';
                //exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
                header("Location:cancel.php?$methodName_ failed:".curl_error($ch).'('.curl_errno($ch).')');
                exit;
            }
            // Extract the RefundTransaction response details
            $httpResponseAr = explode("&", $httpResponse);
            $httpParsedResponseAr = array();
            foreach($httpResponseAr as $i => $value)
            {
                $tmpAr = explode("=", $value);
                if(sizeof($tmpAr)>1)
                {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }
            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr))
            {   echo 'Zen006';
                //exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                header("Location:cancel.php?1-Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                exit;
            }
            return $httpParsedResponseAr;
        }

        function cancelRecurringPaymentsProfile($profileId, $userId)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $nvpStr = "&PROFILEID=$profileId&ACTION=Cancel";
            $httpParsedResponseAr = $this->fn_createRecurringPaymentsProfile('ManageRecurringPaymentsProfileStatus', $nvpStr);
            print_r($httpParsedResponseAr);
            $p1 = new payment();
            $p1->deletePaypalProfile($userId);
            echo 'Zen007';
            header("Location:edit_user.php?profileDeleted=1");
            exit;
        }

        function cancelRecurringPaymentsProfileandProfile($profileId, $userId)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $nvpStr = "&PROFILEID=$profileId&ACTION=Cancel";
            $httpParsedResponseAr = $this->fn_createRecurringPaymentsProfile('ManageRecurringPaymentsProfileStatus', $nvpStr);
            print_r($httpParsedResponseAr);
            $p1 = new payment();
            $p1->deletePaypalProfile($userId);
            //header("Location:edit_user.php?profileDeleted=1");
        }

        function fn_cancelRecurringPaymentsProfile($methodName_, $nvpStr_)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            $description = $this->description;
            $version = urlencode('51.0');
            if("sandbox" === $environment || "beta-sandbox" === $environment)
            {
                $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
            }
            // setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // turning off the server and peer verification(TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // NVPRequest for submitting to server
            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
            // setting the nvpreq as POST FIELD to curl
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
            // getting response from server
            $httpResponse = curl_exec($ch);
            if(!$httpResponse)
            {   echo 'Zen008';
                //exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
                header("Location:cancel.php?$methodName_ failed:".curl_error($ch).'('.curl_errno($ch).')');
                exit;
            }
            // Extract the RefundTransaction response details
            $httpResponseAr = explode("&", $httpResponse);
            $httpParsedResponseAr = array();
            foreach($httpResponseAr as $i => $value)
            {
                $tmpAr = explode("=", $value);
                if(sizeof($tmpAr)>1)
                {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }
            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr))
            {
                //exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                echo 'Zen009';
                header("Location:cancel.php?1-Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                exit;
            }
            return $httpParsedResponseAr;
        }

        function fn_setExpressCheckout($methodName_, $nvpStr_)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            if("sandbox" === $environment || "beta-sandbox" === $environment)
            {
                $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
            }
            $version = urlencode('51.0');
            // Set the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // Turn off the server and peer verification (TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // Set the API operation, version, and API signature in the request.
            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
            // Set the request as a POST FIELD for curl.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
            // Get response from the server.
            $httpResponse = curl_exec($ch);
            if(!$httpResponse)
            {       echo 'Zen0010';
                //exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
                header("Location:cancel.php?$methodName_ failed:".curl_error($ch).'('.curl_errno($ch).')');
                exit;
            }
            // Extract the response details.
            $httpResponseAr = explode("&", $httpResponse);
            $httpParsedResponseAr = array();
            foreach($httpResponseAr as $i => $value)
            {
                $tmpAr = explode("=", $value);
                if(sizeof($tmpAr)>1)
                {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }
            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr))
            {   echo 'Zen0011';
                //exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                header("Location:cancel.php?2-Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                exit;
            }
            return $httpParsedResponseAr;
        }

        function fn_getExpressCheckout($methodName_, $nvpStr_)
        {
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            if("sandbox" === $environment || "beta-sandbox" === $environment)
            {
                $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
            }
            $version = urlencode('51.0');
            // Set the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // Turn off the server and peer verification (TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // Set the API operation, version, and API signature in the request.
            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
            // Set the request as a POST FIELD for curl.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
            // Get response from the server.
            $httpResponse = curl_exec($ch);
            if(!$httpResponse)
            { echo 'Zen0012';
                //exit('$methodName_ failed: '.curl_error($ch).'('.curl_errno($ch).')');
                header("Location:cancel.php?$methodName_ failed:".curl_error($ch).'('.curl_errno($ch).')');
                exit;
            }
            // Extract the response details.
            $httpResponseAr = explode("&", $httpResponse);
            $httpParsedResponseAr = array();
            foreach($httpResponseAr as $i => $value)
            {
                $tmpAr = explode("=", $value);
                if(sizeof($tmpAr)>1)
                {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }
            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr))
            {       echo 'Zen0013';
                //exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                header("Location:cancel.php?3-Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                exit;
            }
            return $httpParsedResponseAr;

        }

        function fn_doExpressCheckout($methodName_, $nvpStr_)
        {
            echo "do1";
            $environment = $this->environment;
            $API_UserName = $this->API_UserName;
            $API_Password = $this->API_Password;
            $API_Signature = $this->API_Signature;
            $API_Endpoint = $this->API_Endpoint;
            $paymentAmount = $this->paymentAmount;
            $currencyID = $this->currencyID;
            $paymentType = $this->paymentType;
            $returnURL = $this->returnURL;
            $cancelURL = $this->cancelURL;
            $startDate = $this->startDate;
            if("sandbox" === $environment || "beta-sandbox" === $environment)
            {
                $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
            }
            $version = urlencode('51.0');
            // setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // Set the curl parameters.
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // Set the API operation, version, and API signature in the request.
            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
            // Set the request as a POST FIELD for curl.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
            // Get response from the server.
            $httpResponse = curl_exec($ch);
            if(!$httpResponse)
            {   echo 'Zen0014';
                //echo('$methodName_ failed: '.curl_error($ch).'('.curl_errno($ch).')');
                header("Location:cancel.php?$methodName_ failed:".curl_error($ch).'('.curl_errno($ch).')');
                exit;
            }
            // Extract the response details.
            $httpResponseAr = explode("&", $httpResponse);
            $httpParsedResponseAr = array();
            foreach($httpResponseAr as $i => $value)
            {
                $tmpAr = explode("=", $value);
                if(sizeof($tmpAr)>1)
                {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }
            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr))
            {
                echo("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                echo 'Zen0015';
                header("Location:cancel.php?4-Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                exit;
            }
            //echo '---1st success---'.print_r($httpParsedResponseAr);
            return $httpParsedResponseAr;
        }

    }

?>