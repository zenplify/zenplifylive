<?php 
session_start();


require_once ("../../classes/payment.php");
$confirm=new payment();
$userId=$_REQUEST['user_id'];
$ExpiryConfirmation=$confirm->trialPeriod($userId);
$isPaypalVerified=$confirm->isPaypalVerified($userId);

?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	 <title>Zenplify</title>
    	 <link rel="stylesheet" href="../../css/style.css" type="text/css">
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
     

	
  </head>
  
  <body>
  	<div class="container">
      <div id="top_cont">
          <div id="logo"><img src="../../images/logo.png" /></div>
      </div>
 	</div>
  <div class="container">
  <div class="planPeriod">
  <h1 >Choose Your Payment Plan</h1>
  <table class="planPeriod" width="500px">
  <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr>
 <td style="text-align:center; " colspan="2">  <?php 
 if(strtotime($ExpiryConfirmation->expiryDate)<strtotime(date('Y-m-d')) && $isPaypalVerified!=1){
 echo "<b>7-days</b> Free Trial on all Paid Plans. No credit card required.";
 }else{
	echo "Your Account has been Expired. Please choose any Plan to Subscribe for Zenplify"; 
	 }?></td>
  
 </tr>
 
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
 
  <tr>
         <td>
             <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                <input type="image" src="../../images/basic.png" border="0" id="basic" name="submit"    onmouseover="this.src='../../images/basic-t.png';" onmouseout="this.src='../../images/basic.png';" >
                 <input type="hidden" name="cmd" value="_xclick-subscriptions">
                <input type="hidden" name="business" value="demomerchant@suavesolutions.net"> 
                <input type="hidden" name="item_name" value="Basic"> 
                <input type="hidden" name="item_number" value="1"> 
                <input type="hidden" name="no_note" value="1">
                <input type="hidden" name="no_shipping" value="1">
                <input type="hidden" name="currency_code" value="USD"> 
                <?php if(strtotime($ExpiryConfirmation->expiryDate)<strtotime(date('Y-m-d')) && $isPaypalVerified!=1){?>
					
					
                <input type="hidden" name="a1" value="0"> 
                <input type="hidden" name="p1" value="1"> 
                <input type="hidden" name="t1" value="W"> 
                <?php }?>
                <input type="hidden" name="a3" value="20.00"> 
                <input type="hidden" name="p3" value="1"> 
                <input type="hidden" name="t3" value="M"> 
                <input type="hidden" name="src" value="1"> 
                <input type="hidden" name="sra" value="1"> 
                <input type="hidden" name="notify_url" value="http://zenplify.biz/modules/loginProcess/ipn.php">
                <input type="hidden" name="return" value="http://zenplify.biz/modules/loginProcess/final.php">
                <input type="hidden" name="cancel_return" value="http://zenplify.biz/modules/loginProcess/cancel.php">
                <input type="hidden" name="custom" value="<?php echo $userId;?>"> 
            </form>
	</td>
    <td>
        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
            <input type="image" src="../../images/classic.png" border="0" name="submit" onmouseover="this.src='../../images/classic-t.png';" onmouseout="this.src='../../images/classic.png';" >
             <input type="hidden" name="cmd" value="_xclick-subscriptions">
            <input type="hidden" name="business" value="demomerchant@suavesolutions.net"> 
            <input type="hidden" name="item_name" value="Classic"> 
            <input type="hidden" name="item_number" value="2"> 
            <input type="hidden" name="no_note" value="1">
            <input type="hidden" name="no_shipping" value="1">
             
            <input type="hidden" name="currency_code" value="USD">
            <?php if(strtotime($ExpiryConfirmation->expiryDate)<strtotime(date('Y-m-d')) && $isPaypalVerified!=1){?>
					
					
                <input type="hidden" name="a1" value="0"> 
                <input type="hidden" name="p1" value="1"> 
                <input type="hidden" name="t1" value="W"> 
                <?php }?>
            <input type="hidden" name="a3" value="50.00"> 
            <input type="hidden" name="p3" value="3"> 
            <input type="hidden" name="t3" value="M"> 
            <input type="hidden" name="src" value="1"> 
            <input type="hidden" name="sra" value="1"> 
            <input type="hidden" name="notify_url" value="http://zenplify.biz/modules/loginProcess/ipn.php">
            <input type="hidden" name="return" value="http://zenplify.biz/modules/loginProcess/final.php">
            <input type="hidden" name="cancel_return" value="http://zenplify.biz/modules/loginProcess/cancel.php">
            <input type="hidden" name="custom" value="<?php echo $userId;?>"> 
        </form>
     </td>
</tr>
 <!-- <tr>
  <td><input type="button" class="basic" value="" id="basic" onclick="javascript: window.location.href='confirm.php?plantype=basic'" /></td>
  <td><input type="button" class="classic" id="classic" value=""  onclick="javascript: window.location.href='confirm.php?plantype=classic'"/></td>
  </tr>-->
  <tr><td>&nbsp;</td><td class="creditcard">&nbsp;</td></tr>
  </table>

  </div>
  <div class="empty"></div>
  <div class="empty"></div>
  
 
   <?php 
		include_once("../headerFooter/footer.php");
	?>
   
     
  