<?php

session_start();

include '../../classes/init.php'; 
 $code=$_GET['code'];

require_once("../../classes/register.php");

$register=new register();

?> 
 
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	 <title>Zenplify</title>
    	 <link rel="stylesheet" href="../../css/style.css" type="text/css">
	

	
  </head>
  
  <body>
  
<?php
	$isVerifiedEmail = $register->isVerifiedEmail($code);
	if ($isVerifiedEmail == 1)
	{
		sendRedirect('../../signup.php');
	}
	else
	{
		if (!empty($code))
		{
			$register->VerifyUserCode($code);
			$register->createUserGroups($code);
			$register->createUserPlans($code);
			$register->createUserProfiles($code);
            $register->createUserSignature($code);
            $register->addPermissionEmail($code);
			?>
			<div align="center" id="index_logo"><img src="../../images/logo.png"/>

				<div id="login_container" style="margin-top:20px;">
					<span style="color:#555555; font-size:12px;">Your Account has been Activated Successfully. Please click  <a href="../../signup.php" style="text-decoration:none; font-weight:bold; color:#555555; ">here</a> to Login.</span>
				</div>
			</div>
		<?php
		}
	}?>