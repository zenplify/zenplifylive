<?php
    require_once("../../classes/payment.php");
    require_once("../../classes/paypalConfigs.php");
    include "class.paypal.recurring.php";
    $obj = new paypal_recurring;
    $obj->API_UserName = urlencode(PAYUSER);
    $obj->API_Password = urlencode(PAYPASSWORD);
    $obj->API_Signature = urlencode(PAYSIGN);
    //$obj->API_Endpoint = "https://api.sandbox.paypal.com/nvp";
    $obj->API_Endpoint = "https://api-3t.paypal.com/nvp";
    if($_REQUEST['cancelPayment'] == 1)
    {
        $obj->cancelRecurringPaymentsProfile($_REQUEST['profileId'], $_REQUEST['userId']);
        die();
    }
    $confirm = new payment();
    $userId = $_REQUEST['user_id'];
    $planId = $_REQUEST['planId'];
    $ExpiryConfirmation = $confirm->trialPeriod($userId);
    $isPaypalVerified = $confirm->isPaypalVerified($userId);
    $planDetail = $confirm->PlanInformation($planId);
    $trialplanDetail = $confirm->PlanInformation(1);
    if(!empty($planDetail))
    {
        $billingFreq = $planDetail->frequency;
        $paymentAmount = $planDetail->fee;
        $plan = $planDetail->title;
        $startAmount = $planDetail->fee;
        $expirydate = Date('Y-m-d', strtotime($planDetail->expiryType));
        $product_name = $planDetail->product_name;
    }
    if($isPaypalVerified != 1)
    {
        $startAmount = $trialplanDetail->fee;
        $expirydate = Date('Y-m-d', strtotime($trialplanDetail->expiryType));
    }
    $startDate = $expirydate.'T0:0:0';
    //$obj->environment = 'sandbox';	// or 'beta-sandbox' or 'live'
    $obj->environment = 'live';
    //if($startAmount == 1)
    $obj->paymentType = urlencode('Authorization'); // or 'Sale' or 'Order'
    //else
    //	$obj->paymentType = urlencode('Sale');
    // Set request-specific fields.
    $obj->startDate = urlencode($startDate);
    $obj->billingPeriod = urlencode("Month"); // or "Day", "Week", "SemiMonth", "Year"
    $obj->billingFreq = urlencode($billingFreq); // combination of this and billingPeriod must be at most a year
    $obj->paymentAmount = urlencode($paymentAmount);
    $obj->startAmount = urlencode($startAmount);
    $obj->currencyID = urlencode('USD'); // or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
    $obj->planId = $planId;
    /* PAYPAL API  DETAILS */
    /*SET SUCCESS AND FAIL URL*/
    /*$obj->returnURL = urlencode("http://suavesol.net/zenplifybeta/modules/loginProcess/paypal.php?task=getExpressCheckout&user_id=".$userId."&planId=".$planId);
    $obj->cancelURL = urlencode('http://suavesol.net/zenplifybeta/modules/loginProcess/paypal.php?task=error&user_id'.$userId."&planId=".$planId);*/
    $obj->returnURL = urlencode("https://zenplify.biz/modules/loginProcess/paypal.php?task=getExpressCheckout&user_id=".$userId."&planId=".$planId);
    $obj->cancelURL = urlencode('https://zenplify.biz/modules/loginProcess/cancel.php?task=error&user_id'.$userId."&planId=".$planId);
    $obj->description = urlencode($product_name);
    //Zenplify Subscription - Basic/Classic
    if(isset($_GET['task']))
        $task = $_GET['task'];
    else
    {
        $task = "setExpressCheckout"; //set initial task as Express Checkout
        $userDetails = $confirm->getUserById($_REQUEST['user_id']);
        if($userDetails->isPaypalProfileActive == 1)
        {
            $obj->cancelRecurringPaymentsProfileandProfile($userDetails->paypalProfileId, $_REQUEST['user_id']);
        }
    }
    switch($task)
    {
        case "setExpressCheckout":
            $obj->setExpressCheckout();
            exit;
        case "getExpressCheckout":
            $obj->getExpressCheckout($_GET['user_id']);
            exit;
        case "ConfirmCheckout":
            $obj->atferConfirmation($_GET['payerId'], $_GET['token'], $_GET['user_id']);
            exit;
        case "error":
            echo "setExpress checkout failed";
            exit;
    }
?>