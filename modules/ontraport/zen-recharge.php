<meta https-equiv="Pragma" content="no-cache">
<meta https-equiv="no-cache">
<meta https-equiv="Expires" content="-1">
<meta https-equiv="Cache-Control" content="no-cache">
<?php
error_reporting(0);
require_once '../../classes/init.php';
require_once '../../classes/register.php';

$register = new register();
$fname = "";
$lname = "";
$email = "";

if (isset($_GET['token']) && empty($_GET['token']) === false) {
	$token = trim($_GET['token']);
	$user = $register->getUserInfo($token);
	$fname = $user->firstName;
	$lname = $user->lastName;
	$email = $user->email;
} else {
	header('Location: https://zenplify.biz/modules/ontraport/sales.php');
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
	<link rel="shortcut icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
	<title>Zenplify Monthly Subscription</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="https://zenplify.biz/modules/ontraport/css/form-styles.css"/>
</head>
<body>
<div class="container-fluid">
	<header>
		<div class="container" id="header-container">
			<img src="https://zenplify.biz/modules/ontraport/images/logo.png" alt="Zenplify"></div>
	</header>
	<div id="top-container">
		<div class="container text-center">
			<h1 class="top-container-title">Learn how to double your reach in half the time</h1>

			<p class="top-container-description">Zenplify is a step-by-step system that will give you the confidence to
				work virtually and grow your business.<br> The tools, strategies and support system is so simple, you'll
				actually do it. And, you'll be great at it.<br> Don't hesitate - take your business to the next level
				today.</p>
		</div>
	</div>
	<div id="form-container">
		<div class="container">
			<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css"/>
			<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c29936f28" type="text/css"/>
			<script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c29936f28"></script>
			<div class="moonray-form-p2c29936f28 ussr">
				<div class="moonray-form moonray-form-label-pos-stacked" id="order-form">
					<form id="form" class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
						<h1 class="form-title text-center">Zenplify Monthly Subscription</h1>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-25747511768" class="moonray-form-label">First Name</label>

							<input name="firstname" type="text" class="moonray-form-input form-control" readonly id="mr-field-element-25747511768" required value="<?php echo $fname;?>"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-144248347263" class="moonray-form-label">Last Name</label>

							<input name="lastname" type="text" class="moonray-form-input form-control" readonly id="mr-field-element-144248347263" required value="<?php echo $lname;?>"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-email">
							<label for="mr-field-element-678319724276" class="moonray-form-label">Email</label>

							<input name="email" type="email" class="moonray-form-input form-control" readonly id="mr-field-element-678319724276" required value="<?php echo $email;?>"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-871785524766" class="moonray-form-label">Billing
								Address</label>

							<input type="text" name="billing_address1" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-871785524766"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-208106660284" class="moonray-form-label">Billing Address
								2</label>

							<input type="text" name="billing_address2" class="moonray-form-input  moonray-form-payments-element form-control" id="mr-field-element-208106660284"/>
						</div>
						<div class="form-group" style="margin-bottom: 0px;">
							<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
								<label for="mr-field-element-271402571117" class="moonray-form-label">Billing
									City</label>

								<input type="text" name="billing_city" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-271402571117"/>
							</div>
							<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
								<label for="mr-field-element-269848507130" class="moonray-form-label">Billing
									Zip</label>

								<input type="text" name="billing_zip" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-269848507130"/>
							</div>
							<div class="clear-fix"></div>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
							<label for="mr-field-element-225418976508" class="moonray-form-label">Billing State</label>

							<select name="billing_state" required class="moonray-form-input  moonray-form-payments-element form-control" id="mr-field-element-225418976508">
								<?php include_once 'form-partials/states.html'; ?>
								<option value="_NOTLISTED_">My State is not listed</option>
							</select>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
							<label for="mr-field-element-362415943993" class="moonray-form-label">Billing
								Country</label>

							<select name="billing_country" required class="moonray-form-input  moonray-form-payments-element form-control" id="mr-field-element-362415943993">
								<?php include_once 'form-partials/countries.html'; ?>
							</select>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-separator" style="padding-bottom: 0px;">
							<span class="moonray-form-label">Enter Your Billing Information</span>
						</div>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-cart">
							<div class="moonray-form-payments-element moonray-from-offer-grid" id="mr-field-element-27565683238"></div>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-paymentmethod">
							<div class="moonray-form-payments-element" id="mr-field-element-443889559712">
								<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-credit-card">
									<div class="moonray-form-element-wrapper moonray-form-element-html">
										<div class="moonray-form-credit-card-display credit_cards moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text moonray-form-element-wrapper-contracted moonray-form-element-html">
											<div class="moonray-form-cc-logo moonray-form-cc-logo-visa"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-discover"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-amex"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-mastercard"></div>
										</div>
									</div>

									<div class="form-group card-info" style="margin-bottom: 0px;">
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-number moonray-form-input-type-text">
											<label for="mr-field-element-2761823455875" class="moonray-form-label">Card
												Number</label>

											<input type="text" name="payment_number" id="mr-field-element-2761823455875" required class="moonray-form-input form-control keyup" pattern="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$"/>
										</div>
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-code moonray-form-input-type-text">
											<label for="mr-field-element-8677835110589" class="moonray-form-label">CVC</label>

											<input type="text" name="payment_code" id="mr-field-element-8677835110589" required class="moonray-form-input form-control keyup"/>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="form-group card-info" style="margin-bottom: 0px;">
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-month moonray-form-input-type-select">
											<label for="mr-field-element-3441974991067" class="moonray-form-label"></label>

											<select name="payment_expire_month" id="mr-field-element-3441974991067" required class="moonray-form-input form-control">
												<option value="">Exp. Month</option>
												<option value="01">01 - January</option>
												<option value="02">02 - February</option>
												<option value="03">03 - March</option>
												<option value="04">04 - April</option>
												<option value="05">05 - May</option>
												<option value="06">06 - June</option>
												<option value="07">07 - July</option>
												<option value="08">08 - August</option>
												<option value="09">09 - September</option>
												<option value="10">10 - October</option>
												<option value="11">11 - November</option>
												<option value="12">12 - December</option>
											</select>
										</div>
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-year moonray-form-input-type-select">
											<label for="mr-field-element-4050126550779" class="moonray-form-label"></label>

											<select name="payment_expire_year" id="mr-field-element-4050126550779" required class="moonray-form-input form-control">
												<option value="">Exp. Year</option>
												<?php $i = date('Y');
												for ($i; $i < (date('Y') + 15); $i++) {
													echo '<option value="' . $i . '">' . $i . '</option>';
												} ?>
											</select>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
							<input type="submit" name="submit-button" value="Submit" class="moonray-form-input" id="mr-field-element-44871697667"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="afft_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="aff_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="sess_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="ref_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="own_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="oprid" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="contact_id" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_source" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_medium" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_term" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_content" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_campaign" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="referral_page" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="uid" type="hidden" value="p2c29936f28"/>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="container">
			<p>Copyrights &copy; <?php echo date('Y'); ?>. All Rights Reserved</p>
		</div>
	</footer>
</div>

<script src="https://zenplify.biz/modules/ontraport/js/jquery-1.11.3.min.js"></script>
<script src="https://zenplify.biz/modules/ontraport/js/jquery.validate.min.js"></script>
<script>

	jQuery.noConflict();

	jQuery.validator.setDefaults({
		debug: true
	});

	(function ($) {
		$(document).ready(function () {

			$('#form').validate({
				onfocusout: function (element) {
					this.element(element);
				},
				onkeyup: function (element) {

					if ($(element).hasClass('keyup')) {
						this.element(element);
					}

				},
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
						email: true
					},
					billing_address1: "required",
					billing_city: "required",
					billing_zip: "required",
					billing_state: "required",
					billing_country: "required",
					payment_number: {
						required: true,
						creditcard: true
					},
					payment_code: {
						required: true,
						digits: true,
						minlength: 3,
						maxlength: 4
					},
					payment_expire_month: "required",
					payment_expire_year: "required"
				},
				messages: {
					firstname: "Please enter first name.",
					lastname: "Please enter last name.",
					email: {
						required: "Please enter email address.",
						email: "Please enter a valid email address."
					},
					billing_address1: "Please enter your billing address.",
					billing_city: "Please enter your billing city.",
					billing_zip: "Please enter your billing zip.",
					billing_state: "Please select your billing state.",
					billing_country: "Please select your billing country.",
					payment_number: {
						required: "Please enter a credit card number.",
						creditcard: "Please enter a valid credit card number."
					},
					payment_code: {
						required: "Please enter a valid CVC.",
						digits: "Please enter a valid CVC.",
						minlength: "Please enter a valid CVC.",
						maxlength: "Please enter a valid CVC."
					},
					payment_expire_month: "Please select a valid expiry month.",
					payment_expire_year: "Please select a valid expiry year."
				}
			});
		});
	})(jQuery);
</script>
</body>
</html>