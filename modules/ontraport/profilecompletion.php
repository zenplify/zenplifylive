<?php
include '../../classes/init.php';
require_once '../../classes/register.php';
require_once '../../classes/email.php';
require_once '../../classes/settings.php';

$database = new database();
$register = new register();
$settings = new settings();
$timezone = $register->getTimeZone();
session_start();
$userId = $_SESSION['userId'];
$leaderId = $_SESSION['leaderId'];
if (empty($userId) && empty($leaderId)){
    SendRedirect("../../signup.php");
}


if ((isset($_POST["submit"]))) {

    $result = $register->udpdateUserinformation($_POST, $userId);
    SendRedirect("../loginProcess/home.php");


}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Zenplify</title>
    <link rel="stylesheet" href="<?php echo $CONF["siteURL"]; ?>css/style.css" type="text/css">
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>js/script.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>validation/lib/jquery.js"></script>
    <script type="text/javascript" src="<?php echo $CONF["siteURL"]; ?>validation/jquery.validate.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#email").focus(function (e) {
                $("#messageEmail").html('');
                messageUser
            });
            $("#username").focus(function (e) {
                $("#messageUser").html('');
            });

        });
        window.onload = function () {
            var myInput = document.getElementById('emailConfirm');
            if (myInput) {
                myInput.onpaste = function (e) {
                    e.preventDefault();
                }
            }
        }
        function checkIfAlredyExist() {
            var emailcheck = document.getElementById('emailcheck').value;
            var usercheck = document.getElementById('usercheck').value;
            var userSpecialcheck = document.getElementById('userSpecialcheck').value;
            if (emailcheck == 0 || usercheck == 0 || userSpecialcheck == 0) {
                if (userSpecialcheck == 0) {
                    $("#username").focus();
                }
                return false;
            }
            else {
                var leaderTemplate = $("#leader_template").val();
                if (leaderTemplate == 1)
                    return confirm("Do you want to signup as blank account!");

                return true;
            }
        }
        function cancelPage() {
            window.location.replace("https://zenplify.biz/signup.php");
        }
    </script>

    <?php require_once("../../classes/resource.php"); ?>
</head>
<body>
<div class="container">
    <div id="top_cont">
        <div id="logo"><img alt="" src="../../images/logo.png"/></div>
    </div>
</div>
<div class="container">
<div style="float:right; margin-bottom:10px; ">
    <!--<input  class="signin" type="button" value="" name="submit" onclick="window.location.href='../../signup.php'">-->
</div>

<form name="add_user" id="add_user" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post"
      onsubmit="return checkIfAlredyExist();">
<table id="frmSignupUser" cellspacing="0" cellpadding="0" width="970px">

<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td colspan="2" class="task_col">Contact Information</td>
    <td></td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td colspan="2" class="label">Enter your information below exactly as you would like it to appear on
        your email signature and guest profile pages.
    </td>
</tr>
<tr>
    <td class="label">First Name</td>
    <td><input type="text" name="firstname" class="textfield"/></td>
</tr>
<tr>
    <td class="label">Last Name</td>
    <td><input type="text" name="lastname" class="textfield"/></td>
</tr>


<tr>
    <td class="label">Time Zone</td>
    <td>
        <select name="timezone" class="SelectField">
            <?php

            foreach ($timezone as $tz) {
                echo '<option value="' . $tz->timeZoneId . '">' . $tz->zoneTitle . ' (GMT ' . $tz->GMTDiff . ':00 )</option>';
            }?>
        </select>
    </td>
</tr>
<tr>
    <td class="label">Phone Number</td>
    <td><input type="text" name="phonenumber" class="textfield" style="float:left;"/>

        <div style="margin-top:4px; float:left; font-size:12px;color:#555555;margin-right:98px;"> Format
            your number the way you want it to show in your signature
        </div>
    </td>
</tr>


<?php
$leaderCustomFields = $settings->getLeaderDefaultFields($leaderId, 1);
$leaderCustomQuestions = $settings->getLeaderCustomQuestions($leaderId, '1');

if (!empty($leaderCustomFields) || !empty($leaderCustomQuestions)) {
    ?>
    <tr>
        <td colspan="2" class="task_col"></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    <!--<input type="hidden" name="upComingTask" id="upComingTask" value="< ?php echo $upComingTask ?>" />-->
    <?php
    foreach ($leaderCustomFields as $lcf) {
        switch ($lcf->fieldMappingName) {
            case 'consultantId':
                ?>
                <tr>
                    <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                    <td><input type="text" name="consultant_id" class="textfield"/></td>
                </tr>
                <?php
                break;
            case 'title':
                ?>
                <tr>
                <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                <td><input type="text" name="title" class="textfield" style="float:left;"/></td>
                </tr><?php
                break;
            case 'webAddress':
                ?>
                <tr>
                <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                <td><input type="text" name="web" class="textfield url" style="float:left;" id="web"/></td>
                </tr><?php
                break;
            case 'facebookAddress':
                ?>
                <tr>
                <td class="label"><?php echo $lcf->fieldCustomName; ?></td>
                <td><input type="text" name="facebook_page" class="textfield url" style="float:left;"
                           id="facebook_page"/></td>
                </tr><?php
                break;
        }
    }
    foreach ($leaderCustomQuestions as $lcq) {
        switch ($lcq->typeId) {
            case '1':
                ?>
                <tr>
                <td class="label"><?php echo $lcq->question; ?></td>
                <td><input type="text" name="<?php echo 'question' . $lcq->questionId; ?>"
                           id="<?php echo 'question' . $lcq->questionId; ?>"
                           class="textfield <?php if ($lcq->mandatory == 1) {
                               echo 'required';
                           } ?>"/></td>
                </tr><?php
                break;
            case '3':
                $questionChoice = $settings->getQuestionChoice($lcq->questionId);
                ?>
                <tr>
                <td class="label"><?php echo $lcq->question; ?></td>
                <td>
                    <?php
                    foreach ($questionChoice as $qc) {
                        ?>
                        <input type="checkbox" name="<?php echo 'choice' . $lcq->questionId; ?>[]"
                               class="customField <?php if ($lcq->mandatory == 1) {
                                   echo 'required';
                               } ?>" value="<?php echo $qc->choiceId . '_' . $qc->choiceText; ?>"/>
                        <span
                            style="display:inline-block; vertical-align:top;"><?php echo $qc->choiceText; ?></span>
                        <label for="<?php echo 'choice' . $lcq->questionId; ?>[]" generated="true"
                               class="error"></label>
                        <br>
                    <?php
                    }
                    ?>
                </td>
                </tr><?php
                break;
            case '4':
                $questionChoice = $settings->getQuestionChoice($lcq->questionId);
                ?>
                <tr>
                <td class="label"><?php echo $lcq->question; ?></td>
                <td>
                    <?php
                    foreach ($questionChoice as $qc) {
                        ?>
                        <input type="radio" name="<?php echo 'choice' . $lcq->questionId; ?>[]"
                               class="customField <?php if ($lcq->mandatory == 1) {
                                   echo 'required';
                               } ?>" value="<?php echo $qc->choiceId . '_' . $qc->choiceText; ?>"/>
                        <span
                            style="display:inline-block; vertical-align:top;"><?php echo $qc->choiceText; ?></span>
                        <label for="<?php echo 'choice' . $lcq->questionId; ?>[]" generated="true"
                               class="error"></label>
                        <br>
                    <?php
                    }
                    ?>
                </td>
                </tr><?php
                break;
        }


    }
}
?>
<tr>
    <td colspan="2" class="label" style="padding-left:0px !important;">
        <input type="checkbox" name="termandconditions" class="termandconditions"
               style="float:left; margin-left:12px;"/>
        <label
            style="display: inline-block;height: 12px; margin-right:30px;margin-left: 10px; float:left; line-height:17px;">
            Please accept our
            <a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;"
               onclick="loadform()">Terms of Service</a> &
            <a href="javascript:" style="text-decoration:none; font-weight:bold;color: #9B9B9B;"
               onclick="loadform1()">Privacy Policy</a>
        </label>
    </td>
</tr>
<tr>
    <td class="label">&nbsp;</td>
    <td>
        <input type="submit" name="submit" class="update" value=""/>

    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>
</form>

<?php include("../headerFooter/footer.php"); ?>
<div id="Popup"></div>
<div id="popupQuickquote">
    <h1 class="gray">Terms of Service </h1>
    <?php include('termofUse.html'); ?>
    <!--	<ol>-->
    <!--		<li> You must be 13 years or older to use this Service.</li>-->
    <!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
    <!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</li>-->
    <!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to-->
    <!--			 comply with this security obligation.-->
    <!--		</li>-->
    <!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
    <!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
    <!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not-->
    <!--			 limited to copyright laws).-->
    <!--		</li>-->
    <!--	</ol>-->
</div>
<div id="popupQuickquote1">
    <h1 class="gray">Privacy Policy</h1>
    <?php include('termofUse.html'); ?>
    <!--	<ol>-->
    <!--		<li> You must be 13 years or older to use this Service.</li>-->
    <!--		<li>You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>-->
    <!--		<li>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</li>-->
    <!--		<li> You are responsible for maintaining the security of your account and password. Zenplify cannot and will not be liable for any loss or damage from your failure to-->
    <!--			 comply with this security obligation.-->
    <!--		</li>-->
    <!--		<li> You are responsible for all Content posted and activity that occurs under your account.</li>-->
    <!--		<li> One person or legal entity may not maintain more than one free account.</li>-->
    <!--		<li> You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not-->
    <!--			 limited to copyright laws).-->
    <!--		</li>-->
    <!--	</ol>-->
</div>
</div>