<?php
/*
 * Recharge to ontraport users
 */
include '../../classes/init.php';
$database=new database();
$code = $_GET['access_token'];
if(empty($code)){
    echo 'Invalid User, Please contact to Zenplify Help Desk.';
}else{
   echo $paymentPlan=getUserDetail($code);
}
if($paymentPlan==2){
    header('Location: https://zenplify.biz/modules/ontraport/monthly_subscription.php?token='.$code);
}else if ($paymentPlan==3){
    header('Location: https://zenplify.biz/modules/ontraport/quarterly_subscription.php?token='.$code);
}else{
    header('Location: https://zenplify.biz/modules/ontraport/zen-recharge.php?token='.$code);
}
function getUserDetail($code){
    $database=new database();
   $checkPlan = $database->executeScalar("SELECT up.`planId` FROM `user` u JOIN userpayments up ON u.`userId`=up.`userId` WHERE u.`emailVerificationCode`='".$code."' ORDER BY up.`paymentId` DESC LIMIT 1");

    return $checkPlan;
}
?>
