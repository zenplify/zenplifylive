<?php
/*
 * Recharge to ontraport users
 */
include '../../classes/init.php';
require_once("../../classes/register.php");
require_once("../../classes/payment.php");
$register = new register();
$payment = new payment();
$chargeAmount = $_GET['amount'];
if($chargeAmount==50){
    $duration=92;
}else{
    $duration=32;
}

//Get parameters
$email = $_GET['email'];
$chargeAmount = $_GET['amount'];
$nex_payment = date('Y-m-d', strtotime("+".$duration." days"));
$planTitle='Zen-Monthly-Recharge';

if(!empty($email)){
//get email verification code, userId
$code = $register->getEmailVerificationCode($email);


$userId = $register->userId($email, $code);
//check for payment status
$isPaymentActive= isPaymentStatusActive($code,$userId);

    if($isPaymentActive == '0'){
        updatePaymentStatus($code,$userId);
       
    }
//Recharge Execution
$payment->userPaymentsforOntraPort($userId, $chargeAmount, $nex_payment,$planTitle);
$register->updateuserInfo($userId, $code);



}else {

  failedPaymentsLog($userId, $email, $chargeAmount);

}

//check if ontraport payment is active

function isPaymentStatusActive($code,$userId){
    $database=new database();
   $isPaymentActive = $database->executeScalar("SELECT `isOntraportPaymentActive` from `user` where `emailVerificationCode`='".$code."' and userId='".$userId."'");

    return $isPaymentActive;
}
// update payment status
function updatePaymentStatus($code,$userId){
    $database=new database();
    return $database->executeNonQuery("UPDATE `user` set `isOntraportPaymentActive`='1' where  `emailVerificationCode`='".$code."' and userId='".$userId."' ");


}
//Failed payments log
function failedPaymentsLog($userId, $email, $chargeAmount){
    $database=new database();
    return $database->executeNonQuery("Insert into `ontraportfailedpaymentslog` (`userId`,`email`,`chargeAmount`) values ('" . $userId . "','" . $email . "','" . $chargeAmount . "')");

}
?>
