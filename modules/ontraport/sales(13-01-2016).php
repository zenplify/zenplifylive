<?php
error_reporting(0);
require_once '../../classes/register.php';
$register = new register();
$leaderName = $_REQUEST['leaderName'];

if (isset($leaderName)) {

    $getUserIdIfexist = $register->checkUserAvailable($_REQUEST['leaderName']);

    if (empty($getUserIdIfexist)) {
        header('Location:https://zenplify.biz/signup/' . $_REQUEST['leaderName']);
  }
}

$base_url = 'https://zenplify.biz/modules/ontraport/';
//$base_url = 'http://localhost/ontraport/'
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
    <title>Zenplify Sales</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo $base_url; ?>css/sales.css"/>
</head>
<body>
<div class="container-fluid">

    <!--HEADER-->
    <header>
        <div class="custom-container">
            <img id="site-logo" src="<?php echo $base_url; ?>images/logo.png" alt="Zenplify">
        </div>
    </header>

    <!--BANNER-->
    <div id="banner">
        <div class="custom-container" id="text-wrapper">
            <div>
                <h1 class="banner-title text-center">
                    Learn how to double your reach in half the time.
                </h1>

                <p class="text-center">
                    Zenplify is a step-by-step system that will give you the confidence to work virtually and grow your
                    business.<br/> The tools, strategies and support system is so simple, you'll actually do it. And,
                    you'll be great at it.<br/> Don't hesitate - take your business to the next level today.
                </p>
            </div>
        </div>
    </div>

    <div id="middle-container">
        <div id="middle-container-left" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h2 class="list-title">What is included?</h2>
            <ul class="lists">
                <li>Access a simple, sleek system so easy to navigate you don't need to be a "techy"</li>
                <li>Build stronger relationships with your customers</li>
                <li>Leverage the freedom to work virtually when and where you want</li>
                <li>Master a scalable system for your team to easily duplicate best practices</li>
                <li>Increase your productivity with the proven tools</li>
                <li>Learn from the experts on how to grow your business online</li>
            </ul>
        </div>
        <div id="middle-container-right" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h2 class="list-title">Watch Zenplify in Action</h2>
            <iframe width="100%" height="360" src="https://www.youtube.com/embed/igKRAPe-mAE" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>

    <div class="quotes-container">

        <div class="left-quote col-md-offset-1 col-lg-offset-1  col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                <img class="img-responsive" src="<?php echo $base_url; ?>images/Crissy-Lowe.png" alt="Author">
            </div>

            <div class="quote col-xs-8 col-sm-8 col-md-10 col-lg-10">
                <div class="quote-bg">
                    <div class="paragraph">
                        &#8220;I love how clean and professional the system looks. The follow up system is something
                        that I did not have on my own. Automating the follow up is genius. A lot of my warm market lives
                        in another state, so this is an alternative to getting in their living room.&#8221;
                    </div>
                    <div class="client-name">Chrissy Lowe, San Francisco, CA</div>
                </div>
            </div>
        </div>

        <div class="right-quote col-md-offset-1 col-lg-offset-1  col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <div class="quote col-xs-8 col-sm-8 col-md-10 col-lg-10">
                <div class="quote-bg">
                    <div class="paragraph">
                        &#8220;I love Facebook parties. I am extremely ill right now with health issues and virtual
                        parties have saved my life.&#8221;

                    </div>
                    <div class="client-name">Christina Brajcic, Windsor, Ontario</div>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                <img class="img-responsive" src="<?php echo $base_url; ?>images/Christina-Brajcic.png" alt="Author">
            </div>

        </div>
        <div class="clear-fix"></div>
    </div>

    <div id="join-us">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <span class="join-us-title text-center">
                Join us. We want YOU in the Zenplify community!
            </span>
        </div>
    </div>

    <div id="products">
        <div class="col-md-offset-3 col-lg-offset-3 col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="<?php echo ($leaderName == 'dianesears') ? 'margin: 0px 29.17%;' : ''; ?>">
                    <?php if($leaderName != 'dianesears'): ?>
                    <div id="best-value">
                        <img class="img-responsive" src="<?php echo $base_url; ?>images/circle-best.png" alt="Best Value Offer">
                    </div>
                    <?php endif; ?>
                    <div id="zen-product" style="<?php echo ($leaderName == 'dianesears') ? 'margin: 0px auto 30px;' : ''; ?>">
                        <a href="<?php echo $base_url; ?>zen.php?leaderId=<?php echo $leaderName; ?>">
                            <img class="img-responsive" src="<?php echo $base_url; ?>images/Zen-basic-package.png" alt="ZEN">
                        </a>
                    </div>
                </div>
                <?php if($leaderName != 'dianesears'): ?>
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <div id="zap-product">
                            <a href="<?php echo $base_url; ?>zap.php?leaderId=<?php echo $leaderName; ?>">
                                <img class="img-responsive" src="<?php echo $base_url; ?>images/ZAP-package.png" alt="ZAP">
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img class="img-responsive" src="<?php echo $base_url; ?>images/Stamp.png" alt="Stamp">
                </div>
            </div>
        </div>
    </div>

    <div class="quotes-container">
        <div class="left-quote col-md-offset-1 col-lg-offset-1  col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                <img class="img-responsive" src="<?php echo $base_url; ?>images/Christine-Kenneally.png" alt="Author">
            </div>

            <div class="quote col-xs-8 col-sm-8 col-md-10 col-lg-10">
                <div class="quote-bg">
                    <div class="paragraph">
                        &#8220;Zen has totally enhanced my business. I use it virtually and "live". I don’t treat it as
                        my only reach-out method but as an additional reach-out method. The program is such a fantastic
                        tool, but the leadership, support and fellowship is what sets it apart and above any other tool
                        out there.&#8221;

                    </div>
                    <div class="client-name">Christine Kenneally Rockville Centre, NY</div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    </div>

    <footer>
        <div class="container">
            <p>Copyright &copy; 2015. All Rights Reserved</p>
        </div>
    </footer>

</div>
</body>
</html>
