<?php
error_reporting(0);
if (isset($_GET['leaderId'])) {

	$leaderId = $_GET['leaderId'];
	if (ctype_alnum($leaderId) == false) {
		header('Location: https://zenplify.biz/modules/ontraport/sales.php');
	}
} else {
	header('Location: https://zenplify.biz/modules/ontraport/sales.php');
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
	<title>Zenplify Accelerator Pack</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="https://zenplify.biz/modules/ontraport/css/form-styles.css"/>
</head>
<body>
<div class="container-fluid">
	<header>
		<div class="container" id="header-container">
			<img src="https://zenplify.biz/modules/ontraport/images/logo.png" alt="Zenplify"></div>
	</header>
	<div id="top-container">
		<div class="container text-center">
			<h1 class="top-container-title">Learn how to double your reach in half the time</h1>

			<p class="top-container-description">Zenplify is a step-by-step system that will give you the confidence to
				work virtually and grow your business.<br> The tools, strategies and support system is so simple, you'll
				actually do it. And, you'll be great at it.<br> Don't hesitate - take your business to the next level
				today.</p>
		</div>
	</div>
	<div id="form-container">
		<div class="container">

			<!-- ORDER FORM -->
			<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css"/>
			<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css"/>
			<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c29936f18" type="text/css"/>
			<script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c29936f18"></script>
			<div class="moonray-form-p2c29936f18 ussr">
				<div class="moonray-form moonray-form-label-pos-stacked" id="order-form">
					<form id="form" class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
						<h1 class="form-title text-center">Zenplify Accelerator Pack</h1>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-522997159045" class="moonray-form-label">First Name</label>

							<input name="firstname" type="text" class="moonray-form-input form-control" id="mr-field-element-522997159045" required/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-816983724944" class="moonray-form-label">Last Name</label>

							<input name="lastname" type="text" class="moonray-form-input form-control" id="mr-field-element-816983724944" required/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-email">
							<label for="mr-field-element-127862499561" class="moonray-form-label">Email</label>

							<input name="email" type="email" class="moonray-form-input form-control keyup" id="mr-field-element-127862499561" required/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-78557934379" class="moonray-form-label">Unique Name</label>

							<input name="f1427" required type="text" class="moonray-form-input form-control keyup" id="mr-field-element-78557934379"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-401065105805" class="moonray-form-label">Billing
								Address</label>

							<input type="text" name="billing_address1" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-401065105805"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
							<label for="mr-field-element-351159136742" class="moonray-form-label">Billing Address
								2</label>

							<input type="text" name="billing_address2" class="moonray-form-input  moonray-form-payments-element" id="mr-field-element-351159136742"/>
						</div>

						<div class="form-group" style="margin-bottom: 0px;">
							<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
								<label for="mr-field-element-354087201878" class="moonray-form-label">Billing
									City</label>

								<input type="text" name="billing_city" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-354087201878"/>
							</div>
							<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
								<label for="mr-field-element-162628164049" class="moonray-form-label">Billing
									Zip</label>

								<input type="text" name="billing_zip" class="moonray-form-input  moonray-form-payments-element form-control" required id="mr-field-element-162628164049"/>
							</div>
							<div class="clear-fix"></div>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
							<label for="mr-field-element-8278908440" class="moonray-form-label">Billing State</label>

							<select name="billing_state" required class="moonray-form-input  moonray-form-payments-element form-control" id="mr-field-element-8278908440">
								<?php include_once 'form-partials/states.html'; ?>
							</select>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
							<label for="mr-field-element-157594502437" class="moonray-form-label">Billing
								Country</label>

							<select name="billing_country" required class="moonray-form-input  moonray-form-payments-element form-control" id="mr-field-element-157594502437">
								<?php include_once 'form-partials/countries.html'; ?>
							</select>
						</div>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-separator" style="padding-bottom: 0px;">
							<span class="moonray-form-label">Enter Your Billing Information</span>
						</div>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-cart">
							<div class="moonray-form-payments-element moonray-from-offer-grid" id="mr-field-element-738180157030"></div>
						</div>

						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-paymentmethod">
							<div class="moonray-form-payments-element" id="mr-field-element-658198488876">
								<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-credit-card">
									<div class="moonray-form-element-wrapper moonray-form-element-html">
										<div class="moonray-form-credit-card-display credit_cards moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text moonray-form-element-wrapper-contracted moonray-form-element-html">
											<div class="moonray-form-cc-logo moonray-form-cc-logo-visa"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-discover"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-amex"></div>
											<div class="moonray-form-cc-logo moonray-form-cc-logo-mastercard"></div>
										</div>
									</div>

									<div class="form-group card-info" style="margin-bottom: 0px;">
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-number moonray-form-input-type-text">
											<label for="mr-field-element-6642660140094" class="moonray-form-label">Card
												Number</label>

											<input type="text" name="payment_number" id="mr-field-element-6642660140094" required class="moonray-form-input form-control" pattern="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$"/>
										</div>
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-code moonray-form-input-type-text">
											<label for="mr-field-element-742449257615" class="moonray-form-label">CVC</label>

											<input type="text" name="payment_code" id="mr-field-element-742449257615" required class="moonray-form-input form-control"/>
										</div>

										<div class="clear-fix"></div>
									</div>
									<div class="form-group card-info" style="margin-bottom: 0px;">
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-month moonray-form-input-type-select">
											<label for="mr-field-element-7300591254634" class="moonray-form-label"></label>

											<select name="payment_expire_month" id="mr-field-element-7300591254634" required class="moonray-form-input form-control">
												<option value="">Exp. Month</option>
												<option value="01">01 - January</option>
												<option value="02">02 - February</option>
												<option value="03">03 - March</option>
												<option value="04">04 - April</option>
												<option value="05">05 - May</option>
												<option value="06">06 - June</option>
												<option value="07">07 - July</option>
												<option value="08">08 - August</option>
												<option value="09">09 - September</option>
												<option value="10">10 - October</option>
												<option value="11">11 - November</option>
												<option value="12">12 - December</option>
											</select>
										</div>
										<div class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-year moonray-form-input-type-select">
											<label for="mr-field-element-7720027738274" class="moonray-form-label"></label>

											<select name="payment_expire_year" id="mr-field-element-7720027738274" required class="moonray-form-input form-control">
												<option value="">Exp. Year</option>
												<?php $i = date('Y');
												for ($i; $i < (date('Y') + 15); $i++) {
													echo '<option value="' . $i . '">' . $i . '</option>';
												} ?>
											</select>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
							<input type="submit" name="submit-button" value="Submit" class="moonray-form-input" id="mr-field-element-421455579577"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="afft_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="aff_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="sess_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="ref_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="own_" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="oprid" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="contact_id" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_source" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_medium" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_term" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_content" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="utm_campaign" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="referral_page" type="hidden" value=""/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="uid" type="hidden" value="p2c29936f18"/>
						</div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden">
							<input name="f1426" type="hidden" value="<?php echo $leaderId; ?>"/>
						</div>
					</form>
				</div>
			</div>
			<!-- END -->
		</div>
	</div>
	<footer>
		<div class="container">
			<p>Copyrights &copy; <?php echo date('Y'); ?>. All Rights Reserved</p>
		</div>
	</footer>
</div>

<script src="https://zenplify.biz/modules/ontraport/js/jquery-1.11.3.min.js"></script>
<script src="https://zenplify.biz/modules/ontraport/js/jquery.validate.min.js"></script>
<script>

	jQuery.noConflict();

	jQuery.validator.setDefaults({
		debug: true
	});

	var emailVal = "", nameVal = "";

	var isValidEmail = false;
	var isValidName = false;

	(function ($) {
		$(document).ready(function () {

			$('#form').validate({
				onfocusout: function (element) {
					this.element(element);
				},
				onkeyup: function (element) {

					if ($(element).hasClass('keyup')) {
						this.element(element);
					}

				},
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
						email: true
					},
					f1427: {
						required: true
					},
					billing_address1: "required",
					billing_city: "required",
					billing_zip: "required",
					billing_state: "required",
					billing_country: "required",
					payment_number: {
						required: true,
						creditcard: true
					},
					payment_code: {
						required: true,
						digits: true,
						minlength: 3,
						maxlength: 4
					},
					payment_expire_month: "required",
					payment_expire_year: "required"
				},
				messages: {
					firstname: "Please enter first name.",
					lastname: "Please enter last name.",
					email: {
						required: "Please enter email address.",
						email: "Please enter a valid email address."
					},
					f1427: {
						required: "Please enter unique name."
					},
					billing_address1: "Please enter your billing address.",
					billing_city: "Please enter your billing city.",
					billing_zip: "Please enter your billing zip.",
					billing_state: "Please select your billing state.",
					billing_country: "Please select your billing country.",
					payment_number: {
						required: "Please enter a credit card number.",
						creditcard: "Please enter a valid credit card number."
					},
					payment_code: {
						required: "Please enter a valid CVC.",
						digits: "Please enter a valid CVC.",
						minlength: "Please enter a valid CVC.",
						maxlength: "Please enter a valid CVC."
					},
					payment_expire_month: "Please select a valid expiry month.",
					payment_expire_year: "Please select a valid expiry year."
				}
			});


			$.validator.addMethod("validateUniqueName", function (value, element) {
				var data = {action: 'username', uname: value, type: 'json'};
				nameVal = value;
				if (value) {
					$.ajax({
						type: "POST",
						url: 'https://zenplify.biz/modules/ontraport/check.php',
						dataType: "json",
						data: data,
						success: function (response) {
							if (response == 1) {
								// Add error class
								$('#mr-field-element-78557934379').addClass('error');
								// Show to error to the user
								$('#mr-field-element-78557934379-error').html("Unique name is already in use.");
								// Invalidate the unique name field
								$('#mr-field-element-78557934379').attr('aria-invalid', true);
								isValidName = false;
							} else {
								// Remove error class from unique name field
								$('#mr-field-element-78557934379').removeClass('error');
								// Add a valid class
								$('#mr-field-element-78557934379').addClass('valid');
								// Show success message to the user
								$('#mr-field-element-78557934379-error').html("<span style='color: green;'>Unique name is available.</span>");
								// Validate the unique name field
								$('#mr-field-element-78557934379').attr('aria-invalid', false);

								isValidName = true;
							}
						}
					});
				}

			}, '');

			$(':input[name="f1427"]').rules("add", {"validateUniqueName": true});

			$.validator.addMethod("validateUniqueEmail", function (value, element) {

				var data = {action: 'email', email: value, type: 'json'};
				emailVal = value;
				$.ajax({
					type: "POST",
					url: 'https://zenplify.biz/modules/ontraport/check.php',
					dataType: "json",
					data: data,
					success: function (response) {
						if (response == 1) {
							// Add error class
							$('#mr-field-element-127862499561').addClass('error');
							// Enable unique name check
							$(':input[name="f1427"]').rules("add", {"validateUniqueName": true});
							// Enable Unique name field
							$('#mr-field-element-78557934379').prop('disabled', false);
							// Show error to the user
							$('#mr-field-element-127862499561-error').html('<span style="color: red;">Email Address is already in use.</span>');
							// Invalidate the email field
							$('#mr-field-element-127862499561').attr('aria-invalid', true);
							isValidEmail = false;
						} else if (response == 0) {
							// Enable unique name check
							$(':input[name="f1427"]').rules("add", {"validateUniqueName": true});
							// Enable Unique name field
							$('#mr-field-element-78557934379').prop('disabled', false);
							// Remove error class from email field
							$('#mr-field-element-127862499561').removeClass('error');
							// Show success message to the user
							$('#mr-field-element-127862499561-error').html('<span style="color: green;">Email Address is available.</span>');
							// Validate the email field
							$('#mr-field-element-127862499561').attr('aria-invalid', false);
							isValidEmail = true;
						}
						else {
							response = response.split(',');
							// Remove error class from email field
							$('#mr-field-element-127862499561').removeClass('error');
							// Remove error class from unique name field
							$('#mr-field-element-78557934379').removeClass('error');
							// Disable the unique name field
							$('#mr-field-element-78557934379').val(response[1]).prop('disabled', true);
							// Validate the email field
							$('#mr-field-element-127862499561').attr('aria-invalid', false);
							// Hide the error message for unique name
							$('#mr-field-element-78557934379-error').hide();
							// Hide the error message for email
							$('#mr-field-element-127862499561-error').hide();
							// Disable unique name check
							$(':input[name="f1427"]').rules("add", {"validateUniqueName": false});
							isValidEmail = true;
						}
					}
				});

			}, '');

			$(':input[name="email"]').rules("add", {"validateUniqueEmail": true});

			$('#mr-field-element-421455579577').on('click', function () {
				if (isValidEmail == false || isValidName == false) {
					if (isValidEmail == false) {
						$(':input[name="email"]').focus();
					} else {
						$(':input[name="f1427"]').focus();
					}
					return false;
				}
				return true;
			});

		});
	})(jQuery);
</script>
</body>
</html>