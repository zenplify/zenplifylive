<?php


include '../../classes/init.php';
require_once("../../classes/register.php");
require_once("../../classes/payment.php");

$register = new register();
$payment = new payment();

// getting peramemters from ping url

$userName = urldecode($_GET['username']);
$password = $_GET['password'];
$email = urldecode($_GET['email']);
$chargeAmount = $_GET['amount'];
$first_name = $_GET['first_name'];
$last_name = $_GET['last_name'];
$ontraportPaymentDuration = isset($_GET['paymentduration']) ? $_GET['paymentduration'] : '';
$leaderName = isset($_GET['leaderId']) ? $_GET['leaderId'] : 'VENVPs';
If (empty($leaderName)) {
    $leaderName = 'VENVPs';
}
$nex_payment = date('Y-m-d', strtotime("+" . $_GET['trial_period'] . " days"));
$planTitle = isset($_GET['trial_period']) ? 'ontraport_trial_period' : 'ontraport_trial_period';
$contactId = $_GET['contact_id'];

// Ping url record (Log)
$register->updateUserLog($email, $password, $leaderName, $userName, $first_name, $last_name);
// getting leaderId from leader name
$leaderId = $register->getleaderId($leaderName);

// check if user already exist
$code = $register->getEmailVerificationCode($email);
$userId = $register->userId($email, $code);
// if not exist create user
if ((empty($code) && empty($userId)) || ($code == "" && $userId == "")) {


    $code = $register->addUser($email, $password, $leaderId, $userName, $first_name, $last_name);
    $userId = $register->userId($email, $code);
}
// if already user or new user get is email verified ? for copying data
$isVerifiedEmail = $register->isVerifiedEmail($code);

// if already data is copied just update information to make ontraport user

if ($isVerifiedEmail == 1) {


    $updateUser = $register->updateInformation($userId, $password, $userName);
    $payment->userPaymentsforOntraPort($userId, $chargeAmount, $nex_payment, $planTitle);
    $updateUser = $register->updateUserAndOntraportInfo($userId, $password, $contactId);

} else {
// when new user or it doesn't have data. copying data
    $register->VerifyUserCode($code);
    $register->createUserGroups($code);
    $register->createUserPlans($code);
    $register->createUserProfiles($code);
    $register->createUserSignature($code);
    $register->addPermissionEmail($code);
    $updateUser = $register->updateUserAndOntraportInfo($userId, $password, $contactId);
    $payment->userPaymentsforOntraPort($userId, $chargeAmount, $nex_payment, $planTitle);
}

// end


?>
