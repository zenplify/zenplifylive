<meta https-equiv="Pragma" content="no-cache">
<meta https-equiv="no-cache">
<meta https-equiv="Expires" content="-1">
<meta https-equiv="Cache-Control" content="no-cache">
<?php
error_reporting(0);
require_once '../../classes/init.php';
require_once '../../classes/register.php';

$register = new register();
$fname = "";
$lname = "";
$email = "";

if (isset($_GET['token']) && empty($_GET['token']) === false) {
    $token = trim($_GET['token']);
    $user = $register->getUserInfo($token);
    $fname = $user->firstName;
    $lname = $user->lastName;
    $email = $user->email;
} else {
    header('Location: https://zenplify.biz/modules/ontraport/sales.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://zenplify.biz/favicon1.ico" type="image/x-icon"/>
    <title>Zenplify Monthly Subscription</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://zenplify.biz/modules/ontraport/css/form-styles.css"/>
</head>
<body>
<div class="container-fluid">
<header>
    <div class="container" id="header-container">
        <img src="https://zenplify.biz/modules/ontraport/images/logo.png" alt="Zenplify"></div>
</header>
<div id="top-container">
    <div class="container text-center">
        <h1 class="top-container-title">Learn how to double your reach in half the time</h1>

        <p class="top-container-description">Zenplify is a step-by-step system that will give you the confidence to
            work virtually and grow your business.<br> The tools, strategies and support system is so simple, you'll
            actually do it. And, you'll be great at it.<br> Don't hesitate - take your business to the next level
            today.</p>
    </div>
</div>
<div id="form-container">
<div class="container">

<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css"
      type="text/css"/>
<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css"/>
<link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css"/>
<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css"/>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css"
      type="text/css"/>
<link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c29936f43" type="text/css"/>
<script type="text/javascript"
        src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c29936f43"></script>
<div class="moonray-form-p2c29936f43 ussr">
    <div class="moonray-form moonray-form-label-pos-stacked" id="order-form">
        <form id="form" class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post"
              accept-charset="UTF-8">
        <h1 class="form-title text-center" style="font-size: 30px;">Zenplify Grandfathered Monthly Subscription</h1>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-25747511768" class="moonray-form-label">First Name</label><input
                    name="firstname" type="text" class="moonray-form-input" id="mr-field-element-25747511768" required
                    value="<?php echo $fname;?>" readonly placeholder/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-144248347263" class="moonray-form-label">Last Name</label><input
                    name="lastname" type="text" class="moonray-form-input" id="mr-field-element-144248347263" required
                    value="<?php echo $lname;?>" readonly placeholder/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-email">
                <label for="mr-field-element-678319724276" class="moonray-form-label">Email</label><input name="email"
                                                                                                          type="email"
                                                                                                          class="moonray-form-input"
                                                                                                          id="mr-field-element-678319724276"
                                                                                                          required
                                                                                                          value="<?php echo $email;?>" readonly
                                                                                                          placeholder/>
            </div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-871785524766" class="moonray-form-label">Billing Address</label><input
                    type="text" name="billing_address1" class="moonray-form-input  moonray-form-payments-element"
                    required id="mr-field-element-871785524766"/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-208106660284" class="moonray-form-label">Billing Address 2</label><input
                    type="text" name="billing_address2" class="moonray-form-input  moonray-form-payments-element"
                    id="mr-field-element-208106660284"/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-271402571117" class="moonray-form-label">Billing City</label><input
                    type="text" name="billing_city" class="moonray-form-input  moonray-form-payments-element" required
                    id="mr-field-element-271402571117"/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text">
                <label for="mr-field-element-269848507130" class="moonray-form-label">Billing Zip</label><input
                    type="text" name="billing_zip" class="moonray-form-input  moonray-form-payments-element" required
                    id="mr-field-element-269848507130"/></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
                <label for="mr-field-element-225418976508" class="moonray-form-label">Billing State</label><select
                    name="billing_state" required class="moonray-form-input  moonray-form-payments-element"
                    id="mr-field-element-225418976508">
                    <option value="">Select...</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">D.C.</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NM">New Mexico</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                    <option value="AB">Alberta</option>
                    <option value="BC">British Columbia</option>
                    <option value="MB">Manitoba</option>
                    <option value="NB">New Brunswick</option>
                    <option value="NL">Newfoundland and Labrador</option>
                    <option value="NS">Nova Scotia</option>
                    <option value="NT">Northwest Territories</option>
                    <option value="NU">Nunavut</option>
                    <option value="ON">Ontario</option>
                    <option value="PE">Prince Edward Island</option>
                    <option value="QC">Quebec</option>
                    <option value="SK">Saskatchewan</option>
                    <option value="YT">Yukon</option>
                    <option value="ACT">(AU) Australian Capital Territory</option>
                    <option value="NSW">(AU) New South Wales</option>
                    <option value="VIC">(AU) Victoria</option>
                    <option value="QLD">(AU) Queensland</option>
                    <option value="AU_NT">(AU) Northern Territory</option>
                    <option value="AU_WA">(AU) Western Australia</option>
                    <option value="SA">(AU) South Australia</option>
                    <option value="TAS">(AU) Tasmania</option>
                    <option value="GP">(AF) Gauteng</option>
                    <option value="WP">(AF) Western Cape</option>
                    <option value="EC">(AF) Eastern Cape</option>
                    <option value="KZN">(AF) KwaZulu Natal</option>
                    <option value="NW">(AF) North West</option>
                    <option value="AF_NC">(AF) Northern Cape</option>
                    <option value="MP">(AF) Mpumalanga</option>
                    <option value="FS">(AF) Free State</option>
                    <option value="_NOTLISTED_">My State is not listed</option>
                </select></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-select">
                <label for="mr-field-element-362415943993" class="moonray-form-label">Billing Country</label><select
                    name="billing_country" required class="moonray-form-input  moonray-form-payments-element"
                    id="mr-field-element-362415943993">
                    <option value="">Select...</option>
                    <option value="US">United States</option>
                    <option value="CA">Canada</option>
                    <option value="GB">United Kingdom</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Åland</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia</option>
                    <option value="BQ">Bonaire</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="VG">British Virgin Islands</option>
                    <option value="BN">Brunei</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="HR">Croatia (Local Name: Hrvatska)</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curacao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="CD">Democratic Republic of the Congo</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="TL">East Timor</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and McDonald Islands</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="CI">Ivory Coast</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="XK">Kosovo</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Laos</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia</option>
                    <option value="MD">Moldova</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar (Burma)</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="KP">North Korea</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestine</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn Islands</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="CG">Republic of the Congo</option>
                    <option value="RE">Réunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russia</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthélemy</option>
                    <option value="SH">Saint Helena</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten</option>
                    <option value="SK">Slovakia (Slovak Republic)</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="KR">South Korea</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syria</option>
                    <option value="ST">São Tomé and Príncipe</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania</option>
                    <option value="TH">Thailand</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UM">U.S. Minor Outlying Islands</option>
                    <option value="VI">U.S. Virgin Islands</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VA">Vatican City</option>
                    <option value="VE">Venezuela</option>
                    <option value="VN">Vietnam</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                </select></div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-separator">
                <div class="moonray-form-element-separator moonray-form-payments-element  moonray-form-payments-element"
                     id="mr-field-element-341343800770">
                    <div class="moonray-form-element-separator-legend"><span>Enter Your Billing Information</span></div>
                </div>
            </div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-cart">
                <div class="moonray-form-payments-element moonray-from-offer-grid"
                     id="mr-field-element-27565683238"></div>
            </div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-paymentmethod">
                <div class="moonray-form-payments-element" id="mr-field-element-443889559712">
                    <div
                        class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-credit-card">
                        <div class="moonray-form-element-wrapper moonray-form-element-html">
                            <div
                                class="moonray-form-credit-card-display credit_cards moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text moonray-form-element-wrapper-contracted moonray-form-element-html">
                                <div class="moonray-form-cc-logo moonray-form-cc-logo-visa"></div>
                                <div class="moonray-form-cc-logo moonray-form-cc-logo-discover"></div>
                                <div class="moonray-form-cc-logo moonray-form-cc-logo-amex"></div>
                                <div class="moonray-form-cc-logo moonray-form-cc-logo-mastercard"></div>
                            </div>
                        </div>
                        <div
                            class="moonray-form-element-wrapper moonray-form-input-type-payment-number moonray-form-input-type-text">
                            <label for="mr-field-element-4134667497187" class="moonray-form-label">Card
                                Number</label><input type="text" name="payment_number"
                                                     id="mr-field-element-4134667497187" required
                                                     class="moonray-form-input"
                                                     pattern="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$"
                                                     data-message="Please enter a valid credit card number"/></div>
                        <div
                            class="moonray-form-element-wrapper moonray-form-input-type-payment-code moonray-form-input-type-text">
                            <label for="mr-field-element-2729688407926" class="moonray-form-label">CVC</label><input
                                type="text" name="payment_code" id="mr-field-element-2729688407926" required
                                class="moonray-form-input" maxlength="4" pattern="[0-9]{3,4}"
                                data-message="Please enter a valid CVC"/></div>
                        <div
                            class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-month moonray-form-input-type-select">
                            <label for="mr-field-element-9832115620377" class="moonray-form-label"></label><select
                                name="payment_expire_month" id="mr-field-element-9832115620377" required
                                class="moonray-form-input">
                                <option value="">Exp. Month</option>
                                <option value="01">01 - January</option>
                                <option value="02">02 - February</option>
                                <option value="03">03 - March</option>
                                <option value="04">04 - April</option>
                                <option value="05">05 - May</option>
                                <option value="06">06 - June</option>
                                <option value="07">07 - July</option>
                                <option value="08">08 - August</option>
                                <option value="09">09 - September</option>
                                <option value="10">10 - October</option>
                                <option value="11">11 - November</option>
                                <option value="12">12 - December</option>
                            </select></div>
                        <div
                            class="moonray-form-element-wrapper moonray-form-input-type-payment-exp-year moonray-form-input-type-select">
                            <label for="mr-field-element-1704782405951" class="moonray-form-label"></label><select
                                name="payment_expire_year" id="mr-field-element-1704782405951" required
                                class="moonray-form-input">
                                <option value="">Exp. Year</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                                <option value="2031">2031</option>
                                <option value="2032">2032</option>
                                <option value="2033">2033</option>
                                <option value="2034">2034</option>
                                <option value="2035">2035</option>
                                <option value="2036">2036</option>
                                <option value="2037">2037</option>
                            </select></div>
                    </div>
                </div>
            </div>
            <div
                class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit">
                <input type="submit" name="submit-button" value="Submit" class="moonray-form-input"
                       id="mr-field-element-44871697667"/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden"
                                                                                            value=""/></div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page"
                                                                                            type="hidden" value=""/>
            </div>
            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden"
                                                                                            value="p2c29936f43"/></div>

        </form>
    </div>
</div>
</div>
</div>
<footer>
    <div class="container">
        <p>Copyrights &copy; <?php echo date('Y'); ?>. All Rights Reserved</p>
    </div>
</footer>
</div>

<script src="https://zenplify.biz/modules/ontraport/js/jquery-1.11.3.min.js"></script>
<script src="https://zenplify.biz/modules/ontraport/js/jquery.validate.min.js"></script>
<script>

    jQuery.noConflict();

    jQuery.validator.setDefaults({
        debug: true
    });

    (function ($) {
        $(document).ready(function () {

            $('#form').validate({
                onfocusout: function (element) {
                    this.element(element);
                },
                onkeyup: function (element) {

                    if ($(element).hasClass('keyup')) {
                        this.element(element);
                    }

                },
                rules: {
                    firstname: "required",
                    lastname: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    billing_address1: "required",
                    billing_city: "required",
                    billing_zip: "required",
                    billing_state: "required",
                    billing_country: "required",
                    payment_number: {
                        required: true,
                        creditcard: true
                    },
                    payment_code: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    payment_expire_month: "required",
                    payment_expire_year: "required"
                },
                messages: {
                    firstname: "Please enter first name.",
                    lastname: "Please enter last name.",
                    email: {
                        required: "Please enter email address.",
                        email: "Please enter a valid email address."
                    },
                    billing_address1: "Please enter your billing address.",
                    billing_city: "Please enter your billing city.",
                    billing_zip: "Please enter your billing zip.",
                    billing_state: "Please select your billing state.",
                    billing_country: "Please select your billing country.",
                    payment_number: {
                        required: "Please enter a credit card number.",
                        creditcard: "Please enter a valid credit card number."
                    },
                    payment_code: {
                        required: "Please enter a valid CVC.",
                        digits: "Please enter a valid CVC.",
                        minlength: "Please enter a valid CVC.",
                        maxlength: "Please enter a valid CVC."
                    },
                    payment_expire_month: "Please select a valid expiry month.",
                    payment_expire_year: "Please select a valid expiry year."
                }
            });
        });
    })(jQuery);
</script>
</body>
</html>