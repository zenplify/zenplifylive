<?php

include '../../classes/init.php';
require_once("../../classes/register.php");
require_once("../../classes/payment.php");
$register = new register();
$payment = new payment();
$duration=30;

$email = $_GET['email'];
$chargeAmount = $_GET['amount'];
$nex_payment = date('Y-m-d', strtotime("+".$duration." days"));
$planTitle='Zen-Monthly-Recharge';

$code = $register->getEmailVerificationCode($email);
$userId = $register->userId($email, $code);
$payment->userPaymentsforOntraPort($userId, $chargeAmount, $nex_payment,$planTitle);
$register->updateuserInfo($userId, $code);

?>
