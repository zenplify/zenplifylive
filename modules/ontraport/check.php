<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

require_once '../../classes/init.php';

$action = $_REQUEST['action'];
switch ($action) {

    case 'username':
        $result = checkUserName($_REQUEST['uname']);
        if (isset($_REQUEST['type'])) {
            echo json_encode($result);
        } else {
            echo $result;
        }
        break;
    case 'email':
        $result = checkEmail($_REQUEST['email']);
        if (isset($_REQUEST['type'])) {
            echo json_encode($result);
        } else {
            echo $result;
        }
        break;

}
function checkUserName($userName)
{
    if (preg_match('/^[a-z\d_]{2,20}$/i', $userName)){

    $database = new database();
    $checkUserName = $database->executeScalar("SELECT u.userId FROM `user` u JOIN userpayments up ON u.`userId`=up.`userId` WHERE (u.`userName` = '" . $userName . "' or u.`signupName`='" . $userName . "') AND u.`isVerifiedEmail`='1' AND u.`isVerifiedPaypal`='1' GROUP BY u.`userId`");

    if (empty($checkUserName)) {

        return 0;
    } else {

        return 1;

    }
    }
    else{
        return 2;
    }

}

function checkEmail($email)
{

    $database = new database();

    $user = $database->executeObject("SELECT u.`userName`,up.`expiryDate` FROM `user` u JOIN userpayments up ON u.`userId`=up.`userId` WHERE u.`email` = '" . $email . "' AND u.`isVerifiedEmail`='1'  GROUP BY u.`userId`");
    if (!$user) {
        return 0;
    } else if ($user->expiryDate < date('Y-m-d')) {
        return '0' . ',' . $user->userName;
    } else {
        return 1;
    }

}

?>