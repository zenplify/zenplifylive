<?php 
session_start();
include_once("../headerFooter/header.php");
require_once('../../classes/contact.php');
require_once("../../classes/group.php");
require_once("../../classes/plan.php");
//include('../../login_process/logincheck.php');
$contact= new contact();
$group= new group();
$plan =new plan();
?>
 
     
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
   
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
  
<script src="../../js/timepicker/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">	      
	  <script src="../../js/alert/jquery.toastmessage.js" type="text/javascript"></script> 
	  <!-- validation file included  -->
		<script type="text/javascript" src="../../validation/jquery.validate.js"></script>
		
		
	<?php require_once("../../classes/resource.php"); ?>
      
<script>
$(document).ready(function() {
$('#date_of_birth').datepicker();
});

</script>
<!--  <script type="text/javascript" src="../../js/jquery-1.8.2.js"></script>-->
<script>

$(document).ready(function(e) {
    	   $("input[id^='step']").change(function(){
			var id=$(this).val();  
   $('#step_list'+id).toggle('show');
});
});
</script>
<?php

//ON FORM SUBMISSION FOLLONG ACTION SHOULD BE TAKEN
if(isset($_POST['submit'])){
$contactId=$contact->addContact($_POST);	
$contact->addContactPhoneNumberByContactId($contactId,$_POST);
$contact->addContactEmailByContactId($contactId,$_POST);
$contact->addContactAddressByContactId($contactId,$_POST);
$contact->addContactcallTimeByContactId($contactId,$_POST);
$contact->assignPlanStep($contactId,$_POST);
$contact->assignGroupTo($contactId,$_POST);	
//$contact->assignPlanStep($contactId,$_POST);	
//sendredirect('../contact/contact_detail.php?contactId='.$contactId);
?>
<script>
$().toastmessage('showSuccessToast', "Contact  Added Successfully");		
</script>

<?php 	}

	?>	
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <h1 class="gray">Add Quick Contact</h1>
     </div>
      <div class="sub_container" style="padding-bottom:60px;">
      <div class="first_content" style="width:555px !important ;">   
<form method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="addContactForm" name="addContactForm">
      <table cellspacing="0" cellpadding="0">
          <tr>
          	<td class="label" style="width:0% !important;">Title</td>
            <td><input type="text" name="title" class="textfield "  /></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">First Name</td>
            <td><input type="text" name="firstname" class="textfield required"  /></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Last Name</td>
            <td><input type="text" name="lastname" class="textfield required"  /></td>
          </tr>
          <tr>
          <td class="label" style="width:0% !important;">Email</td>
          <td colspan="2">
          <input class="textfield required email" name="email" type="text"></td>
          </tr>
          <tr>
          <td class="label" style="width:0% !important;">Phone</td>
          <td colspan="2" >
          <input class="textfield" name="phone" type="text"></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Company Name</td>
            <td><input type="text" name="companyname" class="textfield"  /></td>
          </tr>
          <tr>
          	<td class="label" style="width:0% !important;">Job Title</td>
            <td><input type="text" name="jobtitle" class="textfield"  /></td>
          </tr>
          
          <tr>
          <td class="label" style="width:0% !important;">Birthday</td>
          <td  colspan="2">
          
          <input class="textfield" name="dateOfBirth" type="text" id="date_of_birth"></td>
          </tr>
           <tr>
        <td class="label" style="width:0% !important;">Street Address</td>
        <td >
        <input class="textfield" name="street" type="text"></td>
        </tr>
        <tr>
        <td class="label" style="width:0% !important;">City</td>
        <td >
        <input class="textfield" name="city" type="text"></td>
        </tr>
        <tr>
        <td class="label" style="width:0% !important;">State/Prov</td>
        <td >
        <input class="textfield" name="state" type="text"></td>
        </tr>
        <tr>
        <td class="label" style="width:0% !important;">Zip/Postal</td>
        <td >
        <input class="textfield" name="zip" type="text"></td>
        </tr>
        <tr>
        <td class="label" style="width:0% !important;">Country</td>
        <td >
        <input class="textfield" name="country" type="text"></td>
        </tr>
          <tr>
          <td class="label" style="width:0% !important;">Referred By</td>
          <td colspan="2">
          <input class="textfield" name="referred_by" type="text"></td>
          </tr>
          <tr>
          <td class="label" style="padding-bottom:68px; width:0% !important;">Best Times To Call</td>
          <td style="padding-left:5px;">
          <div style="clear:both; margin-bottom:5px;">
          <input  name="BestTimestoCallWeekdays" value="Weekdays" type="checkbox">
          <label  class="label" style="height: 12px;margin-bottom: 10px;">Weekdays</label>
          </div>
          <div style="clear:both; margin-bottom:5px;">
          <input  name="BestTimestoCallWeekdayEvenings" value="Weekday Evenings" type="checkbox">
          <label  class="label" style="height: 12px;margin-bottom: 10px;">Weekday Evenings</label>
          </div>
          <div style="clear:both; margin-bottom:5px;">
          <input name="BestTimestoCallWeekendDays" value="Weekend Days" type="checkbox">
          <label class="label" style="height: 12px;margin-bottom: 10px;">Weekend Days</label>
          </div>
          <div style="clear:both; margin-bottom:5px;">
          <input name="BestTimestoCallWeekendDays" value="Weekend Days" type="checkbox">
          <label class="label" style="height: 12px;margin-bottom: 10px;">Weekend Evenings</label>
         </div>
          </td>
          </tr>
       <tr>
     
     <td >&nbsp;</td>
     <td >&nbsp;</td>
   
    </tr>
           <tr>
     
     <td >&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" class="save" value=""/><input type="button" name="cancel" class="cancel" value="" onclick="history.go(-1)"/></td>
    </tr>
         
        </tbody>
        </table>
        </div>
 <div class="seventh_content" style="border:none !important;width:428px !important ;">     
 <h1 class="gray">Follow-ups</h1>
    <table class="contact_table" style="width:428px;" cellspacing="0" cellpadding="0">
    
        <?php
			$showPlan=$plan->showPlans($_SESSION["userId"]);//show all groups sort by group type
            foreach($showPlan as $sp){
			?>
            <tr>
          	<td class="plan_label1" ><input type="checkbox" id="step<?php echo $sp->planId;?>" value="<?php echo $sp->planId;?>" /></td>
            <td class="plan_label" ><?php echo $sp->title;?></td>
			<td>
			<select id="step_list<?php echo $sp->planId;?>" name="planStepDropDown[]" style="display:none;" class="SelectField">
                    <?php 
					$planStep=$plan->showPlanSteps($sp->planId);
					
					foreach($planStep as $ps){
						
					?>
   					 <option value="<?php echo $ps->stepId;?>" ><?php echo $ps->summary;?></option>
    				<?php }?>
					</select>
			</td>
          </tr>
				
			<?php		
				}
			?>

</table>
</div >
<div class="seventh_content" style="width:390px !important ; border:none !important;">
<h1 class="gray">Groups</h1>
<table class="contact_table" style="width:500px; padding-left:4px;" cellspacing="0" cellpadding="0">
	<?php
			$showGroup=$group->showGroups($_SESSION["userId"]); //show all groups sort by group type
            foreach($showGroup as $sg){
	?>
		<tr style="background-color:<?php echo $sg->color;?>">
          	<td class="plan_label1"><input type="checkbox" id="groupcheckbox[]" name="groupcheckbox[]" value="<?php echo $sg->groupId; ?>"></td>
            <td class="plan_label"><?php echo $sg->name;?></td>
          </tr>
      
	<?php
	}
	?>
	</table>       
      </form>  
</div>        
        
        <div style="clear:both"></div>
       </div>
      
    <?php include_once("../headerFooter/footer.php");?>