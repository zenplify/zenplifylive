<?php
session_start(); 
include_once("../headerFooter/header.php");
require_once('../../classes/contact.php');
$userId=$_SESSION['userId'];
$contact=new contact();

//$data=$contact->GetUserContacts(46);
$permissions=$contact->GetAllPermissions();
$transferContacts=$contact->GetAllTransferContacts($userId);

?>

 
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>

 <script type="text/javascript" src="../../js/alert/jquery.toastmessage.js"></script>
 <link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">
  
  
<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				

			        $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra']})
					.tablesorterPager({container: $("#pager"),size:50}); 
	});
	</script>
	
	<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <h1 class="gray">Contact Transfer Requests</h1>
     </div>
    
     <div class="top_content">
       
     <input id="accept" type="button" value="" onclick="return okTransfer()">&nbsp;&nbsp;&nbsp;
     <input id="reject" type="button" value="" onclick="return rejectTransfer()">  	
       	
     
     </div>
      <div class="sub_container">
       <div class="col_table">     

	<form name="view_contact" method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="view_contact"> 
	
	 	
	
	
	
	<table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
	<thead>
                <tr class="header">
                    <th class="first_table"><input type="checkbox" id="chkSelectDeselectAll" onclick="SelectDeselectContact(this)"/><label></label></th>
                    <th class="center big"> Name <img src="../../images/bg.gif" /></th>
                    <th class="center big"> Transferred By <img src="../../images/bg.gif" /></th>
                    <th class="center exbig">Transfer Date <img src="../../images/bg.gif" /></th>
                    <th class="last_table"></th>
				</tr>
	</thead>

	<tbody>
	
	<?php 
	if(!empty($transferContacts))
	{
		foreach ($transferContacts as $row)
		{
			//echo "<tr class=\"odd_gradeX\" id=\"2\">";
			$contact_id=$row->contactId;
			$name=$contact->GetContactName($contact_id);
			$userName=$contact->GetUserName($userId);
			
				echo "<tr class=\"odd_gradeX\" id=\"".$contact_id."\">";
				echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$contact_id."></td>";
				echo "<td>".$name->firstName."</a></td>";
				echo "<td>".$userName->firstName." ".$userName->lastName."</td>";
				echo "<td>".date("m-d-Y", strtotime($row->requestedDate))."</td>";
				echo "</tr>";
			
		}
	}
	else
	   echo '<html><head><META http-equiv="refresh" content="0;URL=http://'.$_SERVER['SERVER_NAME'].'/modules/contact/view_all_contacts.php"></head></html>';
	?>
	
	</tbody>
	
</table>
</form>
<table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="margin-top:10px; float:right;">
                               
                                   
                                    <div class="pager" id="pager">
	<form>
		<img class="first_page" src="../../images/paging_far_left.gif">
		<img class="prev_page" src="../../images/paging_left.gif">
		<input type="text" class="pagedisplay" value="1/1">
		<img class="next_page" src="../../images/paging_right.gif">
		<img class="last_page" src="../../images/paging_far_right.gif">
		
        <select id="pagesize" name="pagesize" class="pagesize"><option value="10" selected="selected">10</option><option value="100">100</option><option value="250">250</option></select>       
	</form>
</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
           <!--<div class="pmnavcontainer">
				<ul>
					<li><a id="tab1" href="tabs_permission_mngr.php?id=1" style="border-bottom-left-radius:10px;" >UnProcessed (5)</a></li>
					<li><a id="tab2" href="tabs_permission_mngr.php?id=2">Pending (1)</a></li>
					<li><a id="tab3" href="tabs_permission_mngr.php?id=3">Subscribed (1)</a></li>
					<li><a id="tab4" href="tabs_permission_mngr.php?id=4">Unsubscribed (0)</a></li>
                    <li><a id="tab5" href="tabs_permission_mngr.php?id=5" style="border-bottom-right-radius:10px;">Bounced (4)</a></li>
               </ul>
			</div>-->
      </div>  
      
      <div class="col_search" style=" margin-left:10px;">
      
                <p class="search_header"><label for="kwd_search">Find Contacts</label></p>
           		<input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
                 <br />
               <!-- <p style="padding-left:5px; font-weight:bold;"> Filter by Status</p>-->
                
                <div id="groups_list" style="display:block;">
                  <form name="myform" action="" method="post">	
                	<ul>
                	
                		<?php 
                		foreach ($permissions as $row1)
                		{
                			echo "<li><input type=\"checkbox\" name=\"list\" value=\"".$row1->title."\"  onClick=\"check(document.myform.list)\" /><label>".$row1->title."</label></li>";
                		}
                		
                		?>
                    </ul>
                   </form> 
                </div>
                
           </div>
            	
       </div>
       		
            
       
    <?php 
		include_once("../headerFooter/footer.php");
	?>
	
	
 <script type="text/javascript">

 

 function okTransfer()
	{
	  var contactArray=new Array();
		
			var length=document.view_contact.select_contact.length;
			var field=document.view_contact.select_contact;
			var check=0;

			if(length!=undefined)
			{
				j=0;
				for (i = 0; i < length; i++)
				{
					if(field[i].checked ==true )
					{
						
						contactArray[j]=field[i].value;
						j++;
						check=1;
							
					}
				}
				
			}
			else //If there is only 1 item
			{
					
				if(field.checked==true)
				{
						check=1;
						contactArray=field.value;
				}		
			}
			
			if(check==1)
			{
				var userId="<?php echo $userId;?>";
				//alert(contactArray);
				$.ajax({ url: '../../ajax/ajax.php?con='+contactArray,
			        data: {action: 'acceptTransferContact',userId:userId},
			        type: 'post',
			        success: function(output) {
				        //alert(output)

						//window.write(output);
			        	//$().toastmessage('showSuccessToast', "Transfer Successfully");
		        		setTimeout('reload()', 800);
			           }
				});
			}
			else
			{
				alert("Please select atleast one contact");
			}
	}




 function rejectTransfer()
	{
	  var contactsArray=new Array();
		
			var length=document.view_contact.select_contact.length;
			var field=document.view_contact.select_contact;
			var check=0;

			if(length!=undefined)
			{
				j=0;
				for (i = 0; i < length; i++)
				{
					if(field[i].checked ==true )
					{
						
						contactsArray[j]=field[i].value;
						j++;
						check=1;
							
					}
				}
				
			}
			else //If there is only 1 item
			{
					
				if(field.checked==true)
				{
						check=1;
						contactsArray=field.value;
				}		
			}
			
			if(check==1)
			{
				if(confirm('Are you sure you want to reject this Contact Transfer Request ?'))
				{
					$.ajax({ url: '../../ajax/ajax.php?con='+contactsArray,
			        data: {action: 'rejectTransferContact'},
			        type: 'post',
			        success: function(output) {

				      
				        if(output=='1')
					        {
				        		//$().toastmessage('showSuccessToast', "Reject Successfully");
				        		setTimeout('reload()', 800);
					        }

				        //location.reload();
					           }
					});
				}
			}
			else
			{
				alert("Please select atleast one contact");
			}
	}
	




 
 
 $(document).ready(function(){
		// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function(){
			// When value of the input is not blank
			if( $(this).val() != "")
			{
				// Show only matching TR, hide rest of them
				$("#contact_table tbody>tr").hide();
				$("#contact_table td:contains-ci('" + $(this).val() + "')").parent("tr").show();
			}
			else
			{
				// When there is no input or clean again, show everything back
				$("#contact_table tbody>tr").show();
			}
		});

		
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"], 
	{
	    "contains-ci": function(elem, i, match, array) 
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});

	
	function check(field)
	{

		//alert("hi");
		$("#contact_table tbody>tr").hide();

		var list = new Array();
		var j=0;
	 	for (i=0;i<field.length;i++)
	 		{
				if (field[i].checked==true)
				{	
					list[j]=field[i].value;	
					j++;
					
				}
			
	 		}
		var arrayLength=list.length;
		if(arrayLength!=0)
			{
				for (k=0;k<arrayLength;k++)
	 				{
						$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
	 				}
			}
		else
			{
				$("#contact_table tbody>tr").show();
			}
 

 	}


	function reload() { 
		location.reload();
		}

	</script>