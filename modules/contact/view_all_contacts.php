<?php 
session_start();
error_reporting(0);
	require_once("../../classes/init.php");
include_once("../headerFooter/header.php");
require_once('../../classes/contact.php');
require_once('../../classes/group.php');
require_once('../../classes/profiles.php');
$userId=$_SESSION['userId'];
$leaderId=$_SESSION['leaderId'];
error_reporting(0);

//$_SESSION['groupsArray']='';
$contact=new contact();
	$profile = new profiles();
	$database = new database();
$group=new group();
$user=$contact->GetUserName($userId);
$getPermissionEmail=$contact->GetPermissionEmail($userId);
if ($_GET['archive'] == 'success')
{
	$mes= 'Contact has been Archived Successfully.';
}
if (isset($_GET['message']))
{
	$message= $_GET['message'];
}

if(empty($_SESSION['groupsArray']))
{
	$data=$contact->GetAllContacts($userId);
}
else
{
	$groupsArray=implode(',',$_SESSION['groupsArray']);
	$data=$contact->GetGroupContacts($userId,$groupsArray);
}
$transferContacts=$contact->GetAllTransferContacts($userId);
?>

 <script type="text/javascript" charset="utf-8">
 function removejscssfile(filename, filetype){
 var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
 var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
 var allsuspects=document.getElementsByTagName(targetelement)
 for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
 }
}
removejscssfile("jquery-ui-timepicker-addon.js", "js");
removejscssfile("jquery-ui.js", "js");

removejscssfile("jquery-ui.css", "css") ;//remove all occurences "somestyle.css" on page
</script>
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
 <script type="text/javascript" src="../../js/script.js"></script>
 <script type="text/javascript" src="../../js/nicEdit.js"></script>
 <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function () {
    <?php if(isset($_REQUEST['data'])){ ?>
    //$("#kwd_search1").css('background','url("../../images/wait.gif") no repeat');
    var srch = "<?php echo $new=$_REQUEST['data']; ?>";
    $.ajax({
        type   : "POST",
        url    : "../../classes/ajax.php",
        data   : {action: 'searchInfo', searchdata: srch, userId:<?php echo $userId;?>},
        success: function (data) {
            $("#kwd_search1").css('background', 'url("../../images/search-icon.png")no-repeat center right');
            $("#tableBody").html(data);
	                
			$("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
        }
    });
    <?php } ?>
    $('#chkSelectDeselectAll').change(function () {
        var checkboxes = $(this).closest('form').find(':checkbox');
        if($(this).is(':checked')) {
            checkboxes.attr('checked', 'checked');
        }
        else {
            checkboxes.removeAttr('checked');
        }
    });
    $("#pmManager").click(function () {
        if(CheckSelectedPermissionContacts()) {
            var arr = [];
            var i = 0;
            $('.ads_Checkbox:checked').each(function () {arr[i++] = $(this).val();});
            var contactIds = arr.toString();
            $.ajax({url: 'optInMessage.php', type: 'post', data: {contactsids: contactIds}, success: function (result) {
                emails = result.split('&');
                var from = emails[0];
                var to = emails[1];
                $("#to").val(to);
                $("#from").val(from);
                $("#emailId").val(emails[2]);
                $("#contactId").val(emails[3]);
                $("#emailflag").val(1);
                loadPermissionManager();
            }});
        }
        else {alert('Please select atleast one contact');}
    });
    //UncheckAll();
    //get_group_contacts();
    <?php if(!isset($_REQUEST['data'])){ ?>
    $("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
    <?php } ?>
    $('*[id^=groupName]').click(function (e) {
        get_group_contacts();
    });
    $('*[id^=interestlevel]').click(function (e) {
        if($(this).attr('id') == 'interestlevel5') {
            if($(this).is(':checked')) {
                $('#interestlevel1').attr('checked', true);
                $('#interestlevel2').attr('checked', true);
            }
            else {
                $('#interestlevel1').attr('checked', false);
                $('#interestlevel2').attr('checked', false);
            }
        }
        get_interest_level();
    });
    $('#transfer').click(function () {
        if(CheckSelected()) {
            loadPopupTransfer();
            centerPopupTransfer();
        }
        else {
            alert('Please select atleast one contact');
        }
    });
    $('#export_data').click(function () {
        if(CheckSelected()) {
            contacts = get_contacts();
            window.location.href = '../import/export_data.php?u=<?php echo $userId; ?>&con=' + contacts;
        }
        else {
            alert('Please select atleast one contact');
        }
    });
    $("#backgroundPopup").click(function () {
        disablePopupTransfer();
    });
    $(".cancel").click(function () {
        disablePopupTransfer();
    });
    $("#get_User").click(function () {
        var name = $("#userName").val();
        $.ajax({
            url: '../../ajax/ajax.php',
            data: {action: 'verifyUser', u_name: name},
            type: 'post',
            success: function (output) {
                var data = JSON.parse(output);
                if (data['status'] == 'success') {
                    transfer(data['id']);
                    disablePopupTransfer();
                }
                else {
                    // Show errors if any
                    alert(data['error']);
                }
                // disablePopupTransfer();
            }
        });
        //disablePopupTransfer();
    });
    $('#archiveContacts').click(function () {
        var arrContacts = [];
        if(CheckSelected()) {
            arrContacts = get_contacts();
            $.ajax({ url: '../../classes/ajax.php?contactId=' + arrContacts,
                data    : {action: 'archiveMultipleContacts'},
                type    : 'post',
                success : function (output) {

                    //$('.messageofsuccess').html('Contacts have been Restored Successfully');
                    window.location.href = 'view_all_contacts.php?message=Contact(s) have been archived successfully';
                    //alert(output)
                }
            });
        }
        else {
            alert('Please select atleast one contact');
        }
    });
    /*$("#pagesize").change(function(){
     $.ajax({
     type: "POST",
     url: "../../classes/ajax.php",
     data: {action:'paginationOfContact',records:$(this).val(),userId:
    <?php //echo $userId;?>},
     success: function(data){
     $("#tableBody").html(data)


     }
     });


     });*/
    $("#sendpermissionconfirmation").change(function () {
        alert('hi');
    });
});
function sendPermission(contactId) {
    $.ajax({ url: '../../classes/ajax.php',
        data    : {action: 'sendPermissionToBlock', contactId: contactId},
        type    : 'post',
        success : function (output) {
            loadPermissionManager();
            $("#to").val(output);
            //alert(output)
        }
    });
    $("#contactId").val(contactId);
}
function permissionMail() {
    var to = $("#to").val();
    var from = $("#from").val();
    var subject = $("#subject").val();
    var emailbody = $("#body").val();
    var userId = $("#userId").val();
    var contactId = $("#contactId").val();
    var emailId = $("#emailId").val();
    var emailflag = $("#emailflag").val();
    var area2 = $(".nicEdit-main").html();
    $.ajax({ url: '../../classes/ajax.php',
        data    : {action: 'sendPermissionMail', to: to, from: from, subject: subject, emailbody: emailbody, userId: userId, contactId: contactId, emailId: emailId, emailflag: emailflag, area2: area2},
        type    : 'post',
        success : function (output) {

            //if(output==1){
            disablepermissionPopup();
            //}
            $("#messageofsuccess").html('Thank you for confirming your Email Address and approving future Emails');
        }

    });
}
function archiveContacts() {
    var arrContacts = [];
    if(CheckSelected()) {
        arrContacts = get_contacts();
        $.ajax({ url: '../../classes/ajax.php?contactId=' + arrContacts,
            data    : {action: 'archiveMultipleContacts'},
            type    : 'post',
            success : function (output) {

                //$('.messageofsuccess').html('Contacts have been Restored Successfully');
                window.location.href = 'view_all_contacts.php?message=Contact(s) have been archived successfully';
                //alert(output)
            }
        });
    }
    else {
        alert('Please select atleast one contact');
    }
}
</script>
<!--  <div id="menu_line"></div>-->
<div class="container">
<div class="top_content">
    <!--<form name="contacts_form" id="contacts_form" action="#" method="">-->
    <a href="new_contact.php"><input type="button" id="new_contact" value=""/></a>
    <!--<a href="permission_manager.php"><input type="button" id="permission_manager" value=""  /></a>-->
    <div style="float:left; margin-left:135px; width:270px; margin-top:6px;"><font class="messageofsuccess"><?php echo($mes == "" ? "" : $mes); ?></font></div>
    <!--<input type="button" id="transfer_contact" value="transfer_contact"  />
     <div id="popupTransfer">
      <h1>List of Transfer Contacts</h1>
      <form>
      <table cellpadding="0" cellspacing="0" border="1px">
      <tr>
      <th> Name </th>
      <th> Action </th>
      </tr>
      <?php
        foreach($transferContacts as $trow)
        {
            $id = $row->contactId;
            //echo "<tr class=\"odd_gradeX\" id=\"".$id."\">";
            //echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
            echo "<td><a href=contactdetail.php?id=".$id.">".$row->firstName." ".$row->lastName."</a></td>";
            echo "<td>".$row->lastName."</td>";
            echo "<td>".$row->companyTitle."</td>";
            echo "<td>".$row->jobTitle."</td>";
            echo "<td></td>";
            echo "<td><a href=\"javascript:\" ><img src=\"../../images/block.png\" width=\"17\" title=\"Delete\" alt=\"Delete\" height=\"17\" onClick=\"deleteStaff(".$id.")\" ></a></td>";
            echo "<td></td>";
            echo "</tr>";

        }
    ?>
      </table>

      <br/>

      <input type="button" class="transfer_btn" id="get_User" />
      <input type="button" class="cancel" style="margin-left:10px;" />

      </form>
      </div>-->
    <p id="sorting">
        <img src="../../images/contact transfericon.png" title="Transfer Contact" alt="Transfer Contact" onclick="return dataTransfer()"/>
        <a href="../import/import_contact.php"><img src="../../images/import.png" alt="Import" title="Import"/></a>
        <img src="../../images/export.png" alt="Export" title="Export" id="export_data"/>
        <img src="../../images/permission manager.png" title="Permission Manager" alt="Permission Manager" id="pmManager"/>
        <a href="javascript:void(0)" id="archiveContacts"><img src="../../images/Archive-iconnew.png" title="Archive Contacts" alt="Archive Contacts" onclick="archiveContacts();"/></a>
        <a href="view_archived_contacts.php"><img src="../../images/restoreContacts.png" alt="View Archived Contacts" title="View Archived Contacts" height="25"/></a>
        <img src="../../images/printer.png" alt="Print" title="Print" onclick="return PrintContent()"/></p>

    <div style="float:left;margin-left:135px; width:100%; margin-top:4px; margin-bottom:3px"><font class="messageofsuccess" id="messageofsuccess"><?php echo $message; ?></font></div>
</div>
<div class="sub_container">
<div class="col_table">
    <form name="view_all_contact" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" id="view_all_contact">
        <div id="prints">
            <!--
            <table id="myTable" cellpadding="0" cellspacing="0" border="0" class="tablesorter" >
            -->
            <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter">
                <thead>
                <tr class="header">
                    <!--
                    <th class="first_table"><input type="checkbox" id="chkSelectDeselectAll" onclick="SelectDeselectContact(this)"/><label></label></th>
                     -->
                    <th class="first_table"><input type="checkbox" id="chkSelectDeselectAll"/><label></label></th>
                    <th class="center medcol3">Name <img src="../../images/bg.gif"/></th>
                    <th class="center medcol">Time <img src="../../images/bg.gif"/></th>
                    <th class="center medcol">Profiles <img src="../../images/bg.gif"/></th>
                    <th class="center medcol3">Interest <img src="../../images/bg.gif"/></th>
                    <th class="center medcol">Phone</th>
                    <th class="center medcol2">GP Date</th>
                    <th class="center smallcol">Status</th>
                    <th class="center smallcol">&nbsp;</th>
                    <th class="last_table"></th>
                </tr>
                </thead>
                <tbody id="tableBody">
                <?php
                    if(!empty($data) && empty($_REQUEST['data']))
                    {
                        foreach($data as $row)
                        {
                            $id = $row->contactId;
                            $gmtDiff = $row->gmt;
                            //$contactAddress=$contact->showContactAddress($id);
                            //$gmtDiff=$contactAddress->gmt;
                            //$contactPermissionManager=$contact->showContactpermission($id);
                            //$profiles = $contactemailStep = $contact->countEmailStepOfContact($id);
                            //$contactProfiles=$contact->getContactProfiles($id);
                            $contactProfiles = $row->profiles;
                            $date = date('g:i a');
                            $offset = $gmtDiff*60*60; //converting 5 hours to seconds.
                            $timeFormat = "g:i a";
                            $currentTime = gmdate($timeFormat, time()+$offset);
                            //$currentTime=gmdate("g:i a", strtotime($date) + (3600 * $offset) );
                            $client = strtolower("CLIENT");
                            $interest = str_replace($client, "PRODUCTS", strtolower($row->interest));
                            $matched = false;
                            if(strstr($interest, 'team leader'))
                            {
                                $matched = true;
                                $interest = str_replace(strtolower('TEAM LEADER,'), "IC,", $interest);
                                $interest = str_replace(strtolower(',TEAM LEADER'), ",IC", $interest);
                                $interest = str_replace(strtolower('TEAM LEADER'), "IC", $interest);
                            }
                            if((strstr($interest, 'sales consultant')) || (strstr($interest, 'SALES CONSULTANT')))
                            {
                                if($matched)
                                {
                                    $interest = str_replace(strtolower('SALES CONSULTANT'), "", $interest);
                                }
                                else
                                {
                                    $matched = true;
                                    $interest = str_replace(strtolower(',SALES CONSULTANT'), ",IC", $interest);
                                    $interest = str_replace(strtolower('SALES CONSULTANT,'), "IC,", $interest);
                                    $interest = str_replace(strtolower('SALES CONSULTANT'), "IC", $interest);
                                }
                            }
                            if(!$matched)
                            {
                                $interest = str_replace(strtolower('INDEPENDENT CONSULTANT'), "IC", $interest);
                            }
                            else
                            {
                                $interest = str_replace(strtolower(',INDEPENDENT CONSULTANT'), "", $interest);
                                $interest = str_replace(strtolower('INDEPENDENT CONSULTANT,'), "", $interest);
                                $interest = str_replace(strtolower('INDEPENDENT CONSULTANT'), "", $interest);
                            }
                            $interest = str_replace(strtolower(',,'), ",", $interest);
                            $interest = str_replace(strtolower(', ,'), ",", $interest);
                            if(trim($interest) == 'IC,')
                            {
                                $interest = str_replace(",", "", $interest);
                            }
                            //$interest=str_replace(strtolower('TEAM LEADER'),"TL",$interest);
                            //$interest=str_replace(strtolower('SALES CONSULTANT'),"SC",$interest);
                            $interest = str_replace(strtolower('VIRTUAL HOST'), "HOST", $interest);
                            $interest = str_replace("PRODUCTS", "CLIENT", $interest);
                            $guestProfileDate = $row->addDate;
                            if($guestProfileDate == '' || $guestProfileDate == NULL)
                            {
                                $guestProfileDate =date('m/d/Y', strtotime($row->contactAddDate));
                            }
                            else
                            {
                                $guestProfileDate = date('m/d/Y', strtotime($guestProfileDate));
                            }
                            echo "<tr class=\"odd_gradeX\" id=\"".$id."\">";
                            //echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
                            echo "<td><input type='checkbox' name='contact[]' class='ads_Checkbox' id='contact'  value=".$row->contactId." ></td>";
                            echo "<td><a href=contact_detail.php?contactId=".$id.">".$row->firstName." ".$row->lastName."</a></td>";
                            echo "<td>".$currentTime."</td>";
                            echo "<td>".$contactProfiles."</td>";
                            echo "<td>".strtoupper($interest)."</td>";
                            echo "<td>".$row->phoneNumber."</td>";
                            echo "<td>".$guestProfileDate."</td>";
                            echo "<td>";
                            echo "<img class='traficLight' src='".$row->imagePath."' onclick=".($row->statusId != 3 ? "sendPermission('$id')" : "sendPer('$id')")." title='".$row->title."' />";
                            //echo"<img class='traficLight' src='".($row->statusId==1 || $contactemailStep!=0 ?"../../images/orange.png":($row->statusId==2|| $contactemailStep!=0 ?"../../images/yellow.png":($row->statusId==3 ?"../../images/green.png":($row->statusId==4 || $contactemailStep!=0 ?"../../images/red.png":''))))."' onclick=".($row->statusId!=3 || $contactemailStep!=0 ?"sendPermission('$id')":"sendPer('$id')")." title='".($row->statusId==1 ?"Unprocessed":($row->statusId==2?"Pending":($row->statusId==3 ?"Subscribed":($row->statusId==4 ?"Unsubscribed":''))))."' />";
                            echo "</td>";
                            echo "<td></td>";
                            echo "</tr>";

                        }
                    }
                    if(empty($data) && empty($_REQUEST['data']))
                    {
                        echo "<tr><td colspan='8' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>
    </form>
    <div style="clear:both"></div>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
        <tr style="border:10px">
            <?php if (!empty($transferContacts)){ ?>
            <td><a href="transfer_contact_requests.php"> View Transfer Requests</a>
                <?php
                    }
                    else{
                ?>
            <td>
                <?php } ?>

                <div id="pager" class="pager" style="float:right; margin-top:5px; ">
                    <form>
                        <span class="total"></span>
                        <img src="../../images/paging_far_left.gif" class="first"/>
                        <img src="../../images/paging_left.gif" class="prev"/>
                        <input type="text" class="pagedisplay"/>
                        <img src="../../images/paging_right.gif" class="next"/>
                        <img src="../../images/paging_far_right.gif" class="last"/>
                        <select id="my_pagesize" name="pagesize" class="pagesize">
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="250">250</option>
                        </select>
                    </form>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col_search">
    <!-- <p class="search_header"><label for="kwd_search">Find Contacts</label></p>

        <input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
      <br />-->
    <!--  <img src="images/check.png" /> Include Custome Fields-->
    <p class="search_header"><label for="kwd_search">Filter By:</label></p>

    <?php if (($leaderId==3658 || $userId==3658) OR ($leaderId==3741 || $userId==3741) OR ($leaderId==3793 || $userId==3793)){
 $style='style="display:none;"';
    }?>

    <p class="search_header2 small" <?php echo $style; ?> >Interest Level</p>

    <div id="groups_list">
        <form name="myform" action="" method="post">
            <ul <?php echo $style; ?>>
                <li style="display:none;"><input type="checkbox" name="list" value="leader" id="interestlevel1"/><label>Leader</label></li>
                <li style="display:none;"><input type="checkbox" name="list" value="Consultant" id="interestlevel2"/><label>Consultant</label></li>
                <li><input type="checkbox" name="list" value="Host" id="interestlevel3"/><label>Referral Program</label></li>
                <li><input type="checkbox" name="list" value="Client" id="interestlevel4"/><label>Customer </label></li>
                <li><input type="checkbox" name="list" value="independent consultant" id="interestlevel5"/><label>Business</label></li>
            </ul>

            <!--<ul>
                    <li><input type="checkbox" name="list" value="TL"  onClick="check(document.myform.list)" id="interestlevel1" /><label>Leader</label></li>
                    <li><input type="checkbox" name="list" value="SC" onClick="check(document.myform.list)" id="interestlevel2" /><label>Consultant</label></li>

                    <li><input type="checkbox" name="list" value="HOST" onClick="check(document.myform.list)" id="interestlevel3"  /><label>Host</label></li>
                    <li><input type="checkbox" name="list" value="PRODUCTS" onClick="check(document.myform.list)" id="interestlevel4"  /><label>Client</label></li>


                </ul>-->
        </form>
    </div>
    <p class="search_header2 small"><a href="../group/groups_view.php">Groups </a></p>

    <div id="groups_list">
        <ul>
            <?php $groups = $group->showGroups($userId);

                foreach($groups as $rowGroup)
                {
                    echo '<li style="background-color:'.$rowGroup->color.'">';
                    if(in_array($rowGroup->groupId, $_SESSION['groupsArray']))
                        echo '<input type="checkbox" checked="checked"  name="groups[]" id="groupName'.$rowGroup->groupId.'" value="'.$rowGroup->groupId.'"/>';
                    else
                        echo '<input type="checkbox" name="groups[]" id="groupName'.$rowGroup->groupId.'" value="'.$rowGroup->groupId.'"/>';
                    echo '<label>'.$rowGroup->name.'</label></li>';
                }
            ?>
        </ul>
    </div>
</div>
<div class="empty"></div>
<div class="empty"></div>
<div class="empty"></div>
<div class="empty"></div>
</div>
<?php
    $from = $contact->GetEmail($userId);


?>
<div id="backgroundPopup"></div>
<div id="popupTransfer">
    <h1>Transfer Contacts</h1>

    <form>
        <label class="survey_label">User Name</label>
        <input type="text" class="textfield" name="userName" id="userName"/>
        <br/>
        <input type="button" class="transfer_btn" id="get_User"/>
        <input type="button" class="cancel" style="margin-left:10px;"/>
    </form>
</div>
<div id="Popup"></div>
<div id="popuppermission">
    <h1 class="gray">Email Permission Confirmation</h1>

    <form action="?action=success" method="post">
        <input type="hidden" name="contactId" id="contactId"/>
        <input type="hidden" name="emailId" id="emailId"/>
        <input type="hidden" name="emailflag" id="emailflag"/>
        <input type="hidden" name="userId" id="userId" value="<?php echo $userId; ?>"/>
        <table width="100%" border="0">
            <tr>
                <td class="label">From</td>
                <td><input type="text" name="from" id="from" readonly="readonly" class="textfieldbig" value="<?php echo $from; ?>"/></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="label">To</td>
                <td><input type="text" name="to" id="to" readonly="readonly" class="textfieldbig" value="<?php echo $to; ?>"/></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="label">Subject</td>
                <?php     $subject = $getPermissionEmail->permissionEmailSubject; $subject = str_replace("[{First Name}]","$user->firstName","$subject");?>
                <td><input type="text" name="subject" id="subject" class="textfieldbig" value="<?php echo $subject; ?>"/></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="padding-left:12px;"><textarea name="area2" id="body" style="width:648px; height:200px;  ">
                    </textarea>

                    <div style="background-color:#EAEAEA;width:640px; padding:5px; padding-bottom:0px; font-size:12px;height: 155px; overflow: auto; display: block;margin-bottom:1px;">
                        <p style="font-size:12px; width: 100%;">
	                        <?php if ($leaderId == 0) { $consultantTitleUserId =$userId; }
	                        else { $consultantTitleUserId = $leaderId; }
		                        $consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
	                        //

                            $AcceptLink="https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013";
                            $declineLink="https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u";

                            $body = $getPermissionEmail->permissionEmailBody;

                            $body= str_replace("[{Accept Emails Link}]","$AcceptLink","$body");
                            $body= str_replace("[{DeclineEmails Link}]","$declineLink","$body");
                            $body= str_replace("[{First Name}]","$user->firstName","$body");
                            //
                            echo $body;
                            ?>

                        </p>
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="button" name="submit" class="sendpermissionconfirmation" value="" onclick="permissionMail();"/><input type="button" name="cancel" class="cancel" onclick="disablepermissionPopup();"/></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</div>



<?php

    include_once("../headerFooter/footer.php");
?>
<script type="text/javascript">
    var contactArray = new Array();
    $(document).ready(function () {
        // Write on keyup event of keyword input element
        $("#kwd_search").keyup(function () {
            // When value of the input is not blank
            var size = $(this).val().length;
            var srch = $(this).val();
            if(size >= 3) {
                $.ajax({
                    type   : "POST",
                    url    : "../../classes/ajax.php",
                    data   : {action: 'searchInfo', searchdata: srch, userId:<?php echo $userId;?>},
                    success: function (data) {
                        $("#tableBody").html(data);
                        $("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
                    }


                });
            }
            else if(size == 0) {
                $.ajax({
                    type   : "POST",
                    url    : "../../classes/ajax.php",
                    data   : {action: 'searchInfo', searchdata: srch, userId:<?php echo $userId;?>},
                    success: function (data) {
                        $("#tableBody").html(data);
                        $("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
                    }
                });
            }
        });
    });
    // jQuery expression for case-insensitive filter
    $.extend($.expr[":"], {
        "contains-ci": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });
    function check(field) {
        $("#contact_table tbody>tr").hide();
        var list = new Array();
        var j = 0;
        for (i = 0; i < field.length; i++) {
            if(field[i].checked == true) {
                list[j] = field[i].value;
                j++;
            }
        }
        var arrayLength = list.length;
        if(arrayLength != 0) {
            for (k = 0; k < arrayLength; k++) {
                $("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
            }
        }
        else {
            $("#contact_table tbody>tr").show();
        }
        //alert (list.length);
    }
    function PrintContent() {
        var DocumentContainer = document.getElementById('prints');
        var WindowObject = window.open('', "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
    function dataTransfer() {
        var length = document.view_all_contact.contact.length;
        var field = document.view_all_contact.contact;
        var check = 0;
        //alert(length);
        if(length != undefined) {
            j = 0;
            for (i = 0; i < length; i++) {
                if(field[i].checked == true) {
                    contactArray[j] = field[i].value;
                    j++;
                    check = 1;
                }
            }
        }
        else //If there is only 1 item
        {
            if(field.checked == true) {
                check = 1;
                contactArray = field.value;
            }
        }
        if(check == 1) {
            //alert("here");
            //loadEmailPopup();
            //centerEmailPopup();
            loadPopupTransfer();
            centerPopupTransfer();
        }
        else {
            alert("Please select atleast one contact");
        }
    }
    function get_contacts() {
        var contactsArray = new Array();
        var length = document.view_all_contact.contact.length;
        var field = document.view_all_contact.contact;
        var check = 0;
        //alert(length);
        if(length != undefined) {
            j = 0;
            for (i = 0; i < length; i++) {
                if(field[i].checked == true) {
                    contactsArray[j] = field[i].value;
                    j++;
                    check = 1;
                }
            }
        }
        else //If there is only 1 item
        {
            if(field.checked == true) {
                check = 1;
                contactsArray = field.value;
            }
        }
        return contactsArray;
    }
    function cancelTransfer() {
        disableEmailPopup();
    }
    function transfer(userId) {

        //contact=$.serialize(contactArray);
        //alert (userId);
        // send request user id
        var s_user_id =<?php echo $_SESSION['userId'] ?>;
        $.ajax({ url: '../../ajax/ajax.php?con=' + contactArray,
            data    : {action: 'transferContact', user_id: userId, SUserId: s_user_id},
            type    : 'post',
            success : function (output) {
                if (output > 0) {                
                	alert("Warning! Some of Your already sent contacts are still pending from reciever side.");
            	}
                disablePopupTransfer();
            }
        });
    }
    function UncheckAll() {
        var w = document.getElementsByTagName('input');
        for (var i = 0; i < w.length; i++) {
            if(w[i].type == 'checkbox') {
                w[i].checked = false;
            }
        }
    }
    function get_group_contacts() {
        var Arraygroups = [];
        $("*[id^=groupName]").each(function () {
            var groupId = $(this).val();
            if($(this).is(':checked')) {
                Arraygroups.push(groupId);
            }
        });
        //alert(Arraygroups)
        $.ajax({
            type   : "POST",
            url    : "get_group_contacts.php?groupIds=" + Arraygroups,
            data   : {groupIds: Arraygroups},
            success: function (data) {
                $("#tableBody").html(data)
                $("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
            }
        });
    }
    function get_interest_level() {
        var Arraygroups = [];
        $("*[id^=interestlevel]").each(function () {
            var groupId = $(this).val();
            if($(this).is(':checked')) {
                Arraygroups.push(groupId);
            }
        });
        $.ajax({
            type   : "POST",
            url    : "get_intrest_level.php?levelIds=" + Arraygroups,
            data   : {levelIds: Arraygroups},
            success: function (data) {
                $("#tableBody").html(data);
                $("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
            }
        });
    }
</script>