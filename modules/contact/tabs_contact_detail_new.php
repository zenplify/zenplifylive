<?php
session_start();
error_reporting(0);
require_once ('../../classes/init.php');
require_once("../../classes/contact.php");
require_once("../../classes/group.php");
require_once("../../classes/plan.php");
require_once("../../classes/task.php");
require_once("../../classes/appiontment.php");
require_once("../../classes/profiles.php");
$database=new database();
$contact= new contact();
$group= new group();
$plan =new plan();
$task =new task();
$appiontment= new appiontment();
$profiles=new Profiles();
$contactId=$_POST['cid'];
$userId= $_SESSION["userId"];

$contactdetail=$contact->showContactDetail($contactId,$database);
$contactaddress=$contact->showContactAddress($contactId,$database);
$conatctemail=$contact->showContactEmailAddress($contactId,$database);
//print_r($conatctemail);
$conatctphone=$contact->showContactPhoneNumber($contactId,$database);
$contacttimetocall=$contact->showContactTimeForCall($contactId,$database);
//$stickyNotes=$profiles->contactNotes($contactId);
$stickyNotes=$task->showStickyNotes($database,$contactId);
//current and upcoming activities
$activities=$task->upcomingActivities($database,$userId,$contactId);
$appiontmentactivities=$appiontment->upcomingActivitiesAppiontments($userId,$contactId);
//activities History
$activitiesCompleted=$task->ActivitiesHistory($database,$userId,$contactId);
$status=$contact->GetContactPermissionStatus($contactId,$database);
//Notes to show in contact Activity History
$notes=$task->contactNotes($database,$contactId);
$p = $_GET['id'];

	switch($p) {
		case "1":
		$profileName='guestProfile';
		$results=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileName);
		//print_r($results);
		if(!empty($results['pageId']) && !empty($results['replyId']) )
		{
			$pageId=$results['pageId'];
			$replyId=$results['replyId'];
			$questions=$profiles->getPageQuestions($database,$pageId);
		}
		break;
		case "3";
		$profileName='skinCareProfile';
		
		$results=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileName);
		//print_r($results);
		if(!empty($results['pageId']) && !empty($results['replyId']) )
		{
			$pageId=$results['pageId'];
			$replyId=$results['replyId'];
			$questions=$profiles->getPageQuestions($database,$pageId);
		}
		break;
		case "4";
		$profileName='fitProfile';
		
		$results=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileName);
		//print_r($results);
		if(!empty($results['pageId']) && !empty($results['replyId']) )
		{
			$pageId=$results['pageId'];
			$replyId=$results['replyId'];
			$questions=$profiles->getPageQuestions($database,$pageId);
		}
		break;
		case "5";
		$profileName='consultantProfile';
		$results=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileName);
		//print_r($results);
		if(!empty($results['pageId']) && !empty($results['replyId']) )
		{
			$pageId=$results['pageId'];
			$replyId=$results['replyId'];
			$questions=$profiles->getPageQuestions($database,$pageId);
		}
		
		break;
		case "6";
		
	
		break;
		
	}

?>
 <!--<link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">	      
	  <script src="../../js/alert/jquery.toastmessage.js" type="text/javascript"></script>-->
<script type="text/javascript">

            
jQuery(document).ready(function(){				
				
				$("#preloader").hide();				
				jQuery("[id^=editTab]").click(function(){	
					
					// get tab id and tab url
					tabId = $(this).attr("id");	
					tabUrl = jQuery("#"+tabId).attr("href");
					jQuery("[id^=tab]").removeClass("current");
					jQuery("#"+tabId).addClass("current");
					
					// load tab content
					loadTabContent(tabUrl);
					return false;
				});
				
				/*$("#change").click(function(){$('#navcontainer').css({"display":"none"});$('#nav_container').show();});*/
			});
function delupcomingActivities(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {taskId:id,action:'deleteTask',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	     $('#record'+id).fadeOut(1000);
		   }
 });
	//$('#record'+id).fadeOut(1000);
	}
function delupcomingAppiontments(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {appiontmentId:id,action:'deleteAppiontment',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordAppiontment'+id).fadeOut(1000);
		   }
 });
	}	
function delnotes(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {notesId:id,action:'deleteNotes',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordNotes'+id).fadeOut(1000);
		
   }
 });
	}
function saveNotes(){

if($('#stickyNotes').val()==""){
	
	}else{
	$.ajax({type: "POST",url: "../../classes/ajax.php",data: {stickyNotes:$('#stickyNotes').val(),action:'checkStickyNotes',contactId:'<?php echo $contactId;?>'},
   	success: function(msg){
		
	   if(msg!=''){
		  $('#stickyNotes').val(msg); 
		 
		 
		   }else{
	     //alert('error');
			   }
   }
 });
	
	}
   
	}
function moveToHistory(){
	
if($('#stickyNotes').val()==""){
	
	}else{
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {stickyNotes:$('#stickyNotes').val(),action:'moveToHistory',contactId:'<?php echo $contactId;?>',noteId:'<?php echo $stickyNotes->noteId; ?>'},
   success: function(msg){
	    $('#stickyNotes').val('');
	  $('.appendnotes tr:last').after(msg);
	   
   }
 });
	}
	}	
function editTask(editType){
				var taskId=$("#taskId").val();
				window.location.href='../task/edit_task.php?edit_type='+editType+'&task_id='+taskId+'&contactId='+<?php echo $contactId;?>;
				}
function archiveContact(contactId){
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {action:'archiveContact',contact_id:contactId},
   success: function(msg){
	  
	   if(msg=='1'){
		window.location.href="view_all_contacts.php?archive=success";
		   }
   }
 });
	}	
	function markTaskCompleted(taskId){
			
				var userId=<?php echo $userId; ?>;
				var contactId=<?php echo $contactId; ?>;
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{taskId:taskId,userId:userId,contactId:contactId,action:'updateTaskAgainstContact'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+taskId).fadeOut('500');
		    			}
		    		}
		    		});
				
				}	
				
				function sendPermission(contactId){
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionToBlock',contactId:contactId},
    	        type: 'post',
    	        success: function(output) {
        	     loadPermissionManager();
				   $("#to").val(output);
				      	           }
    	});
		
		$("#contactId").val(contactId);
		}	
	function permissionMail(){
		var to=$("#to").val();
		var from=$("#from").val();
		var subject=$("#subject").val();
		var emailbody=$("#body").val();
		var userId=$("#userId").val();
		var contactId=$("#contactId").val();
		var emailId=$("#emailId").val();
		var emailflag=$("#emailflag").val();
		
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionMail',to:to,from:from,subject:subject,emailbody:emailbody,userId:userId,contactId:contactId,emailId:emailId,emailflag:emailflag},
				type: 'post',
    	        success: function(output) {
					
        	       if(output==1){
					
					   disablepermissionPopup();}
				  $("#messageofsuccess").html('Thank you for confirming your Email Address and approving future Emails');
				  
				  }
				  
    			});
			}							
</script>	
<?php
	$p = $_GET['id'];

	switch($p) {
		case "1":
?>
	<div class="top_content" style="width:855px;">
     <a href="../task/new_task.php?contact_id=<?php echo $contactId;?>"><input type="button" style="margin-left:10px;" id="new_task" value=""  /></a>
    
     <a href="../appiontment/new_appiontment.php?contact_id=<?php echo $contactId;?>"><input type="button" style="margin-left:10px;" id="new_appointment" value=""  /></a>
     <a href="../webmail/messages.php"><input type="button" style="margin-left:10px;" id="newemail" value=""  /></a>
     <!--<input type="button" style="margin-left:5px;" id="archive" value="" id="archive_contact" onclick="archiveContact('<?php //echo $contactId; ?>')" />-->
      <?php     
 /* if(!empty($results['replyId']))
  { */
  
  ?>
   	<a href="tabs_contact_edit.php?id=1&contact_id=<?php echo $contactId;?>" id="editTab1" class="editContact">Edit Profile</a>
    
    <?php // } ?>
     </div>
      
 	
<div class="firstContent">        
<table cellspacing="0" cellpadding="0">
          <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Address</td>
            <td class="label"><?php echo $contactaddress->street.' '.$contactaddress->city.' '.$contactaddress->state.' '. $contactaddress->zipcode.' '.$contactaddress->country; ?></td>
          </tr>

<?php
$status=$contact->GetContactPermissionStatus($contactId);
?>
			<tr>
          	<td class="label">Email <span style="float:right;"><?php if($status=="Subscribed"){?> <a href="javascript:"><img src="../../images/green.png" style="vertical-align:middle;" title="Subscribed"/></a><?php }elseif($status=="Unprocessed"){?><a href="javascript:"><img src="../../images/orange.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Unprocessed"/></a> <?php }elseif($status=="Pending"){?><a href="javascript:"><img src="../../images/yellow.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Pending"/></a> <?php }elseif($status=="Unsubscribed"){?><a href="javascript:"><img src="../../images/red.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Unsubscribed"/></a> <?php }?></span></td>
            <td class="label">
 	<?php echo $conatctemail->email; ?></td>
          </tr>

          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          <tr>
          <td class="label" >Best Times To Call</td>
            <td class="label"><?php echo $contacttimetocall->weekdays."<br>".$contacttimetocall->weekdayEvening."<br>".$contacttimetocall->weekenddays."<br>".$contacttimetocall->weekenddayEvening; ?></td>
          </tr>
          <td class="label">Referred By</td>
          <td class="label">
          <?php echo $contactdetail->referredBy;  ?></td>
          </tr>
          <?php $dateofbirth = $contactdetail->dateOfBirth;
			if($dateofbirth=='0000-00-00'){
			$dateOfBirth="";
			}else{
			$dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
			}
          ?>
		  <tr>
          <td class="label">Birthday</td>
          <td class="label">
           <?php echo $dateOfBirth;  ?></td>
          </tr>
          
          <td class="label">Age</td>
          <td  class="label">
            <?php 
	function getAge($then) {
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
    return $age;
}
if($dateofbirth=='0000-00-00'){
			
			}else{
			echo (getAge($contactdetail->dateOfBirth)<0?'0':getAge($contactdetail->dateOfBirth)).' Years';
			}


	  ?></td>
          </tr>
          
          <tr>
          	<td class="label">Company /Title</td>
            <td class="label"><?php echo $contactdetail->companyTitle.'-'.$contactdetail->jobTitle;  ?></td>
          </tr>
          
</table>
</div>

<div class="secondContent">
<div class="t_heading gray">Campaigns</div>

 <table class="contact_table" style="width:278px;" cellspacing="0" cellpadding="0">
    
        <?php
			$stepcount=0;
			$showPlan=$plan->showPlanStepsInContactDetail($database,$contactId);
			if(empty($showPlan)){
				echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Step Assign</span>";
				echo "</td></tr>";
				}else{
            foreach($showPlan as $sp){
				
			
		//$stepId=$plan->getLastStepOfPlan($sp->planId,$contactId);
		$stepId=$plan->CheckLastStepOfPlan($database,$sp->planId,$contactId);
		
		  $steptasktype=$plan->getPlanStepType($database,$sp->stepId);
		  $planIsDoubleOpt=$plan->getPlanType($database,$sp->planId);
		 $isContactUnsubscribed=$plan->getContactPermissionStatus($database,$contactId);
		
		echo "<tr><td colspan='2'>";
				echo "<span class='label' style='padding-left:16px;font-weight:bold;'>".$sp->title."</span>";
				echo "</td></tr>";
				
				if(!empty($stepId->stepId)){
			?>
            
            <td class="plan_label" style="padding-left:18px; font-size:12px  !important; " ><span height:25px; width:200px; padding:2px 20px 2px 5px>Completed</span></td>
			
          </tr>
            <?php }else{
				
				
				?>
            <tr>
          	
            <td class="plany_label" style="padding-left:18px; padding-bottom:5px; font-size:12px  !important; background-color:<?php echo(($stepId->stepId==$sp->stepId && $steptasktype==0 && $planIsDoubleOpt==1 && $isContactUnsubscribed==3)?"#FF0000":"")?>;"  onclick="<?php echo(($stepId->stepId==$sp->stepId && $steptasktype==0 && $planIsDoubleOpt==1 && $isContactUnsubscribed==3)?'blockAlert()':'')?>"><span  style="height:25px; width:200px; padding:2px 20px 2px 5px"><?php echo ($sp->isActive==0?'':'Skipped:').$sp->summary;?></span></td>
			
          </tr>
          <tr>
          	
			<?php
			}
					//$stepcount++;
					
				}
			}
			?>
			

</table>
</div>

<div class="sticky_notes">
			<img src="../../images/note--top.png" />	
            <textarea class="stickynotes" onblur="" id="stickyNotes"><?php echo $stickyNotes->notes;?></textarea>
            
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:inline-block; padding-left:64px; width:238px; height:22px; cursor:pointer; " onclick="moveToHistory();" >Move To History</span>
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:inline-block; padding-left:64px; width:238px; height:22px; cursor:pointer; " onclick="saveNotes();" >Save Note</span></div>
<div class="secondContent">
<div class="t_heading gray">Groups</div>
<div id="groupslist">
                	<ul>

<?php
$contactgroups=$group->showContactGroups($database,$contactId);
if(empty($contactgroups)){
echo "<tr><td>&nbsp;</td><td>";
echo "<span class='label'>No Group Assign</span>";
echo "</td></tr>";
}else{
foreach($contactgroups as $cg){

$linked=$group->isGroupLinked($database,$cg->groupId,$userId);
if(empty($linked)){$groupflag=1;}else{$groupflag=0;}
$count++;
?>
<li style="background-color:<?php echo $cg->color;?>"><input type="checkbox" checked="checked" id="groupcheckbox[]" name="groupcheckbox[]" value="<?php echo $cg->groupId; ?>" disabled="disabled"><label class="plan_label"><?php echo $cg->name;?></label><label style="float:right;"><?php if($groupflag!=1){ $count=0;?>
<img src="../../images/link.gif" style="cursor:pointer;" width="17" height="17" title="<?php foreach($linked as $lg){echo ($count==0?"":","); echo $lg->title; $count++;} ?>"/>
<?php }else{ echo '&nbsp;';}?></label></li>

<?php }
}
?>

</ul>

 </div>


</div>
 <?php if(!empty($questions)){
    	foreach($questions as $q)
		{
			
			echo '<div class="thirdContent">
        
        	<table border="0" cellpadding="0" width="100%">
           
                <tr>
                <td  class="question_label bullet">'.$q->question.'</td></tr>';
                
				$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if($q->typeId==1 || $q->typeId==2)
				{
					echo' <tr>';
							
							if(!empty($answer->answer))
							echo '<td  class="answer_label">'.$answer->answer;
							else
							echo '<td  class="answer_label">'.$answer->answerText;
							echo ' </td></tr>';
				}
				else
				{
					foreach($answer as $a)
					{
						echo' <tr>';
						if($a->answer==NULL && $a->value==NULL)
						{
							echo '<td  class="answer_label">'.$a->answerText;
						}
						else
						{
							echo '<td  class="answer_label">'.$a->choiceText;
							if($a->value!=NULL)
							echo '&nbsp;&nbsp;&nbsp;'.$a->value;
						}
							echo '</td>
						</tr>';
					}
					
				}
        echo' </table>       
         
        </div> ';
		}
        }?> 
       
        <div class="thirdContent" style="padding-left:15px; padding-bottom:40px;">
        <div class="table_heading">Current & Upcoming Activities</div>
	
	   <div class="table_col">
              <table width="100%" cellpadding="0" cellspacing="0" id="contact_table">
                <tr class="header">
                    <th class="first"></th>
                    <th class="center">Summary</th>
                      <th class="center">Date</th>
                       <th class="center">Time</th>
                        <th class="center">Who</th>
                       <th class="center">Actions</th>
                    <th class="last"></th>
                </tr>
         <?php if(empty($activities)&&empty($appiontmentactivities)){
			 echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
			 }else{?>       
         <?php foreach ($activities as $act){
			 if($act->isActive){
				 
				 }else{
			 $datetime = $act->dueDateTime;
			$date = date('m/d/Y', strtotime($datetime));
			((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
			
			$contactIds=$task->getcontactId2($database,$act->taskId,$userId);
			
         
         
         	  echo "<tr class=\"odd_gradeX\" id='task".$act->taskId."'>";  ?>
                    <?php  echo "<td><input type='checkbox' name='task[]' class='ads_Checkbox' id='contact".$act->taskId."'  value=".$act->taskId." onChange=\"markTaskCompleted('".$act->taskId."')\" ></td>"; ?>
                    <td><?php echo $act->title;?></td>
                    <td><?php echo $date;?>
                    <img  src="<?php echo ($act->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($act->repeatTypeId!=1?'recurs ':'')?>"/>	
                    </td>
                     <td><?php echo $time;?></td>
                      
                      <td><?php 
					   foreach($contactIds as $ci){
									//$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$ci->contactId.">".$ci->firstName." ".$ci->lastName."</a><br>";
														}
					  ?></td>
                      	<?php if($act->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $act->taskId;?>')" />
							<img onclick=<?php echo ($act->planId==0?"delupcomingActivities('$act->taskId')":"skipTask('$act->taskId')")?> src="<?php echo ($act->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($act->planId==0?'Archive':'Skip')?>" title="<?php echo ($act->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="../task/edit_task.php?task_id=<?php echo $act->taskId; ?>&contactId=<?php echo $contactId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($act->planId==0?"delupcomingActivities('$act->taskId')":"skipTask('$act->taskId')")?> src="<?php echo ($act->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($act->planId==0?'Archive':'Skip')?>" title="<?php echo ($act->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }?>
													
                      <!--<td><a href="../task/edit_task.php?task_id=<?php //echo $act->taskId;  ?>"><img src="../../images/edit.png" title="edit" alt="edit" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingActivities('<?php //echo $act->taskId;  ?>');" /></td>-->
                      <td></td>
                 </tr>
					
         <?php } }?>      
         <?php foreach ($appiontmentactivities as $app){
			  $datetime = $app->endDateTime;
			$date = date('m/d/Y', strtotime($datetime));
			((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
			 ?>
         	  <tr id="recordAppiontment<?php echo $app->appiontmentId;  ?>">
                    <td></td>
                    <td><?php echo $app->title;?></td>
                    <td><?php echo $date;?>
                     <img  src="<?php echo ($app->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($app->repeatTypeId!=1?'recurs ':'')?>"/>	
                    </td>
                     <td><?php echo $time;?></td>
                      
                      <td>
                      
                      </td>
                      
                      <?php if($app->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur()" />
							<img onclick="delupcomingAppiontments('<?php echo $app->appiontmentId; ?>')" src="../../images/Archive-icon.png" alt="Archive" title="Archive" />
								</td>        
													<?php }else{?>
                                <td><a href="../appiontment/edit_appiontment.php?appId=<?php echo $app->appiontmentId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick="delupcomingAppiontments('<?php echo $app->appiontmentId; ?>')" src="../../images/Archive-icon.png" alt="Archive" title="Archive" />
                                </td>
                                                <?php }?>
						
                     <!-- <td><a href="edit_task.php"><img src="../../images/edit.png" title="edit" alt="edit" onclick="editAppiontment('<?php// echo $app->appiontmentId;  ?>');" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingAppiontments('<?php// echo $app->appiontmentId;  ?>');" /></td>-->
                      <td></td>
                 </tr>
					
         <?php } }?>  
                   </table>
               
       		</div>
	   </div>
<div class="thirdContent" style="padding-left:15px; padding-bottom:40px;">
	<div class="table_heading">History</div>
	   <div class="table_col">
              <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="appendnotes">
                <tr class="header" >
                    <th class="first"><input type="checkbox" /><label></label></th>
                    <th class="center">Summary</th>
                      <th class="center">Date</th>
                       <th class="center">Time</th>
                        <th class="center">Who</th>
                       <th class="center">Actions</th>
                    <th class="last"></th>
                </tr>
                  <?php
          foreach($notes as $n){
			  $datetime = $n->addedDate;
			$date = date('m/d/Y', strtotime($datetime));
			((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
			?>
			<tr id="recordNotes<?php echo $n->noteId;  ?>">
                    <td><img src="../../images/sticky_notes.jpg" style="cursor:pointer; width:15px; height:15px;" id="img<?php echo $n->noteId;?>"></td>
                    <td><?php echo $n->notes;?></td>
                    <td><?php echo $date;?></td>
                     <td><?php echo $time;?></td>
                      
                      <td><?php echo "<a href=../contact/contact_detail.php?contactId=".$n->contactId.">".$n->name."</a>"; ?></td>
                      <td><img src="../../images/delete.png" alt="delete" title="delete" style="margin-left:25px;" class="linkButton" onclick="delnotes('<?php echo $n->noteId;  ?>');" /></td>
                      <td>&nbsp;</td>
                 </tr>
			<?php }?>
                <?php
				$nodate='1990-12-12';
				 foreach ($activitiesCompleted as $actCmplt){
					$datetime = $actCmplt->completedDate;
			$date = date('m/d/Y', strtotime($datetime));
			((date('H:i:s', strtotime($datetime)))!='00:00:00'?$time=date('g:i a', strtotime($datetime)):$time='');
			$contactIds=$task->getcontactId2($database,$actCmplt->taskId,$userId);
					?>
         	  <tr id="record<?php echo $actCmplt->taskId;  ?>">
                    <td><input type="checkbox" name="contact<?php echo $actCmplt->taskId;  ?>"   /><label></label></td>
                    <td  class='cross'><?php echo $actCmplt->title;?></td>
                    <td  class='cross'><?php echo ( strtotime($date)< strtotime($nodate)?'':$date);?></td>
                     <td  class='cross'><?php echo $time;?></td>
                      
                      <td><?php 
					   foreach($contactIds as $cc){
									//$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$cc->contactId.">".$cc->firstName." ".$cc->lastName."</a><br>";
														}
					  ?></td>
                      <td><a href="../task/edit_task.php?task_id=<?php echo $actCmplt->taskId; ?>&contactId=<?php echo $contactId; ?>"><img src="../../images/edit.png" title="edit" alt="edit" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingActivities('<?php echo $actCmplt->taskId;  ?>');" /></td>
                      <td>&nbsp;</td>
                 </tr>
					
         <?php } ?> 
         
               </table>
               
       		</div>
	   </div>
               
<?php		
		break;

		case "3":
		
?>
 
<div class="edit_content" >
<?php     
  if(!empty($results['replyId']))
  { 
  
  ?>
<a href="tabs_contact_edit.php?id=3&contact_id=<?php echo $contactId;?>" id="editTab3" class="editContact">Edit Profile</a>
</div>  

<div class="thirdContent" style="padding-top:10px;">
       
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
           <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          
                
                </tbody>
                </table>
        </div>
   
     <?php }
	 if(!empty($questions)){
    	foreach($questions as $q)
		{
			
			echo '<div class="thirdContent">
        
        	<table border="0" cellpadding="0" width="100%">
           
                <tr>
                <td  class="question_label bullet">'.$q->question.'</td></tr>';
               
				
				$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if($q->typeId==1 || $q->typeId==2)
				{
					echo' <tr>';
							
							if(!empty($answer->answer))
							echo '<td  class="answer_label">'.$answer->answer;
							else
							echo '<td  class="answer_label">'.$answer->answerText;
							echo ' </td></tr>';
				}
				else
				{
					foreach($answer as $a)
					{
						echo' <tr>';
						if($a->answer==NULL && $a->value==NULL)
						{
							echo '<td  class="answer_label">'.$a->answerText;
						}
						else
						{
							echo '<td  class="answer_label">'.$a->choiceText;
							if($a->value!=NULL)
							echo '&nbsp;&nbsp;&nbsp;'.$a->value;
						}
							echo '</td>
						</tr>';
					}
					
				}
				
        echo' </table>       
         
        </div> ';
		}
        } ?> 
 
<?php		
	
		break;

		case "4":
?>
<div class="edit_content">
<?php     
 if(!empty($results['replyId']))
  { 
  
  ?> 
<a href="tabs_contact_edit.php?id=4&contact_id=<?php echo $contactId;?>" id="editTab4" class="editContact">Edit Profile</a>
</div>  

	<div class="thirdContent" style="padding-top:10px;">
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                    <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
                
                
                </tbody>
                </table>
        </div>
        <?php }if(!empty($questions)){
    	foreach($questions as $q)
		{
			
			echo '<div class="thirdContent">
        
        	<table border="0" cellpadding="0" width="100%">
           
                <tr>
                <td  class="question_label bullet">'.$q->question.'</td></tr>';
                
				$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if($q->typeId==1 || $q->typeId==2)
				{
					echo' <tr>';
							
							if(!empty($answer->answer))
							echo '<td  class="answer_label">'.$answer->answer;
							else
							echo '<td  class="answer_label">'.$answer->answerText;
							echo ' </td></tr>';
				}
				else
				{
					foreach($answer as $a)
					{
						echo' <tr>';
						if($a->answer==NULL && $a->value==NULL)
						{
							echo '<td  class="answer_label">'.$a->answerText;
						}
						else
						{
							echo '<td  class="answer_label">'.$a->choiceText;
							if($a->value!=NULL)
							echo '&nbsp;&nbsp;&nbsp;'.$a->value;
						}
							echo '</td>
						</tr>';
					}
					
				}
        echo' </table>       
         
        </div> ';
		}
       }?> 
 
<?php
		break;
	case "5":
?>
<div class="edit_content" >
<?php     
 if(!empty($results['replyId']))
  { 
  
  ?>
<a href="tabs_contact_edit.php?id=5&contact_id=<?php echo $contactId;?>" id="editTab5" class="editContact">Edit Profile</a>
</div>  

<div class="thirdContent" style="padding-top:10px;">
        
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                    <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
                
                
                </tbody>
                </table>
        </div>
         <?php } if(!empty($questions)){
    	foreach($questions as $q)
		{
			
			echo '<div class="thirdContent">
        
        	<table border="0" cellpadding="0" width="100%">
           
                <tr>
                <td  class="question_label bullet">'.$q->question.'</td></tr>';
                
				$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
				if($q->typeId==1 || $q->typeId==2)
				{
					echo' <tr>';
							
							if(!empty($answer->answer))
							echo '<td  class="answer_label">'.$answer->answer;
							else
							echo '<td  class="answer_label">'.$answer->answerText;
							echo ' </td></tr>';
				}
				else
				{
					if($q->questionId==21)
					{
						$radioLables=array('1st Year','1 to 2 Years','2 to 3 Years','3 to 4 Years','5 Plus Years');
						$labCount=0;
						foreach($answer as $a)
						{
							
								echo' <tr>';
								if($a->answer==NULL && $a->value==NULL)
								{
									echo '<td  class="answer_label">'.$radioLables[$labCount].' - '.$a->answerText;
								}
								else
								{
									echo '<td  class="answer_label">'.$a->choiceText;
									if($a->value!=NULL)
									echo '&nbsp;&nbsp;&nbsp;'.$a->value;
								}
									echo '</td>
								</tr>';
								$labCount++;
							
						}
					}
					else
					{
						
					
						foreach($answer as $a)
						{
							echo' <tr>';
							if($a->answer==NULL && $a->value==NULL)
							{
								echo '<td  class="answer_label">'.$a->answerText;
							}
							else
							{
								echo '<td  class="answer_label">'.$a->choiceText;
								if($a->value!=NULL)
								echo '&nbsp;&nbsp;&nbsp;'.$a->value;
							}
								echo '</td>
							</tr>';
						}
					}
				}
        echo' </table>       
         
        </div> ';
		}
        }?> 
  

<?php	
	break;
	case "6":
		
?>
<div class="edit_content">
<a href="tabs_contact_edit.php?id=6&contact_id=<?php echo $contactId;?>" id="editTab6" class="editContact">Edit All</a>
</div>  
<div class="table_heading">Basic Information</div>
    	
<div class="firstContent">        
<table cellspacing="0" cellpadding="0">
          <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
         <tr>
          	<td class="label">Address</td>
            <td class="label"><?php echo $contactaddress->street.' '.$contactaddress->city.' '.$contactaddress->state.' '. $contactaddress->zipcode.' '.$contactaddress->country; ?></td>
          </tr>
          <tr>
          <tr>
          	<td class="label">Email<span style="float:right;"><?php if($status=="Subscribed"){?> <a href="javascript:"><img src="../../images/green.png" style="vertical-align:middle;" title="Subscribed"/></a><?php }elseif($status=="Unprocessed"){?><a href="javascript:"><img src="../../images/orange.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Unprocessed"/></a> <?php }elseif($status=="Pending"){?><a href="javascript:"><img src="../../images/yellow.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Pending"/></a> <?php }elseif($status=="Unsubscribed"){?><a href="javascript:"><img src="../../images/red.png" onclick="sendPermission('<?php echo $contactId; ?>')" style="vertical-align:middle;" title="Unsubscribed"/></a> <?php }?></span></td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          <tr>
          <td class="label" >Best Times To Call</td>
            <td class="label"><?php echo $contacttimetocall->weekdays."<br>".$contacttimetocall->weekdayEvening."<br>".$contacttimetocall->weekenddays."<br>".$contacttimetocall->weekenddayEvening; ?></td>
          </tr>
          <td class="label">Referred By</td>
          <td class="label">
          <?php echo $contactdetail->referredBy;  ?></td>
          </tr>
           <?php $dateofbirth = $contactdetail->dateOfBirth;
			if($dateofbirth=='0000-00-00'){
			$dateOfBirth="";
			}else{
			$dateOfBirth = date('m/d/Y', strtotime($dateofbirth));
			}
          ?>
		  <tr>
          <td class="label">Birthday</td>
          <td class="label">
           <?php echo $dateOfBirth;  ?></td>
          </tr>
          
          <td class="label">Age</td>
          <td  class="label">
            <?php 
	function getAge($then) {
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
    return $age;
}
if($dateofbirth=='0000-00-00'){
			
			}else{
			echo (getAge($contactdetail->dateOfBirth)<0?'0':getAge($contactdetail->dateOfBirth)).' Years';
			}


	  ?></td>
          </tr>
          <tr>
          	<td class="label">Company /Title</td>
            <td class="label"><?php echo $contactdetail->companyTitle.'-'.$contactdetail->jobTitle;  ?></td>
          </tr>
          
</table>
</div>
</div>

<div class="secondContent">
<div class="t_heading gray">Campaigns</div>
<table class="contact_table" style="width:278px;" cellspacing="0" cellpadding="0">
    
        <?php
			$stepcount=0;
			$showPlan=$plan->showPlanStepsInContactDetail($database,$contactId);
			if(empty($showPlan)){
				echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Step Assign</span>";
				echo "</td></tr>";
				}else{
            foreach($showPlan as $sp){
				
			
		//$stepId=$plan->getLastStepOfPlan($sp->planId,$contactId);
		$stepId=$plan->CheckLastStepOfPlan($database,$sp->planId,$contactId);
		
		  $steptasktype=$plan->getPlanStepType($database,$sp->stepId);
		  $planIsDoubleOpt=$plan->getPlanType($database,$sp->planId);
		 $isContactUnsubscribed=$plan->getContactPermissionStatus($database,$contactId);
		
		echo "<tr><td colspan='2'>";
				echo "<span class='label' style='padding-left:16px;font-weight:bold;'>".$sp->title."</span>";
				echo "</td></tr>";
				
				if(!empty($stepId->stepId)){
			?>
            
            <td class="plan_label" style="padding-left:18px; font-size:12px  !important; " ><span height:25px; width:200px; padding:2px 20px 2px 5px>Completed</span></td>
			
          </tr>
            <?php }else{
				
				
				?>
            <tr>
          	
            <td class="plan_label" style="padding-left:18px; padding-bottom:5px; font-size:12px  !important; background-color:<?php echo(($stepId->stepId==$sp->stepId && $steptasktype==0 && $planIsDoubleOpt==1 && $isContactUnsubscribed==3)?"#FF0000":"")?>;"  onclick="<?php echo(($stepId->stepId==$sp->stepId && $steptasktype==0 && $planIsDoubleOpt==1 && $isContactUnsubscribed==3)?'blockAlert()':'')?>"><span  style="height:25px; width:200px; padding:2px 20px 2px 5px"><?php echo ($sp->isActive==0?'':'Skipped:').$sp->summary;?></span></td>
			
          </tr>
          <tr>
          	
			<?php
			}
					//$stepcount++;
					
				}
			}
			?>
			

</table>
</div>

<div class="sticky_notes">
			<img src="../../images/note--top.png" />	
            <textarea class="stickynotes"  id="stickyNotes"><?php echo $stickyNotes->notes;?></textarea>
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:inline-block; padding-left:64px; width:238px; height:22px; cursor:pointer; " onclick="moveToHistory();" >Move To History</span>
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:inline-block; padding-left:64px; width:238px; height:22px; cursor:pointer; " onclick="saveNotes();" >Save Note</span></div>
<div class="secondContent">
<div class="t_heading gray">Groups</div>
<div id="groupslist">
                	<ul>

<?php
$contactgroups=$group->showContactGroups($database,$contactId);
if(empty($contactgroups)){
echo "<tr><td>&nbsp;</td><td>";
echo "<span class='label'>No Group Assign</span>";
echo "</td></tr>";
}else{
foreach($contactgroups as $cg){

$linked=$group->isGroupLinked($database,$cg->groupId,$userId);
if(empty($linked)){$groupflag=1;}else{$groupflag=0;}
$count++;
?>
<li style="background-color:<?php echo $cg->color;?>"><input type="checkbox" checked="checked" id="groupcheckbox[]" name="groupcheckbox[]" value="<?php echo $cg->groupId; ?>" disabled="disabled"><label class="plan_label"><?php echo $cg->name;?></label><label style="float:right;"><?php if($groupflag!=1){ $count=0;?>
<img src="../../images/link.gif" style="cursor:pointer;" width="17" height="17" title="<?php foreach($linked as $lg){echo ($count==0?"":","); echo $lg->title; $count++;} ?>"/>
<?php }else{ echo '&nbsp;';}?></label></li>

<?php }
}
?>

</ul>

 </div>


</div>
 <?php
 $profileNames=array('guestProfile','SkinCareProfile','fitProfile','consultantProfile');
		
		for($i=0;$i<4;$i++)
		{
			$questions=array();
			
			$results=$profiles->checkContactReplyAgainstProfile($database, $contactId, $profileNames[$i]);
			
			if(!empty($results['pageId']) && !empty($results['replyId']))
			{
				$pageId=$results['pageId'];
				$replyId=$results['replyId'];
				$question=$profiles->getPageQuestions($database,$pageId);
				$questions=array_merge($questions,$question);
			}
			 if(!empty($questions))
			{
				if($profileNames[$i]=='guestProfile')
				$profiletitle='Guest Profile';
				if($profileNames[$i]=='SkinCareProfile')
				$profiletitle='Skin Care Profile';
				if($profileNames[$i]=='fitProfile')
				$profiletitle='Fit Profile';
				if($profileNames[$i]=='consultantProfile')
				$profiletitle='Consultant Profile';
				echo '<div style="border-bottom:1px solid #666;width: 700px;display:inline-block;" class="table_heading">'.$profiletitle.'</div>';
			foreach($questions as $q)
			{
				
					echo '<div class="thirdContent">
				
					<table border="0" cellpadding="0" width="100%">
				   
						<tr>
						<td  class="question_label bullet">'.$q->question.'</td></tr>';
						
						$answer=$profiles->getContactAnswerAgainstQuestion($database,$replyId,$q->questionId,$q->typeId);
						if($q->typeId==1 || $q->typeId==2)
						{
							echo' <tr>';
							
							if(!empty($answer->answer))
							echo '<td  class="answer_label">'.$answer->answer;
							else
							echo '<td  class="answer_label">'.$answer->answerText;
							echo ' </td></tr>';
						}
						else
						{
							foreach($answer as $a)
							{
								echo' <tr>';
								if($a->answer==NULL && $a->value==NULL)
								{
									echo '<td  class="answer_label">'.$a->answerText;
								}
								else
								{
									echo '<td  class="answer_label">'.$a->choiceText;
									if($a->value!=NULL)
									echo '&nbsp;&nbsp;&nbsp;'.$a->value;
								}
									echo '</td>
								</tr>';
							}
							
						}
				echo' </table>       
				 
				</div> ';
				}
        	}
			
		}
 
 ?> 


<?php	
	}
	
$from=$contact->GetEmail($userId);	
?>

   
      <div id="Popup"></div>
<div id="popuppermission" >
  <h1 class="gray">Email Permission Confirmation</h1>
  <form action="?action=success" method="post">
   
    <input type="hidden" name="contactId" id="contactId"  />
    <input type="hidden" name="emailId" id="emailId"  />
     <input type="hidden" name="emailflag" id="emailflag"  />
    
     <input type="hidden" name="userId" id="userId"  value="<?php echo $userId;?>" />
    <table width="100%" border="0">
      <tr>
        <td class="label">From</td>
        <td><input type="text" name="from" id="from"  class="textfieldbig" value="<?php echo  $from;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">To</td>
        <td><input type="text" name="to" id="to"  class="textfieldbig" value="<?php echo $to;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">Subject</td>
        <td><input type="text" name="subject" id="subject"  class="textfieldbig" value="Permission Confirmation"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td style="padding-left:12px;"><textarea name="area2" id="body" style="width:648px; height:200px;  ">
    
</textarea>
<div style="background-color:#EAEAEA;width:640px; padding:5px;margin-bottom:15px;">
<p style="font-size:12px">
Please click the link below TO RECEIVE MY NEWSLETTER <br />
<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br />
Click on following links to Unsubscribe <br />
<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span>
</p>
</div>

</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="button" name="submit" class="sendpermissionconfirmation" value="" onclick="permissionMail();"/><input type="button" name="cancel" class="cancel" onclick="disablepermissionPopup();"/></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
</div>