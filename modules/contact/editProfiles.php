<?php
	error_reporting(0);
	session_start();
	include('../../classes/init.php');
	include('../../classes/profiles.php');
	include('../../classes/contact.php');
	include('../../classes/task.php');

	$database = new database();
	$profiles = new Profiles();
	$contact = new contact();
	$task = new task();

	$pageId = '-1';
	$contactId = $_POST['contactId'];
	$userId = $_SESSION['userId'];
	if (!empty($_POST['pageId']))
	{
		$pageId = $_POST['pageId'];
	}
	$pageDetails = $profiles->getPageDetails($database, $pageId);


	$basicInfoQuestions = $profiles->getPageQuestions($database, $pageId, 1, 1);
	if (!empty($basicInfoQuestions))
	{
		$address = $profiles->editMailingAddressCustom($database, $pageId, $contactId, $_SESSION['userId'], $_POST['title'], $_POST['birthdate'], $_POST['referredBy'], $_POST['bestTimeToCall'], $_POST['street'], $_POST['city'], $_POST['state'], $_POST['zipCode'], $_POST['country'], $_POST['companyName'], $_POST['jobTitle'], $basicInfoQuestions);
	}

	$results = $profiles->editContact($database, $_SESSION['userId'], $contactId, $_POST['firstName'], $_POST['lastName'], $_POST['email'], $_POST['phone']);

	$x = 0;
	$groupIds = array();
	foreach ($_POST['groups'] as $groupId)
	{
		$groupIds[$x] = $groupId;
		$x++;
	}
	$currentAssociatedGroups = $contact->getCurrentAssociatedGroups($database, $contactId, $_SESSION['userId']);
	$currentContactGroups = $database->executeScalar("SELECT GROUP_CONCAT(p.`planId` SEPARATOR ',') FROM `planstepusers` psu LEFT JOIN plansteps ps ON psu.`stepId` = ps.`stepId` LEFT JOIN plan p ON ps.`planId` = p.`planId` WHERE contactId = '" . $contactId . "' GROUP BY psu.`contactId`");
	if ($_POST['page'] == 'guestProfile')
	{
		$address = $profiles->editMailingAddress($database, $contactId, $_SESSION['userId'], $_POST['title'], $_POST['birthdate'], $_POST['referredBy'], $_POST['bestTimeToCall'], $_POST['street'], $_POST['city'], $_POST['state'], $_POST['zipCode'], $_POST['country'], $_POST['companyName'], $_POST['jobTitle']);
	}

	$editProfile = false;
	echo 'generated Via is :' . $pageDetails->generatedVia . '<br>';
	echo 'newCode is :' . $pageDetails->newCode . '<br>';
	echo 'loadFirst is:' . $_REQUEST['loadFirst'];

	if (($pageDetails->newCode != 'FitProfile') && ($pageDetails->newCode != 'ConsultantProfile') && ($pageDetails->newCode != 'SkinCareProfile'))
	{
		if ($pageDetails->newCode == 'GuestProfile')
		{
			$editProfile = true;
		}
		else
		{
			if (($pageDetails->generatedVia != 2) && ($pageDetails->generatedVia != 0))
			{
				$editProfile = true;
			}
		}
	}

	//checking contacts created without profile filling and allow plan and groups editing
	if (!$editProfile)
	{
		if ($pageId == '-1')
		{
			$editProfile = true;
		}
	}

	//allowing editing for loadFirst
	if ($_REQUEST['loadFirst'] == '1')
	{
		$editProfile = true;
	}


	if ($editProfile)
	{
		echo 'in editing';
		$profiles->deleteContactPlanLinkedtoGroup($database, $contactId, $_SESSION['userId'], $groupIds);
		$steps = $profiles->assignPlanStep($_SESSION['userId'], $contactId, $_POST);
		//echo '<pre>';print_r($_POST['plan']); print_r($_POST['groups']); echo'</pre><br>';
		foreach ($_POST['groups'] as $groupId)
		{
			$group = $contact->manageAddContactsToGroup($contactId, $groupId);
			$goahead = false;

			$plan = $database->executeObject("SELECT * from plan WHERE groupId='" . $groupId . "' and (userId='" . $userId . "') and isactive = 1");
			$currentContactGroupsArray = explode(",", $currentContactGroups);
			if (in_array($plan->planId, $currentContactGroupsArray))
			{
				foreach ($_POST['plan'] as $postPlan)
				{
					if ($postPlan == $plan->planId)
					{
						echo 'already assigned<br>';
						$goahead = true;
						break;
					}
				}
				//				foreach($_POST['plan'] as $postPlan)
				//				{
				//					if($postPlan == $plan->planId)
				//					{
				//						echo 'go ahead with campaign assigning';
				//						break;
				//					}
				//					else
				//					{
				//						echo 'nope';
				//						return $plan->planId;
				//						exit();
				//					}
				//				}
			}
			else
			{
				$goahead = true;
				//echo 'plan '.$plan->planId.' is not matched<br>';
				//				foreach($_POST['plan'] as $postPlan)
				//				{
				//					//echo $postPlan.'__'.$plan->planId;
				//					//echo '<br>';
				//					if($postPlan == $plan->planId)
				//					{
				//						//echo 'new<br>';
				//						$goahead = true;
				//						break;
				//					}
				//				}
				//return 'cancelled';
			}
			if ($goahead)
			{
				$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $_SESSION['userId'], 1);
			}


			//$task->updateStickyNotes($_POST['stickynotes'],$contactId,$_POST['noteId']);
		}
		if ($_POST['page'] == '6')
		{
			$address = $profiles->editMailingAddress($database, $contactId, $_SESSION['userId'], $_POST['title'], $_POST['birthdate'], $_POST['referredBy'], $_POST['bestTimeToCall'], $_POST['street'], $_POST['city'], $_POST['state'], $_POST['zipCode'], $_POST['country'], $_POST['comapnyName'], $_POST['jobTitle']);
			$profiles->deleteContactPlanLinkedtoGroup($database, $contactId, $_SESSION['userId'], $groupIds);
			$profiles->assignPlanStep($_SESSION['userId'], $contactId, $_POST);
			foreach ($_POST['groups'] as $groupId)
			{
				//$group = $contact->manageAddContactsToGroup($contactId, $groupId);
				//$planIds = $profiles->EditContactPlanByGroup($database, $contactId, $groupId, $_SESSION['userId'], 0);
			}

			//$task->updateStickyNotes($_POST['stickynotes'],$contactId,$_POST['noteId']);
		}
	}
	$questions = $_POST['questionId'];


	if (!empty($questions))
	{
		$replyId = $_POST['replyId'];
		$typeId = $_POST['typeId'];
		for ($i = 0; $i < sizeof($questions); $i++)
		{
			$name = 'question' . $questions[$i];
			if ($i == 0)
			{
				$database->executeNonQuery("DELETE FROM pageformreplyanswers WHERE replyId='" . $replyId[$i] . "' and contactId='" . $contactId . "'");
			}
			if ($typeId[$i] == '1' || $typeId[$i] == '2')
			{
				$ans = str_replace("'", "\'", $_POST[$name]);
				$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId[$i] . "','" . $questions[$i] . "', '" . $ans . "', '" . $contactId . "')");
			}
			else if ($typeId[$i] == '3')
			{
				if ($questions[$i] == '10' || $questions[$i] == '14' || $questions[$i] == '42')
				{
					$interests = '';
					foreach ($_POST[$name] as $checkox)
					{
						$inter = $database->executeObject("SELECT choiceText from pageformquestionchoices where choiceId ='" . $checkox . "'");
						$start = strpos($inter->choiceText, '<b>');
						$stop = strpos($inter->choiceText, ':</b>');
						$stop = $stop - 3;
						$choiceText = substr($inter->choiceText, $start + 3, $stop);
						if ($choiceText == 'NO THANK YOU')
							$choiceText = 'CLIENT';
						if ($interests != '')
							$interests = $interests . ', ' . $choiceText;
						else
							$interests = $choiceText;
						//$mailQuesions=$mailQuesions.$interests.'<br />';
					}
					//echo "UPDATE contact SET interest='".strtoupper($interests)."' WHERE contactId='".$contactId."'";
					$database->executeNonQuery("UPDATE contact SET interest='" . strtoupper($interests) . "' WHERE contactId='" . $contactId . "'");

				}
				foreach ($_POST[$name] as $checkox)
				{
					$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId[$i] . "','" . $questions[$i] . "', '" . $checkox . "', '" . $contactId . "')");
				}

			}
			else if ($typeId[$i] == '4')
			{
				$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, contactId) VALUES ('" . $replyId[$i] . "','" . $questions[$i] . "', '" . $_POST[$name] . "', '" . $contactId . "')");
			}
			else if ($typeId[$i] == '5')
			{
				$answers = $profiles->getQuestionChoices($database, $questions[$i]);
				$database->executeNonQuery("DELETE FROM pageformreplyanswers WHERE replyId='" . $replyId[$i] . "' and contactId='" . $contactId . "' and questionId='" . $questions[$i] . "'");
				$questionsCountr = 0;
				foreach ($answers as $radio)
				{
					$name = 'answer' . $radio->choiceId;
					$_POST[$name];
					$database->executeNonQuery("INSERT INTO pageformreplyanswers (replyId, questionId, answer, value, contactId) VALUES ('" . $replyId[$i] . "','" . $questions[$i] . "', '" . $radio->choiceId . "', '" . $_POST[$name] . "' ,'" . $contactId . "')");
					$questionsCountr++;
				}
			}
		}
	}
	//echo $questionsCountr;
	if ($editProfile)
	{
		foreach ($currentAssociatedGroups as $cag)
		{
			if (!(in_array($cag->groupId, $groupIds)))
			{
				//$contact->deleteContactPlanLinkedtoGroup($database, $contactId, $_SESSION['userId'], $cag->groupId, $cag->planId);
			}
		}
	}
	echo 'done';


?>