<?php

include '../../classes/init.php'; 
require_once('../../classes/contact.php');
require_once('../../classes/profiles.php');
require_once('../../lib/functions.php');

$database = new Database();
$profiles = new Profiles();
$contact = new contact();

$subscribe = $_GET['subscribe'];
$contactId = $_GET['cid'];
$statusId = $_GET['sid'];
$userId = base62_decode($_GET['uid']);
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Zenplify</title>
        <link rel="stylesheet" href="../../css/style.css" type="text/css">
    </head>

    <body>
        <div align="center" id="index_logo"><img src="../../images/logo.png" />
            <div id="login_container" style="margin-top:20px;">
            <?php if ($subscribe == "success") {
                $contact->subscribeEmail($userId, $contactId, $statusId);
                $profiles->sendEmailToSubscribedContacts($database, $userId, $contactId);
            ?>
            <span style="color:#555555; font-size:12px;">Thank you for confirming your Email Address and approving future Emails. </span>
            <?php }if($subscribe=="failure"){?>
            <span style="color:#555555; font-size:12px;">You have been successfully unsubscribed. You will no longer receive Emails from Zenplify.
<br />
If you unsubscribed by accident, click <a href="https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&cid=<?=$contactId?>&sid=3">here</a> to resubscribe. </span>
            <?php $contact->subscribeEmail($userId,$contactId,$statusId);}?>
        
        </div>

        </div>
    </body>
</html>
