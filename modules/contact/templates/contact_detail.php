<?php 
session_start();
error_reporting(0);
require_once ("../../classes/init.php");
include_once("../headerFooter/headerContact.php");
include_once("../../classes/contact_details.php");
$database=new database();
$contact_details=new contactDetails();
$contactId=$_GET['contactId'];
if($contactId=='')
$contactId=$_SESSION['contactId'];
$userId=$_SESSION['userId'];
?>
  
      <script src="../../js/script.js" type="text/javascript"></script>
      <script type="text/javascript" src="../../js/handlebars.js"></script>
      <script type="text/javascript" src="../../js/helpers.js"></script>
      <script type="text/javascript">		
		//$.get("http://zenplify.biz/modules/profiles/userProfile.php?code=kanzaguestprofile", function(response) { alert(response) });
		//window.location = "view-source: http://zenplify.biz/modules/profiles/userProfile.php?code=kanzaguestprofile";
			
		function loadTabContent(tabUrl){
				$("#preloader").show();
				jQuery.ajax({
					url: tabUrl,
					type:'post', 
					cache: false,
					data:{cid:<?php echo $contactId; ?>},
					success: function(message) {
						jQuery("#content").empty().append(message);
						$("#preloader").hide();
					}
				});
			}
		function loadFilters(tabUrl){
		var $deferredsLocal = [];
		$deferredsLocal[0]= $.ajax({ url : tabUrl, type: "POST"});
		$deferredsLocal[1] = $.ajax({url:'templates/tabs_contact_detail.html',type:"GET",cache:true});
		
		
		
		$.when.apply(null, $deferredsLocal).then(function (json, htm) {
	   // do something with the data and template
		//var json=eval('('+json[0]+')');
		
		var htmls = htm[0];
		var template = Handlebars.compile(htmls);
		var completeHtml    = template(json[0]);
	
		$('#content').empty().append(completeHtml);
		$("#preloader").hide();
		
		
		});
		};
		function getEditUrl(profile,contactId, userId){
			// get tab id and tab url
			$("#preloader").show();
					tabId = $(this).attr("id");	
					tabUrl = "storeproc.php?action=edit&contactId="+contactId+"&profileId="+profile+"&userId="+userId;
					
					if(profile=='guestProfile')
					{
					$("[id^=tab]").removeClass("current");
					$("#tab1").addClass("current");
					}
					if(profile=='skinCareProfile')
					{
					$("[id^=tab]").removeClass("current");
					$("#tab3").addClass("current");
					}
					if(profile=='fitProfile')
					{
					$("[id^=tab]").removeClass("current");
					$("#tab4").addClass("current");
					}
					if(profile=='consultantProfile')
					{
					$("[id^=tab]").removeClass("current");
					$("#tab5").addClass("current");
					}
					// load tab content
					loadEditScreen(tabUrl);
					return false;
			
		}
		function submitProfile(profile,contactId, userId){
			
			$("#backgroundEditContact").show();
           var data = $('#editQuestions').serialize();
				$.post('editProfiles.php', data).done(function(data) {
					
					//alert("Data Loaded:");
					
					loadFilters("storeproc.php?action=view&contactId="+contactId+"&profileId="+profile+"&userId="+userId);
					
					$("#backgroundEditContact").hide();
					});
			
		}
		function loadEditScreen(tabUrl){
		var $deferredsLocal = [];
		$deferredsLocal[0]= $.ajax({ url : tabUrl, type: "POST"});
		$deferredsLocal[1] = $.ajax({url:'templates/tabs_contact_edit.html',type:"GET",cache:true});
		
		
		
		$.when.apply(null, $deferredsLocal).then(function (json, htm) {
	   // do something with the data and template
		//var json=eval('('+json[0]+')');
		
		var htmls = htm[0];
		var template = Handlebars.compile(htmls);
		var completeHtml    = template(json[0]);
	
		$('#content').empty().append(completeHtml);
		$("#preloader").hide();
		
		
		});
		};
		jQuery(document).ready(function(){	
			
			
			
	



				$("#preloader").show();			
				loadFilters("storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=guestProfile&userId=<?php echo $userId;?>");
				jQuery("[id^=tab]").click(function(){	
					$("#preloader").show();
					// get tab id and tab url
					tabId = $(this).attr("id");	
					tabUrl = jQuery("#"+tabId).attr("href");
					jQuery("[id^=tab]").removeClass("current");
					jQuery("#"+tabId).addClass("current");
					
					// load tab content
					loadFilters(tabUrl);
					return false;
				});
				
				
				 $('#birthdate').datepicker();
	 $("input[id^='step']").change(function(){
			var id=$(this).val();  
   $('#step_list'+id).toggle('show');
});
	$("#birthdate").change(function(){
	var dateOfBirth=$(this).val();
	var todaydate=new Date();
	var todaytime=todaydate.getTime();
	var birthdaytime=new Date(dateOfBirth).getTime();
	if(birthdaytime>=todaytime){
		$("#doberror").html('Invalid Date of Birth');
		$("#dobflag").val(1);
		$("#submit").get(0).setAttribute('type','button');
		}else{
			$("#doberror").html('');
			$("#dobflag").val(0);
		$("#submit").get(0).setAttribute('type','submit');
			}
	
	});
			
				
				$(window).resize(function() {
				var left=$('.sticky_notes').css('left')	
    			$('.sticky_notes').css('left',(50+20)+'%');
				//alert('hi');
				});

				$(window).trigger('resize');
				/*$("#change").click(function(){$('#navcontainer').css({"display":"none"});$('#nav_container').show();});*/
				
			
	});
		function skipTask(id){

				//alert(id);
				
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{skipId:id,action:'skipIt'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+id).fadeOut('500');
		    			}
		    		}
		    		});
				
            }
		function delupcomingActivities(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {taskId:id,action:'deleteTask',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	    // $('#record'+id).fadeOut(1000);
		 $("#task"+id).fadeOut('500');
		   }
 });
	//$('#record'+id).fadeOut(1000);
	}
		function delupcomingAppiontments(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {appiontmentId:id,action:'deleteAppiontment',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordAppiontment'+id).fadeOut(1000);
		   }
 });
	}	
		function delnotes(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {notesId:id,action:'deleteNotes',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordNotes'+id).fadeOut(1000);
		
   }
 });
	}
		function saveNotes(){
$("#preloader").show();
if($('#stickyNotes').val()==""){
$("#preloader").hide();
}else{
$.ajax({type: "POST",url: "../../classes/ajax.php",data: {stickyNotes:$('#stickyNotes').val(),action:'checkStickyNotes',contactId:'<?php echo $contactId;?>'},
success: function(msg){

if(msg!=''){
$("#preloader").hide();
$.trim('#stickyNotes').val(msg);


}else{
//alert('error');
}
}
});

}
}
		function moveToHistory(){
	
if($('#stickyNotes').val()==""){
	
	}else{
	var notesId=$('#stickyNoteId').val();
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {stickyNotes:$('#stickyNotes').val(),action:'moveToHistory',contactId:'<?php echo $contactId;?>',noteId: notesId },
   success: function(msg){
	    $('#stickyNotes').val('');
	  $('.appendnotes tr:last').after(msg);
	   
   }
 });
	}
	}	
		function editTask(editType){
				var taskId=$("#taskId").val();
				var cid=<?php echo $contactId;?>;
				window.location.href='../task/edit_task.php?edit_type='+editType+'&task_id='+taskId+'&contactId='+cid;
				}	
		function archiveContact(contactId){
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {action:'archiveContact',contact_id:contactId},
   success: function(msg){
	  
	   
		window.location.href="view_all_contacts.php?archive=success";
		   
   }
 });
	}	
		function markTaskCompleted(taskId){
			
				var userId=<?php echo $userId; ?>;
				var contactId=<?php echo $contactId; ?>;
				$.ajax({    
		            type    : "POST",
		            url     : "../../classes/ajax.php",
		            data    :{taskId:taskId,userId:userId,contactId:contactId,action:'updateTaskAgainstContact'},                
		    		success: function(data) {
		    			if(data==1){
		    				
		    			$("#task"+taskId).fadeOut('500');
		    			}
		    		}
		    		});
				
				}	
		function sendPermission(contactId){
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionToBlock',contactId:contactId},
    	        type: 'post',
    	        success: function(output) {
        	     loadPermissionManager();
				   $("#to").val(output);
				      	           }
    	});
		
		$("#contactId").val(contactId);
		}	
		function permissionMail(){
		var to=$("#to").val();
		var from=$("#from").val();
		var subject=$("#subject").val();
		var emailbody=$("#body").val();
		var userId=$("#userId").val();
		var contactId=$("#contactId").val();
		var emailId=$("#emailId").val();
		var emailflag=$("#emailflag").val();
		
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionMail',to:to,from:from,subject:subject,emailbody:emailbody,userId:userId,contactId:contactId,emailId:emailId,emailflag:emailflag},
				type: 'post',
    	        success: function(output) {
					
        	       if(output==1){
					
					   disablepermissionPopup();}
				  $("#messageofsuccess").html('Thank you for confirming your Email Address and approving future Emails');
				  
				  }
				  
    			});
			}	
		function blockAlert(){
		editRecur(0);	
		
				}	
			
		</script>
        
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <h1 class="gray">Contact Detail</h1>
     </div>
      <div class="sub_container">
          
           <div class="navcontainer" id="navcontainer">
				<ul>
                	<li><a style="border-top-left-radius:10px;" class="current" id="tab1" href="storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=guestProfile&userId=<?php echo $userId;?>">Guest Profile</a></li><img  src="../../images/sprater1.png">
					<li><a id="tab3" href="storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=skinCareProfile&userId=<?php echo $userId;?>">Skin Profile</a></li><img  src="../../images/sprater1.png" >
					<li><a id="tab4" href="storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=fitProfile&userId=<?php echo $userId;?>">Fit Profile</a></li><img  src="../../images/sprater1.png" >
                    <li><a style="padding:12px 9px 10px 11px; height:28px;" id="tab5" href="storeproc.php?action=view&contactId=<?php echo $contactId; ?>&profileId=consultantProfile&userId=<?php echo $userId;?>">Consultant Profile</a></li>
                    <!--<img  src="../../images/sprater1.png" >
                    <li><a style="border-bottom-left-radius:10px;" id="tab6" href="tabs_contact_detail.php?id=6">All</a></li>-->
				</ul>
			</div>
			<!--<div class="navcontainer" id="nav_container" >
				<ul>
					<li><a style="border-top-left-radius:10px;" id="tab1" href="tabs_contact_detail-.php?id=1">Guest Profile</a></li><img  src="images/sprater1.png" >
					
					<li><a id="tab3" href="tabs_contact_detail-.php?id=3">Skin Profile</a></li><img  src="images/sprater1.png" >
					<li><a id="tab4" href="tabs_contact_detail-.php?id=4">Fit Profile</a></li><img  src="images/sprater1.png" >
                    <li><a style="padding:12px 9px 10px 11px; height:28px;" id="tab5" href="tabs_contact_detail-.php?id=5">Consultant Profile</a></li><img  src="images/sprater1.png" >
                    <li><a style="border-bottom-left-radius:10px;" id="tab6" href="tabs_contact_detail-.php?id=6">All</a></li>
                    
                    
				</ul>
			</div>-->
            <div id="backgroundEditContact"></div>
			<div id="preloader">
				<img src="../../images/loading.gif" align="absmiddle">					
			</div>
			
			<div id="content">
			
			</div>
           
			
       </div>
       <div id="Popup"></div>

<div id="popupReoccur" >
  <h1 class="gray">Changing  recurring task</h1>
  <input type="hidden" name="taskId" id="taskId" />
  <span class="popupEditTask">You're changing a recurring task. Do you want to change only this task, or all future tasks?</span>
  <br /><br />
  <span ><input type="button" value="" class="onlyThisTask" onclick="editTask('1')" /> <input type="button" value="" onclick="editTask('0')" class="allFutureTask"/><input type="button" name="cancel" class="cancel" value="" onClick="disablereoccurPopup()"/></span>
</div> 
<?php	
	$to=$contact_details->showContactEmailAddress($database,$contactId);
	
$from=$contact_details->GetEmail($database,$userId);	
?>

   
      <div id="Popup"></div>
<div id="popuppermission" >
  <h1 class="gray">Email Permission Confirmation</h1>
  <form action="?action=success" method="post">
   
    <input type="hidden" name="contactId" id="contactId"  />
    <input type="hidden" name="emailId" id="emailId"  />
     <input type="hidden" name="emailflag" id="emailflag"  />
    
     <input type="hidden" name="userId" id="userId"  value="<?php echo $userId;?>" />
    <table width="100%" border="0">

      <tr>
        <td class="label">From</td>
        <td><input type="text" name="from" id="from"  class="textfieldbig" value="<?php echo  $from;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">To</td>
        <td><input type="text" name="to" id="to"  class="textfieldbig" value="<?php echo $to->email;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">Subject</td>
        <td><input type="text" name="subject" id="subject"  class="textfieldbig" value="Permission Confirmation"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td style="padding-left:12px;"><textarea name="area2" id="body" style="width:648px; height:200px;  ">
    
</textarea>
<div style="background-color:#EAEAEA;width:640px; padding:5px; padding-bottom:0px; margin-bottom:-5px;">
<p style="font-size:12px">
Your information is safe and will not be shared!<br /><br />I'll be sending a little info each day for the first week or so to help you get to know Arbonne, and a monthly newsletter if you requested it on your guest profile<br/><br/>To Accept Email click the link below. <br/>
<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br />
To Decline Email click the link below:<br/>
<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span> 
</p>
</div>

<!--<div style="background-color:#EAEAEA;width:640px; padding:5px;margin-bottom:15px;">
<p style="font-size:12px">
Please click the link below TO RECEIVE MY NEWSLETTER <br />
<span style="color:#06F; font-size:12px;"> hhttp://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br />
Click on following links to Unsubscribe <br />
<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span> 
</p>
</div>-->

</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="button" name="submit" class="sendpermissionconfirmation" value="" onclick="permissionMail();"/><input type="button" name="cancel" class="cancel" onclick="disablepermissionPopup();"/></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
</div>
    <?php include_once("../headerFooter/footer.php");?>