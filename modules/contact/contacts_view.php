<?php 
session_start();
include_once("../headerFooter/header.php");
?>
<script>
	$(document).ready(function() {
		   
		
		$('#transfer').click(function(){
			if(CheckSelected())
			{
				loadPopupTransfer();
				centerPopupTransfer();
			}
			else
			{
				alert('Please select atleast one contact');
			}
		});
		$("#backgroundPopup").click(function(){
			disablePopupTransfer();
			
			
		});
		$(".cancel").click(function(){
			disablePopupTransfer();
			
			
		});
	});
	</script>
<!--  <div id="menu_line"></div>-->
  <div class="container">
     <div class="top_content">
     <form name="contacts_form" id="contacts_form" action="#" method="">
     <a href="new_contact.php"><input type="button" id="new_contact" value=""  /></a>
     <p id="sorting"><img src="images/contact transfericon.png" id="transfer" title="Transfer Contact" alt="Transfer Contact" /><a href="import_contact.php"><img src="images/import.png" alt="Import" title="Import" /></a><img src="images/export.png"  alt="Export" title="Export" /><a href="permission_manager.php"><img src="images/permission manager.png"  title="Permission Manager" alt="Permission Manager"/></a><img src="images/printer.png" alt="Print" title="Print" /></p>
     </div>
      <div class="sub_container">
          <div class="col_table">
              <table width="100%" cellpadding="0" cellspacing="0" id="contact_table">
                <tr class="header">
                    <th class="first"><input type="checkbox" id="chkSelectDeselectAll" onclick="SelectDeselectContact(this)"/><label></label></th>
                    <th class="center medcol">Name <img src="images/bg.gif" /></th>
                      <th  class="center">Current Time  <img src="images/bg.gif" /></th>
                       <th class="center">Interest  <img src="images/bg.gif" /></th>
                        <th class="center">Phone </th>
                         <th class="center smallcol">Points <img src="images/bg.gif" /></th>
                        <th class="center smallcol">Blocked  </th>
                 
                    <th class="last"></th>
                </tr>
                 <tr>
                    <td><input type="checkbox" name="contact[]" /><label></label></td>
                    <td><a href="contact_detail.php">Eric Jhony</a></td>
                    <td>10:30 AM</td>
                     <td>Leader</td>
                      <td>00897654321</td>
                      <td>500</td>
                      <td><img src="images/block.png" title="Blocked" alt="Blocked" class="request_remind"/></td>
                      <td></td>
                 </tr>
                  <tr>
                    <td><input type="checkbox" name="contact[]" /><label></label></td>
                   <td><a href="contact_detail.php">Eric Jhony</a></td>
                    <td>10:30 AM</td>
                     <td>Consultant</td>
                      <td>00997654320</td>
                      <td>400</td>
                      <td><img src="images/block.png" title="Blocked" alt="Blocked" class="request_remind"/></td>
                     <td></td>
                 </tr>
                  <tr>
                    <td><input type="checkbox" name="contact[]" /><label></label></td>
                   <td><a href="contact_detail.php">Eric Jhony</a></td>
                    <td>10:30 AM</td>
                     <td>Host</td>
                      <td>00697654341</td>
                      <td>200</td>
                      <td>&nbsp;</td>
                       <td></td>
                 </tr>
                  <tr>
                    <td><input type="checkbox" name="contact[]" /><label></label></td>
                    <td><a href="contact_detail.php">Eric Jhony</a></td>
                    <td>10:30 AM</td>
                     <td>Client</td>
                      <td>00897654321</td>
                      <td>80</td>
                      <td><img src="images/block.png" title="Blocked" alt="Blocked" class="request_remind"/></td>
                      <td></td>
                 </tr>
               </table>
               </form>
               <table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
                                 <tbody><tr>
                                 <td>
                               
                            </td>
                            <td>
                           <div style="float:right; margin-top:10px;">
                               
                                   
                                    <div class="pager" id="pager">
	<form>
		<img class="first_page" src="images/paging_far_left.gif">
		<img class="prev_page" src="images/paging_far_left.gif">
		<input type="text" class="pagedisplay" value="1/1">
		<img class="next_page" src="images/paging_far_right.gif">
		<img class="last_page" src="images/paging_far_right.gif">
		
        <select id="pagesize" name="pagesize" class="pagesize"><option value="50" selected="selected">50</option><option value="100">100</option><option value="250">250</option></select>       
	</form>
</div></div></td>
                                       
                                                                                 
                                    </tr>
                                </tbody></table>
               
       		</div>
           <div class="col_search">
                <p class="search_header">Find Contacts</p>
           		<input type="text" name="searchContact" class="searchbox2" id="searchbox2" title="Search Contacts" />
                 <br />
              <!--  <img src="images/check.png" /> Include Custome Fields-->
             
                <p class="search_header small">Filter By:</p>
                 <p class="search_header2 small">Interest Level</p>
                <div id="groups_list">
                	<ul>
                    	<li><input type="checkbox" /><label>Leader</label></li>
                        <li><input type="checkbox" /><label>Consultant</label></li>
                     
                        <li><input type="checkbox" /><label>Hosts</label></li>
                        <li><input type="checkbox" /><label>Clients</label></li>
                        
                    
                    </ul>
                </div>
                <p class="search_header2 small"><a href="../group/groups_view.php">Groups </a></p>
                <div id="groups_list">
                	<ul>
                    	<li class="blue"><input type="checkbox" /><label>Independent Consultants</label></li>
                        <li class="grey"><input type="checkbox" /><label>Interested in Opportunity</label></li>
                        <li class="purple"><input type="checkbox" /><label>Clients</label></li>
                        <li class="pink"><input type="checkbox" /><label>Hosts</label></li>
                        <li class="green"><input type="checkbox" /><label>Guests</label></li>
                        <li class="orange"><input type="checkbox" /><label>Newsletter</label></li>
                        <li class="red"><input type="checkbox" /><label>Interested in Hosting</label></li>
                        <li class="yellow"><input type="checkbox" /><label>Fit Kit PURCHASE</label></li>
                    
                    </ul>
                </div>
               
           </div>
          
            <div class="empty"></div>  
            <div class="empty"></div>  
            <div class="empty"></div>  
            <div class="empty"></div>  
           
       </div>
      <div id="backgroundPopup"></div>
      <div id="popupTransfer">
      <h1>Transfer Contacts</h1>
      <form>
      <label class="survey_label">User Name</label>
      <input type="text" class="textfield" name="surveyName" id="surveyName" />
      <br />
      <input type="button" class="transfer_btn" id="next_Survey" /> <input type="button" class="cancel" style="margin-left:10px;" />
      </form>
      </div>
    <?php 
		include_once("footer.php");
	?>
   