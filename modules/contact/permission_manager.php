<?php
	session_start();
	include_once("../headerFooter/header.php");
	require_once('../../classes/contact.php');
	require_once('../../classes/profiles.php');
	require_once('../../lib/functions.php');

	$userId = $_SESSION['userId'];
	$leaderId = $_SESSION['leaderId'];
	$uid = base62_encode($userId);
	//$uid=$userId;
	$contact = new contact();
	//
	$profile = new profiles();
	$database = new database();
	if ($leaderId == 0) { $consultantTitleUserId =$userId; }
	else { $consultantTitleUserId = $leaderId; }
	$consultantTitle = $profile->getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
//
	$data = $contact->GetUserContacts($userId);
	$permissions = $contact->GetAllPermissions();
	$user = $contact->GetUserName($userId);

	$idCounter = 0;
	if(isset($_POST['submit']))
	{
		$emailTo = explode(',', $_POST['to']);
		$emailId = explode(',', $_POST['emailId']);
		$contact_ids = explode(',', $_POST['contactId']);
		$html = $_POST['area2'];
		$stripped = strip_tags($html);
		foreach($emailTo as $et)
		{
			if(!empty($et))
			{
				$headers = "From:".$user->firstName." ".$user->lastName." <".$user->email.">\r\n";
				$sub = $stripped."\n\nYour information is safe and will not be shared!\n\nI'll be sending a little info each day for the first week or so to help you get to know ". $consultantTitle." and a monthly newsletter if you requested it on your Guest profile\n\nTo Accept Email from '".$user->firstName." ".$user->lastName."' click the link below. \n"."https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=".$uid."&cid=".$contact_ids[$idCounter]."\nTo Decline Email from '".$user->firstName." ".$user->lastName."' click the link below:\nhttps://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=".$uid."&cid=".$contact_ids[$idCounter];
				/*$sub=$stripped."\r\nTo Approve Email from ".$user->firstName." ".$user->lastName." click the link below: \n"."http://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=".$uid."&cid=".$contact_ids[$idCounter]."\n\n
			To Decline Email from ".$user->firstName." ".$user->lastName." click the link below: \n".
				"http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&uid=".$uid."&cid=".$contact_ids[$idCounter];*/
				$status = mail($et, $_POST['subject'], $sub, $headers);
				if($status == 1)
				{
					$contact->permissionRequest($userId, $contact_ids[$idCounter], $emailId[$idCounter]);
					$idCounter++;
				}
			}
		}

	}
	if($_GET['action'] == 'success')
	{
		$mes = 'Thank you for confirming your Email Address and approving future Emails';
	}
?>
<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript" src="../../js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function () {
		nicEditors.allTextAreas()
	});
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function () {
		$("#pm_manager").click(function () {
			if(CheckSelectedPermissionContacts()) {
				var arr = [];
				var i = 0;
				$('.ads_Checkbox:checked').each(function () {
					arr[i++] = $(this).val();
				});
				var contactIds = arr.toString();
				$.ajax({url: 'optInMessage.php', type: 'post', data: {contactsids: contactIds}, success: function (result) {
					emails = result.split('&');
					var from = emails[0];
					var to = emails[1];
					$("#to").val(to);
					$("#from").val(from);
					$("#emailId").val(emails[2]);
					$("#contactId").val(emails[3]);
					loadPermissionManager();
				}});
			}
			else {
				alert('Please select atleast one contact');
			}
		});
		$(".Unsubscribed").click(function () {
			var contactId = $(this).attr('id');
			$.ajax({url: '../../classes/ajax.php', type: 'post', data: {action: 'Unsubscribed', contactId: contactId}, success: function (result) {
				if(result == 1) {
					var id = ".status" + contactId + "";
					var imgId = "#" + contactId + "";
					$(id).html('Unsubscribe');
					$(imgId).fadeOut(1000);
				}
			}});
		});
		$('*[id^=groupName]').click(function (e) {
			get_permission_contact();
		});
		$("#contact_table")
			.tablesorter({widthFixed: true, widgets: ['zebra']})
			.tablesorterPager({container: $("#pager"), size: 50});
	});
</script>
<!--  <div id="menu_line"></div>-->
<div class="container">
	<div class="top_content">
		<h1 class="gray">Permission Manager</h1>
	</div>
	<form method="post" name="permission_contacts" id="permission_contacts">
		<div class="top_content">
			<input id="pm_manager" type="button" value="" name="submit">
			<div style="float:left; margin-left:135px; width:450px; margin-top:15px;"><font class="messageofsuccess"><?php echo($mes == "" ? "" : $mes); ?></font></div>
		</div>
		<div class="sub_container">
			<div class="col_table">
				<table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter">
					<thead>
					<tr class="header">
						<th class="first_table"><input type="checkbox" id="chkSelectDeselectAll" onclick="SelectDeselectContact(this)"/>
							<label></label></th>
						<th class="center medcol">Name <img src="../../images/bg.gif"/></th>
						<th class="center"></th>
						<th class="center"></th>
						<th class="center"></th>
						<th class="center smallcol">Status <img src="../../images/bg.gif"/></th>
						<th class="center">Action</th>
						<th class="last_table"></th>
					</tr>
					</thead>
					<tbody id="tableBody">
					<?php

						foreach($data as $row)
						{
							$id = $row->contactId;
							$status = $contact->GetContactPermissionStatus($id);
							if(empty($status))
							{
								$cStatus = "Unprocessed";
							}
							else
							{
								$cStatus = $status;
							}
							echo "<tr class=\"odd_gradeX\" id=\"permission".$id."\">";
							echo "<td><input type='checkbox' name='contact[]' class='ads_Checkbox' id='contact[]'  value=".$row->contactId." ></td>";
							echo "<td><a href=contact_detail.php?contactId=".$id.">".$row->firstName." ".$row->lastName."</a></td>";
							echo "<td></td>";
							echo "<td></td>";
							echo "<td></td>";
							echo "<td class='status".$row->contactId."'>".$cStatus."</td>";
							if($cStatus == 'Subscribed')
							{
								echo "<td><img src='../../images/unsubscribe.png' class='Unsubscribed' id='".$row->contactId."' width='20px' height='20px' alt='Turn it to Unsubscribe' title='Turn it to Unsubscribe' /></td>";
							}
							echo "<td></td>";
							echo "</tr>";
						}
					?>
					</tbody>
				</table>
	</form>
	<table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
		<tbody>
		<tr>
			<td></td>
			<td>
				<div id="pager" class="pager" style="float:right; margin-top:5px; ">
					<form>
						<span class="total"></span>
						<img src="../../images/paging_far_left.gif" class="first"/>
						<img src="../../images/paging_left.gif" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="../../images/paging_right.gif" class="next"/>
						<img src="../../images/paging_far_right.gif" class="last"/>
						<select id="pagesize" name="pagesize" class="pagesize">
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="250">250</option>
						</select>
					</form>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	<!--<div class="pmnavcontainer">
					<ul>
						<li><a id="tab1" href="tabs_permission_mngr.php?id=1" style="border-bottom-left-radius:10px;" >UnProcessed (5)</a></li>
						<li><a id="tab2" href="tabs_permission_mngr.php?id=2">Pending (1)</a></li>
						<li><a id="tab3" href="tabs_permission_mngr.php?id=3">Subscribed (1)</a></li>
						<li><a id="tab4" href="tabs_permission_mngr.php?id=4">Unsubscribed (0)</a></li>
						<li><a id="tab5" href="tabs_permission_mngr.php?id=5" style="border-bottom-right-radius:10px;">Bounced (4)</a></li>
				   </ul>
				</div>-->
</div>
<div class="col_search" style=" margin-left:10px;">
	<p class="search_header">
		<label for="kwd_search">Filer By:</label>
	</p>
	<br/>

	<p style="padding-left:5px; font-weight:bold;"> Filter by Status</p>

	<div id="groups_list" style="display:block;">
		<form name="myform" action="" method="post">
			<ul>
				<?php
					foreach($permissions as $row1)
					{
						echo "<li><input type=\"checkbox\" name=\"list\" value=\"".$row1->title."\" id=\"groupName".$row1->statusId."\"/><label>".$row1->title."</label></li>";
					}

				?>
			</ul>
		</form>
	</div>
</div>
</div>
<div class="empty"></div>
<div class="empty"></div>
<?php
	include_once("../headerFooter/footer.php");
?>
<script type="text/javascript">
	$(document).ready(function () {
		// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function () {
			// When value of the input is not blank
			var size = $(this).val().length;
			var srch = $(this).val();
			if(size >= 3) {
				$.ajax({
					type   : "POST",
					url    : "../../classes/ajax.php",
					data   : {action: 'searchPermissionManagerContact', searchdata: srch, userId:<?php echo $userId;?>},
					success: function (data) {
						$("#tableBody").html(data);
						$("#contact_table")
							.tablesorter({widthFixed: true, widgets: ['zebra']})
							.tablesorterPager({container: $("#pager"), size: 50});
						var currentRows = $('#contact_table tbody tr:visible').length;
						var totalRows =<?php echo sizeof($data);?>;
						totalRows = currentRows + ' of ' + totalRows;
						$('.total').html(totalRows);
					}


				});
			}
			else if(size == 0) {
				$.ajax({
					type   : "POST",
					url    : "../../classes/ajax.php",
					data   : {action: 'searchPermissionManagerContact', searchdata: srch, userId:<?php echo $userId;?>},
					success: function (data) {
						$("#tableBody").html(data);
						$("#contact_table")
							.tablesorter({widthFixed: true, widgets: ['zebra']})
							.tablesorterPager({container: $("#pager"), size: 50});
						var currentRows = $('#contact_table tbody tr:visible').length;
						var totalRows =<?php echo sizeof($data);?>;
						totalRows = currentRows + ' of ' + totalRows;
						$('.total').html(totalRows);
					}
				});
			}
		});
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"],
		{
			"contains-ci": function (elem, i, match, array) {
				return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
			}
		});
	function check(field) {

		//alert("hi");
		$("#contact_table tbody>tr").hide();
		var list = new Array();
		var j = 0;
		for (i = 0; i < field.length; i++) {
			if(field[i].checked == true) {
				list[j] = field[i].value;
				j++;
			}
		}
		var arrayLength = list.length;
		if(arrayLength != 0) {
			for (k = 0; k < arrayLength; k++) {
				$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
			}
		}
		else {
			$("#contact_table tbody>tr").show();
		}
		$("#contact_table")
			.tablesorter({widthFixed: true, widgets: ['zebra']})
			.tablesorterPager({container: $("#pager"), size: 50});
	}
	function get_permission_contact() {
		var Arraygroups = [];
		$("*[id^=groupName]").each(function () {
			var groupId = $(this).val();
			if($(this).is(':checked')) {
				Arraygroups.push(groupId);
			}
		});
		$.ajax({
			type   : "POST",
			url    : "get_permission_contact.php?permissionIds=" + Arraygroups,
			data   : {permissionIds: Arraygroups},
			success: function (data) {
				$("#tableBody").html(data);
				$("#contact_table").tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}}).tablesorterPager({container: $("#pager"), size: 50});
			}
		});
	}
</script>
<div id="Popup"></div>
<div id="popuppermission">
	<h1 class="gray">Email Permission Confirmation</h1>

	<form action="?action=success" method="post">
		<input type="hidden" name="emailId" id="emailId"/>
		<input type="hidden" name="contactId" id="contactId"/>
		<table width="100%" border="0">
			<tr>
				<td class="label">From</td>
				<td><input type="text" name="from" id="from" readonly="readonly" class="textfieldbig" value="<?php echo $email; ?>"/></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="label">To</td>
				<td><input type="text" name="to" id="to" readonly="readonly" class="textfieldbig" value="<?php echo $to; ?>"/></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="label">Subject</td>
				<td><input type="text" name="subject" class="textfieldbig" value="Newsletter Permission from<?php echo " ".$user->firstName." ".$user->lastName; ?>"/></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style="padding-left:12px;"><textarea name="area2" style="width:648px; height:200px;  ">
					</textarea>

					<div style="background-color:#EAEAEA;width:640px; padding:5px; padding-bottom:0px; margin-bottom:-5px;">
						<p style="font-size:12px">
							Your information is safe and will not be shared!<br/><br/>I'll be sending a little info each day for the first week or so to help you get to know <?php echo $consultantTitle;  ?> and a monthly newsletter if you requested it on your Guest profile<br/><br/>To Accept Email click the link below. <br/>
							<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br/>
							To Decline Email click the link below:<br/>
							<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span>
						</p>
					</div>
					<!--<div style="background-color:#EAEAEA;width:640px; padding:5px;margin-bottom:15px;">
					<p style="font-size:12px">
					click on following links to subscribe <br />
					<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=success&cid=contact_id&sid=3 </span> <br />
					click on following links to Unsubscribe <br />
					<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&cid=contact_id&sid=4 </span>
					</p>
					</div>-->
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" class="sendpermissionconfirmation" value=""/><input type="button" name="cancel" class="cancel" onclick="disablepermissionPopup();"/></td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form>
</div>
