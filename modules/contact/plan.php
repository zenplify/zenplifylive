<?php
    require_once('init.php');

    //include_once('notification.php');

    class plan
    {
        function SavePlanTask($postArray, $plan_id, $notification, $userId, $leaderId, $type, $action)
        {
            $summary = $postArray['summary'];
            $daysAfter = $postArray['daysAfter'];
            $description = $postArray['description'];
            if (!empty($postArray))
            {
                $database = new database();
                //$query = "SELECT type, MAX(price) FROM products GROUP BY type";
                //$database->executeObjectList("SELECT MAX(order_no) FROM plansteps where stepId='".$stepId."'");
                $result = $database->executeScalar("SELECT MAX(order_no) FROM plansteps where planId='" . $plan_id . "'");
                //print_r($result);
                $result++;
                $database->executeNonQuery("INSERT INTO plansteps
								 	SET
									planId='" . $plan_id . "',
									summary='" . mysql_real_escape_string($summary) . "',
									isTask='1',
									daysAfter='" . $daysAfter . "',
									order_no='" . $result . "',
									description='" . mysql_real_escape_string($description) . "',
									isActive = 1,
									oldId='0',
									parentId = 0
									");
                $newStepId = $database->insertid();
                $planDetails = $database->executeObject("select `title`,`privacy` from `plan` where `planId` = '" . $plan_id . "'");
                $privacy = $planDetails->privacy;
                $planTitle = $planDetails->title;
                if (($planDetails->privacy == 1) && ($userId == $leaderId))
                {
                    $notification->notificationToUser($database, $userId, $newStepId, $type, $action, "Do you want to add step \'" . $summary . "\' to Plan \'" . $planTitle . "\' ?");
                    //($database, $userId, $newStepId, $type, $action, "Do you want to add step '".$summary."' to Plan '".$planTitle."' ?");
                }

                return $newStepId;
            }
        }

        function EditPlanTask($postArray, $plan_id, $stepId, $userId, $leaderId, $type, $notification, $ntype, $action)
        {
            // $notification = new notification();
            $summary = $postArray['summary'];
            $daysAfter = $postArray['daysAfter'];
            $description = $postArray['description'];
            if (!empty($postArray))
            {
                $database = new database();
//				$result = $database->executeScalar("SELECT MAX(order_no) FROM plansteps where planId='" . $plan_id . "'");
//				$result++;
                $result = $database->executeScalar("Select order_no from plansteps where stepId = '" . $stepId . "'");
                $parentDetail = $database->executeScalar("Select `parentId` from `plansteps` where `stepId` = '" . $stepId . "'");
                $parentId = ($parentDetail == '0' ? $stepId : $parentDetail);

                $database->executeNonQuery("INSERT INTO plansteps SET
									planId='" . $plan_id . "',
									summary='" . mysql_real_escape_string($summary) . "',
									isTask='" . $type . "',
									daysAfter='" . $daysAfter . "',
									order_no='" . $result . "',
									description='" . mysql_real_escape_string($description) . "',
									isActive = 1,
									oldId='" . $stepId . "',
									parentId = '" . $parentId . "',
									isUserChange='1'");

                $idStep = $database->insertid();
                $database->executeNonQuery("update `plansteps` set `isactive` ='0' where `stepId` = '" . $stepId . "'");

                $oldStepDetail = $database->executeObject("select `summary` from `plansteps` where `stepId` = '" . $stepId . "'");
                $oldStepName = $oldStepDetail->summary;

                $planDetails = $database->executeObject("select `title`,`privacy` from `plan` where `planId` = '" . $plan_id . "'");
                $privacy = $planDetails->privacy;
                $planTitle = $planDetails->title;

                if ($type == 0)
                {
                    $oldEmailStepDetail = $database->executeObject("Select * from `planstepemails` where `id` = '" . $stepId . "'");

                    $database->executeNonQuery("Insert into `planstepemails` (`id`,`subject`,`body`,`fromUser`) values
					('" . $idStep . "',
					'" . mysql_real_escape_string($postArray['subject']) . "',
					'" . mysql_real_escape_string($postArray['area2']) . "',
					'" . $oldEmailStepDetail->fromUser . "')");
                }

                if (($privacy == 1) && ($userId == $leaderId))
                {
                    $notification->notificationToUser($database, $userId, $stepId, $ntype, $action, "Do you want to Edit step \'" . $oldStepName . "\' in Plan \'" . $planTitle . "\' ?");
                }

                return $idStep;
            }
        }

        function EditPlanTaskForUser($database, $stepId, $userId, $leaderId)
        {
            $parentId = $database->executeScalar("SELECT parentId FROM plan WHERE planId IN (SELECT planId FROM plansteps WHERE stepId = '" . $stepId . "')");

            if ($parentId == 0)
            {
                $planId = $database->executeScalar("SELECT p.planId FROM plan p WHERE p.userId = '" . $userId . "' AND p.leaderId = '" . $leaderId . "' AND p.`oldId` IN (SELECT p.planId FROM plan p JOIN plansteps ps ON ps.planId = p.planId WHERE p.userId = '" . $leaderId . "' AND (p.leaderId IS NULL or p.leaderId = 0 or p.leaderId = '" . $leaderId . "') AND ps.stepId = '" . $stepId . "')");
            }
            else
            {
                $planId = $database->executeScalar("SELECT planId FROM plan WHERE oldId IN (SELECT p.planId FROM plan p JOIN plansteps ps ON ps.planId = p.planId WHERE p.userId = '" . $leaderId . "' AND ps.stepId = '" . $stepId . "') and userId = '" . $userId . "'");

//				$planId = $database->executeScalar("SELECT planId FROM plan WHERE oldId IN (SELECT p.planId FROM plan p JOIN plansteps ps ON ps.planId = p.planId WHERE p.userId = '" . $leaderId . "' AND ps.stepId = '" . $stepId . "')");
            }

            $newStepId = $database->executeScalar("SELECT * FROM plansteps ps LEFT JOIN plan p ON p.planId = ps.planId WHERE ps.stepId IN(SELECT stepId FROM plansteps WHERE oldId = '" . $stepId . "') AND p.userId = '" . $leaderId . "' AND ps.isactive = 1 AND ps.isArchive = 0");

            if (empty($newStepId))
            {
//				$newStepId = $stepId;
                $parentId = $database->executeScalar("select `parentId` from plansteps where stepId = '" . $stepId . "'");
                $newStepId = $database->executeScalar("SELECT stepId FROM plansteps ps
														JOIN plan p ON p.`planId` = ps.`planId`
														JOIN `user` u ON u.`userId` = p.`userId`
														WHERE ps.parentId = '" . $parentId . "' AND ps.isactive = 1
														AND ps.isArchive = 0 AND u.`userId` = '" . $leaderId . "'");

            }

            $stepDetails = $database->executeObject("Select * from plansteps where stepId = '" . $newStepId . "'");
            $order_no = $database->executeScalar("select order_no from plansteps where stepId= '" . $stepId . "'");

            $parentId = ($stepDetails->parentId == 0 ? $stepDetails->stepId : $stepDetails->parentId);

            $database->executeNonQuery("Insert into plansteps
			(`summary` ,`planId`, `isTask`, `daysAfter`, `order_no`, `description`, `isactive`, `oldId`, `parentId`) values
			('" . $stepDetails->summary . "',
			'" . $planId . "',
			'" . $stepDetails->isTask . "',
			'" . $stepDetails->daysAfter . "',
			'" . $order_no . "',
			'" . $stepDetails->description . "',
			'1',
			'" . $newStepId . "',
			'" . $parentId . "')");

            $updatedStepId = $database->insertid();

            if ($stepDetails->isTask == 0)
            {
                $planStepEmailDetails = $database->executeObject("select * from `planstepemails` where `id` = '" . $newStepId . "'");

                $database->executeNonQuery("Insert into `planstepemails` (`id`,`subject`,`body`,`fromUser`) values
				('" . $updatedStepId . "',
				'" . mysql_real_escape_string($planStepEmailDetails->subject) . "',
				'" . mysql_real_escape_string($planStepEmailDetails->body) . "',
				'" . $planStepEmailDetails->fromUser . "')");
            }

//			$database->executeNonQuery("Insert into plansteps (`summary` ,`planId`, `isTask`, `DaysAfter`, `order_no`, `description`, `isactive`, `oldId`)Select `summary` ,'" . $planId . "', `isTask`, `DaysAfter`, `order_no`, `description`, '1', '" . $stepId . "' from plansteps where stepId = '" . $stepId . "'");


            //echo ("SELECT stepId FROM plansteps WHERE planId = '".$planId."' AND isactive = '1' AND oldId IN (SELECT oldId FROM `plansteps` WHERE stepId = '".$stepId."')");

            $getId = $database->executeScalar("SELECT `stepId` FROM plansteps WHERE planId = '" . $planId . "' AND isactive = '1' AND oldId IN (SELECT oldId FROM `plansteps` WHERE stepId = '" . $stepId . "')");


            $database->executeNonQuery("update `plansteps` set `isactive` = '0' where stepId = '" . $getId . "'");

            return 'done';

        }

//		$id = $pl->SavePlanEmail($_POST, $plan_id, $userId, 5, 1, "Do you want to add Steps ".$_POST['summary']." to Plan ?");

        function SavePlanEmail($postArray, $plan_id, $userId, $leaderId, $type, $action, $notification)
        {
            $summary = $postArray['summary'];
            $daysAfter = $postArray['daysAfter'];
            $subject = $postArray['subject'];
            $body = mysql_real_escape_string($postArray['area2']);
            $from = $postArray['from'];
            if (!empty($postArray))
            {
                $database = new database();
                //$query = "SELECT type, MAX(price) FROM products GROUP BY type";
                //$database->executeObjectList("SELECT MAX(order_no) FROM plansteps where stepId='".$stepId."'");

                $result = $database->executeScalar("SELECT MAX(order_no) FROM plansteps where planId='" . $plan_id . "'");
                $result++;

                $database->executeNonQuery("INSERT INTO plansteps
								 	SET
									planId='" . $plan_id . "',
									summary='" . $summary . "',
									isTask='0',
									daysAfter='" . $daysAfter . "',
									order_no='" . $result . "',
									oldId = 0,
									parentId = 0");

                $step_id = $database->insertid();

                $planDetails = $database->executeObject("Select * from `plan` where `planId` = '" . $plan_id . "'");


                $database->executeNonQuery("INSERT INTO planstepemails
								 	SET
									id='" . $step_id . "',
									subject='" . $subject . "',
									body='" . $body . "',
									fromUser='" . $from . "',
									templateId='" . $_POST['templates'] . "'");
                if (($planDetails->privacy == 1) && ($userId == $leaderId))
                {
                    $notification->notificationToUser($database, $userId, $step_id, $type, $action, "Do you want to add Steps \'" . $summary . "\' to Plan \'" . $planDetails->title . "\' ?");
                }

                return $step_id;
            }
        }

        function SavePlanTaskReminder()
        {

        }

        function GetPlanTaskInfo($stepId)
        {
            $database = new database();
            $taskInfo = $database->executeObjectList("SELECT * FROM plansteps where plansteps.stepId='" . $stepId . "' ");

            return $taskInfo;
        }

        function GetPlanEmailInfo($stepId)
        {
            $database = new database();
            $taskInfo = $database->executeObjectList("SELECT * FROM plansteps left join planstepemails on  plansteps.stepId=planstepemails.id  where plansteps.stepId='" . $stepId . "'");

            return $taskInfo;
        }

        function DeletePlanStep($stepId)
        {
            $database = new database();
            $database->executeNonQuery("delete from plansteps where stepId='" . $stepId . "'");
            //echo ("delete from plansteps where stepId='".$stepId."'");
        }

        function DeletePlan($planId)
        {
            $database = new database();
            $database->executeNonQuery("delete from plan where planId='" . $planId . "'");
        }

        function CreatePlan($name, $userId, $leaderId)
        {
            $database = new database();
            $generatedVia = ($userId == $leaderId) ? '2' : '3';

            $database->executeNonQuery("INSERT INTO plan SET userId='" . $userId . "',
									title='" . $name . "',
									isDoubleOptIn='1',
									isactive='1',
									addDate=now(),
									groupId='4',
									reminderValue='3',
									reminderUnit='4',
									privacy = '0',
									parentId = '0',
									leaderId ='" . $leaderId . "',
									generatedVia = '" . $generatedVia . "',
									oldId = '0'");

            return $database->insertid();

        }

        function GetPlanInfo($id)
        {
            $database = new database();
            $plan_info = $database->executeObjectList("SELECT * FROM plan where planId='" . $id . "'");

            return $plan_info;

        }

        function ChangePlanName($name, $planId)
        {
            $database = new database();
            $database->executeNonQuery("update plan set title='" . $name . "' where planId='" . $planId . "'");
        }

        function ChangePlanTyep($planId)
        {
            $database = new database();
            $plan_info = $database->executeScalar("SELECT isDoubleOptIn FROM plan where planId='" . $planId . "'");
//			if (!empty($plan_info))
//			{
//				foreach ($plan_info as $row)
//				{
//					$option = $row->isDoubleOptIn;
//				}
//			}
            if ($plan_info == 1)
            {
                $database->executeNonQuery("update plan set isDoubleOptIn='0' where planId='" . $planId . "'");
            }
            else
            {
                $database->executeNonQuery("update plan set isDoubleOptIn='1' where planId='" . $planId . "'");
            }

        }

        function GetAllPlans($userId, $leaderId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM plan where userId='" . $userId . "' and `isActive` = 1");
            //			if($userId == $leaderId)
            //			{
            //				return $database->executeObjectList("SELECT * FROM plan where userId='".$userId."' and `isActive` = 1");
            //			}
            //			else
            //			{
            //				return $database->executeObjectList("SELECT * FROM plan where (`userId`='".$userId."' and `isActive` = 1 and `isLeader` = 0) or (`userId`='".$leaderId."' and `isActive` = 1 and `isLeader` = 1 and `privacy` = 1 )");
            //			}
        }

        function AddReminderInPlan($plan_id)
        {
            $database = new database();
            $database->executeNonQuery("INSERT INTO plansteps
								 	SET
									planId='" . $plan_id . "',
									summary='reminder',
									isTask='',
									daysAfter='',
									order_no='',
									description=''");

        }

        function UpdatePlanGroup($plan_id, $group_id, $user_id)
        {
            $database = new database();
            $dat = date('Y-m-d');
            $date = strtotime($dat);
            $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $user_id . "'");
            $firstStepOfPlan = $database->executeScalar("select stepId FROM plansteps WHERE planId='" . $plan_id . "' order by stepId asc limit 1");
            $groupId = $database->executeScalar("select groupId from plan where planId='" . $plan_id . "'");
            $planType = $database->executeScalar("select isDoubleOptIn from plan where planId='" . $plan_id . "'");
            $database->executeNonQuery("delete from planstepusers where contactId IN (SELECT groupcontacts.contactId FROM groupcontacts,contact where groupId='" . $groupId . "' and userId='" . $user_id . "' and groupcontacts.contactId=contact.contactId) AND stepId='" . $firstStepOfPlan . "'");
            $database->executeNonQuery("update plan set groupId='" . $group_id . "' where planId='" . $plan_id . "'");
            $planSteps = $database->executeObjectList("select * FROM plansteps WHERE planId='" . $plan_id . "' order by stepId asc");
            $i = 0;
            $totalSteps = sizeof($planSteps);
            $stepCounter = 1;
            foreach ($planSteps as $ps)
            {
                $contactIds = $database->executeObjectList("SELECT * FROM groupcontacts,contact where groupId='" . $group_id . "' and userId='" . $user_id . "' and groupcontacts.contactId=contact.contactId");
                //echo "<br />";
                //print_r($contactIds);
                //assign
                foreach ($contactIds as $cid)
                {
                    $contactId = $cid->contactId;
                    //echo 'not exists'.$ps->stepId;
                    //echo '<br />';
                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $stepDesc->stepId . "',contactId='" . $contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                    $newStepId = $database->insertid();
                    if ($ps->isTask == 0)
                    {
                        if ($ps->daysAfter == 0)
                        {
                            //echo "\r\nemail step";
                            //echo 'isDouble:'.$plan->isDoubleOptIn.'\r\n';
                            $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $contactId . "'");
                            if (($contactPermission == 3 && $planType == 1) || $planType == 0)
                            {
                                //echo 'emial allowed';
                                $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $contactId . "'");
                                $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                foreach ($match[1] as $key)
                                {
                                    if ($key == "First Name")
                                    {
                                        $firstname = 'firstName';
                                    }
                                    if ($key == "Last Name")
                                    {
                                        $lastname = 'lastName';
                                    }
                                    if ($key == "Phone Number")
                                    {
                                        $phonenumber = 'phoneNumber';
                                    }
                                    if ($key == "Company Name")
                                    {
                                        $companyname = 'companyTitle';
                                    }
                                    if ($key == "@Signature")
                                    {
                                        $signature = self::getSignatures($database, $user_id);
                                    }

                                }
                                if (!empty($firstname))
                                {
                                    $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($lastname))
                                {
                                    $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($companyname))
                                {
                                    $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $contactId . "'");
                                }
                                if (!empty($phonenumber))
                                {
                                    $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $contactId . "'");
                                }
                                $subject = $mailBody->subject;
                                $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                $message = str_replace("\'", "'", $rplbysig);
                                $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                    "Reply-To: " . $userInfo->email . "\r\n";
                                $headers .= "Content-Type: text/html";
                                //echo 'Email\r\n'.$contactemail.'Subject\r\n'.$subject.'Body\r\n'.$message.'Header\r\n'.$headers;
                                $mail = new smtpEmail();
                                $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject);
                                //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan_id . "', '" . $stepDesc->stepId . "','" . $contactId . "', '" . $user_id . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                $database->executeNonQuery("delete from planstepusers where stepId='" . $stepDesc->stepId . "' and contactId='" . $contactId . "'");
                                if ($totalSteps == $stepCounter)
                                {
                                    $database->executeNonQuery("INSERT INTO contactPlansCompleted(stepId, planId, contactId, dateCompleted) VALUES ('" . $stepDesc->stepId . "','" . $stepDesc->planId . "','" . $contactId . "',CURDATE())");
                                }
                            }
                        }
                        else
                        {
                            //echo 'breaking days after not zero';
                            //echo '<br>';
                            break;
                        }
                    }
                    else
                    {
                        //echo "<br /> task step<br />";
                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $user_id . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','" . mysql_escape_string($stepDesc->description) . "','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                        $taskId = $database->insertid();
                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $stepDesc->planId . "',stepId='" . $stepDesc->stepId . "'");
                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $contactId . "',taskId='" . $taskId . "'");
                        //echo '<br />';
                        //echo 'breaking task occured';
                        break;
                    }
                    $stepCounter++;

                }

            }

            return 'ending';
        }

        function getSignatures($database, $userId)
        {
            $details = $database->executeObject("select u.firstName, u.lastName, u.phoneNumber, u.webAddress, u.facebookAddress, u.consultantId, u.title, u.leaderId,u.userId from user u where u.userId='" . $userId . "'");
            $consultantTitleUserId = '';
            if ($details->leaderId == 0)
            {
                $consultantTitleUserId = $details->userId;
            }
            else
            {
                $consultantTitleUserId = $details->leaderId;
            }
            $consultantTitle = self::getconsultantTitle($database, $consultantTitleUserId, 'consultantId');
            if (empty($consultantTitle))
            {
                $arbonLogo = '<img src="http://zenplify.biz/images/GreenLogo1x1.png">';
            }

            $signature = '<span style="font-size:14px;">Best Regards,</span><br /><br /><span style="font-size:14px;">' . $details->firstName . '</span><br />
			<div><div style="float:left;">' . $arbonLogo . '</div><div style=" margin-left:5px;float:left;font-size:14px;">' . $details->firstName . ' ' . $details->lastName;


            if ($details->title != '')
                $signature = $signature . ', ' . $details->title;
            if (strpos($details->webAddress, "http://") >= 0 || strpos($details->webAddress, "https://") >= 0)
                $web = $details->webAddress;
            else
                $web = "http://" . $details->webAddress;
            if (strpos($details->facebookAddress, "http://") >= 0 || strpos($details->facebookAddress, "https://") >= 0)
                $facebook = $details->facebookAddress;
            else
                $facebook = "http://" . $details->facebookAddress;
            $webNew = preg_replace('#^https?://#', '', $web);
            $fbNew = preg_replace('#^https?://#', '', $facebook);

            $signature = $signature . '<br /><span style="color: #2E6A30;font-size:14px;"> ' . $consultantTitle . ' ' . $details->consultantId . '</span><br /><span style="color: #2E6A30;font-size:14px;"><a style="color: #2E6A30;font-size:14px;" href="' . $web . '" target="_blank">' . $webNew . '</a></span><br />' . $details->phoneNumber . '<br /><a href="' . $facebook . '" target="_blank">' . $fbNew . '</a></div></div>';

            return $signature;
        }

        function getconsultantTitle($database, $userId, $fieldMappingName)
        {

            return $consultantTitle = $database->executeScalar("SELECT `fieldCustomName` FROM `leaderdefaultfields` WHERE userId='" . $userId . "' AND `fieldMappingName`='" . $fieldMappingName . "' AND `status`='1'");

        }

        function UpdatePlanGroupOLD($plan_id, $group_id, $user_id)
        {
            $database = new database();
            $dat = date('Y-m-d');
            $date = strtotime($dat);
            $userInfo = $database->executeObject("SELECT * FROM user WHERE userId='" . $user_id . "'");
            $firstStepOfPlan = $database->executeScalar("select stepId FROM plansteps WHERE planId='" . $plan_id . "' order by stepId asc limit 1");
            $groupId = $database->executeScalar("select groupId from plan where planId='" . $plan_id . "'");
            $planType = $database->executeScalar("select isDoubleOptIn from plan where planId='" . $plan_id . "'");
            $database->executeNonQuery("delete from planstepusers where contactId IN (SELECT groupcontacts.contactId FROM groupcontacts,contact where groupId='" . $groupId . "' and userId='" . $user_id . "' and groupcontacts.contactId=contact.contactId) AND stepId='" . $firstStepOfPlan . "'");
            $database->executeNonQuery("update plan set groupId='" . $group_id . "' where planId='" . $plan_id . "'");
            $planSteps = $database->executeObjectList("select * FROM plansteps WHERE planId='" . $plan_id . "' order by stepId asc");
            $i = 0;
            foreach ($planSteps as $ps)
            {
                //echo $i+1 .'<==step no';
                if ($i > 0 && $ps->isTask == 0)
                {
                    $prevPlanStepType = $database->executeObject("select * FROM plansteps WHERE order_no<(select order_no from plansteps where stepId='" . $ps->stepId . "') AND planId='" . $plan_id . "' order by stepId desc limit 1");
                    //print_r($prevPlanStepType);
                    if ($prevPlanStepType->isTask == 1)
                    {
                        /* $planStep=$database->executeObject("select * from tasks,taskplan where taskplan.stepId='".$prevPlanStepType->stepId."' AND taskplan.planId='".$prevPlanStepType->planId."' AND tasks.taskId=taskplan.taskId AND planstepusers.contactId='".$contactId."' AND planstepusers.stepId='".$prevPlanStepType->stepId."' ");
                        $print_r($planStep);
                        if($planStep->isCompleted!=1){
                        echo "\r\n\r\n not completed";
                        break;
                        }*/
                        break;
                    }

                }
                $contactIds = $database->executeObjectList("SELECT * FROM groupcontacts,contact where groupId='" . $group_id . "' and userId='" . $user_id . "' and groupcontacts.contactId=contact.contactId");
                //echo "<br />";
                //print_r($contactIds);
                //assign
                foreach ($contactIds as $cid)
                {
                    $stepDesc = $database->executeObject("SELECT * FROM plansteps WHERE stepId='" . $ps->stepId . "'");
                    $dueDate = strtotime("+" . $stepDesc->daysAfter . " day", $date); //add days in date
                    $database->executeNonQuery("INSERT INTO planstepusers SET stepId='" . $ps->stepId . "',contactId='" . $cid->contactId . "',stepStartDate='" . date('Y-m-d') . "',stepEndDate='" . date('Y-m-d', $dueDate) . "'");
                    $contactId = $cid->contactId;
                    /*echo "\r\n";
                    echo "insert into plan step user";*/
                    if ($ps->daysAfter == 0)
                    {
                        /*echo "\n\n";
                        echo "o days";*/
                        if ($ps->isTask == 0)
                        {
                            echo "\r\nemail step";
                            $contactPermission = $database->executeScalar("SELECT statusId FROM contactpermissionrequests where toContactId='" . $cid->contactId . "'");
                            if (($contactPermission == 3 && $planType == 1) || $planType == 0)
                            {
                                $contactemail = $database->executeScalar("SELECT email FROM emailcontacts WHERE contactId='" . $cid->contactId . "'");
                                $mailBody = $database->executeObject("SELECT * FROM planstepemails WHERE id='" . $ps->stepId . "'");
                                preg_match_all('#\[{(.*?)\}]#', $mailBody->body, $match);
                                foreach ($match[1] as $key)
                                {
                                    if ($key == "First Name")
                                    {
                                        $firstname = 'firstName';
                                    }
                                    if ($key == "Last Name")
                                    {
                                        $lastname = 'lastName';
                                    }
                                    if ($key == "Phone Number")
                                    {
                                        $phonenumber = 'phoneNumber';
                                    }
                                    if ($key == "Company Name")
                                    {
                                        $companyname = 'companyTitle';
                                    }
                                    if ($key == "@Signature")
                                    {
                                        $signature = self::getSignatures($database, $userId);
                                    }

                                }
                                if (!empty($firstname))
                                {
                                    $f = $database->executeScalar("SELECT firstName FROM contact where contactId='" . $cid->contactId . "'");
                                }
                                if (!empty($lastname))
                                {
                                    $l = $database->executeScalar("SELECT lastName FROM contact where contactId='" . $cid->contactId . "'");
                                }
                                if (!empty($companyname))
                                {
                                    $c = $database->executeScalar("SELECT companyTitle FROM contact where contactId='" . $cid->contactId . "'");
                                }
                                if (!empty($phonenumber))
                                {
                                    $p = $database->executeScalar("SELECT phoneNumber FROM contactphonenumbers where contactId='" . $cid->contactId . "'");
                                }
                                $subject = $mailBody->subject;
                                $rplbyfn = str_replace('[{First Name}]', $f, $mailBody->body);
                                $rplbyln = str_replace('[{Last Name}]', $l, $rplbyfn);
                                $rplbycn = str_replace('[{Company Name}]', $c, $rplbyln);
                                $rplbypn = str_replace('[{Phone Number}]', $p, $rplbycn);
                                $rplbysig = str_replace('[{@Signature}]', $signature, $rplbypn);
                                $message = str_replace("\'", "'", $rplbysig);
                                $headers = "From:" . $userInfo->firstName . " " . $userInfo->lastName . " <" . $userInfo->email . ">\r\n" .
                                    "Reply-To: " . $userInfo->email . "\r\n";
                                $headers .= "Content-Type: text/html";
                                $mail = new smtpEmail();
                                $mail->sendMail($database, $userInfo->email, $contactemail, $userInfo->firstName, $userInfo->lastName, $message, $subject);
                                //mail($contactemail,$subject,$message,$headers,"-f ".$userInfo->email."");
                                $database->executeNonQuery("INSERT INTO contactemailhistory( planId, stepId, contactId, userId, toEmail, fromEmail, subject, body, dateTime) VALUES
												('" . $plan_id . "', '" . $stepDesc->stepId . "','" . $cid->contactId . "', '" . $user_id . "', '" . $contactemail . "', '" . $userInfo->email . "', '" . mysql_real_escape_string($subject) . "', '" . mysql_real_escape_string($message) . "', NOW())");
                                $database->executeNonQuery("delete from planstepusers where stepId='" . $ps->stepId . "' and contactId='" . $cid->contactId . "'");
                            }
                        }
                        else
                        {
                            //echo "\r\n task step";
                            $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $user_id . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                            $taskId = $database->insertid();
                            $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $plan_id . "',stepId='" . $ps->stepId . "'");
                            $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $cid->contactId . "',taskId='" . $taskId . "'");
                            break;
                        }
                    }
                    else
                    {
                        //echo "\r\n task step";
                        $database->executeNonQuery("INSERT INTO tasks(userId, title, isCompleted, priorityId, notes, dueDateTime, recurrenceId, addedDate, isActive, isPlanReminder, planId,endingOn,customRuleId, repeatTypeId) VALUES('" . $user_id . "','" . mysql_real_escape_string($stepDesc->summary) . "','0','','','" . date('Y-m-d', $dueDate) . "','0','" . date('Y-m-d') . "','0','0','" . $stepDesc->planId . "','','0','1')");
                        $taskId = $database->insertid();
                        $database->executeNonQuery("INSERT INTO taskplan set taskId='" . $taskId . "',planId='" . $plan_id . "',stepId='" . $ps->stepId . "'");
                        $database->executeNonQuery("INSERT INTO taskcontacts SET contactId='" . $cid->contactId . "',taskId='" . $taskId . "'");
                        $flag = 1;
                    }

                }
                $i++;
                if ($flag == 1)
                {
                    break;
                }
            }

            return 'ending';
        }

        function ActivatePlan($plan_id)
        {
            $database = new database();
            $database->executeNonQuery("update plan set isactive='1',isArchive='0' where planId='" . $plan_id . "'");
        }

        function DisablePlan($plan_id)
        {
            $database = new database();
            $database->executeNonQuery("update plan set isactive='0',isArchive='1' where planId='" . $plan_id . "'");
        }

        function DisableStep($database, $stepId)
        {
            $database->executeNonQuery("update plansteps set isactive='0' where stepId='" . $stepId . "'");
        }

        function GetPlanStepsInfo($id)
        {
            $database = new database();
            $plan_step_info = $database->executeObjectList("SELECT * FROM plansteps where planId='" . $id . "' AND isactive='1' order by order_no asc");

            return $plan_step_info;

        }

        function UpdatePlanTask($postArray, $step_id)
        {
            $summary = $postArray['summary'];
            $daysAfter = $postArray['daysAfter'];
            $subject = $postArray['subject'];
            $body = $postArray['area2'];
            $tempId = $postArray['templates'];
            if (!empty($postArray))
            {
                $database = new database();
                $body = mysql_real_escape_string($body);
                $database->executeNonQuery("update plansteps set summary='" . $summary . "', daysAfter='" . $daysAfter . "' where stepId='" . $step_id . "'");
                $database->executeNonQuery("update planstepemails set subject='" . $subject . "', body='" . $body . "', templateId='" . $tempId . "' where id='" . $step_id . "'");
            }

        }

        public
        function showPlans($userId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM plan WHERE userId='" . $userId . "' AND isActive=1");
        }

        function showPlanSteps($planId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM plansteps WHERE planId='" . $planId . "' and isactive = 1 and isArchive=0 ORDER BY order_no ASC");
        }

        function showPlanStepsInContactDetail($contactId)
        {
            $database = new database();
            //return $database->executeObjectList("SELECT * FROM tasks WHERE taskId IN(SELECT taskId from taskcontacts WHERE contactId='".$contactId."') AND planId!=0 ");
            //return $database->executeObjectList("SELECT p2.summary,p2.planId,p2.stepId,p3.title,t1.isActive FROM planstepusers p1,plansteps p2,plan p3,tasks t1,taskcontacts t2 WHERE p1.stepId=p2.stepId and p1.contactId='".$contactId."' and t2.contactId='".$contactId."' and p2.planId=p3.planId and t1.taskId=t2.taskId and t1.planId=p2.planId  and p3.isactive=1 group by p2.planId  order by p1.stepEndDate desc ");
            /*SELECT tp.taskId,p2.summary,p2.planId,p2.stepId,p3.title,IF(t1.isActive=1,'Skippes','') as status FROM plan p3
            join plansteps p2 on p3.planId=p2.planId
            join planstepusers p1 on p2.stepId=p1.stepId

                    left join  tasks t1 on p2.planId=t1.planId left join taskplan tp on t1.taskId=tp.taskId and p1.stepId=tp.stepId
            WHERE p1.contactId='2640' AND p3.isActive=1 group by  p2.planId order by p3.planId */

            return $database->executeObjectList("SELECT t.taskId,p2.summary,p2.planId,p2.stepId,p3.title, t.isActive,p1.contactId, cpc.contactId as cpcContactId FROM plan p3
		left join plansteps p2 on p3.planId=p2.planId
		left join planstepusers p1 on p2.stepId=p1.stepId
                left join contactPlansCompleted cpc on p3.planId=cpc.planId and p2.stepId = cpc.stepId and cpc.contactId ='" . $contactId . "'
                left join (
SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
inner join tasks t on tc.taskId = t.taskId
inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
where tc.contactId = '" . $contactId . "' and t.isCompleted=0
) as t on p2.stepId = t.stepId and p2.planId = t.planId
                where (p1.contactId ='" . $contactId . "' or cpc.contactId = '" . $contactId . "') AND p3.isActive=1 order by p3.planId");

            //$database->executeObjectList("SELECT * FROM plansteps WHERE stepId IN(SELECT stepId from planstepusers WHERE contactId='".$contactId."')");
        }

        function showPlanStepsIdsInContactDetail($contactId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT p1.stepId,p3.planId FROM plan p3
		left join plansteps p2 on p3.planId=p2.planId
		left join planstepusers p1 on p2.stepId=p1.stepId
                left join contactPlansCompleted cpc on p3.planId=cpc.planId and p2.stepId = cpc.stepId and cpc.contactId ='" . $contactId . "'
                left join (
SELECT tc.taskId, t.planId, tp.stepId, t.isActive, tc.contactId FROM taskcontacts tc
inner join tasks t on tc.taskId = t.taskId
inner join taskplan tp on t.taskId = tp.taskId and t.planId = tp.planId
where tc.contactId = '" . $contactId . "'  and t.isCompleted=0
) as t on p2.stepId = t.stepId and p2.planId = t.planId
                where (p1.contactId ='" . $contactId . "' or cpc.contactId = '" . $contactId . "') AND p3.isActive=1 order by p3.planId");

        }

        function getPlanName($planId)
        {
            $database = new database();

            return $database->executeScalar("SELECT title FROM plan WHERE planId='" . $planId . "'");

        }

        function getcontactId($appiontmentId)
        {
            $database = new database();

            return $database->executeObjectList("SELECT contactId FROM appiontmentcontacts WHERE  appiontmentId='" . $appiontmentId . "'");

        }

        function getBlockContactCount($planId)
        {
            $database = new database();

            return $database->executeScalar("SELECT count(toContactId) FROM contactpermissionrequests WHERE toContactId IN(SELECT contactId FROM planstepusers where stepId IN (SELECT stepId FROM plansteps,plan  WHERE isTask=0 AND plansteps.planId='" . $planId . "' AND plan.planId=plansteps.planId and plan.isDoubleOptIn=1) AND statusId<>3)");

        }

        function getLastStepOfPlan($planId, $contactId)
        {
            $database = new database();

            return $database->executeObject("select * from planstepusers where stepId=(select stepId from plansteps where planId='" . $planId . "' order by order_no desc limit 1) and contactId='" . $contactId . "'");

        }

        function CheckLastStepOfPlan($planId, $contactId)
        {
            $database = new database();

            return $database->executeObject("select * from contactPlansCompleted where planId='" . $planId . "' and contactId='" . $contactId . "'");

        }

        function getPlanType($planId)
        {
            $database = new database();

            return $database->executeScalar("select isDoubleOptIn from plan where planId='" . $planId . "'");

        }

        function getContactPermissionStatus($contactId)
        {
            $database = new database();

            return $database->executeScalar("select statusId from contactpermissionrequests where toContactId='" . $contactId . "'");

        }

        function getPlanStepType($stepId)
        {
            $database = new database();

            return $database->executeScalar("select istask from plansteps where stepId='" . $stepId . "'");

        }

        function getEmailTemplates()
        {
            $database = new database();

            return $database->executeObjectList("SELECT * FROM  emailtemplate WHERE  isDeleted=0");
        }

        function LoadEmailTemplates($tempId)
        {
            $database = new database();

            return $database->executeObject("SELECT * FROM  emailtemplate WHERE  templateId='" . $tempId . "'");
        }

        function archivePlan($planId)
        {
            $database = new database();
            $database->executeNonQuery("update `plan` set `isactive` = IF(isactive=1, 0, 1),`isArchive`='1' where `planId` = '" . $planId . "'");

            return 'done';
        }

        function publishStep($database, $stepId, $userId, $leaderId)
        {
            $parentId = $database->executeScalar("SELECT parentId FROM plan WHERE planId IN (SELECT planId FROM plansteps WHERE stepId = '" . $stepId . "')");

            if ($parentId == 0)
            {
                $planId = $database->executeScalar("SELECT p.planId FROM plan p WHERE p.userId = '" . $userId . "' AND p.leaderId = '" . $leaderId . "' AND p.`oldId` IN (SELECT p.planId FROM plan p JOIN plansteps ps ON ps.planId = p.planId WHERE p.userId = '" . $leaderId . "' AND (p.leaderId IS NULL or p.leaderId = 0 or p.leaderId = '" . $leaderId . "') AND ps.stepId = '" . $stepId . "')");
            }
            else
            {
                $planId = $database->executeScalar("SELECT planId FROM plan WHERE oldId IN (SELECT p.planId FROM plan p JOIN plansteps ps ON ps.planId = p.planId WHERE p.userId = '" . $leaderId . "' AND ps.stepId = '" . $stepId . "') and userId = '" . $userId . "'");
            }

            $stepDetails = $database->executeObject("Select * from plansteps where stepId = '" . $stepId . "'");

            $parentId = ($stepDetails->parentId == 0 ? $stepDetails->stepId : $stepDetails->parentId);


            $database->executeNonQuery("Insert into plansteps
			(`summary` ,`planId`, `isTask`, `daysAfter`, `order_no`, `description`, `isactive`, `oldId`, `parentId`) values
			('" . $stepDetails->summary . "',
			'" . $planId . "',
			'" . $stepDetails->isTask . "',
			'" . $stepDetails->daysAfter . "',
			'" . $stepDetails->order_no . "',
			'" . $stepDetails->description . "',
			'1',
			'" . $stepId . "',
			'" . $parentId . "')");

            $newStepId = $database->insertid();
            if ($stepDetails->isTask == 0)
            {
                $planStepEmailDetails = $database->executeObject("select * from `planstepemails` where `id` = '" . $stepDetails->stepId . "'");


                $database->executeNonQuery("Insert into `planstepemails` (`id`,`subject`,`body`,`fromUser`) values
				('" . $newStepId . "',
				'" . mysql_real_escape_string($planStepEmailDetails->subject) . "',
				'" . mysql_real_escape_string($planStepEmailDetails->body) . "',
				'" . $planStepEmailDetails->fromUser . "')");
            }
//			Select `summary` ,'" . $planId . "', `isTask`, `daysAfter`, `order_no`, `description`, '1', '" . $stepId . "' from plansteps where stepId = '" . $stepId . "'");
//				$database->executeNonQuery("Insert into plansteps
//			(`summary` ,`planId`, `isTask`, `daysAfter`, `order_no`, `description`, `isactive`, `oldId`, `parentId`)
//			Select `summary` ,'" . $planId . "', `isTask`, `daysAfter`, `order_no`, `description`, '1',
//			'" . $stepId . "',
//			'". ($parentId == 0 ? $stepId : `parentId`)."' from plansteps where stepId = '" . $stepId . "'");
            return 'done';

        }

        function publishPlanleader($database, $planId)
        {
            return $database->executeNonQuery("Update `plan` set `privacy` = 1 where `parentId` = '" . $planId . "' or `planId` = '" . $planId . "'");
        }

        function publishPlan($database, $planId, $userId, $leaderId)
        {
//			$database->executeNonQuery("Update `plan` set `privacy` = 1 where `parentId` = '" . $planId . "' or `planId` = '" . $planId . "'");

            $planStatus = $database->executeObject("select * from `plan` where `planId` = '" . $planId . "' and `userId` = '" . $leaderId . "'");
            if (($planStatus->isactive == 0))
            {
                $planId = $database->executeScalar("SELECT * FROM plan WHERE userId = '" . $leaderId . "' AND parentId = '" . $planId . "' AND isactive=1");
            }


            $database->executeNonQuery("INSERT INTO plan (userId, title ,isDoubleOptIn, isactive, `addDate`, groupId, reminderValue, reminderUnit, privacy, parentId, leaderId,oldId)
SELECT '" . $userId . "', title ,isDoubleOptIn, isactive, CURDATE(), groupId, reminderValue, reminderUnit, privacy, planId , '" . $leaderId . "','" . $planId . "' FROM plan WHERE planId = '" . $planId . "'");
            $planIdUpdated = $database->insertid();

            $steps = $database->executeObjectList("select * from plansteps where planId ='" . $planId . "'");

            foreach ($steps as $s)
            {
                $parentId = ($s->parentId == 0 ? $s->stepId : $s->parentId);

                $database->executeNonQuery("INSERT INTO plansteps
				(planId,summary,isTask,daysAfter,order_no,description,isactive,oldId,parentId) values
				('" . $planIdUpdated . "',
				'" . mysql_real_escape_string($s->summary) . "',
				'" . $s->isTask . "',
				'" . $s->daysAfter . "',
				'" . $s->order_no . "',
				'" . mysql_real_escape_string($s->description) . "',
				'" . $s->isactive . "',
				'" . $s->stepId . "',
				'" . $parentId . "')");

                $stepId = $database->insertid();

                if ($s->isTask == 0)
                {
                    $planStepEmailDetails = $database->executeObject("select * from `planstepemails` where `id` = '" . $s->stepId . "'");


                    $database->executeNonQuery("Insert into `planstepemails` (`id`,`subject`,`body`,`fromUser`) values
												('" . $stepId . "',
												'" . mysql_real_escape_string($planStepEmailDetails->subject) . "',
												'" . mysql_real_escape_string($planStepEmailDetails->body) . "',
												'" . $planStepEmailDetails->fromUser . "')");
                }

                $dat = date('Y-m-d');
                $date = strtotime($dat);
                $dueDate = strtotime("+" . $s->daysAfter . " day", $date);
                $contacts = $database->executeObjectList("SELECT * FROM contact WHERE userId='" . $userId . "'");
                foreach ($contacts as $c)
                    $database->executeNonQuery("INSERT INTO planstepusers (contactId, stepId, stepStartDate, stepEndDate) values ('" . $c->contactId . "','" . $stepId . "','" . date('Y-m-d') . "','" . date('Y-m-d', $dueDate) . "')");
            }

            //}
            return 'done';
        }

        function archiveStep($database, $stepId)
        {
            $database->executeNonQuery("update `plansteps` set `isactive` = '0',`isArchive`='1' where `stepId` = '" . $stepId . "'");

            return 'done';
        }

        function archiveStepForUser($database, $stepId)
        {
            $database->executeNonQuery("update `plansteps` set `isactive` = '0',`isArchive`='1' where `stepId` = '" . $stepId . "'");

            return 'done';
        }

        function setPublishedPlanForUser($database, $userId, $leaderId)
        {

            $plan = $database->executeObjectList("SELECT  title ,isDoubleOptIn, isactive, CURDATE(), groupId, reminderValue, reminderUnit, privacy, planId, '" . $leaderId . "' FROM plan WHERE userId = '" . $leaderId . "' AND privacy='1'");

            foreach ($plan as $p)
            {
                $database->executeNonQuery("INSERT INTO plan (userId, title ,isDoubleOptIn, isactive, `addDate`, groupId, reminderValue, reminderUnit, privacy, parentId, leaderId,oldId) values ('" . $userId . "','" . $p->title . "','" . $p->isDoubleOptIn . "','" . $p->isactive . "','" . $p->addDate . "','" . $p->groupId . "','" . $p->reminderValue . "','" . $p->reminderUnit . "','" . $p->privacy . "','" . $p->parentId . "','" . $leaderId . "','" . $p->planId . "')");
                $planId = $database->insertid();

                $steps = $database->executeObjectList("select * from plansteps where planId ='" . $p->planId . "'");
                foreach ($steps as $s)
                {
                    $parentId = ($s->parentId == 0 ? $s->stepId : $s->parentId);

                    $database->executeNonQuery("INSERT INTO plansteps
					(planId,summary,isTask,daysAfter,order_no,description,isactive,oldId,parentId) values
					('" . $s->planId . "',
					'" . $s->summary . "',
					'" . $s->isTask . "',
					'" . $s->daysAfter . "',
					'" . $s->order_no . "',
					'" . mysql_real_escape_string($s->description) . "',
					'" . $s->isactive . "',
					'" . $s->stepId . "',
					'" . $parentId . "')");

                    $stepId = $database->insertid();

                    $dat = date('Y-m-d');
                    $date = strtotime($dat);
                    $dueDate = strtotime("+" . $s->daysAfter . " day", $date);
                    $database->executeNonQuery("INSERT INTO planstepusers (contactId, stepId, stepStartDate, stepEndDate) values ('" . $userId . "','" . $stepId . "','" . date('Y-m-d') . "','" . date('Y-m-d', $dueDate) . "')");
                }

            }
        }

        function archiveUserPlan($database, $planId, $userId, $leaderId)
        {
            $database->executeNonQuery("update `plan` set `isactive` = 0 ,`isArchive`='1' where `oldId` = '" . $planId . "' AND userId='" . $userId . "'");

            return 'done';
        }

        function editPlan($database, $planName, $planId, $userId, $leaderId)
        {
            if ($userId != $leaderId)
            {
                $database->executeNonQuery("Update `plan` set `title` = '" . $planName . "' where planId = '" . $planId . "'");

                return $planId;
            }
            else
            {
                $database->executeNonQuery("Update `plan` set `isactive` = 0 where `planId` = '" . $planId . "' ");
                $database->executeNonQuery("INSERT INTO plan (userId, title ,isDoubleOptIn, isactive, `addDate`, groupId, reminderValue, reminderUnit, privacy, parentId, leaderId,oldId) SELECT '" . $userId . "', '" . $planName . "',isDoubleOptIn, '1', CURDATE(), groupId, reminderValue, reminderUnit, privacy, '" . $planId . "' , '" . $leaderId . "','" . $planId . "' FROM plan WHERE planId = '" . $planId . "'");
                $planIdUpdated = $database->insertid();
                $steps = $database->executeObjectList("select * from plansteps where planId ='" . $planId . "'");
                foreach ($steps as $s)
                {
                    $parentId = ($s->parentId == 0 ? $s->stepId : $s->parentId);

                    $database->executeNonQuery("INSERT INTO plansteps
				(planId,summary,isTask,daysAfter,order_no,description,isactive,oldId,parentId) values
				('" . $planIdUpdated . "',
				'" . mysql_real_escape_string($s->summary) . "',
				'" . $s->isTask . "',
				'" . $s->daysAfter . "',
				'" . $s->order_no . "',
				'" . mysql_real_escape_string($s->description) . "',
				'" . $s->isactive . "',
				'" . $s->stepId . "',
				'" . $parentId . "')");
                    $stepId = $database->insertid();

                    $dat = date('Y-m-d');
                    $date = strtotime($dat);
                    $dueDate = strtotime("+" . $s->daysAfter . " day", $date);
                    $contacts = $database->executeObjectList("SELECT * FROM contact WHERE userId='" . $userId . "'");
                    foreach ($contacts as $c)
                        $database->executeNonQuery("INSERT INTO planstepusers (contactId, stepId, stepStartDate, stepEndDate) values ('" . $c->contactId . "','" . $stepId . "','" . date('Y-m-d') . "','" . date('Y-m-d', $dueDate) . "')");
                }

                return $planIdUpdated;
            }
        }

        function editPlanFromUser($database, $planId, $userId, $leaderId)
        {

            $parentId = $database->executeScalar("SELECT `parentId` FROM `plan` WHERE `planId`='" . $planId . "'");
            $newPlanId = $database->executeScalar("SELECT `planId` FROM `plan` WHERE `oldId` = '" . $planId . "' AND `userId` = '" . $leaderId . "' AND  `isactive` = 1");
            $database->executeNonQuery("Update `plan` set `isactive` = 0 where `oldId` = '" . $planId . "' AND `userId` = '" . $userId . "' AND `isactive` = 1");
            if ($parentId == 0)
                $parentId = $planId;


            $database->executeNonQuery("INSERT INTO plan (userId, title ,isDoubleOptIn, isactive, `addDate`, groupId, reminderValue, reminderUnit, privacy, parentId, leaderId,oldId) SELECT '" . $userId . "', title ,isDoubleOptIn, isactive, CURDATE(), groupId, reminderValue, reminderUnit, privacy, '" . $parentId . "' , '" . $leaderId . "','" . $newPlanId . "' FROM plan WHERE planId = '" . $newPlanId . "' AND userId='" . $leaderId . "'");

            $planIdUpdated = $database->insertid();
            $steps = $database->executeObjectList("select * from plansteps where planId ='" . $newPlanId . "'");
            foreach ($steps as $s)
            {
                $parentId = ($s->parentId == 0 ? $s->stepId : $s->parentId);

                $database->executeNonQuery("INSERT INTO plansteps
				(planId,summary,isTask,daysAfter,order_no,description,isactive,oldId,parentId) values
				('" . $planIdUpdated . "',
				'" . mysql_real_escape_string($s->summary) . "',
				'" . $s->isTask . "',
				'" . $s->daysAfter . "',
				'" . $s->order_no . "',
				'" . mysql_real_escape_string($s->description) . "',
				'" . $s->isactive . "',
				'" . $s->stepId . "',
				'" . $parentId . "')");

                $stepId = $database->insertid();

                $dat = date('Y-m-d');
                $date = strtotime($dat);
                $dueDate = strtotime("+" . $s->daysAfter . " day", $date);
                $contacts = $database->executeObjectList("SELECT * FROM contact WHERE userId='" . $userId . "'");
                foreach ($contacts as $c)
                    $database->executeNonQuery("INSERT INTO planstepusers (contactId, stepId, stepStartDate, stepEndDate) values ('" . $c->contactId . "','" . $stepId . "','" . date('Y-m-d') . "','" . date('Y-m-d', $dueDate) . "')");
            }

            return 'done';
        }

        function checkStepStatus($database, $stepId, $userId)
        {


            $stepCount = $database->executeScalar("SELECT COUNT(*) FROM plansteps WHERE isUserChange='1' AND isArchive='0' AND isactive='1'  AND parentId=
(SELECT parentId FROM plansteps WHERE stepId=
(SELECT changeId FROM `notification` WHERE notificationId=
(SELECT notificationId FROM `notificationuserstatus` WHERE userstatusId='" . $stepId . "'AND userId='" . $userId . "') GROUP BY parentId))");
            echo $stepCount;
            return $stepCount;

        }

    }

?>