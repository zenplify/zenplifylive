<?php
session_start();
error_reporting(0);
include_once("../headerFooter/header.php");
require_once ('../../classes/init.php');
require_once ('../../classes/contact_details.php');

$database= new database();
$c1=new  contactDetails();
$userId=$_SESSION['userId'];


$emailid=$_REQUEST['id'];



if(!empty($emailid))
{
	$data=$c1->getEmailHistory($database,$emailid);
	
	if(!empty($data))
	{
		foreach ($data as $row)
		{
			$toemail=$row->toEmail;
			$fromemail=$row->fromEmail;
			$subject=$row->subject;
			$body=$row->body;
			$body=str_replace("\'","'",$body);
		}
	}
}

?>
<script type="text/javascript" src="../../js/nicEdit.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
   // $('#templates').change(function (){ showtemplate();});
});
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
	$(document).ready(function(e) {
    	$("#firstName").click(function() {$(".nicEdit-main").html($(".nicEdit-main").html()+"[{First Name}]");});    
		$("#lastName").click(function() {$(".nicEdit-main").html($(".nicEdit-main").html()+"[{Last Name}]");});  
		$("#phoneNumber").click(function(e) {$(".nicEdit-main").html($(".nicEdit-main").html()+"[{Phone Number}]");}); 
		$("#companyName").click(function(e) {$(".nicEdit-main").html($(".nicEdit-main").html()+"[{Company Name}]");});   
    });
function showtemplate()
	{
		
		var id=$('#templates').val();
		//alert(id);
		if(id!='')
		{
			//alert('If : '+id);
			$.ajax({ url: 'getEmailTemp.php',
	        data: {tempId:id},
	        type: 'post',
	        success: function(output) {
	        	//alert(output);
				if(id=='1')
				{
					subject=$('#sub1').val();
					$('#subject').val(subject);
				}
				if(id=='2')
				{
					subject=$('#sub2').val();
					$('#subject').val(subject);
				}
				if(id=='3')
				{
					subject=$('#sub3').val();
					$('#subject').val(subject);
				}
	        	$('#area2').val(output);
				$('.nicEdit-main').html(output);
				
				 //nicEditors.allTextAreas() ;
				//document.write(output);					
	                 }
	});
		}
	}
	
</script>
<!--  <div id="menu_line"></div>-->

<div class="container">

  <h1 class="gray"> Email Step</h1>
  
    <table cellspacing="0" cellpadding="0">
    
      <tr>
        
        <td class="label">From</td>
        <td><input type="text" name="from" id="from"  class="textfieldbig" value="<?php echo $fromemail;?>"/></td>
      </tr>
      <tr>
        
        <td class="label">To</td>
        <td><input type="text" name="from" id="from"  class="textfieldbig" value="<?php echo $toemail;?>"/></td>
      </tr>
      <tr>
        <td class="label">Subject</td>
        <td><input type="text" name="subject" id="subject"  class="textfieldbig" value="<?php echo $subject;?>"/></td>
      </tr>
     
      <tr>
        <td class="label"></td>
        <td style="padding-left:10px;">
        <textarea name="area2" style="width:648px; height:200px; margin-left:12px;" id="area2" ><?php echo $body;?></textarea>
        
        </td>
      </tr>
        </tr>
      <tr>
        <td >&nbsp;</td><td >&nbsp;</td>
       </tr> 
      <tr>
        <td class="label">&nbsp;</td>
        <td >
          <input type="button" name="cancel" class="cancel" value="" onclick="javascript:history.go(-1)"/></td>
      </tr>
    </table>
  </form>
  <div style="min-height:70px;"></div>
  <?php
     include_once("../headerFooter/footer.php");
	 ?>
<script type="text/javascript">
function validate()
{
	var summary=false;
	var days=false;
	if($('#summary').val()=='' || $('#summary').val()==null )
	{
		$('#alertSummary').css("visibility","visible");
		summary=false;
	}
	else
	{
		$('#alertSummary').css("visibility","hidden");
		summary=true;
	}
	if($('#daysAfter').val()=='' || $('#daysAfter').val()==null )
	{
		$('#alertDays').css("visibility","visible");
		days= false;
	}
	else
	{
		$('#alertDays').css("visibility","hidden");
		days= true;
	}
	if(summary==true && days==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function cancelPage(id)
{
	//alert("hello");
	window.open ('create_plan_detail.php?id='+id,'_self',false)
}
function deleteStep()
{

	if(confirm('Are you sure you want to permanently delete this Step?'))
		{
			var id="<?php echo $step_id;?>";

			$.ajax({ url: '../../ajax/ajax.php',
	        data: {action: 'DeletePlanStep',step_id:id},
	        type: 'post',
	        success: function(output) {
	        	//alert(output);
	        	window.open ('create_plan_detail.php?id='+id,'_self',false)
				//document.write(output);					
	                 }
	});
		}

}

</script>