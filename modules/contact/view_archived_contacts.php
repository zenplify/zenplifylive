<?php 
session_start();
error_reporting(0);
include_once("../headerFooter/header.php");
require_once('../../classes/contact.php');
require_once('../../classes/group.php');
$userId=$_SESSION['userId'];
//$_SESSION['groupsArray']='';
$contact=new contact();
$group=new group();


if ($_GET['archive'] == 'success')
{
	$mes= 'Contact has been Archived Successfully.';
}
if (isset($_GET['message']))
{
	$mes= $_GET['message'];
}

/*if(empty($_SESSION['groupsArray']))
{*/
	$data=$contact->GetAllArchivedContacts($userId);
/*}
else
{
	//print_r($_SESSION['groupsArray']);
	$groupsArray=implode(',',$_SESSION['groupsArray']);
	$data=$contact->GetGroupContacts($userId,$groupsArray);
}*/

?>

 <script type="text/javascript" charset="utf-8">
 function removejscssfile(filename, filetype){
 var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
 var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
 var allsuspects=document.getElementsByTagName(targetelement)
 for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
 }
}
removejscssfile("jquery-ui-timepicker-addon.js", "js");
removejscssfile("jquery-ui.js", "js");

removejscssfile("jquery-ui.css", "css") ;//remove all occurences "somestyle.css" on page
</script>
 <script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
 <script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
 <script type="text/javascript" src="../../js/script.js"></script>
 <script type="text/javascript" src="../../js/nicEdit.js"></script>
 <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>


 <script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#chkSelectDeselectAll').change(function() {
					var checkboxes = $(this).closest('form').find(':checkbox');
					if($(this).is(':checked')) {
						checkboxes.attr('checked', 'checked');
					} else {
						checkboxes.removeAttr('checked');
					}
				});
				
				$("#pmManager").click(function(){
					if(CheckSelectedPermissionContacts()){
						
					var arr = [];
					var i= 0;
			
					$('.ads_Checkbox:checked').each(function(){arr[i++] = $(this).val();});
 					var contactIds = arr.toString();
    				$.ajax({url:'optInMessage.php',type:'post',data:{contactsids:contactIds},success: function(result){
						emails=result.split('&');
						var from=emails[0];
						var to=emails[1];
						$("#to").val(to);
						$("#from").val(from);
						$("#emailId").val(emails[2]);
						$("#contactId").val(emails[3]);
						$("#emailflag").val(1);
						
						
						loadPermissionManager();
						}});
						
					}else{alert('Please select atleast one contact');}
						
					});
					//UncheckAll();
					//get_group_contacts();
			        $("#contact_table")
					.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
					.tablesorterPager({container: $("#pager"),size:50}); 
					 
		
		$('*[id^=groupName]').click(function(e)
		{
			
			get_group_contacts();
			
			});   
		$('#transfer').click(function(){
			if(CheckSelected())
			{
				loadPopupTransfer();
				centerPopupTransfer();
			}
			else
			{
				alert('Please select atleast one contact');
			}
		});
		
		$('#restoreContacts').click(function(){
			var arrContacts = [];
			if(CheckSelected())
			{
				arrContacts=get_contacts();
				
				$.ajax({ url: '../../classes/ajax.php?contactId='+arrContacts,
    	        data: {action: 'restoreAllContacts'},
    	        type: 'post',
    	        success: function(output) {
        	       
				   //$('.messageofsuccess').html('Contacts have been Restored Successfully');
				 window.location.href='view_archived_contacts.php?message=Contact(s) have been restored successfully';
				   //alert(output)
				      	           }
    	});
			}
			else
			{
				alert('Please select atleast one contact');
			}
		});
		
		$('#deleteContacts').click(function(){
			var arrContacts = [];
			if(CheckSelected())
			{
				arrContacts=get_contacts();
				
				$.ajax({ url: '../../classes/ajax.php?contactId='+arrContacts,
    	        data: {action: 'deleteAllContacts'},
    	        type: 'post',
    	        success: function(output) {
        	       
				   //$('.messageofsuccess').html('Contacts have been Deleted Successfully');
				   window.location.href='view_archived_contacts.php?message=Contact(s) have been deleted successfully';
				   //alert(output)
				      	           }
    	});
			}
			else
			{
				alert('Please select atleast one contact');
			}
		});
		
		$('#export_data').click(function(){
			if(CheckSelected())
			{
				contacts=get_contacts();
				
				window.location.href='../import/export_data.php?con='+contacts;
				
			}
			else
			{
				alert('Please select atleast one contact');
			}
		});
		
		$("#backgroundPopup").click(function(){
			disablePopupTransfer();
			
			
		});
		
		$(".cancel").click(function(){
			disablePopupTransfer();
			
			
		});

		$("#get_User").click(function(){

			 var name = $( "#userName" );
				//alert(name.val());	
			$.ajax({ url: '../../ajax/ajax.php',
    	        data: {action: 'verifyUser',u_name:name.val()},
    	        type: 'post',
    	        success: function(output) {
        	       if(output!='')
            	       {
        	    	  	 transfer(output);
         	    	  	disablePopupTransfer();
            	       }
        	       else
            	       {
							alert("In valid User Name.");		
            	       }
        	       // disablePopupTransfer();
    	           }
    	});
			//disablePopupTransfer();
			
			
		});
	
		/*$("#pagesize").change(function(){
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {action:'paginationOfContact',records:$(this).val(),userId:<?php //echo $userId;?>},
   success: function(data){
	   $("#tableBody").html(data)
	
	  
   }
 });
						
			
			});*/
	$("#sendpermissionconfirmation").change(function() {
        alert('hi');
    });
	
	});
	function sendPermission(contactId){
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionToBlock',contactId:contactId},
    	        type: 'post',
    	        success: function(output) {
        	       loadPermissionManager();
				   $("#to").val(output);
				   
				   //alert(output)
				      	           }
    	});
		
		$("#contactId").val(contactId);
		}
		
	function restoreContact(contactId){
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'restoreContact',contactId:contactId},
    	        type: 'post',
    	        success: function(output) {
        	       
				   //$('.messageofsuccess').html('Restore Successfull');
				    window.location.href='view_archived_contacts.php?message=Contact has been restored successfully';
				   //window.location.reload();
				   //alert(output)
				      	           }
    	});
	
		}
		
		function deleteContact(contactId){
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'deleteContact',contactId:contactId},
    	        type: 'post',
    	        success: function(output) {
        	       
				   //$('.messageofsuccess').html('Delete Successfull');
				    window.location.href='view_archived_contacts.php?message=Contact has been deleted successfully';
				   //window.location.reload();
				   //alert(output)
				      	           }
    	});
		
		
		}
	function permissionMail(){
		var to=$("#to").val();
		var from=$("#from").val();
		var subject=$("#subject").val();
		var emailbody=$("#body").val();
		var userId=$("#userId").val();
		var contactId=$("#contactId").val();
		var emailId=$("#emailId").val();
		var emailflag=$("#emailflag").val();
		
		$.ajax({ url: '../../classes/ajax.php',
    	        data: {action: 'sendPermissionMail',to:to,from:from,subject:subject,emailbody:emailbody,userId:userId,contactId:contactId,emailId:emailId,emailflag:emailflag},
				type: 'post',
    	        success: function(output) {
					
        	       if(output==1){
					
					   disablepermissionPopup();}
				  $("#messageofsuccess").html('Thank you for confirming your Email Address and approving future Emails');
				  
				  }
				  
    			});
			}	
	
	</script>
<!--  <div id="menu_line"></div>-->
  <div class="container">
<h1 class="gray">Restore Contacts</h1>
     <div class="top_content">
<!--  
     <form name="contacts_form" id="contacts_form" action="#" method="">
-->  
     <!--<a href="new_contact.php"><input type="button" id="new_contact" value=""  /></a>-->
      <!--<a href="permission_manager.php"><input type="button" id="permission_manager" value=""  /></a>-->
    <div style="float:left; margin-left:135px; width:270px; margin-top:6px;"><font class="messageofsuccess" ><?php echo $mes; ?></font></div>
    
     <!--<input type="button" id="transfer_contact" value="transfer_contact"  />
     
     
     <div id="popupTransfer">
      <h1>List of Transfer Contacts</h1>
      <form>
      <table cellpadding="0" cellspacing="0" border="1px">
      <tr>
      <th> Name </th>
      <th> Action </th>
      </tr>
   
      </table> 
      
      <br/>
      
      <input type="button" class="transfer_btn" id="get_User" /> 
      <input type="button" class="cancel" style="margin-left:10px;" />
     
      </form>
      </div>-->
     
     
     
     
     
     
     
     
     
     
     
     <p id="sorting">
     <input type="button" id="restoreContacts" name="restoreContacts"  class="restoreContacts" value=""  />
     <input type="button" id="deleteContacts" name="deleteContacts" value="" class="deleteContacts" />
     </p>
     <!--<p id="sorting">
       
     	<img src="../../images/contact transfericon.png" title="Transfer Contact" alt="Transfer Contact" onclick="return dataTransfer()" />
     <a href="../import/import_contact.php"><img src="../../images/import.png" alt="Import" title="Import" /></a>
    <img src="../../images/export.png"  alt="Export" title="Export" id="export_data" />
     <img src="../../images/permission manager.png"  title="Permission Manager" alt="Permission Manager" id="pmManager" />
     <a href="view_archived_contacts.php" ><img src="../../images/restoreContacts.png" alt="Restore" title="Restore"  /></a>
     <img src="../../images/printer.png" alt="Print" title="Print" onclick="return PrintContent()"  /></p>-->
  
     </div>
     <div class="sub_container">
     <div class="col_table">
<form name="view_all_contact" method="post" action="<?php  $_SERVER['PHP_SELF']?>" id="view_all_contact">              
<div id="prints">
<!--  
<table id="myTable" cellpadding="0" cellspacing="0" border="0" class="tablesorter" >
-->	

<table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter" >
<thead>

                <tr class="header">
                    <!-- 
                    <th class="first_table"><input type="checkbox" id="chkSelectDeselectAll" onclick="SelectDeselectContact(this)"/><label></label></th>
                     -->
                     <th class="first_table"><input type="checkbox" id="chkSelectDeselectAll" /><label></label></th>
                     <th class="center exbig">Name <img src="../../images/bg.gif" /></th>
                     <th class="center big">Deleted Date & Time<img src="../../images/bg.gif" /></th>
                     
             
                     <th class="center big">Action  </th>
                 
                    <th class="last_table"></th>
		</tr>
	</thead>

	<tbody id="tableBody">
	<?php 
	if(!empty($data))
{
	foreach ($data as $row)
	{
		
		$id=$row->contactId;
		
		
		$date = $row->archiveDate;
		$dateTime=date(" m/d/Y g:i a", strtotime($date));
		echo "<tr class=\"odd_gradeX\" id=\"".$id."\">";
			//echo "<td><input type=\"checkbox\" id=\"select_contact\" name=\"select_contact[]\" value=".$row->contactId."></td>";
			echo "<td><input type='checkbox' name='contact[]' class='ads_Checkbox' id='contact'  value=".$row->contactId." ></td>";
			echo "<td><a href=contact_detail.php?contactId=".$id.">".$row->firstName." ".$row->lastName."</a></td>";
			echo "<td>".$dateTime."</td>";
			echo "<td><img src='../../images/restore(1).png' alt='Restore' title='Restore' onClick=\"restoreContact(".$row->contactId.")\" width='14' height='18'  > <img src='../../images/delete.png' alt='Delete' title='Delete' onClick=\"deleteContact(".$row->contactId.")\" ></td>";

			echo "<td></td>";
		echo "</tr>";
		
	}
}
else
{
	echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
}
	?>
	
	</tbody>
</table>
</div>
</form>
<div style="clear:both"></div>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
	<tr style="border:10px">
    
 <td></td>
 	<td>
 	
 		
        <div id="pager" class="pager" style="float:right; margin-top:5px; ">
			<form>
				<img src="../../images/paging_far_left.gif" class="first"/>
					<img src="../../images/paging_left.gif" class="prev"/>
					<input type="text" class="pagedisplay"/>
					<img src="../../images/paging_right.gif" class="next"/>
					<img src="../../images/paging_far_right.gif" class="last"/>
					
					
					<select id="pagesize" name="pagesize" class="pagesize">
						
						<option value="50">50 </option>
						<option value="100">100 </option>
						<option value="250">250 </option>
					
						
					</select>
			</form>
		
		</div>
	</td>
    </tr>
</tbody>
</table>
               
       		</div>
           <!--<div class="col_search">
                <p class="search_header"><label for="kwd_search">Find Contacts</label></p>
                <!--  
                <label for="kwd_search">Search:</label> 
                <input type="text" id="kwd_search" value=""/>
           		
           		<input type="text" name="searchContact" class="searchbox2" id="kwd_search" title="Search Contacts" />
                 <br />
              <!--  <img src="images/check.png" /> Include Custome Fields-->
             
              <!--  <p class="search_header small">Filter By:</p>
                 <p class="search_header2 small">Interest Level</p>
                <div id="groups_list">
                <form name="myform" action="" method="post">
                	<ul>
                    	<li><input type="checkbox" name="list" value="leader"  onClick="check(document.myform.list)" id="interestlevel1" /><label>Leader</label></li>
                        <li><input type="checkbox" name="list" value="Consultant" onClick="check(document.myform.list)" id="interestlevel2" /><label>Consultant</label></li>
                     
                        <li><input type="checkbox" name="list" value="Host" onClick="check(document.myform.list)" id="interestlevel3"  /><label>Host</label></li>
                        <li><input type="checkbox" name="list" value="Client" onClick="check(document.myform.list)" id="interestlevel4"  /><label>Client</label></li>
                        
                    
                    </ul>
                 </form>   
                </div>
                
               
           </div>-->
          
            <div class="empty"></div>  
            <div class="empty"></div>  
            <div class="empty"></div>  
            <div class="empty"></div>  
           
       </div>
       <?php
       $from=$contact->GetEmail($userId);
	 
	
      ?>
	  <div id="backgroundPopup"></div>
      <div id="popupTransfer">
      <h1>Transfer Contacts</h1>
      <form>
      <label class="survey_label">User Name</label>
      <input type="text" class="textfield" name="userName" id="userName" />
      <br/>
      <input type="button" class="transfer_btn" id="get_User" /> 
      <input type="button" class="cancel" style="margin-left:10px;" />
      </form>
      </div>
      
      <div id="Popup"></div>
<div id="popuppermission" >
  <h1 class="gray">Email Permission Confirmation</h1>
  <form action="?action=success" method="post">
   
    <input type="hidden" name="contactId" id="contactId"  />
    <input type="hidden" name="emailId" id="emailId"  />
     <input type="hidden" name="emailflag" id="emailflag"  />
    
     <input type="hidden" name="userId" id="userId"  value="<?php echo $userId;?>" />
    <table width="100%" border="0">
      <tr>
        <td class="label">From</td>
        <td><input type="text" name="from" id="from"  class="textfieldbig" value="<?php echo  $from;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">To</td>
        <td><input type="text" name="to" id="to"  class="textfieldbig" value="<?php echo $to;?>"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="label">Subject</td>
        <td><input type="text" name="subject" id="subject"  class="textfieldbig" value="Permission Confirmation"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td style="padding-left:12px;"><textarea name="area2" id="body" style="width:648px; height:200px;  ">
    
</textarea>
<div style="background-color:#EAEAEA;width:640px; padding:5px; padding-bottom:0px; margin-bottom:-5px;">
<p style="font-size:12px">
Your information is safe and will not be shared!<br /><br />I'll be sending a little info each day for the first week or so to help you get to know Arbonne, and a monthly newsletter if you requested it on your guest profile<br/><br/>To Accept Email click the link below. <br/>
<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br />
To Decline Email click the link below:<br/>
<span style="color:#06F; font-size:12px;"> https://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span>
</p>
</div>

<!--<div style="background-color:#EAEAEA;width:640px; padding:5px;margin-bottom:15px;">
<p style="font-size:12px">
Please click the link below TO RECEIVE MY NEWSLETTER <br />
<span style="color:#06F; font-size:12px;"> hhttp://zenplify.biz/modules/contact/subscribe.php?subscribe=success&sid=3&uid=100&cid=2013 </span> <br />
Click on following links to Unsubscribe <br />
<span style="color:#06F; font-size:12px;"> http://zenplify.biz/modules/contact/subscribe.php?subscribe=failure&sid=4&u </span> 
</p>
</div>-->

</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="button" name="submit" class="sendpermissionconfirmation" value="" onclick="permissionMail();"/><input type="button" name="cancel" class="cancel" onclick="disablepermissionPopup();"/></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
</div>

      
      
   <?php 

    	include_once("../headerFooter/footer.php");
	?>
   <script type="text/javascript">
   var contactArray=new Array();
   $(document).ready(function(){
		// Write on keyup event of keyword input element
		$("#kwd_search").keyup(function(){
// When value of the input is not blank
var size=$(this).val().length;
var srch=$(this).val();
if(size>=3){
$.ajax({
type: "POST",
url: "../../classes/ajax.php",
data: {action: 'searchInfo',searchdata:srch,userId:<?php echo $userId;?>},
success: function(data){

$("#tableBody").html(data);
$("#contact_table")
.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
.tablesorterPager({container: $("#pager"),size:50});

}


});

}else if(size==0){
$.ajax({
type: "POST",
url: "../../classes/ajax.php",
data: {action: 'searchInfo',searchdata:srch,userId:<?php echo $userId;?>},
success: function(data){

$("#tableBody").html(data);
$("#contact_table")
.tablesorter({widthFixed: true, widgets: ['zebra'], headers: { 0: {sorter: false}}})
.tablesorterPager({container: $("#pager"),size:50});

}
});

}

});

		
	});
	// jQuery expression for case-insensitive filter
	$.extend($.expr[":"], 
	{
	    "contains-ci": function(elem, i, match, array) 
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
	function check(field)
	{
		$("#contact_table tbody>tr").hide();

		var list = new Array();
		var j=0;
	 	for (i=0;i<field.length;i++)
	 		{
				if (field[i].checked==true)
				{	
					list[j]=field[i].value;	
					j++;
					
				}
			
	 		}
		var arrayLength=list.length;
		if(arrayLength!=0)
			{
				for (k=0;k<arrayLength;k++)
	 				{
						$("#contact_table td:contains-ci('" + list[k] + "')").parent("tr").show();
	 				}
			}
		else
			{
				$("#contact_table tbody>tr").show();
			}
 		//alert (list.length);		


 	}


	function PrintContent()
	{
		var DocumentContainer = document.getElementById('prints');
		var WindowObject = window.open('', "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
		WindowObject.document.writeln(DocumentContainer.innerHTML);
		WindowObject.document.close();
		WindowObject.focus();
		WindowObject.print();
		WindowObject.close();
	}


	
	function dataTransfer()
	{
		
			var length=document.view_all_contact.contact.length;
			var field=document.view_all_contact.contact;
			var check=0;
			//alert(length);
			if(length!=undefined)
			{
				j=0;
				for (i = 0; i < length; i++)
				{
					if(field[i].checked ==true )
					{
						
						contactArray[j]=field[i].value;
						j++;
						check=1;
							
					}
				}
				
			}
			else //If there is only 1 item
			{
					
				if(field.checked==true)
				{
						check=1;
						contactArray=field.value;
				}		
			}
			
			if(check==1)
			{
				//alert("here");
				//loadEmailPopup();
				//centerEmailPopup();
				loadPopupTransfer();
				centerPopupTransfer();
					
			}
			else
			{
				alert("Please select atleast one contact");
			}
	}
	function get_contacts()
	{
			var contactsArray=new Array();
			var length=document.view_all_contact.contact.length;
			var field=document.view_all_contact.contact;
			var check=0;
			//alert(length);
			if(length!=undefined)
			{
				j=0;
				for (i = 0; i < length; i++)
				{
					if(field[i].checked ==true )
					{
						
						contactsArray[j]=field[i].value;
						j++;
						check=1;
							
					}
				}
				
			}
			else //If there is only 1 item
			{
					
				if(field.checked==true)
				{
						check=1;
						contactsArray=field.value;
				}		
			}
			return contactsArray;
	}
	function cancelTransfer()
	{
		disableEmailPopup();
		
	}

	function transfer(userId)
	{
		
		//contact=$.serialize(contactArray);
		//alert (userId);
		// send request user id 
		var s_user_id=<?php echo $_SESSION['userId'] ?>;	
		$.ajax({ url: '../../ajax/ajax.php?con='+contactArray,
	        data: {action: 'transferContact',user_id:userId,SUserId:s_user_id},
	        type: 'post',
	        success: function(output) {
					//alert(output);
	    	      // document.write(output);
    	        	disablePopupTransfer();
	           }
		});

		
	}
	
	function UncheckAll(){ 
      var w = document.getElementsByTagName('input'); 
      for(var i = 0; i < w.length; i++){ 
        if(w[i].type=='checkbox'){ 
          w[i].checked = false; 
        }
      }
  } 

function get_group_contacts()
{
	var Arraygroups=[];	
		 $("*[id^=groupName]").each(function() {
                                         var groupId = $(this).val();
										 if($(this).is(':checked')){
                                         Arraygroups.push(groupId);}
                        });	
			
			//alert(Arraygroups)
			$.ajax({
   				type: "POST",
   				url: "get_group_contacts.php?groupIds="+Arraygroups,
  				data: {groupIds:Arraygroups},
   				success: function(data){
	   			$("#tableBody").html(data)
	   			check(document.myform.list);
	
	  
   }
 });
}
   </script>
   