<?php
session_start();
require_once("../../classes/contact.php");
require_once("../../classes/group.php");
require_once("../../classes/plan.php");
require_once("../../classes/task.php");
require_once("../../classes/appiontment.php");


$contact= new contact();
$group= new group();
$plan =new plan();
$task =new task();
$appiontment= new appiontment();
$contactId=$_POST['cid'];
$userId= $_SESSION["userId"];

$contactdetail=$contact->showContactDetail($contactId);
$contactaddress=$contact->showContactAddress($contactId);
$conatctemail=$contact->showContactEmailAddress($contactId);
$conatctphone=$contact->showContactPhoneNumber($contactId);
$contacttimetocall=$contact->showContactTimeForCall($contactId);
$stickyNotes=$task->showStickyNotes($contactId);

//current and upcoming activities
$activities=$task->upcomingActivities($userId,$contactId);
$appiontmentactivities=$appiontment->upcomingActivitiesAppiontments($userId,$contactId);
//activities History
$activitiesCompleted=$task->ActivitiesHistory($userId,$contactId);

//Notes to show in contact Activity History
$notes=$task->contactNotes($contactId);
?>
 <!--<link rel="stylesheet" href="../../js/alert/css/jquery.toastmessage.css" type="text/css">	      
	  <script src="../../js/alert/jquery.toastmessage.js" type="text/javascript"></script>-->
<script type="text/javascript">

            
jQuery(document).ready(function(){				
				
				$("#preloader").hide();				
				jQuery("[id^=editTab]").click(function(){	
					
					// get tab id and tab url
					tabId = $(this).attr("id");	
					tabUrl = jQuery("#"+tabId).attr("href");
					jQuery("[id^=tab]").removeClass("current");
					jQuery("#"+tabId).addClass("current");
					
					// load tab content
					loadTabContent(tabUrl);
					return false;
				});
				
				/*$("#change").click(function(){$('#navcontainer').css({"display":"none"});$('#nav_container').show();});*/
			});
function delupcomingActivities(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {taskId:id,action:'deleteTask',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	     $('#record'+id).fadeOut(1000);
		   }
 });
	//$('#record'+id).fadeOut(1000);
	}
function delupcomingAppiontments(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {appiontmentId:id,action:'deleteAppiontment',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordAppiontment'+id).fadeOut(1000);
		   }
 });
	}	
function delnotes(id){
	//alert('u want to delete');
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {notesId:id,action:'deleteNotes',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	      $('#recordNotes'+id).fadeOut(1000);
		
   }
 });
	}
function saveNotes(){

	$.ajax({type: "POST",url: "../../classes/ajax.php",data: {stickyNotes:$('#stickyNotes').val(),action:'checkStickyNotes',contactId:'<?php echo $contactId;?>'},
   	success: function(msg){
		
	   if(msg!=''){
		  $('#stickyNotes').val(msg); 
		  $().toastmessage('showSuccessToast', "Saved");
		 
		   }else{
	     alert('error');
			   }
   }
 });
	
	
   
	}
function moveToHistory(){
	
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {stickyNotes:$('#stickyNotes').val(),action:'moveToHistory',contactId:'<?php echo $contactId;?>'},
   success: function(msg){
	  $('.appendnotes tr:last').after(msg);
	   
   }
 });
	}	
function editTask(editType){
				var taskId=$("#taskId").val();
				window.location.href='../task/edit_task.php?edit_type='+editType+'&task_id='+taskId;
				}
function archiveContact(contactId){
	$.ajax({
   type: "POST",
   url: "../../classes/ajax.php",
   data: {action:'archiveContact',contact_id:contactId},
   success: function(msg){
	  
	   if(msg=='1'){
		window.location.href="view_all_contacts.php";
		   }
   }
 });
	}					
</script>	
<?php
	$p = $_GET['id'];

	switch($p) {
		case "1":
?>
	<div class="top_content" style="width:855px;">
     <a href="../task/new_task.php?contact_id=<?php echo $contactId;?>"><input type="button" style="margin-left:10px;" id="new_task" value=""  /></a>
    
     <a href="../appiontment/new_appiontment.php?contact_id=<?php echo $contactId;?>"><input type="button" style="margin-left:10px;" id="new_appointment" value=""  /></a>
     <a href="#"><input type="button" style="margin-left:10px;" id="newemail" value=""  /></a>
     <input type="button" style="margin-left:5px;" id="archive" value="" id="archive_contact" onclick="archiveContact('<?php echo $contactId;?>')"  />
   	<a href="tabs_contact_edit.php?id=1&contact_id=<?php echo $contactId;?>" id="editTab1" class="editContact">Edit Profile</a>
     </div>
      
     
    	
<div class="firstContent">        
<table cellspacing="0" cellpadding="0">
          <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Address</td>
            <td class="label"><?php echo $contactaddress->zipcode.' '.$contactaddress->street.' '.$contactaddress->city.' '.$contactaddress->state.' '.$contactaddress->country; ?></td>
          </tr>
          <tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          <tr>
          <td class="label" >Best Times To Call</td>
            <td class="label"><?php echo $contacttimetocall->weekdays."<br>".$contacttimetocall->weekdayEvening."<br>".$contacttimetocall->weekenddays."<br>".$contacttimetocall->weekenddayEvening; ?></td>
          </tr>
          <td class="label">Referred By</td>
          <td class="label">
          <?php echo $contactdetail->referredBy;  ?></td>
          </tr>
          <tr>
          <td class="label">Birthday</td>
          <td class="label">
           <?php echo date('m/d/Y',strtotime($contactdetail->dateOfBirth));  ?></td>
          </tr>
          
          <td class="label">Age</td>
          <td  class="label">
            <?php 
	function getAge($then) {
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
    return $age;
}
echo (getAge($contactdetail->dateOfBirth)<0?'0':getAge($contactdetail->dateOfBirth)).' Years';

	  ?></td>
          </tr>
          
          <tr>
          	<td class="label">Company /Title</td>
            <td class="label"><?php echo $contactdetail->companyTitle.'-'.$contactdetail->jobTitle;  ?></td>
          </tr>
          
</table>
</div>

<div class="secondContent">
<div class="t_heading gray">Follow-ups</div>

 <table class="contact_table" style="width:500px;" cellspacing="0" cellpadding="0">
    
        <?php
			
			$showPlan=$plan->showPlanStepsInContactDetail($contactId);//show all groups sort by group type
			if(empty($showPlan)){
				echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Step Assign</span>";
				echo "</td></tr>";
				}else{
            foreach($showPlan as $sp){
		
			?>
            <tr>
          	
            <td class="plan_label" style="padding-left:18px; font-size:12px !important;" ><?php echo ($sp->isActive==0?'':'Skipped: ').$sp->title;?></td>
			
          </tr>
				
			<?php		
				}
				}
			?>
			

</table>
</div>

<div class="sticky_notes">
			<img src="../../images/note--top.png" />	
            <textarea class="stickynotes" onblur="saveNotes();" id="stickyNotes"><?php echo $stickyNotes->notes;?></textarea>
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:block; padding-left:64px; width:238px; height:37px; cursor:pointer; " onclick="moveToHistory();" >Move To History</span>
			</div>
<div class="secondContent">
<div class="t_heading gray">Groups</div>
<div id="groupslist">
                	<ul>

<?php
$contactgroups=$group->showContactGroups($contactId);
if(empty($contactgroups)){
	echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Group Assign</span>";
				echo "</td></tr>";
	}else{
foreach($contactgroups as $cg){
	?>
                    	<li style="background-color:<?php echo $cg->color;?>"><input type="checkbox" checked="checked" id="groupcheckbox[]" name="groupcheckbox[]" value="<?php echo $cg->groupId; ?>" disabled="disabled"><label class="plan_label"><?php echo $cg->name;?></label></li>
	
<?php }
	}
?>
                    
                    </ul>
 </div>


</div>
 
    <!-- 
        <div class="thirdContent">
        
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td class="question_label bullet"  >What brand(s) of weight loss/nutrition products are you currently using? (GNC, Slimfast, Isagenix, etc.)</td>
                </tr>
                <tr>
                <td  class="answer_label">GNC</td>
                </tr>
                <tr>
                <td  class="question_label bullet">What brand(s) of skincare are you currently using? (Clinique, Olay, NuSkin, etc.)</td>
                </tr>
                <tr>
                <td  class="answer_label">Clinique</td>
                </tr>
         </table>       
         
        </div>  
         <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">How often do you add to your monthly grocery bill by stopping for coffee, snacks, fast food, etc?</td>
                </tr>
                <tr>
                 <td  class="answer_label">0-1 times per week</td>
                </tr>
                
         </table>       
         
        </div>
        
<div class="thirdContent">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Which of the following do you experience on a regular basis? (Check all that apply)</td>
                </tr>
                <tr>
                 <td  class="answer_label">Brain fog, lack of concentration and/or poor memory</td>
                </tr>
                </tbody>
                </table> 
         
        </div>
        
<div class="thirdContent">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Check the conditions below that best describe your skin: (check all that apply)*</td>
                </tr>
                <tr> 
                <td  class="answer_label">Combination</td>
                </tbody>
                </table> 
         
        </div>
 <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3  class="question_label">Which of the following products does someone in your family use each month? (Check all that apply):*</td>
                </tr>
                <tr>
                <td  class="answer_label">Snack/Granola/Nutrition Bars</td>
                </tr>
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Imagine you could go to one store to buy all of the snack bars, energy drinks, protein shakes, vitamins, skincare, shampoo and cosmetics that you would need to last your family for a month, how much would your total bill be?</td>
                </tr>
                <tr>
                <td  class="answer_label">$250-$350</td>
                </tr>
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">If you had a magic wand, what would you change about your health, skin and lifestyle?</td>
                </tr>
                <tr>
                <td  class="answer_label">skin</td>
                </tr>
				<tr>
                <td  class="question_label">Have you used Arbonne before?*</td>
                </tr>
                <tr>
                <td  class="answer_label">Yes</td>
                </tr>
                
                
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which of the following are you most interested in? (Check all that apply)*</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span>Better Nutrition</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span>Arbonne Business</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span>Skincare Workshop</td>
                </tr>
                
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">When making a decision about purchasing products, which is more important to you?*</td>
                </tr>
                 <tr>
                <td  class="answer_label"><b>PRICE:</b> The least expensive option</td>
                </tr>
                 <tr>
                <td  class="question_label">What kind of service do you prefer?*</td>
                </tr>
                 <tr>
                <td  class="answer_label"><b>PERSONAL:</b> Phone support and help placing my order from my consultant</td>
                </tr>
                
                
                
                
         </table>       
         
        </div>

         <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)*</td>
                </tr>
                <tr>
                <td class="answer_label">
               <b>TEAM LEADER:</b> Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.
				</td>
                </tr>
                <tr>
                <td class="answer_label">
                	<b>VIRTUAL HOST:</b> Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.
				</td>
                </tr>
				                
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Would you like to receive my monthly newsletter that features current specials and opportunities for additional samples?*</td>
                </tr>
                <tr>
                <td class="answer_label">
                	Yes
				</td>
                </tr>
                 <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td class="answer_label">
                	No Comments
				</td>
                </tr>
                
         </table>       
         
        </div> -->
        <div class="thirdContent" style="padding-left:15px; padding-bottom:40px;">
        <div class="table_heading">Current & Upcoming Activities</div>
	
	   <div class="table_col">
              <table width="100%" cellpadding="0" cellspacing="0" id="contact_table">
                <tr class="header">
                    <th class="first"></th>
                    <th class="center">Summary</th>
                      <th class="center">Date</th>
                       <th class="center">Time</th>
                        <th class="center">Who</th>
                       <th class="center">Actions</th>
                    <th class="last"></th>
                </tr>
         <?php if(empty($activities)&&empty($appiontmentactivities)){
			 echo "<tr><td colspan='7' class='label' style='font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;'>No Record Found</td></tr>";
			 }else{?>       
         <?php foreach ($activities as $act){
			 $datetime = $act->dueDateTime;
			$date = date('m/d/Y', strtotime($datetime));
			$time = date('H:i a', strtotime($datetime));
			$contactId=$task->getcontactId($act->taskId);
			 ?>
         
         
         	  <tr id="record<?php echo $act->taskId;  ?>">
                    <td></td>
                    <td><?php echo $act->title;?></td>
                    <td><?php echo $date;?>
                    <img  src="<?php echo ($act->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($act->repeatTypeId!=1?'recurs ':'')?>"/>	
                    </td>
                     <td><?php echo $time;?></td>
                      
                      <td><?php 
					   foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
					  ?></td>
                      	<?php if($act->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur('<?php echo $act->taskId;?>')" />
							<img onclick=<?php echo ($act->planId==0?"delupcomingActivities('$act->taskId')":"skipTask('$act->taskId')")?> src="<?php echo ($act->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($act->planId==0?'Archive':'Skip')?>" title="<?php echo ($act->planId==0?'Archive':'Skip')?>" />
								</td>        
													<?php }else{?>
                                <td><a href="../task/edit_task.php?task_id=<?php echo $act->taskId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick=<?php echo ($act->planId==0?"delupcomingActivities('$act->taskId')":"skipTask('$act->taskId')")?> src="<?php echo ($act->planId==0?'../../images/Archive-icon.png':'../../images/skip.png')?>" alt="<?php echo ($act->planId==0?'Archive':'Skip')?>" title="<?php echo ($act->planId==0?'Archive':'Skip')?>" />
                                </td>
                                                <?php }?>
													
                      <!--<td><a href="../task/edit_task.php?task_id=<?php //echo $act->taskId;  ?>"><img src="../../images/edit.png" title="edit" alt="edit" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingActivities('<?php //echo $act->taskId;  ?>');" /></td>-->
                      <td></td>
                 </tr>
					
         <?php } ?>      
         <?php foreach ($appiontmentactivities as $app){
			  $datetime = $app->endDateTime;
			$date = date('m/d/Y', strtotime($datetime));
			$time = date('H:i a', strtotime($datetime));
			 ?>
         	  <tr id="recordAppiontment<?php echo $app->appiontmentId;  ?>">
                    <td></td>
                    <td><?php echo $app->title;?></td>
                    <td><?php echo $date;?>
                     <img  src="<?php echo ($app->repeatTypeId!=1?'../../images/recurrence.png':'')?>" title="<?php echo ($app->repeatTypeId!=1?'recurs ':'')?>"/>	
                    </td>
                     <td><?php echo $time;?></td>
                      
                      <td>
                      
                      </td>
                      
                      <?php if($app->repeatTypeId!=1){
											
														?>
                               <td>
							<img src="../../images/edit.png" title="Edit" alt="Edit" onclick="editRecur()" />
							<img onclick="delupcomingAppiontments('<?php echo $app->appiontmentId; ?>')" src="../../images/Archive-icon.png" alt="Archive" title="Archive" />
								</td>        
													<?php }else{?>
                                <td><a href="../appiontment/edit_appiontment.php?appId=<?php echo $app->appiontmentId; ?>">
                                <img src="../../images/edit.png" title="Edit" alt="Edit" />
                                </a>
                                <img onclick="delupcomingAppiontments('<?php echo $app->appiontmentId; ?>')" src="../../images/Archive-icon.png" alt="Archive" title="Archive" />
                                </td>
                                                <?php }?>
						
                     <!-- <td><a href="edit_task.php"><img src="../../images/edit.png" title="edit" alt="edit" onclick="editAppiontment('<?php// echo $app->appiontmentId;  ?>');" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingAppiontments('<?php// echo $app->appiontmentId;  ?>');" /></td>-->
                      <td></td>
                 </tr>
					
         <?php } }?>  
                   </table>
               
       		</div>
	   </div>
<div class="thirdContent" style="padding-left:15px; padding-bottom:40px;">
	<div class="table_heading">History</div>
	   <div class="table_col">
              <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="appendnotes">
                <tr class="header" >
                    <th class="first"><input type="checkbox" /><label></label></th>
                    <th class="center">Summary</th>
                      <th class="center">Date</th>
                       <th class="center">Time</th>
                        <th class="center">Who</th>
                       <th class="center">Actions</th>
                    <th class="last"></th>
                </tr>
                  <?php
          foreach($notes as $n){
			?>
			<tr id="recordNotes<?php echo $n->noteId;  ?>">
                    <td><img src="../../images/sticky_notes.jpg" style="cursor:pointer; width:15px; height:15px;" id="img<?php echo $n->noteId;?>"></td>
                    <td><?php echo $n->notes;?></td>
                    <td>&nbsp;</td>
                     <td>&nbsp;</td>
                      
                      <td>&nbsp;</td>
                      <td><img src="../../images/delete.png" alt="delete" title="delete" style="margin-left:25px;" class="linkButton" onclick="delnotes('<?php echo $n->noteId;  ?>');" /></td>
                 </tr>
			<?php }?>
                <?php
				$nodate='1990-12-12';
				 foreach ($activitiesCompleted as $actCmplt){
					$datetime = $act->completedDate;
			$date = date('m/d/Y', strtotime($datetime));
			$time = date('H:i:s', strtotime($datetime));
			$contactId=$task->getcontactId($act->taskId);
					?>
         	  <tr id="record<?php echo $actCmplt->taskId;  ?>">
                    <td><input type="checkbox" name="contact<?php echo $actCmplt->taskId;  ?>"   /><label></label></td>
                    <td  class='cross'><?php echo $actCmplt->title;?></td>
                    <td  class='cross'><?php echo ( strtotime($date)< strtotime($nodate)?'':$date);?></td>
                     <td  class='cross'><?php echo $time;?></td>
                      
                      <td><?php 
					   foreach($contactId as $ci){
									$task_contact=$task->taskContact($ci->contactId);
									echo "<a href=../contact/contact_detail.php?contactId=".$task_contact->contactId.">".$task_contact->firstName." ".$task_contact->lastName."</a><br>";
														}
					  ?></td>
                      <td><a href="../task/edit_task.php?task_id=<?php echo $actCmplt->taskId;  ?>"><img src="../../images/edit.png" title="edit" alt="edit" /></a> &nbsp;<img src="../../images/delete.png" alt="delete" title="delete" class="linkButton" onclick="delupcomingActivities('<?php echo $actCmplt->taskId;  ?>');" /></td>
                 </tr>
					
         <?php } ?> 
         
               </table>
               
       		</div>
	   </div>
               
<?php		
		break;

		case "3":
		
?>
<div class="edit_content" >
<a href="tabs_contact_edit.php?id=3	" id="editTab3" class="editContact">Edit Profile</a>
</div>  
<div class="thirdContent" style="padding-top:10px;">
       
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
           <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          
                
                </tbody>
                </table>
        </div>
    
  <!--     
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3  class="question_label">Which of the following skincare products do you use daily or weekly?</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span> Cleanser</td>
                </tr>
                 <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span> Lift/Skin Tightener</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span> Eye Cream</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:12px;">iv.</span> Cleanser</td>
                </tr>
               
              
				
                
         </table>       
         
        </div>       
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">If you were to purchase all of those products at once, how much would your total bill be?*</td>
				</tr>
                <tr>
                <td  class="answer_label"> Less than $30</td>
                </tr>
               
                
         </table>       
         
        </div>
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Check the conditions below that best describe your skin: (check all that apply)*</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span> Dry</td>
                </tr>
                 <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span> Blackheads</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span> Dark Circles under the eyes</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:12px;">iv.</span> Thinning Skin</td>
                </tr>
				
         </table>       
         
        </div>    
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Are there any other improvements you would like to see in your skin?</td>
                </tr>
				<tr>
                <td  class="answer_label">Comments: No</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)</td>
                </tr>
                <tr>
                <td class="answer_label">
                <b>TEAM LEADER:</b> Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.
                </td>
                
</tr>
                 <tr>
                <td class="answer_label">
                <b>SALES CONSULTANT:</b> Flexible schedule, work from home. 10-15 hrs/wk. Marketing & Customer Service: Mail samples & follow up by phone. Training provided, no experience necessary.
                </td>
                </tr>
                 <tr>
                <td class="answer_label">
                
               <b>VIRTUAL HOST:</b>  Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.

                </td>
                </tr>
                 
         </table>       
         
        </div>       
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                 <td class="answer_label">
                
                No Comments
                </td>
                </tr>
                
         </table>       
         
        </div>-->
<?php		
	
		break;

		case "4":
?>
<div class="edit_content">
<a href="tabs_contact_edit.php?id=4	" id="editTab4" class="editContact">Edit Profile</a>
</div>  
	<div class="thirdContent" style="padding-top:10px;">
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                    <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
                
                
                </tbody>
                </table>
        </div>
       
 <!--      
<div class="third_content" >
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">Which of the following supplements do you use? (Check all that apply):*</td>
                </tr>
                <tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:15px;">i:</span>Multi-Minerals
                </td>
				
                </tr>
                <tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:13px;">ii:</span>Multi-Vitamins
                </td>
				
                </tr><tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:10px;">iii:</span>Hormone Balancing
                </td>
				
                </tr>
         </table>       
         
        </div>       
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which of the following products do you use to boost your energy throughout the day?*</td>
				</tr>
               <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:15px;">i.</span>Coffee</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:13px;">ii.</span> Green Tea</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:10px;">iii.</span> Soda</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:12px;">iv.</span> Energy Drinks</td>
                </tr>
                 </table>       
         
        </div>	
	       
<div class="third_content">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Which of the following do you experience on a regular basis? (Check all that apply)*</td>
                </tr>
              <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:15px;">i.</span> Depression or mood swings</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:13px;">ii.</span>Recurring fungal infections </td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:10px;">iii.</span> Allergies (nasal congestion, post nasal drip, stuffy nose)</td>
                </tr>
                </tbody>
                </table> 
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">How many glasses of water do you think you drink each day?</td>
                </tr>
				<tr>
                <td  class="answer_label"> 4 glass</td>
                </tr>
				<tr>
                <td  class="question_label">
                Do you currently exercise 3 or more days per week?
				</td>
                </tr>
				<tr>
                <td  class="answer_label"> No</td>
                </tr>
               
                
                
         </table>       
         
        </div><div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Describe your daily eating patterns including meals and snacks:*</td>
                </tr>
				<tr>
                <td  class="answer_label"> Meat and milk</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Do you eat breakfast within 30 minutes of waking up?</td>
                </tr>
				<tr>
                <td  class="answer_label"> No</td>
                </tr>
                
                
         </table>       
         
        </div>
        <div style="clear:both;"></div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Recognizing that different people have different motivations for changing their health & eating habits, tell me a little bit about why you are interested in the 30 Days to Feeling Fit program.</td>
                </tr>
                <tr>
                <td  class="answer_label"> Ok sure</td>
                </tr>
         </table>       
         
        </div>   
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Do you need help with ideas for healthy snacks and meals?</td>
                </tr>
				<tr>
                <td  class="answer_label"> Yes</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)</td>
                </tr>
                <tr>
                <td class="answer_label">
                
                <b>TEAM LEADER:</b>  Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.

                </td>
                
</tr>
                 <tr>
                <td class="answer_label">
                <b>SALES CONSULTANT:</b> Flexible schedule, work from home. 10-15 hrs/wk. Marketing & Customer Service: Mail samples & follow up by phone. Training provided, no experience necessary.
                </td>
                </tr>
                 <tr>
                <td class="answer_label">
                 <b>VIRTUAL HOST:</b>  Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.

                </td>
                </tr>
                
         </table>       
         
        </div>       
<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td  class="answer_label"> No Comments</td>
                </tr>
                
         </table>       
         
        </div>		

 -->
<?php
		break;
	case "5":
?>
<div class="edit_content" >
<a href="tabs_contact_edit.php?id=5	" id="editTab5" class="editContact">Edit Profile</a>
</div>  
<div class="thirdContent" style="padding-top:10px;">
        
         <table border="0" cellpadding="0" width="35%">
                <tbody>
                    <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
                
                
                </tbody>
                </table>
        </div>
   <!-- <div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">Based on the information you have so far, which of the following best describes your level of interest at this time?</td>
                </tr>
                <tr>
                <td class="answer_label">
                
                <b>Curious but cautious.</b>  I have some questions and want more information but I'm not ready to commit to anything yet.
                </td>
                
</tr>
                
         </table>       
         
        </div> 
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">How many hours per week would you plan to commit to Arbonne?*</td>
                </tr>
				<tr>
                <td  class="answer_label"> 2 Hours</td>
                </tr>
				<tr>
                <td  class="question_label">
                What days and times are you most likely to schedule your Arbonne activities?*
				</td>
                </tr>
				<tr>
                <td  class="answer_label"> Monday,Tuesday,Friday</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">If you anticipate having difficulty scheduling Arbonne consistently, what will likely cause the challenges? (Work, Kids' activities, etc)*</td>
                </tr>
                <tr>
                <td  class="answer_label"> Activities</td>
                </tr>
         </table>       
         
        </div>
<div class="third_content">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">What are your goals/expectations regarding monthly income for the following time frames?</td>
                </tr>
                 <tr>
                   
                    <td class="answer_label">In your first year :</td>
                    <td class="answer_label">
                    $50-$200/mo</td>
                     
                 </tr>
				 <tr >
                   
                    <td class="answer_label">In 1-2 years:</td>
                    <td class="answer_label">
                    $200-$1000/mo</td>
                     </tr>
				 <tr >
                   
                    <td class="answer_label">In 2-3 years :</td>
                    <td class="answer_label">
                     $1000-$3500/mo</td>
                    
                    
                 </tr>
				 <tr >
                   
                    <td class="answer_label">In 3-4 years :</td>
                    <td class="answer_label">
                     $3500-$10,000/mo</td>
                    
                    
                    
                 </tr>
				 <tr>
                   
                    <td class="answer_label">In 5+ Years :</td>
                    <td class="answer_label">$10,000/mo+</td>
                     
                 </tr>
                 
               
                
                
         </table>       
         
        </div>		
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which amount below most closely represents your budget for starting your Arbonne business?</td>
                </tr>
				<tr >
                <td  class="answer_label">$200</td>
                </tr>
				
                
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">If you're not quite sure about moving forward as a consultant, what are the things that are holding you back?</td>
                </tr>
               <tr >
                <td class="answer_label" > I Will Consult It</td>
                </tr>
         </table>       
         
        </div> 
		<div style="clear:both;"></div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
               <tr>
                <td  colspan="2" class="question_label">Why are you interested in the Arbonne Opportunity?</td>
                </tr>
               <tr >
                <td class="answer_label" > I Like Quality Work.</td>
                </tr> 
                
         </table>       
         
        </div>
        
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">Briefly describe your work experience to date. Include what you think your strengths and challenges are in a working environment.</td>
                </tr>
               <tr >
                <td class="answer_label" > It will be going good.</td>
                </tr>
         </table>       
         
        </div>

		<div class="third_content">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">Which of the following marketing methods is most appealing to you? (You'll be able to choose one or all based on your preferences)*</td>
                </tr>
                <tr>
                   
                    <td class="answer_label" >Scheduling live virtual parties where you provide them with information, mail them a sample and follow up by phone to help them place an order.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 <tr>
                   
                    <td class="answer_label">Scheduling home parties hosted by yourself or others where guests can try products while you provide them with information and help them place orders.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 <tr>
                   
                    <td class="answer_label">Scheduling time to meet people one on one to give them information and a full-size set to try for 3-4 days, then returning to get the set and help them place an order.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 
         </table>       
         
        </div>
		<div class="third_content">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">How comfortable are you with each of the following? There are no good or bad answers - this just gives us an idea of where you might need the most training*</td>
                </tr>
                  <tr>
                   
                    <td class="answer_label">Connecting with people through facebook.<br /></td>
                    <td class="answer_label">
                    Umm...I can try.</td>
                     
                 </tr>
                  <tr>
                   
                    <td class="answer_label">Using online tools (surveys, email, etc.).</td>
                    <td class="answer_label">
                    
                    Umm...I can try.</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Technology in general.</td>
                    <td class="answer_label">
                    Piece of Cake!!</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Meeting new people.</td>
                    <td class="answer_label">
                    Umm...I can try.</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Talking to people you know about your business.</td>
                    <td class="answer_label">
                   Sure...I can do that.</td>
                     
                 </tr>
                         </table>       
         
        </div>
		
		<div class="third_content">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td  class="answer_label">No comments</td>
                </tr>
                
         </table>       
         
        </div>	-->

<?php	
	break;
	case "6":
?>
<div class="edit_content">
<a href="tabs_contact_edit.php?id=6	" id="editTab6" class="editContact">Edit All</a>
</div>  
<div class="table_heading">Basic Information</div>
    	
<div class="firstContent">        
<table cellspacing="0" cellpadding="0">
          <tr>
          	<td class="label">Name</td>
            <td class="label"><?php echo $contactdetail->title.' '.$contactdetail->firstName.' '.$contactdetail->lastName; ?></td>
          </tr>
          <tr>
          	<td class="label">Address</td>
            <td class="label"><?php echo $contactaddress->zipcode.' '.$contactaddress->street.' '.$contactaddress->city.' '.$contactaddress->state.' '.$contactaddress->country; ?></td>
          </tr>
          <tr>
          <tr>
          	<td class="label">Email</td>
            <td class="label"><?php echo $conatctemail->email; ?></td>
          </tr>
          <tr>
          	<td class="label">Phone</td>
            <td class="label"><?php echo $conatctphone->phoneNumber; ?></td>
          </tr>
          <tr>
          <td class="label" >Best Times To Call</td>
            <td class="label"><?php echo $contacttimetocall->weekdays."<br>".$contacttimetocall->weekdayEvening."<br>".$contacttimetocall->weekenddays."<br>".$contacttimetocall->weekenddayEvening; ?></td>
          </tr>
          <td class="label">Referred By</td>
          <td class="label">
          <?php echo $contactdetail->referredBy;  ?></td>
          </tr>
          <tr>
          <td class="label">Birthday</td>
          <td class="label">
           <?php echo $contactdetail->dateOfBirth;  ?></td>
          </tr>
          <td class="label">Age</td>
          <td  class="label">
           </td>
          </tr>
          
          <tr>
          	<td class="label">Company /Title</td>
            <td class="label"><?php echo $contactdetail->companyTitle.'-'.$contactdetail->jobTitle;  ?></td>
          </tr>
          
</table>
</div>
</div>

<div class="secondContent">
<div class="t_heading gray">Follow-ups</div>
<table class="contact_table" style="width:500px;" cellspacing="0" cellpadding="0">
    
        <?php
			
			$showPlan=$plan->showPlanStepsInContactDetail($contactId);//show all groups sort by group type
			if(empty($showPlan)){
				echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Task Assign</span>";
				echo "</td></tr>";
				}else{
            foreach($showPlan as $sp){
			?>
            <tr>
          	<td>
			<img src="../../images/attachedemail.png" />
			</td>
            <td class="plan_label" ><?php echo $sp->summary;?></td>
			
          </tr>
				
			<?php		
				}}
			?>
			

</table>
</div>

<div class="sticky_notes">
			<img src="images/note--top.png" />	
            <textarea class="stickynotes" onblur="saveNotes();" id="stickyNotes"><?php echo $stickyNotes->notes;?></textarea>
			<span  class="gray" style="color: #9B9B9B;font-weight: bold;
    text-decoration: none; background-image:url(../../images/note-center.png); background-repeat:no-repeat; display:block; padding-left:64px; width:238px; height:37px; cursor:pointer; " onclick="moveToHistory();" >Move To History</span>
			</div>
<div class="secondContent">
<div class="t_heading gray">Groups</div>
<div id="groupslist">
                	<ul>

<?php
$contactgroups=$group->showContactGroups($contactId);
if(empty($contactgroups)){
	echo "<tr><td>&nbsp;</td><td>";
				echo "<span class='label'>No Group Assign</span>";
				echo "</td></tr>";
	}else{
foreach($contactgroups as $cg){
	?>
                    	<li style="background-color:<?php echo $cg->color;?>"><input type="checkbox" checked="checked" id="groupcheckbox[]" name="groupcheckbox[]" value="<?php echo $cg->groupId; ?>"><label class="plan_label"><?php echo $cg->name;?></label></li>
	
<?php }
	}
?>
                    
                    </ul>
 </div>


</div>

<!--<table class="contact_table" style="width:278px;" cellspacing="0" cellpadding="0">
		<tr style="background-color:#D1D1D1;">
          	<td class="plan_label1"><input type="checkbox" name="group[]" checked="checked"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label">Interested in the Opportunity</td>
          </tr>
        <tr style="background-color:#BBBBFF;">
          	<td class="plan_label1"><input type="checkbox" name="group[]" checked="checked"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label"> Independent Consultants</td>
          </tr>
        <tr style="background-color:#DCAFEF;">
          	<td class="plan_label1"><input type="checkbox" name="group[]"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label">Clients</td>
          </tr>
          <tr style="background-color:#FFC1E0;">
          	<td class="plan_label1"><input type="checkbox" name="group[]" checked="checked"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label">Hosts</td>
          </tr>
          <tr class="label" style="background-color:#A8FFA8;">
          	<td class="plan_label1"><input type="checkbox" name="group[]"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label">Guests</td>
          </tr>
          <tr style="background-color:#FFCFB9;">
          	<td class="plan_label1"><input type="checkbox" name="group[]"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label" >Newsletter</td>
          </tr>
          <tr style="background-color:#FFBBBB;">
          	<td class="plan_label1"><input type="checkbox" name="group[]"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label" >Interested in Hosting</td>
          </tr>
          <tr style="background-color:#FFFFC4;">
          	<td class="plan_label1"><input type="checkbox" name="group[]" checked="checked"/><label style="display: inline-block;height: 12px;margin-bottom: 10px;margin-left: 10px;"></label></td>
            <td class="plan_label" >Fit kit purchase</td>
          </tr>
          
          
      </table>-->
</div>
 
     
     
<!--           
        <div class="thirdContent">
         <div class="table_heading" style="border-bottom:1px solid #666;">Guest Profile</div>
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">What brand(s) of weight loss/nutrition products are you currently using? (GNC, Slimfast, Isagenix, etc.)</td>
                </tr>
                <tr>
                <td  class="answer_label">GNC</td>
                </tr>
                <tr>
                <td  class="question_label">What brand(s) of skincare are you currently using? (Clinique, Olay, NuSkin, etc.)</td>
                </tr>
                <tr>
                <td  class="answer_label">Clinique</td>
                </tr>
         </table>       
         
        </div>  
         <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">How often do you add to your monthly grocery bill by stopping for coffee, snacks, fast food, etc?</td>
                </tr>
                <tr>
                 <td  class="answer_label">0-1 times per week</td>
                </tr>
                
         </table>       
         
        </div>
        
<div class="thirdContent">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Which of the following do you experience on a regular basis? (Check all that apply)</td>
                </tr>
                <tr>
                 <td  class="answer_label">Brain fog, lack of concentration and/or poor memory</td>
                </tr>
                </tbody>
                </table> 
         
        </div>
        
<div class="thirdContent">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Check the conditions below that best describe your skin: (check all that apply)*</td>
                </tr>
                <tr> 
                <td  class="answer_label">Combination</td>
                </tbody>
                </table> 
         
        </div>
 <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3  class="question_label">Which of the following products does someone in your family use each month? (Check all that apply):*</td>
                </tr>
                <tr>
                <td  class="answer_label">Snack/Granola/Nutrition Bars</td>
                </tr>
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Imagine you could go to one store to buy all of the snack bars, energy drinks, protein shakes, vitamins, skincare, shampoo and cosmetics that you would need to last your family for a month, how much would your total bill be?</td>
                </tr>
                <tr>
                <td  class="answer_label">$250-$350</td>
                </tr>
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">If you had a magic wand, what would you change about your health, skin and lifestyle?</td>
                </tr>
                <tr>
                <td  class="answer_label">skin</td>
                </tr>
				<tr>
                <td  class="question_label">Have you used Arbonne before?*</td>
                </tr>
                <tr>
                <td  class="answer_label">Yes</td>
                </tr>
                
                
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which of the following are you most interested in? (Check all that apply)*</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span>Better Nutrition</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span>Arbonne Business</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span>Skincare Workshop</td>
                </tr>
                
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">When making a decision about purchasing products, which is more important to you?*</td>
                </tr>
                 <tr>
                <td  class="answer_label"><b>PRICE:</b> The least expensive option</td>
                </tr>
                 <tr>
                <td  class="question_label">What kind of service do you prefer?*</td>
                </tr>
                 <tr>
                <td  class="answer_label"><b>PERSONAL:</b> Phone support and help placing my order from my consultant</td>
                </tr>
                
                
                
                
         </table>       
         
        </div>

         <div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)*</td>
                </tr>
                <tr>
                <td class="answer_label">
               <b>TEAM LEADER:</b> Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.
				</td>
                </tr>
                <tr>
                <td class="answer_label">
                	<b>VIRTUAL HOST:</b> Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.
				</td>
                </tr>
				                
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Would you like to receive my monthly newsletter that features current specials and opportunities for additional samples?*</td>
                </tr>
                <tr>
                <td class="answer_label">
                	Yes
				</td>
                </tr>
                 <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td class="answer_label">
                	No Comments
				</td>
                </tr>
                
         </table>       
         
        </div>
            
<div class="thirdContent">
<div class="table_heading" style="border-bottom:1px solid #666;">Skin Profile</div>
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3  class="question_label">Which of the following skincare products do you use daily or weekly?</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span> Cleanser</td>
                </tr>
                 <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span> Lift/Skin Tightener</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span> Eye Cream</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:12px;">iv.</span> Cleanser</td>
                </tr>
               
              
				
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">If you were to purchase all of those products at once, how much would your total bill be?*</td>
				</tr>
                <tr>
                <td  class="answer_label"> Less than $30</td>
                </tr>
               
                
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Check the conditions below that best describe your skin: (check all that apply)*</td>
                </tr>
                <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:15px;">i.</span> Dry</td>
                </tr>
                 <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:13px;">ii.</span> Blackheads</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:10px;">iii.</span> Dark Circles under the eyes</td>
                </tr> <tr>
                <td  class="answer_label"><span style="padding-left:2px; padding-right:12px;">iv.</span> Thinning Skin</td>
                </tr>
				
         </table>       
         
        </div>    
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Are there any other improvements you would like to see in your skin?</td>
                </tr>
				<tr>
                <td  class="answer_label">Comments: No</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)</td>
                </tr>
                <tr>
                <td class="answer_label">
                <b>TEAM LEADER:</b> Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.
                </td>
                
</tr>
                 <tr>
                <td class="answer_label">
                <b>SALES CONSULTANT:</b> Flexible schedule, work from home. 10-15 hrs/wk. Marketing & Customer Service: Mail samples & follow up by phone. Training provided, no experience necessary.
                </td>
                </tr>
                 <tr>
                <td class="answer_label">
                
               <b>VIRTUAL HOST:</b>  Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.

                </td>
                </tr>
                 
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                 <td class="answer_label">
                
                No Comments
                </td>
                </tr>
                
         </table>       
         
        </div>
        
 
       
       
<div class="thirdContent" >

 <div class="table_heading" style="border-bottom:1px solid #666;">Fit Profile</div>
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td colspan=3 class="question_label">Which of the following supplements do you use? (Check all that apply):*</td>
                </tr>
                <tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:15px;">i:</span>Multi-Minerals
                </td>
				
                </tr>
                <tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:13px;">ii:</span>Multi-Vitamins
                </td>
				
                </tr><tr>
                <td class="answer_label">
                <span style="padding-left:2px; padding-right:10px;">iii:</span>Hormone Balancing
                </td>
				
                </tr>
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which of the following products do you use to boost your energy throughout the day?*</td>
				</tr>
               <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:15px;">i.</span>Coffee</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:13px;">ii.</span> Green Tea</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:10px;">iii.</span> Soda</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:12px;">iv.</span> Energy Drinks</td>
                </tr>
                 </table>       
         
        </div>	
	       
<div class="thirdContent">
                <table border="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                <td class="question_label" colspan="2" >Which of the following do you experience on a regular basis? (Check all that apply)*</td>
                </tr>
              <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:15px;">i.</span> Depression or mood swings</td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:13px;">ii.</span>Recurring fungal infections </td>
                </tr>
                <tr>
                <td  class="answer_label"> <span style="padding-left:2px; padding-right:10px;">iii.</span> Allergies (nasal congestion, post nasal drip, stuffy nose)</td>
                </tr>
                </tbody>
                </table> 
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">How many glasses of water do you think you drink each day?</td>
                </tr>
				<tr>
                <td  class="answer_label"> 4 glass</td>
                </tr>
				<tr>
                <td  class="question_label">
                Do you currently exercise 3 or more days per week?
				</td>
                </tr>
				<tr>
                <td  class="answer_label"> No</td>
                </tr>
               
                
                
         </table>       
         
        </div><div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Describe your daily eating patterns including meals and snacks:*</td>
                </tr>
				<tr>
                <td  class="answer_label"> Meat and milk</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Do you eat breakfast within 30 minutes of waking up?</td>
                </tr>
				<tr>
                <td  class="answer_label"> No</td>
                </tr>
                
                
         </table>       
         
        </div>
        <div style="clear:both;"></div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Recognizing that different people have different motivations for changing their health & eating habits, tell me a little bit about why you are interested in the 30 Days to Feeling Fit program.</td>
                </tr>
                <tr>
                <td  class="answer_label"> Ok sure</td>
                </tr>
         </table>       
         
        </div>   
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Do you need help with ideas for healthy snacks and meals?</td>
                </tr>
				<tr>
                <td  class="answer_label"> Yes</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">We are actively seeking people to help promote our products. If you like the samples you try, are you interested in any of the following? (Check all that apply)</td>
                </tr>
               <tr>
                <td class="answer_label">
                <b>TEAM LEADER:</b> Flexible schedule, work from home. 15+ hrs/wk: Marketing, Recruiting, Customer Service & Training. Mail Samples, follow up by phone & mentor new Sales Consultants. Training provided, no experience necessary.
                </td>
                
</tr>
                 <tr>
                <td class="answer_label">
                <b>SALES CONSULTANT:</b> Flexible schedule, work from home. 10-15 hrs/wk. Marketing & Customer Service: Mail samples & follow up by phone. Training provided, no experience necessary.
                </td>
                </tr>
                 <tr>
                <td class="answer_label">
                
               <b>VIRTUAL HOST:</b>  Invite friends to choose a free sample & attend a virtual party online! Host rewards = Free product + 50-80% discounts and free shipping.

                </td>
                </tr>
                
         </table>       
         
        </div>       
<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td  class="answer_label"> No Comments</td>
                </tr>
                
         </table>       
         
        </div>		
       
<div class="thirdContent">
<div class="table_heading" style="border-bottom:1px solid #666;">Consultant Profile</div> 
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">Based on the information you have so far, which of the following best describes your level of interest at this time?</td>
                </tr>
                <tr>
                <td class="answer_label">
                
                <b>Curious but cautious.</b>  I have some questions and want more information but I'm not ready to commit to anything yet.
                </td>
                
</tr>
                
         </table>       
         
        </div> 
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">How many hours per week would you plan to commit to Arbonne?*</td>
                </tr>
				<tr>
                <td  class="answer_label"> 2 Hours</td>
                </tr>
				<tr>
                <td  class="question_label">
                What days and times are you most likely to schedule your Arbonne activities?*
				</td>
                </tr>
				<tr>
                <td  class="answer_label"> Monday,Tuesday,Friday</td>
                </tr>
                
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">If you anticipate having difficulty scheduling Arbonne consistently, what will likely cause the challenges? (Work, Kids' activities, etc)*</td>
                </tr>
                <tr>
                <td  class="answer_label"> Activities</td>
                </tr>
         </table>       
         
        </div>
<div class="thirdContent">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">What are your goals/expectations regarding monthly income for the following time frames?</td>
                </tr>
                 <tr>
                   
                    <td class="answer_label">In your first year :</td>
                    <td class="answer_label">
                    $50-$200/mo</td>
                     
                 </tr>
				 <tr >
                   
                    <td class="answer_label">In 1-2 years:</td>
                    <td class="answer_label">
                    $200-$1000/mo</td>
                     </tr>
				 <tr >
                   
                    <td class="answer_label">In 2-3 years :</td>
                    <td class="answer_label">
                     $1000-$3500/mo</td>
                    
                    
                 </tr>
				 <tr >
                   
                    <td class="answer_label">In 3-4 years :</td>
                    <td class="answer_label">
                     $3500-$10,000/mo</td>
                    
                    
                    
                 </tr>
				 <tr>
                   
                    <td class="answer_label">In 5+ Years :</td>
                    <td class="answer_label">$10,000/mo+</td>
                     
                 </tr>
                 
               
                
                
         </table>       
         
        </div>		
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Which amount below most closely represents your budget for starting your Arbonne business?</td>
                </tr>
				<tr >
                <td  class="answer_label">$200</td>
                </tr>
				
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">If you're not quite sure about moving forward as a consultant, what are the things that are holding you back?</td>
                </tr>
               <tr >
                <td class="answer_label" > I Will Consult It</td>
                </tr>
         </table>       
         
        </div> 
		<div style="clear:both;"></div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
               <tr>
                <td  colspan="2" class="question_label">Why are you interested in the Arbonne Opportunity?</td>
                </tr>
               <tr >
                <td class="answer_label" > I Like Quality Work.</td>
                </tr> 
                
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  colspan="2" class="question_label">Briefly describe your work experience to date. Include what you think your strengths and challenges are in a working environment.</td>
                </tr>
               <tr >
                <td class="answer_label" > It will be going good.</td>
                </tr>
         </table>       
         
        </div>

		<div class="thirdContent">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">Which of the following marketing methods is most appealing to you? (You'll be able to choose one or all based on your preferences)*</td>
                </tr>
                <tr>
                   
                    <td class="answer_label" >Scheduling live virtual parties where you provide them with information, mail them a sample and follow up by phone to help them place an order.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 <tr>
                   
                    <td class="answer_label">Scheduling home parties hosted by yourself or others where guests can try products while you provide them with information and help them place orders.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 <tr>
                   
                    <td class="answer_label">Scheduling time to meet people one on one to give them information and a full-size set to try for 3-4 days, then returning to get the set and help them place an order.</td>
                    <td class="answer_label">
                    Aargh... Scary!!!</td>
                     
                 </tr>
                 
         </table>       
         
        </div>
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="99%">
                <tr>
                <td  class="question_label" colspan="2">How comfortable are you with each of the following? There are no good or bad answers - this just gives us an idea of where you might need the most training*</td>
                </tr>
                  <tr>
                   
                    <td class="answer_label">Connecting with people through facebook.<br /></td>
                    <td class="answer_label">
                    Umm...I can try.</td>
                     
                 </tr>
                  <tr>
                   
                    <td class="answer_label">Using online tools (surveys, email, etc.).</td>
                    <td class="answer_label">
                    
                    Umm...I can try.</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Technology in general.</td>
                    <td class="answer_label">
                    Piece of Cake!!</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Meeting new people.</td>
                    <td class="answer_label">
                    Umm...I can try.</td>
                     
                 </tr> <tr>
                   
                    <td class="answer_label">Talking to people you know about your business.</td>
                    <td class="answer_label">
                   Sure...I can do that.</td>
                     
                 </tr>
                         </table>       
         
        </div>
		
		<div class="thirdContent">
        <table border="0" cellpadding="0" width="100%">
                <tr>
                <td  class="question_label">Additional comments or questions?</td>
                </tr>
                <tr>
                <td  class="answer_label">No comments</td>
                </tr>
                
         </table>       
         
        </div>-->	


<?php	
	}
?>