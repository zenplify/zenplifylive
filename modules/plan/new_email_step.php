<?php
session_start();
error_reporting(0);
include_once("../headerFooter/header.php");
require_once('../../classes/plan.php');
require_once('../../classes/contact.php');
include_once("../../classes/notification.php");
$message = '';

$database = new database();
$notification = new notification();

$pl = new plan();
$c1 = new contact();

$userId = $_SESSION['userId'];
$leaderId = $_SESSION['leaderId'];

$plan_id = $_REQUEST['planId'];
$step_id = $_REQUEST['step_id'];
$email = $c1->GetEmail($userId);
if (empty($step_id)) {
    $templates = $pl->getEmailTemplates();
    $i = 1;
    foreach ($templates as $temp) {
        $templatesemail = $templatesemail . '<option value="' . $temp->templateId . '">' . $temp->title . '</option>';
        $inputs = $inputs . '<input type="hidden" name="sub' . $i . '" id="sub' . $i . '" value="' . $temp->subject . '" />';
        $i++;
    }
}
if (isset($_POST['submit'])) {

    preg_match_all('#\[{(.*?)\}]#', $_POST['area2'], $match);
//		print_r($match[1]);
    //-------------------------------------------------------//


    $result = $pl->getFileextention($_FILES["file"]["name"]);

    if (empty($result)) {
        $message = 1;
        $newfilename = '';
        $originalName = '';

        // $id = $pl->SavePlanEmail($_POST, $plan_id, $userId, $leaderId, 5, 1, $notification,NULL,NULL);
    } else {

        $message = 0;
        $temp = explode(".", $_FILES["file"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $newfilename = rand(0, 99999999) . 'ZEN_FILE' . $newfilename;
        $originalName = $_FILES["file"]["name"];

        $uploadfile = move_uploaded_file($_FILES["file"]["tmp_name"], "../../images/emailAttachments/" . $newfilename);


    }


    //$uploadfile =  move_uploaded_file($_FILES["file"]["name"], "../../images/emailAttachments/" . $_FILES["file"]["tmp_name"]);


    $id = $pl->SavePlanEmail($_POST, $plan_id, $userId, $leaderId, 5, 1, $notification, $newfilename, $originalName);
    //-------------------------------------------------------//


//		$notification->notificationToUser($database, $userId, $id, 5, 1, "Do you want to add Steps ".$_POST['summary']." to Plan ?");
    $loc = "create_plan_detail.php?id=" . $plan_id;
    SendRedirect($loc);


}


//$step_id=3;

if (isset($_POST['update'])) {

    $result = $pl->getFileextention($_FILES["file"]["name"]);

    if (empty($result)) {
        $message = 1;
        $newfilename = '';
        $originalName = '';


    } else {
        $temp = explode(".", $_FILES["file"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $newfilename = rand(0, 99999999) . 'ZEN_FILE' . $newfilename;
        $originalName = $_FILES["file"]["name"];

        $uploadfile = move_uploaded_file($_FILES["file"]["tmp_name"], "../../images/emailAttachments/" . $newfilename);

        $pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, $leaderId, 0, $notification, 5, 2, $newfilename, $originalName);
//		$pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, 0);
        //$pl->UpdatePlanTask($_POST, $step_id);
        $notification->notificationToUser($database, $userId, $step_id, 3, 2, "Do you want to Edit Plans Steps");
        $loc = "create_plan_detail.php?id=" . $plan_id;
        SendRedirect($loc);
    }
}

if (!empty($step_id)) {
    $data = $pl->GetPlanEmailInfo($step_id);
    $templates = $pl->getEmailTemplates();
    $i = 1;
    foreach ($templates as $temp) {
        $templatesemail = $templatesemail . '<option value="' . $temp->templateId . '">' . $temp->title . '</option>';
        $inputs = $inputs . '<input type="hidden" name="sub' . $i . '" id="sub' . $i . '" value="' . $temp->subject . '" />';
        $i++;
    }
    //print_r($data);
    //die();
    if (!empty($data)) {
        foreach ($data as $row) {
            $summary = $row->summary;
            $daysAfter = $row->daysAfter;
            $description = $row->description;
            $subject = $row->subject;
            $body = $row->body;
            $file = $row->attachmentOriginalName;
            $body = str_replace("\'", "'", $body);
        }
    }
}
if (isset($_POST['update'])) {
    $pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, $leaderId, 0, $notification, 5, 2);

//		$pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, 0);
    //$pl->UpdatePlanTask($_POST, $step_id);
    $notification->notificationToUser($database, $userId, $step_id, 3, 2, "Do you want to Edit Plans Steps");
    $loc = "create_plan_detail.php?id=" . $plan_id;
    SendRedirect($loc);
}
?>
<script type="text/javascript" src="../../js/nicEdit.js"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        // $('#templates').change(function (){ showtemplate();});
    });
    bkLib.onDomLoaded(function () {
        nicEditors.allTextAreas()
    });
    $(document).ready(function (e) {
        /*$("#firstName").click(function () {
         var cursorPosition = $('.nicEdit-main').prop("selectionStart");
         console.log(cursorPosition);
         $(".nicEdit-main").html($(".nicEdit-main").html() + "[{First Name}]");
         });
         $("#lastName").click(function () {
         $(".nicEdit-main").html($(".nicEdit-main").html() + "[{Last Name}]");
         });
         $("#phoneNumber").click(function (e) {
         $(".nicEdit-main").html($(".nicEdit-main").html() + "[{Phone Number}]");
         });
         $("#companyName").click(function (e) {
         $(".nicEdit-main").html($(".nicEdit-main").html() + "[{Company Name}]");
         });*/
    });

    function pasteHtmlAtCaret(html) {
        var sel, range;
        if (window.getSelection) {
            // IE9 and non-IE
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();

                // Range.createContextualFragment() would be useful here but is
                // non-standard and not supported in all browsers (IE9, for one)
                var el = document.createElement("div");
                el.innerHTML = html;
                var frag = document.createDocumentFragment(), node, lastNode;
                while ((node = el.firstChild)) {
                    lastNode = frag.appendChild(node);
                }
                range.insertNode(frag);

                // Preserve the selection
                if (lastNode) {
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            }
        } else if (document.selection && document.selection.type != "Control") {
            // IE < 9
            document.selection.createRange().pasteHTML(html);
        }
    }

    function showtemplate() {
        var id = $('#templates').val();
        //alert(id);
        if (id != '') {
            //alert('If : '+id);
            $.ajax({ url: 'getEmailTemp.php',
                data: {tempId: id},
                type: 'post',
                success: function (output) {
                    //alert(output);
                    if (id == '1') {
                        subject = $('#sub1').val();
                        $('#subject').val(subject);
                    }
                    if (id == '2') {
                        subject = $('#sub2').val();
                        $('#subject').val(subject);
                    }
                    if (id == '3') {
                        subject = $('#sub3').val();
                        $('#subject').val(subject);
                    }
                    $('#area2').val(output);
                    $('.nicEdit-main').html(output);

                    //nicEditors.allTextAreas() ;
                    //document.write(output);
                }
            });
        }
    }
</script>

<!--  <div id="menu_line"></div>-->
<div class="container">
    <h1 class="gray">New Email Step</h1>

    <form name="add_new_task" method="post" id="add_new_task" action="<?php $_SERVER['PHP_SELF'] ?>"
          onsubmit="return validate('add_new_task');" enctype="multipart/form-data">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td class="label">Summary</td>
                <td><input type="text" name="summary" id="summary" value="<?php echo $summary; ?>"
                           class="textfield"/> <span class="erroralert"
                                                     id="alertSummary">Please enter summary</span></td>
            </tr>
            <tr>
                <td class="label">Days After</td>
                <td><input type="text" name="daysAfter" id="daysAfter" class="smalltextfield"
                           value="<?php echo $daysAfter; ?>" onkeypress="return isNumber(event)"/> <span
                        class="col_comments">Days after previous step is completed</span><span class="erroralert"
                                                                                               id="alertDays">Please enter days</span>
                </td>
            </tr>
            <tr>
                <input type="hidden" name="emailId" id="emailId"/> <input type="hidden" name="contactId"
                                                                          id="contactId"/>
                <td class="label">From</td>
                <td><input type="text" name="from" id="from" class="textfieldbig" value="<?php echo $email; ?>"/></td>
            </tr>
            <tr>
                <td class="label">Use Stored Email (optional)</td>
                <td><select name="templates" id="templates" class="SelectField" style="margin:0 30px 10px 10px;"
                            onchange="showtemplate()">
                        <option value="">Select a stored message to use (optional)</option>
                        <?php echo $templatesemail; ?></select> <?php echo $inputs; ?></td>
            </tr>
            <tr>
                <td class="label">Subject</td>
                <td><input type="text" name="subject" id="subject" class="textfieldbig"
                           value="<?php echo $subject; ?>"/></td>
            </tr>
            <tr>
                <td class="label"></td>
                <td class="label"><input type="file" name="file" id="file"
                                         style="margin:0 0 10px 10px;"><?php echo "   File Format:(.jpg,.png,.gif,.txt,.xlsx,.PDF)"; ?>
                </td>

            </tr>
            <?php if (!empty($file)): ?>
            <tr id="attachment-file">
                <td class="label">Attached file</td>
                <td>


                    <p class="attachmentP"><img src="../../images/attachment.png"
                                                style="margin-right: 10px; padding-left: 8px;">  <?php echo $file; ?>
                        <img src="../../images/delete.png" id="delImage"
                             style="cursor: pointer; padding-left: 10px; float: right; padding-right: 3px;padding-top: 4px;"
                             onclick="deleteFile(<?php echo $step_id; ?>)"></p>

                    <?php endif; ?>
                </td>

            </tr>

            <tr>
                <td class="label"></td>
                <td>
                    <?php if ($message == 1): ?>
                        <p id="attachment-error">invalid extention use(jpg,png,gif,xlsx,xlsm,pptx,pptm,txt)</p>

                    <?php endif; ?>

                </td>
            </tr>

            <tr>
                <td>
                    <input type="button" value="" id="firstName" class="firstname"
                           onclick="document.getElementsByClassName('nicEdit-main')[0].focus(); pasteHtmlAtCaret('<b>[{First Name}]</b>');"/>
                    <input type="button" value=""
                           id="lastName" class="lastname"
                           onclick="document.getElementsByClassName('nicEdit-main')[0].focus(); pasteHtmlAtCaret('<b>[{Last Name}]</b>');"/>
                    <input
                        type="button" value="" id="phoneNumber" class="phonenumber"
                        onclick="document.getElementsByClassName('nicEdit-main')[0].focus(); pasteHtmlAtCaret('<b>[{Phone Number}]</b>');"/>
                    <input type="button" value=""
                           id="companyName" class="companyname"
                           onclick="document.getElementsByClassName('nicEdit-main')[0].focus(); pasteHtmlAtCaret('<b>[{Company Name}]</b>');"/>
                </td>
                <td style="padding-left:10px;">
                    <textarea name="area2" style="width:648px; height:200px; margin-left:12px;"
                              id="area2"><?php echo $body; ?></textarea>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td><?php

                    if (!empty($step_id)) {
                        echo "<input type=\"submit\" name=\"update\" class=\"update\" value=\"\"  >";
                    } else {
                        echo "<input type=\"submit\" name=\"submit\" id=\"submit\" class=\"save\" value=\"\"  >";
                    }

                    ?>
                    <input type="button" name="cancel" class="cancel" value=""
                           onclick="return cancelPage(<?php echo $plan_id; ?>)"/></td>
            </tr>
        </table>
    </form>
    <div style="min-height:70px;"></div>
    <?php
    include_once("../headerFooter/footer.php");
    ?>
    <script>
        function deleteFile(step_id) {

            if (confirm('Are you sure you want to delete this file?')) {
                $("#delImage").attr("src", "../../images/loader.gif");
                //$('#delImage').html('<img src="../../images/loader.gif" width="16" height="16">');
                $.ajax({ url: '../../classes/ajax.php',
                    data: {action: 'deleteFile', step_id: step_id},
                    type: 'post',
                    success: function (result) {
                        //alert(output);
                        $('#attachment-file').hide('slow').delay(5000).fadeOut('slow');
                        //document.write(output);
                    }
                });
            }
        }
    </script>
    <script type="text/javascript">
        function validate() {

            var file = $('input[type=file]').val()

            var extension = file.substr((file.lastIndexOf('.') + 1));
           


            if (extension == 'document' || extension.length === 0 || extension == 'docx' || extension == 'xlsx' || extension == 'xlsm' || extension == 'pptx' || extension == 'pptm' || extension == 'txt' || extension == 'gif' || extension == 'GIF' || extension == 'png' || extension == 'PNG' || extension == 'jpeg' || extension == 'JPEG' || extension == 'jpg' || extension == 'JPG' || extension == 'pdf' || extension == 'PDF') {

            }
            else {
                alert('selected file type is invalid.');
                return false;
            }


            var summary = false;
            var days = false;
            if ($('#summary').val() == '' || $('#summary').val() == null) {
                $('#alertSummary').css("visibility", "visible");
                summary = false;
            }
            else {
                $('#alertSummary').css("visibility", "hidden");
                summary = true;
            }
            if ($('#daysAfter').val() == '' || $('#daysAfter').val() == null) {
                $('#alertDays').css("visibility", "visible");
                days = false;
            }
            else {
                $('#alertDays').css("visibility", "hidden");
                days = true;
            }
            if (summary == true && days == true) {
                return true;
            }
            else {
                return false;
            }


        }
        function cancelPage(id) {
            //alert("hello");
            window.open('create_plan_detail.php?id=' + id, '_self', false)
        }
        function deleteStep() {
            if (confirm('Are you sure you want to permanently delete this Step?')) {
                var id = "<?php echo $step_id;?>";
                $.ajax({ url: '../../ajax/ajax.php',
                    data: {action: 'DeletePlanStep', step_id: id},
                    type: 'post',
                    success: function (output) {
                        //alert(output);
                        window.open('create_plan_detail.php?id=' + id, '_self', false)
                        //document.write(output);
                    }
                });
            }
        }
    </script>
