<?php
    session_start();
    include_once("../headerFooter/header.php");
    require_once('../../classes/plan.php');
    include('../../classes/group.php');
    $plan = new plan();
    $planId = $_GET['id'];

    $planInfo = $plan->GetPlanInfo($planId);
    $planStep = $plan->showPlanSteps($planId);
    (!empty($planStep) ? $stepflag = 1 : $stepflag = 0);
    $userId = $_SESSION['userId'];
    $leaderId = $_SESSION['$leaderId'];

    if (!empty($planInfo))
    {
        foreach ($planInfo as $row)
        {
            $plan_name = $row->title;
            $option = $row->isDoubleOptIn;
            $group_id = $row->groupId;
            $is_active = $row->isactive;
        }
    }
    //die();
?>
<script src="../../js/jquery.tablednd.js" type="text/javascript"></script>
<script src="../../js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="../../js/sort/jquery.ui.sortable.js"></script>
<!--
<script type="text/javascript" src="../../js/dragdrop/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/dragdrop/jquery-ui-1.7.1.custom.min.js"></script>
-->
<script type="text/javascript">
$(document).ready(function () {
    $(function () {
        //alert("hello");
        $("[id^='contentLeft'] ul ").sortable({ opacity: 0.6, cursor: 'move', update: function () {
            //var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
            //var plan_id=$('.planId').val();
            var plan_id = "<?php echo $planId; ?>";
            var order = $(this).sortable("serialize") + '&action=UpdateRecordsListings&planId=' + plan_id;
            $.post("../../ajax/ajax.php", order, function (theResponse) {
                alert(theResponse);
                //$("#contentRight").html(theResponse);
                //document.write(theResponse);
            });
        }
        });
    });
    //

    function DropDown(el) {
        this.dd = el;
        this.initEvents();
    }

    DropDown.prototype = {
        initEvents: function () {
            var obj = this;
            obj.dd.on('click', function (event) {
                $(this).toggleClass('active');
                event.stopPropagation();
            });
        }
    }
    $(function () {
        var dd = new DropDown($('#dd'));
        $(document).click(function () {
            // all dropdowns
            $('.wrapper-dropdown-5').removeClass('active');
        });
    });
    // sorting steps
    $('.sortableFields ').sortable({
        update: function (event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.ajax({
                url: "../../classes/ajax.php",
                type: "post",
                data: {action: 'updatePlanStepOrder', id: newOrder},
                success: function (result) {
                    $("#msg").html('Plan Step Ordered SuccessFully').css('color',
                        '#20BF04').show('slow').delay(5000).fadeOut('slow');
                    $("#usercheck").val(result);
                }
            });
        }
    });
    // sorting step end
    $('#new_follow').click(function () {
        loadPopupSurvey();
        centerPopupSurvey();
    });
    $('#groups').click(function () {
        loadpopupGroups();
        centerPopupGroups();
    });
    $("#backgroundPopup").click(function () {
        disablePopupSurvey();
        disablePopupGroups();
    });
    $("#backgroundPopup1").click(function () {
        disablePopupSurvey();
        disablePopupGroups();
    });
    $('#cancel').click(function () {
        disablePopupSurvey();
        disablePopupGroups();
    });
    $('#update').click(function () {
        var user_id = "<?php echo $userId?>";
        var leaderId = "<?php echo $leaderId?>";

            if ($("#compaignName").val() == '' || $("#compaignName").val() == null) {
                $('#alert1').html('Please enter Follow-up name');
                return false;
            }
            else {
                var plan_name = $("#compaignName").val();
                var pOldName = $("#pOldName").val();

                var id = "<?php echo $planId; ?>";
                $.ajax({ url: '../../ajax/ajax.php',
                    data: {action: 'EditPlan', name: plan_name, plan_id: id, p_name: plan_name, planId: id, leaderId: leaderId, userId: user_id},
                    //data    : {action: 'ChangePlanName', name: plan_name, plan_id: id},
                    type: 'post',
                    success: function (output) {
                        $("#old_name").html(plan_name.bold());
                        document.location = "create_plan_detail.php?id=" + output;
                    }
                });
                //window.location='create_plan_detail.php';
                disablePopupSurvey();
                disablePopupGroups();
            }
        });
        $('.save2').click(function () {
            //window.location='create_plan_detail.php';
            //alert("hello");
            disablePopupGroups();
        });
        $('#update1').click(function () {
            if (<?php echo $stepflag; ?>==0
            )
            {
                alert('No step exist in Follow-ups');
            }
            else
            {
                var selected_val = '';
                var id = "<?php echo $planId; ?>";
                var userId = "<?php echo $userId; ?>";
                var groupId = '';
                //$("#my_select").change(function () {
                selected_val = $("#my_select option:selected").text();
                groupId = $("#my_select option:selected").val();
                $("#group_name").html(selected_val)
                //  $("#selected_group").html(selected_val);
                $.ajax({ url: '../../ajax/ajax.php',
                    data: {action: 'UpdatePlanGroup', plan_id: id, group_id: groupId, user_id: userId},
                    type: 'post',
                    success: function (output) {
                        //alert(output);
                        //window.open ('create_plan.php','_self',false);
                        //document.write(output);
//						document.location = "create_plan_detail.php?id=" + output;
//						location.reload();
                    }
                });
                //  })
                disablePopupGroups();
            }
        });
        $("#cancelOpt").click(function () {
            disablepermissionPopup();
        });
        $("#agreeOpt").click(function () {
            var user_id = "<?php echo $userId; ?>";
            var leaderId = "<?php echo $leaderId; ?>";
            var id = "<?php echo $planId; ?>";
            var pOldName = $("#pOldName").val();

            $.ajax({ url: '../../ajax/ajax.php',
//				data    : {action: 'ChangePlanTyep', plan_id: id},
                data: {action: 'EditPlan', plan_id: id, updateType: 'optIn', leaderId: leaderId, userId: user_id, p_name: pOldName, planId: id },
                type: 'post',
                success: function (output) {
                    document.location = "create_plan_detail.php?id=" + output.trim();
//					location.reload();
                }
            });
        });
        $('#change').click(function () {
            loadPermissionManager();
        });
        $('#change1').click(function () {
            var user_id = "<?php echo $userId; ?>";
            var leaderId = "<?php echo $leaderId; ?>";
            var id = "<?php echo $planId; ?>";
            var pOldName = $("#pOldName").val();

            $.ajax({ url: '../../ajax/ajax.php',
                data: {action: 'EditPlan', plan_id: id, updateType: 'optIn', leaderId: leaderId, userId: user_id, p_name: pOldName, planId: id },
//				data    : {action: 'ChangePlanTyep', plan_id: id},
                type: 'post',
                success: function (output) {
                    document.location = "create_plan_detail.php?id=" + output;
                }
            });
            //$("#plan_type").show();
            //$("#plan_type1").hide();
        });
        $('#cancel1').click(function () {
            //disablePopupSurvey();
            disablePopupGroups();
        });
    })
    function archiveStep(stepId) {
        var thisVal = $("#archiveStep" + stepId).html();
        var stepName = $("#stepName_" + stepId).html();
        $("#archiveStep" + stepId).html('<img style="" src="../../images/loading.gif" width="18" height="18">');
        $.ajax({
            url: "../../classes/ajax.php",
            type: "post",
            data: {action: 'archiveStep', stepId: stepId, stepName: stepName},
            success: function (result) {
                $("#" + stepId).closest('tr')
                    .children('td')
                    .animate({ padding: 0 })
                    .wrapInner('<div />')
                    .children()
                    .slideUp(function () {
                        $(this).closest('tr').remove();
                    });
//				$("#archiveStep" + stepId).html(newVal);
            }
        });
    }
    ;


function archivePlan(planId, userId) {

    var planName = $("#plan_"+planId).html();
    $("#archive").hide('slow').delay(9000).fadeOut('slow');
   

    $.ajax({
        url    : "../../classes/ajax.php",
        type   : "post",
        data   : {action: 'archivePlan', planId: planId, userId: userId, planName : planName},
        success: function (result) {

            window.location.replace("plans_view.php");

        }
    });
}


</script>
<!--  <div id="menu_line"></div>-->
<div class="container">
<!-- <h1 class="gray">New Follow-up</h1>-->
</br>
<form name="add_new_task" action="" method="post">
<table cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px #CCC">
    <input type="hidden" name="pOldName" id="pOldName" value="<?php echo $plan_name; ?>"/>
    <tr>
        <td width="33%" style="border:solid 1px #CCC">
            <!--	<div style="border:solid #CCC;"> -->
            <div id="plan_name" style="display:block">
                <div class="left_label">
                    <label id="old_name"> &nbsp <b> <?php echo $plan_name; ?>  </b></label>
                </div>
                <div class="right_label">
                    <a href="javascript:" id="new_follow">Edit </a> &nbsp&nbsp
                    <!--<span id="new_follow" style="cursor:pointer">edit</span>-->
                </div>
                <div id="backgroundPopup"></div>
                <div id="popupSurvey">
                    <h1>Edit Follow-up Name</h1>
                    <span class="errorrequired" id="alert1" style="display:block;"></span>
                    <br/>

                    <form>
                        <!--<label class="survey_label">Follow-up </label>-->
                        <input type="text" class="textfield" name="compaignName" id="compaignName"
                               value="<?php echo $plan_name; ?>"/>
                        <br/>
                        &nbsp&nbsp <input type="button" class="update" id="update"/>
                        <input type="button" class="cancel" value="" id="cancel"/>
                    </form>
                </div>
            </div>
        </td>
        <td width="33%" style="border:solid 1px #CCC">
            <?php if ($option == 1)
            {
                ?>
                <div id="plan_type" style="display:block">
                    <div class="left_label" style="padding-top:3px !important">
                        <label id="type_statement"> &nbsp <b> Double Opt In </b></label>
                    </div>
                    <div class="right_label">
                        <a href="javascript:" id="change">Change </a> &nbsp&nbsp
                        <!--<span id="new_follow" style="cursor:pointer">edit</span>-->
                    </div>
                </div>
            <?php
            }
            else
            {
                ?>
                <div id="plan_type1" style="display:block">
                    <div class="left_label" style="padding-top:3px !important">
                        <label id="type_statement"> &nbsp <b> Single Opt In </b></label>
                    </div>
                    <div class="right_label">
                        <a href="javascript:" id="change1">Change </a> &nbsp&nbsp
                        <!--<span id="new_follow" style="cursor:pointer">edit</span>-->
                    </div>
                </div>
            <?php } ?>
        </td>
        <td width="33%" style="border:solid 1px #CCC">
            <div class="left_label" style="padding-top:3px !important">
                <label id="old_name_Group"> &nbsp <b> Linked to:</b>
					<span id="group_name">
					<?php
                        $name = group::GetGroupName($group_id);
                        if (!empty($name))
                        {
                            echo $name;

                        }
                        else
                        {
                            echo "No Group selected";
                        }
                    ?> </span></label>
            </div>
            <div class="right_label">
                <a href="javascript:" id="groups">Edit </a> &nbsp&nbsp
                <!--<span id="new_follow" style="cursor:pointer">edit</span>-->
            </div>
            <div id="backgroundPopup1"></div>
            <div id="PopupGroups">
                <h1>Select Group</h1>

                <form>
                    <!--<label class="survey_label">Follow-up </label>-->
                    </br>
                    <div id="option" style="margin-left:10px">
                        <!--  <select>
                            <option value="Not Linked">Not Linked</option>
                            <option value="Independent Consultants">Independent Consultants</option>
                            <option value=" Interested in the Opportunity"> Interested in the Opportunity</option>
                            <option value="Clients">Clients</option>
                            <option value="Hosts">Hosts</option>
                            <option value="Guests">Guests</option>
                            <option value="Newsletter">Newsletter</option>
                            <option value="Interested in Hosting">Interested in Hosting</option>
                            <option value="Fit Kit PURCHASE">Fit Kit PURCHASE</option>
                        </select>
                        -->


                        <?php
                            $allGroups = group::GetAllGroups();
                            $allGroups = group::GetUserGroups($userId);
                            if (!empty($allGroups))
                            {
                                $dropdownhtml = "<select id = \"my_select\" name=\"my_select\" ><Option Value = \"0\">Unlink From Group</Option>";
                                foreach ($allGroups as $row)
                                {
                                    $key = $row->groupId;
                                    $val = $row->name;
                                    $dropdownhtml .= "<Option Value = \"" . $key . "\">" . $val . "</Option>";
                                }
                                $dropdownhtml .= "</select>";

                            }

                            echo $dropdownhtml;


                        ?>

                        </br></br>
                    </div>
                    &nbsp&nbsp <input type="button" class="update" id="update1"/>
                    &nbsp&nbsp&nbsp&nbsp <input type="button" class="cancel2" value="" id="cancel1"/>
                </form>
            </div>
            <!--<div class="right_label">
                            <input type="button" name="step3" id="edit1_button" value=""  onclick="return step3Change()" />
                        </div>
                        </br>
                        <label id="selected_group"> </label>

                           </div>
                               <div id="step3_1" style="display:none">

                           </br>
                           <div class="left_label">
                                <label> <b> Groups: &nbsp;  </b></label>
                           </div>
                        <div id="option" class="left_label">
                            <select>
                                    <option value="Not Linked">Not Linked</option>
                                   <option value="Independent Consultants">Independent Consultants</option>
                                <option value=" Interested in the Opportunity"> Interested in the Opportunity</option>
                                <option value="Clients">Clients</option>
                                <option value="Hosts">Hosts</option>
                                <option value="Guests">Guests</option>
                                <option value="Newsletter">Newsletter</option>
                                <option value="Interested in Hosting">Interested in Hosting</option>
                                <option value="Fit Kit PURCHASE">Fit Kit PURCHASE</option>
                            </select>

                            </br></br>
                            <input type="button" name="save" id="save" value=""  onclick="return saveStep3()" />
                               <input type="button" name="Cancel" id="cancel" value=""  onclick="return cancelStep3()" />
                               </br>
                        </div>
                   </div>-->
        </td>
    </tr>
</table>
</br>
<!--Step Two -->
<fieldset class="field_set">
    <div id="plan_type" style="display:block">
        <div class="left_label">
            <label id="type_option"><b> Add Steps To Your Follow-up </b></br><span style="font-size: 12px;">Drag and drop to change the order of your campaign steps</span></label>
        </div>
        </br>
        </br>
        <div class="sub_container" style="min-height:0px; height:auto;">
            <div id="msg"></div>
            <table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter">
                <tbody class="sortableFields">
                <tr class="header">
                    <th class="first1">&nbsp;</th>
                    <th class="center big">Step Title <img src="../../images/bg.gif"/></th>
                    <th class="center big">Type <img src="../../images/bg.gif"/></th>
                    <th class="center big">Days after Previous Step <img src="../../images/bg.gif"/></th>
                    <th class="center ">Actions</th>
                    <th class="last"></th>
                </tr>
                <?php
                    $planSteps = plan::GetPlanStepsInfo($planId); //show all groups sort by group type

                    if (!empty($planSteps))
                    {
                        foreach ($planSteps as $p)
                        {
                            ?>
                            <tr id="<?php echo $p->stepId; ?>">
                                <td></td>
                                <td>
                                    <a id="stepName_<?php echo $p->stepId; ?>"
                                       href="<?php echo($p->isTask == 1 ? 'new_task_step.php' : 'new_email_step.php') ?>?planId=<?php echo $planId; ?>&step_id=<?php echo $p->stepId; ?>">
                                        <?php echo $p->summary; ?>
                                    </a></td>
                                <td><?php if ($p->isTask == 1)
                                    {
                                        echo "Task";

                                    }
                                    else echo "Email";?></td>
                                <td><?php echo $p->daysAfter; ?></td>
                                <td>
                                    <a id="stepName_<?php echo $p->stepId; ?>"
                                       href="<?php echo($p->isTask == 1 ? 'new_task_step.php' : 'new_email_step.php') ?>?planId=<?php echo $planId; ?>&step_id=<?php echo $p->stepId; ?>"><img
                                            src="../../images/edit.png" title="Edit" alt="Edit"/></a>
                                    <span id="archiveStep<?php echo $p->stepId; ?>" class="link"
                                          onClick="archiveStep('<?php echo $p->stepId; ?>')"><?php echo($p->isactive == '1' ? 'Archive' : 'Unarchive'); ?></span>
                                    <!-- <img src="../../images/delete.png" width="17" height="17" onClick="deletePlanStep('<?php //echo $p->stepId;?>')" />-->
                                </td>
                                <td></td>
                            </tr>

                        <?php
                        }
                    } ?>
                </tbody>
            </table>
            <div class="contentLeft" id="contentLeft">
                <ul>
                    <?php

                        //$planSteps=plan::GetPlanStepsInfo($planId);//show all groups sort by group type

                        //if(!empty($planSteps)){
                        //foreach($planSteps as $p)
                        //{
                    ?>
                    <!--
							<li id="recordsArray_<?php echo $p->stepId; ?> " style="background-color:E5E5E5 !important;">
			                    
			                        <div class="formcolleft"  >
			                                 <div class="formcolrightrow" ><label style="color:#000 !important"><?php echo $p->summary; ?></label></div>
			                         </div>
			                         <div class="formcolleftend" >
			                                 <div class="formcolrightrow" ><label style="color:#000 !important">Task</label></div>
			                         </div>
			                          
			                        <div class="formcolleft" >
			                                 <div class="formcolrightrow" ><label style="color:#000 !important"><?php echo $p->daysAfter; ?></label></div>
			                         </div>
			                        <div class="formcolleftend" >
			                           <a href="new_task_step.php?planId=<?php echo $planId; ?> &step_id=<?php echo $p->stepId; ?>"><img src="../../images/edit.png" title="Edit" alt="Edit" /></a>
			                           <img src="../../images/delete.png" width="17" height="17" onClick="deletePlanStep('<?php echo $p->stepId; ?>')" />
			                           
			                        </div>
			                		  
			                 </li>
			                -->
                    <?php
                        //  }}
                    ?>
                </ul>
            </div>
        </div>
        </br>
        <div class="clear"></div>
        <div class="left_label">
            <div class="dropdown">
                <div id="dd" class="wrapper-dropdown-5" tabindex="1">
                    <b> New Step</b>
                    <ul class="dropdown" style="padding:0px !important;">
                        <li><a href="new_task_step.php?planId=<?php echo $planId; ?>">Task</a></li>
                        <li><a href="new_email_step.php?planId=<?php echo $planId; ?>">Email</a></li>
                    </ul>
                </div>
                <!--<div class="submenu">
							<ul class="root">
								
								<li ><a href="plan_task.php?planId=<?php echo $id; ?>" >Task</a></li>
								<li ><a href="#settings">Email</a></li>
							</ul>
						</div>-->
            </div>
        </div>
    </div>
</fieldset>
</br></br>
<!--  End Step 4 -->
<a href="plans_view.php"><input type="button" id="back_plan_list_button" value=""/></a>
<!--<input type="button" name="back" id="back_plan_list_button" value=""  onclick="return backToPlan()" />-->
<!--<input type="button" name="delete" id="delete_plan" value=""  onclick="return deleteCampaign(<?php //echo $planId; ?>)" />-->
<div style="float:right">
    <!--  <div id="a" style="display:block">
              <input type="button" name="activate" id="activate" value=""  onclick="return changeButton()" />
     </div>
    <div id="d" style="display:none">
              <input type="button" name="disable" id="disable" value=""  onclick="return changeButton1()" />
     </div>
-->
    <?php
        if ($is_active == 1)
        {
            echo "<input type=\"button\" name=\"archive\" id=\"archive\"   onclick=\"archivePlan(" . $planId . ",
            ".$userId.")\"/>";
        }
        else
        {
            echo "<input type=\"button\" name=\"activate\" id=\"activate\"   onclick=\"return activatePlan(" . $planId . ")\" />";
        }
    ?>

    <a href="#" style="display: none" id="plan_<?php echo $planId; ?>" name="planName"><?php echo $plan_name; ?></a>

</div>
</form>
<div></br></br></br>
</div>
</div>

<?php
    include_once("../headerFooter/footer.php");
?>



<script type="text/javascript">
    function changeValue() {
        var display = document.getElementById("plan_name").style.display;
        document.getElementById("plan_name").style.display = "none";
        document.getElementById("plan_name1").style.display = "block";
    }
    function cancelChange() {
        document.getElementById("plan_name").style.display = "block";
        document.getElementById("plan_name1").style.display = "none";
    }
    function convertToSingle() {
        $("#plan_type").hide();
        $("#plan_type1").show();
    }
    function convertToDouble() {
        var id = "<?php echo $id; ?>";
        $.ajax({ url: '../../ajax/ajax.php',
            data: {action: 'ChangePlanTyep', plan_id: id},
            type: 'post',
            success: function (output) {

                //alert("HTML: " + $("#test").html());
            }

        });
        $("#plan_type").show();
        $("#plan_type1").hide();
    }
    //	function DropDown(el) {
    //		this.dd = el;
    //		this.initEvents();
    //	}
    //	DropDown.prototype = {
    //		initEvents: function () {
    //			var obj = this;
    //			obj.dd.on('click', function (event) {
    //				$(this).toggleClass('active');
    //				event.stopPropagation();
    //			});
    //		}
    //	}
    //	$(function () {
    //		var dd = new DropDown($('#dd'));
    //		$(document).click(function () {
    //			// all dropdowns
    //			$('.wrapper-dropdown-5').removeClass('active');
    //		});
    //	});
    function stepChange() {
        $("#step2").hide();
        $("#step2_1").show();
    }
    function cancelStepTwo() {
        $("#step2").show();
        $("#step2_1").hide();
    }
    function step3Change() {
        $("#step3").hide();
        $("#step3_1").show();
    }
    function cancelStep3() {
        $("#step3").show();
        $("#step3_1").hide();
    }
    function changeButton() {
        $("#d").show();
        $("#a").hide();
    }
    function changeButton1() {
        $("#a").show();
        $("#d").hide();
    }
    function deletePlanStep(id) {
        $.ajax({ url: '../../ajax/ajax.php',
            data: {action: 'DeletePlanStep', step_id: id},
            type: 'post',
            success: function (output) {
                location.reload();
            }
        });
    }
    function disablePlan(id) {
        $.ajax({ url: '../../ajax/ajax.php',
            data: {action: 'DisablePlan', plan_id: id},
            type: 'post',
            success: function (output) {
                location.reload();
                //alert(output);
                //window.open ('create_plan.php','_self',false);
                //document.write(output);
            }
        });
    }
    function activatePlan(id) {
        $.ajax({ url: '../../ajax/ajax.php',
            data: {action: 'ActivatePlan', plan_id: id},
            type: 'post',
            success: function (output) {
                location.reload();
            }
        });
    }
    function deleteCampaign(id) {
        //if(confirm('Are you sure you want to permanently delete this Campaign?'))
        //{
        $.ajax({ url: '../../ajax/ajax.php',
            data: {action: 'DeletePlan', plan_id: id},
            type: 'post',
            success: function (output) {

                //location.reload();
                //$('#example').datagrid('reload');
                //$("#wrap-fields").load(location.href + " #wrap-fields");
                //$("#"+id).remove();
                window.location.href = 'plans_view.php';
                //$("#example").load(location + " #example");
            }
        });
        //}
    }
    }
    // soting plan steps
    //    $(document).ready(function () {
    //
    //
    //
    //    });
</script>
<div id="Popup"></div>
<div id="popuppermission" style="font-size:14px;padding-top:15px;">
    <h1 class="gray">Zenplify Single Opt-in Agreement</h1>
    <br/><br/>
    To ensure that you clearly understand the single opt-in agreement, please read and agree to the points below.
    <br/><br/>
    <ol>
        <li style="font-weight:bold;"> You can not send Spam from Zenplify.</li>
        <br/>
        <li style="font-weight:bold;"> Spam is unsolicited bulk email.
            <ul style="font-weight:100;">
                <br/>
                <li>Unsolicited means that the recipient never asked to receive email from you.</li>
                <br/>
                <li>Bulk email means that the message has roughly the same content and is sent to multiple recipients.
                    The emails could be sent all at once - like a list mail, or one at a time - by sending the same
                    stored email to multiple people or via Email steps in the Relationship Builder
                </li>
                <br/>
                <li>Your emails are only considered Spam if they are both unsolicited AND bulk.</li>
                <br/>
            </ul>
        </li>
        <br/>
        <li style="font-weight:bold;"> You are assuming the responsibility for having received Permission to send bulk
            email to any and all Contacts added to this Follow-ups.
        </li>
        <br/>
        <li style="font-weight:bold;">Any Spam complaints from any of Contacts on this Follow-ups will result in the
            immediate suspension of outbound email services from your account, and could result in your account being
            terminated without any option of a refund for unused time of your current subscription period.
        </li>
        <br/>
    </ol>
    <br/><br/><br/>
    <input type="button" name="agree" id="agreeOpt" class="agree" value="">
    <input type="button" name="cancel" class="cancel" value="" id="cancelOpt"/></td>
    </p>
</div> 