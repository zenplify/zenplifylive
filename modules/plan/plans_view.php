<?php
	session_start();
	include_once("../headerFooter/header.php");
	include('../../classes/plan.php');
	include('../../classes/group.php');
	$userId = $_SESSION["userId"];
	$leaderId = $_SESSION["leaderId"];
	$pl = new plan();
	$gr = new group();

	$plans = $pl->GetAllPlans($userId, $leaderId);
?>


<script type="text/javascript" src="../../js/sorttable/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/sorttable/jquery.tablesorter.pager.js"></script>
<script src="../../js/jquery.tablednd.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#contact_table").tableDnD({
			onDragClass: "DragClass"
		});
		$("#contact_table")
			.tablesorter({widthFixed: true, widgets: ['zebra']})
			.tablesorterPager({container: $("#pager"), size: 50
			});
		$('#new_follow_up').click(function () {
			$('#alert1').html('');
			loadPopupSurvey();
			centerPopupSurvey()
		});

		$("#backgroundPopup").click(function () {
			disablePopupSurvey();
		});
		$('#cancel').click(function () {
			disablePopupSurvey();
		});
		$('#next_Compaign').click(function () {
			var user_id = "<?php echo $userId?>";
			var name = $("#compaignName");
			if ($("#compaignName").val() == '' || $("#compaignName").val() == null) {
				$('#alert1').html('Please enter Compaign name');
				return false;
			}
			else {
				$.ajax({ url: '../../ajax/ajax.php',
					data    : {action: 'CreatePlan', p_name: name.val(), userId: user_id},
					type    : 'post',
					success : function (output) {
						window.open("create_plan_detail.php?id=" + output.trim(), "_self", false);
					}
				});
			}
		});

	});
	function editPlan(id, title) {
		$('#alert2').html('');
		loadPopupEditLink();
		centerPopupEditLink();
		$('#compaignName1').val(title);
		$('#next_Compaign1').click(function () {
			var user_id = "<?php echo $userId?>";
			var leaderId = "<?php echo $leaderId?>";
			var planId = id;
			var compainvalue = $("#compaignName1").val();

			if (compainvalue == "") {
				$('#alert2').html('Please enter Compaign name');
				return false;
			}
			else {
				$.ajax({ url: '../../ajax/ajax.php',
					data    : {action: 'EditPlan', p_name: compainvalue, planId: planId, leaderId: leaderId, userId: user_id},
					type    : 'post',
					success : function (output) {
						window.open("create_plan_detail.php?id=" + output, "_self", false);
					}
				});
			}

		})

	}
	function cancelPopupEdit() {
		$("#backgroundPopup").fadeOut("slow");
		$("#popupEditLink").fadeOut("slow");
		popupStatus1 = 0;
	}
	function archivePlan(planId, userId) {
		var thisVal = $("#archivePlan" + planId).html();
		var planName = $("#plan_"+planId).html();
		$("#archivePlan" + planId).html('<img style="" src="../../images/loading.gif" width="18" height="18">');
		var newVal = (thisVal == 'Archive' ? 'Unarchive' : 'Archive').trim();
		$.ajax({
			url    : "../../classes/ajax.php",
			type   : "post",
			data   : {action: 'archivePlan', planId: planId, userId: userId, planName : planName},
			success: function (result) {
				//$("#"+planId).slideUp('slow');
				$("#" + planId).closest('tr')
					.children('td')
					.animate({ padding: 0 })
					.wrapInner('<div />')
					.children()
					.slideUp(function () { $(this).closest('tr').remove(); });
				//$("#archivePlan" + planId).html(newVal);
			}
		});
	}
	function publishPlan(planId) {
		var thisVal = $("#publishPlan" + planId).html();
		var planName = $("#plan_"+planId).html();
		$("#publishPlan" + planId).html('<img src="../../images/loading.gif" width="28" height="28">');
		var newVal = (thisVal == 'Publish Plan' ? 'Unpublish Plan' : 'Publish Plan').trim();
		$.ajax({
			url    : "../../classes/ajax.php",
			type   : "post",
			data   : {action: 'publishPlan', planId: planId, planName: planName},
			success: function (result) {
				$("#publishPlan" + planId).html('');
			}
		});
	}
</script>
<div class="container">
	<div class="top_content">
		<input type="button" name="new_follow_up" id="new_follow_up" value=""/>
	</div>
	<div id="backgroundPopup"></div>
	<div id="popupSurvey">
		<h1>Create New Compaign</h1>

		<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" id="frm">
			<label class="survey_label">Compaign Name</label>
			<span class="errorrequired" id="alert1" style="display:block;"></span> <br/>
			<input type="text" class="textfield" name="compaignName" id="compaignName"/>
			<br/>
			<input type="button" class="next_Compaign" id="next_Compaign"/>
			<input type="button" class="cancel" value="" id="cancel"/>
		</form>
	</div>
	<!----- popup for Edit ---->

	<div id="popupEditLink" style=" height: 200px; width: 254px;">
		<h1>Edit Compaign</h1>

		<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" id="frm">
			<label class="survey_label">Compaign Name</label>
			<span class="errorrequired" id="alert2" style="display:block;"></span> <br/>
			<input type="text" class="textfield" name="compaignName" value="" id="compaignName1"/>
			<br/>
			<input type="button" class="next_Compaign" id="next_Compaign1"/>
			<input type="button" class="cancel" value="" onclick="cancelPopupEdit()" id="cancel"/>
		</form>
	</div>
	<!----- popup for Edit ---->
	<div class="sub_container">
		<div class="col_plan_table">
			<table width="100%" cellpadding="0" cellspacing="0" id="contact_table" class="tablesorter">
				<thead>
				<tr class="header">
					<th class="first">&nbsp;Active</th>
					<th class="center big">&nbsp;&nbsp;Name <img src="../../images/bg.gif"/></th>
					<th class="center"># In Compaign <img src="../../images/bg.gif"/></th>
					<th class="center "># Block <img src="../../images/bg.gif"/></th>
					<th class="center ">Linked To Group <img src="../../images/bg.gif"/></th>
					<th class="center medcol">Actions</th>
					<th class="last"></th>
				</tr>
				</thead>
				<tbody>
				<?php
					if (!empty($plans))
					{
						foreach ($plans as $row)
						{
							//echo "<tr class=\"odd_gradeX\" id=\"2\">";
							$id = $row->planId;
							$groupid = $row->groupId;
							$userId = $row->userId;
							$groupName = $gr->GetGroupName($groupid);

							if (!empty($groupid) || $groupid != 0 || $groupid != null)
							{
								//$groupContacts=$gr->countGroupContacts($groupid,$userId);
//								$groupContacts = $gr->countPlanContacts($id);
							}
							else
							{
//								$groupContacts = $gr->countPlanContacts($id);
							}
//							$countBlockContact = $pl->getBlockContactCount($row->planId);
							echo "<tr class=\"odd_gradeX\" id=\"" . $id . "\">";
							//echo "<td><input type=\"checkbox\" " .($row->isactive=='1' ? ' checked="checked"' : '')."></td>";
							if ($row->isactive == '1')
							{
								echo "<td align=\"center\"> <a href=\"javascript:\" onclick=\"disablePlan(" . $id . ")\" ><img src=\"../../images/check-icon.png\" /></a></td>";
							}
							else
							{
								echo "<td align=\"center\"><a href=\"javascript:\" onclick=\"activatePlan(" . $id . ")\" ><img src=\"../../images/close-icon.png\" /></a></td>";
							}
							echo "<td><a id='plan_".$id."' href=create_plan_detail.php?id=" . $id . ">" . $row->title . "</a></td>";

							echo "<td>" . $groupContacts . "</td>";
							echo "<td>" . $countBlockContact . "</td>";
							echo "<td>" . $groupName . "</td>";


							echo "<td class='actions'>";

						/*	if (($row->leaderId == $userId OR $row->leaderId == null) && ($row->isactive == 1))
							{*/
								?>
								<span onclick="editPlan('<?php echo $id; ?>','<?php echo $row->title; ?>')" style='cursor:pointer;'><img src='../../images/edit.png'/></span>
							<?php
							// }
							echo "<span id='archivePlan" . $id . "' class=\"link\" onClick=\"archivePlan('" . $id . "','" . $userId . "')\">" . ($row->isactive == '1' ? 'Archive' : 'Unarchive') . "</span>";
							if (($leaderId == $userId) && ($row->privacy == 0))
							{
								echo "  <span id='publishPlan" . $id . "' class=\"link\" onClick=\"publishPlan('" . $id . "')\">" . ($row->privacy == '0' ? 'Publish Plan' : 'Unpublish Plan') . "</span></td>";
							}
							/* echo "<a href=\"javascript:\" ><img src=\"../../images/delete.png\" width=\"17\" title=\"Delete\" alt=\"Delete\" height=\"17\" onClick=\"deleteCampaign(".$id.")\" >
										</a></td>";*/
							echo "<td></td>";
							echo "<td></td>";
							echo "</tr>";
						}
					}
					else
					{
						echo '<tr><td colspan="7" class="label" style="font-size:12px; font-weight: bold;padding-left: 30px; padding-top: 10px;">No Record Found</td></tr>';
					}
				?>
				</tbody>

			</table>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" id="paging-table">
				<tbody>
				<tr>
					<td></td>
					<td>
						<div style="float:right; margin-top:10px;">
							<div class="pager" id="pager">
								<form>
									<img class="first_page" src="../../images/paging_far_left.gif"/> <img class="prev_page" src="../../images/paging_left.gif"/>
									<input type="text" class="pagedisplay" value="1/1"/>
									<img class="next_page" src="../../images/paging_right.gif"/> <img class="last_page" src="../../images/paging_far_right.gif"/>
									<select id="pagesize" name="pagesize" class="pagesize">
										<option value="50" selected="selected">50</option>
										<option value="100">100</option>
										<option value="250">250</option>
									</select>
								</form>
							</div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="empty"></div>
	<?php
		include_once("../headerFooter/footer.php");
	?>
	<script type="text/javascript">
		function disablePlan(id) {
			$.ajax({ url: '../../ajax/ajax.php',
				data    : {action: 'DisablePlan', plan_id: id},
				type    : 'post',
				success : function (output) {
					location.reload();
				}
			});
		}
		function activatePlan(id) {
			$.ajax({ url: '../../ajax/ajax.php',
				data    : {action: 'ActivatePlan', plan_id: id},
				type    : 'post',
				success : function (output) {
					location.reload();
				}
			});
		}
		function deleteCampaign(id) {
			$.ajax({ url: '../../ajax/ajax.php',
				data    : {action: 'DeletePlan', plan_id: id},
				type    : 'post',
				success : function (output) {
					location.reload();
				}
			});
		}
		function getPlanId() {
			var name = $("#compaignName").val();
			if (name.length != 0) {
				$.ajax({ url: '../../ajax/ajax.php',
					data    : {action: 'CreatePlan', p_name: name},
					type    : 'post',
					success : function (output) {
						alert(output);
						window.open("create_plan_detail.php?id=" + output, "_self", false);
					}

				});
			}
			else {
			}
		}
	</script>