<?php
	session_start();
	include_once("../headerFooter/header.php");
	require_once('../../classes/plan.php');
	include_once("../../classes/notification.php");

	$database = new database();
	$notification = new notification();
	$pl = new plan();
	$plan_id = $_REQUEST['planId'];
	$step_id = $_REQUEST['step_id'];

	$userId = $_SESSION["userId"];
	$leaderId = $_SESSION["leaderId"];

	if (isset($_POST['submit']))
	{
		$id = $pl->SavePlanTask($_POST, $plan_id, $notification, $userId, $leaderId, 5, 1);
//		$notification->notificationToUser($database, $userId, $id, 5, 1, "Do you want to add Steps to Plan ?");
		$loc = "create_plan_detail.php?id=" . $plan_id;
		SendRedirect($loc);
	}
	//$step_id=3;

	if (!empty($step_id))
	{
		$data = $pl->GetPlanTaskInfo($step_id);
		if (!empty($data))
		{
			foreach ($data as $row)
			{
				$summary = $row->summary;
				$daysAfter = $row->daysAfter;
				$description = $row->description;
			}
		}
	}
	if (isset($_POST['update']))
	{
//		$idStep = $pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, 1);
//		$notification->notificationToUser($database, $userId, $idStep, 5, 2, "Do you want to Edit Plans Steps");
		$idStep = $pl->EditPlanTask($_POST, $plan_id, $step_id, $userId, $leaderId, 1, $notification, 5, 2);
		$loc = "create_plan_detail.php?id=" . $plan_id;
		SendRedirect($loc); //echo "hello";
	}
?>

<!--  <div id="menu_line"></div>-->
<div class="container">
	<h1 class="gray">New Task Step</h1>

	<form name="add_new_task" method="post" id="add_new_task" action="<?php echo $_SERVER['PHP_SELF'] ?>" onsubmit="return validate();">
		<input type="hidden" name="planId" id="planId" value="<?php echo $plan_id; ?>">
		<input type="hidden" name="step_id" id="step_id" value="<?php echo $step_id; ?>">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td class="label">Summary</td>
				<td><input type="text" name="summary" id="summary" value="<?php echo $summary; ?>"
						   class="textfield"/><span class="erroralert" id="alertSummary">Please enter summary</span>
				</td>
			</tr>
			<tr>
				<td class="label">Days After</td>
				<td><input type="text" name="daysAfter" id="daysAfter" class="smalltextfield" value="<?php echo $daysAfter; ?>" onkeypress="return isNumber(event)"/>
					<span class="col_comments">Days after previous step is completed</span>
					<span class="erroralert" id="alertDays">Please enter days</span>
				</td>
			</tr>
			<tr>
				<td class="label">Reminder</td>
				<td>
					<select name="reminderInterval" id="form_reminder_Interval" class="SelectField"
							onchange="if (this.value == '-1') {displayReminder();} else if(this.value != '-1'){hideReminder();} else {hideReminder();}">
						<option value="-2">None</option>
						<option value="10">10 Minutes</option>
						<option value="30">30 Minutes</option>
						<option value="60">1 Hour</option>
						<option value="1440">1 Day</option>
						<option value="-1">Custom</option>
					</select>

					<p class="reminder">
						<input type="text" class="textfieldExSmall" id="form_reminder_Quantity" size="3" name="reminderQuantity">
						<select id="form_reminder_Unit" name="reminderUnit" class="SelectFieldSmall">
							<option value="1">Minutes</option>
							<option value="60">Hours</option>
							<option value="1440">Day(s)</option>
						</select>
					</p>
					<br/></td>
			</tr>
			<tr>
				<td class="label">Description</td>
				<td><textarea class="textarea" name="description"><?php echo $description; ?></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>

				<td class="label">&nbsp;</td>
				<td>


					<?php

						if (!empty($step_id))
						{
							//echo "<input type=\"button\" name=\"delete\" value=\"Delete Step\"  onclick=\"return deleteStep()\"></div>";
							echo "<input type=\"submit\" name=\"update\" class=\"update\" value=\"\"  >";
						}
						else
						{
							echo "<input type=\"submit\" name=\"submit\" id=\"submit\" class=\"save\" value=\"\"  >";
						}

					?>
					<input type="button" name="cancel" class="cancel" value=""
						   onclick="return cancelPage(<?php echo $plan_id; ?>)"/>
				</td>
			</tr>
		</table>
	</form>
	<div style="min-height:70px;"></div>
	<?php
		include_once("../headerFooter/footer.php");
	?>
	<script type="text/javascript">
		function validate() {
			var summary = false;
			var days = false;
			if ($('#summary').val() == '' || $('#summary').val() == null) {
				$('#alertSummary').css("visibility", "visible");
				$('#summary').focus();
				summary = false;
				return false;
			}
			else {
				$('#alertSummary').css("visibility", "hidden");
				summary = true;
			}
			if ($('#daysAfter').val() == '' || $('#daysAfter').val() == null) {
				$('#alertDays').css("visibility", "visible");
				$('#daysAfter').focus();
				days = false;
				return false;
			}
			else {
				$('#alertDays').css("visibility", "hidden");
				days = true;
			}
			if (summary == true && days == true) {
				return true;
			}
			else {
				return false;
			}
		}
		function cancelPage(id) {
			//alert("hello");
			window.open('create_plan_detail.php?id=' + id, '_self', false)
		}
		function deleteStep() {

			if (confirm('Are you sure you want to permanently delete this Step?')) {
				var id = "<?php echo $step_id;?>";

				$.ajax({ url: '../../ajax/ajax.php',
					data    : {action: 'DeletePlanStep', step_id: id},
					type    : 'post',
					success : function (output) {
						//alert(output);
						window.open('create_plan_detail.php?id=' + id, '_self', false)
						//document.write(output);
					}
				});
			}

		}


	</script>
	 
 